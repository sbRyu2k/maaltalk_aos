# Begin: Debug ProGuard rules

-dontobfuscate
-keepattributes SoureFile,LineNumberTable

# End: Debug ProGuard rules

# Begin: Common Proguard rules

# Don't note duplicate definition (Legacy Apche Http Client)
-dontnote android.net.http.*
-dontnote org.apache.http.*

# Add when compile with JDK 1.7
-keepattributes EnclosingMethod

# Firebase Authentication
-keepattributes *Annotation*

# Firebase Realtime database
-keepattributes Signature

-dontwarn okhttp3.**
-dontwarn okio.**

-dontnote okhttp3.**

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# End: Common Proguard rules

-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
    public <init>(...);
 }

-keep class org.pjsip.pjsua2** {*;}
-keep class org.pjsip.pjsua** {*;}

-keep class com.dial070.sip.utils.accessibility.** {*;}

-dontwarn me.pushy.**
-keep class me.pushy.** { *; }

-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.** { *; }
-keep class com.squareup.leakcanary.** { *; }

# Marshmallow removed Notification.setLatestEventInfo()
-dontwarn android.app.Notification

-keep class androidx.appcompat.widget.** { *; }