package com.dial070.widgets;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.widget.RemoteViews;

import com.dial070.maaltalk.R;
import com.dial070.sip.api.*;

public class RegistrationNotification extends RemoteViews
{

	private static final Integer[] cells = new Integer[] { R.id.cell1, R.id.cell2, R.id.cell3, };

	private static final Integer[] icons = new Integer[] { R.id.icon1, R.id.icon2, R.id.icon3, };

	private static final Integer[] texts = new Integer[] { R.id.account_label1, R.id.account_label2, R.id.account_label3, };

	public RegistrationNotification(String aPackageName)
	{
		super(aPackageName, R.layout.notification_registration_layout);
		
	}

	public void clearRegistrations()
	{
		for (Integer cellId : cells)
		{
			setViewVisibility(cellId, View.GONE);
		}
	}

	public void addAccountInfos(Context context, ArrayList<SipProfileState> activeAccountsInfos)
	{
		int i = 0;
		for (SipProfileState accountInfo : activeAccountsInfos)
		{
			if (i < cells.length)
			{
				int icon = R.drawable.noti_offline;
				String displayName = accountInfo.getDisplayName().toString();
				if (accountInfo.isValidForCall())
				{
					icon = R.drawable.noti_online;
					displayName = displayName + "";
				}					
								
				setViewVisibility(cells[i], View.VISIBLE);
				setImageViewResource(icons[i], icon);
				setTextViewText(texts[i], displayName);
				i++;
			}
		}
	}
	
	public void setAccountMessage(Context context, int icon, String msg)
	{
		int i = 0;
		setViewVisibility(cells[i], View.VISIBLE);
		setImageViewResource(icons[i], icon);
		setTextViewText(texts[i], msg);
	}	

}
