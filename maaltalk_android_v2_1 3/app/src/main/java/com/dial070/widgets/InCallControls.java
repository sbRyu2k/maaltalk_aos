package com.dial070.widgets;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.*;
//import android.view.animation.Animation;
//import android.view.animation.AccelerateInterpolator;
//import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.MediaManager.MediaState;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.dial070.widgets.Keypad.OnDialKeyListener;

public class InCallControls extends FrameLayout implements
        android.view.View.OnClickListener, OnDialKeyListener {
    private static final String THIS_FILE = "InCallControls";
    private static final int MODE_LOCKER = 0;
    private static final int MODE_CONTROL = 1;
    private static final int MODE_NO_ACTION = 2;
    // private ViewGroup mContainerKeypad;

    // private int mViewStatus;
    // private boolean mAnimationRunning;
    OnTriggerListener onTriggerListener;
    OnDialTriggerListener onDialTriggerListener;
    private ViewGroup mContainerControls;
    private boolean isHangup = false;
    private boolean isConfirmed = false;
    private ImageButton clearCallButton;
    private CheckBox dialButton, speakerButton, muteButton, holdButton,
            recButton, contactButton, returnButton;
    private LinearLayout inCallButtons;
    // private LinearLayout keypadControl;
    // private boolean isDialpadOn = false;
    private ImageButton takeCallButton, declineCallButton;
    private LinearLayout alternateLockerWidget;
    private LinearLayout callModeControlWidget;
    private LinearLayout callModeControlKeyPadWidget;
    // private LinearLayout callModeDialPadWidget;
    private LinearLayout callControlBottomWidget;
    private LinearLayout linearReturnBtn;
    private Keypad dialPad;
    private int controlMode;

    private MediaState lastMediaState;
    private SipCallSession currentCall;

    // BJH
    private AppPrefs mPrefs;
    private String mAutoRec = null;
    private String mAutoRecToggle = null;
    private Context mContext;

    private Button btnReturnSend,btnReturnEnd,btnReturnReplace;

    public InCallControls(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) //BJH 2016.09.09
            return;

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.in_call_controls, this, true);

        // BJH 자동 녹취 일 때 녹음 토글 버튼이 체크되도록
        mContext = context; //BJH 2016.11.09
        mPrefs = new AppPrefs(context);
        mAutoRec = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_REC);
        mAutoRecToggle = mPrefs
                .getPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mContainerControls = (ViewGroup) findViewById(R.id.call_container_controls);
        // mContainerKeypad = (ViewGroup) findViewById(R.id.layout_keypad);

        // keypadControl = (LinearLayout) findViewById(R.id.layout_keypad);

        // mViewStatus = 0;
        // mAnimationRunning = false;

        // Since we are caching large views, we want to keep their cache
        // between each animation
        // mContainerControls.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);
        // mContainerKeypad.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);

        // jwkim remove
        // slidingTabWidget = (SlidingTab) findViewById(R.id.takeCallUnlocker);
        alternateLockerWidget = (LinearLayout) findViewById(R.id.takeCallUnlocker);
        callModeControlWidget = (LinearLayout) findViewById(R.id.callModeControl);
        callModeControlKeyPadWidget = (LinearLayout) findViewById(R.id.callModeControlKeyPad);
        // callModeDialPadWidget = (LinearLayout)
        // findViewById(R.id.callModeDialPad);

        // Inbound Call 전화받기 끊기 버튼 위치변경을 위해 만든 레이아웃
        callControlBottomWidget = (LinearLayout) findViewById(R.id.layout_controls_bottom);

        inCallButtons = (LinearLayout) findViewById(R.id.inCallButtons);

        clearCallButton = (ImageButton) findViewById(R.id.clearCallButton);

        linearReturnBtn = findViewById(R.id.linearReturnBtn);

        btnReturnSend=findViewById(R.id.btnReturnSend);
        btnReturnEnd=findViewById(R.id.btnReturnEnd);
        btnReturnReplace=findViewById(R.id.btnReturnReplace);

        // clearCallSmallButton = (ImageButton)
        // findViewById(R.id.clearCallSmallButton);
        // closeKeypadButton = (ImageButton)
        // findViewById(R.id.closeKeypadButton);

        dialButton = findViewById(R.id.dialpadButton);
        // memoButton = findViewById(R.id.memoButton);
        contactButton = findViewById(R.id.addContactButton);
        returnButton =  findViewById(R.id.returnButton);
        speakerButton = findViewById(R.id.speakerButton);
        muteButton = findViewById(R.id.muteButton);
        recButton = findViewById(R.id.recButton);

        takeCallButton = findViewById(R.id.takeCallButton);
        declineCallButton = findViewById(R.id.declineCallButton);
        holdButton = findViewById(R.id.holdButton);

        if (!isInEditMode()) { //BJH 2016.09.09
            mContainerControls.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);

            setEnabledMediaButtons(false);
            isConfirmed = false;
            //dialButton.setEnabled(false);
            controlMode = MODE_LOCKER;
            inCallButtons.setVisibility(INVISIBLE);
            setCallLockerVisibility(VISIBLE);

            Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
            intent.putExtra("visible", View.VISIBLE);
            mContext.sendBroadcast(intent);

            setCallModeControlVisibility(INVISIBLE);
            inCallButtons.setVisibility(INVISIBLE);
            // keypadControl.setVisibility(GONE);

            dialPad = (Keypad) findViewById(R.id.keyPad);
            dialPad.setOnDialKeyListener(this);

            clearCallButton.setOnClickListener(this);
            // //clearCallSmallButton.setOnClickListener(this);
            // //closeKeypadButton.setOnClickListener(this);
            //

            dialButton.setOnClickListener(this);
            speakerButton.setOnClickListener(this);
            muteButton.setOnClickListener(this);
            takeCallButton.setOnClickListener(this);
            declineCallButton.setOnClickListener(this);
            holdButton.setOnClickListener(this);
            // memoButton.setOnClickListener(this);
            contactButton.setOnClickListener(this);
            returnButton.setOnClickListener(this);

            btnReturnSend.setOnClickListener(this);
            btnReturnEnd.setOnClickListener(this);
            btnReturnReplace.setOnClickListener(this);

            recButton.setOnClickListener(this);
            // BJH 녹음 버튼 컨트롤
            if (mAutoRec != null && mAutoRec.equals("Y")) {// NullPointerException
                // 해결
                recButton.setChecked(true);
                recButton.setEnabled(false);
            } else {// 녹취 버튼을 누르고 말톡을 실행하거나 StartForeground 노티바를 클릭할 경우 녹취
                // Toggle 이 해제되는 부분
                if (mAutoRecToggle != null && mAutoRecToggle.equals("Y"))
                    recButton.setChecked(true);
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        /*
         * final int parentWidth = r - l; final int parentHeight = b - t; final
         * int top = parentHeight * 3/4 - slidingTabWidget.getHeight()/2; final
         * int bottom = parentHeight * 3/4 + slidingTabWidget.getHeight() / 2;
         * slidingTabWidget.layout(0, top, parentWidth, bottom);
         */

    }

    public void setEnabledMediaButtons(boolean isInCall) {

        if (lastMediaState == null) {
            speakerButton.setEnabled(isInCall);
            muteButton.setEnabled(isInCall);

        } else {
            speakerButton.setEnabled(lastMediaState.canSpeakerphoneOn
                    && isInCall);
            muteButton.setEnabled(lastMediaState.canMicrophoneMute && isInCall);
        }
        // dialButton.setEnabled(isInCall);
    }

    private void setCallLockerVisibility(int visibility) {
        /*
         * if (useSlider) { slidingTabWidget.setVisibility(visibility); } else {
         *
         * }
         */
        alternateLockerWidget.setVisibility(visibility);
    }

    // private void animationViewFlip(ViewGroup fromView, ViewGroup toView, int
    // duration) {
    // // Find the center of the container
    // final float centerX = fromView.getWidth() / 2.0f;
    // final float centerY = fromView.getHeight() / 2.0f;
    //
    // // Create a new 3D rotation with the supplied parameter
    // // The animation listener is used to trigger the next animation
    // final Rotate3dAnimation rotation =
    // new Rotate3dAnimation(0, 90, centerX, centerY, 310.0f, true);
    // rotation.setDuration(duration/2);
    // rotation.setFillAfter(true);
    // rotation.setInterpolator(new AccelerateInterpolator());
    // rotation.setAnimationListener(new DisplayNextView(fromView, toView,
    // duration/2));
    //
    // fromView.setVisibility(View.VISIBLE);
    // toView.setVisibility(View.GONE);
    //
    // fromView.startAnimation(rotation);
    // }

    private void setCallModeControlVisibility(int visibility) {
        callModeControlWidget.setVisibility(visibility);
        callModeControlKeyPadWidget.setVisibility(visibility);
    }

    /**
     * Toggle the mute button as if pressed by the user.
     */
    public void toggleMuteButton() {
        muteButton.setChecked(!muteButton.isChecked());
        muteButton.performClick();
    }

    // jwkim add function
    public SipCallSession getCurrentCall() {
        return currentCall;
    }

    public void setHangup() {
        isHangup = true;
        setCallState(currentCall);
    }

    public void setCallState(SipCallSession callInfo) {
        currentCall = callInfo;
        if (currentCall == null || isHangup) {
            controlMode = MODE_NO_ACTION;
            inCallButtons.setVisibility(INVISIBLE);
            setCallLockerVisibility(INVISIBLE);

            Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
            intent.putExtra("visible", View.INVISIBLE);
            mContext.sendBroadcast(intent);

            setCallModeControlVisibility(INVISIBLE);
            return;
        }

        Log.d(THIS_FILE,"setCallState CallId:"+currentCall.getCallId());

        int state = currentCall.getCallState();
        Log.d(THIS_FILE,"setCallState state:"+state);
        switch (state) {
            case SipCallSession.InvState.INCOMING:
                Log.d(THIS_FILE,"setCallState INCOMING:"+currentCall.getCallId());
                controlMode = MODE_LOCKER;
                inCallButtons.setVisibility(INVISIBLE);
                setCallLockerVisibility(VISIBLE);

                Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent.putExtra("visible", View.VISIBLE);
                mContext.sendBroadcast(intent);

                callControlBottomWidget.setVisibility(GONE);
                setCallModeControlVisibility(INVISIBLE);
                inCallButtons.setVisibility(INVISIBLE);
                break;
            case SipCallSession.InvState.CALLING:
                Log.d(THIS_FILE,"setCallState CALLING:"+currentCall.getCallId());

                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);
            case SipCallSession.InvState.CONNECTING:
                Log.d(THIS_FILE,"setCallState CONNECTING:"+currentCall.getCallId());
                controlMode = MODE_CONTROL;
                setCallLockerVisibility(GONE); // INVISIBLE);

                Intent intent2 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent2.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent2);

                inCallButtons.setVisibility(VISIBLE);
                callControlBottomWidget.setVisibility(VISIBLE);
                setCallModeControlVisibility(VISIBLE);
                clearCallButton.setEnabled(true);
                setEnabledMediaButtons(true);

                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);
                break;
            case SipCallSession.InvState.CONFIRMED:
                Log.d(THIS_FILE,"setCallState CONFIRMED:"+currentCall.getCallId());
                controlMode = MODE_CONTROL;
                setCallLockerVisibility(GONE);

                Intent intent3 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent3.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent3);

                setCallModeControlVisibility(VISIBLE);
                inCallButtons.setVisibility(VISIBLE);
                callControlBottomWidget.setVisibility(VISIBLE);
                clearCallButton.setEnabled(true);
                setEnabledMediaButtons(true);
                dialButton.setEnabled(true);
                isConfirmed = true;

                dialButton.getBackground().setAlpha(255);
                dialButton.setEnabled(true);

                muteButton.getBackground().setAlpha(255);
                muteButton.setEnabled(true);

                returnButton.getBackground().setAlpha(255);
                returnButton.setEnabled(true);

                holdButton.getBackground().setAlpha(255);
                holdButton.setEnabled(true);

                recButton.getBackground().setAlpha(255);
                recButton.setEnabled(true);
                break;
            case SipCallSession.InvState.NULL:
            case SipCallSession.InvState.DISCONNECTED:
                controlMode = MODE_NO_ACTION;
                inCallButtons.setVisibility(INVISIBLE);
                setCallLockerVisibility(GONE); // INVISIBLE);

                Intent intent4 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent4.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent4);

                setCallModeControlVisibility(INVISIBLE);
                break;
            case SipCallSession.InvState.EARLY:
                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);
            default:
                if (currentCall.isIncoming()) {
                    controlMode = MODE_LOCKER;
                    inCallButtons.setVisibility(INVISIBLE);
                    setCallLockerVisibility(VISIBLE);

                    Intent intent5 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                    intent5.putExtra("visible", View.VISIBLE);
                    mContext.sendBroadcast(intent5);

                    callControlBottomWidget.setVisibility(GONE);
                    inCallButtons.setVisibility(INVISIBLE);
                    setCallModeControlVisibility(INVISIBLE);
                } else {
                    controlMode = MODE_CONTROL;
                    setCallLockerVisibility(GONE); // INVISIBLE);

                    Intent intent6 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                    intent6.putExtra("visible", View.GONE);
                    mContext.sendBroadcast(intent6);

                    inCallButtons.setVisibility(VISIBLE);
                    callControlBottomWidget.setVisibility(VISIBLE);
                    setCallModeControlVisibility(VISIBLE);
                    clearCallButton.setEnabled(true);
                    setEnabledMediaButtons(true);
                }
                break;
        }

        int mediaStatus = callInfo.getMediaStatus();
        switch (mediaStatus) {
            case SipCallSession.MediaState.ACTIVE:
            case SipCallSession.MediaState.REMOTE_HOLD:
                holdButton.setChecked(false);
                break;
            case SipCallSession.MediaState.LOCAL_HOLD:
                // case SipCallSession.MediaState.NONE:
                holdButton.setChecked(true);
                break;
            case SipCallSession.MediaState.ERROR:
            default:
                break;
        }
    }

    /**
     * Registers a callback to be invoked when the user triggers an event.
     *
     * @param listener the OnTriggerListener to attach to this view
     */

    public void setOnTriggerListener(OnTriggerListener listener) {
        onTriggerListener = listener;
    }

    private void dispatchTriggerEvent(int whichHandle) {
        if (onTriggerListener != null) {
            onTriggerListener.onTrigger(whichHandle, currentCall);
        }
    }

    public void setOnDialTriggerListener(OnDialTriggerListener listener) {
        onDialTriggerListener = listener;
    }

    private void dispatchDialTriggerEvent(int keyCode, int dialTone) {
        if (onDialTriggerListener != null) {
            onDialTriggerListener.onTrigger(keyCode, dialTone);
        }
    }

    @Override
    public void onTrigger(int keyCode, int dialTone) {
        dispatchDialTriggerEvent(keyCode, dialTone);

        Log.d(THIS_FILE, "keyCode:" + keyCode);

    }

    /**
     * This class is responsible for swapping the views and start the second
     * half of the animation.
     */
    // private final class SwapViews implements Runnable {
    // private ViewGroup mFromView;
    // private ViewGroup mToView;
    // private int mDuration;
    //
    // public SwapViews(ViewGroup fromView, ViewGroup toView, int duration) {
    // mFromView = fromView;
    // mToView = toView;
    // mDuration = duration;
    // }
    //
    // public void run() {
    // final float centerX = mToView.getWidth() / 2.0f;
    // final float centerY = mToView.getHeight() / 2.0f;
    // Rotate3dAnimation rotation;
    //
    // // -90도로 대기중인 VIEW를 0도로 원상복구.
    // rotation = new Rotate3dAnimation(-90, 0, centerX, centerY, 310.0f,
    // false);
    // rotation.setDuration(mDuration);
    // rotation.setFillAfter(true);
    // rotation.setInterpolator(new DecelerateInterpolator());
    //
    // mFromView.setVisibility(View.GONE);
    // mToView.setVisibility(View.VISIBLE);
    // mToView.startAnimation(rotation);
    //
    // // mAnimationRunning = false;
    // }
    // }
    //
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.clearCallButton:
                Log.i(THIS_FILE,"clearCallButton clicked");
                // case R.id.clearCallSmallButton:
                dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                break;
            // case R.id.closeKeypadButton:
            // if (!mAnimationRunning)
            // {
            // mAnimationRunning = true;
            // callModeDialPadWidget.setVisibility(GONE);
            // callModeControlWidget.setVisibility(VISIBLE);
            //
            //
            // animationViewFlip(mContainerKeypad,mContainerControls, 400);
            // mContainerControls.bringToFront();
            // //dispatchTriggerEvent(OnTriggerListener.DIALPAD_ON);
            // }
            // break;
            case R.id.dialpadButton:
                // dispatchTriggerEvent(isDialpadOn ? OnTriggerListener.DIALPAD_OFF
                // : OnTriggerListener.DIALPAD_ON);
                // isDialpadOn = !isDialpadOn;

                boolean isReturn= mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED);
                if (isConfirmed && !isReturn) {
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                    returnButton.setBackgroundResource(R.drawable.btn_return_normal);

                    if (((CheckBox) v).isChecked()) {
                        // if (!mAnimationRunning)
                        // {
                        // mAnimationRunning = true;
                        // callModeControlWidget.setVisibility(VISIBLE); //GONE);
                        // //animationViewFlip(mContainerControls, mContainerKeypad,
                        // 400);
                        // //mContainerKeypad.bringToFront();
                        //
                        // keypadControl.setVisibility(VISIBLE);
                        // keypadControl.bringToFront();
                        // }
                        // keypadControl.setVisibility(VISIBLE);
                        // keypadControl.bringToFront();
                        dialButton.setBackgroundResource(R.drawable.btn_hide_keypad);
                        dialPad.setVisibility(VISIBLE);
                        dialPad.bringToFront();
                        dialPad.setMode("KEYPAD");
                        linearReturnBtn.setVisibility(View.GONE);
                    } else {
                        // if (!mAnimationRunning)
                        // {
                        // mAnimationRunning = true;
                        // callModeControlWidget.setVisibility(VISIBLE);
                        //
                        // keypadControl.setVisibility(GONE);
                        // mContainerControls.bringToFront();
                        //
                        // //animationViewFlip(mContainerKeypad,mContainerControls,
                        // 400);
                        // //mContainerControls.bringToFront();
                        // }
                        // keypadControl.setVisibility(GONE);
                        // mContainerControls.bringToFront();
                        dialButton.setBackgroundResource(R.drawable.btn_call_keypad);
                        dialPad.setVisibility(GONE);
                        linearReturnBtn.setVisibility(View.GONE);
                    }
                } else {
                    dialButton.setChecked(false);
                }


                break;
            // case R.id.bluetoothButton:
            // if (((ToggleButton) v).isChecked()) {
            // dispatchTriggerEvent(OnTriggerListener.BLUETOOTH_ON);
            // } else {
            // dispatchTriggerEvent(OnTriggerListener.BLUETOOTH_OFF);
            // }
            // break;
            case R.id.speakerButton:
                if (((CheckBox) v).isChecked()) {
                    dispatchTriggerEvent(OnTriggerListener.SPEAKER_ON);
                } else {
                    dispatchTriggerEvent(OnTriggerListener.SPEAKER_OFF);
                }
                break;
            case R.id.muteButton:
                if (((CheckBox) v).isChecked()) {
                    dispatchTriggerEvent(OnTriggerListener.MUTE_ON);
                } else {
                    dispatchTriggerEvent(OnTriggerListener.MUTE_OFF);
                }
                break;
            case R.id.takeCallButton:
                Log.i(THIS_FILE,"takeCallButton clicked");
                dispatchTriggerEvent(OnTriggerListener.TAKE_CALL);
                setCallLockerVisibility(GONE);

                Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent.putExtra("visible", View.INVISIBLE);
                mContext.sendBroadcast(intent);
                break;
            case R.id.declineCallButton:
                dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
                setCallLockerVisibility(GONE);

                Intent intent2 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent2.putExtra("visible", View.INVISIBLE);
                mContext.sendBroadcast(intent2);
                break;
            // case R.id.detailsButton:
            // dispatchTriggerEvent(OnTriggerListener.DETAILED_DISPLAY);
            // break;
            case R.id.holdButton:
                boolean isReturn2= mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED);
                if (!isReturn2){
                    dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                } else {
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                    holdButton.setChecked(false);
                }

                break;
            // case R.id.settingsButton:
            // dispatchTriggerEvent(OnTriggerListener.MEDIA_SETTINGS);
            case R.id.addContactButton:
                dispatchTriggerEvent(OnTriggerListener.CONTACTS);
                break;
            case R.id.recButton:
                // BJH
                String toggle = mPrefs
                        .getPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE);
                if (toggle != null && toggle.equals("Y"))
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE, "");
                else
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE, "Y");
                dispatchTriggerEvent(OnTriggerListener.REC);
                break;
            case R.id.returnButton:
                if (isConfirmed && !dialButton.isChecked()) {
                    dialButton.setBackgroundResource(R.drawable.btn_call_keypad);
                    if (((CheckBox) v).isChecked()) {
                        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,true);
                        returnButton.setBackgroundResource(R.drawable.btn_return_pressed);
                        dialPad.setVisibility(VISIBLE);
                        dialPad.bringToFront();
                        dialPad.setMode("RETURN");
                        linearReturnBtn.setVisibility(View.VISIBLE);
                        if (!holdButton.isChecked()){
                            holdButton.setChecked(true);
                            dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                        }
                        dispatchTriggerEvent(OnTriggerListener.RETURN_ON);
                    } else {
                        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                        returnButton.setBackgroundResource(R.drawable.btn_return_normal);
                        dialPad.setVisibility(GONE);
                        linearReturnBtn.setVisibility(View.GONE);

                        holdButton.setChecked(false);
                        dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                        dispatchTriggerEvent(OnTriggerListener.RETURN_OFF);
                    }
                } else {
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                    returnButton.setChecked(false);
                }
                break;
            case R.id.btnReturnSend:
                Log.i(THIS_FILE,"btnReturnSend clicked");
                if (getReturnCallNumber().trim().length()>3){
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ACTION_YN,true);
                    dispatchTriggerEvent(OnTriggerListener.RETURN_SEND);
                    btnReturnSend.setVisibility(View.GONE);
                    btnReturnEnd.setVisibility(View.VISIBLE);
                    btnReturnReplace.setVisibility(View.GONE);
                }
                break;
            case R.id.btnReturnEnd:
                Log.i(THIS_FILE,"btnReturnEnd clicked");
                dispatchTriggerEvent(OnTriggerListener.RETURN_CANCEL);
                btnReturnSend.setVisibility(View.VISIBLE);
                btnReturnEnd.setVisibility(View.GONE);
                btnReturnReplace.setVisibility(View.GONE);
                break;
            case R.id.btnReturnReplace:
                Log.i(THIS_FILE,"btnReturnReplace clicked");
                dispatchTriggerEvent(OnTriggerListener.RETURN_REPLACE);
                btnReturnReplace.setEnabled(false);
                dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Log.d(THIS_FILE, "Hey you hit the key : " + keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_CALL:
                if (controlMode == MODE_LOCKER) {
                    dispatchTriggerEvent(OnTriggerListener.TAKE_CALL);
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_ENDCALL:
                if (controlMode == MODE_LOCKER) {
                    // dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
                    dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                    return true;
                } else if (controlMode == MODE_CONTROL) {
                    dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                    return true;
                }
            default:
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    /**
     * This class listens for the end of the first half of the animation. It
     * then posts a new action that effectively swaps the views when the
     * container is rotated 90 degrees and thus invisible.
     */
    // private final class DisplayNextView implements
    // Animation.AnimationListener {
    // private ViewGroup mFromView;
    // private ViewGroup mToView;
    // private int mDuration;
    //
    // private DisplayNextView(ViewGroup fromView, ViewGroup toView, int
    // duration) {
    // mFromView = fromView;
    // mToView = toView;
    // mDuration = duration;
    // }
    //
    // public void onAnimationStart(Animation animation) {
    // }
    //
    // public void onAnimationEnd(Animation animation) {
    // mToView.post(new SwapViews(mFromView,mToView,mDuration));
    // }
    //
    // public void onAnimationRepeat(Animation animation) {
    // }
    // }
    public void setMediaState(MediaState mediaState) {
        lastMediaState = mediaState;
        muteButton.setEnabled(mediaState.canMicrophoneMute);
        muteButton.setChecked(mediaState.isMicrophoneMute);
        speakerButton.setEnabled(mediaState.canSpeakerphoneOn);
        speakerButton.setChecked(mediaState.isSpeakerphoneOn);
    }

    public void declineCall() { //거절메시지 보내기용
        dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
        setCallLockerVisibility(GONE);
    }

    public void setNumber(String number){
        dialPad.setNumber(number);
    }

    public interface OnDialTriggerListener {
        void onTrigger(int keyCode, int dialTone);
    }

    public interface OnTriggerListener {

        int CLEAR_CALL = 1;

        int TAKE_CALL = CLEAR_CALL + 1;

        int DECLINE_CALL = TAKE_CALL + 1;

        int DIALPAD_ON = DECLINE_CALL + 1;

        int DIALPAD_OFF = DIALPAD_ON + 1;

        int MUTE_ON = DIALPAD_OFF + 1;

        int MUTE_OFF = MUTE_ON + 1;

        int BLUETOOTH_ON = MUTE_OFF + 1;

        int BLUETOOTH_OFF = BLUETOOTH_ON + 1;

        int SPEAKER_ON = BLUETOOTH_OFF + 1;

        int SPEAKER_OFF = SPEAKER_ON + 1;

        int DETAILED_DISPLAY = SPEAKER_OFF + 1;

        int TOGGLE_HOLD = DETAILED_DISPLAY + 1;

        int MEDIA_SETTINGS = TOGGLE_HOLD + 1;

        // int MEMO = MEDIA_SETTINGS +1;
        int REC = MEDIA_SETTINGS + 1;

        int CONTACTS = REC + 1;

        int STAT = CONTACTS + 1;

        int RETURN_ON = STAT + 1;

        int RETURN_OFF = RETURN_ON + 1;

        int RETURN_SEND = RETURN_OFF + 1;

        int RETURN_CANCEL = RETURN_SEND + 1;

        int RETURN_REPLACE = RETURN_CANCEL + 1;

        void onTrigger(int whichAction, SipCallSession call);
    }

    public String getReturnCallNumber(){
        return dialPad.getReturnCallNumber();
    }

    public void setBtnReturnReplaceVisible(){
        if (mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
            btnReturnReplace.setVisibility(View.VISIBLE);
        }
    }

    public void setBtnReturnEnd(){
        if (mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
            btnReturnSend.setVisibility(View.VISIBLE);
            btnReturnEnd.setVisibility(View.GONE);
            btnReturnReplace.setVisibility(View.GONE);
        }
    }
}
