package com.dial070.widgets;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dial070.App;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.MediaManager.MediaState;
import com.dial070.ui.Calls;
import com.dial070.ui.CallsEdit;
import com.dial070.ui.Contacts;
import com.dial070.ui.SmsWrite;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.widgets.Keypad.OnDialKeyListener;

import java.util.ArrayList;
import java.util.List;

//import android.view.animation.Animation;
//import android.view.animation.AccelerateInterpolator;
//import android.view.animation.DecelerateInterpolator;

public class InCallControls2 extends RelativeLayout implements
        View.OnClickListener, OnDialKeyListener {
    private static final String THIS_FILE = "InCallControls2";
    private static final int MODE_LOCKER = 0;
    private static final int MODE_CONTROL = 1;
    private static final int MODE_NO_ACTION = 2;

    OnTriggerListener onTriggerListener;
    OnDialTriggerListener onDialTriggerListener;
    private ViewGroup mContainerControls;
    private boolean isHangup = false;
    private boolean isConfirmed = false;
    private ImageButton clearCallButton;
    private CheckBox dialButton, speakerButton, muteButton, holdButton,
            recButton, returnButton;
    private LinearLayout inCallButtons;

    private ImageButton takeCallButton, declineCallButton, imgBtnReturn, imgBtnKeypadClose;
    private LinearLayout alternateLockerWidget;
    private LinearLayout callModeControlWidget;
    private LinearLayout callModeControlKeyPadWidget;
    private LinearLayout callControlBottomWidget;
    private RelativeLayout linearReturnBtn;
    private Keypad dialPad;
    private int controlMode;

    private MediaState lastMediaState;
    private SipCallSession currentCall;

    // BJH
    private AppPrefs mPrefs;
    private String mAutoRec = null;
    private String mAutoRecToggle = null;
    private Context mContext;

    private ImageButton btnReturnSend,btnReturnEnd,btnReturnReplace;

    public static final int MENU_OPTION_DIALPAD = 1;
    public static final int MENU_OPTION_CONTACT = MENU_OPTION_DIALPAD+ 1;

    private Activity mActivity;

    public InCallControls2(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) //BJH 2016.09.09
            return;

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.in_call_controls_v2, this, true);

        // BJH 자동 녹취 일 때 녹음 토글 버튼이 체크되도록
        mContext = context; //BJH 2016.11.09
        mActivity = (Activity) context;
        mPrefs = new AppPrefs(context);
        mAutoRec = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_REC);
        mAutoRecToggle = mPrefs
                .getPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mContainerControls = (ViewGroup) findViewById(R.id.call_container_controls);

        // jwkim remove
        alternateLockerWidget = (LinearLayout) findViewById(R.id.takeCallUnlocker);
        callModeControlWidget = (LinearLayout) findViewById(R.id.callModeControl);
        callModeControlKeyPadWidget = (LinearLayout) findViewById(R.id.callModeControlKeyPad);

        // Inbound Call 전화받기 끊기 버튼 위치변경을 위해 만든 레이아웃
        callControlBottomWidget = (LinearLayout) findViewById(R.id.layout_controls_bottom);

        inCallButtons = (LinearLayout) findViewById(R.id.inCallButtons);

        clearCallButton = (ImageButton) findViewById(R.id.clearCallButton);
        imgBtnReturn=findViewById(R.id.imgBtnReturn);
        imgBtnKeypadClose=findViewById(R.id.imgBtnKeypadClose);

        linearReturnBtn = findViewById(R.id.linearReturnBtn);

        btnReturnSend=findViewById(R.id.btnReturnSend);
        btnReturnEnd=findViewById(R.id.btnReturnEnd);
        btnReturnReplace=findViewById(R.id.btnReturnReplace);

        dialButton = findViewById(R.id.dialpadButton);
        returnButton =  findViewById(R.id.returnButton);
        speakerButton = findViewById(R.id.speakerButton);
        muteButton = findViewById(R.id.muteButton);
        recButton = findViewById(R.id.recButton);

        takeCallButton = findViewById(R.id.takeCallButton);
        declineCallButton = findViewById(R.id.declineCallButton);
        holdButton = findViewById(R.id.holdButton);

        if (App.isReturnClicked){
            returnButton.setVisibility(View.INVISIBLE);
            btnReturnReplace.setVisibility(View.VISIBLE);
            btnReturnReplace.getBackground().setAlpha(100);
            btnReturnReplace.setEnabled(false);
        }else {
            returnButton.setVisibility(View.VISIBLE);
            btnReturnReplace.setVisibility(View.GONE);
        }

        if (!isInEditMode()) { //BJH 2016.09.09
            mContainerControls.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);

            setEnabledMediaButtons(false);
            isConfirmed = false;
            //dialButton.setEnabled(false);
            controlMode = MODE_LOCKER;
            setCallLockerVisibility(VISIBLE);

            Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
            intent.putExtra("visible", View.VISIBLE);
            mContext.sendBroadcast(intent);

            setCallModeControlVisibility(INVISIBLE);
            inCallButtons.setVisibility(INVISIBLE);
            // keypadControl.setVisibility(GONE);

            dialPad = (Keypad) findViewById(R.id.keyPad);
            dialPad.setOnDialKeyListener(this);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) dialPad.getLayoutParams();
            DisplayMetrics dm = mContext.getApplicationContext().getResources().getDisplayMetrics();
            int height = dm.heightPixels;
            LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(params.width,height/2);
            dialPad.setLayoutParams(parms);

            clearCallButton.setOnClickListener(this);

            dialButton.setOnClickListener(this);
            speakerButton.setOnClickListener(this);
            muteButton.setOnClickListener(this);
            takeCallButton.setOnClickListener(this);
            declineCallButton.setOnClickListener(this);
            holdButton.setOnClickListener(this);
            returnButton.setOnClickListener(this);

            btnReturnSend.setOnClickListener(this);
            btnReturnEnd.setOnClickListener(this);
            btnReturnReplace.setOnClickListener(this);

            imgBtnKeypadClose.setOnClickListener(this);

            recButton.setOnClickListener(this);
            // BJH 녹음 버튼 컨트롤
            if (mAutoRec != null && mAutoRec.equals("Y")) {// NullPointerException
                // 해결
                recButton.setChecked(true);
                recButton.setEnabled(false);
            } else {// 녹취 버튼을 누르고 말톡을 실행하거나 StartForeground 노티바를 클릭할 경우 녹취
                // Toggle 이 해제되는 부분
                if (mAutoRecToggle != null && mAutoRecToggle.equals("Y"))
                    recButton.setChecked(true);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        /*
         * final int parentWidth = r - l; final int parentHeight = b - t; final
         * int top = parentHeight * 3/4 - slidingTabWidget.getHeight()/2; final
         * int bottom = parentHeight * 3/4 + slidingTabWidget.getHeight() / 2;
         * slidingTabWidget.layout(0, top, parentWidth, bottom);
         */

    }

    public void setEnabledMediaButtons(boolean isInCall) {

        if (lastMediaState == null) {
            speakerButton.setEnabled(isInCall);
            muteButton.setEnabled(isInCall);

        } else {
            speakerButton.setEnabled(lastMediaState.canSpeakerphoneOn
                    && isInCall);
            muteButton.setEnabled(lastMediaState.canMicrophoneMute && isInCall);
        }
        // dialButton.setEnabled(isInCall);
    }

    private void setCallLockerVisibility(int visibility) {
        /*
         * if (useSlider) { slidingTabWidget.setVisibility(visibility); } else {
         *
         * }
         */
        alternateLockerWidget.setVisibility(visibility);
        /*if (visibility==VISIBLE){
            dispatchTriggerEvent(OnTriggerListener.INCOMING_CONTROL_ON);
        }else {
            dispatchTriggerEvent(OnTriggerListener.INCOMING_CONTROL_OFF);
        }*/

    }

    // private void animationViewFlip(ViewGroup fromView, ViewGroup toView, int
    // duration) {
    // // Find the center of the container
    // final float centerX = fromView.getWidth() / 2.0f;
    // final float centerY = fromView.getHeight() / 2.0f;
    //
    // // Create a new 3D rotation with the supplied parameter
    // // The animation listener is used to trigger the next animation
    // final Rotate3dAnimation rotation =
    // new Rotate3dAnimation(0, 90, centerX, centerY, 310.0f, true);
    // rotation.setDuration(duration/2);
    // rotation.setFillAfter(true);
    // rotation.setInterpolator(new AccelerateInterpolator());
    // rotation.setAnimationListener(new DisplayNextView(fromView, toView,
    // duration/2));
    //
    // fromView.setVisibility(View.VISIBLE);
    // toView.setVisibility(View.GONE);
    //
    // fromView.startAnimation(rotation);
    // }

    private void setCallModeControlVisibility(int visibility) {
        callModeControlWidget.setVisibility(visibility);
        //callModeControlKeyPadWidget.setVisibility(visibility);
    }

    /**
     * Toggle the mute button as if pressed by the user.
     */
    public void toggleMuteButton() {
        muteButton.setChecked(!muteButton.isChecked());
        muteButton.performClick();
    }

    // jwkim add function
    public SipCallSession getCurrentCall() {
        return currentCall;
    }

    public void setHangup() {
        isHangup = true;
        setCallState(currentCall);
    }

    public void setCallState(SipCallSession callInfo) {
        currentCall = callInfo;
        if (currentCall == null || isHangup) {
//            Toast.makeText(mContext, "Exit step 1", Toast.LENGTH_SHORT).show();
            controlMode = MODE_NO_ACTION;
            inCallButtons.setVisibility(View.INVISIBLE);
            setCallLockerVisibility(INVISIBLE);

            /*Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
            intent.putExtra("visible", View.INVISIBLE);
            mContext.sendBroadcast(intent);*/

            setCallModeControlVisibility(INVISIBLE);
            return;
        }

        Log.d(THIS_FILE,"setCallState CallId:"+currentCall.getCallId());
//        DebugLog.d("setCallState called callInfo --> "+currentCall.getCallId());

        int state = currentCall.getCallState();
        Log.d(THIS_FILE,"setCallState state:"+state);
        switch (state) {
            case SipCallSession.InvState.INCOMING:
//                Toast.makeText(mContext, "Exit step incoming", Toast.LENGTH_SHORT).show();
                Log.d(THIS_FILE,"setCallState INCOMING:"+currentCall.getCallId());
                controlMode = MODE_LOCKER;
                inCallButtons.setVisibility(INVISIBLE);
                setCallModeControlVisibility(INVISIBLE);
                setCallLockerVisibility(VISIBLE);

                /*Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent.putExtra("visible", View.VISIBLE);
                mContext.sendBroadcast(intent);*/

                //callControlBottomWidget.setVisibility(GONE);

                break;
            case SipCallSession.InvState.CALLING:
//                Toast.makeText(mContext, "Exit step calling", Toast.LENGTH_SHORT).show();
                Log.d(THIS_FILE,"setCallState CALLING:"+currentCall.getCallId());

                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);
            case SipCallSession.InvState.CONNECTING:
//                Toast.makeText(mContext, "Exit step connecting", Toast.LENGTH_SHORT).show();
                Log.d(THIS_FILE,"setCallState CONNECTING:"+currentCall.getCallId());
                controlMode = MODE_CONTROL;
//                setCallLockerVisibility(INVISIBLE); // INVISIBLE);
                setCallLockerVisibility(GONE);

                /*Intent intent2 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent2.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent2);*/

                inCallButtons.setVisibility(VISIBLE);
                setCallModeControlVisibility(VISIBLE);

                //callControlBottomWidget.setVisibility(VISIBLE);

                clearCallButton.setEnabled(true);

                setEnabledMediaButtons(true);

                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);
                break;
            case SipCallSession.InvState.CONFIRMED:
//                Toast.makeText(mContext, "Exit step confirmed", Toast.LENGTH_SHORT).show();
                Log.d(THIS_FILE,"setCallState CONFIRMED:"+currentCall.getCallId());
                controlMode = MODE_CONTROL;
                setCallLockerVisibility(GONE);

                /*Intent intent3 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent3.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent3);*/

                setCallModeControlVisibility(VISIBLE);
                if (callModeControlKeyPadWidget.getVisibility()!=VISIBLE){
                    Log.d(THIS_FILE,"callModeControlKeyPadWidget.getVisibility()!=VISIBLE");
                    inCallButtons.setVisibility(VISIBLE);
                }else {
                    Log.d(THIS_FILE,"callModeControlKeyPadWidget.getVisibility()==VISIBLE");
                }

                //callControlBottomWidget.setVisibility(VISIBLE);
                clearCallButton.setEnabled(true);
                setEnabledMediaButtons(true);
                dialButton.setEnabled(true);
                isConfirmed = true;

                dialButton.getBackground().setAlpha(255);
                dialButton.setEnabled(true);

                muteButton.getBackground().setAlpha(255);
                muteButton.setEnabled(true);

                returnButton.getBackground().setAlpha(255);
                returnButton.setEnabled(true);

                holdButton.getBackground().setAlpha(255);
                holdButton.setEnabled(true);

                recButton.getBackground().setAlpha(255);
                recButton.setEnabled(true);

                if (mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
                    btnReturnReplace.getBackground().setAlpha(255);
                    btnReturnReplace.setEnabled(true);
                }


                break;
            case SipCallSession.InvState.NULL:
            case SipCallSession.InvState.DISCONNECTED:
//                Toast.makeText(mContext, "Exit step null & disconnected", Toast.LENGTH_SHORT).show();
                Log.d(THIS_FILE,"setCallState DISCONNECTED");
                controlMode = MODE_NO_ACTION;
                inCallButtons.setVisibility(INVISIBLE);
                setCallLockerVisibility(GONE); // INVISIBLE);

                /*Intent intent4 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent4.putExtra("visible", View.GONE);
                mContext.sendBroadcast(intent4);*/

                setCallModeControlVisibility(GONE);
                break;
            case SipCallSession.InvState.EARLY:
//                Toast.makeText(mContext, "Exit step early", Toast.LENGTH_SHORT).show();
                dialButton.getBackground().setAlpha(100);
                dialButton.setEnabled(false);

                muteButton.getBackground().setAlpha(100);
                muteButton.setEnabled(false);

                returnButton.getBackground().setAlpha(100);
                returnButton.setEnabled(false);

                holdButton.getBackground().setAlpha(100);
                holdButton.setEnabled(false);

                recButton.getBackground().setAlpha(100);
                recButton.setEnabled(false);

                controlMode = MODE_LOCKER;
                inCallButtons.setVisibility(INVISIBLE);
                setCallModeControlVisibility(INVISIBLE);
                setCallLockerVisibility(VISIBLE);

            default:
                if (currentCall.isIncoming()) {
//                    Toast.makeText(mContext, "Exit step default incoming", Toast.LENGTH_SHORT).show();
                    Log.d(THIS_FILE,"setCallState default currentCall.isIncoming()");
                    controlMode = MODE_LOCKER;
                    /*inCallButtons.setVisibility(INVISIBLE);
                    setCallLockerVisibility(VISIBLE);
                    callModeControlKeyPadWidget.setVisibility(GONE);

                    *//*Intent intent5 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                    intent5.putExtra("visible", View.VISIBLE);
                    mContext.sendBroadcast(intent5);*//*

                    callControlBottomWidget.setVisibility(GONE);
                    inCallButtons.setVisibility(INVISIBLE);
                    setCallModeControlVisibility(GONE);*/
                } else {
//                    Toast.makeText(mContext, "Exit step default is not incoming", Toast.LENGTH_SHORT).show();
                    Log.d(THIS_FILE,"setCallState default currentCall.isIncoming() is not");
                    controlMode = MODE_CONTROL;
                    setCallLockerVisibility(GONE); // INVISIBLE);

                    /*Intent intent6 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                    intent6.putExtra("visible", View.GONE);
                    mContext.sendBroadcast(intent6);*/

                    inCallButtons.setVisibility(VISIBLE);
                    //callControlBottomWidget.setVisibility(VISIBLE);
                    setCallModeControlVisibility(VISIBLE);
                    clearCallButton.setEnabled(true);
                    setEnabledMediaButtons(true);
                }
                break;
        }

        int mediaStatus = callInfo.getMediaStatus();
        switch (mediaStatus) {
            case SipCallSession.MediaState.ACTIVE:
            case SipCallSession.MediaState.REMOTE_HOLD:
                holdButton.setChecked(false);
                break;
            case SipCallSession.MediaState.LOCAL_HOLD:
                // case SipCallSession.MediaState.NONE:
                holdButton.setChecked(true);
                break;
            case SipCallSession.MediaState.ERROR:
            default:
                break;
        }
    }

    /**
     * Registers a callback to be invoked when the user triggers an event.
     *
     * @param listener the OnTriggerListener to attach to this view
     */

    public void setOnTriggerListener(OnTriggerListener listener) {
        onTriggerListener = listener;
    }

    private void dispatchTriggerEvent(int whichHandle) {
        if (onTriggerListener != null) {
            onTriggerListener.onTrigger(whichHandle, currentCall);
        }
    }

    public void setOnDialTriggerListener(OnDialTriggerListener listener) {
        onDialTriggerListener = listener;
    }

    private void dispatchDialTriggerEvent(int keyCode, int dialTone) {
        if (onDialTriggerListener != null) {
            onDialTriggerListener.onTrigger(keyCode, dialTone);
        }
    }

    @Override
    public void onTrigger(int keyCode, int dialTone) {
        dispatchDialTriggerEvent(keyCode, dialTone);

        Log.d(THIS_FILE, "keyCode:" + keyCode);

    }

    /**
     * This class is responsible for swapping the views and start the second
     * half of the animation.
     */
    // private final class SwapViews implements Runnable {
    // private ViewGroup mFromView;
    // private ViewGroup mToView;
    // private int mDuration;
    //
    // public SwapViews(ViewGroup fromView, ViewGroup toView, int duration) {
    // mFromView = fromView;
    // mToView = toView;
    // mDuration = duration;
    // }
    //
    // public void run() {
    // final float centerX = mToView.getWidth() / 2.0f;
    // final float centerY = mToView.getHeight() / 2.0f;
    // Rotate3dAnimation rotation;
    //
    // // -90도로 대기중인 VIEW를 0도로 원상복구.
    // rotation = new Rotate3dAnimation(-90, 0, centerX, centerY, 310.0f,
    // false);
    // rotation.setDuration(mDuration);
    // rotation.setFillAfter(true);
    // rotation.setInterpolator(new DecelerateInterpolator());
    //
    // mFromView.setVisibility(View.GONE);
    // mToView.setVisibility(View.VISIBLE);
    // mToView.startAnimation(rotation);
    //
    // // mAnimationRunning = false;
    // }
    // }
    //
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.clearCallButton:
                Log.i(THIS_FILE,"clearCallButton clicked");
                dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                break;
            case R.id.imgBtnKeypadClose:
                dispatchTriggerEvent(OnTriggerListener.RETURN_OFF);
                inCallButtons.setVisibility(VISIBLE);
                clearCallButton.setVisibility(VISIBLE);
                returnButton.setVisibility(VISIBLE);
                //imgBtnReturn.setVisibility(View.VISIBLE);
                imgBtnKeypadClose.setVisibility(View.GONE);
                btnReturnSend.setVisibility(GONE);
                btnReturnReplace.setVisibility(GONE);
                btnReturnEnd.setVisibility(GONE);
                dialPad.setVisibility(GONE);
                callModeControlKeyPadWidget.setVisibility(GONE);
                mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);

                break;
            case R.id.dialpadButton:
                inCallButtons.setVisibility(View.GONE);
                //imgBtnReturn.setVisibility(View.GONE);
                imgBtnKeypadClose.setVisibility(View.VISIBLE);
                dialPad.setVisibility(VISIBLE);
                callModeControlKeyPadWidget.setVisibility(VISIBLE);
                dialPad.bringToFront();
                dialPad.setMode("KEYPAD");
                break;
            case R.id.speakerButton:
                if (((CheckBox) v).isChecked()) {
                    dispatchTriggerEvent(OnTriggerListener.SPEAKER_ON);
                } else {
                    dispatchTriggerEvent(OnTriggerListener.SPEAKER_OFF);
                }
                break;
            case R.id.muteButton:
                if (((CheckBox) v).isChecked()) {
                    dispatchTriggerEvent(OnTriggerListener.MUTE_ON);
                } else {
                    dispatchTriggerEvent(OnTriggerListener.MUTE_OFF);
                }
                break;
            case R.id.takeCallButton:
                Log.i(THIS_FILE,"takeCallButton clicked");

                /**
                 *  안드로이드10 관련 수정 2020.05.29 맹완석
                 */
                if (Build.VERSION.SDK_INT >= 29){
                    App.isCallTakeClicked=true;
                }

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasPermissionAudio = mContext.checkSelfPermission( Manifest.permission.RECORD_AUDIO );

                    List<String> permissions = new ArrayList<String>();
                    if( hasPermissionAudio != PackageManager.PERMISSION_GRANTED ) {
                        permissions.add( Manifest.permission.RECORD_AUDIO );
                    }
                    if( !permissions.isEmpty() ) {
                        mActivity.requestPermissions( permissions.toArray( new String[permissions.size()] ), 1 );
                        if (mActivity.shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {

                        }
                    } else {
                        App.isReturnContactClicked=false;
                        dispatchTriggerEvent(OnTriggerListener.TAKE_CALL);
//                        setCallLockerVisibility(GONE);
//                        inCallButtons.setVisibility(VISIBLE);
//                        setCallModeControlVisibility(VISIBLE);

                        Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                        intent.putExtra("visible", View.INVISIBLE);
                        mContext.sendBroadcast(intent);
                    }
                } else {
                    App.isReturnContactClicked=false;
                    dispatchTriggerEvent(OnTriggerListener.TAKE_CALL);
//                    setCallLockerVisibility(GONE);
//                    inCallButtons.setVisibility(VISIBLE);
//                    setCallModeControlVisibility(VISIBLE);

                    Intent intent = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                    intent.putExtra("visible", View.INVISIBLE);
                    mContext.sendBroadcast(intent);
                }


                break;
            case R.id.declineCallButton:
                App.isReturnContactClicked=false;
                dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
                setCallLockerVisibility(GONE);

                Intent intent2 = new Intent(SipManager.ACTION_SIP_CALL_BOTTOMSHEET);
                intent2.putExtra("visible", View.INVISIBLE);
                mContext.sendBroadcast(intent2);
                break;
            // case R.id.detailsButton:
            // dispatchTriggerEvent(OnTriggerListener.DETAILED_DISPLAY);
            // break;
            case R.id.holdButton:
                boolean isReturn2= mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED);
                if (!isReturn2){
                    dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                } else {
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                    holdButton.setChecked(false);
                }

                break;
            case R.id.recButton:
                // BJH
                String toggle = mPrefs
                        .getPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE);
                if (toggle != null && toggle.equals("Y"))
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE, "");
                else
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE, "Y");
                dispatchTriggerEvent(OnTriggerListener.REC);
                break;
            case R.id.returnButton:
                App.isReturnContactClicked=false;
                showFilterPopup(v);
                /*App.isDialClicked=true;
                App.isReturnClicked=true;
                if (!holdButton.isChecked()){
                    dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                }
                Intent intentDial=new Intent(mContext, Dial.class);
                intentDial.putExtra("ReturnClicked",true);
                mContext.startActivity(intentDial);*/
                break;
            case R.id.btnReturnSend:
                Log.i(THIS_FILE,"btnReturnSend clicked");
                if (getReturnCallNumber().trim().length()>3){
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ACTION_YN,true);
                    dispatchTriggerEvent(OnTriggerListener.RETURN_SEND);
                    btnReturnEnd.setVisibility(VISIBLE);
                    btnReturnReplace.setVisibility(View.VISIBLE);
                    btnReturnReplace.getBackground().setAlpha(100);
                    btnReturnReplace.setEnabled(false);

                    btnReturnSend.setVisibility(View.GONE);
                    imgBtnKeypadClose.setVisibility(GONE);

                    dialPad.setVisibility(GONE);
                    callModeControlKeyPadWidget.setVisibility(GONE);

                    setReturnConnect();
                }
                break;
            case R.id.btnReturnEnd:
                Log.i(THIS_FILE,"btnReturnEnd clicked");
                dispatchTriggerEvent(OnTriggerListener.RETURN_CANCEL);
                if (App.isReturnContactClicked){
                    setReturnContactDisconnect();
                }else {
                    setReturnDisconnect();
                }

                break;
            case R.id.btnReturnReplace:
                Log.i(THIS_FILE,"btnReturnReplace clicked");
                dispatchTriggerEvent(OnTriggerListener.RETURN_REPLACE);
                btnReturnReplace.setEnabled(false);
                dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Log.d(THIS_FILE, "Hey you hit the key : " + keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_CALL:
                if (controlMode == MODE_LOCKER) {
                    dispatchTriggerEvent(OnTriggerListener.TAKE_CALL);
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_ENDCALL:
                if (controlMode == MODE_LOCKER) {
                    // dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
                    dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                    return true;
                } else if (controlMode == MODE_CONTROL) {
                    dispatchTriggerEvent(OnTriggerListener.CLEAR_CALL);
                    return true;
                }
            default:
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void showFilterPopup(View v) {
        Context wrapper = new ContextThemeWrapper(mContext, R.style.SmsActionTheme);
        PopupMenu popup = new PopupMenu(wrapper, v);
        Menu menu=popup.getMenu();
        menu.add(Menu.NONE, MENU_OPTION_CONTACT, Menu.NONE, R.string.contact);
        menu.add(Menu.NONE, MENU_OPTION_DIALPAD, Menu.NONE, R.string.input_number);
        // Setup menu item selection
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Log.i(THIS_FILE,"onMenuItemClick:"+item.getItemId());
                switch (item.getItemId()) {
                    case MENU_OPTION_CONTACT:
                        dispatchTriggerEvent(OnTriggerListener.SELECT_CONTACT);
                        if (!holdButton.isChecked()){
                            dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                        }
                        break;
                    case MENU_OPTION_DIALPAD:
                        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,true);
                        imgBtnKeypadClose.setVisibility(VISIBLE);
                        btnReturnSend.setVisibility(VISIBLE);

                        returnButton.setVisibility(INVISIBLE);

                        inCallButtons.setVisibility(GONE);
                        btnReturnEnd.setVisibility(GONE);
                        clearCallButton.setVisibility(GONE);
                        btnReturnReplace.setVisibility(GONE);

                        dialPad.setVisibility(VISIBLE);
                        callModeControlKeyPadWidget.setVisibility(VISIBLE);
                        dialPad.bringToFront();
                        dialPad.setMode("RETURN");
                        if (!holdButton.isChecked()){
                            dispatchTriggerEvent(OnTriggerListener.TOGGLE_HOLD);
                        }
                        dispatchTriggerEvent(OnTriggerListener.RETURN_ON);
                        break;
                    default:
                        break;
                }

                return false;
            }
        });
        // Handle dismissal with: popup.setOnDismissListener(...);
        // Show the menu
        popup.show();
    }

    /**
     * This class listens for the end of the first half of the animation. It
     * then posts a new action that effectively swaps the views when the
     * container is rotated 90 degrees and thus invisible.
     */
    // private final class DisplayNextView implements
    // Animation.AnimationListener {
    // private ViewGroup mFromView;
    // private ViewGroup mToView;
    // private int mDuration;
    //
    // private DisplayNextView(ViewGroup fromView, ViewGroup toView, int
    // duration) {
    // mFromView = fromView;
    // mToView = toView;
    // mDuration = duration;
    // }
    //
    // public void onAnimationStart(Animation animation) {
    // }
    //
    // public void onAnimationEnd(Animation animation) {
    // mToView.post(new SwapViews(mFromView,mToView,mDuration));
    // }
    //
    // public void onAnimationRepeat(Animation animation) {
    // }
    // }
    public void setMediaState(MediaState mediaState) {
        lastMediaState = mediaState;
        muteButton.setEnabled(mediaState.canMicrophoneMute);
        muteButton.setChecked(mediaState.isMicrophoneMute);
        speakerButton.setEnabled(mediaState.canSpeakerphoneOn);
        speakerButton.setChecked(mediaState.isSpeakerphoneOn);
    }

    public void declineCall() { //거절메시지 보내기용
        dispatchTriggerEvent(OnTriggerListener.DECLINE_CALL);
        setCallLockerVisibility(GONE);
    }

    public void setNumber(String number){
        dialPad.setNumber(number);
    }

    public interface OnDialTriggerListener {
        void onTrigger(int keyCode, int dialTone);
    }

    public interface OnTriggerListener {

        int CLEAR_CALL = 1;

        int TAKE_CALL = CLEAR_CALL + 1;

        int DECLINE_CALL = TAKE_CALL + 1;

        int DIALPAD_ON = DECLINE_CALL + 1;

        int DIALPAD_OFF = DIALPAD_ON + 1;

        int MUTE_ON = DIALPAD_OFF + 1;

        int MUTE_OFF = MUTE_ON + 1;

        int BLUETOOTH_ON = MUTE_OFF + 1;

        int BLUETOOTH_OFF = BLUETOOTH_ON + 1;

        int SPEAKER_ON = BLUETOOTH_OFF + 1;

        int SPEAKER_OFF = SPEAKER_ON + 1;

        int DETAILED_DISPLAY = SPEAKER_OFF + 1;

        int TOGGLE_HOLD = DETAILED_DISPLAY + 1;

        int MEDIA_SETTINGS = TOGGLE_HOLD + 1;

        // int MEMO = MEDIA_SETTINGS +1;
        int REC = MEDIA_SETTINGS + 1;

        int CONTACTS = REC + 1;

        int STAT = CONTACTS + 1;

        int RETURN_ON = STAT + 1;

        int RETURN_OFF = RETURN_ON + 1;

        int RETURN_SEND = RETURN_OFF + 1;

        int RETURN_CANCEL = RETURN_SEND + 1;

        int RETURN_REPLACE = RETURN_CANCEL + 1;

        int INCOMING_CONTROL_ON = RETURN_REPLACE + 1;

        int INCOMING_CONTROL_OFF = INCOMING_CONTROL_ON + 1;

        int SELECT_CONTACT = INCOMING_CONTROL_OFF + 1;

        void onTrigger(int whichAction, SipCallSession call);
    }

    public String getReturnCallNumber(){
        return dialPad.getReturnCallNumber();
    }

    public void setBtnReturnReplaceVisible(){
        if (mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
            btnReturnReplace.setVisibility(View.VISIBLE);
        }
    }

    /*public void setBtnReturnEnd(){
        if (mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
            btnReturnSend.setVisibility(View.VISIBLE);
            btnReturnEnd.setVisibility(View.GONE);
            btnReturnReplace.setVisibility(View.GONE);
        }
    }
*/
    /*public void setSipCallSession(SipCallSession[] callinfo){
        mCallinfo=callinfo;
        int active_count=0;
        for (SipCallSession callSession:callinfo){
            if (callSession.isActive()){
                active_count++;
            }
        }

        if (App.isReturnClicked && active_count==2){
            returnButton.setVisibility(View.INVISIBLE);
            btnReturnReplace.setVisibility(View.VISIBLE);
        }else {
            returnButton.setVisibility(View.VISIBLE);
            btnReturnReplace.setVisibility(View.GONE);
        }
    }*/

    public void setReturnDisconnect(){
        Log.i(THIS_FILE,"setReturnDisconnect");
        imgBtnKeypadClose.setVisibility(VISIBLE);
        btnReturnSend.setVisibility(VISIBLE);

        inCallButtons.setVisibility(GONE);
        btnReturnEnd.setVisibility(GONE);
        clearCallButton.setVisibility(GONE);
        btnReturnReplace.setVisibility(GONE);

        callModeControlKeyPadWidget.setVisibility(VISIBLE);
        dialPad.setVisibility(VISIBLE);
        dialPad.bringToFront();
        dialPad.setMode("RETURN");

        //컨트롤버튼의 돌려주기
        returnButton.setVisibility(View.VISIBLE);
    }

    public void setReturnContactDisconnect(){
        Log.i(THIS_FILE,"setReturnContactDisconnect");
        imgBtnKeypadClose.setVisibility(GONE);
        btnReturnSend.setVisibility(GONE);

        inCallButtons.setVisibility(VISIBLE);
        btnReturnEnd.setVisibility(GONE);
        clearCallButton.setVisibility(VISIBLE);
        btnReturnReplace.setVisibility(GONE);

        callModeControlKeyPadWidget.setVisibility(GONE);
        dialPad.setVisibility(GONE);

        //컨트롤버튼의 돌려주기
        returnButton.setVisibility(View.VISIBLE);
    }

    public void setReturnConnect(){
        returnButton.setVisibility(View.INVISIBLE);
    }

    public void setReturnContactConnect(){
        Log.i(THIS_FILE,"setReturnContactConnect");
        btnReturnEnd.setVisibility(VISIBLE);
        btnReturnReplace.setVisibility(View.VISIBLE);
        btnReturnReplace.getBackground().setAlpha(100);
        btnReturnReplace.setEnabled(false);

        btnReturnSend.setVisibility(View.GONE);
        imgBtnKeypadClose.setVisibility(GONE);
        clearCallButton.setVisibility(GONE);
        returnButton.setVisibility(View.INVISIBLE);

        callModeControlKeyPadWidget.setVisibility(GONE);
        dialPad.setVisibility(GONE);
    }
}
