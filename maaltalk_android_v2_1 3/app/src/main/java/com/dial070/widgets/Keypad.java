package com.dial070.widgets;

import java.util.HashMap;
import java.util.Map;

import com.dial070.maaltalk.R;
import com.dial070.ui.Contacts;
import com.dial070.ui.SmsWrite;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.media.ToneGenerator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
//import android.view.View.OnClickListener;

public class Keypad extends LinearLayout implements View.OnClickListener, View.OnLongClickListener {
	private static final String THIS_FILE = "DIAL070_KEYPAD";
	private OnDialKeyListener onDialKeyListener;
	private EditText mEditDTMFNumber;
	private TextView mTxtReturnManual;
    private AppPrefs prefs;
    private ImageButton btn_msg_contact,btn_number_delete;
    private Context mContext;
	private static final Map<Integer, int[]> keypadButtons = new HashMap<Integer, int[]>(){
		private static final long serialVersionUID = -6640726621906396734L;

	{
		put(R.id.button0, new int[] {ToneGenerator.TONE_DTMF_0, KeyEvent.KEYCODE_0});
		put(R.id.button1, new int[] {ToneGenerator.TONE_DTMF_1, KeyEvent.KEYCODE_1});
		put(R.id.button2, new int[] {ToneGenerator.TONE_DTMF_2, KeyEvent.KEYCODE_2});
		put(R.id.button3, new int[] {ToneGenerator.TONE_DTMF_3, KeyEvent.KEYCODE_3});
		put(R.id.button4, new int[] {ToneGenerator.TONE_DTMF_4, KeyEvent.KEYCODE_4});
		put(R.id.button5, new int[] {ToneGenerator.TONE_DTMF_5, KeyEvent.KEYCODE_5});
		put(R.id.button6, new int[] {ToneGenerator.TONE_DTMF_6, KeyEvent.KEYCODE_6});
		put(R.id.button7, new int[] {ToneGenerator.TONE_DTMF_7, KeyEvent.KEYCODE_7});
		put(R.id.button8, new int[] {ToneGenerator.TONE_DTMF_8, KeyEvent.KEYCODE_8});
		put(R.id.button9, new int[] {ToneGenerator.TONE_DTMF_9, KeyEvent.KEYCODE_9});
		put(R.id.buttonpound, new int[] {ToneGenerator.TONE_DTMF_P, KeyEvent.KEYCODE_POUND});
		put(R.id.buttonstar, new int[] {ToneGenerator.TONE_DTMF_S, KeyEvent.KEYCODE_STAR});
	}};

	public void setMode(String mode){
		if (mode!=null && mode.equals("RETURN")){
			//btn_msg_contact.setVisibility(View.VISIBLE);
			btn_number_delete.setVisibility(View.VISIBLE);
			/*if (mEditDTMFNumber.getText().toString().trim().length()==0){
				mTxtReturnManual.setVisibility(View.VISIBLE);
				mEditDTMFNumber.setVisibility(View.GONE);
			}else {
				mTxtReturnManual.setVisibility(View.GONE);
				mEditDTMFNumber.setVisibility(View.VISIBLE);
			}*/
		}else{
			//btn_msg_contact.setVisibility(View.GONE);
			btn_number_delete.setVisibility(View.GONE);
			/*mTxtReturnManual.setVisibility(View.GONE);
			mEditDTMFNumber.setVisibility(View.VISIBLE);*/
		}
	}

	public void setNumber(String number){
		mEditDTMFNumber.setText(number);
		mEditDTMFNumber.setSelection(number.length());
		/*if (mEditDTMFNumber.getText().toString().trim().length()==0){
			mTxtReturnManual.setVisibility(View.VISIBLE);
			mEditDTMFNumber.setVisibility(View.GONE);
		}else {
			mTxtReturnManual.setVisibility(View.GONE);
			mEditDTMFNumber.setVisibility(View.VISIBLE);
		}*/
	}

	@Override
	public boolean onLongClick(View v) {
		switch (v.getId()) {
			case R.id.buttonpound: {
				Log.d("Keypad","buttonpound long Clicked");
                dispatchLongDialKeyEvent(v.getId());
				return true;
			}
			case R.id.buttonstar: {
				Log.d("Keypad","buttonstar long Clicked");
				dispatchLongDialKeyEvent(v.getId());
				return true;
			}
		}
		return false;
	}


	public interface OnDialKeyListener {
		
		void onTrigger(int keyCode, int dialTone);
	}
	
	public Keypad(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (isInEditMode()) //BJH 2016.09.09
			return;
		mContext=context;
        prefs = new AppPrefs(context);
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.keypad, this, true);

		mEditDTMFNumber = (EditText) findViewById(R.id.edit_dtmf_number);
		mTxtReturnManual = findViewById(R.id.txtReturnManual);
		btn_msg_contact = findViewById(R.id.btn_msg_contact);
		btn_number_delete = findViewById(R.id.btn_number_delete);
		btn_number_delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL);
				mEditDTMFNumber.onKeyDown(KeyEvent.KEYCODE_DEL, event);
				/*if (mEditDTMFNumber.getText().toString().trim().length()==0){
					mTxtReturnManual.setVisibility(View.VISIBLE);
					mEditDTMFNumber.setVisibility(View.GONE);
				}else {
					mTxtReturnManual.setVisibility(View.GONE);
					mEditDTMFNumber.setVisibility(View.VISIBLE);
				}*/
			}
		});
		btn_msg_contact.setOnClickListener(this);

		boolean isReturn= prefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED);
		if (isReturn){
			//btn_msg_contact.setVisibility(View.VISIBLE);
			btn_number_delete.setVisibility(View.VISIBLE);
			/*if (mEditDTMFNumber.getText().toString().trim().length()==0){
				mTxtReturnManual.setVisibility(View.VISIBLE);
				mEditDTMFNumber.setVisibility(View.GONE);
			}else {
				mTxtReturnManual.setVisibility(View.GONE);
				mEditDTMFNumber.setVisibility(View.VISIBLE);
			}*/
		}else{
			//btn_msg_contact.setVisibility(View.GONE);
			btn_number_delete.setVisibility(View.GONE);
			/*mEditDTMFNumber.setVisibility(View.VISIBLE);
			mTxtReturnManual.setVisibility(View.GONE);*/
		}
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		Log.d(THIS_FILE,"onFinishInflate");
		if (!isInEditMode()) {//BJH 2016.09.09
			for(int buttonId : keypadButtons.keySet()) {
				ImageButton button = (ImageButton) findViewById(buttonId);
				button.setOnClickListener(this);
				button.setOnLongClickListener(this);
			}
		}
		
	}	
	
	public void setOnDialKeyListener(OnDialKeyListener listener) {
		onDialKeyListener = listener;
	}

	private void dispatchDialKeyEvent(int buttonId) {
		if (onDialKeyListener != null) {
			if (buttonId==R.id.btn_msg_contact){
				onDialKeyListener.onTrigger(-1013, KeyEvent.KEYCODE_UNKNOWN);
			}else if(keypadButtons.containsKey(buttonId)) {
				int[] datas = keypadButtons.get(buttonId);
				onDialKeyListener.onTrigger(datas[1], datas[0]);
				addDTMF(datas[1]);
			}
			
		}
	}

    private void dispatchLongDialKeyEvent(int buttonId) {
        if (onDialKeyListener != null) {
            if(buttonId==R.id.buttonpound) {
                int[] datas = keypadButtons.get(buttonId);

		        prefs.setPreferenceStringValue(AppPrefs.RETURN_CALL_NUMBER, mEditDTMFNumber.getText().toString());

                onDialKeyListener.onTrigger(-1011, datas[0]);  //샵 롱클릭
            }else if(buttonId==R.id.buttonstar) {
				int[] datas = keypadButtons.get(buttonId);

				prefs.setPreferenceStringValue(AppPrefs.RETURN_CALL_NUMBER, mEditDTMFNumber.getText().toString());

				onDialKeyListener.onTrigger(-1012, datas[0]);  //별 롱클릭
			}

        }
    }

    public String getReturnCallNumber(){
		return mEditDTMFNumber.getText().toString().trim();
	}
	
	@Override
	public void onClick(View v) {
		int view_id = v.getId();
		dispatchDialKeyEvent(view_id);
		
	}	
	
	public String getKeyCode(int keyCode)
	{
		String str = "";
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_0:
				str = "0";
				break;
			case KeyEvent.KEYCODE_1:
				str = "1";
				break;
			case KeyEvent.KEYCODE_2:
				str = "2";
				break;
			case KeyEvent.KEYCODE_3:
				str = "3";
				break;
			case KeyEvent.KEYCODE_4:
				str = "4";
				break;
			case KeyEvent.KEYCODE_5:
				str = "5";
				break;
			case KeyEvent.KEYCODE_6:
				str = "6";
				break;
			case KeyEvent.KEYCODE_7:
				str = "7";
				break;
			case KeyEvent.KEYCODE_8:
				str = "8";
				break;
			case KeyEvent.KEYCODE_9:
				str = "9";
				break;
			case KeyEvent.KEYCODE_POUND:
				str = "#";
				break;
			case KeyEvent.KEYCODE_STAR:
				str = "*";
				break;
		}
		return str;
	}	
	
	public void addDTMF(int keyCode)
	{
		String str = getKeyCode(keyCode);
		mEditDTMFNumber.append(str);
		/*if (mEditDTMFNumber.getText().toString().trim().length()==0){
			mTxtReturnManual.setVisibility(View.VISIBLE);
			mEditDTMFNumber.setVisibility(View.GONE);
		}else {
			mTxtReturnManual.setVisibility(View.GONE);
			mEditDTMFNumber.setVisibility(View.VISIBLE);
		}*/
	}
	
	public void clearDTMF()
	{
		mEditDTMFNumber.setText("");
	}
}
