package com.dial070.widgets;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.media.ToneGenerator;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.dial070.global.GlobalConst;
import com.dial070.maaltalk.R;

public class DialPad extends LinearLayout implements OnClickListener
{

	/**
	 * 
	 */
	OnDialKeyListener onDialKeyListener;

	/**
	 * 
	 */
	private static final Map<Integer, int[]> mDialButtons = new HashMap<Integer, int[]>()
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(R.id.dial_0, new int[] { ToneGenerator.TONE_DTMF_0, KeyEvent.KEYCODE_0 });
			put(R.id.dial_1, new int[] { ToneGenerator.TONE_DTMF_1, KeyEvent.KEYCODE_1 });
			put(R.id.dial_2, new int[] { ToneGenerator.TONE_DTMF_2, KeyEvent.KEYCODE_2 });
			put(R.id.dial_3, new int[] { ToneGenerator.TONE_DTMF_3, KeyEvent.KEYCODE_3 });
			put(R.id.dial_4, new int[] { ToneGenerator.TONE_DTMF_4, KeyEvent.KEYCODE_4 });
			put(R.id.dial_5, new int[] { ToneGenerator.TONE_DTMF_5, KeyEvent.KEYCODE_5 });
			put(R.id.dial_6, new int[] { ToneGenerator.TONE_DTMF_6, KeyEvent.KEYCODE_6 });
			put(R.id.dial_7, new int[] { ToneGenerator.TONE_DTMF_7, KeyEvent.KEYCODE_7 });
			put(R.id.dial_8, new int[] { ToneGenerator.TONE_DTMF_8, KeyEvent.KEYCODE_8 });
			put(R.id.dial_9, new int[] { ToneGenerator.TONE_DTMF_9, KeyEvent.KEYCODE_9 });
			put(R.id.dial_pound, new int[] { ToneGenerator.TONE_DTMF_P, KeyEvent.KEYCODE_POUND });
			put(R.id.dial_star, new int[] { ToneGenerator.TONE_DTMF_S, KeyEvent.KEYCODE_STAR });
			put(R.id.dial_gcall, new int[] { GlobalConst.TONE_NONE, GlobalConst.USER_KEY_CODE_GCALL });
			put(R.id.dial_call, new int[] { GlobalConst.TONE_NONE, GlobalConst.USER_KEY_CODE_SIP });
			put(R.id.dial_contact, new int[] { GlobalConst.TONE_NONE, GlobalConst.USER_KEY_CODE_CONTACT });
			put(R.id.imgBtnClose, new int[] { GlobalConst.TONE_NONE, GlobalConst.USER_KEY_CODE_CLOSE });
		}
	};

	/** Interface definition for a callback.
	 * @author 
	 *
	 */
	public interface OnDialKeyListener
	{

		/**
		 * Called when the user make an action
		 * 
		 * @param keyCode
		 *            keyCode pressed
		 * @param dialTone
		 *            corresponding dialtone
		 */
		void onTrigger(int keyCode, int dialTone);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public DialPad(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		if (isInEditMode()) //BJH 2016.10.24
			return;
		
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.dialpad, this, true);
	}

	/* (non-Javadoc)
	 * @see android.view.View#onFinishInflate()
	 */
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		
		if (!isInEditMode()) { //BJH 2016.10.20
			for (int buttonId : mDialButtons.keySet())
			{
				ImageButton button = (ImageButton) findViewById(buttonId);
				button.setOnClickListener(this);
			}
		}
	}

	/**
	 * @param listener
	 */
	public void setOnDialKeyListener(OnDialKeyListener listener)
	{
		onDialKeyListener = listener;
	}

	/**
	 * @param buttonId
	 */
	private void dispatchDialKeyEvent(int buttonId)
	{
		if (onDialKeyListener != null)
		{
			if (mDialButtons.containsKey(buttonId))
			{
				int[] datas = mDialButtons.get(buttonId);

				onDialKeyListener.onTrigger(datas[1], datas[0]);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{

		int view_id = v.getId();
		dispatchDialKeyEvent(view_id);
	}

}
