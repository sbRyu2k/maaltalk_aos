package com.dial070.widgets;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarPreference extends DialogPreference implements SeekBar.OnSeekBarChangeListener {
	private static final String androidns = "http://schemas.android.com/apk/res/android";

//	private static final String THIS_FILE = "SeekBarPrefs";

	private SeekBar seekBar;
	private TextView splashText, valueText;
	private Context mContext;

	private String dialogMessage, suffix;
	private float mValue, mMax, mDefaultValue = (float) 0.0;

	public SeekBarPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;

		dialogMessage = attrs.getAttributeValue(androidns, "dialogMessage");
		suffix = attrs.getAttributeValue(androidns, "text");
		mDefaultValue = attrs.getAttributeFloatValue(androidns, "defaultValue", (float) 0.0);
		mMax = attrs.getAttributeIntValue(androidns, "max", 10);
		
	}

//	public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
//		super(context, attrs, defStyle);
//		// TODO Auto-generated constructor stub
//	}

	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
		// TODO Auto-generated method stub
		String t = String.valueOf(arg1/10.0);
		valueText.setText(suffix == null ? t : t.concat(suffix));
		mValue = (float) (arg1 / 10.0);
		callChangeListener(new Float(mValue));		
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void setMax(float aMax) {
		mMax = aMax;
	}

	public float getMax() {
		return mMax;
	}
	
	public void setProgress(float progress) {
		mValue = progress;
		if (seekBar != null) {
			seekBar.setProgress( (int) (progress*10.0));
		}
	}

	public double getProgress() {
		return mValue;
	}		
	
	@Override
	protected View onCreateDialogView() {
		// TODO Auto-generated method stub
		LinearLayout.LayoutParams params;
		LinearLayout layout = new LinearLayout(mContext);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setPadding(6, 6, 6, 6);

		splashText = new TextView(mContext);
		if (dialogMessage != null) {
			splashText.setText(dialogMessage);
		}
		layout.addView(splashText);

		valueText = new TextView(mContext);
		valueText.setGravity(Gravity.CENTER_HORIZONTAL);
		valueText.setTextSize(32);
		params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		layout.addView(valueText, params);

		seekBar = new SeekBar(mContext);
		seekBar.setOnSeekBarChangeListener(this);
		layout.addView(seekBar, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

		if (shouldPersist()) {
			mValue = getPersistedFloat(mDefaultValue);
		}
		
		seekBar.setMax( (int)(mMax*10) );
		seekBar.setProgress( (int)(10*mValue) );
		
		return layout;
	}

	@Override
	protected void onBindDialogView(View view) {
		// TODO Auto-generated method stub
		super.onBindDialogView(view);
		seekBar.setMax( (int) (10*mMax) );
		seekBar.setProgress( (int) (10 * mValue) );		
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		// TODO Auto-generated method stub
		super.onDialogClosed(positiveResult);
		if(positiveResult && shouldPersist()) {
			persistFloat(mValue);
		}		
	}

	@Override
	protected void onSetInitialValue(boolean restorePersistedValue,
			Object defaultValue) {
		// TODO Auto-generated method stub
		super.onSetInitialValue(restorePersistedValue, defaultValue);
		if (restorePersistedValue) {
			mValue = shouldPersist() ? getPersistedFloat(mDefaultValue) : 0;
		} else {
			mValue = (Float) defaultValue;
		}		
	}

	
}
