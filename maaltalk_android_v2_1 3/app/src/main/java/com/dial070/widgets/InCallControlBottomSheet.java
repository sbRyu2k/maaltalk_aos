package com.dial070.widgets;

import android.content.Context;
import android.graphics.Canvas;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dial070.maaltalk.R;

/**
 * TODO: document your custom view class.
 */
public class InCallControlBottomSheet extends FrameLayout {

    private BottomSheetBehavior bottomSheetBehavior;
    private TextView bottomSheetHeading;
    private ImageView imgArrowUp,imgArrowDown;
    public InCallControlBottomSheet(final Context context, AttributeSet attrs) {
        super(context, attrs);
        //init(attrs, 0);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.in_call_control_bottom_sheet, this, true);

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));

        imgArrowUp=findViewById(R.id.imgArrowUp);
        imgArrowDown=findViewById(R.id.imgArrowDown);
        imgArrowDown.setVisibility(View.INVISIBLE);
        bottomSheetHeading = (TextView) findViewById(R.id.bottomSheetHeading);

        // Capturing the callbacks for bottom sheet
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                /*if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    imgArrowUp.setVisibility(View.INVISIBLE);
                    imgArrowDown.setVisibility(View.VISIBLE);
                } else {
                    imgArrowUp.setVisibility(View.VISIBLE);
                    imgArrowDown.setVisibility(View.INVISIBLE);
                }*/

                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        imgArrowUp.setVisibility(View.VISIBLE);
                        imgArrowDown.setVisibility(View.INVISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        imgArrowUp.setVisibility(View.INVISIBLE);
                        imgArrowDown.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        imgArrowUp.setVisibility(View.INVISIBLE);
                        imgArrowDown.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }


            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
                //imgArrowDown.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }


}
