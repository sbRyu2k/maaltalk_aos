package com.dial070.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.api.SipUri.ParsedSipContactInfos;
import com.dial070.sip.models.CallerInfo;
import com.dial070.sip.pjsip.PjSipCalls;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.CallsUtils;
import com.dial070.ui.CallStat;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.Log;

import java.util.Timer;
import java.util.TimerTask;

//import android.widget.LinearLayout;

public class InCallInfo2 extends RelativeLayout {

	private static final String THIS_FILE = "InCallInfo2";
	private Context mContext;

	// private LinearLayout photoView;
	private TextView remoteName,txt_number, title;
	private Chronometer elapsedTime;
	// private int colorConnected, colorEnd;
	// private ImageButton statButton;
	private ImageView callUserBack;
	private ImageView callUserAnt;
	private TextView CallUserRTT;
	private TextView CallUserCodec;

	private ImageView callLock;

	SipCallSession callInfo = null;
	private boolean isHangup = false;
	// private boolean isAnswer = false;

	String remoteUri = "";
	private static String mGetNumber; //BJH 2016.11.11

	public InCallInfo2(Context context) {
		super(context);
		mContext = context;

		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.in_call_info_v2, this, true);
	}

	public InCallInfo2(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (isInEditMode()) //BJH 2016.09.09
			return;
		// TODO Auto-generated constructor stub

		mContext = context;
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.in_call_info_v2, this, true);
	}

	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();

		remoteName = (TextView) findViewById(R.id.name);
		remoteName.setSelected(true);
		txt_number = findViewById(R.id.txt_number);
		title = (TextView) findViewById(R.id.title);
		elapsedTime = (Chronometer) findViewById(R.id.elapsedTime);

		// photoView = (LinearLayout) findViewById(R.id.layout_lcd_call_top);

		callUserBack = (ImageView) findViewById(R.id.call_user_bg);
		callUserAnt = (ImageView) findViewById(R.id.call_user_ant);
		CallUserRTT = (TextView) findViewById(R.id.callRTT);
		CallUserCodec = (TextView) findViewById(R.id.callCodec);
		// statButton = (ImageButton) findViewById(R.id.statButton);
		// statButton.setVisibility(View.INVISIBLE);

		callLock = (ImageView) findViewById(R.id.call_lock);

		if (!isInEditMode()) { //BJH 2016.09.09
			if (ServicePrefs.mSRTP == 0)
				callLock.setVisibility(View.INVISIBLE);
			else
				callLock.setVisibility(View.VISIBLE);

			callUserAnt.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext, CallStat.class);
					mContext.startActivity(intent);
				}
			});

			CallUserCodec.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					togleCodec();
				}
			});
		}
	}

	private void showCodec()
	{
		CallUserCodec.setVisibility(View.INVISIBLE);

		if (SipService.currentService == null) return;
		if (SipService.pjService == null) return;
		if (callInfo != null)
		{
			int state = callInfo.getCallState();
			if (state == SipCallSession.InvState.CONFIRMED) {
				String codec = SipService.pjService.prefsWrapper.getMediaCodec();
				if (codec == null || codec.length() == 0)
					codec = "SILK";
				if (codec.equals("G729"))
				{
					CallUserCodec.setText("일반음질");
				}
				else
				{
					CallUserCodec.setText("고음질");
				}
				CallUserCodec.setVisibility(View.VISIBLE);
			}
		}
	}

	private void togleCodec()
	{
		if (SipService.currentService == null) return;
		if (SipService.pjService == null) return;


		if (callInfo != null)
		{
			int state = callInfo.getCallState();
			if (state == SipCallSession.InvState.CONFIRMED)
			{
				// change codec
				String currentCodec=SipService.pjService.prefsWrapper.getMediaCodec();
				String currentCodecQuality;
				String codec;
				if (currentCodec == null || currentCodec.length() == 0 || currentCodec.equals("SILK")) {
					codec = "G729";
					currentCodecQuality="고음질";
				}
				else {
					codec = "SILK";
					currentCodecQuality="일반음질";
				}

				final String codecCopy=codec;
				String sQuality;
				if (codec.equals("G729"))
				{
					sQuality="일반음질";
				}
				else
				{
					sQuality="고음질";
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setMessage("현재 "+currentCodecQuality+" 상태입니다.\n"+sQuality+"로 변경하시겠습니까?");
				builder.setPositiveButton(
						getResources().getString(R.string.yes),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								SipService.pjService.prefsWrapper.setMediaCodec(codecCopy);

								showCodec();

								SipService.pjService.setCodecsPriorities();

								// SEND REINIVATE FOR MEDIA UPDATE
								if (callInfo.getCallId() != SipCallSession.INVALID_CALL_ID) {
									final int _call_id = callInfo.getCallId();
									SipService.pjService.callReinvite2(_call_id, 0);
								}
							}
						});

				builder.setNegativeButton(
						getResources().getString(R.string.no),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
												int which) {

							}
						});

				builder.setTitle("음질 변경");
				builder.create();
				builder.show();
			}
		}
	}

	public void setCallState(SipCallSession aCallInfo) {
		callInfo = aCallInfo;
		updateRemoteName();
		updateTitle();
		updateElapsedTimer();
		updateRTT();
		showCodec();
	}

	public void setHangup() {
		isHangup = true;
		setCallState(callInfo);
	}

	public void setAnswer() {
		// isAnswer = true;
		title.setText(R.string.call_state_answer);
	}

	private synchronized void updateTitle() {
		if (isHangup) {
			if (callInfo != null)
				title.setText(R.string.call_state_disconnecting);
			else
				title.setText(R.string.call_state_disconnected);
			return;
		}

		if (callInfo != null) {
			int state = callInfo.getCallState();

			if (state == SipCallSession.InvState.NULL
					|| state == SipCallSession.InvState.DISCONNECTED) {
				title.setText(R.string.call_state_disconnected);
			} else {
				title.setText(CallsUtils.getStringCallState(callInfo, mContext));
			}
		} else {
			title.setText(R.string.call_state_disconnected);
		}

	}

	public void setTitleForCall(String number) {
	    Log.d(THIS_FILE,"number:"+number);
		String display_name = ContactHelper.getContactsNameByPhoneNumber(
				mContext, number);
		if (display_name != null) {
			remoteName.setText(display_name);
			txt_number.setVisibility(View.VISIBLE);
			txt_number.setText(number);
		}
		else {
			remoteName.setText(number);
			txt_number.setVisibility(View.GONE);
			txt_number.setText("");
		}
		title.setText(R.string.call_state_calling);
	}

	public void setTitleForFail() {
		title.setText(R.string.call_state_fail);
	}

	private synchronized void updateRemoteName() {
		if (callInfo == null) {
			return;
		}

		final String aRemoteUri = callInfo.getRemoteContact();

		// If not already set with the same value, just ignore it
		if (aRemoteUri != null && !aRemoteUri.equalsIgnoreCase(remoteUri)) {
			remoteUri = aRemoteUri;
			ParsedSipContactInfos uriInfos = SipUri.parseSipContact(remoteUri);
			String remoteContact = SipUri.getDisplayedSimpleContact(aRemoteUri);
			String remoteNumber = uriInfos.userName;
			mGetNumber = remoteNumber; // BJH 2016.11.11

			Log.d(THIS_FILE,"remoteContact:"+remoteContact);
			Log.d(THIS_FILE,"remoteNumber:"+remoteNumber);

			CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(
					mContext, remoteUri);
			if (callerInfo != null) {
				remoteContact = callerInfo.name;
				if (remoteContact == null || remoteContact.length() == 0) {
					remoteContact = callerInfo.phoneNumber;
					txt_number.setVisibility(View.GONE);
					txt_number.setText("");
				}else {
					txt_number.setVisibility(View.VISIBLE);
					txt_number.setText(callerInfo.phoneNumber);
				}

				// if (callerInfo.callType == 1)
				// {
				// remoteContact += "";
				//
				// callUserPhoto.setVisibility(View.VISIBLE);
				// String jid =
				// BuddyDataContainer.getBuddyJid(callerInfo.phoneNumber);
				// if(jid != null)
				// {
				// Bitmap avatar = BuddyDataContainer.getBuddyAvatar(jid);
				// if(avatar != null) photo.setImageBitmap(avatar);
				// }
				// }
				// else if(callerInfo.callType == 2) //VMS
				// {
				// callUserPhoto.setVisibility(View.GONE);
				// }
			}else{
				txt_number.setVisibility(View.GONE);
				txt_number.setText("");
			}

			remoteName.setText(remoteContact);

			// if (remoteNumber != null)
			// {
			// Drawable photo = ContactHelper.getPhoto(mContext, remoteNumber);
			// if (photo != null)
			// {
			// photoView.setBackgroundDrawable(photo);
			// }
			// else
			// {
			// photoView.setBackgroundDrawable(null);
			// }
			// }
		}
	}

	private void updateElapsedTimer() {

		if (callInfo == null || isHangup) {
			elapsedTime.stop();
			elapsedTime.setVisibility(GONE);
			title.setVisibility(VISIBLE);
			// statButton.setVisibility(View.INVISIBLE);
			return;
		}
		elapsedTime.setBase(callInfo.getConnectStart());

		int state = callInfo.getCallState();
		switch (state) {
		case SipCallSession.InvState.INCOMING:
		case SipCallSession.InvState.CALLING:
		case SipCallSession.InvState.EARLY:
		case SipCallSession.InvState.CONNECTING:
			title.setVisibility(VISIBLE);
			elapsedTime.setVisibility(GONE);
			elapsedTime.start();
			connectTimerStart();
			break;
		case SipCallSession.InvState.CONFIRMED:
			Log.d(THIS_FILE, "we start the timer now ");
			title.setVisibility(GONE);
			elapsedTime.start();
			elapsedTime.setVisibility(VISIBLE);
			// statButton.setVisibility(View.VISIBLE);
			connectTimerStop();
			activeTimerStart();
			break;
		case SipCallSession.InvState.NULL:
		case SipCallSession.InvState.DISCONNECTED:
			elapsedTime.stop();
			elapsedTime.setVisibility(GONE);
			title.setVisibility(VISIBLE);
			// statButton.setVisibility(View.INVISIBLE);
			connectTimerStop();
			activeTimerStop();
			// elapsedTime.setTextColor(colorEnd);
			break;
		}
	}

	private boolean checkOnline() {
		if (SipService.currentService == null)
			return false;
		SipProfileState accountInfo = null;
		try {
			accountInfo = SipService.currentService.getSipProfileState(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return (accountInfo != null && accountInfo.isValidForCall());
	}

	private boolean reg_state_online = false;

	private void updateRTT() {
		Thread t = new Thread() {
			public void run() {
				try {
					reg_state_online = checkOnline();
					handler.sendEmptyMessage(UPDATE_REG_STATE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		if (t != null)
			t.start();
	}

	private void updateRegState() {
		// BJH
		int RTT;
		if (callRTT > 0)
			RTT = callRTT;
		else
			RTT = SipService.RTT;

		// Log.d(THIS_FILE, "RTT : " + RTT);

		if (reg_state_online) {

			/*
			 * int antena = R.drawable.img_call_0;
			 * 
			 * if (RTT < 100) antena = R.drawable.img_call_4; else if (RTT <
			 * 150) antena = R.drawable.img_call_3; else if (RTT < 200) antena =
			 * R.drawable.img_call_2; else antena = R.drawable.img_call_1;
			 * 
			 * callUserAnt.setBackgroundResource(antena);
			 */
			CallUserRTT.setText(RTT + "ms");
		} else {
			// callUserAnt.setBackgroundResource(R.drawable.img_call_0);
			CallUserRTT.setText(getResources().getString(
					R.string.net_stat_offline));
		}
	}

	private int m_Index = 0;

	private void updateUIFromConnectTimer() {
		/*
		 * int[] backs =
		 * {R.drawable.img_call_bg_1,R.drawable.img_call_bg_2,R.drawable
		 * .img_call_bg_3,R.drawable.img_call_bg_4};//BJH outofMemory int i =
		 * m_Index++ % backs.length; int back = backs[i];
		 * callUserBack.setBackgroundResource(back);
		 */
		callRTT = -1;
		updateRTT();
	}

	private int callRTT = -1;

	private void updateUIFromActiveTimer() {
		if (callInfo == null)
			return;

		int callId = callInfo.getCallId();
		int rtt = PjSipCalls.getCallRTT(callId);
		if (rtt > 0) {
			callRTT = rtt;
			updateRTT();
		} else {
			callRTT = -1;
		}
	}

	private static final int UPDATE_FROM_CONNECT_TIMER = 1;
	private static final int UPDATE_FROM_ACTIVE_TIMER = 2;
	private static final int UPDATE_REG_STATE = 3;
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case UPDATE_FROM_CONNECT_TIMER:
				updateUIFromConnectTimer();
				break;
			case UPDATE_FROM_ACTIVE_TIMER:
				updateUIFromActiveTimer();
				break;
			case UPDATE_REG_STATE:
				updateRegState();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	};

	private Timer connectTimer = null;

	private synchronized void connectTimerStart() {
		if (connectTimer != null)
			return;
		m_Index = 0;
		try {
			connectTimer = new Timer();
			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					// UPDATE
					if (handler != null)
						handler.sendMessage(handler
								.obtainMessage(UPDATE_FROM_CONNECT_TIMER));
				}
			};
			connectTimer.schedule(task, 100, 500);
		} catch (Exception e) {
		}
	}

	private synchronized void connectTimerStop() {
		if (connectTimer == null)
			return;
		connectTimer.cancel();
		connectTimer.purge();
		connectTimer = null;
		callUserBack.setBackgroundResource(R.drawable.img_call_bg_4);
	}

	private Timer activeTimer = null;

	private synchronized void activeTimerStart() {
		if (activeTimer != null)
			return;
		m_Index = 0;
		try {
			activeTimer = new Timer();
			TimerTask task = new TimerTask() {
				@Override
				public void run() {
					// UPDATE
					if (handler != null)
						handler.sendMessage(handler
								.obtainMessage(UPDATE_FROM_ACTIVE_TIMER));
				}
			};
			activeTimer.schedule(task, 100, 500);
		} catch (Exception e) {
		}
	}

	private synchronized void activeTimerStop() {
		if (activeTimer == null)
			return;
		activeTimer.cancel();
		activeTimer.purge();
		activeTimer = null;
		callUserBack.setBackgroundResource(R.drawable.img_call_bg_4);
	}
	
	//BJH 2016.11.11
	public static String getPhoneNumber() {
		return mGetNumber;
	}

}
