package com.dial070.widgets;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.maaltalk.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import com.dial070.utils.Log;
//import com.dial070.utils.AppPrefs;


public class IndexScrollView_backup extends LinearLayout implements OnGestureListener {

	private static final String DEFAULT_INDEX_TEXT_COLOR = "#009DC6";//"#339748";
	private static final int DEFAULT_TEXT_SIZE = 10;
	private static int TEXT_HEIGHT = 22;
	private static int MAX_TEXT_SIZE = 18;

	private static String[] INITIAL_SECTIONS = null;
    private static final String[] INITIAL_HANGUL_SECTIONS = { "ㄱ", "ㄴ", "ㄷ", "ㄹ",
		"ㅁ", "ㅂ", "ㅅ", "ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ", "#" };
	public static final String[] INITIAL_ENGLISH_SECTION = { "A", "B", "C", "D",
		"E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
		"O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y","Z", "#"};


	private String[] mSection;
	private TextView mCurrentTextView;
	private TextView mOldTextView;

	private GestureDetector gestureScanner;
	private List<TextView> mTextViewList = new ArrayList<TextView>();
	private Context mContext;
	private static float mDensity;
	private static int mTextHeight;
	private static int mTextSize;

//	private AppPrefs mPrefs;

	private final class FadingWindow implements Runnable {

		//@Override
		public void run() {
			fadingWindow();
		}
	}
	private FadingWindow mFadingWindow = new FadingWindow();
	Handler mHandler = new Handler();

	OnScrollChangeListener onScrollChangeListener;

	/** Interface definition for a callback.
	 * @author
	 *
	 */
	public interface OnScrollChangeListener
	{

		/**
		 * Called when the user make an action
		 *
		 * @param section
		 *            Current section character
		 */
		void onTrigger(String section);
	}


	public IndexScrollView_backup(Context context) {
		super(context);
		mContext = context;
		init();
	}

	public IndexScrollView_backup(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		init();
	}


	void init()
	{
		mSection = null;
		mOldTextView = null;

		gestureScanner = new GestureDetector(this);

		mTextViewList.clear();

//		mPrefs = new AppPrefs(mContext);
		if (Locale.getDefault().toString().equals("ko_KR")) {//BJH 2017.02.27
			INITIAL_SECTIONS = INITIAL_HANGUL_SECTIONS;
			TEXT_HEIGHT = 22;
			MAX_TEXT_SIZE = 18;
		} else {
			INITIAL_SECTIONS = INITIAL_ENGLISH_SECTION;
			TEXT_HEIGHT = 17;
			MAX_TEXT_SIZE = 13;
		}

		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(R.layout.index_scrollview, this, true);
		//this.setBackgroundColor(Color.DKGRAY);
		this.setGravity(Gravity.CENTER);
		this.setOrientation(LinearLayout.VERTICAL);
		this.setBackgroundResource(R.drawable.index_scrollview_border);

		mDensity = mContext.getResources().getDisplayMetrics().density;

		mTextHeight = (int) (TEXT_HEIGHT * mDensity + 0.5f + 5);
		mTextSize = (int) (DEFAULT_TEXT_SIZE * mDensity + 0.5f);
		if(mTextSize > MAX_TEXT_SIZE) mTextSize = MAX_TEXT_SIZE;

		//TextView Size
		//LinearLayout.LayoutParams param = new LinearLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		LayoutParams param = new LayoutParams( LayoutParams.WRAP_CONTENT, mTextHeight);
		

			for(int i=0; i < INITIAL_SECTIONS.length; i++)
			{
				TextView section = new TextView(getContext());
				section.setBackgroundColor(Color.TRANSPARENT);
				section.setText(INITIAL_SECTIONS[i]);
				section.setTextColor(Color.WHITE);
				//section.setTextColor(Color.parseColor("#5d6f82"));
				section.setTextSize(mTextSize);
				//section.setPadding(15,15,15,15);
				section.setTypeface(Typeface.DEFAULT_BOLD);
				section.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL);
				addView(section, param);
				mTextViewList.add(section);
			}


		//Fading 초기화 
		setFade(true);
	}
	
	private void fadingWindow()
	{
		setFade(true);
	}

	public void setSections(String[] sections)
	{
		if(mSection == null) mSection = new String[sections.length];
		mSection = sections.clone();
	}
	
	private void setTextColor(int color)
	{
		for(int i=0; i < mTextViewList.size(); i++)
		{
			TextView section = mTextViewList.get(i);

			section.setTextColor(color);
		}
	}
	
	private void setFade(boolean fade)
	{
		if(fade)
		{
			//this.setBackgroundColor(Color.TRANSPARENT);
			this.setBackgroundResource(R.drawable.index_scrollview_border);
			//setTextColor(Color.DKGRAY);
			setTextColor(Color.parseColor("#B4B5B4"));
		}
		else
		{
			//this.setBackgroundColor(Color.DKGRAY);
			this.setBackgroundResource(R.drawable.index_scrollview_border);
			setTextColor(Color.parseColor("#B4B5B4"));
			//setTextColor(Color.WHITE);
			//setTextColor(Color.parseColor("#009DC6"));
		}
	}
	
	private TextView setCurrentTextView(int y)
	{
//		TextView section;
		int i = 0;
		for(;i < mTextViewList.size(); i++)
		{
			if(y <= mTextHeight) break;
			y -= mTextHeight;
		}
		
		if(i >= mTextViewList.size()) i = mTextViewList.size()-1;
				
		if(mOldTextView != null) mOldTextView.setTextColor(Color.WHITE); //mOldTextView.setTextColor(Color.parseColor("#5d6f82")); //
		mOldTextView = mTextViewList.get(i);
		mCurrentTextView = mOldTextView;
		//mCurrentTextView.setTextColor(Color.BLUE);
		mCurrentTextView.setTextColor(Color.parseColor(DEFAULT_INDEX_TEXT_COLOR));
		return mCurrentTextView;
	}	
	
	private TextView getCurrentTextView(int y)
	{
//		TextView section;
		int i = 0;
		for(;i < mTextViewList.size(); i++)
		{
			if(y <= mTextHeight) break;
			y -= mTextHeight;
		}
		
		if(i >= mTextViewList.size()) i=mTextViewList.size()-1;
				
		if(mOldTextView == mTextViewList.get(i))
		{
			return mOldTextView;
		}
		else
		{
			mOldTextView.setTextColor(Color.WHITE);
			//mOldTextView.setTextColor(Color.parseColor("#5d6f82"));
			mOldTextView = mTextViewList.get(i);
			mCurrentTextView = mOldTextView;
			//mCurrentTextView.setTextColor(Color.BLUE);
			mCurrentTextView.setTextColor(Color.parseColor(DEFAULT_INDEX_TEXT_COLOR));
		}
		
		return mCurrentTextView;
	}		
	
	//제스쳐 이벤트 처리 
	//@Override
	public boolean onDown(MotionEvent e) {
			
		TextView section = setCurrentTextView((int)e.getY());
		
		if (onScrollChangeListener != null)
		{
			String text = section.getText().toString();
			onScrollChangeListener.onTrigger(text);
		}			
		
		setFade(false);
		//section.setTextColor(Color.BLUE);
		section.setTextColor(Color.parseColor(DEFAULT_INDEX_TEXT_COLOR));
		mHandler.removeCallbacks(mFadingWindow);
		mHandler.postDelayed(mFadingWindow, 2000);
		
		return true;
	}

	//@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	//@Override
	public void onLongPress(MotionEvent e) {
	}

	//@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		//Log.i("TEST","onScroll ----");
		//Log.i("TEST","E1 Action=" +e1.getAction() + ", E1 X=" +e1.getX() + " E1 Y=" +e1.getY() + ", E2 Action=" +e2.getAction() +  ", E2 X=" +e2.getX() + " E2 Y=" +e2.getY() + ", distanceX=" + distanceX + ", distanceY=" + distanceY);
		
		TextView section = getCurrentTextView((int)e2.getY());
		
		if (onScrollChangeListener != null)
		{
			onScrollChangeListener.onTrigger(section.getText().toString());
		}			

		setFade(false);
		//section.setTextColor(Color.BLUE);
		section.setTextColor(Color.parseColor(DEFAULT_INDEX_TEXT_COLOR));
		mHandler.removeCallbacks(mFadingWindow);
		mHandler.postDelayed(mFadingWindow, 2000);
			
		return false;
	}

	//@Override
	public void onShowPress(MotionEvent e) {
		
	}

	//@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gestureScanner.onTouchEvent(event);
	}
	
	/**
	 * @param listener
	 */
	public void setOnScrollChangeListener(OnScrollChangeListener listener)
	{
		onScrollChangeListener = listener;
	}
		
}
