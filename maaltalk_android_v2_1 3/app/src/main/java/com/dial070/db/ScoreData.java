package com.dial070.db;

import android.content.ContentValues;

public class ScoreData {
	
	public static String FIELD_NUMBER = "number";
	public static String FIELD_SCORE = "score";	
	public static String FIELD_ZONE_ID = "zone_id";	
	public static String FIELD_ZONE_SCORE = "zone_score";	
	public static String FIELD_CALL_COUNT = "call_count";	
	public static String FIELD_CALL_COUNT_SCORE = "call_count_score";	
	public static String FIELD_CALL_START = "call_start";	
	public static String FIELD_CALL_START_SCORE = "call_start_score";	
	public static String FIELD_CALL_DUR = "call_dur";	
	public static String FIELD_CALL_DUR_SCORE = "call_dur_score";	
	public static String FIELD_CALL_STATE = "call_state";	
	public static String FIELD_CALL_STATE_SCORE = "call_state_score";	
	public static String FIELD_CON_ADD = "con_add";	
	public static String FIELD_CON_ADD_SCORE = "con_add_score";	
	public static String FIELD_FAV_ADD = "fav_add";	
	public static String FIELD_FAV_ADD_SCORE = "fav_add_score";	
	public static String FIELD_CAL_ADD = "cal_add";	
	public static String FIELD_CAL_ADD_SCORE = "cal_add_score";	
	public static String FIELD_F1 = "f1";	
	public static String FIELD_F1_SCORE = "f1_score";	

	private String number;
	private int score;
	private int zone_id,zone_score;
	private int call_count,call_count_score;
	private int call_start,call_start_score;
	private int call_dur,call_dur_score;
	private int call_state,call_state_score;
	private int con_add,con_add_score;
	private int fav_add,fav_add_score;
	private int cal_add,cal_add_score;
	private int f1,f1_score;
	
	public ScoreData(ContentValues cv) {
		this.number = cv.getAsString(FIELD_NUMBER);
		this.score = cv.getAsInteger(FIELD_SCORE);
		this.zone_id = cv.getAsInteger(FIELD_ZONE_ID);
		this.zone_score = cv.getAsInteger(FIELD_ZONE_SCORE);
		this.call_count = cv.getAsInteger(FIELD_CALL_COUNT);
		this.call_count_score = cv.getAsInteger(FIELD_CALL_COUNT_SCORE);
		this.call_start = cv.getAsInteger(FIELD_CALL_START);
		this.call_start_score = cv.getAsInteger(FIELD_CALL_START_SCORE);
		this.call_dur = cv.getAsInteger(FIELD_CALL_DUR);
		this.call_dur_score = cv.getAsInteger(FIELD_CALL_DUR_SCORE);
		this.call_state = cv.getAsInteger(FIELD_CALL_STATE);
		this.call_state_score = cv.getAsInteger(FIELD_CALL_STATE_SCORE);
		this.con_add = cv.getAsInteger(FIELD_CON_ADD);
		this.con_add_score = cv.getAsInteger(FIELD_CON_ADD_SCORE);
		this.fav_add = cv.getAsInteger(FIELD_FAV_ADD);
		this.fav_add_score = cv.getAsInteger(FIELD_FAV_ADD_SCORE);
		this.cal_add = cv.getAsInteger(FIELD_CAL_ADD);
		this.cal_add_score = cv.getAsInteger(FIELD_CAL_ADD_SCORE);
		this.f1 = cv.getAsInteger(FIELD_F1);
		this.f1_score = cv.getAsInteger(FIELD_F1_SCORE);		
	}		
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_NUMBER, this.number);
		cv.put(FIELD_SCORE, this.score);
		cv.put(FIELD_ZONE_ID, this.zone_id);
		cv.put(FIELD_ZONE_SCORE, this.zone_score);
		cv.put(FIELD_CALL_COUNT, this.call_count);
		cv.put(FIELD_CALL_COUNT_SCORE, this.call_count_score);
		cv.put(FIELD_CALL_START, this.call_start);
		cv.put(FIELD_CALL_START_SCORE, this.call_start_score);
		cv.put(FIELD_CALL_DUR, this.call_dur);
		cv.put(FIELD_CALL_DUR_SCORE, this.call_dur_score);
		cv.put(FIELD_CALL_STATE, this.call_state);
		cv.put(FIELD_CALL_STATE_SCORE, this.call_state_score);
		cv.put(FIELD_CON_ADD, this.con_add);
		cv.put(FIELD_CON_ADD_SCORE, this.con_add_score);
		cv.put(FIELD_FAV_ADD, this.fav_add);
		cv.put(FIELD_FAV_ADD_SCORE, this.fav_add_score);
		cv.put(FIELD_CAL_ADD, this.cal_add);
		cv.put(FIELD_CAL_ADD_SCORE, this.cal_add_score);
		cv.put(FIELD_F1, this.f1);
		cv.put(FIELD_F1_SCORE, this.f1_score);
		return cv;
	}		
	
	
	public ScoreData(String number, int score, int zone_id, int zone_score, int call_count,int call_count_score,int call_start,int call_start_score,
			int call_dur,int call_dur_score, int call_state,int call_state_score, int con_add,int con_add_score, int fav_add,int fav_add_score,
			int cal_add, int cal_add_score, int f1, int f1_score)
	{
		this.number = number;
		this.score = score;
		this.zone_id = zone_id;
		this.zone_score = zone_score;
		this.call_count = call_count;
		this.call_count_score = call_count_score;
		this.call_start = call_start;
		this.call_start_score = call_start_score;
		this.call_dur = call_dur;
		this.call_dur_score = call_dur_score;
		this.call_state = call_state;
		this.call_state_score = call_state_score;
		this.con_add = con_add;
		this.con_add_score = con_add_score;
		this.fav_add = fav_add_score;
		this.fav_add_score = fav_add_score;
		this.cal_add = cal_add;
		this.cal_add_score = cal_add_score;
		this.f1 = f1;
		this.f1_score = f1_score;
	}

	
}
