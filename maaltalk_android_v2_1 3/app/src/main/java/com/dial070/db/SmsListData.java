package com.dial070.db;

import android.content.ContentValues;

public class SmsListData {
	public static String FIELD_ID = "_id";		//Seq
	//public static String FIELD_UID = "uid";		//Contacts ID
	public static String FIELD_USER_TYPE = "user_type";		//1:연락처 2:친구 
	public static String FIELD_MSG_TYPE = "msgType";	//1:receive 2:send 3:예약메세지
	public static String FIELD_USER_NAME = "user_name";
	public static String FIELD_PHONE_NUMBER = "phoneNumber";
	public static String FIELD_LAST_DATE = "lastDate";   
	public static String FIELD_NEW_MSG = "msgNewCount";
	public static String FIELD_LAST_MSG = "lastMessage";	

	public static final int RECEIVE_TYPE 	= 1;
	public static final int SEND_TYPE 		= 2;
	public static final int RESERVED_TYPE 	= 3;
	
	public static final int SORT_DATE_DESC	= 0;
	public static final int SORT_DATE_ASC	= 1;
	public static final int SORT_NAME_ASC	= 2;
	
	
	public static final int USER_TYPE_CONTACT 	= 1;
	public static final int USER_TYPE_BUDDY 	= 2;	
	//BJH
	public static final int USER_TYPE_PUSH 	= 3;	
	
	private String phoneNumber;
	private String userName;
	private String lastMessage;
	private int userType;
	private int msgType;
	private int msgNew;
	private long lastTime;

		
	public SmsListData(int aUserType, String aPhoneNumber, String aTo, int aType, String aMessage, String aUserName, long aTime, int aNewCount)
	{
		userType			= aUserType;
		phoneNumber			= aPhoneNumber;
		msgType 			= aType;
		lastMessage			= aMessage;		
		lastTime 			= aTime;
		msgNew				= aNewCount;
		userName			= aUserName;
	}
	
	public SmsListData(ContentValues cv) {
		
		userType			= cv.getAsInteger(FIELD_USER_TYPE);
		userName			= cv.getAsString(FIELD_USER_NAME);
		phoneNumber			= cv.getAsString(FIELD_PHONE_NUMBER);
		msgType 			= cv.getAsInteger(FIELD_MSG_TYPE);
		lastMessage			= cv.getAsString(FIELD_LAST_MSG);
		msgNew				= cv.getAsInteger(FIELD_NEW_MSG);
		lastTime 			= cv.getAsLong(FIELD_LAST_DATE);;
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_USER_TYPE, userType);
		cv.put(FIELD_USER_NAME, userName);
		cv.put(FIELD_PHONE_NUMBER, phoneNumber);
		cv.put(FIELD_MSG_TYPE, msgType);
		cv.put(FIELD_LAST_DATE, lastTime);
		cv.put(FIELD_LAST_MSG, lastMessage);
		cv.put(FIELD_NEW_MSG, msgNew);
		return cv;
	}		
}