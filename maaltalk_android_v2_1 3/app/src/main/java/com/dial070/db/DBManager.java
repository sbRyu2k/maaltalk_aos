package com.dial070.db;


import java.util.Date;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
//import android.text.format.DateFormat;
import android.util.*;

//import com.dial070.db.*;
import com.dial070.utils.ScoreUtils;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;


public class DBManager {
	static String THIS_FILE = "DB MANAGER";
	
	private static final String DATABASE_NAME = "com.dail070.database";
	private static final int DATABASE_VERSION = 11;
	
	private static final String FAVORITES_TABLE_NAME = "favorites";	
	private static final String RECENTS_TABLE_NAME = "recents";			
	private static final String SCORES_TABLE_NAME = "scores";		
	private static final String ZONES_TABLE_NAME = "zones";
	private static final String CTIME_TABLE_NAME = "ctime";
	
	private static final String SMS_LIST_TABLE_NAME = "sms_list";	
	private static final String SMS_MSG_TABLE_NAME = "sms_msg";	
	
	private static final String PUSH_RECORD_NAME = "push_record";	//BJH 2016.11.09

	private static final String TRAVEL_CATEGORY_TABLE_NAME = "travel_category";
	private static final String TRAVEL_STEP_TABLE_NAME = "travel_step";
	
	private static final int MAX_FAVORITES_ITEM_COUNT	= 24;
	private static final int MAX_RECENT_ITEM_COUNT	= 200;
	private static final int MAX_PUSH_RECORD_COUNT	= 200;
	
	//FIELD_FIELD1 -> 자동등록/수동등록 필드로 사용 : "AUTO", "MANUAL"
	private final static String TABLE_FAVORITES_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ FAVORITES_TABLE_NAME
			+ " ("
				+ FavoritesData.FIELD_PK				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ FavoritesData.FIELD_DISPLAYNAME		+ " TEXT,"
				+ FavoritesData.FIELD_PHONENUMBER		+ " TEXT,"
				+ FavoritesData.FIELD_UID				+ " TEXT,"
				+ FavoritesData.FIELD_IDENTIFIER		+ " TEXT,"
				+ FavoritesData.FIELD_SEARCH_NUMBER		+ " TEXT,"
				+ FavoritesData.FIELD_DENSITY			+ " INTEGER,"
				+ FavoritesData.FIELD_FIELD1			+ " TEXT,"
				+ FavoritesData.FIELD_FIELD2			+ " TEXT"
			+");";
						
	

	private final static String TABLE_RECENTCALLS_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ RECENTS_TABLE_NAME
			+ " ("
				+ RecentCallsData.FIELD_PK				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ RecentCallsData.FIELD_COMPOSITE_NAME	+ " TEXT,"
				+ RecentCallsData.FIELD_DATE			+ " INTEGER,"
				+ RecentCallsData.FIELD_PHONENUMBER		+ " TEXT,"
				+ RecentCallsData.FIELD_TYPE			+ " INTEGER,"
				+ RecentCallsData.FIELD_IDENTIFIER		+ " INTEGER,"
				+ RecentCallsData.FIELD_DURATION		+ " INTEGER,"
				+ RecentCallsData.FIELD_CALL_TYPE		+ " INTEGER,"
				+ RecentCallsData.FIELD_HIDDEN			+ " INTEGER,"
				+ RecentCallsData.FIELD_CALL_MEMO		+ " TEXT,"
				+ RecentCallsData.FIELD_CALL_REC		+ " TEXT,"
				+ RecentCallsData.FIELD_FIELD1			+ " TEXT,"
				+ RecentCallsData.FIELD_FIELD2			+ " TEXT,"
				+ RecentCallsData.FIELD_FIELD3			+ " TEXT,"
				+ RecentCallsData.FIELD_FIELD4			+ " TEXT"			
			+");";	
	
	private final static String TABLE_SCORES_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ SCORES_TABLE_NAME
			+ " ("
				+ ScoreData.FIELD_NUMBER			+ " TEXT PRIMARY KEY,"
				+ ScoreData.FIELD_SCORE				+ " INTEGER,"
				+ ScoreData.FIELD_ZONE_ID			+ " INTEGER,"
				+ ScoreData.FIELD_ZONE_SCORE		+ " INTEGER,"
				+ ScoreData.FIELD_CALL_COUNT		+ " INTEGER,"
				+ ScoreData.FIELD_CALL_COUNT_SCORE	+ " INTEGER,"
				+ ScoreData.FIELD_CALL_START		+ " INTEGER,"
				+ ScoreData.FIELD_CALL_START_SCORE	+ " INTEGER,"
				+ ScoreData.FIELD_CALL_DUR			+ " INTEGER,"
				+ ScoreData.FIELD_CALL_DUR_SCORE	+ " INTEGER,"
				+ ScoreData.FIELD_CALL_STATE		+ " INTEGER,"
				+ ScoreData.FIELD_CALL_STATE_SCORE	+ " INTEGER,"
				+ ScoreData.FIELD_CON_ADD			+ " INTEGER,"
				+ ScoreData.FIELD_CON_ADD_SCORE		+ " INTEGER,"
				+ ScoreData.FIELD_FAV_ADD			+ " INTEGER,"
				+ ScoreData.FIELD_FAV_ADD_SCORE		+ " INTEGER,"
				+ ScoreData.FIELD_CAL_ADD			+ " INTEGER,"
				+ ScoreData.FIELD_CAL_ADD_SCORE		+ " INTEGER,"
				+ ScoreData.FIELD_F1				+ " INTEGER,"
				+ ScoreData.FIELD_F1_SCORE			+ " INTEGER"
			+");";	
	
	
	private final static String TABLE_ZONES_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ ZONES_TABLE_NAME
			+ " ("
				+ ZoneData.FIELD_ZONE_ID		+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ ZoneData.FIELD_NUMBER			+ " TEXT,"
				+ ZoneData.FIELD_LATITUDE		+ " DOUBLE,"
				+ ZoneData.FIELD_LONGITUDE		+ " DOUBLE,"
				+ ZoneData.FIELD_DISTANCE		+ " INTEGER"

			+");";
	
	
	private final static String TABLE_CTIME_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ CTIME_TABLE_NAME
			+ " ("
				+ CTimeData.FIELD_CTIME_ID		+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ CTimeData.FIELD_NUMBER		+ " TEXT,"
				+ CTimeData.FIELD_CTIME			+ " INTEGER,"
				+ CTimeData.FIELD_CTIME_COUNT	+ " INTEGER"

			+");";	
	
	private final static String TABLE_SMS_LIST_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ SMS_LIST_TABLE_NAME
			+ " ("
				+ SmsListData.FIELD_ID					+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ SmsListData.FIELD_USER_TYPE			+ " INTEGER,"
				+ SmsListData.FIELD_USER_NAME			+ " TEXT,"
				+ SmsListData.FIELD_MSG_TYPE			+ " INTEGER,"
				+ SmsListData.FIELD_PHONE_NUMBER		+ " TEXT,"
				+ SmsListData.FIELD_LAST_DATE			+ " INTEGER,"
				+ SmsListData.FIELD_LAST_MSG			+ " TEXT,"
				+ SmsListData.FIELD_NEW_MSG				+ " INTEGER"
			+");";	
		
		private final static String TABLE_SMS_MSG_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ SMS_MSG_TABLE_NAME
			+ " ("
				+ SmsMsgData.FIELD_ID				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ SmsMsgData.FIELD_MSG_SEQ			+ " INTEGER,"
				+ SmsMsgData.FIELD_MSG_TYPE			+ " INTEGER,"
				+ SmsMsgData.FIELD_FROM				+ " TEXT,"
				+ SmsMsgData.FIELD_TO				+ " TEXT,"
				+ SmsMsgData.FIELD_CALLBACK			+ " TEXT,"
				+ SmsMsgData.FIELD_DATE				+ " INTEGER,"
				+ SmsMsgData.FIELD_MSG				+ " TEXT,"
				+ SmsMsgData.FIELD_NEW				+ " TEXT,"
				+ SmsMsgData.FIELD_SUBJECT			+ " TEXT,"
				+ SmsMsgData.FIELD_FILE1			+ " TEXT,"
				+ SmsMsgData.FIELD_FILE2			+ " TEXT,"
				+ SmsMsgData.FIELD_FILE3			+ " TEXT,"
				
				+ SmsMsgData.FIELD_FIELD1			+ " TEXT,"
				+ SmsMsgData.FIELD_FIELD2			+ " TEXT,"
				+ SmsMsgData.FIELD_FIELD3			+ " TEXT,"
				+ SmsMsgData.FIELD_FIELD4			+ " TEXT"			

				
			+");";	
	
//	private final static String TABLE_FAVORITES_DROP = "DROP TABLE IF EXIST"
//			+ FAVORITES_TABLE_NAME
//			+";";
//				
//	private final static String TABLE_RECENTCALLS_DROP = "DROP TABLE "
//			+ RECENTS_TABLE_NAME
//			+";";
//	
//	private final static String TABLE_SCORES_DROP = "DROP TABLE "
//			+ SCORES_TABLE_NAME
//			+";";
//	
//	private final static String TABLE_ZONES_DROP = "DROP TABLE "
//			+ ZONES_TABLE_NAME
//			+";";
//	
//	private final static String TABLE_CTIME_DROP = "DROP TABLE "
//			+ CTIME_TABLE_NAME
//			+";";		
//
//	private final static String TABLE_SMS_LIST_DROP = "DROP TABLE "
//			+ SMS_LIST_TABLE_NAME
//			+";";		
//	
//	private final static String TABLE_SMS_MSG_DROP = "DROP TABLE "
//			+ SMS_MSG_TABLE_NAME
//			+";";	
		//BJH 2016.11.09
		private final static String TABLE_PUSH_RECORD_CREATE = "CREATE TABLE IF NOT EXISTS "
				+ PUSH_RECORD_NAME
				+ " ("
					+ PushRecordData.FIELD_PID					+ " INTEGER PRIMARY KEY NOT NULL,"
					+ PushRecordData.FIELD_PUSH_TIME			+ " INTEGER,"
					+ PushRecordData.FIELD_RECEIVE_TIME			+ " INTEGER,"
					+ PushRecordData.FIELD_RESPONSE_TIME		+ " INTEGER,"
					+ PushRecordData.FIELD_PUSH_TYPE			+ " TEXT,"
					+ PushRecordData.FIELD_PUSH_VALUE			+ " TEXT,"
					+ PushRecordData.FIELD_CID					+ " TEXT,"
					+ PushRecordData.FIELD_USERID				+ " TEXT,"
					+ PushRecordData.FIELD_PHONE_NUMBER			+ " TEXT,"
					+ PushRecordData.FIELD_ACTION				+ " TEXT"		
				+");";

	private final static String TABLE_TRAVEL_CATEGORY = "CREATE TABLE IF NOT EXISTS "
			+ TRAVEL_CATEGORY_TABLE_NAME
			+ " ("
			+ TravelCategoryData.FIELD_PK				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ TravelCategoryData.FIELD_PATH	+ " TEXT NOT NULL UNIQUE,"
			+ TravelCategoryData.FIELD_THUMBNAIL_PATH		+ " TEXT,"
			+ TravelCategoryData.FIELD_COUNTRY_NAME		+ " TEXT NOT NULL,"
			+ TravelCategoryData.FIELD_COUNTRY_CODE		+ " TEXT,"
			+ TravelCategoryData.FIELD_STARTDATE		+ " TEXT NOT NULL,"
			+ TravelCategoryData.FIELD_ENDDATE		+ " TEXT NOT NULL,"
			+ TravelCategoryData.FIELD_PHOTO_COUNT		+ " INTEGER DEFAULT 0,"
			+ TravelCategoryData.FIELD_CATEGORY_NUM			+ " TEXT NOT NULL UNIQUE"
			+");";

		private final static String TABLE_TRAVEL_STEP = "CREATE TABLE IF NOT EXISTS "
			+ TRAVEL_STEP_TABLE_NAME
			+ " ("
			+ TravelStepData.FIELD_PK				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ TravelStepData.FIELD_PATH	+ " TEXT NOT NULL UNIQUE,"
			+ TravelStepData.FIELD_URI_PATH			+ " TEXT NOT NULL UNIQUE,"
			+ TravelStepData.FIELD_THUMBNAIL_PATH		+ " TEXT,"
			+ TravelStepData.FIELD_LATITUDE			+ " INTEGER DEFAULT 0,"
			+ TravelStepData.FIELD_LONGITUDE		+ " INTEGER DEFAULT 0,"
			+ TravelStepData.FIELD_COUNTRY_NAME		+ " TEXT NOT NULL,"
			+ TravelStepData.FIELD_COUNTRY_CODE		+ " TEXT,"
			+ TravelStepData.FIELD_ADDRESS			+ " TEXT,"
			+ TravelStepData.FIELD_DATE		+ " TEXT NOT NULL,"
			+ TravelStepData.FIELD_SIMPLE_DATE		+ " TEXT NOT NULL,"
			+ TravelStepData.FIELD_CATEGORY_NUM			+ " TEXT NOT NULL"
			+");";

		private final static String TABLE_TRAVEL_CATEGORY_DROP = "DROP TABLE IF EXISTS "
			+ TRAVEL_CATEGORY_TABLE_NAME
			+";";

	private final static String TABLE_TRAVEL_STEP_DROP = "DROP TABLE IF EXISTS "
			+ TRAVEL_STEP_TABLE_NAME
			+";";


	private final Context context;
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase db;	

	public DBManager(Context aContext) {
		context = aContext;
		databaseHelper = new DatabaseHelper(context);

	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);

		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(TABLE_FAVORITES_CREATE);	
			db.execSQL(TABLE_RECENTCALLS_CREATE);
			db.execSQL(TABLE_SCORES_CREATE);			
			db.execSQL(TABLE_ZONES_CREATE);			
			db.execSQL(TABLE_CTIME_CREATE);
			db.execSQL(TABLE_SMS_LIST_CREATE);
			db.execSQL(TABLE_SMS_MSG_CREATE);
			db.execSQL(TABLE_PUSH_RECORD_CREATE);//BJH 2016.11.09
			db.execSQL(TABLE_TRAVEL_STEP);
			db.execSQL(TABLE_TRAVEL_CATEGORY);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(THIS_FILE, "Upgrading database from version "
					+ oldVersion + " to " + newVersion);
			//db.execSQL(TABLE_ZONES_DROP);	
			//db.execSQL(TABLE_RECENTCALLS_DROP);
			//db.execSQL(TABLE_SMS_LIST_DROP);
			//db.execSQL(TABLE_SMS_MSG_DROP);
			onCreate(db);			
		}
		
	}
	
	private boolean opened = false;
	/**
	 * Open database
	 * 
	 * @return database manager
	 * @throws SQLException
	 */
	public DBManager open() 
	{
		try
		{
			db = databaseHelper.getWritableDatabase();
		}
		catch(SQLiteException e)
		{
			return null;
		}
		catch(Exception e)
		{
			return null;
		}		
		opened = true;
		return this;
	}

	/**
	 * Close database
	 */
	public void close() {
		databaseHelper.close();
		opened = false;
	}
	
	public boolean isOpen() {
		return opened;
	}	
		


	
	// --------
	// FAVORITES
	// --------

	/**
	 * Insert a new favorites into the database
	 * @param Favorites to add into the database
	 * @return the id of inserted row into database
	 */
	
	public long insertFavorites(ContentValues args) {
		deleteExpiredFavorites(MAX_FAVORITES_ITEM_COUNT-1);
		long result = db.insert(FAVORITES_TABLE_NAME, null, args);
		return result;
	}	
	
	public long insertFavorites(FavoritesData favorites){		
		deleteExpiredFavorites(MAX_FAVORITES_ITEM_COUNT-1);	
		return db.insert(FAVORITES_TABLE_NAME, null, favorites.getContentValues());
	}

	//public Cursor query (String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)	
	
	public Cursor getAllFavorites() {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_PK,
					FavoritesData.FIELD_DISPLAYNAME, 
					FavoritesData.FIELD_PHONENUMBER, 
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_SEARCH_NUMBER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, 
				null, null, 
				null, null, 
				FavoritesData.FIELD_DENSITY+" DESC LIMIT " + MAX_FAVORITES_ITEM_COUNT);
	}
	
	public Cursor getScoredFavorites() {
		String sql_stmt = "SELECT _pk,displayName,phoneNumber,uid,identifier,searchNumber,desity,FIELD1,FIELD2 FROM favorites a INNER JOIN scores b ON a.phoneNumber=b.number order by b.score desc limit 24";
		return db.rawQuery(sql_stmt, null);
	}
	
	
	
	public Cursor getAutoFavorites() {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_PK,
					FavoritesData.FIELD_DISPLAYNAME, 
					FavoritesData.FIELD_PHONENUMBER, 
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_SEARCH_NUMBER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, 
				FavoritesData.FIELD_FIELD1 + "= 'AUTO'", null, 
				null, null, 
				FavoritesData.FIELD_DENSITY+" DESC LIMIT 20");
	}	
	
	public Cursor getManualFavorites() {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_PK,
					FavoritesData.FIELD_DISPLAYNAME, 
					FavoritesData.FIELD_PHONENUMBER, 
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_SEARCH_NUMBER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, 
				FavoritesData.FIELD_FIELD1 + "= 'MANUAL'", null, 
				null, null, 
				FavoritesData.FIELD_DENSITY+" DESC LIMIT 4");
	}		
	
	public Cursor getFavorite(int fid) {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_DISPLAYNAME, 
					FavoritesData.FIELD_PHONENUMBER, 
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_SEARCH_NUMBER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, FavoritesData.FIELD_PK + "=" + Integer.toString(fid),
				null, null, null,null); 
	}	
	
	public Cursor getFavoriteByUid(int uid) {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_DISPLAYNAME, 
					FavoritesData.FIELD_PHONENUMBER, 
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_SEARCH_NUMBER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, FavoritesData.FIELD_UID + "=" + uid,
				null, null, null,null); 
	}		
	
	public Cursor getFavoriteByPhone(String phone_number) {
		return db.query(FAVORITES_TABLE_NAME, 
				new String[]{
					FavoritesData.FIELD_UID,
					FavoritesData.FIELD_DISPLAYNAME,
					FavoritesData.FIELD_PHONENUMBER ,
					FavoritesData.FIELD_UID, 
					FavoritesData.FIELD_IDENTIFIER,
					FavoritesData.FIELD_FIELD1,
					FavoritesData.FIELD_FIELD2,
					FavoritesData.FIELD_DENSITY
				}, FavoritesData.FIELD_SEARCH_NUMBER + "='" + phone_number + "'",
				null, null, null,null); 
	}		
	
	public boolean existFavoriteByPhone(String phone_number) {
		boolean rs = false;
		
		Cursor c = db.query(FAVORITES_TABLE_NAME, 
				new String[]{
				FavoritesData.FIELD_UID,
				FavoritesData.FIELD_DISPLAYNAME,
				FavoritesData.FIELD_PHONENUMBER ,
				FavoritesData.FIELD_UID, 
				FavoritesData.FIELD_IDENTIFIER
				}, 
				FavoritesData.FIELD_PHONENUMBER + "='" + phone_number + "'", null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;
	}
	
	public int updateFavoriteDensity(String phone_number, int density)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(FavoritesData.FIELD_DENSITY, density);
		return db.update(FAVORITES_TABLE_NAME, val, FavoritesData.FIELD_PHONENUMBER + "='" + phone_number + "'", null);
	}	
	
	public int updateFavoritesMemo(int fid, String memo)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(FavoritesData.FIELD_FIELD2, memo);
		return db.update(FAVORITES_TABLE_NAME, val, FavoritesData.FIELD_PK + "=" + fid, null);
	}		
	
	public int updateFavoritesName(String phone_number, String user_name)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(FavoritesData.FIELD_DISPLAYNAME, user_name);
		return db.update(FAVORITES_TABLE_NAME, val, FavoritesData.FIELD_PHONENUMBER + "='" + phone_number + "'", null);
	}			
	
	public int deleteFavoritesMemo(int fid)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(FavoritesData.FIELD_FIELD2, "");
		return db.update(FAVORITES_TABLE_NAME, val, FavoritesData.FIELD_PK + "=" + fid, null);
	}		
	
	public boolean deleteFavoriteByPhone(String phone_number) {
		return db.delete(FAVORITES_TABLE_NAME, FavoritesData.FIELD_PHONENUMBER + "='" + phone_number + "'", null) > 0;
	}
		
	
	public boolean deleteFavorite(int fid) {
		deleteExpiredFavorites(MAX_FAVORITES_ITEM_COUNT);
		return db.delete(FAVORITES_TABLE_NAME, FavoritesData.FIELD_PK + "=" + fid, null) > 0;
	}
	
	public boolean deleteExpiredFavorites(int offset) {		
		/*
		String sql_stmt = "SELECT _pk FROM favorites a INNER JOIN scores b ON a.phoneNumber=b.number order by b.score desc limit -1 offset 3";
		Cursor c = db.rawQuery(sql_stmt, null);
		if (c != null)
		{
			if (c.moveToFirst())
			{
				do
				{
					Log.d(THIS_FILE, "PK = " + c.getInt(0));
				}
				while(c.moveToNext());
			}
			c.close();
		}
		*/
		
		// DELETE INT
		String sql_stmt = "DELETE FROM favorites WHERE _pk IN (SELECT _pk FROM favorites a INNER JOIN scores b ON a.phoneNumber=b.number order by b.score desc limit -1 offset " + offset + ")";
		db.execSQL(sql_stmt);
		return true;
	}	
			
	
	
	public boolean deleteAllFavorites() {
		return db.delete(FAVORITES_TABLE_NAME, null, null) > 0;
	}	
		
	// ------------
	// RECENT CALLS
	// ------------

	/**
	 * Insert a new favorites into the database
	 * @param Favorites to add into the database
	 * @return the id of inserted row into database
	 */
	
	public long insertRecentCalls(ContentValues args) {		
		removeExpiredRecentCalls(MAX_RECENT_ITEM_COUNT-1);
		long result = db.insert(RECENTS_TABLE_NAME, null, args);

		return result;
	}
	
	public long insertRecentCalls(RecentCallsData recents) {		
		removeExpiredRecentCalls(MAX_RECENT_ITEM_COUNT-1);
		return db.insert(RECENTS_TABLE_NAME, null, recents.getContentValues());
	}	
	
	/*
	public Cursor getAllRecentCalls() {
		return db.query(RECENTS_TABLE_NAME, 
				new String[]{
					RecentCallsData.FIELD_PK,
					RecentCallsData.FIELD_COMPOSITE_NAME, 
					RecentCallsData.FIELD_DATE,
					RecentCallsData.FIELD_PHONENUMBER, 
					RecentCallsData.FIELD_TYPE,
					RecentCallsData.FIELD_IDENTIFIER,
					RecentCallsData.FIELD_DURATION,
					RecentCallsData.FIELD_CALL_TYPE,
					RecentCallsData.FIELD_HIDDEN,
					RecentCallsData.FIELD_CALL_MEMO
				}, 
				null, null, 
				null, null, 
				RecentCallsData.FIELD_PK +" DESC");
	}
	
	public Cursor getAllRecentCalls(int callType) {
		//SMS 제외 
		String where = RecentCallsData.FIELD_CALL_TYPE + " <> 4 AND " + RecentCallsData.FIELD_TYPE + "=";

		// Set the icon
		switch (callType) {
		case RecentCallsData.INCOMING_TYPE:
		case RecentCallsData.OUTGOING_TYPE:
		case RecentCallsData.MISSED_TYPE:	
			where += Integer.toString(callType);
			break;
		default:
			where = RecentCallsData.FIELD_CALL_TYPE + " <> 4";
			break;
		}
		
		return db.query(RECENTS_TABLE_NAME, 
				new String[]{
					RecentCallsData.FIELD_PK,
					RecentCallsData.FIELD_COMPOSITE_NAME, 
					RecentCallsData.FIELD_DATE,
					RecentCallsData.FIELD_PHONENUMBER, 
					RecentCallsData.FIELD_TYPE,
					RecentCallsData.FIELD_IDENTIFIER,
					RecentCallsData.FIELD_DURATION,
					RecentCallsData.FIELD_CALL_TYPE,
					RecentCallsData.FIELD_HIDDEN,
					RecentCallsData.FIELD_CALL_MEMO					
				}, 
				where, null, 
				null, null, 
				RecentCallsData.FIELD_PK +" DESC",
				String.valueOf(MAX_RECENT_ITEM_COUNT));
	}	
	*/
	
	private static boolean recentChanged = true;
	public boolean isRecentChanged()
	{
		return recentChanged;
	}
	
	public Cursor getAllRecentNormalCalls() 
	{
		// delete old data
		removeExpiredRecentCalls(MAX_RECENT_ITEM_COUNT);
		
		recentChanged = false;
		
		//Normal call 0 and SMS 4
		String where = RecentCallsData.FIELD_CALL_TYPE + "= 0 OR " + RecentCallsData.FIELD_CALL_TYPE + " = 4";
		
		return db.query(RECENTS_TABLE_NAME, 
				new String[]{
					RecentCallsData.FIELD_PK,
					RecentCallsData.FIELD_COMPOSITE_NAME, 
					RecentCallsData.FIELD_DATE,
					RecentCallsData.FIELD_PHONENUMBER, 
					RecentCallsData.FIELD_TYPE,
					RecentCallsData.FIELD_IDENTIFIER,
					RecentCallsData.FIELD_DURATION,
					RecentCallsData.FIELD_CALL_TYPE,
					RecentCallsData.FIELD_HIDDEN,
					RecentCallsData.FIELD_CALL_MEMO,					
					RecentCallsData.FIELD_CALL_REC
				}, 
				where, null, 
				null, null, 
				RecentCallsData.FIELD_DATE +" DESC", 
				String.valueOf(MAX_RECENT_ITEM_COUNT));
	}		
	
	public Cursor getRecentCalls(int pk) {
		return db.query(RECENTS_TABLE_NAME, 
				new String[]{
				RecentCallsData.FIELD_COMPOSITE_NAME, 
				RecentCallsData.FIELD_DATE,
				RecentCallsData.FIELD_PHONENUMBER, 
				RecentCallsData.FIELD_TYPE,
				RecentCallsData.FIELD_IDENTIFIER,
				RecentCallsData.FIELD_DURATION,
				RecentCallsData.FIELD_CALL_TYPE,
				RecentCallsData.FIELD_HIDDEN,
				RecentCallsData.FIELD_CALL_MEMO,
				RecentCallsData.FIELD_CALL_REC
				
				}, RecentCallsData.FIELD_PK + "=" + Integer.toString(pk),
				null, null, null,null); 
	}

	public Cursor searchRecentNormalCalls(String searchkey)
	{
		// delete old data
		removeExpiredRecentCalls(MAX_RECENT_ITEM_COUNT);

		recentChanged = false;

		//Normal call 0 and SMS 4
		String where = "("+RecentCallsData.FIELD_CALL_TYPE + "= 0 OR " + RecentCallsData.FIELD_CALL_TYPE + " = 4)" +" AND " + RecentCallsData.FIELD_PHONENUMBER + " LIKE '%"+searchkey + "%' OR " + RecentCallsData.FIELD_COMPOSITE_NAME + " LIKE '%"+searchkey +"%' COLLATE NOCASE";

		return db.query(RECENTS_TABLE_NAME,
				new String[]{
						RecentCallsData.FIELD_PK,
						RecentCallsData.FIELD_COMPOSITE_NAME,
						RecentCallsData.FIELD_DATE,
						RecentCallsData.FIELD_PHONENUMBER,
						RecentCallsData.FIELD_TYPE,
						RecentCallsData.FIELD_IDENTIFIER,
						RecentCallsData.FIELD_DURATION,
						RecentCallsData.FIELD_CALL_TYPE,
						RecentCallsData.FIELD_HIDDEN,
						RecentCallsData.FIELD_CALL_MEMO,
						RecentCallsData.FIELD_CALL_REC
				},
				where, null,
				null, null,
				RecentCallsData.FIELD_DATE +" DESC",
				String.valueOf(MAX_RECENT_ITEM_COUNT));
	}
	
	public boolean deleteRecentCalls(int pk) {
		recentChanged = true;
		return db.delete(RECENTS_TABLE_NAME, RecentCallsData.FIELD_PK + "=" + pk, null) > 0;
	}
	
	public int updateCallsMemo(int pk, String memo)
	{
		recentChanged = true;
		ContentValues val = new ContentValues();
		val.clear();
		val.put(RecentCallsData.FIELD_CALL_MEMO, memo);
		return db.update(RECENTS_TABLE_NAME, val, RecentCallsData.FIELD_PK + "=" + pk, null);
	}		
	
	public int deleteCallsMemo(int pk)
	{
		recentChanged = true;
		ContentValues val = new ContentValues();
		val.clear();
		val.put(RecentCallsData.FIELD_CALL_MEMO, "");
		return db.update(RECENTS_TABLE_NAME, val, RecentCallsData.FIELD_PK + "=" + pk, null);
	}		
	
	public boolean deleteAllRecentCalls() 
	{
		recentChanged = true;
		return db.delete(RECENTS_TABLE_NAME, null, null) > 0;
	}
	
	public void removeExpiredRecentCalls(int offset) 
	{
		recentChanged = true;
		db.delete(RECENTS_TABLE_NAME,RecentCallsData.FIELD_PK + " IN " +
			"(SELECT "+RecentCallsData.FIELD_PK+" FROM "+RECENTS_TABLE_NAME+" ORDER BY " + 
			RecentCallsData.FIELD_DATE + " DESC" + " LIMIT -1 OFFSET " + offset + ")", null);
	}
	
	public boolean checkRecentCalls(long call_date)
	{
		boolean rs = false;
		Cursor c = db.query(RECENTS_TABLE_NAME, 
				new String[]{
					RecentCallsData.FIELD_PK
				}, 
				RecentCallsData.FIELD_DATE + "='" + String.valueOf(call_date) + "'" 
				, null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;		
	}
	
	public long getLastCallDate()
	{
		long callDate = 0;
		Cursor c = db.query(RECENTS_TABLE_NAME, 
				new String[]{
					RecentCallsData.FIELD_DATE
				}, 
				null 
				, null, null, null, RecentCallsData.FIELD_DATE + " DESC LIMIT 1");

		if(c == null) return 0;
		
		if(c.getCount() > 0)
		{
			c.moveToFirst();
			callDate = c.getLong(c.getColumnIndex(RecentCallsData.FIELD_DATE));
		}
		c.close();
		return callDate;		
	}	
	
	
	// ------------
	// SCORES
	// ------------
	public long insertScores(ContentValues args) {

		long result = db.insert(SCORES_TABLE_NAME, null, args);

		return result;
	}
	
	public long insertScores(ScoreData scores) {

		return db.insert(SCORES_TABLE_NAME, null, scores.getContentValues());
	}
	
	public boolean existScoreByPhone(String phone_number) {
		boolean rs = false;
		
		Cursor c = db.query(SCORES_TABLE_NAME, 
				new String[]{
				ScoreData.FIELD_SCORE
				}, 
				ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;
	}

	public Cursor getScoreByPhone(String phone_number) {
		return db.query(SCORES_TABLE_NAME, 
				new String[]{
					ScoreData.FIELD_SCORE,
					ScoreData.FIELD_ZONE_ID,
					ScoreData.FIELD_ZONE_SCORE,
					ScoreData.FIELD_CALL_COUNT,
					ScoreData.FIELD_CALL_COUNT_SCORE,
					ScoreData.FIELD_CALL_START,
					ScoreData.FIELD_CALL_START_SCORE,
					ScoreData.FIELD_CALL_DUR,
					ScoreData.FIELD_CALL_DUR_SCORE,
					ScoreData.FIELD_CALL_STATE,
					ScoreData.FIELD_CALL_STATE_SCORE,
					ScoreData.FIELD_CON_ADD,
					ScoreData.FIELD_CON_ADD_SCORE,
					ScoreData.FIELD_FAV_ADD,
					ScoreData.FIELD_FAV_ADD_SCORE,
					ScoreData.FIELD_CAL_ADD,
					ScoreData.FIELD_CAL_ADD_SCORE,					
					ScoreData.FIELD_F1,
					ScoreData.FIELD_F1_SCORE
				}, ScoreData.FIELD_NUMBER + "='" + phone_number + "'",
				null, null, null,null); 
	}			
	
	public int updateScoreCall(String phone_number, int loc_score, int call_count, long call_start, int call_state, int call_dur, int con_add,int call_count_score, int call_start_score, int call_dur_score, int call_state_score, int con_add_score, int score)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_ZONE_SCORE, loc_score);
		val.put(ScoreData.FIELD_CALL_COUNT, call_count);
		val.put(ScoreData.FIELD_CALL_START, call_start);
		val.put(ScoreData.FIELD_CALL_STATE, call_state);
		val.put(ScoreData.FIELD_CALL_DUR, call_dur);
		val.put(ScoreData.FIELD_CON_ADD, con_add);
		val.put(ScoreData.FIELD_CALL_COUNT_SCORE, call_count_score);
		val.put(ScoreData.FIELD_CALL_START_SCORE, call_start_score);
		val.put(ScoreData.FIELD_CALL_STATE_SCORE, call_state_score);
		val.put(ScoreData.FIELD_CALL_DUR_SCORE, call_dur_score);
		val.put(ScoreData.FIELD_CON_ADD_SCORE, con_add_score);		
		val.put(ScoreData.FIELD_SCORE, score);
		return db.update(SCORES_TABLE_NAME, val, ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null);
	}		

	public int updateScoreContact(String phone_number, int con_add, int con_add_score)
	{
		//	주소록에 있으면 6 없으면 0
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_CON_ADD, con_add);
		val.put(ScoreData.FIELD_CON_ADD_SCORE, con_add_score);
		return db.update(SCORES_TABLE_NAME, val, ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null);
	}		
	
	public int updateScoreFavorite(String phone_number, int fav_add, int fav_add_score)
	{
		//	즐겨찾기에 등록이면 10 없으면 0
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_FAV_ADD, fav_add);
		val.put(ScoreData.FIELD_FAV_ADD_SCORE, fav_add_score);
		return db.update(SCORES_TABLE_NAME, val, ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null);
	}		
	
	public int updateScoreLocation(String phone_number,int score)
	{
		//UPDATE scores SET zone_id = 0, zone_score = 0";
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_ZONE_SCORE, score);
		return db.update(SCORES_TABLE_NAME, val, ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null);	
	}
	
	// 이 함수는 위치가 변경되었을때 전체를 다시 계산하기 위해 호출된다.
	public int updateResetScoreLocation()
	{
		//UPDATE scores SET zone_id = 0, zone_score = 0";
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_ZONE_SCORE, 0);
		val.put(ScoreData.FIELD_ZONE_ID, 0);
		return db.update(SCORES_TABLE_NAME, val, null, null);		
	}
	
	public int updateScoreZone()
	{
		String sql_stmt;
		sql_stmt = "UPDATE scores SET zone_id = 2, zone_score = 10 WHERE number IN (SELECT number FROM zones WHERE distance <= 2000)";
		db.execSQL(sql_stmt);
		return 0;
	}
	
	public int updateScoreCTime(String phone_number,int score)
	{
		//UPDATE scores SET zone_id = 0, zone_score = 0";
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ScoreData.FIELD_CALL_START_SCORE, score);
		return db.update(SCORES_TABLE_NAME, val, ScoreData.FIELD_NUMBER + "='" + phone_number + "'", null);	
	}
	
	public int updateScoreCTime()
	{
		Date startDate = new Date();
		int ctime = ScoreUtils.getTimeUnit(startDate);

		// reset all
		String sql_stmt = "UPDATE scores SET call_start_score = 4";
		db.execSQL(sql_stmt);
		
		int start = ctime;
		int end = ctime;
		
		// 3시간 이내
		start = ctime - 180;
		if (start < 0)
		{
			sql_stmt = "UPDATE scores SET call_start_score = 6 WHERE number IN (SELECT number FROM ctime WHERE ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
			
			start = 1440 - start;
			sql_stmt = "UPDATE scores SET call_start_score = 6 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) + ")";
			db.execSQL(sql_stmt);
		}
		else
		{
			sql_stmt = "UPDATE scores SET call_start_score = 6 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) +  " and ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
		}

		
		// 2시간 이내
		start = ctime - 120;
		if (start < 0)
		{
			sql_stmt = "UPDATE scores SET call_start_score = 8 WHERE number IN (SELECT number FROM ctime WHERE ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
			
			start = 1440 - start;
			sql_stmt = "UPDATE scores SET call_start_score = 8 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) + ")";
			db.execSQL(sql_stmt);
		}
		else
		{
			sql_stmt = "UPDATE scores SET call_start_score = 8 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) +  " and ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
		}

		
		// 1시간 이내
		start = ctime - 60;
		if (start < 0)
		{
			sql_stmt = "UPDATE scores SET call_start_score = 10 WHERE number IN (SELECT number FROM ctime WHERE ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
			
			start = 1440 - start;
			sql_stmt = "UPDATE scores SET call_start_score = 10 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) + ")";
			db.execSQL(sql_stmt);
		}
		else
		{
			sql_stmt = "UPDATE scores SET call_start_score = 10 WHERE number IN (SELECT number FROM ctime WHERE ctime >= " + String.valueOf(start) +  " and ctime <= " + String.valueOf(end) + ")";
			db.execSQL(sql_stmt);
		}
		
		return 0;
	}
	
	
	public void updateScore()
	{
		String sql_stmt = "UPDATE scores SET score = zone_score + call_count_score + call_start_score + call_dur_score + call_state_score + con_add_score + fav_add_score + cal_add_score";
		db.execSQL(sql_stmt);
	}
	
	public Cursor getScoreList() {
		String sql_stmt = "SELECT displayName,phoneNumber,score,zone_score,call_count_score,call_start_score,call_dur_score,call_state_score,con_add_score,fav_add_score FROM favorites a INNER JOIN scores b ON a.phoneNumber=b.number order by b.score desc limit 24";
		return db.rawQuery(sql_stmt, null);
	}
	
	
	// ------------
	// ZONES
	// ------------
	public long insertZones(ContentValues args) {

		long result = db.insert(ZONES_TABLE_NAME, null, args);

		return result;
	}
	
	public long insertZones(ZoneData zones) {

		return db.insert(ZONES_TABLE_NAME, null, zones.getContentValues());
	}
	
	public boolean existZoneByPhone(String phone_number) {
		boolean rs = false;
		
		Cursor c = db.query(ZONES_TABLE_NAME, 
				new String[]{
				ZoneData.FIELD_ZONE_ID
				}, 
				ZoneData.FIELD_NUMBER + "='" + phone_number + "'", null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;
	}
	
	public Cursor getZones() {
		return db.query(ZONES_TABLE_NAME, 
				new String[]{
					ZoneData.FIELD_ZONE_ID,
					ZoneData.FIELD_NUMBER,
					ZoneData.FIELD_LATITUDE,
					ZoneData.FIELD_LONGITUDE,
					ZoneData.FIELD_DISTANCE
				}, null,null, null, null,null); 
	}			

	public Cursor getZoneByPhone(String phone_number) {
		return db.query(ZONES_TABLE_NAME, 
				new String[]{
					ZoneData.FIELD_ZONE_ID,
					ZoneData.FIELD_LATITUDE,
					ZoneData.FIELD_LONGITUDE,
					ZoneData.FIELD_DISTANCE
				}, ZoneData.FIELD_NUMBER + "='" + phone_number + "'",
				null, null, null,null); 
	}			
	
	public int updateZoneDistance(int zone_id,int distance)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ZoneData.FIELD_DISTANCE, distance);
		return db.update(ZONES_TABLE_NAME, val, ZoneData.FIELD_ZONE_ID + "=" + zone_id , null);
	}	
	
	public int updateResetZones(String phone_number)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(ZoneData.FIELD_LATITUDE, 0.0);
		val.put(ZoneData.FIELD_LONGITUDE, 0.0);
		val.put(ZoneData.FIELD_DISTANCE, 0);
		return db.update(ZONES_TABLE_NAME, val, ZoneData.FIELD_NUMBER + "='" + phone_number + "'", null);
	}
	
	
	// ------------
	// CTIME
	// ------------
	public long insertCTime(ContentValues args) {

		long result = db.insert(CTIME_TABLE_NAME, null, args);

		return result;
	}
	
	public long insertCTime(CTimeData CTimes) {

		return db.insert(CTIME_TABLE_NAME, null, CTimes.getContentValues());
	}
	
	public boolean existCTimeByPhone(String phone_number, Date d) {
		boolean rs = false;
		int ctime = ScoreUtils.getTimeUnit(d);
		
		Cursor c = db.query(CTIME_TABLE_NAME, 
				new String[]{
				CTimeData.FIELD_CTIME_ID
				}, 
				CTimeData.FIELD_NUMBER + "='" + phone_number + "' and " + CTimeData.FIELD_CTIME + "='" + String.valueOf(ctime) + "'" 
				, null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;
	}
	
	public Cursor getCTime() {
		return db.query(CTIME_TABLE_NAME, 
				new String[]{
					CTimeData.FIELD_CTIME_ID,
					CTimeData.FIELD_NUMBER,
					CTimeData.FIELD_CTIME,
					CTimeData.FIELD_CTIME_COUNT
				}, null,null, null, null,null); 
	}			

	public Cursor getCTimeByPhone(String phone_number) {
		return db.query(CTIME_TABLE_NAME, 
				new String[]{
					CTimeData.FIELD_CTIME_ID,
					CTimeData.FIELD_CTIME,
					CTimeData.FIELD_CTIME_COUNT
				}, CTimeData.FIELD_NUMBER + "='" + phone_number + "'",
				null, null, null,null); 
	}			
	
	public int updateCTimeCount(int ctime_id)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(CTimeData.FIELD_CTIME_COUNT, 1);
		return db.update(CTIME_TABLE_NAME, val, CTimeData.FIELD_CTIME_ID + "=" + ctime_id , null);
	}	
	
	public int updateResetCTime(String phone_number)
	{
		ContentValues val = new ContentValues();
		val.clear();
		val.put(CTimeData.FIELD_CTIME_COUNT, 0);
		return db.update(CTIME_TABLE_NAME, val, CTimeData.FIELD_NUMBER + "='" + phone_number + "'", null);
	}
	
	

	// --------
	// SMS LIST
	// --------
	
//	private boolean smsListChanged = true;

	public long insertSmsList(SmsListData sms_list){
//		smsListChanged = true;
		return db.insert(SMS_LIST_TABLE_NAME, null, sms_list.getContentValues());
	}

	public int updateSmsList(String phone_number, String user_name, String last_msg, int msg_type, int count)
	{
//		smsListChanged = true;
		long time = System.currentTimeMillis();
		ContentValues val = new ContentValues();
		val.clear();
		val.put(SmsListData.FIELD_LAST_DATE, time);
		val.put(SmsListData.FIELD_LAST_MSG, last_msg);
		val.put(SmsListData.FIELD_MSG_TYPE, msg_type);
		val.put(SmsListData.FIELD_USER_NAME, user_name);
		val.put(SmsListData.FIELD_NEW_MSG, count);
		return db.update(SMS_LIST_TABLE_NAME, val, SmsListData.FIELD_PHONE_NUMBER + "='" + phone_number + "'", null);		
	}
	
	//BJH
	public int updateMsgSmsList(String phone_number, String user_name, String last_msg, int msg_type, int count)//, long time)
	{
//		smsListChanged = true;
		ContentValues val = new ContentValues();
		val.clear();
		//val.put(SmsListData.FIELD_LAST_DATE, time);
		val.put(SmsListData.FIELD_LAST_MSG, last_msg);
		val.put(SmsListData.FIELD_MSG_TYPE, msg_type);
		val.put(SmsListData.FIELD_USER_NAME, user_name);
		val.put(SmsListData.FIELD_NEW_MSG, count);
		return db.update(SMS_LIST_TABLE_NAME, val, SmsListData.FIELD_PHONE_NUMBER + "='" + phone_number + "'", null);		
	}
	
	public Cursor getAllSmsList(int option) {
//		smsListChanged = false;
		//BJH orderStr 수정 푸시 메시지를 상단 배치
		String orderStr = null;
		switch (option) {
		case SmsListData.SORT_DATE_DESC:
			orderStr = SmsListData.FIELD_USER_TYPE + " DESC, " + SmsListData.FIELD_LAST_DATE + " DESC";
			break;
		case SmsListData.SORT_DATE_ASC:
			orderStr = SmsListData.FIELD_USER_TYPE + " DESC, " + SmsListData.FIELD_LAST_DATE + " ASC";
			break;
		case SmsListData.SORT_NAME_ASC:
			orderStr = SmsListData.FIELD_USER_TYPE + " DESC, " + SmsListData.FIELD_USER_NAME + " COLLATE LOCALIZED ASC";
			break;
		default:
			orderStr = null;
			break;
		}

		return db.query(SMS_LIST_TABLE_NAME, 
				new String[]{
				SmsListData.FIELD_ID,
				SmsListData.FIELD_USER_NAME,
				SmsListData.FIELD_PHONE_NUMBER,
				SmsListData.FIELD_MSG_TYPE,
				SmsListData.FIELD_NEW_MSG,
				SmsListData.FIELD_LAST_DATE,
				SmsListData.FIELD_LAST_MSG
				}, 
				null, null, 
				null, null, 
				orderStr);

	}		

	public boolean existSmsList(String phone_number) {
		boolean rs = false;
		Cursor c = db.query(SMS_LIST_TABLE_NAME, 
				new String[]{
				SmsListData.FIELD_ID,
				SmsListData.FIELD_PHONE_NUMBER,
				SmsListData.FIELD_MSG_TYPE,
				SmsListData.FIELD_NEW_MSG,
				SmsListData.FIELD_LAST_DATE,
				SmsListData.FIELD_LAST_MSG
				}, 
				SmsListData.FIELD_PHONE_NUMBER + "='" + phone_number + "'", null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = true;
		c.close();
		return rs;
	}
		
	
	public boolean deleteSmsList(String phone_number) {
//		smsListChanged = true;
		return db.delete(SMS_LIST_TABLE_NAME, SmsListData.FIELD_PHONE_NUMBER + "='" + phone_number + "'", null) > 0;
	}
	
	
	public boolean deleteAllSmsList() {
//		smsListChanged = true;
		return db.delete(SMS_LIST_TABLE_NAME, null, null) > 0;
	}			
	
	
	// --------
	// SMS MSG
	// --------
	
//	private boolean smsMsgChanged = true;

	public long insertSmsMsg(SmsMsgData sms_msg){
//		smsMsgChanged = true;
		long seq = db.insert(SMS_MSG_TABLE_NAME, null, sms_msg.getContentValues());
		sms_msg.msgSeq = seq;
		return seq;
	}

	public Cursor getAllSmsMsg(String order,int option) {
//		smsMsgChanged = false;
		
		String where = SmsMsgData.FIELD_MSG_TYPE + "=";
		
		// Set the icon
		switch (option) {
		case SmsMsgData.RECEIVE_TYPE:
		case SmsMsgData.SEND_TYPE:
		case SmsMsgData.RESERVED_TYPE:	
			where += Integer.toString(option);
			break;
		default:
			where = null;
			break;
		}
		
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
					SmsMsgData.FIELD_ID,
					SmsMsgData.FIELD_MSG_SEQ,
					SmsMsgData.FIELD_FROM,
					SmsMsgData.FIELD_TO,
					SmsMsgData.FIELD_CALLBACK,
					SmsMsgData.FIELD_MSG_TYPE,
					SmsMsgData.FIELD_DATE,
					SmsMsgData.FIELD_NEW,
					SmsMsgData.FIELD_MSG,				
					SmsMsgData.FIELD_SUBJECT,
					SmsMsgData.FIELD_FILE1,
					SmsMsgData.FIELD_FILE2,
					SmsMsgData.FIELD_FILE3,
				}, 
				where, null, 
				null, null, 
				SmsMsgData.FIELD_DATE + " "+ order + ", " + SmsMsgData.FIELD_MSG_SEQ + " " + order);
	}		
	
	public Cursor getSmsMsg(String phone_number) {
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_MSG_SEQ,
				SmsMsgData.FIELD_FROM,
				SmsMsgData.FIELD_TO,
				SmsMsgData.FIELD_CALLBACK,
				SmsMsgData.FIELD_MSG_TYPE,
				SmsMsgData.FIELD_DATE,
				SmsMsgData.FIELD_NEW,
				SmsMsgData.FIELD_MSG,
				SmsMsgData.FIELD_SUBJECT,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3,
				
				}, SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "'",
				null, null, null,SmsMsgData.FIELD_DATE + " ASC, " + SmsMsgData.FIELD_MSG_SEQ + " ASC"); 
	}	

	public int getSmsMsgCount(String phone_number) {
		int rs = -1;
		Cursor c =  db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID
				}, "(" + SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "') AND " + SmsMsgData.FIELD_MSG_TYPE + " > 0",
				null, null, null, null); 
		
		if(c != null)
		{
			rs = c.getCount();
			c.close();
		}
		
		return rs;
	}		
	
	public int getSmsNewMsgCount(String phone_number) {
		int rs = -1;
		Cursor c = null;  
				
		if (phone_number != null && phone_number.length() > 0)
		{
			c = db.query(SMS_MSG_TABLE_NAME, 
					new String[]{
					SmsMsgData.FIELD_ID
					}, "(" + SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "') AND " + SmsMsgData.FIELD_MSG_TYPE + " > 0 AND " + SmsMsgData.FIELD_NEW + " <> 0",
					null, null, null, null);
		}
		else
		{
			c = db.query(SMS_MSG_TABLE_NAME, 
					new String[]{
					SmsMsgData.FIELD_ID
					}, SmsMsgData.FIELD_MSG_TYPE + " > 0 AND " + SmsMsgData.FIELD_NEW + " <> 0",
					null, null, null, null);
		}
		
		if(c != null)
		{
			rs = c.getCount();
			c.close();
		}
		
		return rs;
	}

	public Cursor getMoreSmsMsg(String phone_number) {
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_MSG_SEQ,
				SmsMsgData.FIELD_FROM,
				SmsMsgData.FIELD_TO,
				SmsMsgData.FIELD_CALLBACK,
				SmsMsgData.FIELD_MSG_TYPE,
				SmsMsgData.FIELD_DATE,
				SmsMsgData.FIELD_NEW,
				SmsMsgData.FIELD_MSG,
				SmsMsgData.FIELD_SUBJECT,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3
				
				}, "(" + SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "') AND " + SmsMsgData.FIELD_NEW + "= 1",
				null, null, null,SmsMsgData.FIELD_DATE + " ASC, " + SmsMsgData.FIELD_MSG_SEQ + " ASC"); 
	}	
	
	public Cursor getLastSmsRecvMsg() {
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_MSG_SEQ,
				SmsMsgData.FIELD_FROM,
				SmsMsgData.FIELD_TO,
				SmsMsgData.FIELD_CALLBACK,
				SmsMsgData.FIELD_MSG_TYPE,
				SmsMsgData.FIELD_DATE,
				SmsMsgData.FIELD_NEW,
				SmsMsgData.FIELD_MSG,
				SmsMsgData.FIELD_SUBJECT,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3
				
				}, SmsMsgData.FIELD_MSG_TYPE + "= 1",
				null, null, null,SmsMsgData.FIELD_DATE + " DESC, " + SmsMsgData.FIELD_MSG_SEQ + " DESC LIMIT 1"); 
	}
	
	public Cursor getLastPushRecvMsg() {//BJH LAST PUSH
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_MSG_SEQ,
				SmsMsgData.FIELD_FROM,
				SmsMsgData.FIELD_TO,
				SmsMsgData.FIELD_CALLBACK,
				SmsMsgData.FIELD_MSG_TYPE,
				SmsMsgData.FIELD_DATE,
				SmsMsgData.FIELD_NEW,
				SmsMsgData.FIELD_MSG,
				SmsMsgData.FIELD_SUBJECT,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3
				
				}, SmsMsgData.FIELD_CALLBACK + "= 'PUSH'",
				null, null, null,SmsMsgData.FIELD_DATE + " DESC LIMIT 1"); 
	}		
		
	public Cursor getLastSmsMsg(String phone_number) {
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_MSG_SEQ,
				SmsMsgData.FIELD_FROM,
				SmsMsgData.FIELD_TO,
				SmsMsgData.FIELD_CALLBACK,
				SmsMsgData.FIELD_MSG_TYPE,
				SmsMsgData.FIELD_DATE,
				SmsMsgData.FIELD_NEW,
				SmsMsgData.FIELD_MSG,
				SmsMsgData.FIELD_SUBJECT,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3
				
				}, SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "'",
				null, null, null,SmsMsgData.FIELD_DATE + " DESC, " + SmsMsgData.FIELD_MSG_SEQ + " DESC LIMIT 1"); 
	}		
	
	public int updateSmsMsgFile(SmsMsgData sms_msg){
		ContentValues val = new ContentValues();
		val.clear();
		val.put(SmsMsgData.FIELD_FILE1, sms_msg.file1);
		val.put(SmsMsgData.FIELD_FILE2, sms_msg.file2);
		val.put(SmsMsgData.FIELD_FILE3, sms_msg.file3);
		db.update(SMS_MSG_TABLE_NAME, val, SmsMsgData.FIELD_ID + "='" + sms_msg.msgSeq + "'", null);
		return 0;
	}	
	
	
	public int updateReadSmsMsg(String phone_number)
	{
		// RESET MSG
		ContentValues val = new ContentValues();
		val.clear();
		val.put(SmsMsgData.FIELD_NEW, 0);
		db.update(SMS_MSG_TABLE_NAME, val, SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "'", null);
		
		// RESET LIST
		val.clear();
		val.put(SmsListData.FIELD_NEW_MSG, 0);
		db.update(SMS_LIST_TABLE_NAME, val, SmsListData.FIELD_PHONE_NUMBER + "='" + phone_number + "'", null);
		
		return 0;
	}
	
	public boolean deleteSmsMsg(String phone_number) {
		return db.delete(SMS_MSG_TABLE_NAME, SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "'", null) > 0;
	}

	public boolean deleteSmsMsg(int id) {
		return db.delete(SMS_MSG_TABLE_NAME, SmsMsgData.FIELD_ID + "=" + id , null) > 0;
	}
	
	public boolean deleteAllSmsMsg() {
		return db.delete(SMS_MSG_TABLE_NAME, null, null) > 0;
	}			
	
	public boolean isTodayFirstMsg(String phone_number, String msg) {
		boolean rs = true;
		Cursor c = db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID
				}, 
				 SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "' AND " + SmsMsgData.FIELD_MSG + "='" + msg + "'" , null, null, null, null);

		if(c == null) return rs;
		
		if(c.getCount() > 0) rs = false;
		c.close();
		return rs;
	}
	//BJH 2016.11.01
	public Cursor getSmsMsgImg(String phone_number) {
		return db.query(SMS_MSG_TABLE_NAME, 
				new String[]{
				SmsMsgData.FIELD_ID,
				SmsMsgData.FIELD_FILE1,
				SmsMsgData.FIELD_FILE2,
				SmsMsgData.FIELD_FILE3,
				
				}, SmsMsgData.FIELD_FROM + "='" + phone_number + "' OR " + SmsMsgData.FIELD_TO + "='" + phone_number + "'",
				null, null, null,SmsMsgData.FIELD_DATE + " ASC, " + SmsMsgData.FIELD_MSG_SEQ + " ASC"); 
	}	
	
	// id + seq 조합으로 약속된 경로에 있음.
//	public int updateMediaPath(int id, int seq, String path)
//	{
//		ContentValues val = new ContentValues();
//		if (seq == 1)
//			val.put(SmsMsgData.FIELD_FILE1, path);
//		else if (seq == 2)
//			val.put(SmsMsgData.FIELD_FILE2, path);
//		else if (seq == 3)
//			val.put(SmsMsgData.FIELD_FILE3, path);		
//		return db.update(SMS_MSG_TABLE_NAME, val, SmsMsgData.FIELD_ID + "=" + id, null);		
//	}	
	
	//BJH 2016.11.09
	public long insertPushRecord(PushRecordData push_record_data){
		removeExpiredPushRecord(MAX_PUSH_RECORD_COUNT-1);
		return db.insert(PUSH_RECORD_NAME, null, push_record_data.getContentValues());
	}
	
	public void removeExpiredPushRecord(int offset) 
	{
		db.delete(PUSH_RECORD_NAME, PushRecordData.FIELD_PID + " IN " +
			"(SELECT "+ PushRecordData.FIELD_PID + " FROM " + PUSH_RECORD_NAME + " ORDER BY " + 
			PushRecordData.FIELD_PID + " DESC" + " LIMIT -1 OFFSET " + offset + ")", null);
	}
	
	public int updatePushRecord(long push_time, String action)
	{
		ContentValues val = new ContentValues();
		val.clear();
		long response_time = System.currentTimeMillis();
		val.put(PushRecordData.FIELD_ACTION, action);
		val.put(PushRecordData.FIELD_RESPONSE_TIME, response_time);
		return db.update(PUSH_RECORD_NAME, val, PushRecordData.FIELD_PUSH_TIME + "='" + push_time + "'", null);
	}	
	
	public Cursor exportPushRecord() {
		removeExpiredPushRecord(MAX_PUSH_RECORD_COUNT);
		return db.rawQuery("SELECT * FROM " + PUSH_RECORD_NAME, null);
	}
	
	public int getPushRecordCount() {
		Cursor c = db.query(PUSH_RECORD_NAME, 
				new String[]{
					PushRecordData.FIELD_PID
				}, 
				null 
				, null, null, null, PushRecordData.FIELD_PID + " DESC LIMIT 1");

		if(c == null) return 0;
		
		return c.getCount();
	}

	public long insertTravelStep(TravelStepData data){
		long rowID;
		try {
			rowID=db.insertOrThrow(TRAVEL_STEP_TABLE_NAME, null, data.getContentValues());
		} catch (SQLException e) {
			Log.e(THIS_FILE,"error:"+e.getLocalizedMessage());
			return -1;
		}

		return rowID;
	}

	/**
	 *
	 * ContentValues val = new ContentValues();
	 * 		val.clear();
	 * 		val.put(RecentCallsData.FIELD_CALL_MEMO, memo);
	 * 		return db.update(RECENTS_TABLE_NAME, val, RecentCallsData.FIELD_PK + "=" + pk, null);
	 */

	public long insertTravelCategory(TravelCategoryData data){
		long rowID=-1;
		if (data!=null){
			String categoryNum=data.getCategory_num();
			Cursor cursor= isTravelCategoryData(categoryNum);
			if (cursor!=null && cursor.getCount()>0){
				cursor.moveToFirst();
				String path=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_PATH));
				//int photoCount=cursor.getInt(cursor.getColumnIndex(TravelCategoryData.FIELD_PHOTO_COUNT));
				/*if (data.getPath()!=null && data.getPath().length()>0){
					if (data.getPath().contains("######")){
						String imgArray[]=data.getPath().split("######");
						if (imgArray!=null && imgArray.length>0){
							for (String sPath: imgArray) {
								photoCount++;
							}
						}
					}else {
						photoCount++;
					}
				}*/

				ContentValues val = new ContentValues();
				val.clear();
				//val.put(TravelCategoryData.FIELD_PATH, path+"######"+data.getPath());
				val.put(TravelCategoryData.FIELD_PATH, data.getPath());
				val.put(TravelCategoryData.FIELD_PHOTO_COUNT, data.getPhoto_count());
				val.put(TravelCategoryData.FIELD_ENDDATE,data.getEnd_date());
				db.update(TRAVEL_CATEGORY_TABLE_NAME, val, TravelCategoryData.FIELD_CATEGORY_NUM + "='" + categoryNum+"'" +
						"", null);
			}else {
				try {
					rowID=db.insert(TRAVEL_CATEGORY_TABLE_NAME, null, data.getContentValues());
				} catch (SQLException e) {
					Log.e(THIS_FILE,"error:"+e.getLocalizedMessage());
					return -1;
				}
			}
		}




		return rowID;
	}

	public Cursor getTravelCategoryData() {
		return db.rawQuery("SELECT * FROM " + TRAVEL_CATEGORY_TABLE_NAME +" ORDER BY "+TravelCategoryData.FIELD_STARTDATE+" DESC, "+TravelCategoryData.FIELD_PK+" DESC;", null);
	}

	public Cursor isTravelCategoryData(String category_num) {
		return db.rawQuery("SELECT * FROM " + TRAVEL_CATEGORY_TABLE_NAME +" WHERE "+TravelCategoryData.FIELD_CATEGORY_NUM+"='"+category_num+"' ORDER BY "+TravelCategoryData.FIELD_STARTDATE+" DESC, "+TravelCategoryData.FIELD_PK+" DESC LIMIT 1", null);
	}

	public Cursor getTravelCategoryRecentLastCategoryNumData() {
		return db.rawQuery("SELECT "+TravelCategoryData.FIELD_CATEGORY_NUM+" FROM " + TRAVEL_CATEGORY_TABLE_NAME +" ORDER BY "+TravelCategoryData.FIELD_STARTDATE+" DESC,"+TravelCategoryData.FIELD_PK+" DESC LIMIT 1", null);
	}

	public Cursor getTravelStepData(String category_num) {
		return db.rawQuery("SELECT * FROM " + TRAVEL_STEP_TABLE_NAME +" WHERE "+TravelStepData.FIELD_CATEGORY_NUM+"='"+category_num+"' ORDER BY "+TravelStepData.FIELD_DATE+" ASC", null);
	}

	public Cursor getTravelStepRecentLastData(String category_num) {
		return db.rawQuery("SELECT * FROM " + TRAVEL_STEP_TABLE_NAME +" WHERE "+TravelStepData.FIELD_CATEGORY_NUM+"='"+category_num+"' ORDER BY "+TravelStepData.FIELD_DATE+" DESC,"+TravelStepData.FIELD_PK+" DESC "+" LIMIT 1", null);
	}

	public void dropTravelData(){
		db.execSQL(TABLE_TRAVEL_CATEGORY_DROP);
		db.execSQL(TABLE_TRAVEL_STEP_DROP);
	}

	public void createTravelTable(){
		db.execSQL(TABLE_TRAVEL_CATEGORY);
		db.execSQL(TABLE_TRAVEL_STEP);
	}
}
