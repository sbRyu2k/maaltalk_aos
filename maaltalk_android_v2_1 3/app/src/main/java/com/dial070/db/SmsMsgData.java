package com.dial070.db;

import android.content.ContentValues;

public class SmsMsgData {
	public static String FIELD_ID = "_id";		//
	public static String FIELD_MSG_SEQ = "msgSeq";	// 서버에서 관리하는 SEQ
	public static String FIELD_MSG_TYPE = "msgType";	//0:헤더 1:receive 2:send 3:예약메세지
	public static String FIELD_FROM = "msgFrom";
	public static String FIELD_TO = "msgTo";
	public static String FIELD_CALLBACK = "callBack";   //회신번호 
	public static String FIELD_DATE = "date";   
	public static String FIELD_NEW = "msgNew";
	public static String FIELD_MSG = "message";	
	
	public static String FIELD_SUBJECT = "subject";
	public static String FIELD_FILE1 = "file1";
	public static String FIELD_FILE2 = "file2";
	public static String FIELD_FILE3 = "file3";
	
	public static String FIELD_FIELD1 = "field1";
	public static String FIELD_FIELD2 = "field2";
	public static String FIELD_FIELD3 = "field3";
	public static String FIELD_FIELD4 = "field4";
	

	public static final int HEADER_TYPE 	= 0;
	public static final int RECEIVE_TYPE 	= 1;
	public static final int SEND_TYPE 		= 2;
	public static final int RESERVED_TYPE 	= 3;
	public static final int MSG_NEW		 	= 1;
	
	public long msgSeq;
	public String msgFrom;
	public String msgTo;
	public String msgCallBack;
	public int msgNew;
	public int msgType;
	public String msgSubject;
	public String msgText;
	public long msgTime;
	
	public String file1;
	public String file2;
	public String file3;
	
		
	public SmsMsgData(int aSeq, String aFrom, String aTo, String aCallBack, int aType, int aNew, String aSubject, String aMessage, long aTime)
	{
		msgSeq			= aSeq;
		msgFrom			= aFrom;
		msgTo			= aTo;
		msgCallBack     = aCallBack;
		msgType 		= aType;
		msgNew 			= aNew;
		msgSubject		= aSubject;
		msgText			= aMessage;		
		msgTime			= aTime;
	}
	
	public SmsMsgData(ContentValues cv) {
		msgSeq			= cv.getAsInteger(FIELD_MSG_SEQ);
		msgFrom			= cv.getAsString(FIELD_FROM);
		msgTo			= cv.getAsString(FIELD_TO);
		msgCallBack		= cv.getAsString(FIELD_CALLBACK);
		msgType 		= cv.getAsInteger(FIELD_MSG_TYPE);
		msgNew 			= cv.getAsInteger(FIELD_NEW);
		msgSubject		= cv.getAsString(FIELD_SUBJECT);
		msgText			= cv.getAsString(FIELD_MSG);
		msgTime			= cv.getAsInteger(FIELD_DATE);

		// media
		file1			= cv.getAsString(FIELD_FILE1);
		file2			= cv.getAsString(FIELD_FILE2);
		file3			= cv.getAsString(FIELD_FILE3);
	}

	public void setMediaValues(String aFile1, String aFile2, String aFile3) {
		file1 = aFile1;
		file2 = aFile2;
		file3 = aFile3;
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_MSG_SEQ, msgSeq);
		cv.put(FIELD_FROM, msgFrom);
		cv.put(FIELD_TO, msgTo);
		cv.put(FIELD_CALLBACK, msgCallBack);
		cv.put(FIELD_MSG_TYPE, msgType);
		cv.put(FIELD_NEW, msgNew);
		cv.put(FIELD_SUBJECT, msgSubject);
		cv.put(FIELD_MSG, msgText);
		cv.put(FIELD_DATE, msgTime);
		
		cv.put(FIELD_FILE1, file1);
		cv.put(FIELD_FILE2, file2);
		cv.put(FIELD_FILE3, file3);
		
		return cv;
	}		
}



