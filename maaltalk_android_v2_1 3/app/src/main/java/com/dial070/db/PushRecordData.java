package com.dial070.db;

import android.content.ContentValues;

public class PushRecordData {
	public static String FIELD_PID = "pid";	
	public static String FIELD_PUSH_TIME = "push_time";	
	public static String FIELD_RECEIVE_TIME = "receive_time";	
	public static String FIELD_RESPONSE_TIME = "response_time";
	public static String FIELD_PUSH_TYPE = "push_type";
	public static String FIELD_PUSH_VALUE = "push_value"; 
	public static String FIELD_CID = "cid";   
	public static String FIELD_USERID = "userid";
	public static String FIELD_PHONE_NUMBER = "phone_number";		
	public static String FIELD_ACTION = "action";

	public static final String PUSH_RECORD_GCM = "GCM";
	public static final String PUSH_RECORD_PUSHY = "PUSHY";
	
	public long mPushTime;
	public long mReceiveTime;
	public long mResponseTime;
	public String mPushType;
	public String mPushValue;
	public String mCid;
	public String mUserId;
	public String mPhoneNumber;
	public String mAction;
		
	public PushRecordData(long aPushTime, long aReceiveTime, long aResponseTime, String aPushType, String aPushValue, String aCid, String aUserId, String aPhoneNumber, String aAction)
	{
		mPushTime = aPushTime;
		mReceiveTime = aReceiveTime;
		mResponseTime = aResponseTime;
		mPushType = aPushType;
		mPushValue = aPushValue;
		mCid = aCid;
		mUserId = aUserId;
		mPhoneNumber = aPhoneNumber;
		mAction = aAction;
	}
	
	public PushRecordData(ContentValues cv) {
		mPushTime = cv.getAsLong(FIELD_PUSH_TIME);
		mReceiveTime = cv.getAsLong(FIELD_RECEIVE_TIME);
		mResponseTime = cv.getAsLong(FIELD_RESPONSE_TIME);
		mPushType =  cv.getAsString(FIELD_PUSH_TYPE);
		mPushValue = cv.getAsString(FIELD_PUSH_VALUE);
		mCid = cv.getAsString(FIELD_CID);
		mUserId = cv.getAsString(FIELD_USERID);
		mPhoneNumber = cv.getAsString(FIELD_PHONE_NUMBER);
		mAction = cv.getAsString(FIELD_ACTION);
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_PUSH_TIME, mPushTime);
		cv.put(FIELD_RECEIVE_TIME, mReceiveTime);
		cv.put(FIELD_RESPONSE_TIME, mResponseTime);
		cv.put(FIELD_PUSH_TYPE, mPushType);
		cv.put(FIELD_PUSH_VALUE, mPushValue);
		cv.put(FIELD_CID, mCid);
		cv.put(FIELD_USERID, mUserId);
		cv.put(FIELD_PHONE_NUMBER, mPhoneNumber);
		cv.put(FIELD_ACTION, mAction);
		
		return cv;
	}		
}



