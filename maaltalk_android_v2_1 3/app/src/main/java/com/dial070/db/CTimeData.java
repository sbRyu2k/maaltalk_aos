package com.dial070.db;

import java.util.Date;

import com.dial070.utils.ScoreUtils;

import android.content.ContentValues;

public class CTimeData {

	public static String FIELD_NUMBER = "number";
	public static String FIELD_CTIME_ID = "ctime_id";	
	public static String FIELD_CTIME = "ctime";		
	public static String FIELD_CTIME_COUNT = "ctime_count";
	
	private String number;
	private int ctime;
	private int ctime_count;
	
	public CTimeData(ContentValues cv) {
		this.number = cv.getAsString(FIELD_NUMBER);
		this.ctime = cv.getAsInteger(FIELD_CTIME);
		this.ctime_count = cv.getAsInteger(FIELD_CTIME_COUNT);
	}		
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_NUMBER, this.number);
		cv.put(FIELD_CTIME, this.ctime);
		cv.put(FIELD_CTIME_COUNT, this.ctime_count);
		return cv;
	}		
	
	public CTimeData(String number, Date d)
	{
		this.number = number;
		this.ctime = ScoreUtils.getTimeUnit(d);
		this.ctime_count = 1;
	}	
}
