package com.dial070.db;

import android.content.ContentValues;

public class TravelCategoryData {

	public static String FIELD_PK = "_id";
	public static String FIELD_PATH = "path";
	public static String FIELD_THUMBNAIL_PATH = "thumbnail_path";
	public static String FIELD_COUNTRY_NAME = "country_name";
	public static String FIELD_COUNTRY_CODE = "country_code";
	public static String FIELD_STARTDATE = "start_date";
	public static String FIELD_ENDDATE = "end_date";
	public static String FIELD_CATEGORY_NUM = "category_num";
	public static String FIELD_PHOTO_COUNT = "photo_count";


	private int _id;
	private String path;
	private String thumbnail_path;
	private String country_name;
	private String country_code;
	private String start_date;
	private String end_date;
	private String category_num;
	private int photo_count;

	public TravelCategoryData() {

	}

	public TravelCategoryData(ContentValues cv) {
		_id 			= cv.getAsInteger(FIELD_PK);
		path 	= cv.getAsString(FIELD_PATH);
		thumbnail_path 	= cv.getAsString(FIELD_THUMBNAIL_PATH);
		country_name 	= cv.getAsString(FIELD_COUNTRY_NAME);
		country_code 	= cv.getAsString(FIELD_COUNTRY_CODE);
		start_date 	= cv.getAsString(FIELD_STARTDATE);
		end_date 	= cv.getAsString(FIELD_ENDDATE);
		category_num 	= cv.getAsString(FIELD_CATEGORY_NUM);
		photo_count 			= cv.getAsInteger(FIELD_PHOTO_COUNT);
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_PATH, path);
		cv.put(FIELD_THUMBNAIL_PATH, thumbnail_path);
		cv.put(FIELD_COUNTRY_NAME, country_name);
		cv.put(FIELD_COUNTRY_CODE, country_code);
		cv.put(FIELD_STARTDATE, start_date);
		cv.put(FIELD_ENDDATE, end_date);
		cv.put(FIELD_CATEGORY_NUM, category_num);
		cv.put(FIELD_PHOTO_COUNT, photo_count);
		return cv;
	}

	public int get_id() {
		return _id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getThumbnail_path() {
		return thumbnail_path;
	}

	public void setThumbnail_path(String thumbnail_path) {
		this.thumbnail_path = thumbnail_path;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getCategory_num() {
		return category_num;
	}

	public void setCategory_num(String category_num) {
		this.category_num = category_num;
	}

	public int getPhoto_count() {
		return photo_count;
	}

	public void setPhoto_count(int photo_count) {
		this.photo_count = photo_count;
	}
}
