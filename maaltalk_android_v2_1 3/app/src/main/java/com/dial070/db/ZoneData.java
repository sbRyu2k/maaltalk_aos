package com.dial070.db;

import android.content.ContentValues;

public class ZoneData {

	public static String FIELD_NUMBER = "number";
	public static String FIELD_ZONE_ID = "zone_id";	
	public static String FIELD_LATITUDE = "latitude";	
	public static String FIELD_LONGITUDE = "longitude";		
	public static String FIELD_DISTANCE = "distance";		
	
	private String number;

	private double latitude,longitude;
	private int distance;
	
	public ZoneData(ContentValues cv) {
		this.number = cv.getAsString(FIELD_NUMBER);
		//this.zone_id = cv.getAsInteger(FIELD_ZONE_ID);
		this.latitude = cv.getAsDouble(FIELD_LATITUDE);
		this.longitude = cv.getAsDouble(FIELD_LONGITUDE);
		this.distance = cv.getAsInteger(FIELD_DISTANCE);
	}		
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_NUMBER, this.number);
		cv.put(FIELD_DISTANCE, this.distance);
		cv.put(FIELD_LATITUDE, this.latitude);
		cv.put(FIELD_LONGITUDE, this.longitude);
		return cv;
	}		
	
	
	public ZoneData(String number, double latitude, double longitude,int distance)
	{
		this.number = number;
		this.latitude = latitude;
		this.longitude = longitude;
		this.distance = distance;
	}	
}
