package com.dial070.db;

import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.models.CallerInfo;

import android.content.ContentValues;
import android.content.Context;

public class FavoritesData {

	public static String FIELD_PK = "_pk";						//즐겨찾기 GridView 위치 인덱스 
	public static String FIELD_DISPLAYNAME = "displayName";
	public static String FIELD_PHONENUMBER = "phoneNumber";
	public static String FIELD_UID = "uid";
	public static String FIELD_IDENTIFIER = "identifier"; 		// 포토 아이디를 저
	public static String FIELD_SEARCH_NUMBER = "searchNumber";	// 전화번호 검색을 위해 nomalize된 번호를 넣는다.
	public static String FIELD_DENSITY = "desity";	
	public static String FIELD_FIELD1 = "FIELD1";				//자동등록/수동등록 필드로 사용 : "AUTO", "MANUAL"
	public static String FIELD_FIELD2 = "FIELD2";				//메모 필드로 사용
	
	/*
	CREATE TABLE "favorites" (
	"fid" INTEGER PRIMARY KEY  NOT NULL ,
	"displayName" CHAR(48),
	"phoneNumber" CHAR(24),
	"uid" INTEGER,
	"identifier" INTEGER, 
	"FIELD1" VARCHAR, 
	"FIELD2" VARCHAR, 
	"FIELD3" VARCHAR, 
	"FIELD4" VARCHAR)	
	 */	
	
	//private int fid;
	private String display_name;
	private String phone_number;
	private String field1;
	private String field2;	
	private String search_number;		
	private String uid;
	private String identifier;
	private int desity;

	
	public static ContentValues getContentValues(Context context, SipCallSession call, long callStart) {
		ContentValues cv = new ContentValues();
		
		String remoteContact = call.getRemoteContact();
		String phone_number;
		
		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);
		if(callerInfo != null) 
		{
			String remoteName = callerInfo.name;
			if (remoteName == null || remoteName.length() == 0) remoteName = callerInfo.phoneNumber;
			
			phone_number = callerInfo.phoneNumber;
			
			if (remoteName.equals(phone_number))
			{
				//phone_number = phone_number.replaceAll("\\+82", "0");  // BJH 2017.01.17 아이폰과 동일하도록
				phone_number = phone_number.replaceAll("\\-", "");				
				cv.put(FIELD_DISPLAYNAME, phone_number);
				cv.put(FIELD_PHONENUMBER, phone_number);
				cv.put(FIELD_SEARCH_NUMBER,phone_number);			
				
			}
			else
			{
				cv.put(FIELD_DISPLAYNAME, 	remoteName);
				
				//phone_number = phone_number.replaceAll("\\+82", "0"); // BJH 2017.01.17 아이폰과 동일하도록
				phone_number = phone_number.replaceAll("\\-", "");
				cv.put(FIELD_PHONENUMBER, 	phone_number);
				cv.put(FIELD_SEARCH_NUMBER,	phone_number);
			}
		}
		else
		{
			phone_number = remoteContact;
			//phone_number = phone_number.replaceAll("\\+82", "0"); // BJH 2017.01.17 아이폰과 동일하도록
			phone_number = phone_number.replaceAll("\\-", "");
			
			cv.put(FIELD_DISPLAYNAME, phone_number);
			cv.put(FIELD_PHONENUMBER, phone_number);
			cv.put(FIELD_SEARCH_NUMBER,phone_number);
		}
		
		cv.put(FIELD_UID, "");
		cv.put(FIELD_IDENTIFIER, "");
		cv.put(FIELD_DENSITY, 1);
		
		cv.put(FIELD_FIELD1, "AUTO");
		cv.put(FIELD_FIELD2, "");
		
		return cv;
	}		
	
	
	public FavoritesData(String aDisplayName, String aPhoneNumber, String aUid, String aIdentifier, String aSearchNumber, String aFiled1, String aFiled2, int aDesity) {
		//fid = aFid;
		display_name = aDisplayName;
		phone_number = aPhoneNumber;
		uid = aUid;
		identifier = aIdentifier;
		desity = aDesity;
		field1 =  aFiled1;
		field2 =  aFiled2;
		search_number =  aSearchNumber;		
	}	
	
	public FavoritesData(ContentValues cv) {
		//fid				= cv.getAsInteger(FIELD_FID);
		display_name 	= cv.getAsString(FIELD_DISPLAYNAME);
		phone_number 	= cv.getAsString(FIELD_PHONENUMBER);
		uid 			= cv.getAsString(FIELD_UID);
		identifier 		= cv.getAsString(FIELD_IDENTIFIER);
		desity			= cv.getAsInteger(FIELD_DENSITY);
		field1 			= cv.getAsString(FIELD_FIELD1);
		field2 			= cv.getAsString(FIELD_FIELD2);
		search_number 			= cv.getAsString(FIELD_SEARCH_NUMBER);			
	}
	
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		//cv.put(FIELD_FID, fid);
		cv.put(FIELD_DISPLAYNAME, display_name);
		cv.put(FIELD_PHONENUMBER, phone_number);
		cv.put(FIELD_UID, uid);
		cv.put(FIELD_IDENTIFIER, identifier);
		cv.put(FIELD_DENSITY, desity);
		cv.put(FIELD_FIELD1, field1);
		cv.put(FIELD_FIELD2, field2);
		cv.put(FIELD_SEARCH_NUMBER, search_number);
		return cv;
	}	
}
