package com.dial070.db;

import android.content.ContentValues;
import android.content.Context;

import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.models.CallerInfo;
import com.dial070.utils.AppPrefs;

public class TravelStepData {

	public static String FIELD_PK = "_id";
	public static String FIELD_PATH = "path";
	public static String FIELD_URI_PATH = "uri_path";
	public static String FIELD_THUMBNAIL_PATH = "thumbnail_path";
	public static String FIELD_LATITUDE = "latitude";
	public static String FIELD_LONGITUDE = "longitude";
	public static String FIELD_COUNTRY_NAME = "country_name";
	public static String FIELD_COUNTRY_CODE = "country_code";
	public static String FIELD_ADDRESS = "address";
	public static String FIELD_DATE = "origin_date";
	public static String FIELD_SIMPLE_DATE = "simple_date";
	public static String FIELD_CATEGORY_NUM = "category_num";


	private int _id;
	private String path;
	private String uri_path;
	private String thumbnail_path;
	private double latitude;
	private double longitude;
	private String country_name;
	private String country_code;
	private String address;
	private String origin_date;
	private String simple_date;
	private String category_num;

	public TravelStepData() {

	}

	public TravelStepData(ContentValues cv) {
		_id 			= cv.getAsInteger(FIELD_PK);
		path 	= cv.getAsString(FIELD_PATH);
		uri_path 	= cv.getAsString(FIELD_URI_PATH);
		thumbnail_path 	= cv.getAsString(FIELD_THUMBNAIL_PATH);
		country_name 	= cv.getAsString(FIELD_COUNTRY_NAME);
		country_code 	= cv.getAsString(FIELD_COUNTRY_CODE);
		address 	= cv.getAsString(FIELD_ADDRESS);
		origin_date 	= cv.getAsString(FIELD_DATE);
		simple_date 	= cv.getAsString(FIELD_SIMPLE_DATE);
		category_num 	= cv.getAsString(FIELD_CATEGORY_NUM);

		latitude 		= cv.getAsDouble(FIELD_LATITUDE);
		longitude 		= cv.getAsDouble(FIELD_LONGITUDE);
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_PATH, path);
		cv.put(FIELD_URI_PATH, uri_path);
		cv.put(FIELD_THUMBNAIL_PATH, thumbnail_path);
		cv.put(FIELD_COUNTRY_NAME, country_name);
		cv.put(FIELD_COUNTRY_CODE, country_code);
		cv.put(FIELD_ADDRESS, address);
		cv.put(FIELD_DATE, origin_date);
		cv.put(FIELD_SIMPLE_DATE, simple_date);
		cv.put(FIELD_CATEGORY_NUM, category_num);
		cv.put(FIELD_LATITUDE, latitude);
		cv.put(FIELD_LONGITUDE, longitude);
		return cv;
	}

	public int get_id() {
		return _id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUri_path() {
		return uri_path;
	}

	public void setUri_path(String uri_path) {
		this.uri_path = uri_path;
	}

	public String getThumbnail_path() {
		return thumbnail_path;
	}

	public void setThumbnail_path(String thumbnail_path) {
		this.thumbnail_path = thumbnail_path;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrigin_date() {
		return origin_date;
	}

	public void setOrigin_date(String origin_date) {
		this.origin_date = origin_date;
	}

	public String getSimple_date() {
		return simple_date;
	}

	public void setSimple_date(String simple_date) {
		this.simple_date = simple_date;
	}

	public String getCategory_num() {
		return category_num;
	}

	public void setCategory_num(String category_num) {
		this.category_num = category_num;
	}
}
