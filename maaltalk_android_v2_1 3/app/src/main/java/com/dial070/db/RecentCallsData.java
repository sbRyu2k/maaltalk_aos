package com.dial070.db;

import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.models.CallerInfo;
import com.dial070.utils.AppPrefs;

import android.content.ContentValues;
import android.content.Context;

public class RecentCallsData {
	
	public static String FIELD_PK = "_id";		 
	public static String FIELD_COMPOSITE_NAME = "compositeName";
	public static String FIELD_DATE = "callDate";
	public static String FIELD_PHONENUMBER = "phoneNumber";
	public static String FIELD_TYPE = "type";			//1:Incomming, 2: Outgoing, 3:Missed
	public static String FIELD_IDENTIFIER = "identifier";
	public static String FIELD_DURATION = "duration";
	public static String FIELD_HIDDEN = "hidden";		//0:show, 1:hidden
	public static String FIELD_CALL_TYPE = "callType"; // 0:normal, 1:internal, 2:vms, 3:qa , 4:sms
	public static String FIELD_CALL_MEMO = "callMemo";
	public static String FIELD_CALL_REC = "callRec"; // REC URL
	
	public static String FIELD_FIELD1 = "FIELD1";
	public static String FIELD_FIELD2 = "FIELD2";
	public static String FIELD_FIELD3 = "FIELD3";
	public static String FIELD_FIELD4 = "FIELD4";
	
	
	public static final int INCOMING_TYPE = 1;
	public static final int OUTGOING_TYPE = 2;
	public static final int MISSED_TYPE = 3;	

	public static final int CALL_TYPE_SMS = 4;	
	public static final int VMS_TYPE = 5;
	
	
	public static ContentValues getContentValues(Context context, SipCallSession call, long callStart) {
		ContentValues cv = new ContentValues();
		String remoteContact = call.getRemoteContact();
		
		AppPrefs prefs = new AppPrefs(context);
		cv.put(FIELD_CALL_MEMO,prefs.getPreferenceStringValue(AppPrefs.LAST_CALL_MEMO));
		
		//cv.put(CallLog.Calls.NUMBER, remoteContact);
		cv.put(FIELD_PHONENUMBER, remoteContact);
		
		//cv.put(CallLog.Calls.NEW, (callStart > 0)?1:0);
		cv.put(FIELD_IDENTIFIER, (callStart > 0)?1:0);
		
		//cv.put(CallLog.Calls.DATE, (callStart>0 )?callStart:System.currentTimeMillis());
		cv.put(FIELD_DATE,(callStart>0 )?callStart:System.currentTimeMillis());
		
		int type = OUTGOING_TYPE;
		int nonAcknowledge = 0; 
		if(call.isIncoming()) {
			type = MISSED_TYPE;
			nonAcknowledge = 1;
			if(callStart>0) {
				nonAcknowledge = 0;
				type = INCOMING_TYPE;
			}
			//type = VMS_TYPE; // BJH 2016.09.23
		}
		//cv.put(CallLog.Calls.TYPE, type);
		cv.put(FIELD_TYPE, type);
		
		//cv.put(CallLog.Calls.NEW, nonAcknowledge);
		cv.put(FIELD_IDENTIFIER, nonAcknowledge);
		
		//cv.put(CallLog.Calls.DURATION, (callStart>0)?(System.currentTimeMillis()-callStart)/1000:0);
		cv.put(FIELD_DURATION,(callStart>0)?(System.currentTimeMillis()-callStart)/1000:0);
		
		cv.put(FIELD_CALL_TYPE, 0);		
		cv.put(FIELD_HIDDEN, 1);
		
		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);
		if(callerInfo != null) 
		{
			cv.put(FIELD_COMPOSITE_NAME, callerInfo.name);
			cv.put(FIELD_PHONENUMBER, callerInfo.phoneNumber);
			cv.put(FIELD_CALL_TYPE, callerInfo.callType);
			if (callerInfo.callType != 0)
				cv.put(FIELD_HIDDEN, 1);				
		}
		
		cv.put(FIELD_CALL_REC, "");
		
		return cv;
	}	
	
	/*
	//For SMS
	public static ContentValues getContentValues(Context context, String from, String to, String name) {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_DATE,System.currentTimeMillis());
		int type = OUTGOING_TYPE;
		if(to.equalsIgnoreCase("me")) type = INCOMING_TYPE;
		cv.put(FIELD_TYPE, type);
		cv.put(FIELD_IDENTIFIER, 0);
		cv.put(FIELD_DURATION,0);
		cv.put(FIELD_CALL_TYPE, 4);		//4 : SMS
		cv.put(FIELD_HIDDEN, 1);
		cv.put(FIELD_COMPOSITE_NAME, name);
		if(type == OUTGOING_TYPE)
			cv.put(FIELD_PHONENUMBER, to);			
		else
			cv.put(FIELD_PHONENUMBER, from);			
		return cv;
	}		
	*/

	private int mID;
	private String composite_name;
	private String phone_number;
	private long call_date;
	private int type;
	private int duration;
	private long mDuration;
	private int identifier;	
	private int hidden;
	private int call_type;
	private String call_memo;
	private String call_rec;

	public RecentCallsData(){

	}

	public RecentCallsData(String DisplayName, String PhoneNumber, long CallDate, int Identifier,int Type, int Duration, int CallType, int Hidden,String CallMemo, String CallRec) {
		
		composite_name = DisplayName;
		//call_date = System.currentTimeMillis();
		call_date = CallDate;
		phone_number = PhoneNumber;
		identifier = Identifier;
		duration = Duration;
		type = Type;
		call_type =  CallType;		
		hidden = Hidden;
		call_memo = CallMemo;
		call_rec = CallRec;
	}

	public RecentCallsData(ContentValues cv) {
		composite_name 	= cv.getAsString(FIELD_COMPOSITE_NAME);
		call_date 		= cv.getAsInteger(FIELD_DATE);	
		phone_number 	= cv.getAsString(FIELD_PHONENUMBER);
		type 			= cv.getAsInteger(FIELD_TYPE);		
		identifier 		= cv.getAsInteger(FIELD_IDENTIFIER);
		duration 		= cv.getAsInteger(FIELD_DURATION);
		call_type		= cv.getAsInteger(FIELD_CALL_TYPE);
		hidden			= cv.getAsInteger(FIELD_HIDDEN);
		call_memo		= cv.getAsString(FIELD_CALL_MEMO);
		call_rec		= cv.getAsString(FIELD_CALL_REC);
	}
	
	public ContentValues getContentValues() {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_COMPOSITE_NAME, composite_name);
		cv.put(FIELD_PHONENUMBER, phone_number);
		cv.put(FIELD_IDENTIFIER, identifier);
		cv.put(FIELD_DATE, call_date);
		cv.put(FIELD_TYPE, type);
		cv.put(FIELD_DURATION, duration);
		cv.put(FIELD_CALL_TYPE, call_type);
		cv.put(FIELD_HIDDEN, hidden);
		cv.put(FIELD_CALL_MEMO, call_memo);
		cv.put(FIELD_CALL_REC, call_rec);
		return cv;
	}


	public void setID(int id) {
		this.mID = id;
	}

	public void setComposite_name(String composite_name) {
		this.composite_name = composite_name;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public void setCall_date(long call_date) {
		this.call_date = call_date;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setDuration(long duration) {
		this.mDuration = duration;
	}

	public void setIdentifier(int identifier) {
		this.identifier = identifier;
	}

	public void setHidden(int hidden) {
		this.hidden = hidden;
	}

	public void setCall_type(int call_type) {
		this.call_type = call_type;
	}

	public void setCall_memo(String call_memo) {
		this.call_memo = call_memo;
	}

	public void setCall_rec(String call_rec) {
		this.call_rec = call_rec;
	}

	public int getID() {
		return mID;
	}

	public String getComposite_name() {
		return composite_name;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public long getCall_date() {
		return call_date;
	}

	public int getType() {
		return type;
	}

	public long getDuration() {
		return mDuration;
	}

	public int getIdentifier() {
		return identifier;
	}

	public int getHidden() {
		return hidden;
	}

	public int getCall_type() {
		return call_type;
	}

	public String getCall_memo() {
		return call_memo;
	}

	public String getCall_rec() {
		return call_rec;
	}
}
