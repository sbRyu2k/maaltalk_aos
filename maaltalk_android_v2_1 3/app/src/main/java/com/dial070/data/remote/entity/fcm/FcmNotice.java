package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmNotice implements FcmType, Parcelable {

    public static final String KEY_NAME = "name";
    public static final String KEY_URL = "url";
    public static final String KEY_IMG_URL = "img";
    public static final String KEY_FB = "fb";

    private String name;
    private String url;
    private String imgUrl;
    private String fbUr;

    public FcmNotice(@NonNull String name, @Nullable String url, @Nullable String imgUrl, @Nullable String fbUr) {
        this.name = name;
        this.url = url;
        this.imgUrl = imgUrl;
        this.fbUr = fbUr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getFbUr() {
        return fbUr;
    }

    public void setFbUr(String fbUr) {
        this.fbUr = fbUr;
    }

    public static FcmType parseNotice(String message) {
        FcmType fcmNotice = null;

        try {
            JSONObject noticeObject = new JSONObject(message);

            String name = "";
            String url = null;
            String imgUrl = null;
            String fbUrl = null;

            if(noticeObject.has(KEY_NAME)) {
                name = noticeObject.getString(KEY_NAME);
            }

            if(noticeObject.has(KEY_URL)) {
                url = noticeObject.getString(KEY_URL);
            }

            if(noticeObject.has(KEY_IMG_URL)) {
                imgUrl = noticeObject.getString(KEY_IMG_URL);
            }

            if(noticeObject.has(KEY_FB)) {
                fbUrl = noticeObject.getString(KEY_FB);
            }

            fcmNotice = new FcmNotice(name, url, imgUrl, fbUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmNotice;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.url);
        dest.writeString(this.imgUrl);
        dest.writeString(this.fbUr);
    }

    protected FcmNotice(Parcel in) {
        this.name = in.readString();
        this.url = in.readString();
        this.imgUrl = in.readString();
        this.fbUr = in.readString();
    }

    public static final Parcelable.Creator<FcmNotice> CREATOR = new Parcelable.Creator<FcmNotice>() {
        @Override
        public FcmNotice createFromParcel(Parcel source) {
            return new FcmNotice(source);
        }

        @Override
        public FcmNotice[] newArray(int size) {
            return new FcmNotice[size];
        }
    };
}
