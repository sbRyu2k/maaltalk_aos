package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

public class FcmAps implements Parcelable {

    private String alert;
    private String sound;

    public FcmAps(String alert, String sound) {
        this.alert = alert;
        this.sound = sound;
    }

    public String getAlert() {
        return alert;
    }

    public String getSound() {
        return sound;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.alert);
        dest.writeString(this.sound);
    }

    protected FcmAps(Parcel in) {
        this.alert = in.readString();
        this.sound = in.readString();
    }

    public static final Parcelable.Creator<FcmAps> CREATOR = new Parcelable.Creator<FcmAps>() {
        @Override
        public FcmAps createFromParcel(Parcel source) {
            return new FcmAps(source);
        }

        @Override
        public FcmAps[] newArray(int size) {
            return new FcmAps[size];
        }
    };
}
