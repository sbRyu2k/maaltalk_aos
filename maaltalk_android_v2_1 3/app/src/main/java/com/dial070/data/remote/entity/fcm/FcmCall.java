package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmCall implements FcmType, Parcelable {

    public static final String KEY_NAME = "name";
    public static final String KEY_CID = "cid";
    public static final String KEY_DIAL_NUMBER = "dial_number";
    public static final String KEY_CURRENT_TIME = "current_time";
    public static final String KEY_STATUS = "status";

    private String name;
    private String cid;

    private String dialNumber;
    private String currentTime;
    private String status;

    public FcmCall(@NonNull String name, @Nullable String cid, @Nullable String dialNumber,
                   @Nullable String currentTime, @Nullable String status) {
        this.name = name;
        this.cid = cid;
        this.dialNumber = dialNumber;
        this.currentTime = currentTime;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDialNumber() {
        return dialNumber;
    }

    public void setDialNumber(String dialNumber) {
        this.dialNumber = dialNumber;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static FcmType parseCall(String message) {
        FcmType fcmCall = null;

        try {
            JSONObject callObject = new JSONObject(message);

            String name = "";
            String cid = null;
            String dialNumber = null;
            String currentTime = null;
            String status = null;

            if(callObject.has(KEY_NAME)) {
                name = callObject.getString(KEY_NAME);
            }

            if(callObject.has(KEY_CID)) {
                cid = callObject.getString(KEY_CID);
            }

            if(callObject.has(KEY_DIAL_NUMBER)) {
                dialNumber = callObject.getString(KEY_DIAL_NUMBER);
            }

            if(callObject.has(KEY_CURRENT_TIME)) {
                currentTime = callObject.getString(KEY_CURRENT_TIME);
            }

            if(callObject.has(KEY_STATUS)) {
                status = callObject.getString(KEY_STATUS);
            }

            fcmCall = new FcmCall(name, cid, dialNumber, currentTime, status);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmCall;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.cid);
        dest.writeString(this.dialNumber);
        dest.writeString(this.currentTime);
        dest.writeString(this.status);
    }

    protected FcmCall(Parcel in) {
        this.name = in.readString();
        this.cid = in.readString();
        this.dialNumber = in.readString();
        this.currentTime = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<FcmCall> CREATOR = new Parcelable.Creator<FcmCall>() {
        @Override
        public FcmCall createFromParcel(Parcel source) {
            return new FcmCall(source);
        }

        @Override
        public FcmCall[] newArray(int size) {
            return new FcmCall[size];
        }
    };
}
