package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmMTO implements FcmType, Parcelable {

    public static final String KEY_CID = "cid";
    public static final String KEY_CURRENT_TIME = "current_time";

    private String cid;
    private String currentTime;

    public FcmMTO(@NonNull String cid, @Nullable String currentTime) {
        this.cid = cid;
        this.currentTime = currentTime;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public static FcmType parseMTO(String message) {
        FcmType fcmMTO = null;

        try {
            JSONObject mtoObject = new JSONObject(message);

            String cid = "";
            String currentTime = null;

            if(mtoObject.has(KEY_CID)) {
                cid = mtoObject.getString(KEY_CID);
            }

            if(mtoObject.has(KEY_CURRENT_TIME)) {
                currentTime = mtoObject.getString(KEY_CURRENT_TIME);
            }

            fcmMTO = new FcmMTO(cid, currentTime);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmMTO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cid);
        dest.writeString(this.currentTime);
    }

    protected FcmMTO(Parcel in) {
        this.cid = in.readString();
        this.currentTime = in.readString();
    }

    public static final Parcelable.Creator<FcmMTO> CREATOR = new Parcelable.Creator<FcmMTO>() {
        @Override
        public FcmMTO createFromParcel(Parcel source) {
            return new FcmMTO(source);
        }

        @Override
        public FcmMTO[] newArray(int size) {
            return new FcmMTO[size];
        }
    };
}
