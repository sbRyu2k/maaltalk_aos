package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmMessage implements FcmType, Parcelable {

    public static final String KEY_NAME = "name";
    public static final String KEY_IMG = "img";
    public static final String KEY_FB = "fb";
    public static final String KEY_URL = "url";

    private String name;
    private String img;
    private String fb;
    private String url;

    public FcmMessage(@NonNull String name, @Nullable String img, @Nullable String fb, @Nullable String url) {
        this.name = name;
        this.img = img;
        this.fb = fb;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getFb() {
        return fb;
    }

    public void setFb(String fb) {
        this.fb = fb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static FcmType parseMessage(String message) {
        FcmType fcmMessage = null;

        try {
            JSONObject messageObject = new JSONObject(message);
            String name = "";
            String img = null;
            String fb = null;
            String url = null;

            if(messageObject.has(KEY_NAME)) {
                name = messageObject.getString(KEY_NAME);
            }

            if(messageObject.has(KEY_IMG)) {
                img = messageObject.getString(KEY_IMG);
            }

            if(messageObject.has(KEY_FB)) {
                fb = messageObject.getString(KEY_FB);
            }

            if(messageObject.has(KEY_URL)) {
                url = messageObject.getString(KEY_URL);
            }

            fcmMessage = new FcmMessage(name, img, fb, url);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.img);
        dest.writeString(this.fb);
        dest.writeString(this.url);
    }

    protected FcmMessage(Parcel in) {
        this.name = in.readString();
        this.img = in.readString();
        this.fb = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<FcmMessage> CREATOR = new Parcelable.Creator<FcmMessage>() {
        @Override
        public FcmMessage createFromParcel(Parcel source) {
            return new FcmMessage(source);
        }

        @Override
        public FcmMessage[] newArray(int size) {
            return new FcmMessage[size];
        }
    };
}
