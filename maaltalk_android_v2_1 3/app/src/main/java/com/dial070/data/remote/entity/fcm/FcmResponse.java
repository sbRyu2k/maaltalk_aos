package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class FcmResponse implements Parcelable {

    /**
     * ===========================================
     * type 1 --> bridge
     * type 2 --> call
     * type 3 --> msg
     * type 4 --> notice
     * type 5 --> vms
     * type 6 --> mto
     * type 7 --> cb
     * type 8 --> master
     * =============================================
     */

    public static final int TYPE_BRIDGE = 1;
    public static final int TYPE_CALL = 2;
    public static final int TYPE_MSG = 3;
    public static final int TYPE_NOTICE = 4;
    public static final int TYPE_VMS = 5;
    public static final int TYPE_MTO = 6;
    public static final int TYPE_CB = 7;
    public static final int TYPE_MASTER = 8;

    private int messageType;
    private FcmAps aps;
    private FcmType fcmType;

    public FcmResponse(FcmAps aps, FcmType fcmType, int type) {
        this.aps = aps;
        this.fcmType = fcmType;
        this.messageType = type;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public FcmAps getAps() {
        return aps;
    }

    public void setAps(FcmAps aps) {
        this.aps = aps;
    }

    public FcmType getFcmType() {
        return fcmType;
    }

    public void setFcmType(FcmType fcmType) {
        this.fcmType = fcmType;
    }

    public static FcmResponse parseFcmData(String message) {
        FcmResponse fcmResponse = null;
        FcmAps fcmAps = null;
        FcmType fcmType = null;
        int type = 0;

        try {
            JSONObject fcmObject = new JSONObject(message);
            if (!fcmObject.has("aps")) {
                return null;
            }
            String apsString = fcmObject.getString("aps");
            JSONObject apsObject = new JSONObject(apsString);
            String alert = "";
            String sound = "";

            if (apsObject.has("alert")) {
                alert = apsObject.getString("alert");
            }
            if (apsObject.has("sound")) {
                sound = apsObject.getString("sound");
            }

            fcmAps = new FcmAps(alert, sound);

            if (fcmObject.has("bridge")) {
                type = 1;
                fcmType = FcmBridge.parseBridge(fcmObject.getString("bridge"));

            } else if (fcmObject.has("call")) {
                type = 2;
                fcmType = FcmCall.parseCall(fcmObject.getString("call"));

            } else if (fcmObject.has("msg")) {
                type = 3;
                fcmType = FcmMessage.parseMessage(fcmObject.getString("msg"));

            } else if (fcmObject.has("notice")) {
                type = 4;
                fcmType = FcmNotice.parseNotice(fcmObject.getString("notice"));

            } else if (fcmObject.has("vms")) {
                type = 5;
                fcmType = FcmVMS.parseVMS(fcmObject.getString("vms"));

            } else if (fcmObject.has("mto")) {
                type = 6;
                fcmType = FcmMTO.parseMTO(fcmObject.getString("mto"));

            } else if (fcmObject.has("cb")) {
                type = 7;

            } else if (fcmObject.has("master")) {
                type = 8;

            }

            fcmResponse = new FcmResponse(fcmAps, fcmType, type);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmResponse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.messageType);
        dest.writeParcelable(this.aps, flags);
        dest.writeParcelable((Parcelable) this.fcmType, flags);
    }

    protected FcmResponse(Parcel in) {
        this.messageType = in.readInt();
        this.aps = in.readParcelable(FcmAps.class.getClassLoader());
        this.fcmType = in.readParcelable(FcmType.class.getClassLoader());
    }

    public static final Creator<FcmResponse> CREATOR = new Creator<FcmResponse>() {
        @Override
        public FcmResponse createFromParcel(Parcel source) {
            return new FcmResponse(source);
        }

        @Override
        public FcmResponse[] newArray(int size) {
            return new FcmResponse[size];
        }
    };
}


