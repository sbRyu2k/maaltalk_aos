package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmBridge implements FcmType, Parcelable {

    private static final String KEY_NAME = "name";
    private static final String KEY_CID = "cid";
    private static final String KEY_DIAL_NUMBER = "dial_number";
    private static final String KEY_CURRENT_TIME = "current_time";
    private static final String KEY_STATUS = "status";

    private String name;
    private String cid;

    private String dialNumber;
    private String currentTime;
    private String status;

    public FcmBridge(@NonNull String name, @Nullable String cid, @Nullable String dialNumber,
                     @Nullable String currentTime, @Nullable String status) {
        this.name = name;
        this.cid = cid;
        this.dialNumber = dialNumber;
        this.currentTime = currentTime;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDialNumber() {
        return dialNumber;
    }

    public void setDialNumber(String dialNumber) {
        this.dialNumber = dialNumber;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static FcmType parseBridge(String message) {
        FcmType fcmBridge = null;

        try {
            JSONObject bridgeObject = new JSONObject(message);

            String name = "";
            String cid = null;
            String dialNumber = null;
            String currentTime = null;
            String status = null;

            if(bridgeObject.has(KEY_NAME)) {
                name = bridgeObject.getString(KEY_NAME);
            }

            if(bridgeObject.has(KEY_CID)) {
                cid = bridgeObject.getString(KEY_CID);
            }

            if(bridgeObject.has(KEY_DIAL_NUMBER)) {
                dialNumber = bridgeObject.getString(KEY_DIAL_NUMBER);
            }

            if(bridgeObject.has(KEY_CURRENT_TIME)) {
                currentTime = bridgeObject.getString(KEY_CURRENT_TIME);
            }

            if(bridgeObject.has(KEY_STATUS)) {
                status = bridgeObject.getString(KEY_STATUS);
            }

            fcmBridge = new FcmBridge(name, cid, dialNumber, currentTime, status);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmBridge;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.cid);
        dest.writeString(this.dialNumber);
        dest.writeString(this.currentTime);
        dest.writeString(this.status);
    }

    protected FcmBridge(Parcel in) {
        this.name = in.readString();
        this.cid = in.readString();
        this.dialNumber = in.readString();
        this.currentTime = in.readString();
        this.status = in.readString();
    }

    public static final Parcelable.Creator<FcmBridge> CREATOR = new Parcelable.Creator<FcmBridge>() {
        @Override
        public FcmBridge createFromParcel(Parcel source) {
            return new FcmBridge(source);
        }

        @Override
        public FcmBridge[] newArray(int size) {
            return new FcmBridge[size];
        }
    };
}
