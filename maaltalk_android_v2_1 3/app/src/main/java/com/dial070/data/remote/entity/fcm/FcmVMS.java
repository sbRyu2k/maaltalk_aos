package com.dial070.data.remote.entity.fcm;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public class FcmVMS implements FcmType, Parcelable {

    public static final String KEY_CID = "cid";
    public static final String KEY_CURRENT_TIME = "current_time";
    public static final String KEY_FILE_NAME = "file_name";

    private String cid;
    private String currentTIme;
    private String fileUrl;

    public FcmVMS(@NonNull String cid, @Nullable String currentTIme, @Nullable String fileUrl) {
        this.cid = cid;
        this.currentTIme = currentTIme;
        this.fileUrl = fileUrl;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCurrentTIme() {
        return currentTIme;
    }

    public void setCurrentTIme(String currentTIme) {
        this.currentTIme = currentTIme;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public static FcmType parseVMS(String message) {
        FcmType fcmVMS = null;

        try {
            JSONObject vmsObject = new JSONObject(message);

            String cid = "";
            String currentTime = null;
            String fileUrl = null;

            if(vmsObject.has(KEY_CID)) {
                cid = vmsObject.getString(KEY_CID);
            }

            if(vmsObject.has(KEY_CURRENT_TIME)) {
                currentTime = vmsObject.getString(KEY_CURRENT_TIME);
            }

            if(vmsObject.has(KEY_FILE_NAME)) {
                fileUrl = vmsObject.getString(KEY_FILE_NAME);
            }

            fcmVMS = new FcmVMS(cid, currentTime, fileUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fcmVMS;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cid);
        dest.writeString(this.currentTIme);
        dest.writeString(this.fileUrl);
    }

    protected FcmVMS(Parcel in) {
        this.cid = in.readString();
        this.currentTIme = in.readString();
        this.fileUrl = in.readString();
    }

    public static final Parcelable.Creator<FcmVMS> CREATOR = new Parcelable.Creator<FcmVMS>() {
        @Override
        public FcmVMS createFromParcel(Parcel source) {
            return new FcmVMS(source);
        }

        @Override
        public FcmVMS[] newArray(int size) {
            return new FcmVMS[size];
        }
    };
}
