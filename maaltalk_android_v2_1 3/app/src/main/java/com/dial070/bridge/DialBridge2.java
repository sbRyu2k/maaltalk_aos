package com.dial070.bridge;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.bridge.utils.BridgeMediaManager;
import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.maaltalk.R;
import com.dial070.service.BridgePopupService;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.SipNotifications;
import com.dial070.ui.MarshmallowResponse;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DialBridge2 extends Activity {
	private static final String THIS_FILE = "DIAL070_BRIDGE";
	public static Activity mBridge = null;
	public static final String ACTION_BRIDGE_UI = "com.dial070.bridge.action.BRIDGE";
	public BridgeMediaManager mediaManager = null;
	private UncaughtExceptionHandler defaultUEH;
	private Handler autoCloseHandler = null;
	private Runnable autoCloseRunnable = null;
	private Handler setMediaHandler = null;
	private Runnable setMediaRunnable = null;
	private ImageButton takeCallButton, declineCallButton;
	private String mCid, mDial_Number = null;
	private Context mContext = null;
	private TextView remoteName, title;
	private LinearLayout mBridgeButtons;
	//private TextView remotePhoneNumber;
	private Notification inCallNotification = null;
	private Notification missedCallNotification = null;
	private NotificationManager notificationManager = null;
	private TelephonyManager mTelephonyManager = null;
	private static final int BRIDGE_NOTIF_ID = 3001;
	private static final int CALL_NOTIF_ID = BRIDGE_NOTIF_ID + 1;
	private String mDisplay_name = null;
	private Long mExpired_time = 60000L;
	private Long mMedia_time = 1000L;
	private static final int CALL_INCOMING = 1;
	private static final int CALL_CONNECTING = 2;
	private static final int CALL_DISCONNECTED = 3;
	private boolean mIncoming = false;
	private WakeLock wakeLock = null;
	private boolean mWakeLock = false;
	private PowerManager powerManager = null;
	private KeyguardManager km;
	private long now;
	private BroadcastReceiver mReceiver;
	private int mID;

	private void setAutoClose(long delayMS) {
		cancelAutoClose();
		Log.d(THIS_FILE, "setAutoClose (" + delayMS + ")");
		autoCloseRunnable = new Runnable() {
			@Override
			public void run() {
				missed();
			}
		};
		autoCloseHandler = new Handler();
		autoCloseHandler.postDelayed(autoCloseRunnable, delayMS);
	}

	private void cancelAutoClose() {
		if (autoCloseHandler != null) {
			Log.d(THIS_FILE, "cancelAutoClose !!");
			if (autoCloseRunnable != null) {
				autoCloseHandler.removeCallbacks(autoCloseRunnable);
				autoCloseRunnable = null;
			}
			autoCloseHandler = null;
		}
	}

	private void setMediaStart(final Context context, long delayMS) {
		Log.d(THIS_FILE, "setMediaStart (" + delayMS + ")");
		setMediaRunnable = new Runnable() {
			@Override
			public void run() {
				if (wakeLock != null && !wakeLock.isHeld()) {
					Log.d(THIS_FILE, "Acquire wake up lock");
					wakeLock.acquire();
					mWakeLock = true;
				}
				if (mediaManager == null) {
					mediaManager = new BridgeMediaManager(context);
					mediaManager.startService();
					/*if (mediaManager != null) {
						mediaManager.stopAnnoucing();
						mediaManager.resetSettings();
					}*/
					if (mediaManager != null) {
						mediaManager.startRing();
					}
				}
			}
		};
		setMediaHandler = new Handler();
		setMediaHandler.postDelayed(setMediaRunnable, delayMS);
	}

	private void cancelMediaStart() {
		if (setMediaHandler != null) {
			Log.d(THIS_FILE, "cancelMediaStart !!");
			if (setMediaRunnable != null) {
				setMediaHandler.removeCallbacks(setMediaRunnable);
				setMediaRunnable = null;
				if (mediaManager != null) {
					stopRing();
				}
			}
			setMediaHandler = null;
		}
	}

	public class DefaultExceptionHandler implements UncaughtExceptionHandler {
		private Activity activity = null;
		private UncaughtExceptionHandler defaultUEH = null;

		public DefaultExceptionHandler(Activity activity,
									   UncaughtExceptionHandler defaultUEH) {
			this.activity = activity;
			this.defaultUEH = defaultUEH;
		}

		@Override
		public void uncaughtException(Thread thread, Throwable ex) {
			try {
				Context context = this.activity.getBaseContext();

				StringWriter errors = new StringWriter();
				ex.printStackTrace(new PrintWriter(errors));
				String strerr = errors.toString();
				Log.e(THIS_FILE, "uncaughtException !!\n" + strerr);

				// 이 메시지를 저장해 두었다가 다음 실행시 전송하면 디버깅이 가능하다. !!
				AppPrefs mPrefs = new AppPrefs(context);
				mPrefs.setPreferenceStringValue(AppPrefs.LAST_ERROR_DATA,
						strerr);

				Intent intent = new Intent(activity, DialMain.class);
				PendingIntent pendingIntent = PendingIntent.getActivity(
						context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

				// Following code will restart your application after 1 seconds
				AlarmManager mgr = (AlarmManager) context
						.getSystemService(Context.ALARM_SERVICE);

				mgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
						SystemClock.elapsedRealtime() + 1000, pendingIntent);

				release();

				mBridge = null;

				// This will finish your activity manually
				activity.finish();

				// This will stop your application and take out from it.
				System.exit(2);

				if (defaultUEH != null)
					defaultUEH.uncaughtException(thread, ex);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onCreate");
		DebugLog.d("onCreate() --> Bridge2");

		super.onCreate(savedInstanceState);

		setContentView(R.layout.in_call_bridge2);

		defaultUEH = Thread.getDefaultUncaughtExceptionHandler(); // bakcup
		Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(
				this, defaultUEH));

		km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);

		mBridge = DialBridge2.this;
		mContext = this;

		RelativeLayout root=findViewById(R.id.root);
		TextView txtTime=findViewById(R.id.txtTime);
		TextView txtDesc=findViewById(R.id.txtDesc);
		ImageView imgClose=findViewById(R.id.imgClose);

		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setTitleForCall(CALL_DISCONNECTED);
				cancelCallNotification();
				//cancelAutoClose();
				stopRing();
				listen_none();
				releaseLockResource();
				pushResponse("DECLINE");
				finish();
				return;
			}
		});

		remoteName = (TextView) findViewById(R.id.bridge_name);
		title = (TextView) findViewById(R.id.bridge_title);
		//remotePhoneNumber = (TextView) findViewById(R.id.bridge_phoneNumber);

		mTelephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);
		mTelephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			if (notificationManager == null) {
				notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
				if (Build.VERSION.SDK_INT >= 26) {
					int importance = NotificationManager.IMPORTANCE_DEFAULT;
					NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), importance);
					notificationManager.createNotificationChannel(channel);
				}
			}

			mCid = extras.getString("cid");
			mDial_Number = extras.getString("dial_number");
			mExpired_time = extras.getLong("expired_time");
			now= extras.getLong("TIME");
			mID = extras.getInt("ID");

			SimpleDateFormat mFormatter = new SimpleDateFormat("aa hh:mm");
			mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
			String time=mFormatter.format(now);
			txtTime.setText(time);

			mExpired_time = 60000L; //BJH 2017.07.26 브릿지 단말기 시간 오류 관련
			mDisplay_name = ContactHelper.getContactsNameByPhoneNumber(mContext, mCid);
			if (mDisplay_name == null)
				mDisplay_name = mCid;

			txtDesc.setText(mDisplay_name+" 님으로부터 전화가 왔습니다.");

			root.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					cancelAutoClose();
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
						km.requestDismissKeyguard(DialBridge2.this,null);
					}

					Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mDial_Number));
					mContext.startActivity(intent2);

					Intent intent3 = new Intent(mContext, BridgePopupService.class);
					intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					intent3.putExtra("CID",mCid);
					intent3.putExtra("TIME", now);
					intent3.putExtra("dial_number", mDial_Number);
					intent3.putExtra("NAME", mDisplay_name);
					intent3.putExtra("ID", mID);
					mContext.startService(intent3);
					DialBridge2.this.finish();
				}
			});

			remoteName.setText(mDisplay_name);

			showNotificationForCall();
			setTitleForCall(CALL_INCOMING);
			setMediaStart(mContext, mMedia_time);
			setAutoClose(mExpired_time);
		} else {
			finish();
		}

		mBridgeButtons = (LinearLayout) findViewById(R.id.BridgeButtons);
		takeCallButton = (ImageButton) findViewById(R.id.takeCallButton);
		declineCallButton = (ImageButton) findViewById(R.id.declineCallButton);
		takeCallButton.setOnClickListener(new View.OnClickListener() { //BJH 2017.02.23 케이스
			@Override
			public void onClick(View v) {
				setTitleForCall(CALL_CONNECTING);
				//cancelAutoClose();
				stopRing();
				listen_none();
				releaseLockResource();
				mIncoming = true;
				pushResponse("TAKE");
				makeCall();
				/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};
					if (!hasPermissions(mContext, PERMISSIONS)) {
						ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST_CODE_PERMISSIONS);
					} else {
						makeCall();
					}
				} else {
					makeCall();
				}*/
				return;
			}
		});
		declineCallButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setTitleForCall(CALL_DISCONNECTED);
				cancelCallNotification();
				//cancelAutoClose();
				stopRing();
				listen_none();
				releaseLockResource();
				pushResponse("DECLINE");
				finish();
				return;
			}
		});

		createLockResource();
	}

	private static final int REQUEST_CODE_PERMISSIONS = 1;




	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_CODE_PERMISSIONS: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					makeCall();
				} else {
					MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(this);
					builder.setTitle(getResources().getString(R.string.permission_setting));
					builder.setMessage(getResources().getString(R.string.permission_desc));
					builder.setCancelable(false);
					builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
							Uri uri = Uri.fromParts("package", getPackageName(), null);
							intent.setData(uri);
							startActivity(intent);
							finish();
						}
					});
					builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							finish();
						}
					});
					builder.show();
				}
			}
		}
	}

	private static boolean hasPermissions(Context context, String... permissions) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}

	public void makeCall() {
		/*if (Build.VERSION.SDK_INT >= 26) {
			SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
			dialog.setTitleText(getResources().getString(R.string.app_name))
					.setContentText(getResources().getString(R.string.oreo_issue))
					.setConfirmText(getResources().getString(R.string.yes))
					.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
						@Override
						public void onClick(SweetAlertDialog sDialog) {
							sDialog.cancel();
							Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mDial_Number));
							startActivity(intent);
							finish();
						}
					})
					.show();
		} else {
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mDial_Number));
			startActivity(intent);
			finish();
		}*/
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mDial_Number));
		startActivity(intent);
		finish();
	}

	@Override
	protected void onDestroy() {
		Log.d(THIS_FILE, "onDestroy");
		super.onDestroy();
		mBridge = null;

		release();

		if(!mIncoming)
			setTitleForCall(CALL_DISCONNECTED);

		if (defaultUEH != null) {
			Thread.setDefaultUncaughtExceptionHandler(defaultUEH);
			defaultUEH = null;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onPause");
		super.onPause();
		unregisterReceiver();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(THIS_FILE, "onResume");
		registerReceiver();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onRestart");
		super.onRestart();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onStart");
		super.onStart();
	}

	@Override
	protected void onStop() {
		Log.d(THIS_FILE, "onStop");
		if (mediaManager != null)
			mediaManager.stopRing();
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onAttachedToWindow() {
		this.getWindow().setFlags(
		//WindowManager.LayoutParams.FLAG_FULLSCREEN |
		WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
		WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
		WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,

		//WindowManager.LayoutParams.FLAG_FULLSCREEN |
		WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
		WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
		WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	}

	public void setTitleForCall(int state)
	{
		switch(state)
		{
		case CALL_INCOMING:
			title.setText(R.string.call_state_incoming);
			break;
		case CALL_CONNECTING:
			title.setText(R.string.call_state_connecting);
			break;
		case CALL_DISCONNECTED:
			title.setText(R.string.call_state_disconnected);
			break;
		default:
			title.setText(R.string.call_state_incoming);
			break;
		}
	}

	public String getDisplayName()
	{
		return mDisplay_name;
	}

	public String getCid()
	{
		return mCid;
	}

	public String getDial_Number()
	{
		return mDial_Number;
	}

	public Long getExpired_Time()
	{
		return mExpired_time;
	}

	private static final String CHANNEL_ID = "channel_01";

	public void showNotificationForMissedCall(Context context, String displayName) {
		if(notificationManager == null) {
			notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
			if (Build.VERSION.SDK_INT >= 26) {
				int importance = NotificationManager.IMPORTANCE_DEFAULT;
				NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}
		}

		int icon = R.drawable.noti_missed;
		CharSequence tickerText =  context.getText(R.string.missed_call);
		long when = System.currentTimeMillis();

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_CALLLOG);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		missedCallNotification = MarshmallowResponse.createNotification(context, contentIntent, CHANNEL_ID, tickerText.toString(), displayName, icon);

		notificationManager.notify(SipNotifications.CALLLOG_NOTIF_ID, missedCallNotification);
	}

	private void showNotificationForCall() {
		if(notificationManager == null) {
			notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
			if (Build.VERSION.SDK_INT >= 26) {
				int importance = NotificationManager.IMPORTANCE_DEFAULT;
				NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}
		}

		int icon = R.drawable.noti_inbound;
		CharSequence tickerText =  mContext.getText(R.string.incomming_call);
		long when = System.currentTimeMillis();

		Intent intent = new Intent(DialBridge2.ACTION_BRIDGE_UI);
		intent.putExtra("cid", mCid);
		intent.putExtra("dial_number", mDial_Number);
		intent.putExtra("expired_time", mExpired_time);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		inCallNotification = MarshmallowResponse.createNotification(mContext, contentIntent, CHANNEL_ID, tickerText.toString(), mDisplay_name, icon);

		notificationManager.notify(CALL_NOTIF_ID, inCallNotification);
	}

	private void stopRing() {
		if (mediaManager != null) {
			mediaManager.stopAnnoucing();
			mediaManager.resetSettings();
			mediaManager.stopService();
			mediaManager = null;
		}
	}

	private void cancelCallNotification() {
		notificationManager.cancel(CALL_NOTIF_ID);
	}

	private void missed() {
		setTitleForCall(CALL_DISCONNECTED);
		cancelCallNotification();
		stopRing();
		mBridgeButtons.setVisibility(View.GONE);
		showNotificationForMissedCall(mContext, mDisplay_name);
		finish();
	}

	private void release() {
		Log.d(THIS_FILE, "release");
		cancelCallNotification();
		cancelAutoClose();
		cancelMediaStart();
		listen_none();
		releaseLockResource();
	}

	private void listen_none() {
		if(mTelephonyManager != null) {
			mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
			mTelephonyManager = null;
		}
	}

	private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING: // 폰이 울린다.
			case TelephonyManager.CALL_STATE_OFFHOOK: // 폰이 현재 통화 중.
				if(!mWakeLock) { // wakeLock.acquire()이면 브릿지가 활성화된 상태이므로 브릿지가 끊기면 안되기 때문
					cancelMediaStart();
					missed();
				} /*else {
					if(!killCall(mContext)) {
						cancelMediaStart();
						missed();
					}
				}*/
				break;
			default:
				break;
			}
		}
	};

	private void createLockResource()
	{
		// lock
		powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "maaltalk:com.dial070.sip.onIncomingCall");
	}

	private synchronized void releaseLockResource()
	{
		if (wakeLock != null && wakeLock.isHeld())
		{
			wakeLock.release();
			mWakeLock = false;
			wakeLock = null;
		}
	}

	public boolean killCall(Context context) {
		try {
			// Get the boring old TelephonyManager
			TelephonyManager telephonyManager =
					(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

			// Get the getITelephony() method
			Class classTelephony = Class.forName(telephonyManager.getClass().getName());
			Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

			// Ignore that the method is supposed to be private
			methodGetITelephony.setAccessible(true);

			// Invoke getITelephony() to get the ITelephony interface
			Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

			// Get the endCall method from ITelephony
			Class telephonyInterfaceClass =
					Class.forName(telephonyInterface.getClass().getName());
			Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

			// Invoke endCall()
			methodEndCall.invoke(telephonyInterface);

		} catch (Exception ex) { // Many things can go wrong with reflection calls
			Log.d(THIS_FILE,"PhoneStateReceiver **" + ex.toString());
			return false;
		}
		return true;
	}

	//BJH 2016.11.10
	private void pushResponse(String action) {
		long response_time = System.currentTimeMillis();
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			AppPrefs prefs = new AppPrefs(mContext);
			String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
			String number = "";
			TelephonyManager telManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
					int hasPermissionSTATE = checkSelfPermission( Manifest.permission.READ_PHONE_STATE );
					if( hasPermissionSTATE == PackageManager.PERMISSION_GRANTED ) {
						number = telManager.getLine1Number();
					}
				} else {
					int hasPermissionSTATE = checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS);
					if(hasPermissionSTATE==PackageManager.PERMISSION_GRANTED) {
						number = telManager.getLine1Number();
					}
				}
			} else {
				number = telManager.getLine1Number();
			}
			PushRecordData push_record_data = new PushRecordData(0, 0, response_time, "", "BRIDGE", mCid, uid, number, action);
			database.insertPushRecord(push_record_data);
			database.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

	private void registerReceiver() {
		final IntentFilter theFilter = new IntentFilter();
		theFilter.addAction("android.intent.action.USER_PRESENT");

		if(mReceiver == null){
			this.mReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					Log.d(THIS_FILE,"onReceive:"+intent.getAction());
					if (intent.getAction().equals("android.intent.action.USER_PRESENT")){
						KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
							if (keyguardManager.isKeyguardSecure()) {
								//phone was unlocked, do stuff here
								cancelAutoClose();
								Intent intent3 = new Intent(mContext, BridgePopupService.class);
								intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
								intent3.putExtra("CID",mCid);
								intent3.putExtra("TIME", now);
								intent3.putExtra("dial_number", mDial_Number);
								intent3.putExtra("NAME", mDisplay_name);
								intent3.putExtra("ID", mID);
								mContext.startService(intent3);
								DialBridge2.this.finish();
							}
						}
					}
				}
			};
		}

		this.registerReceiver(this.mReceiver, theFilter);
	}

	private void unregisterReceiver() {
		if(mReceiver != null)
			this.unregisterReceiver(mReceiver);
	}

}
