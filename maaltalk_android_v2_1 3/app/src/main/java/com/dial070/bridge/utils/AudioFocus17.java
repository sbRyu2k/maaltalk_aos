/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.bridge.utils;

import android.annotation.TargetApi;

@TargetApi(17)
public class AudioFocus17 extends AudioFocus8 {

	protected static final String THIS_FILE = "AudioFocus 17";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csipsimple.utils.audio.AudioFocusWrapper#getProperty(java.lang.String
	 * )
	 */
	@Override
	public String getProperty(String property) {
		return audioManager.getProperty(property);
	}

}
