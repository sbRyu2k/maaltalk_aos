/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.bridge.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

import com.dial070.sip.service.HeadsetButtonReceiver;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
//import com.dial070.sip.utils.*;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

public class AudioFocus3 extends AudioFocusWrapper {
	

	final static String PAUSE_ACTION = "com.android.music.musicservicecommand.pause";
	final static String TOGGLEPAUSE_ACTION = "com.android.music.musicservicecommand.togglepause";
	private static final String THIS_FILE = "AudioFocus3";
	
	private AudioManager audioManager;

	private Context context; // BJH 2016.09.01
	private PreferencesWrapper prefsWrapper;
	private boolean isMusicActive = false;
	private boolean isFocused = false;
	//private HeadsetButtonReceiver headsetButtonReceiver;

	public void focus() {
		DebugLog.d("Focus --> "+isFocused);
		if(!isFocused) {
			pauseMusic();
			//registerHeadsetButton();
			isFocused = true;
		}
	}
	
	public void unFocus() {
		DebugLog.d("UnFocus --> "+isFocused);
		if(isFocused) {
			restartMusic();
			//unregisterHeadsetButton();
			isFocused = false;
		}
	}

	
	private void pauseMusic() {
		DebugLog.d("pauseMusic()");
		isMusicActive = audioManager.isMusicActive();
		if(isMusicActive && prefsWrapper.integrateWithMusicApp()) {
			context.sendBroadcast(new Intent(PAUSE_ACTION));
		}
	}
	
	private void restartMusic() {
		DebugLog.d("restartMusic()");
		if(isMusicActive && prefsWrapper.integrateWithMusicApp()) {
			context.sendBroadcast(new Intent(TOGGLEPAUSE_ACTION));
		}
	}
	
	/*private void registerHeadsetButton() {
		Log.d(THIS_FILE, "Register media button");
		IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
		intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY +100 );
		if(headsetButtonReceiver == null) {
			headsetButtonReceiver = new HeadsetButtonReceiver();
			HeadsetButtonReceiver.setService(SipService.getUAStateReceiver());
		}
		context.registerReceiver(headsetButtonReceiver, intentFilter);
	}
	
	private void unregisterHeadsetButton() {
		try {
			context.unregisterReceiver(headsetButtonReceiver);
			HeadsetButtonReceiver.setService(null);
			headsetButtonReceiver = null;
		}catch(Exception e) {
			//Nothing to do else. just consider it has not been registered
		}
	}*/
	@Override 
	public String getProperty(String property) { //BJH 2016.07.18
	    return null; 
	}
	//BJH 2016.09.01
	public void initBridge(Context aContext, AudioManager manager) {
		DebugLog.d("initBridge --> "+aContext.getPackageName());
		context = aContext;
		audioManager = manager;
		prefsWrapper = new PreferencesWrapper(context);
	}
}
