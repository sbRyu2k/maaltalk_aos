/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.bridge.utils;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;

import com.dial070.sip.service.HeadsetButtonReceiver;
import com.dial070.sip.utils.Compatibility;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

@TargetApi(8) 
public class AudioFocus8 extends AudioFocusWrapper{
	
	
	protected static final String THIS_FILE = "AudioFocus 8";
	protected AudioManager audioManager; //BJH 2016.07.18 private -> protected
	private Context context; // BJH 2016.09.01
	private ComponentName headsetButtonReceiverName;
	
	private boolean isFocused = false;
	
	private OnAudioFocusChangeListener focusChangedListener = new OnAudioFocusChangeListener() {
		
		@Override
		public void onAudioFocusChange(int focusChange) {
//			Log.d(THIS_FILE, "Focus changed");
			DebugLog.d("AudioFocusChangeListener --> Focus changed");
		}
	};
	
	public void focus() {
//		Log.d(THIS_FILE, "Focus again "+isFocused);
		DebugLog.d("Focus again --> "+isFocused);
		if(!isFocused) {
			//HeadsetButtonReceiver.setService(SipService.getUAStateReceiver());
			audioManager.registerMediaButtonEventReceiver(headsetButtonReceiverName);
			audioManager.requestAudioFocus(focusChangedListener, 
					Compatibility.getInCallStream(), AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
			isFocused = true;
		}
	}
	
	public void unFocus() {
		DebugLog.d("UnFocus --> "+isFocused);

		if(isFocused) {
			//HeadsetButtonReceiver.setService(null);
			audioManager.unregisterMediaButtonEventReceiver(headsetButtonReceiverName);
			//TODO : when switch to speaker -> failure to re-gain focus then cause music player will wait before reasking focus
			audioManager.abandonAudioFocus(focusChangedListener);
			isFocused = false;
		}
	}
	@Override 
    public String getProperty(String property) { //BJH 2016.07.18
        return null; 
    }
	
	//BJH 2016.09.01
	public void initBridge(Context aContext, AudioManager manager) {
		DebugLog.d("initBridge --> "+aContext.getPackageName());

		context = aContext;
		audioManager = manager;
		headsetButtonReceiverName = new ComponentName(context.getPackageName(), 
				HeadsetButtonReceiver.class.getName());
	}

}
