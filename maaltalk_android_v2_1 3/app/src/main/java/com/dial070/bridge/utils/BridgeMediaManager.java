/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.bridge.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.PowerManager.WakeLock;

import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.sip.utils.accessibility.AccessibilityWrapper;
import com.dial070.sip.utils.audio.MyAudioRecord;
import com.dial070.sip.utils.bluetooth.BluetoothWrapper;
import com.dial070.sip.utils.bluetooth.BluetoothWrapper.BluetoothChangeListener;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

import org.pjsip.pjsua.pjsua;

import java.util.Locale;

public class BridgeMediaManager implements BluetoothChangeListener {

	final private static String THIS_FILE = "Bridge MediaManager";

	Context context;
	private AudioManager audioManager;
	private BridgeRinger ringer;

	// Locks
	private WifiLock wifiLock;
	private WakeLock screenLock;

	// Media settings to save / resore
	private int savedRoute, savedMode;
	private boolean isSavedAudioState = false, isSetAudioMode = false;

	// By default we assume user want bluetooth.
	// If bluetooth is not available connection will never be done and then
	// UI will not show bluetooth is activated
	private boolean userWantBluetooth = true;
	private boolean userWantSpeaker = false;
	private boolean userWantMicrophoneMute = false;

	private Intent mediaStateChangedIntent;

	// Bluetooth related
	private BluetoothWrapper bluetoothWrapper;

	private AudioFocusWrapper audioFocusWrapper;

	private AccessibilityWrapper accessibilityManager;

	private static int MODE_SIP_IN_CALL = AudioManager.MODE_NORMAL;
	private boolean needSoundFix = false;
	//private boolean startBeforeInit = false;
	public PreferencesWrapper prefsWrapper;
	//public AppPrefs mPrefs;

	/**
	 * DailBridgeNew | BridgePopupService
	 * @param aContext
	 */
	public BridgeMediaManager(Context aContext) {
		DebugLog.d("caller --> "+aContext.getClass().getSimpleName());

		context = aContext;
		prefsWrapper = new PreferencesWrapper(context);
		//mPrefs = new AppPrefs(context);
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		accessibilityManager = AccessibilityWrapper.getInstance();
		accessibilityManager.init(context);
		ringer = new BridgeRinger(context);
		//mediaStateChangedIntent = new Intent(SipManager.ACTION_SIP_MEDIA_CHANGED);
	}

//	public void setAudio() {
//		DebugLog.d("set Audio called...");
//
//		if(bluetoothWrapper!=null) {
//			DebugLog.d("set bt");
//			bluetoothWrapper.setBluetoothOn(true);
//
//			/**
//			 * test
//			 */
//			DebugLog.d("setAudio canBluetooth"+bluetoothWrapper.canBluetooth());
//
//		} else {
//			DebugLog.d("bluetooth Wrapper is null...");
//		}
//	}
	
	private void actualUnsetAudio() { // BJH 2017.02.07 브릿지 이슈 해결, 2017.11.29 필요없는 부분도 많음
		DebugLog.d("actualUnsetAudio called...");
		audioManager.setSpeakerphoneOn(false);

		if (bluetoothWrapper != null) {
			// This fixes the BT activation but... but... seems to introduce a
			// lot of other issues
			// bluetoothWrapper.setBluetoothOn(true);
//			Log.d(THIS_FILE, "Unset bt");
			DebugLog.d("Unset bt");

			bluetoothWrapper.setBluetoothOn(false);
		}
		
		audioManager.setMicrophoneMute(false);

		int inCallStream = Compatibility.getInCallStream();
		//setStreamVolume(inCallStream, savedVolume, 0);
		audioManager.setStreamSolo(inCallStream, false);
		audioManager.setMode(savedMode);
		
		if (wifiLock != null && wifiLock.isHeld())
		{
//			Log.d(THIS_FILE, "actualUnsetAudioInCall : ACQUIRE WIFI");
			DebugLog.d("actualUnsetAudioInCall : ACQUIRE WIFI");

			wifiLock.release();
		}
		if (screenLock != null && screenLock.isHeld())
		{
//			Log.d(THIS_FILE, "Release screen lock");
			DebugLog.d("Release screen lock");

			screenLock.release();
		}
		
		audioFocusWrapper.unFocus();

		isSavedAudioState = false;
		isSetAudioMode = false;

	}

	public void startService() {
		DebugLog.d("Start Media manager...");

		if (bluetoothWrapper == null) {
			bluetoothWrapper = BluetoothWrapper.getInstance(context);
			bluetoothWrapper.setBluetoothChangeListener(this);
			bluetoothWrapper.register();
		}

		if (audioFocusWrapper == null) {
			audioFocusWrapper = AudioFocusWrapper.getInstance();
			audioFocusWrapper.initBridge(context, audioManager);
		}

		if (android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M11")
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M13")
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M190")
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("GT-I9000")
				)
			needSoundFix = true;
		else
			needSoundFix = false;

		context.registerReceiver(mHeadsetPlugReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
	}

	public void stopService() {
//		Log.i(THIS_FILE, "Remove media manager....");
		DebugLog.d("Remove media manager...");

		if (bluetoothWrapper != null) {
			bluetoothWrapper.unregister();
		}
		
		context.unregisterReceiver(mHeadsetPlugReceiver);
	}
	
	
	private BroadcastReceiver mHeadsetPlugReceiver = new BroadcastsHandler();
	public class BroadcastsHandler extends BroadcastReceiver 
	{
		@Override public void onReceive(Context context, Intent intent) 
		{    
			if (intent.getAction().equalsIgnoreCase(Intent.ACTION_HEADSET_PLUG)) {
//				String data = intent.getDataString();        
//				Bundle extraData = intent.getExtras();
				String nm = intent.getStringExtra("name");
				int st = intent.getIntExtra("state", 0);        
				int mic = intent.getIntExtra("microphone", 0);        
				String all = String.format(Locale.getDefault(), "st=%d, nm=%s, mic=%d", st, nm != null?nm:"", mic);
//				Log.d(THIS_FILE, "HEADSET_PLUG : (" + all + ")");

				DebugLog.d("HEADSET_PLUG : (" + all + ")");
				DebugLog.d("needSoundFix --> "+needSoundFix);
				DebugLog.d("isSetAudioMode --> "+isSetAudioMode);
				DebugLog.d("userWantSpeaker --> "+userWantSpeaker);
				DebugLog.d("userWantBt --> "+userWantBluetooth);
				
				if (needSoundFix && isSetAudioMode)
				{
					if (!userWantSpeaker)
					{
						MyAudioRecord.routingchanged = true;
					}
				}
			}
		}
	}

	private int getAudioTargetMode()
	{
		int targetMode = MODE_SIP_IN_CALL;
		
		if (needSoundFix)
		{
			if(audioManager.isWiredHeadsetOn() == true || userWantSpeaker == true)
			{
				return AudioManager.MODE_NORMAL;
			}
			else
			{
				return AudioManager.MODE_IN_CALL;			
			}
		}		
		
		if (prefsWrapper.getUseModeApi() /*|| android.os.Build.DEVICE.toUpperCase().startsWith("GT-I9000")*/)
		{
			Log.d(THIS_FILE, "User want speaker now..." + userWantSpeaker);
			if (!prefsWrapper.generateForSetCall())
			{
				return userWantSpeaker ? AudioManager.MODE_NORMAL : AudioManager.MODE_IN_CALL;
			}
			else
			{
				return userWantSpeaker ? AudioManager.MODE_IN_CALL : AudioManager.MODE_NORMAL;
			}
		}
		return targetMode;
	}
	
	private boolean needSetMode(int mode)
	{
		int old = audioManager.getMode();
		return (old != mode);
	}
	
	/**
	 * Save current audio mode in order to be able to restore it once done
	 */
	private void saveAudioState()
	{
		Log.d(THIS_FILE, "saveAudioState : " + isSavedAudioState);
		
		if (isSavedAudioState) { return; }
		
//		ContentResolver ctntResolver = service.getContentResolver();
//		savedVibrateRing = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);
//		savedVibradeNotif = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION);
//		savedRingerMode = audioManager.getRingerMode();
//		savedWifiPolicy = android.provider.Settings.System.getInt(ctntResolver, android.provider.Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_DEFAULT);
		
		
//		savedVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		savedMode = audioManager.getMode();
		savedRoute = audioManager.getRouting(getAudioTargetMode());

		isSavedAudioState = true;

	}

	public AudioManager getAudioManager() {
		return audioManager;
	}

	/**
	 * ADDED
	 * 2020.08.19
	 * Sip 서비스 연결 시 media 제어권을 넘겨주기 위해 Ringer 상태 체크 기능
	 */
	public boolean isRinging() {
		if(ringer==null) {
			return false;
		}

		if(ringer.isRinging()) return true;
		else return false;
	}
	
	public void startRing() {
		saveAudioState();
		audioFocusWrapper.focus();

		DebugLog.d("userWantBt --> "+userWantBluetooth);

		/**
		 * TEST
		 */
		boolean canBluetooth = bluetoothWrapper.canBluetooth();
		boolean isConnectedBluetooth = bluetoothWrapper.isBluetoothOn();
		boolean isBTHeadsetConnected = bluetoothWrapper.isBTHeadsetConnected();

		DebugLog.d("can bluetooth --> "+canBluetooth);
		DebugLog.d("is Connected bluetooth --> "+isConnectedBluetooth);
		DebugLog.d("is Bt Headset Connected --> "+isBTHeadsetConnected);

		if(isBTHeadsetConnected) {
			audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
			bluetoothWrapper.setBluetoothOn(true);

		} else {

		}

		ringer.ring(prefsWrapper.getRingtone());

//		if (!ringer.isRinging())
//		{
//			Log.d(THIS_FILE, " ring()   )ng ....");
//			ringer.ring(prefsWrapper.getRingtone());
//			//ringer.ring(getRingtone());
//		}
//		else
//		{
//			Log.d(THIS_FILE, "Already ringing ....");
//		}

	}

	public void stopRing() {
		DebugLog.d("BR stopRing is called...");
		if(ringer==null) {
			return;
		}

		ringer.stopRing();

//		if (ringer.isRinging()) {
//			Log.d(THIS_FILE, " stopring()   )ng ....");
//			DebugLog.d("current ringing state...");
//
//		}
	}

	public void stopAnnoucing() {
		DebugLog.d("BR stopAnnouncing called...");
		stopRing();
		DebugLog.d("savedMode --> "+savedMode);

//		actualUnsetAudio(); // BJH 2017.02.07 삼성단말기 브릿지 콜 이슈 해결

		/**
		 * 2020.08.19
		 * 블루투스 헤드셋이 연결중일 경우 Ring Stop 처리시 블루투스 소스 연결 종료 기능
		 */
		if(bluetoothWrapper.isBTHeadsetConnected()) {
//			setBluetoothOn(false);
			setBluetoothOn(false);
			bluetoothWrapper.setBluetoothOn(false);
//			audioManager.setBluetoothScoOn(false);
//			audioManager.stopBluetoothSco();
		}

		audioFocusWrapper.unFocus();
	}

	public void resetSettings() {
		userWantBluetooth = true;
		userWantMicrophoneMute = false;
		userWantSpeaker = false;
	}

	public void toggleMute()
	{
		setMicrophoneMute(!userWantMicrophoneMute);
	}

	public synchronized void setMicrophoneMute(boolean on)
	{
		if (on != userWantMicrophoneMute)
		{
			float level = on ? 0 : prefsWrapper.getMicLevel();
			pjsua.conf_adjust_rx_level(0, level);
			Log.d(THIS_FILE, "conf_adjust_rx_level : " + level);
			userWantMicrophoneMute = on;
			broadcastMediaChanged();
		}
	}

	public synchronized void setSpeakerphoneOn(boolean on)
	{
		if (needSoundFix)
		{
			if (on != userWantSpeaker)
			{
				userWantSpeaker = on;
				
				int targetMode = getAudioTargetMode();
				if (needSetMode(targetMode))
				{
					MyAudioRecord.routingchanged = true;
				}
				else
				{
					audioManager.setSpeakerphoneOn(userWantSpeaker);
				}
				broadcastMediaChanged();
			}
		}
		else
		{
			/*
			pjsua.set_no_snd_dev();
			userWantSpeaker = on;
			pjsua.set_snd_dev(0, 0);
			*/
			userWantSpeaker = on;
			audioManager.setSpeakerphoneOn(userWantSpeaker);
			broadcastMediaChanged();
		}
	}

	public synchronized void setBluetoothOn(boolean on)
	{
		Log.d(THIS_FILE, "Set BT " + on);
		//pjsua.set_no_snd_dev();
		userWantBluetooth = on;
		//pjsua.set_snd_dev(0, 0);
		broadcastMediaChanged();
	}

	public class MediaState
	{
		public boolean isMicrophoneMute = false;
		public boolean isSpeakerphoneOn = false;
		public boolean isBluetoothScoOn = false;
		public boolean canMicrophoneMute = true;
		public boolean canSpeakerphoneOn = true;
		public boolean canBluetoothSco = false;

		@Override
		public boolean equals(Object o)
		{

			if (o != null && o.getClass() == MediaState.class)
			{
				MediaState oState = (MediaState) o;
				if (oState.isBluetoothScoOn == isBluetoothScoOn && oState.isMicrophoneMute == isMicrophoneMute && oState.isSpeakerphoneOn == isSpeakerphoneOn && oState.canBluetoothSco == canBluetoothSco && oState.canSpeakerphoneOn == canSpeakerphoneOn && oState.canMicrophoneMute == canMicrophoneMute)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			return super.equals(o);
		}
	}

	public MediaState getMediaState()
	{
		MediaState mediaState = new MediaState();

		// Micro
		mediaState.isMicrophoneMute = userWantMicrophoneMute;
		mediaState.canMicrophoneMute = true; /* && !mediaState.isBluetoothScoOn */// Compatibility.isCompatible(5);

		// Speaker
		mediaState.isSpeakerphoneOn = userWantSpeaker;
		mediaState.canSpeakerphoneOn = true && !mediaState.isBluetoothScoOn; // Compatibility.isCompatible(5);

		// Bluetooth

		if (bluetoothWrapper != null)
		{
			mediaState.isBluetoothScoOn = bluetoothWrapper.isBluetoothOn();
			mediaState.canBluetoothSco = bluetoothWrapper.canBluetooth();
		}
		else
		{
			mediaState.isBluetoothScoOn = false;
			mediaState.canBluetoothSco = false;
		}

		return mediaState;
	}

	public void broadcastMediaChanged()
	{
		//context.sendBroadcast(mediaStateChangedIntent);
	}

	private static final String ACTION_AUDIO_VOLUME_UPDATE = "org.openintents.audio.action_volume_update";
	private static final String EXTRA_STREAM_TYPE = "org.openintents.audio.extra_stream_type";
	private static final String EXTRA_VOLUME_INDEX = "org.openintents.audio.extra_volume_index";
	private static final String EXTRA_RINGER_MODE = "org.openintents.audio.extra_ringer_mode";
	private static final int EXTRA_VALUE_UNKNOWN = -9999;

	private void broadcastVolumeWillBeUpdated(int streamType, int index) {
		DebugLog.d("broadcastVolumeWillBeUpdated... streamType --> "+streamType);

		Intent notificationIntent = new Intent(ACTION_AUDIO_VOLUME_UPDATE);
		notificationIntent.putExtra(EXTRA_STREAM_TYPE, streamType);
		notificationIntent.putExtra(EXTRA_VOLUME_INDEX, index);
		notificationIntent.putExtra(EXTRA_RINGER_MODE, EXTRA_VALUE_UNKNOWN);

		context.sendBroadcast(notificationIntent, null);
	}

	public void setStreamVolume(int streamType, int index, int flags) {
		broadcastVolumeWillBeUpdated(streamType, index);
		audioManager.setStreamVolume(streamType, index, flags);
		Log.d(THIS_FILE, "setStreamVolume : " + streamType + ", " + index);
	}

	public void adjustStreamVolume(int streamType, int direction, int flags) {
		broadcastVolumeWillBeUpdated(streamType, EXTRA_VALUE_UNKNOWN);
		audioManager.adjustStreamVolume(streamType, direction, flags);
	}

	// Public accessor
	public boolean isUserWantMicrophoneMute() {
		Log.d(THIS_FILE, "isUserWantMicrophoneMute : " + userWantMicrophoneMute);
		return userWantMicrophoneMute;
	}
	
    @Override
    public void onBluetoothStateChanged(int status) {
		DebugLog.d("onBluetoothStateChanged... status --> "+status);

		switch (status) {
			case AudioManager.SCO_AUDIO_STATE_CONNECTED:
				DebugLog.d("state SCO_AUDIO_STATE_CONNECTED --> 1");
				break;
			case AudioManager.SCO_AUDIO_STATE_CONNECTING:
				DebugLog.d("state SCO_AUDIO_STATE_CONNECTING --> 2");
				break;
			case AudioManager.SCO_AUDIO_STATE_DISCONNECTED:
				DebugLog.d("state SCO_AUDIO_STATE_DISCONNECTED --> 0");
				break;
		}

        broadcastMediaChanged();
    }

	/**
	 * TEST
	 * ADDED
	 * @param status
	 */
	@Override
	public void onBluetoothConnectionChanged(int status) {
		DebugLog.d("onBluetoothConnectionChanged... status --> "+status);

		switch (status) {
			case 2: // connected
				break;
			case 1: // connecting
				break;
			case 0: // disconnect
				break;
		}

	}
}
