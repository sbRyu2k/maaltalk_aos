/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * Copyright (C) 2006 The Android Open Source Project
 * 
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.bridge.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

/**
 * Ringer manager for the Phone app.
 */
public class BridgeRinger
{
	private static final String THIS_FILE = "Bridge Ringer";

	private static final int VIBRATE_LENGTH = 1000; // ms
	private static final int PAUSE_LENGTH = 1000; // ms

	private int savedMode;

	// Uri for the ringtone.
	Uri customRingtoneUri;

	Ringtone ringtone = null; // [sentinel]
	Vibrator vibrator;
	VibratorThread vibratorThread;
	RingerThread ringerThread;
	Context context;

	/**
	 * @param aContext
	 */

	public BridgeRinger(Context aContext) {
		DebugLog.d("caller --> "+aContext.getClass().getSimpleName());
		context = aContext;
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
	}

	/**
	 * Starts the ringtone and/or vibrator.
	 * 
	 */
	public void ring(String defaultRingtone) {
//		Log.d(THIS_FILE, "==> ring() called...");

		DebugLog.d("ring() called --> "+defaultRingtone);

		synchronized (this) {

			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

			/**
			 *  안드로이드10 벨소리 관련 수정 2020.05.29 맹완석
			 *  모드를 원래대로 돌리지않으면 삼성단말기 모닝콜알람등 울리지않음.
			 */
			savedMode = audioManager.getMode();

			int ringerMode = audioManager.getRingerMode();
			if (ringerMode == AudioManager.RINGER_MODE_SILENT) {
//				Log.d(THIS_FILE, "skipping ring and vibrate because profile is Silent");
				DebugLog.d("Skipping ring and vibrate because profile is Silent...");
				return;
			}

			DebugLog.d("saveMode --> "+savedMode);
			DebugLog.d("ringerMode --> "+ringerMode);

			// Ringer mode is vibrate or normal

			int vibrateSetting = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);

//			Log.d(THIS_FILE, "v=" + vibrateSetting + " rm=" + ringerMode);

			DebugLog.d("v=" + vibrateSetting + " rm=" + ringerMode);

			if (vibratorThread == null && (vibrateSetting == AudioManager.VIBRATE_SETTING_ON || ringerMode == AudioManager.RINGER_MODE_VIBRATE)) {
				vibratorThread = new VibratorThread();
//				Log.d(THIS_FILE, "Starting vibrator...");
				DebugLog.d("Starting vibrator");
				vibratorThread.start();
			}

			if (ringerMode == AudioManager.RINGER_MODE_VIBRATE) {
//				Log.d(THIS_FILE, "skipping ring because profile is Vibrate");
				DebugLog.d("Skipping ring because profile is Vibrate");
				return;
			}

			// Ringer mode is normal

			if (audioManager.getStreamVolume(AudioManager.STREAM_RING) == 0) {
//				Log.d(THIS_FILE, "skipping ring because volume is zero");
				DebugLog.d("Skipping ring because volume is zero");
				return;
			}

			// Ringer normal, audio set for ring, do itf

			ringtone = getRingtone(defaultRingtone);
			if (ringtone == null) {
//				Log.d(THIS_FILE, "No ringtone available - do not ring");
				DebugLog.d("No ringtone available - do not ring");
				return;
			}

//			Log.d(THIS_FILE, "Starting ring with " + ringtone.getTitle(context));
			DebugLog.d("Starting ring with "+ringtone.getTitle(context));

			if (ringerThread == null) {
				ringerThread = new RingerThread();
//				Log.d(THIS_FILE, "Starting ringer...");
				DebugLog.d("Starting ringer");

				/**
				 * TEST
				 */

//				audioManager.setMode(AudioManager.MODE_RINGTONE);
				ringerThread.start();
			}
		}
	}

	/**
	 * @return true if we're playing a ringtone and/or vibrating to indicate
	 *         that there's an incoming call. ("Ringing" here is used in the
	 *         general sense. If you literally need to know if we're playing a
	 *         ringtone or vibrating, use isRingtonePlaying() or isVibrating()
	 *         instead.)
	 */
	public boolean isRinging() {
		return (ringerThread != null || vibratorThread != null);
	}

	/**
	 * Stops the ringtone and/or vibrator if any of these are actually
	 * ringing/vibrating.
	 */
	public void stopRing() {
		synchronized (this) {
			Log.d(THIS_FILE, "==> stopRing() called");

			// Immediately cancel any vibration in progress.
			// vibrator.cancel();
			// ringtone.stop();

			if (vibratorThread != null) {
				vibratorThread.interrupt();
				try {
					vibratorThread.join(250); // Should be plenty long (typ.)
				}
				catch (InterruptedException e) {
				} // Best efforts (typ.)
				vibratorThread = null;
			}

			if (ringerThread != null) {
				ringerThread.interrupt();
				try {
					ringerThread.join(250);
				}
				catch (InterruptedException e) {
				}
				ringerThread = null;
			}
		}
	}

	private class VibratorThread extends Thread {
		public void run() {
			try {
				while (true) {
					vibrator.vibrate(VIBRATE_LENGTH);
					Thread.sleep(VIBRATE_LENGTH + PAUSE_LENGTH);
				}
			}
			catch (InterruptedException ex) {
//				Log.d(THIS_FILE, "Vibrator thread interrupt");
				DebugLog.d("Vibrator thread interrupt");
			}
			finally {
				vibrator.cancel();
			}
//			Log.d(THIS_FILE, "Vibrator thread exiting");
			DebugLog.d("Vibrator thread interrupt");
		}
	}

	private class RingerThread extends Thread {
		public void run() {
			try {
				while (true) {
					ringtone.play();
					while (ringtone.isPlaying()) {
						Thread.sleep(100);
					}
				}
			}
			catch (InterruptedException ex) {
//				Log.d(THIS_FILE, "Ringer thread interrupt");
				DebugLog.d("Ringer thread interrupt");
			}
			catch (IllegalStateException ex) {
//				Log.d(THIS_FILE, "Ringer thread interrupt");
				DebugLog.d("Ringer thread interrupt");
			}
			finally {
				try {
					/**
					 *  안드로이드10 벨소리 관련 수정 2020.05.29 맹완석
					 *  모드를 원래대로 돌리지않으면 삼성단말기 모닝콜알람등 울리지않음.
					 */
//					Log.i(THIS_FILE,"savedMode:"+savedMode);
					DebugLog.d("saveMode --> "+savedMode);

					AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
					audioManager.setMode(savedMode);
					if (ringtone.isPlaying())
						ringtone.stop();
				}
				catch (IllegalStateException e)
				{
				}
			}
			DebugLog.d("Ringer thread exiting");
//			Log.d(THIS_FILE, "Ringer thread exiting");
		}
	}

	private Ringtone getRingtone(String defaultRingtone) {
		DebugLog.d("defaultRingtone --> "+defaultRingtone);

		Uri ringtoneUri = Uri.parse(defaultRingtone);

		return RingtoneManager.getRingtone(context, ringtoneUri);
	}
}
