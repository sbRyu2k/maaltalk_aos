package com.dial070.status

import com.dial070.view.entitiy.CallActionData
import com.dial070.view.entitiy.CallData
import com.dial070.view.entitiy.NativeCallData

enum class CallStatus {

    /**=====================
     * CallStatus callData
     * =====================
     * 22 --> Not SIP CONNECTION ONLY RING (background mode)
     * 0 --> NULL
     * 1 --> CALLING
     * 2 --> INCOMING
     * 3 --> EARLY
     * 4 --> CONNECTING
     * 5 --> CONFIRMED
     * 6 --> DISCONNECTED
     */

    /**
     * Action
     * Idle 0
     * Calling 1
     * Incoming 2
     */

    /**
     * Native Call status
     * 0 --> IDLE
     * 1 --> RINGING
     * 2 --> OFFHOOK
     */

    /**
     * CallStatus modeData
     */

    INSTANCE;

    private lateinit var mCallActionData: CallActionData
    private lateinit var mCallData: CallData
    private lateinit var mNativeCallData: NativeCallData
    private lateinit var mCallPushCallNumber: String

    fun init() {
        mCallActionData = CallActionData(0)
        mCallData = CallData(0, 0, "")
        mNativeCallData = NativeCallData(0)
        mCallPushCallNumber = ""
    }

    fun updateCallAction(callActionData: CallActionData){
        mCallActionData = callActionData
    }

    fun updatePushCallNumber(number: String) {
        mCallPushCallNumber = number
    }

    fun update(callData: CallData) {
        mCallData = callData
    }

    fun updateNativeStatus(nativeCallData: NativeCallData) {
        mNativeCallData = nativeCallData
    }

    fun getCallAction(): Int {
        if(!this::mCallActionData.isInitialized) {
            mCallActionData = CallActionData(0)
        }
        return mCallActionData.actionStatus
    }

    fun getPushCallNumber(): String {
        if(!this::mCallData.isInitialized) {
            mCallPushCallNumber = ""
        }
        return mCallPushCallNumber
    }

    fun getCurrentCallId(): Int {
        if(!this::mCallData.isInitialized) {
            mCallData = CallData(0, 0, "")
        }
        return mCallData.callId
    }

    fun getCurrentStatus(): Int {
        if(!this::mCallData.isInitialized) {
            mCallData = CallData(0, 0, "")
        }
        return mCallData.callStatus
    }

    fun getCurrentCallNumber(): String {
        if(!this::mCallData.isInitialized) {
            mCallData = CallData(0, 0, "")
        }
        return mCallData.callNumber
    }

    fun getNativeStatus(): Int {
        if(!this::mNativeCallData.isInitialized) {
            mNativeCallData = NativeCallData(0)
        }
        return mNativeCallData.status
    }

    fun setNativeStatus(status: Int) {
        mNativeCallData.status = status
    }

}