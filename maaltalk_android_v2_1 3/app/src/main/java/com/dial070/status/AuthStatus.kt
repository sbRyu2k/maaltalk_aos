package com.dial070.status

import com.dial070.data.AuthData

enum class AuthStatus {
    INSTANCE;

    private var mAuthData: AuthData? = null
    fun update(authData: AuthData?) {
        mAuthData = authData
    }
}