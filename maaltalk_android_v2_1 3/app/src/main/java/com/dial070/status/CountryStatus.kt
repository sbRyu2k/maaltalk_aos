package com.dial070.status

import com.dial070.data.CountryData

enum class CountryStatus {
    INSTANCE;

    private var mCountryData: CountryData? = null
    fun update(countryData: CountryData?) {
        mCountryData = countryData
    }

    val countryCode: String
        get() = mCountryData!!.countryCode

    val countryName: String
        get() = mCountryData!!.countryName

    val countryFlag: String
        get() = mCountryData!!.countryFlag
}