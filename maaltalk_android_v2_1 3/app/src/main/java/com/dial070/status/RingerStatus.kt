package com.dial070.status

import com.dial070.view.entitiy.BridgeRingerData
import com.dial070.view.entitiy.MediaRingerData

enum class RingerStatus {

    INSTANCE;

    private lateinit var mediaRingerData: MediaRingerData
    private lateinit var bridgeRingerData: BridgeRingerData

    fun init() {
        mediaRingerData = MediaRingerData(false)
        bridgeRingerData = BridgeRingerData(false)
    }

    fun updateMediaRinger(ringerData: MediaRingerData) {
        mediaRingerData = ringerData
    }

    fun updateBridgeRinger(ringerData: BridgeRingerData) {
        bridgeRingerData = ringerData
    }

    fun isMediaRingerIsRinging(): Boolean {
        if(!this::mediaRingerData.isInitialized) {
            return false
        }
        return mediaRingerData.isRinging
    }

    fun isBridgeRingerIsRinging(): Boolean {
        if(!this::bridgeRingerData.isInitialized) {
            return false
        }
        return bridgeRingerData.isRinging
    }

}