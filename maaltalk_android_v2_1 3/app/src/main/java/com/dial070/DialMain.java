package com.dial070;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Color;
import android.icu.util.Freezable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;

//import com.crashlytics.android.Crashlytics;
import com.dial070.bridge.DialBridge;
import com.dial070.bridge.DialBridgeNew;
import com.dial070.db.DBManager;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.oauth.OauthLoginActivity;
import com.dial070.service.ApnService;
import com.dial070.service.Fcm;
import com.dial070.service.InBoundCallMediaService;
import com.dial070.service.MessageService;
import com.dial070.service.PushyMQTT;
import com.dial070.service.StartForegroundService;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.status.CallStatus;
import com.dial070.status.WaitingStatus;
import com.dial070.ui.AuthActivityV2;
import com.dial070.ui.Calls;
import com.dial070.ui.Contacts;
import com.dial070.ui.Dial;
import com.dial070.ui.HomeActivity;
import com.dial070.ui.KisaActivity;
import com.dial070.ui.MarshmallowResponse;
import com.dial070.ui.PayClient;
import com.dial070.ui.PopupAgency;
import com.dial070.ui.Search;
import com.dial070.ui.Setting;
import com.dial070.ui.Sms;
import com.dial070.ui.SmsMsgContsActivity;
import com.dial070.ui.WebClient;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.dial070.utils.OnSingleClickListener;
import com.dial070.utils.ServiceUtils;
import com.dial070.view.entitiy.CallActionData;
import com.dial070.view.entitiy.CallData;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.DoubleBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

//import io.fabric.sdk.android.Fabric;
import me.pushy.sdk.Pushy;

public class DialMain extends TabActivity {
    private static final String THIS_FILE = "DIAL070_MAIN";
    public static final String TAB_DIAL_TAG = "tab_dial";
    public static final String TAB_CALLS_TAG = "tab_calls";
    public static final String TAB_CONTACT_TAG = "tab_contact";
    public static final String TAB_MESSAGE_TAG = "tab_message";
    // public static final String TAB_SEARCH_TAG = "tab_search";
    public static final String TAB_SETTING_TAG = "tab_setting";

    public static final String ACTION_DIAL_COMMAND = "com.dial070.COMMAND";
    public static final String ACTION_DIAL_COMMAND_RETURN = "com.dial070.COMMAND.RETURN";
    // public static final String ACTION_FAVORITE_VIEW =
    // "com.dial070.FAVORITE_VIEW";
    public static final String ACTION_KEYPAD_VIEW = "com.dial070.KEYPAD_VIEW";
    public static final String ACTION_GCALL_COMMAND = "com.gcall.COMMAND";

    // EXTRAS

    public static final String EXTRA_PUSH = "push";
    public static final String EXTRA_PUSH_CALL = "push_call_play";
    public static final String EXTRA_BALANCE = "balance";
    public static final String EXTRA_COMMAND = "commnad";
    public static final String EXTRA_CALL_NUMBER = "call_number";
    public static final String EXTRA_GCM_CID = "gcm_cid";
    public static final String EXTRA_NOTICE_URL = "notice_url";
    public static final String EXTRA_NOTICE_IMG_URL = "notice_img_url";
    public static final String EXTRA_NOTICE_FB_URL = "notice_fb_url";
    public static final String EXTRA_NOTICE_TITLE = "notice_title";

    public static final String EXTRA_MESSAGE_TITLE = "message_title";
    public static final String EXTRA_MESSAGE_NUMBER = "message_number";

    public static final String EXTRA_MTO = "mto"; // BJH 2017.05.15

    public static final String EXTRA_SCHEME_CALL_TYPE = "scheme_call_type";
    public static final String EXTRA_SCHEME_CALL_NUMBER = "scheme_call_number";

    private static final int REQUEST_CODE_PERMISSIONS = 1;

    public static DialMain currentContext = null;
    public static boolean bMaaltalkRunned = false;
    public static int currentState = 0; // 0:pause, 1:resume
    public static int currentStopState = 0; // 0:start, 1:stop
    public static boolean skipClose = false;
    public static boolean skipCallview = false;
    public static boolean checkStack = false; // 통화 종료후 돌아올때.
    // public static boolean fromPush = false;
    // public static boolean autoClose = false;

    private TabHost mTabHost;
    //private TextView mAppText;
    //private TextView mRTTText;
    //private ImageView mAntena;
    //private TextView mBalanceText;
    //BJH 2016.07.04
    //private static TextView mAppNameText;

    private AppPrefs mPrefs;
    private Context mContext;
    private DBManager mDatabase;
    private UncaughtExceptionHandler defaultUEH;
    private String currentBalance;

    private static final int ALERT_DIALOG = 1;
    private static final int SELECT_CALL = 2;
    private static final int SELECT_CALL2 = 3;

    private static final int BALANCE_REQUEST = 4;
    private static final int BALANCE_UPDATE = 5;
    private static final int SET_TAB = 6;

    private static final int UPDATE_REG_STATE = 7;
    private static final int UPDATE_BADGE = 8;

    private static final int REQUEST_BATTERY_PERMISSIONS = 9;
    //BJH
    public static final String START_FOREGROUND_PACKAGE = "com.dial070.service.StartForegroundService";
    public static final String START_FOREGROUND = "start_foreground";
    //public static final String ACTION_MAALBUDD = "android.intent.action.VIEW";
    //private int mForeground = 0;
    private String mAutoDial = "";
    public static final String MAALTALK_PACKAGE_NAME = "com.dial070.maaltalk";// 최신 버전 비교 관련
    //public static final String MAALTALK_APK_URL = "http://www.maaltalk.com/common/maaltalk.apk";//GCM TOKEN이 없는 경우 바로 홈페이지에서 다운 받도록 이동
    public static final String MAALTALK_APK_URL = "http://www.maaltalk.com/faq.php?id=colp24";//GCM TOKEN이 없는 경우 바로 홈페이지에서 다운 받도록 이동

    //App app;
    //BJH 2016.10.26
    //private static FrameLayout mFL;
    //private static FloatingActionsMenu mActionMenu;
    /*private FloatingActionButton mActionPlace; // BJH 2017.01.02
    private FloatingActionButton mActionUSIM; // BJH 2017.01.02
    private FloatingActionButton mActionForwarding;
    private FloatingActionButton mActionAgency; // BJH 2017.03.10
    private FloatingActionButton mActionUserMSg;
    private FloatingActionButton mActionPoint;*/
    //private FloatingActionButton mActionKakao;
    //private FloatingActionButton mActionSearch;
    //private FloatingActionButton mActionMyinfo;
    //private FloatingActionButton mActionFaq;
    //private FloatingActionButton mActionNotice;
    //private FloatingActionButton mActionStore;
    private static final int SELECT_USER_MSG = 1;// BJH
    private String mProduct, mCallfw;
    private ArrayList<String> mBalanceList = null;
    public static final String MAALTALK_EMAIL = "mailto:private.mng@dial070.co.kr";
    //public static final String URL_MAALTALK_USIM = "http://maaltalk.com/usim.json";
    private static TelephonyManager mTPM = null;
    private static String mDistributor_id = null;
    public static final String MAALTALK_JSON_ROOT = "/data/data/com.dial070.maaltalk/json"; //BJH 2017.01.03
    private final static int SELECT_OVERLAY_PERMISSION = 2; // BJH 2017.03.22 android.permission.SYSTEM_ALERT_WINDOW
    private final static int SELECT_REQ_MYINFO = 3; // BJH 2017.06.22
    private final static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE= 4;

    private final static int SELECT_SEARCH = 5;

    private AlertDialog.Builder builderUpdate =null;
    private AlertDialog mAlertDialogUpdate=null;

    private long mLastClickTime;

    private ImageButton imgBtnDial;

    private IntentFilter intentFilterACTION_DIAL_COMMAND;
    private IntentFilter intentFilterACTION_SIP_REGISTRATION_CHANGED;

    private String DID_EXPIRED="";

    private boolean push;

    @SuppressLint("HandlerLeak")
    private Handler msgHandler = new Handler() {
        public void handleMessage(Message msg) {
            DebugLog.d("handleMessage() --> what : "+msg.what);
            switch (msg.what) {
                case ALERT_DIALOG: {
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> ALERT_DIALOG");
                    AlertDialog.Builder msgDialog = new AlertDialog.Builder(
                            DialMain.this);
                    msgDialog.setTitle(getResources().getString(R.string.app_name));
                    if (msg.obj != null)
                        msgDialog.setMessage((String) msg.obj);
                    else
                        msgDialog.setMessage(msg.arg1);
                    msgDialog.setIcon(R.drawable.ic_launcher);
                    msgDialog.setNegativeButton(getResources().getString(R.string.close), null);
                    msgDialog.show();
                }
                break;
                case SELECT_CALL: {
//                    Log.i(THIS_FILE,"msgHandler SELECT_CALL");
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> SELECT_CALL");
                    String number = (String) msg.obj;
                    if (number == null || number.length() == 0)
                        return;
                    //BJH 2016.08.26 관리팀 요청
                /*if (number.startsWith("+"))
					selectGCall(number);
				else
					selectGSMCall(number);*/

                    if (App.isDialForeground){
//                        Log.i(THIS_FILE,"msgHandler SELECT_CALL isDialForeground true");
                        DebugLog.d("handleMessage() --> what : "+msg.what+" --> SELECT_CALL isDialForeground true");
                        Intent intent = new Intent(Dial.BC_DIALOG);
                        intent.putExtra("type", "type1");
                        intent.putExtra("number", number);
                        intent.setPackage(getPackageName());
                        mContext.sendBroadcast(intent);
                    }else {
                        DebugLog.d("handleMessage() --> what : "+msg.what+" --> SELECT_CALL isDialForeground false");
                        selectGSMCall(number);
                    }
                }
                break;
                case SELECT_CALL2: {
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> SELECT_CALL_2");
                    String number = (String) msg.obj;
                    if (number == null || number.length() == 0)
                        return;
                    //BJH 2016.08.26 관리팀 요청
				/*if (number.startsWith("+"))
					selectGCall2(number);
				else
					selectGSMCall2(number);*/

                    if (App.isDialForeground){
                        Intent intent = new Intent(Dial.BC_DIALOG);
                        intent.putExtra("type", "type2");
                        intent.putExtra("number", number);
                        intent.setPackage(getPackageName());
                        mContext.sendBroadcast(intent);
                    }else {
                        selectGSMCall2(number);
                    }
                }
                break;
                case BALANCE_REQUEST:
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> BALANCE_REQUEST");
                    //mBalanceText.setText(getResources().getString(R.string.balance_loading));
                    break;
                case BALANCE_UPDATE:
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> BALANCE_UPDATE");
                    if (currentBalance == null || currentBalance.length() == 0) {
                        //mBalanceText.setText(getResources().getString(R.string.balance_error));
                    }
                    else { //BJH 2016.11.11
                        if (mThreadTimer == null)
                            balanceList();
                        //mBalanceText.setText(currentBalance);
                    }
                    break;
                case SET_TAB:
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> SET_TAB");
                    mTabHost.setCurrentTab(msg.arg1);
                    //showFAB(); // BJH 2016.10.26
                    break;
                case UPDATE_REG_STATE: {
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> UPDATE_REG_STATE");
                    updateRegState();
                }
                break;
                case UPDATE_BADGE: {
                    DebugLog.d("handleMessage() --> what : "+msg.what+" --> UPDATE_BADGE");
                    updateBadge();
                }
                break;
            }
        }
    };

    public void AppCloseRequest(final Context context) {
        DebugLog.d("AppCloseRequest() caller "+context.getClass().getSimpleName());
        /*if (mFL.getVisibility() == View.VISIBLE) { //BJH 2016.11.02
            mActionMenu.collapse();
            return;
        }*/

        AlertDialog.Builder builder = new AlertDialog.Builder(
                context);
        //LayoutInflater inflater = Setting.this.getLayoutInflater();
        //View view_svr = inflater.inflate(R.layout.svr_dialog, null);
        //builder.setView(view_svr);
        builder.setMessage(getString(R.string.question_quit));
        builder.setPositiveButton(
                getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0,
                                        int arg1) {
                        finish();
                    }
                });
        builder.setNegativeButton(
                getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });

        builder.setTitle(context.getResources().getString(
                R.string.title_quit));
        AlertDialog dialog=builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
    }

    // PUSH로 실행된후 연결이 없으면 자동 종료.
    private Handler autoCloseHandler = null;
    private Runnable autoCloseRunnable = null;

    public void setAutoClose(long delayMS) {
        DebugLog.d("setAutoClose() delayMs --> "+delayMS);

        cancelAutoClose();

//        Log.d(THIS_FILE, "setAutoClose (" + delayMS + ")");
        autoCloseRunnable = new Runnable() {
            @Override
            public void run() {
                disconnectAndQuit(true);
            }
        };
        autoCloseHandler = new Handler();
        autoCloseHandler.postDelayed(autoCloseRunnable, delayMS);
    }

    public void cancelAutoClose() {
        DebugLog.d("cancelAutoClose()");

        if (autoCloseHandler != null) {
            Log.d(THIS_FILE, "cancelAutoClose !!");
            if (autoCloseRunnable != null) {
                autoCloseHandler.removeCallbacks(autoCloseRunnable);
                autoCloseRunnable = null;
            }
            autoCloseHandler = null;
        }
    }

    public class DefaultExceptionHandler implements UncaughtExceptionHandler {
        private Activity activity = null;
        private UncaughtExceptionHandler defaultUEH = null;

        public DefaultExceptionHandler(Activity activity,
                                       UncaughtExceptionHandler defaultUEH) {
            this.activity = activity;
            this.defaultUEH = defaultUEH;
        }

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            try {
                Context context = this.activity.getBaseContext();

                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));
                String strerr = errors.toString();
                Log.e(THIS_FILE, "uncaughtException !!\n" + strerr);

                // 이 메시지를 저장해 두었다가 다음 실행시 전송하면 디버깅이 가능하다. !!
                AppPrefs mPrefs = new AppPrefs(context);
                mPrefs.setPreferenceStringValue(AppPrefs.LAST_ERROR_DATA,
                        strerr);

                // Toast toast = Toast.makeText(context,
                // context.getString(R.string.app_name) + "\n\n" +
                // ex.toString(), Toast.LENGTH_LONG);
                // toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 150);
                // toast.show();

                Intent intent = new Intent(activity, DialMain.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

                // Following code will restart your application after 1 seconds
                AlarmManager mgr = (AlarmManager) context
                        .getSystemService(Context.ALARM_SERVICE);
                // mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000,
                // pendingIntent);
                mgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + 1000, pendingIntent);

                // This will finish your activity manually
                activity.finish();

                // This will stop your application and take out from it.
                System.exit(2);

                if (defaultUEH != null)
                    defaultUEH.uncaughtException(thread, ex);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
//        Log.d(THIS_FILE, "onCreate");
        super.onCreate(savedInstanceState);
        DebugLog.d("onCreate()");

        if(ServiceUtils.isRunningService(App.getGlobalApplicationContext(), InBoundCallMediaService.class)) {
            if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.NULL || CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.DISCONNECTED) {
                Intent invalidPushRemoveIntent = new Intent("invalid_in_call_state_remove");
                invalidPushRemoveIntent.setPackage(getPackageName());
                sendBroadcast(invalidPushRemoveIntent);
            }
        }

//        Fabric.with(this, new Crashlytics());
        mPrefs = new AppPrefs(this);
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String password = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);
        String tokenValue = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);

        String init_url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL);
        DebugLog.d("test_v2 saved_info_check  url: "+init_url);

        try {
            FirebaseCrashlytics.getInstance().setCustomKey("09id", uid);
            FirebaseCrashlytics.getInstance().setCustomKey("password", password);
            FirebaseCrashlytics.getInstance().setCustomKey("fcm_token", tokenValue);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(uid!=null) DebugLog.d("AppPrefs.USER_ID --> "+uid);

//        Crashlytics.setUserIdentifier(uid);

        DebugLog.d("test_v2 dtmf_mode: ");

      if(exceptionlife == true) {
            Log.d(THIS_FILE, "on exceptionlife create number = "+uid);
//            Crashlytics.logException(new Exception("on exceptionlife create number = "+uid));
           return;
      }

      FirebaseCrashlytics.getInstance().setUserId(uid);

        // 커스텀 타이틀 바
        //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dial_main);

        //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        currentContext = this;
        bMaaltalkRunned = true;
        currentStopState = 0;
        isExit = false;

        Fcm.mRegistrationOK = false;

//        bindToService();

        /**
         * TEST
         */
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler(); // bakcup
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(
                this, defaultUEH));

        // by sgkim : 2015-08-26 : TODO : 향후 메인쓰레드에서 네트워크 관련 I/O는 수정할 예정입니다.
        // 임시로 조치함.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mContext = this;
        mPrefs = new AppPrefs(this);

        Log.d(THIS_FILE, "onCreate1");
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(THIS_FILE, "NameNotFoundException:", e);
        }

        mPrefs.setPreferenceStringValue(AppPrefs.DIAL_VERSION, pInfo.versionName);
        ServicePrefs.DIAL_VERSION=pInfo.versionName;

        // CHECK ERROR REPORT
        String strerr = mPrefs.getPreferenceStringValue(AppPrefs.LAST_ERROR_DATA);
        if (strerr != null && strerr.length() > 0) {
//            Log.e(THIS_FILE,
//                    "ERROR REPORT : " + String.valueOf(strerr.length()) + "\n"
//                            + strerr);

            DebugLog.d("ERROR REPORT : "+String.valueOf(strerr.length())+" --> "+strerr);

            // SEND ERROR REPORT TO SERVER
            mPrefs.setPreferenceStringValue(AppPrefs.LAST_ERROR_DATA, "");
            ServicePrefs.ReqHistory(this, "ERROR", strerr);
        }

        mDatabase = new DBManager(this);
        mContext = this;
        mPrefs = new AppPrefs(this);
        if (!mDatabase.isOpen())
            mDatabase.open();

        currentBalance = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String cid = extras.getString(DialMain.EXTRA_GCM_CID);
            if (cid != null && cid.length() > 0) {
//                Log.d(THIS_FILE, "SET AUTO ANSWER : " + cid);
                DebugLog.d("getIntent() Set AUTO ANSWER --> "+cid);
                mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, cid);
            }

            // NOTICE
            String url = extras.getString(DialMain.EXTRA_NOTICE_URL);
            if (url != null && url.length() > 0) {
//                Log.d(THIS_FILE, "SET NOTICE URL : " + url);
                DebugLog.d("getIntent() Set NOTICE URL --> "+url);
                mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_URL, url);
            }

            String fb_url = extras.getString(DialMain.EXTRA_NOTICE_FB_URL);
            if (fb_url != null && fb_url.length() > 0) {
//                Log.d(THIS_FILE, "SET NOTICE FB URL : " + fb_url);
                DebugLog.d("getIntent() Set NOTICE FB URL --> "+fb_url);
                mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_FB_URL, fb_url);
            }

            String title = extras.getString(DialMain.EXTRA_NOTICE_TITLE);
            if (title != null && title.length() > 0) {
//                Log.d(THIS_FILE, "SET NOTICE TITLE : " + title);
                DebugLog.d("getIntent() Set NOTICE TITLE --> "+title);
                mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_TITLE, title);
            }

            // MESSAGE
            String msg_number = extras.getString(DialMain.EXTRA_MESSAGE_NUMBER);
            if (msg_number != null && msg_number.length() > 0) {
//                Log.d(THIS_FILE, "SET MESSAGE NUMBER : " + msg_number);
                DebugLog.d("getIntent() Set MESSAGE NUMBER --> "+msg_number);
                mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_NUMBER,
                        msg_number);
            }

            String msg_title = extras.getString(DialMain.EXTRA_MESSAGE_TITLE);
            if (msg_title != null && msg_title.length() > 0) {
//                Log.d(THIS_FILE, "SET MESSAGE TITLE : " + msg_title);
                DebugLog.d("getIntent() Set MESSAGE TITLE --> "+msg_title);
                mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_TITLE,
                        msg_title);
            }

            //BJH 2017.05.15 MTO
            String mto = extras.getString(DialMain.EXTRA_MTO);
            if (mto != null && mto.length() > 0) {
//                Log.d(THIS_FILE, "SET MTO NUMBER: " + mto);
                DebugLog.d("getIntent() Set MTO NUMBER --> "+mto);
                mPrefs.setPreferenceStringValue(AppPrefs.MTO_NUMBER,
                        mto);
            }

            push = extras.getBoolean(DialMain.EXTRA_PUSH);
            boolean isAndroid10Clicked = getIntent().getBooleanExtra("ANDROID10_CLICK",false);
            DebugLog.d("getIntent() START FROM PUSH --> "+push);
            if (push) {
//                Log.d(THIS_FILE, "START FROM PUSH");
                // fromPush = true;
                // PUSH로 깨어난 다음 INVITE를 수신하지 못하면 자동 종료.
                setAutoClose(60000); // by sgkim : 2015-08-19 : change to 60sec
            }
            //BJH 노티바를 클릭했을 경우 바로 설정 탭으로 이동 수정
            int foreground = extras.getInt(DialMain.START_FOREGROUND);
            DebugLog.d("getIntent() START_FOREGROUND --> "+foreground);

            if (foreground > 0) {
                //mForeground = foreground;
                mPrefs.setPreferenceBooleanValue(AppPrefs.TAB_SETTING, true);
            }
        }

        /*
            BJH 2017.11.14 KISA 권고사항
         */
        boolean kisaOK = mPrefs.getPreferenceBooleanValue(AppPrefs.INCLUDE_ADVICE_KISA);
        boolean termOk = mPrefs.getPreferenceBooleanValue(AppPrefs.TERM_OK);

        if(!kisaOK && !termOk) {
            Intent KisaIntent = new Intent(this, KisaActivity.class);
            KisaIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(KisaIntent);
            finish();
            return;

        } else if(!kisaOK && termOk) {
            mPrefs.setPreferenceBooleanValue(AppPrefs.INCLUDE_ADVICE_KISA, true);
        }

        if (!ServicePrefs.mLogin && !ServicePrefs.mPush) {
//            Log.d(THIS_FILE, "Dial070 NOT RUNNING");
            DebugLog.d("Dial070 NOT RUNNING");

            Boolean no_delay = false;
            String action="";
            String type="";
            if (extras != null) {
                no_delay = extras.getBoolean("NO_DELAY");
                // Log.e(THIS_FILE, "NO_DELAY : " + no_delay.toString());
                action = extras.getString("action");
                type = extras.getString("type");
            }

            DebugLog.d("navigate to DialSplashActivity");

            Intent IntroIntent = new Intent(this, DialSplash.class);
            IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            IntroIntent.putExtra("NO_DELAY", no_delay);
            Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
//                Log.d(THIS_FILE, "url scheme 실행");
                Uri uri = intent.getData();
                DebugLog.d("host --> "+uri.getHost().equals("dial"));
                DebugLog.d("call --> "+uri.getHost().equals("call"));

                if(uri != null) {
                    if (uri.getHost().equals("dial") || uri.getHost().equals("call")){
                        String schemePhoneNumber=uri.getQueryParameter("param1");
                        if (schemePhoneNumber!=null && schemePhoneNumber.trim().length()>0){
                            if (schemePhoneNumber.trim().startsWith("+")){
                                schemePhoneNumber="+"+schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                            }else{
                                schemePhoneNumber=schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                            }

//                            Log.d(THIS_FILE, "url scheme 실행 :"+ uri.getHost() +","+schemePhoneNumber);
                            DebugLog.d("URL Scheme 실생 --> "+uri.getHost()+" , "+schemePhoneNumber);

                            IntroIntent.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,uri.getHost());
                            IntroIntent.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemePhoneNumber);
                        }
                    }
                }
            }

            if (action!=null&& action.equals(SipManager.ACTION_SIP_CALLLOG)){
//                Log.e(THIS_FILE,"action:"+action);
                DebugLog.d("action --> "+action);
                IntroIntent.putExtra("action", action);
            }

            if (type!=null&& type.equals("BridgeReturn")){
//                Log.e(THIS_FILE,"type:"+type);
                DebugLog.d("type --> "+type);
                IntroIntent.putExtra("type", type);
            }

            startActivity(IntroIntent);
            finish();
            return;
        } else {
            ServicePrefs.mPush=false;
            Thread t = new Thread() {
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        Log.e(THIS_FILE,"InterruptedException:"+e.getMessage());
                    }
                    Log.d(THIS_FILE,"updateToken check 1");
                    if(PushyMQTT.mRegistrationID == null) {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            Log.e(THIS_FILE,"InterruptedException:"+e.getMessage());
                        }
                    }
                    Log.d(THIS_FILE,"updateToken check 2");
                    Fcm.updateToken(DialMain.this,"Main");
                }
            };
            if (t != null)
                t.start();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                boolean isWhiteListing = false;
                isWhiteListing = pm.isIgnoringBatteryOptimizations(mContext.getPackageName());
                if(!isWhiteListing){
                    AlertDialog.Builder setdialog = new AlertDialog.Builder(mContext,R.style.MyDialogTheme);
                    setdialog.setTitle(getString(R.string.maaltalk_notification))
                            .setMessage(getString(R.string.maaltalk_battery_optimization_alert))
                            .setPositiveButton("예", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent  = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                                    intent.setData(Uri.parse("package:"+ mContext.getPackageName()));
                                    startActivityForResult(intent,REQUEST_BATTERY_PERMISSIONS);
                                }
                            })
                            .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(mContext, getString(R.string.maaltalk_battery_optimization_alert2), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .create()
                            .show();
                }else {
                    //checkOverlayPermission();
                }
            }
        }
        Log.d(THIS_FILE, "onCreate3");
// shlee add dialer
        imgBtnDial=findViewById(R.id.imgBtnDial);
        imgBtnDial.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DebugLog.d("clicked imgBtnDial --> navigate to Dial");
                App.isDialClicked=true;
                ServicePrefs.DID_EXPIRED=DID_EXPIRED;
                App.isReturnClicked=false;
                mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                App.isReturnContactClicked=false;
                Intent intentDial=new Intent(DialMain.this, Dial.class);
                //intentDial.putExtra("DID_EXPIRED",DID_EXPIRED);
                startActivity(intentDial);
            }
        });

        //mAntena = (ImageView) findViewById(R.id.title_icon);
        //mAntena.setBackgroundResource(R.drawable.antena_0);

        //mAppText = (TextView) findViewById(R.id.left_title);
        //mAppText.setTextColor(Color.WHITE);
        //mAppText.setTypeface(Typeface.DEFAULT_BOLD);
        //mAppText.setText("");
        //BJH 2016.07.04
        //mAppNameText = (TextView) findViewById(R.id.app_name);
        //BJH 2017.03.27
        /*String keyword = mPrefs.getPreferenceStringValue(AppPrefs.MT_KEYWORD);
        if(keyword != null && keyword.length() > 0)
            mAppNameText.setText(keyword);

        {
            ViewParent parent = mAppText.getParent();
            if (parent != null && (parent instanceof View)) {
                View parentView = (View) parent;
                parentView.setBackgroundColor(mContext.getResources().getColor(
                        R.color.custom_title_background));

                parent = parentView.getParent();
                if (parent != null && (parent instanceof View)) {
                    parentView = (View) parent;
                    parentView.setBackgroundColor(mContext.getResources()
                            .getColor(R.color.custom_title_background));
                }
            }
        }*/

        //mRTTText = (TextView) findViewById(R.id.left_desc);
        //mRTTText.setTextColor(Color.WHITE);
        //mRTTText.setText("");

        // 잔액정보 표시
        //mBalanceText = (TextView) findViewById(R.id.right_title);
        //mBalanceText.setTextColor(Color.WHITE);
        //mBalanceText.setTypeface(Typeface.DEFAULT_BOLD);
        // 테스트
        //mBalanceText.setText("");

        /*mBalanceText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 2000){
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();
                checkBalance();
            }
        });
        */

        // 내전화번호 저장
        // mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER,
        // getMyPhoneNumber());
        // shlee add checkbalance for updating info on display main display
        checkBalance();
        // 탭 버튼 생성
        setupTabHost();
        // shlee add
        /*
        App.isDialClicked=true;
        ServicePrefs.DID_EXPIRED=DID_EXPIRED;
        App.isReturnClicked=false;
        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
        App.isReturnContactClicked=false;
        Intent intentDial=new Intent(this, Dial.class);
        if (extras != null) {
            String callType = extras.getString(DialMain.EXTRA_SCHEME_CALL_TYPE);
            String schemeCallNumber = extras.getString(DialMain.EXTRA_SCHEME_CALL_NUMBER);

            intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,callType);
            intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemeCallNumber);
        }
        */


        /*Intent intentDial=new Intent(this, Dial.class);
        if (extras != null) {
            String callType = extras.getString(DialMain.EXTRA_SCHEME_CALL_TYPE);
            String schemeCallNumber = extras.getString(DialMain.EXTRA_SCHEME_CALL_NUMBER);

            intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,callType);
            intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemeCallNumber);
        }*/

        // shlee remove
        Intent intentDial=new Intent(this, HomeActivity.class);


        View tabview1 = createTabView(mTabHost.getContext(), TAB_DIAL_TAG);
        TabSpec setContent = mTabHost.newTabSpec(TAB_DIAL_TAG)
                .setIndicator(tabview1)
                .setContent(intentDial);
        mTabHost.addTab(setContent);
        View tabview2 = createTabView(mTabHost.getContext(), TAB_CALLS_TAG);
        setContent = mTabHost.newTabSpec(TAB_CALLS_TAG).setIndicator(tabview2)
                .setContent(new Intent(this, Calls.class));
        mTabHost.addTab(setContent);
        View tabContact = createTabView(mTabHost.getContext(), TAB_CONTACT_TAG);

        Intent intentContact=new Intent(this, Contacts.class);
        intentContact.putExtra("Type","POPUP");
        setContent = mTabHost.newTabSpec(TAB_CONTACT_TAG).setIndicator(tabContact)
                .setContent(intentContact);
        mTabHost.addTab(setContent);
        View tabview3 = createTabView(mTabHost.getContext(), TAB_MESSAGE_TAG);
        // setContent =
        // mTabHost.newTabSpec(TAB_SEARCH_TAG).setIndicator(tabview3).setContent(new
        // Intent(this, Search.class));
        setContent = mTabHost.newTabSpec(TAB_MESSAGE_TAG)
                .setIndicator(tabview3).setContent(new Intent(this, Sms.class));
        mTabHost.addTab(setContent);
        View tabview4 = createTabView(mTabHost.getContext(), TAB_SETTING_TAG);
        setContent = mTabHost.newTabSpec(TAB_SETTING_TAG)
                .setIndicator(tabview4)
                .setContent(new Intent(this, Setting.class));
        mTabHost.addTab(setContent);

        //BJH START FOREGROUND 노티바로 통해서 들어왔는지 체크
		/*if(mForeground > 0)
			mTabHost.setCurrentTab(3);
		else
			mTabHost.setCurrentTab(0);*/
		/*boolean tab_setting = mPrefs.getPreferenceBooleanValue(AppPrefs.TAB_SETTING);
		if(tab_setting) {
			mPrefs.setPreferenceBooleanValue(AppPrefs.TAB_SETTING, false);
			mTabHost.setCurrentTab(3);
		}
		else {
			mTabHost.setCurrentTab(0);
		}*/
        mTabHost.setCurrentTab(0);//BJH 2016.09.08

        // 다이얼패트 클릭
        tabview1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // Log.e(THIS_FILE, "TAB_CLICKED");
                mTabHost.setCurrentTab(0);
                //showFAB(); //BJH 2016.10.26
                // Intent intent = new Intent(DialMain.ACTION_KEYPAD_VIEW);
                // sendBroadcast(intent);
                imgBtnDial.setVisibility(View.VISIBLE);
                checkBalance();
            }
        });

        // 통화기록 클릭
        tabview2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mTabHost.setCurrentTab(1);
                //showFAB(); //BJH 2016.10.26
                imgBtnDial.setVisibility(View.GONE);
            }
        });

        // 연락처 클릭
        tabContact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mTabHost.setCurrentTab(2);
                //showFAB(); //BJH 2016.10.26
                imgBtnDial.setVisibility(View.GONE);
            }
        });

        // 메시지 클릭
        tabview3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mTabHost.setCurrentTab(3);
                //showFAB(); //BJH 2016.10.26
                imgBtnDial.setVisibility(View.GONE);
            }
        });

        // 설정 클릭
        tabview4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mTabHost.setCurrentTab(4);
                //showFAB(); //BJH 2016.10.26
                imgBtnDial.setVisibility(View.GONE);
            }
        });

        if (getBaseContext() != null) {
            mContextToBindTo = getBaseContext();
        }

        String url = mPrefs.getPreferenceStringValue(AppPrefs.NOTICE_URL);
        if (url != null && url.length() > 0) {
            Log.d(THIS_FILE, "SHOW NOTICE : " + url);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_URL, "");
            String title = mPrefs
                    .getPreferenceStringValue(AppPrefs.NOTICE_TITLE);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_TITLE, "");

            String fb_url = mPrefs
                    .getPreferenceStringValue(AppPrefs.NOTICE_FB_URL);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_FB_URL, "");

            Fcm.cancelNotice(this);

            showNotice(url, fb_url, title);
        }

        String number = mPrefs
                .getPreferenceStringValue(AppPrefs.MESSAGE_NUMBER);
        if (number != null && number.length() > 0) {
            String title = mPrefs
                    .getPreferenceStringValue(AppPrefs.MESSAGE_TITLE);

            mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_NUMBER, "");
            mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_TITLE, "");

            Fcm.cancelMsg(this);

            showMessage(number, title);
        }

        //BJH 2017.05.15
        String mto = mPrefs
                .getPreferenceStringValue(AppPrefs.MTO_NUMBER);
        if (mto != null && mto.length() > 0) {
            mPrefs.setPreferenceStringValue(AppPrefs.MTO_NUMBER, "");
            Fcm.cancelMTO(this);
        }

        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_ANSWER);
        if (cid != null && cid.length() > 0) {
            Log.d(THIS_FILE, "SHOW WIAT FOR : " + cid);
            showProgress();
        }

        //goBridge();//BJH 2016.09.08

        //BJH 2016.10.25
        /*mFL = (FrameLayout) findViewById(R.id.frame_layout);
        mFL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                mActionMenu.collapse();
                return true;
            }
        });
        mActionMenu = (FloatingActionsMenu) findViewById(R.id.fab_action_menu);
        mActionMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {

            @Override
            public void onMenuExpanded() {
                // TODO Auto-generated method stub
                mFL.setVisibility(View.VISIBLE);
                mFL.getBackground().setAlpha(160);
            }

            @Override
            public void onMenuCollapsed() {
                // TODO Auto-generated method stub
                mFL.setVisibility(View.GONE);
                mFL.getBackground().setAlpha(255);
            }
        });
        */
       /* mActionUSIM = (FloatingActionButton) findViewById(R.id.fab_action_usim);
        mActionPlace = (FloatingActionButton) findViewById(R.id.fab_action_place); // BJH 2017.01.02
        mActionAgency = (FloatingActionButton) findViewById(R.id.fab_action_agency); // BJH 2017.03.10
        mActionForwarding = (FloatingActionButton) findViewById(R.id.fab_action_forwarding);
        mActionUserMSg = (FloatingActionButton) findViewById(R.id.fab_action_user_msg);
        mActionPoint = (FloatingActionButton) findViewById(R.id.fab_action_point);*/
        //mActionKakao = (FloatingActionButton) findViewById(R.id.fab_action_kakao);
        //mActionSearch = (FloatingActionButton) findViewById(R.id.fab_action_search);
        //mActionMyinfo = (FloatingActionButton) findViewById(R.id.fab_action_myinfo);
        //mActionFaq = (FloatingActionButton) findViewById(R.id.fab_action_faq);
        //mActionStore = (FloatingActionButton) findViewById(R.id.fab_action_store);
        //mActionNotice = (FloatingActionButton) findViewById(R.id.fab_action_notice);
		/*if(mDistributor_id != null && mDistributor_id.length() > 0 && mDistributor_id.contains("usim"))
			mActionUSIM.setVisibility(View.VISIBLE);*/
        /*mActionUSIM.setOnClickListener(new View.OnClickListener() { // BJH 2017.01.06

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showUSIMNotice(mContext);
                mActionMenu.collapse();
            }
        });

        mActionPlace.setOnClickListener(new View.OnClickListener() { // BJH 2017.01.02

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                receiveUSIM();
                mActionMenu.collapse();
            }
        });
        mActionAgency.setOnClickListener(new View.OnClickListener() { // BJH 2017.01.02

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                goAgency();
                mActionMenu.collapse();
            }
        });
        mActionForwarding.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mtForwarding();
                mActionMenu.collapse();
            }
        });
        mActionUserMSg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                userMsg();
                mActionMenu.collapse();
            }
        });*/
       /* mActionPoint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                myPoint();
                mActionMenu.collapse();
            }
        });*/
        /*mActionKakao.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                kakaoAsk();
                mActionMenu.collapse();
            }
        });*/
        /*mActionSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DialMain.this, Search.class);
                startActivityForResult(intent, SELECT_SEARCH);
            }
        });*/


        /*mActionMyinfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                myInfo();
                mActionMenu.collapse();
            }
        });
        mActionNotice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionNotice();
                mActionMenu.collapse();
            }
        });
        mActionStore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionStore();
                mActionMenu.collapse();
            }
        });
        mActionFaq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionFaq();
                mActionMenu.collapse();
            }
        });
        mActionSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                actionSearch();
                mActionMenu.collapse();
            }
        });
        */

        //getAppKeyHash();

        mRttAlertHandler = new Handler(); // BJH 2018.02.01 상황별 안내 상황
        mRttAlertHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!reg_state_online && SipService.RTT == 0) {
                    rttAlert();
                } else if(!reg_state_online && SipService.RTT > 0) {
                    checkRegState();
                }
            }
        }, 6000); // sx_t4(5000) 뒤에 체크

        if (extras!=null) {
            String callType = extras.getString(DialMain.EXTRA_SCHEME_CALL_TYPE);
            String schemeCallNumber = extras.getString(DialMain.EXTRA_SCHEME_CALL_NUMBER);

            if(callType!=null&&schemeCallNumber!=null&& callType.trim().length()>0 && schemeCallNumber.trim().length()>0){
                if (callType.equals("call")){
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, schemeCallNumber);
                } else if (callType.equals("dial")){
                    App.isDialClicked=true;
                    ServicePrefs.DID_EXPIRED=DID_EXPIRED;
                    App.isReturnClicked=false;
                    mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                    App.isReturnContactClicked=false;
                    Intent intentDial2=new Intent(DialMain.this, Dial.class);
                    intentDial2.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,callType);
                    intentDial2.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemeCallNumber);
                    startActivity(intentDial2);
                }
            }

            String action = extras.getString("action");
            if (action!=null&& action.equals(SipManager.ACTION_SIP_CALLLOG)){
//                Log.e(THIS_FILE,"action:"+action);
                DebugLog.d("action --> "+action);
                msgHandler.sendMessage(msgHandler.obtainMessage(SET_TAB, 1, 0));
            }

            String type = extras.getString("type");
            if (type!=null&& type.equals("BridgeReturn")){
//                Log.e(THIS_FILE,"type:"+type);
                DebugLog.d("type --> "+type);
                Intent intentDial2 = new Intent(DialMain.this, Dial.class);
                startActivity(intentDial2);
            }
        }

        DebugLog.d("test_pushy pushy token: "+PushyMQTT.mRegistrationID);

        checkMandatoryPermissions();

    }

    private boolean exceptionlife = false;

    @Override
    protected void onNewIntent(Intent intent) {
//        Log.d(THIS_FILE, "onNewIntent : START");
        DebugLog.d("onNewIntent start --> ");
        super.onNewIntent(intent);

        exceptionlife = true;

        if (intent == null) {
//            Crashlytics.logException(new Exception("onNewIntent  Null !!!"));
            return;
        }

        final Intent _intent = intent;
        Thread t = new Thread() {
            public void run() {
                processNewIntent(_intent);
            }
        };
        if (t != null)
            t.start();

        /**
         *  안드로이드10 관련 2020.05.29 맹완석
         */
        if (Build.VERSION.SDK_INT >= 29) {
            push = intent.getBooleanExtra(DialMain.EXTRA_PUSH,false);
            if (push || ServicePrefs.mPush){
                setShowWhenLocked(true);
                /*
                this.getWindow().setFlags(
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

                 */

                boolean isAndroid10Clicked=intent.getBooleanExtra("ANDROID10_CLICK",false);
                /*
                if (isAndroid10Clicked){
                        setErrorPopup();
                }
                */
            }
        }


        if (_intent.getBooleanExtra("EXTRA_FINISH",false)){
            DebugLog.d("finishAffinity() --> ");
            finishAffinity();
        }

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
//            Log.d(THIS_FILE, "url scheme 실행");
            DebugLog.d("URL Scheme execute --> ");
            Uri uri = intent.getData();
            if(uri != null) {
                if (uri.getHost().equals("dial")){
                    String schemePhoneNumber=uri.getQueryParameter("param1");
                    if (schemePhoneNumber!=null && schemePhoneNumber.trim().length()>0){
                        if (schemePhoneNumber.trim().startsWith("+")){
                            schemePhoneNumber="+"+schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                        }else{
                            schemePhoneNumber=schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                        }

                        Log.d(THIS_FILE, "url scheme 실행 :"+ uri.getHost() +","+schemePhoneNumber);
                        getTabHost().setCurrentTab(0);
                        /*Intent intentDial=new Intent(Dial.BC_DIAL);
                        intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemePhoneNumber);
                        sendBroadcast(new Intent(intentDial));*/

                        App.isDialClicked=true;
                        ServicePrefs.DID_EXPIRED=DID_EXPIRED;
                        App.isReturnClicked=false;
                        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
                        App.isReturnContactClicked=false;
                        Intent intentDial2=new Intent(DialMain.this, Dial.class);
                        intentDial2.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,"dial");
                        intentDial2.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemePhoneNumber);
                        startActivity(intentDial2);
                    }
                }else if (uri.getHost().equals("call")){
                    String schemePhoneNumber=uri.getQueryParameter("param1");
                    if (schemePhoneNumber!=null && schemePhoneNumber.trim().length()>0){
                        if (schemePhoneNumber.trim().startsWith("+")){
                            schemePhoneNumber="+"+schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                        }else{
                            schemePhoneNumber=schemePhoneNumber.trim().replaceAll("[^0-9]", "");
                        }

                        Log.d(THIS_FILE, "url scheme 실행 :"+ uri.getHost() +","+schemePhoneNumber);
                        getTabHost().setCurrentTab(0);
                        /*Intent intentDial=new Intent(Dial.BC_CALL);
                        intentDial.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemePhoneNumber);
                        sendBroadcast(new Intent(intentDial));*/
                        mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, schemePhoneNumber);
                    }
                }
            }
        }

        DebugLog.d("onNewIntent --> END");

//        Log.d(THIS_FILE, "onNewIntent : END");
    }

    private void processNewIntent(Intent intent) {
        if (intent == null)
            return;

        // for test
        // mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, "1011");
        int notification_id = intent.getIntExtra("id", 0);
        if (notification_id > 0) {
            NotificationManager notificationManager = (NotificationManager) this.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notification_id);
        }
        String action = intent.getAction();
        Bundle extras = intent.getExtras();

        DebugLog.d("processNewIntent start action --> "+action);

        if (action != null) {
            if (action.equals(SipManager.ACTION_SIP_DIALER)) {
                if (extras != null) {
                    String cid = extras.getString(DialMain.EXTRA_GCM_CID);
                    if (cid != null && cid.length() > 0) {
//                        Log.d(THIS_FILE, "SET AUTO ANSWER : " + cid);
                        DebugLog.d("SET AUTO ANSWER cid --> "+cid);
                        mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER,
                                cid);
                        //BJH java.lang.RuntimeException
                        Looper.prepare();
                        showProgress();
                        Looper.loop();
                    }

                    String url = extras.getString(DialMain.EXTRA_NOTICE_URL);
                    if (url != null && url.length() > 0) {
                        String title = extras
                                .getString(DialMain.EXTRA_NOTICE_TITLE);
                        String fb_url = extras
                                .getString(DialMain.EXTRA_NOTICE_FB_URL);
                        showNotice(url, fb_url, title);
                    }

                    String number = extras
                            .getString(DialMain.EXTRA_MESSAGE_NUMBER);
                    if (number != null && number.length() > 0) {
                        String title = extras
                                .getString(DialMain.EXTRA_MESSAGE_TITLE);
                        showMessage(number, title);
                    }

                }
                return;
            } else if (action.equals(SipManager.ACTION_SIP_CALLLOG)) {
                msgHandler.sendMessage(msgHandler.obtainMessage(SET_TAB, 1, 0));
            } else if (action.equals("android.intent.action.MAIN")) { // BJH 2016.12.08
                /*Looper.prepare();
                //usimCheck(mContext, mPrefs);
                setServiceState();
                Looper.loop();*/
            }else if (action.equals(Intent.ACTION_VIEW)) {
                if (!ServicePrefs.mLogin) {
                    Uri uri = intent.getData();
                    if(uri != null) {
                        if (uri.getHost().equals("dial") || uri.getHost().equals("call")){
                            String schemePhoneNumber=uri.getQueryParameter("param1");
                            if (schemePhoneNumber!=null && schemePhoneNumber.trim().length()>0){

                            }

                        }
                    }
                }
            }
        } else {
            if (extras != null) {
                String type = extras.getString("type");
                if (type!=null&& type.equals("BridgeReturn")){
//                    Log.e(THIS_FILE,"type:"+type);
                    DebugLog.d("type --> "+type);
                    Intent intentDial2=new Intent(DialMain.this, Dial.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intentDial2);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
//        Log.d(THIS_FILE, "onDestroy");
        DebugLog.d("onDestroy");

//        unbindFromService();

        if (currentStopState == 1) {
            // STOP 이후만.
            if (mDatabase != null) {
                if (mDatabase.isOpen())
                    mDatabase.close();
            }

            if (defaultUEH != null) {
                Thread.setDefaultUncaughtExceptionHandler(defaultUEH);
                defaultUEH = null;
            }

            currentContext = null;
            doAppExit();

            ServicePrefs.mLogin=false;  //강제 종료뒤 말톡 시작시 스플래쉬화면을 안타서 강제종료시 해당 플래그값을 false로 초기화
        }

        ServicePrefs.mPush=false;
        bMaaltalkRunned=false;
        super.onDestroy();
    }

    private void doAppExit() {
        if (isExit) {
            Log.d(THIS_FILE, "APP EXIT : SERVICE");
            // STOP APN SERVICE
            stopApnService();

            // STOP MESSAGE SERVICE
            stopMessageService();

            // STOP SIP SERVICE

            stopSipService();

			/*
			 * if (SipService.currentService != null) { SipService
			 * currentService = SipService.currentService;
			 * currentService.stopSipStack(); currentService.stopSelf();
			 * SipService.currentService = null; }
			 */

            if (ServicePrefs.mLogin) {
                ServicePrefs.logout();
            }

            // FOR SAFE
            SipService.currentService = null;

            // Log.d(THIS_FILE, "APP EXIT : CHECK SERVICE");
            // int count = 0;
            // while (SipService.currentService != null) {
            // try {
            // Thread.sleep(100);
            // } catch (InterruptedException e) {
            // e.printStackTrace();
            // }
            //
            // if (count++ > 20)
            // break;
            // }

            Log.d(THIS_FILE, "APP EXIT !!");
            // Process.killProcess(Process.myPid());
        }
    }

    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onPause");
        currentState = 0;
        // shlee move the unbind to stop
/*
        unbindFromService();
        if(reg_state_online == false)
        checkRegState();
        stopBalance();
*/
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        ContactHelper.listClear();
        super.onResume();
        Log.d(THIS_FILE, "onResume");
        currentContext = this;
        currentState = 1;
        // shlee add checkbalance
        checkBalance();

        // if (autoClose) {
        // Log.d(THIS_FILE, "AUTO CLOSE !!");
        // autoClose = false;
        // disconnectAndQuit(true);
        // return;
        // }

        // shlee move to onstart
/*
        bindToService();
        if(reg_state_online == false)
        checkRegState();

        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_ANSWER);
        if (cid == null || cid.length() == 0) {
            hideProgres();
        }

        if (checkAuth())
            return;

        //checkBalance();
        updateBadge();

 */


    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onStart");
        currentStopState = 0;
        super.onStart();

        // shlee move here from onresume

        DebugLog.d("test_rtt in DialMain onStart called.");

        bindToService();
        if(reg_state_online == false)
            checkRegState();

        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_ANSWER);
        if (cid == null || cid.length() == 0) {
            hideProgres();
        }

        if (checkAuth())
            return;

       // checkBalance();
        updateBadge();


    }

    @Override
    protected void onStop() {
        Log.d(THIS_FILE, "onStop");

        currentStopState = 1;

        cancelAutoClose();

        // shlee move to here from onpause

        unbindFromService();

        if(reg_state_online == false)
            checkRegState();
        stopBalance();

        //BJH 2016.11.11
        if (mThreadTimer != null) {
            mThreadTimer.interrupt();
            mThreadTimer = null;
        }

        //BJH 2018.01.29
        if(mRttAlertHandler != null) {
            mRttAlertHandler.removeCallbacksAndMessages(null);
            mRttAlertHandler = null;
        }

        if (skipClose) {
            skipClose = false;
            super.onStop();
            return;
        }

        if (ServicePrefs.DIAL_AUTO_EXIT) {
            boolean rs = isApplicationSentToBackground(this);
            if (rs) {
                Log.d(THIS_FILE, "Application Sent To Background !!");
                if (SipService.currentService != null
                        && SipService.pjService != null) {
                    if (SipService.pjService.getActiveCallInProgress() == null) {
                        isExit = true;
                        doAppExit();
                        super.onStop();
                        return;
                    }
                }
            }
        }

        super.onStop();
    }

    private ProgressDialog progressDialog = null;
    private Handler autoHideHandler = null;
    private Runnable autoHideRunnable = null;

    private void showProgress() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int state = telephonyManager.getCallState();
        if (state != TelephonyManager.CALL_STATE_IDLE) {
            mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, ""); // REMOVE
            Toast toast = Toast.makeText(mContext,
                    mContext.getResources().getString(R.string.call_cancel_by_busy), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(mContext, mContext.getResources().getString(R.string.call_incomming_request),
                    mContext.getResources().getString(R.string.call_incomming_process), true, true,
                    new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            mPrefs.setPreferenceStringValue(
                                    AppPrefs.AUTO_ANSWER, ""); // REMOVE
                            progressDialog = null;
                        }
                    });
            progressDialog.show();
            // SET TIMEOUT

            autoHideRunnable = new Runnable() {
                @Override
                public void run() {

                    Toast toast = Toast.makeText(mContext,
                            mContext.getResources().getString(R.string.call_incomming_fail),
                            Toast.LENGTH_SHORT);
                    toast.show();

                    hideProgres();

                }
            };

            autoHideHandler = new Handler();
//            autoHideHandler.postDelayed(autoHideRunnable, 10000);
        }
    }

    private void hideProgres() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (autoHideHandler != null) {
            autoHideHandler.removeCallbacks(autoHideRunnable);
            autoHideHandler = null;
            autoHideRunnable = null;
        }

        if (mPrefs != null) {
            mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, ""); // REMOVE
//            Thread t = new Thread() {
//                public void run() {
//                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, ""); // REMOVE
//                }
//            };
//            if (t != null)
//                t.start();
        }
    }

    private void setupTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
    }

    @SuppressLint("InflateParams")
    private static View createTabView(final Context context, final String tag) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.tabs_bg, null);
        ImageView imgTab=view.findViewById(R.id.imgTab);
        TextView txtTitle=view.findViewById(R.id.txtTitle);
        if (tag.equalsIgnoreCase(TAB_DIAL_TAG)) {
            /*AppPrefs prefs = new AppPrefs(context);
            mDistributor_id = prefs.getPreferenceStringValue(AppPrefs.DISTRIBUTOR_ID);
            if (mDistributor_id != null && mDistributor_id.length() > 0 && mDistributor_id.equalsIgnoreCase("a7000")) {
                view.setBackgroundResource(R.drawable.tab_wide);
                mAppNameText.setText(context.getResources().getString(R.string.widetalk_name));
            } else
                view.setBackgroundResource(R.drawable.tab_dial);*/
            imgTab.setBackgroundResource(R.drawable.tab_dial);
            txtTitle.setText(context.getString(R.string.home));
        } else if (tag.equalsIgnoreCase(TAB_CALLS_TAG)) {
            //view.setBackgroundResource(R.drawable.tab_calls);
            imgTab.setBackgroundResource(R.drawable.tab_calls);
            txtTitle.setText(context.getString(R.string.recent_call));
        }else if (tag.equalsIgnoreCase(TAB_CONTACT_TAG)) {
            //view.setBackgroundResource(R.drawable.tab_calls);
            imgTab.setBackgroundResource(R.drawable.tab_contact);
            txtTitle.setText(context.getString(R.string.contact));
        }else if (tag.equalsIgnoreCase(TAB_MESSAGE_TAG)) {
            //view.setBackgroundResource(R.drawable.tab_message);
            imgTab.setBackgroundResource(R.drawable.tab_message);
            txtTitle.setText(context.getString(R.string.message));
        }
        else if (tag.equalsIgnoreCase(TAB_SETTING_TAG)) {
            //view.setBackgroundResource(R.drawable.tab_setting);
            imgTab.setBackgroundResource(R.drawable.tab_setting);
            txtTitle.setText(context.getString(R.string.setting));
        }
        return view;
    }

    private BroadcastReceiver dialCommandReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            DebugLog.d("test_v2 ACTION_DIAL_COMMAND dialCommandReceiver onReceive in DialMain");

            String command = intent.getStringExtra(EXTRA_COMMAND);
            DebugLog.d("DialCommandReceiver command --> "+command);
//            Log.i(THIS_FILE,"onReceive:"+command);
            if (command == null)
                return;
            if (command.equalsIgnoreCase("CALL")) {
                DebugLog.d("command --> CALL");
                FirebaseCrashlytics.getInstance().setCustomKey("outbound_command_call", true);

                String number = intent.getStringExtra(EXTRA_CALL_NUMBER);
                if (number != null && number.length() > 0) {
                    FirebaseCrashlytics.getInstance().setCustomKey("outbound_call_number", number);
                    sipCall(number);
                } else {
                    FirebaseCrashlytics.getInstance().setCustomKey("outbound_call_number", "");
                }
            } else if (command.equalsIgnoreCase("BALANCE")) {
                DebugLog.d("command --> BALANCE");
                int balance = intent.getIntExtra(EXTRA_BALANCE, 0);
                // 잔액 표시
                //mBalanceText.setText(String.format("%s %d", getResources().getString(R.string.balance), balance));
            } else if (command.equalsIgnoreCase("CHECK_BALANCE")) {
                DebugLog.d("command --> CHECK_BALANCE");
                checkBalance();
            } else if (command.equalsIgnoreCase("NOTICE")) {
                DebugLog.d("command --> NOTICE");
                String url = intent.getStringExtra(DialMain.EXTRA_NOTICE_URL);
                if (url != null && url.length() > 0) {
                    String title = intent
                            .getStringExtra(DialMain.EXTRA_NOTICE_TITLE);
                    String fb_url = intent
                            .getStringExtra(DialMain.EXTRA_NOTICE_FB_URL);
                    showNotice(url, fb_url, title);
                }
            } else if (command.equalsIgnoreCase("MESSAGE")) {
                DebugLog.d("command --> MESSAGE");
                String number = intent
                        .getStringExtra(DialMain.EXTRA_MESSAGE_NUMBER);
                if (number != null && number.length() > 0) {
                    String msg = intent
                            .getStringExtra(DialMain.EXTRA_MESSAGE_TITLE);
                    showMessage(number, msg);
                }
                // }else if (command.equalsIgnoreCase("LOGOUT")) {
                // disconnectAndQuit(false);
            } else if (command.equalsIgnoreCase("QUIT")) {
                DebugLog.d("command --> QUIT");
                disconnectAndQuit(true);
            } else if (command.equalsIgnoreCase("KEYPAD")) {
                DebugLog.d("command --> KEYPAD");
                mTabHost.setCurrentTab(0);
                //showFAB(); //BJH 2016.10.26
                imgBtnDial.setVisibility(View.VISIBLE);
            }
        }
    };


    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(MessageService.ACTION_NEW_MESSAGE)) {
                // Badge Update
                msgHandler.sendEmptyMessage(UPDATE_BADGE);
            }
        }
    };

	/*
	 * private void stopGps() { if (ServicePrefs.checkGps(mContext)) {
	 * AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
	 * msgDialog.setTitle("GPS");
	 * msgDialog.setMessage(getResources().getString(R.string.gps_quest));
	 * msgDialog.setButton(mContext.getResources().getString(R.string.yes), new
	 * DialogInterface.OnClickListener() {
	 *
	 * @Override public void onClick(DialogInterface dialog, int which) {
	 * DialMain.skipClose = true; Intent intent = new
	 * Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	 * intent.addCategory(Intent.CATEGORY_DEFAULT); startActivity(intent);
	 * return; } });
	 * msgDialog.setButton2(mContext.getResources().getString(R.string.no), new
	 * DialogInterface.OnClickListener() {
	 *
	 * @Override public void onClick(DialogInterface dialog, int which) { // if
	 * (mProvider != null) // { // if (mLocMan != null) // { //
	 * mLocMan.requestLocationUpdates(mProvider, MIN_TIME, // MIN_DISTANCE,
	 * mLocationListener); // } // } return; } }); msgDialog.show(); } }
	 */

    private boolean isExit = false;

    private void disconnectAndQuit(boolean terminate) {
        isExit = terminate;

        if (!terminate) {
            // GO INTRO
            Intent IntroIntent = new Intent(this, DialSplash.class);
            IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(IntroIntent);
        }
        finish();
    }

    private boolean reg_state_online = false;

    private void checkRegState() {
//        try {
//            Log.i(THIS_FILE,"checkRegState");
//            reg_state_online = checkOnline();
//            msgHandler.sendEmptyMessage(UPDATE_REG_STATE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        Thread t = new Thread() {
            public void run() {
                try {
                    Log.i(THIS_FILE,"checkRegState");
                    reg_state_online = checkOnline();
                    msgHandler.sendEmptyMessage(UPDATE_REG_STATE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        if (t != null)
            t.start();
    }

    /*private void updateRegState() {
        if (reg_state_online) {
            if (mAntena != null) {
                int RTT = SipService.RTT;
                int LOSS = SipService.LOSS;
                int antena = R.drawable.antena_0;
                if (RTT < 100)
                    antena = R.drawable.antena_3;
                else if (RTT < 200)
                    antena = R.drawable.antena_2;
                else
                    antena = R.drawable.antena_1;

                mAntena.setBackgroundResource(antena);
                mAppText.setText("");

                if (LOSS != 0)
                    mRTTText.setText(" " + RTT + "ms (" + LOSS + ")");
                else
                    mRTTText.setText(" " + RTT + "ms");
            }
        } else {
            if (mAntena != null) {
                mAntena.setBackgroundResource(R.drawable.antena_0);
                mAppText.setText("");
                mRTTText.setText("");
            }
        }
    }
    */

    private void updateRegState() {
        DebugLog.d("updateRegState()");

        Intent intentDial=new Intent(Dial.BC_UPDATE_REG_STATE);
        if (reg_state_online) {
            /*int RTT = SipService.RTT;
            int LOSS = SipService.LOSS;
            intentDial.putExtra("RTT",RTT);
            intentDial.putExtra("LOSS",LOSS);*/
            intentDial.putExtra("ONLINE_STATE",true);
        }else{
            intentDial.putExtra("ONLINE_STATE",false);
        }
        intentDial.setPackage(getPackageName());
        sendBroadcast(new Intent(intentDial));
    }

    private BroadcastReceiver regStateReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            checkRegState();
        }
    };

    private Context mContextToBindTo = this;
    private ISipService mSipService = null;
    private ServiceConnection mSipConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
//            Log.d(THIS_FILE, "onServiceConnected");

            DebugLog.d("onServiceConnected (ServiceConnection --> ISipService)");

            mSipService = ISipService.Stub.asInterface(arg1);
            checkRegState();
            checkCall(true);
            checkAutoDial();

            long mtEndTime = System.currentTimeMillis();
            long mtStartTime = Long.parseLong(mPrefs.getPreferenceStringValue(AppPrefs.MT_STARTTIME));
//            Log.d(THIS_FILE,"process time check end:"+mtEndTime);
//            Log.w(THIS_FILE, "process time check complete : " + ( mtEndTime - mtStartTime )/1000.0 +" sec");

            DebugLog.d("process time check end:"+mtEndTime);
            DebugLog.d("process time check complete : " + ( mtEndTime - mtStartTime )/1000.0 +" sec");

            //mPrefs.setPreferenceStringValue(AppPrefs.MT_ENDTIME,mtEndTime+"");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
//            Log.d(THIS_FILE, "onServiceDisconnected");
            DebugLog.d("onServiceDisconnected...");
            mSipService = null;
            checkRegState();
        }
    };

    private boolean checkOnline() {
        DebugLog.d("checkOnline()");
        if (mSipService == null) {
//            Toast.makeText(DialMain.this, "checkOnline condition1", Toast.LENGTH_SHORT).show();
            DebugLog.d("test_v2 checkOnline condition 1");
            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_online_sip_service", false);
            return false;
        }
        FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_online_sip_service", true);
        SipProfileState accountInfo = null;
        try {
            accountInfo = mSipService.getSipProfileState(0);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
//            Toast.makeText(DialMain.this, "checkOnline condition2", Toast.LENGTH_SHORT).show();
            DebugLog.d("test_v2 checkOnline condition 2");
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(new Exception(e.getMessage()));
            return false;
        }
//        if(accountInfo==null) {
////            Toast.makeText(DialMain.this, "checkOnline condition3", Toast.LENGTH_SHORT).show();
//            DebugLog.d("test_v2 checkOnline condition 3");
//            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_online_account_info", false);
//        } else {
//            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_online_account_info", true);
//            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_online_account_info_is_valid_for_call", accountInfo.isValidForCall());
//        }

//        if(accountInfo!=null) {
//            if(!accountInfo.isValidForCall()) {
////                Toast.makeText(DialMain.this, "checkOnline condition4", Toast.LENGTH_SHORT).show();
//                DebugLog.d("test_v2 checkOnline condition 4");
//            }
//        }

        return true;

//        return (accountInfo != null && accountInfo.isValidForCall());
    }

    private boolean checkCall(boolean showCallView) {
        DebugLog.d("checkCall()");
        if (mSipService == null) {
//            Toast.makeText(DialMain.this, "checkCall condition1: SipService is null", Toast.LENGTH_SHORT).show();
            DebugLog.d("test_v2 checkOnline condition 1");
            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_call_sip_service", false);
            return false;
        }
        boolean skipCallView = DialMain.skipCallview;

        SipCallSession[] callsInfo = null;
        try {
            callsInfo = mSipService.getCalls();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
//            Toast.makeText(DialMain.this, "checkCall condition2: getCalls Exception invoked.", Toast.LENGTH_SHORT).show();
            DebugLog.d("test_v2 checkOnline condition 2");
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
            return false;
        }
        if (callsInfo == null) {
//            Toast.makeText(DialMain.this, "checkCall Condition3: callsInfo is null", Toast.LENGTH_SHORT).show();
            DebugLog.d("test_v2 checkOnline condition 3");
            FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_call_calls_info", false);
            checkStack = false;
            return false;
        }
        FirebaseCrashlytics.getInstance().setCustomKey("outbound_check_call_calls_info", true);

        for (SipCallSession callInfo : callsInfo) {
            int state = callInfo.getCallState();

            DebugLog.d("checkCall call state --> "+state);

            switch (state) {
                case SipCallSession.InvState.CALLING:
                case SipCallSession.InvState.INCOMING:
                case SipCallSession.InvState.EARLY:
                case SipCallSession.InvState.CONNECTING:
                case SipCallSession.InvState.CONFIRMED: {
                    if (checkStack || !showCallView) {
                        checkStack = false;
                        // RESTART

                        try {
                            mSipService.askThreadedRestart();
                        } catch (RemoteException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    if (showCallView && !skipCallView) {
                        Intent intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
                        if (App.isReturnClicked){
                            intent = new Intent(SipManager.ACTION_SIP_CALL_RETURN_UI);
                        }

                        intent.putExtra(SipManager.EXTRA_CALL_INFO, callInfo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP /*
															 * | Intent.
															 * FLAG_ACTIVITY_NO_ANIMATION
															 */);

                        intent.setPackage(getPackageName());
                        startActivity(intent);
                    }
                    return true;
                }
                default:
                    break;
            }
        }
        checkStack = false;
        return false;
    }

    //BJH 말벗 전화 성공률 향상을 위해서
    private Handler autoDialHandler = null;
    private Runnable autoDialRunnable = null;

    private void checkAutoDial() {
        DebugLog.d("checkAutoDial()");
        mAutoDial = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_DIAL);
        if (mAutoDial != null && mAutoDial.length() > 0) {
            autoDialRunnable = new Runnable() {
                @Override
                public void run() {
                    mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, "");
                    sipCall(mAutoDial);
                }
            };
            autoDialHandler = new Handler();
            autoDialHandler.postDelayed(autoDialRunnable, 1000);

        }
    }

    private void selectGSMCall(final String number) {
        Log.i(THIS_FILE,"selectGSMCall");
        DebugLog.d("selectGSMCall --> "+number);

        new MaterialAlertDialogBuilder(mContext)
                .setTitle(getResources().getString(R.string.make_gsm_call))
                .setMessage(getResources().getString(R.string.make_gsm_call_question))
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    private void selectGSMCall2(final String number) {
        DebugLog.d("selectGSMCall2 --> "+number);

        new MaterialAlertDialogBuilder(this)
                .setTitle(getResources().getString(R.string.make_gsm_call))
                .setMessage(getResources().getString(R.string.make_gsm_call_question2))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doSipCall(number);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    /*private void selectGCall(final String number) {
        Resources res = mContext.getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
        msgDialog.setTitle(R.string.make_gsm_call);
        msgDialog.setMessage(res.getString(R.string.make_gcall_question));
        msgDialog.setButton(res.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goGCall(mContext, number);
                        return;
                    }
                });
        msgDialog.setButton2(res.getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
        msgDialog.show();
    }

    private void selectGCall2(final String number) {
        Resources res = mContext.getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
        msgDialog.setTitle(R.string.make_gsm_call);
        msgDialog.setMessage(res.getString(R.string.make_gcall_question2));
        msgDialog.setButton(res.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goGCall(mContext, number);
                        return;
                    }
                });
        msgDialog.setButton2(res.getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        doSipCall(number);
                        return;
                    }
                });
        msgDialog.show();
    }*/

    public static String fullNumber(String number, boolean localBypass) {
        if (number == null || number.length() == 0)
            return "";

        number = PhoneNumberUtils.stripSeparators(number);

        // 최소 5자리는 되어야 한다.
        if (number.length() <= 5)
            return number;

        String n1 = number.substring(0, 1);

        if (n1.equals("+"))
            return number;
        if (n1.equals("0")) {
            // 01로 시작
            String n2 = number.substring(1, 2);
            if (n2.equals("0")) {
                // 3자리 번호 : 001, 002, 005, 006, 008
                String n3 = number.substring(2, 3);
                int num;
                try {
                    num = Integer.parseInt(n3);
                    if (num == 1 || num == 2 || num == 5 || num == 6
                            || num == 8)
                        return "+" + number.substring(3, number.length());
                } catch (NumberFormatException e) {
                    return number;
                }

                // 5자리 번호 : 00300, 00700, 00770, 00777, 00365, 00321, 00755,
                // 00707, 00765, 00766, 00782
                String etc = number.substring(2, 5);
                try {
                    num = Integer.parseInt(etc);
                    if (num == 300 || num == 700 || num == 770 || num == 777
                            || num == 365 || num == 321 || num == 755
                            || num == 707 || num == 765 || num == 766
                            || num == 782) {
                        return "+" + number.substring(5, number.length());
                    }
                } catch (NumberFormatException e) {
                    return number;
                }
            } else {
                if (localBypass)
                    return number;
                else
                    return "+82" + number.substring(1, number.length());
            }
        } else {
            return "+" + number;
        }

        return number;
    }

    private static final String GCALL_PACKAGE_NAME = "com.gcall";

    public static void goGCall(Context context, final String number) {
        DebugLog.d("caller --> "+context.getClass().getSimpleName());
        DebugLog.d("number --> "+number);

        String full_number = fullNumber(number, false);

        DialMain.skipClose = true;

        // Package 설치여부 확인
        PackageManager pm = context.getPackageManager();
        try {
            Intent intent = pm.getLaunchIntentForPackage(GCALL_PACKAGE_NAME);
            if (intent != null) {
                intent.setAction(DialMain.ACTION_GCALL_COMMAND);
                intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
                intent.putExtra(DialMain.EXTRA_CALL_NUMBER, full_number);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(intent);
                return;
            }
        } catch (ActivityNotFoundException e) {
            // not found
            return;
        }

        // Goto Play Store
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://details?id=" + GCALL_PACKAGE_NAME)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id="
                            + GCALL_PACKAGE_NAME)));
        }
    }

    private boolean sipCall(final String number) {
        DebugLog.d("sipCall() --> "+number);

        FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call", true);

        Thread t = new Thread() {
            public void run() {
                try {
                    sipCallProcess(number);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        if (t != null)
            t.start();
        return true;
    }

    private boolean sipCallProcess(String number) {
//        Log.i(THIS_FILE,"sipCallProcess:"+number);
        DebugLog.d("sipCallProcess() --> "+number);
        // check number

        FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call_process", true);

        if (number == null) {
            return false;
        }
        final String toCall = PhoneNumberUtils.stripSeparators(number);

        // check service
        if (mSipService == null) {
//            Log.i(THIS_FILE, "SERIVE BIND ERROR");
            DebugLog.d("SERVICE BIND ERROR");
            FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call_process_exit", "SipService is not initialized.");
            FirebaseCrashlytics.getInstance().recordException(new Exception("outbound SipService is not Initialized."));

            // Message Box
            // msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG,
            // R.string.sip_make_call_error, 0));
            msgHandler.sendMessage(msgHandler
                    .obtainMessage(SELECT_CALL, toCall));
            return false;
        }

        if (!checkOnline()) {
            FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call_process_exit", "checkOnline return false.");
            FirebaseCrashlytics.getInstance().recordException(new Exception("outbound checkOnline return false."));

            msgHandler.sendMessage(msgHandler
                    .obtainMessage(SELECT_CALL, toCall));
            return false;
        }

        if (!App.isReturnClicked){
            if (checkCall(false)) {
                FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call_process_exit", "checkCall return false.");
                FirebaseCrashlytics.getInstance().recordException(new Exception("checkCall return false"));

                try {
                    mSipService.hangup(-1, 0);
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                msgHandler.sendMessage(msgHandler
                        .obtainMessage(SELECT_CALL, toCall));
                return false;
            }
        }


        if (ServicePrefs.isCheckRTT()) {
            if (SipService.RTT >= 1000) {
                FirebaseCrashlytics.getInstance().setCustomKey("outbound_sip_call_process_exit", "rtt value too much: "+SipService.RTT);
                FirebaseCrashlytics.getInstance().recordException(new Exception("rtt value too much"));
                msgHandler.sendMessage(msgHandler.obtainMessage(SELECT_CALL2,
                        toCall));
                return false;
            }
        }

        FirebaseCrashlytics.getInstance().recordException(new Exception("outbound test"));

        doSipCall(toCall);

        return true;
    }

    /*
     * private void doSipCall(final String number) { Thread t = new Thread() {
     *
     * @Override public void run() { try { SipProfileState accountInfo;
     * accountInfo = mSipService.getSipProfileState(0); if (accountInfo != null
     * && accountInfo.isValidForCall()) { mSipService.makeCall(number, 0); }
     * else { Log.e(THIS_FILE, "Service can't be called to make the call");
     * //Message Box
     * //msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG,
     * R.string.sip_make_call_error, 0));
     * msgHandler.sendMessage(msgHandler.obtainMessage(SELECT_CALL, number)); }
     * } catch (RemoteException e) { Log.e(THIS_FILE,
     * "Service can't be called to make the call");
     * //msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG,
     * R.string.sip_make_call_error, 0));
     * msgHandler.sendMessage(msgHandler.obtainMessage(SELECT_CALL, number)); }
     * } }; if (t != null) { t.setPriority(Thread.MIN_PRIORITY); t.start(); } }
     */
    private void doSipCall(final String number) {
        DebugLog.d("doSipCall() --> "+number);

        DialMain.skipClose = true;

        Intent intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
        intent.putExtra(SipManager.EXTRA_CALL_NUMBER, number);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP /*
												 * | Intent.
												 * FLAG_ACTIVITY_NO_ANIMATION
												 */);
        intent.setPackage(getPackageName());
        startActivity(intent);
    }

    // private void gsmCall(String number) {
    // DialMain.skipClose = true;
    //
    // Log.i(THIS_FILE, "gsmCall : " + number);
    // Intent intent = new Intent(Intent.ACTION_CALL,
    // Uri.parse(String.format("tel:%s", number)));
    // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    // startActivity(intent);
    // }

    // BJH StartForeground 서비스가 실행되지 않을 때만 startService 하도록
    private void StartForegroundService() { // BJH 2018.03.09 오레오 부턴 문제가 있음
        /*if (Build.VERSION.SDK_INT >= 26)
            return;*/

        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningServiceInfo> procInfos = am.getRunningServices(Integer.MAX_VALUE);
        boolean package_running = false;
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).service.getClassName().equals(START_FOREGROUND_PACKAGE))
                package_running = true;
        }
        if (!package_running) {
//            Log.d(THIS_FILE, "StartForeground SERVICE START !!");
            DebugLog.d("StartForeground SERVICE START!! --> StartForegroundService.class");
            Intent intent = new Intent(DialMain.this, StartForegroundService.class);
            if (Build.VERSION.SDK_INT >= 26)
                startForegroundService(intent);
            else
                startService(intent);
        }
    }

    private void startMessageService() {
//        Log.d(THIS_FILE, "MESSAGE SERVICE START !!");
        DebugLog.d("MESSAGE SERVICE START !! --> MessageService.class");
        Intent intent = new Intent(DialMain.this, MessageService.class);
        intent.setAction(MessageService.ACTION_START);
        startService(intent);
    }

    private void stopMessageService() {
//        Log.d(THIS_FILE, "MESSAGE SERVICE STOP !!");
        DebugLog.d("MESSAGE SERVICE STOP !! --> MessageService.class");
        stopService(new Intent(DialMain.this, MessageService.class));
    }

    private void startSipService() {
//        Log.d(THIS_FILE, "SIP SERVICE START !!");
        DebugLog.d("SIP SERVICE START --> SipService.class");

//        if(!ServiceUtils.isRunningService(this, SipService.class)) {
//            startService(new Intent(mContextToBindTo, SipService.class));
//        }

        if(!ServiceUtils.isRunningService(this, SipService.class)) {
            DebugLog.d("AppLife in DialMain startSipService service is not running...");
            startService(new Intent(mContextToBindTo, SipService.class));
        } else {
            DebugLog.d("AppLife in DialMain startSipService service is already running...");
        }
//        startService(new Intent(mContextToBindTo, SipService.class));
        mContextToBindTo.bindService(new Intent(mContextToBindTo, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);

        Thread t = new Thread() {
            public void run() {
                final App app= (App) getApplicationContext();
                app.getServerInfoJsonVersion();

                String latestClientVer = mPrefs.getPreferenceStringValue(AppPrefs.LATEST_CLIENT_VER);
                Log.d(THIS_FILE, "latestClientVersion: "+ latestClientVer);
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e(THIS_FILE, "NameNotFoundException:", e);
                }

                String version = pInfo.versionName;
                Log.d(THIS_FILE, "current client version: "+ version);

                /*final String[] arrayLatestVersion = latestClientVer.split("\\.");
                final String[] arrayCurrentVersion = version.split("\\.");*/

                if (latestClientVer!=null && latestClientVer.length()>0 && !latestClientVer.equals("0.0") && latestClientVer.compareTo(version) > 0) {
                    Handler mHandler = new Handler(Looper.getMainLooper());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (mAlertDialogUpdate!=null){
                                if (mAlertDialogUpdate.isShowing()){
                                    mAlertDialogUpdate.dismiss();
                                }
                            }

                            if (builderUpdate ==null){
                                builderUpdate = new AlertDialog.Builder(
                                        DialMain.this);
                            }
                            //LayoutInflater inflater = DialMain.this.getLayoutInflater();
                            //View view_svr = inflater.inflate(R.layout.version_dialog, null);
                            //builderUpdate.setView(view_svr);
                            builderUpdate.setMessage("말톡을 최신버전으로 업데이트해주세요.");
                            builderUpdate.setPositiveButton(
                                    getResources().getString(R.string.version_update),
                                    new DialogInterface.OnClickListener() {
                                        // 사용 안함
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            try {
                                                DialMain.this.startActivity(new Intent(
                                                        Intent.ACTION_VIEW,
                                                        Uri.parse("market://details?id="
                                                                + DialMain.MAALTALK_PACKAGE_NAME)));
                                            } catch (android.content.ActivityNotFoundException anfe) {
                                                DialMain.this.startActivity(new Intent(
                                                        Intent.ACTION_VIEW,
                                                        Uri.parse(DialMain.MAALTALK_APK_URL)));
                                            }
                                        }
                                    });

                            builderUpdate.setNegativeButton(
                                    getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            // TODO Auto-generated method stub
                                            dialog.cancel();
                                            app.requestNewGlobalInfo(DialMain.this);
                                        }
                                    });

												/*if (arrayLatestVersion[0].compareTo(arrayCurrentVersion[0])<=0 && arrayLatestVersion[1].compareTo(arrayCurrentVersion[1])<=0){	//선택 업데이트로 취소버튼 보임

												}else{	//필수 업데이트로 취소버튼 안보임

												}*/

                            builderUpdate.setTitle(DialMain.this.getResources().getString(
                                    R.string.title_new_version));
                            mAlertDialogUpdate=builderUpdate.create();
                            mAlertDialogUpdate.show();
                        }
                    });
                }else{
                    app.requestNewGlobalInfo(DialMain.this);
                }
            }
        };
        if (t != null)
            t.start();
    }

    private void stopSipService() {
//        Log.d(THIS_FILE, "SIP SERVICE STOP !!");
        DebugLog.d("SIP SERVICE STOP --> SipService.class");
        stopService(new Intent(mContextToBindTo, SipService.class));
    }

    private void startGCMService() {
        DebugLog.d("START FCM --> Fcm.Register()");
        Fcm.Register(DialMain.this);
    }

    private void startPushyService() {
        DebugLog.d("START PUSHY SERVICE --> Pushy.listen() / PushyMQTT.Register()");
        Pushy.listen(this);
        PushyMQTT.Register(this);
    }

    // BJH ApnService
    private void stopApnService() {
//        Log.d(THIS_FILE, "APN SERVICE STOP !!");/
        DebugLog.d("STOP APN SERVICE --> ApnService.class");
        stopService(new Intent(DialMain.this, ApnService.class));
    }

    private void bindToService() {
//        Log.d(THIS_FILE, "REGISTER : SIP REGISTRATION CHANGED");
        DebugLog.d("bindToService() called");
        DebugLog.d("REGISTER --> SIP REGISTRATION CHANGED");
        if (intentFilterACTION_SIP_REGISTRATION_CHANGED==null){
            intentFilterACTION_SIP_REGISTRATION_CHANGED = new IntentFilter(SipManager.ACTION_SIP_REGISTRATION_CHANGED);
            registerReceiver(regStateReceiver, intentFilterACTION_SIP_REGISTRATION_CHANGED);
        }

//        Log.d(THIS_FILE, "REGISTER : DIAL COMMAND RECEIVER");
        DebugLog.d("REGISTER --> DIAL COMMAND RECEIVER");
        if (intentFilterACTION_DIAL_COMMAND==null){
            intentFilterACTION_DIAL_COMMAND = new IntentFilter(ACTION_DIAL_COMMAND);
            registerReceiver(dialCommandReceiver, intentFilterACTION_DIAL_COMMAND);
        }

//        Log.d(THIS_FILE, "REGISTER : MESSAGE RECEIVER");
        DebugLog.d("REGISTER --> MESSAGE RECEIVER");
        registerReceiver(messageReceiver, new IntentFilter(MessageService.ACTION_NEW_MESSAGE));

        DebugLog.d("WE CAN NOW start SIP service");
        startSipService();

        DebugLog.d("WE CAN NOW start Pushy service");
        startPushyService();

        DebugLog.d("WE CAN NOW start GCM service");
        startGCMService();

        DebugLog.d("WE CAN NOW start MESSAGE service");
        startMessageService();

        //BJH
        if (mPrefs.getPreferenceBooleanValue(AppPrefs.START_FOREGROUND)) {
            DebugLog.d("WE CAN NOW start Start Foreground");
            StartForegroundService();
        }
    }

    private void unbindFromService() {
        DebugLog.d("unbindFromService called");
        if (mSipService != null) {
            try {
                mContextToBindTo.unbindService(mSipConnection);
            } catch (Exception e) {
                // Just ignore that -- TODO : should be more clean
                DebugLog.d("Unable to un bind --> "+e.getMessage());
            }
        }

        /**
         * mod
         */
//        stopSipService();

        try {
            if (!App.isDialForeground && !App.isDialClicked){
                DebugLog.d("UNREGISTER --> SIP REGISTRATION CHANGED");
                intentFilterACTION_SIP_REGISTRATION_CHANGED=null;
                unregisterReceiver(regStateReceiver);
            }

        } catch (Exception e) {
            Log.w(THIS_FILE, "Unable to unregisterReceiver : regStateReceiver",
                    e);
        }

        try {
            if (!App.isDialForeground && !App.isDialClicked){
                DebugLog.d("UNREGISTER --> DIAL COMMAND RECEIVER");
                intentFilterACTION_DIAL_COMMAND=null;
                unregisterReceiver(dialCommandReceiver);
            }
        } catch (Exception e) {
            Log.w(THIS_FILE,
                    "Unable to unregisterReceiver : dialCommandReceiver", e);
        }

        DebugLog.d("UNREGISTER --> MESSAGE RECEIVER");
        try {
            unregisterReceiver(messageReceiver);
        } catch (Exception e) {
            Log.w(THIS_FILE, "Unable to unregisterReceiver : messageReceiver",
                    e);
        }

    }

    private void showNotice(String url, String fb_url, String title) {
        DialMain.skipCallview = true;

        Log.d(THIS_FILE, "showNotice : " + url);
        Intent intent = new Intent(mContext, WebClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title", title);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP /*
												 * | Intent.
												 * FLAG_ACTIVITY_NO_ANIMATION
												 */);
        startActivity(intent);

        // UPDATE FB_URL
        Fcm.sendFeedback(this, fb_url);
    }

    private void showMessage(String number, String msg) {
        DialMain.skipCallview = true;

        Log.d(THIS_FILE, "showMessage : " + number + " : " + msg);

        Intent intent = new Intent(DialMain.this, SmsMsgContsActivity.class);
        // intent.putExtra("name",name);
        intent.putExtra("number", number);
        startActivity(intent);
    }

    private boolean checkAuth() {
        if (ServicePrefs.DIAL_TEST)
            return false;

        // CHECK AUTH
        Boolean need_auth = false;
        Boolean authOk = mPrefs.getPreferenceBooleanValue(AppPrefs.AUTH_OK);
        String numer = mPrefs
                .getPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER);
        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTH_CID);

        if (!authOk) {
            need_auth = true;
        } else if (numer == null || numer.length() == 0) {
            // 번호가 없다면 인증을 요청한다
            need_auth = true;
        } else if (cid != null && cid.length() > 0) {
            // 번호 인증을 요청한 이후에는 결과를 조회하기 위해 인증 Activity를 다시 띄운다.
            need_auth = true;
        }

        if (need_auth) {
            DialMain.skipClose = true;
            // 인증
            Intent intent = new Intent(DialMain.this, AuthActivityV2.class);
            startActivity(intent);
            finish();
            return true;
        }

        return false;
    }

    private String postBalanceInfo() {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_BALANCE);
        if (url == null || url.length() == 0)
            return null;
        if (uid == null || uid.length() == 0)
            return null;

        //url="http://14.63.160.14/maaltalk/req_balance.php";
        String urlString = String.format("%s?name=%s", url, uid);



        Log.d(THIS_FILE, "BALANCE INFO URL=" + urlString);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httppost = new HttpPost(urlString);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private String requestBalanceInfo() {
        currentBalance = "";

        // 잔액조회
        String msg = "";// mContext.getResources().getString(R.string.none_info);

        // SEND QUERY
        Log.d(THIS_FILE, "REQUEST BALANCE INFO : START");
        String rs = postBalanceInfo();
        Log.d(THIS_FILE, "REQUEST BALANCE INFO : END");
        Log.d(THIS_FILE, "RESULT : " + rs);
        if (rs == null || rs.length() == 0)
            return msg;

        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0")) {

                JSONObject BalanceObject = jObject
                        .getJSONObject("BALANCE_INFO");
                if (BalanceObject == null)
                    return msg;

                String balance = null;
                if (BalanceObject.has("BALANCE")) {
                    balance = BalanceObject.getString("BALANCE");
                    Log.d("BALANCE", balance);
                }

                //BJH 2016.11.11 사장님 요청
                mBalanceList = null;
                mBalanceList = new ArrayList<String>();
/*                String plan = null;
                if (BalanceObject.has("PLAN")) {
                    plan = BalanceObject.getString("PLAN");
                    mBalanceList.add(plan);
                    Log.d(THIS_FILE, plan);
                }*/
                String balance_result = null;
                if (BalanceObject.has("BALANCE_RESULT")) {
                    balance_result = BalanceObject.getString("BALANCE_RESULT");
                    mBalanceList.add(balance_result);
                    Log.d(THIS_FILE, balance_result);
                }
                String did_result = null;
                if (BalanceObject.has("DID_RESULT")) {
                    did_result = BalanceObject.getString("DID_RESULT");
                    if (did_result != null && did_result.length() > 0) {
                        //mBalanceList.add(did_result);
                        if (did_result.startsWith("070번호 : ")){
                            did_result=did_result.replaceAll("070번호 : ","");

                        }
                    }
                    Log.d(THIS_FILE, did_result);
                }

                String currency = null;
                if (BalanceObject.has("CURRENCY")) {
                    currency = BalanceObject.getString("CURRENCY");
                    Log.d("CURRENCY", currency);
                }

                if (balance == null || balance.length() == 0) {
                    balance = "0";
                }

                if (currency != null) {
                    if (currency.equalsIgnoreCase("us")) {
                        currentBalance = "$" + balance;

                    } else if (currency.equalsIgnoreCase("cn")) {
                        currentBalance = "元" + balance;

                    } else {
                        currentBalance = "₩" + balance;
                    }
                } else {
                    currentBalance = balance;
                }

                String DID="";
                String DID_DATE_EXPIRED="";
                String PLAN_NAME="";
                String PLAN_DATE_EXPIRED="";
                String PLAN_EXPIRED="";
                String BALANCE_MONEY="";
                String BALANCE_TIME="";
                String BALANCE_TIME_MAX="";
                String PRODUCT_ID="";
                String DISTRIBUTOR_ID="dial070";

                DID= BalanceObject.optString("070NUMBER");
                DID_DATE_EXPIRED= BalanceObject.optString("070NUMBER_DATE_EXPIRED");
                DID_EXPIRED= BalanceObject.optString("070NUMBER_EXPIRED");
                PLAN_NAME= BalanceObject.optString("PLAN_NAME");
                PLAN_DATE_EXPIRED= BalanceObject.optString("PLAN_DATE_EXPIRED");
                PLAN_EXPIRED= BalanceObject.optString("PLAN_EXPIRED");
                BALANCE_MONEY= BalanceObject.optString("BALANCE_MONEY");
                BALANCE_TIME= BalanceObject.optString("BALANCE_TIME");
                BALANCE_TIME_MAX= BalanceObject.optString("BALANCE_TIME_MAX");
                PRODUCT_ID= BalanceObject.optString("PRODUCT_ID");
                DISTRIBUTOR_ID= BalanceObject.optString("DISTRIBUTOR_ID");

                Intent intentHome=new Intent(HomeActivity.BC_HOME_UPDATE_BALANCE);
                intentHome.putExtra("RES_CODE","0");
                intentHome.putExtra("DID",DID);
                intentHome.putExtra("DID_DATE_EXPIRED",DID_DATE_EXPIRED);
                intentHome.putExtra("DID_EXPIRED",DID_EXPIRED);
                intentHome.putExtra("PLAN_NAME",PLAN_NAME);
                intentHome.putExtra("PLAN_DATE_EXPIRED",PLAN_DATE_EXPIRED);
                intentHome.putExtra("PLAN_EXPIRED",PLAN_EXPIRED);
                intentHome.putExtra("BALANCE_MONEY",BALANCE_MONEY);
                intentHome.putExtra("BALANCE_TIME",BALANCE_TIME);
                intentHome.putExtra("BALANCE_TIME_MAX",BALANCE_TIME_MAX);
                intentHome.putExtra("PRODUCT_ID",PRODUCT_ID);
                intentHome.putExtra("DISTRIBUTOR_ID",DISTRIBUTOR_ID);
                sendBroadcast(new Intent(intentHome));

            } else if (responseObject.getString("RES_CODE").equalsIgnoreCase(
                    "1")) {
                // 잔액 없음
                currentBalance = "";
                Intent intentHome=new Intent(HomeActivity.BC_HOME_UPDATE_BALANCE);
                intentHome.putExtra("RES_CODE","1");
                sendBroadcast(new Intent(intentHome));
            } else {
                Log.d(THIS_FILE, responseObject.getString("RES_TYPE")
                        + "ERROR =" + responseObject.getString("RES_DESC"));
                Intent intentHome=new Intent(HomeActivity.BC_HOME_UPDATE_BALANCE);
                intentHome.putExtra("RES_CODE","1");
                sendBroadcast(new Intent(intentHome));
            }
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            return msg;
        }
        return msg;
    }

    private Thread balanceThread = null;
    private boolean balanceRunning = false;

    private void updateBalanceInfo() {
        if (balanceRunning)
            return;

        balanceRunning = true;
        balanceThread = new Thread() {
            public void run() {
                Log.d(THIS_FILE, "Balance Thread Start !!");
                while (balanceRunning) {
                    msgHandler.sendEmptyMessage(BALANCE_REQUEST);
                    requestBalanceInfo();
                    if (!balanceRunning)
                        break;
                    msgHandler.sendEmptyMessage(BALANCE_UPDATE);

                    if (currentBalance == null || currentBalance.length() == 0) {
                        // wait 3sec
                        for (int i = 0; i < 30; i++) {
                            if (!balanceRunning)
                                break;
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        continue;
                    }
                    break;
                }
                Log.d(THIS_FILE, "Balance Thread Stop !!");
                balanceRunning = false;
                balanceThread = null;
            }

            ;
        };

        if (balanceThread != null) {
            try {
                balanceThread.setPriority(Thread.MIN_PRIORITY);
                balanceThread.start();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopBalance() {
        if (balanceRunning) {
            Log.d(THIS_FILE, "stopBalance..");
            balanceRunning = false;

			/*
			 * if (balanceThread != null) { try { balanceThread.join(2000); }
			 * catch (Exception e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } }
			 */
            Log.d(THIS_FILE, "stopBalance OK !!");
        }
    }

    private void checkBalance() {
        if (!ServicePrefs.DIAL_TEST) {
            String cid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
            if (cid == null || cid.length() == 0)
                return;

            // if (currentBalance == null || currentBalance.length() == 0)
            //BJH 2016.06.08
            updateBalanceInfo();

        }
    }

    public static void mainBackPressed(final Context context) {
        Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
        intent.putExtra(DialMain.EXTRA_COMMAND, "KEYPAD");
        context.sendBroadcast(intent);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // 백버튼 비활성화
        AppCloseRequest(mContext);
        // super.onBackPressed();
    }

    private void SetTabBadge(int index, int count) {
        View tabView = mTabHost.getTabWidget().getChildTabViewAt(index);
        if (tabView == null)
            return;

        View theView = tabView.findViewById(R.id.badgeTextView);
        if (theView == null)
            return;

        TextView textView = (TextView) theView;

        if (count > 0) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(String.valueOf(count));
        } else {
            textView.setVisibility(View.GONE);
            textView.setText("");
        }
    }

    public void updateBadge() {
        //BJH 푸시 메시지도 받기 때문에
        int newCount = 0;
        //if (ServicePrefs.mUseMSG) {
        if (mDatabase == null)
            mDatabase = new DBManager(this);
        if (!mDatabase.isOpen())
            mDatabase.open();
        newCount = mDatabase.getSmsNewMsgCount("");
        //}
        SetTabBadge(3, newCount);
    }

    //BJH 2016.06.09 070번호 등등 요금제 구매 후 서버가 변경되는데 다시 정상적인 서버로 레지하기 위해서
    /*public static void restartSvc(final Context context, String msg) {
        AlertDialog.Builder msgDialog = new AlertDialog.Builder(context);
        msgDialog.setTitle(context.getResources().getString(R.string.app_name));
        msgDialog.setMessage(msg);
        msgDialog.setIcon(R.drawable.ic_launcher);
        msgDialog.setNegativeButton(context.getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        context.stopService(new Intent(context, SipService.class));
                        Intent IntroIntent = new Intent(context,
                                DialSplash.class);
                        IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(IntroIntent);
                        ((Activity) context).finish();
                    }
                });
        msgDialog.show();
    }*/

    //BJH 2016.09.08
    public void goBridge() {
        DialBridgeNew DB = (DialBridgeNew) DialBridge.mBridge;
        if (DB != null) {
            Log.d(THIS_FILE, "GO BRIDGE");
            String cid = DB.getCid();
            String dial_number = DB.getDial_Number();
            Long expired_time = DB.getExpired_Time();
            if (cid != null && cid.length() > 0 && dial_number != null && dial_number.length() > 0) {
                Intent intent = new Intent(DialBridge.ACTION_BRIDGE_UI);
                intent.putExtra("cid", cid);
                intent.putExtra("dial_number", dial_number);
                intent.putExtra("expired_time", expired_time);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setPackage(getPackageName());
                startActivity(intent);
                return;
            }
        }
    }

    //BJH 2016.10.26 ~ 2016.11.07
    /*private void showFAB() {
        int tab = mTabHost.getCurrentTab();
        if (tab == 0)
            mActionMenu.setVisibility(View.VISIBLE);
        else
            mActionMenu.setVisibility(View.GONE);
    }*/

    public void myInfo() {
        //String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION);
        /*String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

        if (url == null || url.length() == 0)
            return;
        if (uid == null || uid.length() == 0)
            return;

        String urlString = String.format("%s?name=%s", url, uid);
        Intent intent = new Intent(DialMain.this, PayClient.class);
        intent.putExtra("url", urlString);
        intent.putExtra("title",
                getResources().getString(R.string.title_myinfo));
        intent.putExtra("popup", true);
        startActivityForResult(intent, SELECT_USER_MSG);*/
        Intent intent = new Intent(mContext, OauthLoginActivity.class);
        startActivityForResult(intent, SELECT_REQ_MYINFO);
    }

    public void actionNotice() {
        String url = mPrefs
                .getPreferenceStringValue(AppPrefs.URL_NOTICE);
        if (url == null || url.length() == 0)
            return;
        Intent intent = new Intent(mContext, WebClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title",
                getResources().getString(R.string.title_notice));
        startActivity(intent);
    }

    public void actionStore() {
        String url = mPrefs
                .getPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI);

        if (url == null || url.length() == 0)
            return;

        Intent intent = new Intent(mContext, PayClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title",
                getResources().getString(R.string.title_usim_pocket));
        startActivityForResult(intent, SELECT_USER_MSG);
    }

    public void actionFaq() {
        String url = mPrefs
                .getPreferenceStringValue(AppPrefs.URL_FAQ);
        if (url == null || url.length() == 0)
            return;
        Intent intent = new Intent(mContext, WebClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title",
                getResources().getString(R.string.title_faq));
        startActivity(intent);
    }

    public void actionSearch() {
        Intent intent = new Intent(mContext, Search.class);
        startActivityForResult(intent, SELECT_SEARCH);
    }

    public void myPoint() {
        String url = mPrefs
                .getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE);
        if (ServicePrefs.mPayType != null
                && ServicePrefs.mPayType.equalsIgnoreCase("1"))
            url = mPrefs
                    .getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA);

        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        //url = "http://pay.maaltalk.com/payment/index.php";
        if (url == null || url.length() == 0)
            return;
        if (uid == null || uid.length() == 0)
            return;

        String urlString = String.format("%s?name=%s", url, uid);
        Intent intent = new Intent(DialMain.this, PayClient.class);
        intent.putExtra("url", urlString);
        intent.putExtra("title",
                getResources().getString(R.string.title_my_point));
        intent.putExtra("popup", true);
        startActivityForResult(intent, SELECT_USER_MSG);
    }

    public void userMsg() {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        if (uid != null && uid.length() > 0) {
            // BJH
            String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_USER_MSG);
            if (url == null || url.length() == 0)
                return;

            String urlString = String.format("%s?name=%s", url, uid);
            Intent intent = new Intent(DialMain.this, PayClient.class); // 사용자별 설명서에서 구매할 수 있도록 PayClient 변경
            intent.putExtra("url", urlString);
            intent.putExtra("title",
                    getResources().getString(R.string.title_user_msg));
            intent.putExtra("popup", true);
            // startActivity(intent);
            startActivityForResult(intent, SELECT_USER_MSG);
        }
    }

    public void kakaoAsk() {
        PackageManager pm = mContext.getPackageManager();
        try {
            Intent intent = pm
                    .getLaunchIntentForPackage("com.kakao.talk");
            if (intent != null) {
                intent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("kakaoplus://plusfriend/friend/@말톡"));
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(DialMain.MAALTALK_EMAIL);
                intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                    .parse("https://play.google.com/store/apps/details?id=com.google.android.gm"));
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    @SuppressWarnings("deprecation")
    public void mtForwarding() {
        String msg = requestForwardingInfo();
        if (msg != null && msg.equals((getResources().getString(R.string.title_forwarding)))) {
            final AlertDialog callfwBuilder = new AlertDialog.Builder(mContext).setPositiveButton(getResources().getString(R.string.forwarding_apply), null).setNegativeButton(getResources().getString(R.string.cancel), null).create();
            LayoutInflater inflater = DialMain.this.getLayoutInflater();
            View view_forward = inflater.inflate(R.layout.callfw_dialog, null);
            final EditText editNumber = (EditText) view_forward.findViewById(R.id.edit_number);
            if (mCallfw != null && mCallfw.length() > 0) {
                editNumber.setText(mCallfw);
                editNumber.setSelection(mCallfw.length());
            } else
                editNumber.setHint(getResources().getString(R.string.hit_forwarding));

            editNumber.requestFocus();
            editNumber.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    InputMethodManager keyboard = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.showSoftInput(editNumber, 0);
                }
            }, 200);
            callfwBuilder.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    Button positiveButton = ((AlertDialog) arg0).getButton(AlertDialog.BUTTON_POSITIVE);
                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String number = editNumber.getText().toString();
                            if (number != null && number.length() > 0) {
                                String equal_number = number.replaceAll("[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]", "");
                                equal_number = equal_number.replaceAll("\\p{Z}", "");
                                if (equal_number.contains(ServicePrefs.mUser070)) {
                                    Alert(getResources().getString(R.string.conts_forwarding_equal));
                                    callfwBuilder.dismiss();
                                    return;
                                }
                            }
                            if (mCallfw != null && mCallfw.equals(number)) {
                                if (number.length() == 0)
                                    Toast.makeText(mContext, getResources().getString(R.string.conts_forwarding_null), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, getResources().getString(R.string.conts_forwarding_equals), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (!(mProduct.equals("3000") || mProduct.equals("3500") || mProduct
                                    .equals("9000"))) {
                                if (number.startsWith("010") || number.startsWith("10")
                                        || number.startsWith("8210")
                                        || number.startsWith("82010")
                                        || number.startsWith("+8210")
                                        || number.startsWith("+82010")) {
                                    Alert(getResources().getString(R.string.conts_forwarding_korea));
                                    callfwBuilder.dismiss();
                                    return;
                                }
                            }
                            Resources res = mContext.getResources();
                            AlertDialog msgDialog = new AlertDialog.Builder(mContext)
                                    .create();
                            msgDialog.setIcon(R.drawable.ic_launcher);
                            msgDialog.setTitle(R.string.title_forwarding);
                            if (mCallfw != null && mCallfw.length() > 0 && number.length() == 0)
                                msgDialog.setMessage(getResources().getString(R.string.forwarding_out_quetion));
                            else
                                msgDialog.setMessage(number + getResources().getString(R.string.forwarding_change_quetion));
                            msgDialog.setButton(Dialog.BUTTON_POSITIVE,res.getString(R.string.yes),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            callfwBuilder.dismiss();
                                            requestForwarding(number);
                                            return;
                                        }
                                    });
                            msgDialog.setButton(Dialog.BUTTON_NEGATIVE, res.getString(R.string.no),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            return;
                                        }
                                    });
                            msgDialog.show();
                            return;
                        }
                    });
                    Button negativetiveButton = ((AlertDialog) arg0).getButton(AlertDialog.BUTTON_NEGATIVE);
                    negativetiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callfwBuilder.dismiss();
                        }
                    });
                }
            });
            callfwBuilder.setView(view_forward);
            callfwBuilder.setTitle(getResources().getString(R.string.title_forwarding));
            callfwBuilder.show();
        } else {
            new MaterialAlertDialogBuilder(this)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(msg)
                    .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();
        }
        return;
    }

    private String postForwardingInfo() {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO);
        if (url == null || url.length() == 0)
            return null;
        if (uid == null || uid.length() == 0)
            return null;

        String urlString = String.format("%s?name=%s", url, uid);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httppost = new HttpPost(urlString);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private String requestForwardingInfo() {
        String msg = (getResources().getString(R.string.setting_edns_fail));
        String rs = postForwardingInfo();
        Log.d(THIS_FILE, "RESULT : " + rs);
        if (rs == null || rs.length() == 0)
            return msg;

        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0")) {
                JSONObject forwardObject = jObject
                        .getJSONObject("FORWARDING_INFO");
                if (forwardObject == null)
                    return msg;

                if (forwardObject.has("product_id")) {
                    mProduct = forwardObject.getString("product_id");
                }

                String status = null;
                if (forwardObject.has("status")) {
                    status = forwardObject.getString("status");
                }

                String verified = null;
                if (forwardObject.has("verified")) {
                    verified = forwardObject.getString("verified");
                }

                if (forwardObject.has("callfw_number")) {
                    mCallfw = forwardObject.getString("callfw_number");
                }

                String balance_result = null;
                if (forwardObject.has("balance_result")) {
                    balance_result = forwardObject.getString("balance_result");
                }

                if (status.equals("A")) {
                    if (balance_result.equals("s")) {
                        msg = (getResources().getString(R.string.title_forwarding));
                    } else if (balance_result.equals("b")) {
                        msg = (getResources().getString(R.string.conts_forwarding_balance));
                    } else if (balance_result.equals("d")) {
                        msg = (getResources().getString(R.string.conts_forwarding_did));
                    } else {
                        msg = (getResources().getString(R.string.conts_forwarding_all));
                    }
                } else {
                    msg = (getResources().getString(R.string.conts_forwarding_status));
                }
            }
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            return msg;
        }
        return msg;
    }

    private String postForwarding(String number) {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING);
        if (url == null || url.length() == 0)
            return null;
        if (uid == null || uid.length() == 0)
            return null;

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(mContext);

            // SET TIMEOUT
            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httppost = new HttpPost(url);

            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("name", uid));
            nameValuePairs.add(new BasicNameValuePair("number", number));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            Log.d(THIS_FILE, "HTTPS POST EXEC OK");
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private void requestForwarding(String number) {
        String msg = (getResources().getString(R.string.setting_edns_fail));
        String rs = postForwarding(number);
        Log.d(THIS_FILE, "RESULT : " + rs);
        if (rs == null || rs.length() == 0)
            Alert(msg);

        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0")) {
                if (number != null && number.length() > 0) {
                    msg = number
                            + (getResources().getString(R.string.forwarding_change));
                } else {
                    msg = (getResources().getString(R.string.forwarding_out));
                }
            }
        } catch (Exception e) {
            Alert(msg);
        }
        Alert(msg);
    }

    private int mListCount = 0;
    private Thread mThreadTimer = null;

    private void balanceList() {
        if (mBalanceList != null) {
            mThreadTimer = new Thread() {
                @Override
                public void run() {
                    try {
                        while (!isInterrupted()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //mBalanceText.setText(mBalanceList.get(mListCount));
                                    if (mListCount == mBalanceList.size() - 1)
                                        mListCount = 0;
                                    else
                                        mListCount++;
                                }
                            });
                            Thread.sleep(3000);
                        }
                    } catch (InterruptedException e) {
                    } finally {
                    }
                }
            };
            if (mThreadTimer != null)
                mThreadTimer.start();
        }
    }

    private void Alert(String msg) {
        new MaterialAlertDialogBuilder(mContext)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String msg = "";
        switch (requestCode) {
            case SELECT_USER_MSG:
                if (resultCode == RESULT_OK) {
                    // BJH 2016.06.21 결제 후 서버 변경
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if (!msg.equals("popup") && !msg.equals("maaltalk_server")) { // 사용자별 설명서 팝업창 닫기가 아닐 경우
                        if (msg.equals(getResources().getString(R.string.pay_ok))) {
                            Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                            intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                            sendBroadcast(intent);
                            int rs = ServicePrefs.login_check(this);// BJH
                            if (rs >= 0)
                                SipService.currentService.changeSipStack();
                        }
                        Alert(msg);
                    }
                    if (msg.equals("maaltalk_server")) { // 사용자별 설명서에서 서버 이동을 눌렀을 경우
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                DialMain.this);
                        LayoutInflater inflater = DialMain.this.getLayoutInflater();
                        View view_svr = inflater.inflate(R.layout.svr_dialog, null);
                        builder.setView(view_svr);
                        builder.setPositiveButton(
                                getResources().getString(R.string.change),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {
                                        // TODO Auto-generated method stub
                                        ServicePrefs.DNSLookUp(mContext);
                                    }
                                });
                        builder.setNegativeButton(
                                getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // TODO Auto-generated method stub
                                        dialog.cancel();
                                    }
                                });
                        builder.setTitle(mContext.getResources().getString(
                                R.string.title_my_svr));
                        builder.create();
                        builder.show();
                    }
                }
                break;
            /*case SELECT_OVERLAY_PERMISSION:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(mContext)) {
                        setApnService(mContext);
                    } else {
                        showMarshmallowAlert();
                    }
                }
                break;*/
            case SELECT_REQ_MYINFO:
                if (resultCode == RESULT_OK) {
                    String res = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if(res != null && res.length() > 0 && res.equals("SUCCESS")) {
                        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
                        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                        if (url == null || url.length() == 0)
                            return;
                        if (uid == null || uid.length() == 0)
                            return;

                        String urlString = String.format("%s?name=%s", url, uid);
                        Intent intent = new Intent(mContext, PayClient.class);


                        intent.putExtra("url", urlString);
                        intent.putExtra("title",
                                getResources().getString(R.string.title_myinfo));
                        intent.putExtra("popup", true);// BJH 나의 정보에서 사용자별 설명서로 이동하므로
                        startActivityForResult(intent, SELECT_USER_MSG);
                    } else if(res != null && res.length() > 0 && res.equals("FAIL")) {
                        Alert(getResources().getString(R.string.setting_edns_fail));
                    }
                }
                break;
            case ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(this)) {
                        // You have permission
                    }
                }
                break;
            case SELECT_SEARCH:
                if (resultCode==RESULT_OK){
                    String type = data.getStringExtra("Type"); // BJH 2016.12.01 사장님 요청 사항
                    String number = data.getStringExtra("PhoneNumber");

                    Intent intentDial=new Intent(Dial.BC_SEARCH);
                    intentDial.putExtra("Type",type);
                    intentDial.putExtra("PhoneNumber",number);
                    intentDial.setPackage(getPackageName());
                    sendBroadcast(new Intent(intentDial));

                }
                break;
            case REQUEST_BATTERY_PERMISSIONS:
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    boolean isWhiteListing = false;
                    isWhiteListing = pm.isIgnoringBatteryOptimizations(mContext.getPackageName());
                    if(!isWhiteListing){
                        AlertDialog.Builder setdialog = new AlertDialog.Builder(mContext,R.style.MyDialogTheme);
                        setdialog.setTitle(getString(R.string.maaltalk_notification))
                                .setMessage(getString(R.string.maaltalk_battery_optimization_alert))
                                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent  = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                                        intent.setData(Uri.parse("package:"+ mContext.getPackageName()));
                                        startActivityForResult(intent,REQUEST_BATTERY_PERMISSIONS);
                                    }
                                })
                                .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(mContext, getString(R.string.maaltalk_battery_optimization_alert2), Toast.LENGTH_SHORT).show();
                                        //checkOverlayPermission();
                                    }
                                })
                                .create()
                                .show();
                    }else {
                        //checkOverlayPermission();
                    }
                }
                break;
            default:
                break;
        }
        return;
    }

    private void checkOverlayPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(mContext)) {

            MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(mContext);
            builder.setTitle(getResources().getString(R.string.permission_setting));
            builder.setMessage(getResources().getString(R.string.conts_permission_overlay_bridge));
            builder.setCancelable(false);
            builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + currentContext.getPackageName()));
                    currentContext.startActivityForResult(intent, SELECT_OVERLAY_PERMISSION);
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            builder.show();
        }
    }

    /*public static void usimCheck(final Context context, AppPrefs prefs, int step) { // BJH 2016.12.07 해외 유심 체크
        Log.d(THIS_FILE, "USIM CHECK");
        //int step = prefs.getPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String operName = telephonyManager.getSimOperatorName().toLowerCase();// 심 회사 체크
        if (operName == null)
            operName = "";
        String operCode = telephonyManager.getSimOperator();
        if (operCode == null)
            operCode = "";
        String oper = operName + operCode;
        //if(oper.length() == 0 || operName.startsWith("sktelecom") || operName.startsWith("kt") || operName.startsWith("lg") || operName.startsWith("olleh"))// 국내 유심 및 유심이 없는 상태
        if (oper.length() == 0 || operCode.equals("45002") || operCode.equals("45003") || operCode.equals("45004") || operCode.equals("45005") || operCode.equals("45006") || operCode.equals("45007") || operCode.equals("45008")
                || operCode.equals("45011") || operCode.equals("45012"))
            return;

        boolean usim_view = prefs.getPreferenceBooleanValue(AppPrefs.USIM_VIEW);
        String ex_oper = prefs.getPreferenceStringValue(AppPrefs.USIM_OPER);
        if (usim_view) {
            if (ex_oper != null && ex_oper.equals(oper)) // 유심이 같은 상태
                return;
            else {
                prefs.setPreferenceBooleanValue(AppPrefs.USIM_VIEW, false); // 유심이 바뀜
                step = 0;
                prefs.setPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP, 0);
            }
        }

        boolean simCheck = usimResultCheck(context, prefs, operName, operCode);
        if (!simCheck) // 해당 유심이 아니기 때문에
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(context)) {
            SweetAlertDialog msgDialog = new SweetAlertDialog(currentContext, SweetAlertDialog.WARNING_TYPE);
            msgDialog.setTitleText(currentContext.getResources().getString(R.string.apn_foreign_usim))
                    .setContentText(currentContext.getResources().getString(R.string.conts_permission_overlay))
                    .setConfirmText(currentContext.getResources().getString(R.string.yes))
                    .setCancelText(currentContext.getResources().getString(R.string.no))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + currentContext.getPackageName()));
                            currentContext.startActivityForResult(intent, SELECT_OVERLAY_PERMISSION);
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    })
                    .show();
        } else {
            apnAlertStep(context, prefs, oper, step);
        }
    }*/

    private static AlertDialog.Builder mStepBuilder = null;

    private static void apnAlertStep(final Context context, final AppPrefs prefs, final String oper, final int step) {
        if (mStepBuilder != null)
            return;
        mStepBuilder = new AlertDialog.Builder(
                context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view_svr = inflater.inflate(R.layout.usim_setting_step, null);
        mStepBuilder.setView(view_svr);
        mStepBuilder.setPositiveButton(
                context.getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        // TODO Auto-generated method stub
                        dialog.cancel();
                        int new_step = step + 1;
                        prefs.setPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP, new_step);
                        if (step == 0) {
                            Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            context.startActivity(intent);

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent Service = new Intent(context,
                                            ApnService.class);
                                    context.startService(Service);
                                }
                            }, 1500);
                            ((Activity) context).finish();
                        } else {
                            apnAlertStepMsg(context, prefs, step);
                        }
                        mStepBuilder = null;
                        return;

                    }
                });
        mStepBuilder.setNegativeButton(
                context.getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        // TODO Auto-generated method stub
                        dialog.cancel();
                        mStepBuilder = null;
                        if (step == 1) {
                            Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            context.startActivity(intent);

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent Service = new Intent(context,
                                            ApnService.class);
                                    context.startService(Service);
                                }
                            }, 1500);
                            ((Activity) context).finish();
                        } else if (step == 2) {
                            apnAlertStepMsg(context, prefs, 1);
                        }
                        return;
                    }
                });
        mStepBuilder.setTitle(context.getResources().getString(
                R.string.apn_usim_setting));
        String msg = context.getResources().getString(R.string.apn_foreign_usim_check);
        if (step == 1)
            msg = context.getResources().getString(R.string.apn_step_two_msg);
        else if (step == 2)
            msg = context.getResources().getString(R.string.apn_step_three_msg);
        mStepBuilder.setMessage(msg);
        mStepBuilder.create();
        mStepBuilder.setCancelable(false);
        mStepBuilder.show();

        CheckBox chk = (CheckBox) view_svr.findViewById(R.id.checkbox_usim);
        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                CheckBox check = (CheckBox) v;
                if (check.isChecked()) {
                    prefs.setPreferenceStringValue(AppPrefs.USIM_OPER, oper);
                    prefs.setPreferenceBooleanValue(AppPrefs.USIM_VIEW, true);
                } else {
                    prefs.setPreferenceBooleanValue(AppPrefs.USIM_VIEW, false);
                }
            }
        });
    }

    private static androidx.appcompat.app.AlertDialog mDialogStepMSg = null;

    private static void apnAlertStepMsg(final Context context, AppPrefs prefs, int step) {
        if (mDialogStepMSg != null)
            return;
        Resources res = context.getResources();
        String title = "";
        String msg = "";
        if (step == 1) {
            title = res.getString(R.string.apn_step_two_title);
            msg = res.getString(R.string.apn_step_two);
            step = 2;
        } else if (step == 2) {
            title = res.getString(R.string.apn_step_three_title);
            msg = res.getString(R.string.apn_step_three);
            step = 0;
        }
        prefs.setPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP, step);

        new MaterialAlertDialogBuilder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(res.getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mDialogStepMSg = null;
                        return;
                    }
                })
                .show();
    }

    /*private static boolean usimResultCheck(Context context, AppPrefs prefs, String operName, String operCode) {
        boolean simCheck = false;
        if (operName != null && operName.length() > 0) {
            if (operName.startsWith("lycamobile")) // Lyca
                simCheck = true;
            else if (operName.startsWith("vinaphone")) // 베트남
                simCheck = true;
            else if (operName.startsWith("ntt")) // 일본 2GB, 일본무제한
                simCheck = true;
            else if (operName.startsWith("unicom") || operName.contains("unicom")) // 중국
                simCheck = true;
            else if (operName.startsWith("ais")) // 태국
                simCheck = true;
            else if (operName.startsWith("abc")) // 홍콩
                simCheck = true;
            else if (operName.startsWith("3")) // 홍콩
                simCheck = true;
        } else {
            if (operCode != null && operCode.length() > 0) {
                if (operCode.equals("310410")) // H2O
                    simCheck = true;
                else if (operCode.equals("45407")) // 마카오
                    simCheck = true;
                else if (operCode.equals("310260")) // 심플모바일, T-Mobile
                    simCheck = true;
                else if (operCode.equals("23420")) // 유럽
                    simCheck = true;
            }
        }

        //if(!simCheck && mDistributor_id != null && mDistributor_id.length() > 0 && mDistributor_id.startsWith("usim"))
        if (!simCheck) // BJH 2017.03.15
            simCheck = getUSIMJSON(context, prefs, operName, operCode); // json에 정의된 값이 있다면 유심 설정을 시도한다.
        return simCheck;
    }*/

    /*public void goNaverMap() { // BJH 2017.01.02
		PackageManager pm = mContext.getPackageManager();
		try {
			Intent intent = pm
					.getLaunchIntentForPackage("com.nhn.android.nmap");
			if (intent != null) {
				intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("navermaps://appLink.nhn?app=Y&version=10&appMenu=location&pinId=36721630&pinType=site&lat=37.4485665&lng=126.4520055&title=%ED%83%91%ED%95%AD%EA%B3%B5%20%EC%9D%B8%EC%B2%9C%EA%B5%AD%EC%A0%9C%EA%B3%B5%ED%95%AD%EC%A0%90&dlevel=11"));
				intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
			} else {
				intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://map.naver.com/?mapmode=0&lng=e22ec27099a30908fad928c9d2f0e985&pinId=36721630&pinType=site&lat=5fe285e585f8b20b5b81b8b8ee887cf5&dlevel=11&enc=b64"));
				intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
			}
		} catch (ActivityNotFoundException e) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri
					.parse("http://map.naver.com/?mapmode=0&lng=e22ec27099a30908fad928c9d2f0e985&pinId=36721630&pinType=site&lat=5fe285e585f8b20b5b81b8b8ee887cf5&dlevel=11&enc=b64"));
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
		}
	}*/

    public void receiveUSIM() { // BJH 2017.01.10 박지원 씨 요청
        /*Intent intent = new Intent(DialMain.this, PopupUSIM.class);
        startActivity(intent);*/
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MT_AIRPORT);
        Intent intent = new Intent(DialMain.this, WebClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title",
                getResources().getString(R.string.air_receive));
        startActivity(intent);
    }

    private void goAgency() { // BJH 2017.03.10 사장님 요청
        if (mTPM == null)
            mTPM = (TelephonyManager) currentContext.getSystemService(Context.TELEPHONY_SERVICE);
		/*String operName = mTPM.getSimOperatorName().toLowerCase();// 심 회사 체크
		if(operName == null)
			operName = "";
		f(operName.startsWith("sktelecom") || operName.startsWith("kt") || operName.startsWith("lg") || operName.startsWith("olleh")) { // 국내 유심 사용자만 통신사 착신전환이 가능함*/
        String operCode = mTPM.getSimOperator();
        if (operCode.equals("45002") || operCode.equals("45003") || operCode.equals("45004") || operCode.equals("45005") || operCode.equals("45006") || operCode.equals("45007") || operCode.equals("45008")
                || operCode.equals("45011") || operCode.equals("45012")) {
            if (ServicePrefs.mUser070 != null && ServicePrefs.mUser070.length() > 0) {
                Intent intent = new Intent(DialMain.this, PopupAgency.class);
                intent.putExtra("AGENCY", operCode);
                startActivity(intent);
            } else {
                Alert(getResources().getString(R.string.error_did));
            }
        } else {
            Alert(getResources().getString(R.string.error_agency));
        }
    }


    private static PhoneStateListener serviceStateListener = new PhoneStateListener() {
        public void onServiceStateChanged(ServiceState serviceState) {
//            Log.d(THIS_FILE, "serviceState : " + serviceState.getState());
            DebugLog.d("PhoneStateListener serviceState --> "+serviceState.getState());

            AppPrefs prefs = new AppPrefs(currentContext);
            int step = prefs.getPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP);
            if (serviceState.getState() == ServiceState.STATE_IN_SERVICE && step > 0)
                prefs.setPreferenceIntegerValue(AppPrefs.USIM_SETTING_STEP, 0); // 정상 개통 초기화
            /*if (serviceState.getState() != ServiceState.STATE_IN_SERVICE && serviceState.getState() != ServiceState.STATE_POWER_OFF) // 정상 개통이 아닌 상태
                usimCheck(currentContext, prefs, step);*/
            mTPM.listen(serviceStateListener, PhoneStateListener.LISTEN_NONE);
        }
    };

    public static void setServiceState() { // 유심 체크를 위해서
        if (mTPM == null)
            mTPM = (TelephonyManager) currentContext.getSystemService(Context.TELEPHONY_SERVICE);
        mTPM.listen(serviceStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
    }

    /*private static boolean getUSIMJSON(Context context, AppPrefs prefs, String operName, String operCode) {
        boolean result = false;
        try {
            InputStream ins = new FileInputStream(MAALTALK_JSON_ROOT + "/usim.json");
            int size = ins.available();

            // Read the entire resource into a local byte buffer.
            byte[] buffer = new byte[size];
            while (ins.read(buffer) != -1) ;
            ins.close();

            String jsontext = new String(buffer);
            JSONObject jObject;
            try {
                jObject = new JSONObject(jsontext);
                JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                Log.d("RES_CODE", responseObject.getString("RES_CODE"));
                Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
                Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

                if (responseObject.getInt("RES_CODE") == 0) {
                    JSONObject urlObject = jObject.getJSONObject("MAALTALK_USIM");
                    if (urlObject == null) return result;

                    String name = null;
                    String code = null;
                    String desc = null;
                    String usim_name = null;
                    String apn = null;
                    String user_name = null;
                    String pwd = null;
                    String mmsc = null;
                    String mms_proxy = null;
                    String mms_port = null;
                    String mcc = null;
                    String mnc = null;
                    String cert = null;
                    String type = null;

                    JSONArray urlItemArray = urlObject.getJSONArray("USIM");
                    if (urlItemArray == null) return result;
                    for (int i = 0; i < urlItemArray.length(); i++) {
                        name = urlItemArray.getJSONObject(i).getString("OPERATOR_NAME").toString().toLowerCase();
                        code = urlItemArray.getJSONObject(i).getString("OPERATOR_CODE").toString();
                        desc = urlItemArray.getJSONObject(i).getString("DESC").toString();
                        usim_name = urlItemArray.getJSONObject(i).getString("USIM_NAME").toString();
                        apn = urlItemArray.getJSONObject(i).getString("APN").toString();
                        user_name = urlItemArray.getJSONObject(i).getString("USER_NAME").toString();
                        pwd = urlItemArray.getJSONObject(i).getString("PWD").toString();
                        mmsc = urlItemArray.getJSONObject(i).getString("MMSC").toString();
                        mms_proxy = urlItemArray.getJSONObject(i).getString("MMS_PROXY").toString();
                        mms_port = urlItemArray.getJSONObject(i).getString("MMS_PORT").toString();
                        mcc = urlItemArray.getJSONObject(i).getString("MCC").toString();
                        mnc = urlItemArray.getJSONObject(i).getString("MNC").toString();
                        cert = urlItemArray.getJSONObject(i).getString("CERT").toString();
                        type = urlItemArray.getJSONObject(i).getString("TYPE").toString();
                        if (operName != null && operName.length() > 0 && name != null && name.length() > 0) {
                            if (operName.startsWith(name)) {
                                //prefs.setPreferenceStringValue(AppPrefs.MTU_OPERATOR_NAME, name);
                                //prefs.setPreferenceStringValue(AppPrefs.MTU_OPERATOR_CODE, code);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_DESC, desc);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_USIM_NAME, usim_name);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_APN, apn);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_USER_NAME, user_name);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_PWD, pwd);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_MMSC, mmsc);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_MMS_PROXY, mms_proxy);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_MMS_PORT, mms_port);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_MCC, mcc);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_MNC, mnc);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_CERT, cert);
                                prefs.setPreferenceStringValue(AppPrefs.MTU_TYPE, type);
                                result = true;
                                break;
                            }
                        } else {
                            if (operCode != null && operCode.length() > 0 && code != null && code.length() > 0) {
                                if (operCode.equals(code)) {
                                    //prefs.setPreferenceStringValue(AppPrefs.MTU_OPERATOR_NAME, name);
                                    //prefs.setPreferenceStringValue(AppPrefs.MTU_OPERATOR_CODE, code);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_DESC, desc);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_USIM_NAME, usim_name);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_APN, apn);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_USER_NAME, user_name);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_PWD, pwd);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_MMSC, mmsc);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_MMS_PROXY, mms_proxy);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_MMS_PORT, mms_port);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_MCC, mcc);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_MNC, mnc);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_CERT, cert);
                                    prefs.setPreferenceStringValue(AppPrefs.MTU_TYPE, type);
                                    result = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(THIS_FILE, "JSON", e);
                return result;
            }
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON ERROR=" + e.getMessage());
            return result;
        }
        return result;
    }*/

    /*public static int saveUSIMJSON(Context context, AppPrefs prefs) {
        String rs = getUSIMINFO(context);
        if (rs == null || rs.length() == 0) return -1;

        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0) {
                JSONObject urlObject = jObject.getJSONObject("MAALTALK_USIM");
                if (urlObject == null) return -1;

                Log.d(THIS_FILE, "VERSION=" + urlObject.getString("VERSION"));
                String ver = urlObject.getString("VERSION");
                String ex_ver = prefs.getPreferenceStringValue(AppPrefs.USIM_INFO_VER);
                if (ver.compareTo(ex_ver) < 1)
                    return 0;
                prefs.setPreferenceStringValue(AppPrefs.USIM_INFO_VER, ver);
            }
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            return -1;
        }
        File file_path = new File(DialMain.MAALTALK_JSON_ROOT);
        if (!file_path.isDirectory()) {
            file_path.mkdirs();
        }
        try {
            FileOutputStream fos = new FileOutputStream(file_path + "/usim.json");
            fos.write(rs.getBytes());
            fos.close();
        } catch (Exception e) {
            return -1;
        }
        return 1;
    }*/

    /*public static String getUSIMINFO(Context context) {
        String url = DialMain.URL_MAALTALK_USIM;
        Log.d(THIS_FILE, "USIM INFO : " + url);

        if (url == null || url.length() == 0) return null;

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            HttpPost httppost = new HttpPost(url);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }*/

    /*private void updateUSIMJSON() { // BJH 2017.01.05 info.json에 USIM INFO VER 확인
        String info_usim_ver = mPrefs.getPreferenceStringValue(AppPrefs.INFO_USIM_VER);
        String usim_json_ver = mPrefs.getPreferenceStringValue(AppPrefs.USIM_INFO_VER);
		*//*if(mDistributor_id != null && mDistributor_id.length() > 0 && mDistributor_id.startsWith("usim")) {
			if(info_usim_ver.compareTo(usim_json_ver) < 1)
				return;
			saveUSIMJSON(mContext, mPrefs);
		}*//* // BJH 2017.03.15
        if (info_usim_ver.compareTo(usim_json_ver) < 1)
            return;
        saveUSIMJSON(mContext, mPrefs);
    }*/


    /* BJH 2017.03.22 */
   /* public static void setApnService(final Context context) {
        Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent Service = new Intent(context,
                        ApnService.class);
                context.startService(Service);
            }
        }, 1500);
    }*/

    /*private void showMarshmallowAlert() {
        if (mMsgDialog != null)
            return;

        mMsgDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        mMsgDialog.setTitleText(getResources().getString(R.string.apn_foreign_usim))
                .setContentText(getResources().getString(R.string.conts_permission_overlay))
                .setConfirmText(getResources().getString(R.string.yes))
                .setCancelText(getResources().getString(R.string.no))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        mMsgDialog = null;
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, SELECT_OVERLAY_PERMISSION);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        mMsgDialog = null;
                    }
                })
                .show();
    }*/

    //BJH 2017.05.22 HashKey
    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash key", something);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("name not found", e.toString());
        }
    }

    //BJH 2017.10.30 디바이스 체인지
    public void deviceChange() {

        new MaterialAlertDialogBuilder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(getResources().getString(R.string.mt_change_device))
                .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, false);
                        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, "");
                        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, "");
                        mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, ""); // GCM-RE
                        Intent intent = new Intent(DialMain.this, AuthActivityV2.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }

    private Handler mRttAlertHandler = null;
    private void rttAlert() { // 2018.02.01 RTT가 X인 경우 상황별 대처법
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return;
        boolean isMobileAvailable = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
        boolean isMobileConnect = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if (!((isWifiAvailable && isWifiConnect) || (isMobileAvailable && isMobileConnect)))
            return;

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String operCode = telephonyManager.getSimOperator();
        if (operCode == null || operCode.length() == 0)
            return;

        boolean date_check = mPrefs.getPreferenceBooleanValue(AppPrefs.MT_RTT_DATE_CHECK);
        long now = System.currentTimeMillis();
        if (date_check) { // 1일 체크
            long ex_date = 0L;
            try {
                ex_date = Long.parseLong(mPrefs.getPreferenceStringValue(AppPrefs.MT_RTT_DATE));
            } catch (NumberFormatException e) {
            }
            Log.d(THIS_FILE, "ex_date:" + ex_date);
            if (ex_date > now) // 1일 안됨
                return;
        }

        long after = now + 86400000L; // 1일
        String date = String.valueOf(after);
        mPrefs.setPreferenceBooleanValue(AppPrefs.MT_RTT_DATE_CHECK, true);
        mPrefs.setPreferenceStringValue(AppPrefs.MT_RTT_DATE, date);

        String msg = null;
        if ((isMobileAvailable && isMobileConnect)) {
            msg = getResources().getString(R.string.action_wifi);
            if (operCode.equals("310260"))
                msg = getResources().getString(R.string.t_mobile_vpn);
            netWorkTypeAlert(msg, operCode);
        } else if(isWifiAvailable && isWifiConnect) {
            msg = getResources().getString(R.string.action_mobile);
            Alert(msg);
        }

    }

    private void netWorkTypeAlert(String msg, final String oper_code) {
        MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(mContext);
        builder.setTitle(getResources().getString(R.string.guide));
        builder.setMessage(msg);
        builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent;
                if (oper_code.equals("310260"))
                    intent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse("https://play.google.com/store/search?q=vpn&c=apps"));
                else
                    intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    //BJH 2018.01.29 안내 문구 변경
    public static final int ERROR_STATUS = 1;
    public static Handler mErrorHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ERROR_STATUS:
                    int status  = (int) msg.obj;
                    alertErrorMsg(currentContext, status);
                    break;
            }
        }
    };

    private static void alertErrorMsg(final Context context, final int status) {
        Log.d(THIS_FILE,"alertErrorMsg:"+status);
        String msg = status + ": ";
        if (status == 403)
            msg = msg + context.getResources()
                    .getString(R.string.status_code_603);
        else if (status == 404)
            msg = msg + context.getResources()
                    .getString(R.string.status_code_404);
        else if (status == 486)
            msg = msg + context.getResources()
                    .getString(R.string.status_code_486);
        else if (status == 603)
            msg = msg + context.getResources()
                    .getString(R.string.status_code_603);
        else
            msg = msg + context.getResources()
                    .getString(R.string.status_code_etc);

        msg = msg + context.getResources().getString(
                R.string.status_code_svc_center);

        /*new SweetAlertDialog(currentContext, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(currentContext.getResources().getString(R.string.guide))
                .setContentText(msg)
                .setConfirmText(currentContext.getResources().getString(R.string.confirm))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        // 에러코드별 확인사항
                    }
                })
                .show();*/

        AlertDialog.Builder builder = new AlertDialog.Builder(currentContext,R.style.MyDialogTheme);
        builder.setTitle(currentContext.getResources().getString(R.string.guide));
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(currentContext.getResources().getString(R.string.confirm),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }

    @Override
    public void onAttachedToWindow() {
        if (push){
            this.getWindow().setFlags(
                    //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,

                    //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
    }

    /**
     *  안드로이드10 관련 2020.05.29 맹완석
     */
    public static void settingNoti(String content){
        NotificationManager notificationManager;
        if (Build.VERSION.SDK_INT >= 26) {
            notificationManager = (NotificationManager) currentContext.getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(Fcm.NEW_CHANNEL_ID);
            if (notificationChannel == null) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel channel = new NotificationChannel(Fcm.NEW_CHANNEL_ID, currentContext.getString(R.string.app_name), importance);
                notificationManager.createNotificationChannel(channel);
            }


        }else{
            notificationManager = (NotificationManager) currentContext.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        Intent intent = new Intent(currentContext, DialMain.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(currentContext, 0, intent, 0 | PendingIntent.FLAG_IMMUTABLE);

        Notification notification = MarshmallowResponse.createNotification(currentContext, pendingIntent, Fcm.NEW_CHANNEL_ID, "알림", content, R.drawable.noti_inbound);

        if (Build.VERSION.SDK_INT >= 29) {
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.when = System.currentTimeMillis();
            notificationManager.notify((int) System.currentTimeMillis(), notification);
        }else {
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.when = System.currentTimeMillis();
            notificationManager.notify((int) System.currentTimeMillis(), notification);
        }
    }

    /**
     *  안드로이드10 관련 수정
     */
    // PUSH로 실행된후 5초이내 연결이 없으면 에러팝업
    private static Handler errorPopupHandler = null;
    private static Runnable errorPopupRunnable = null;

    public static void setErrorPopup() {
        Log.d(THIS_FILE, "setErrorPopup");
        errorPopupRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d(THIS_FILE, "setErrorPopup run");
                settingNoti("현재 Invite를 수신할 수 없습니다.");  //해당 기능은 완벽한것은 아님.의외의 경우에도 해당 노티는 발생가능.
            }
        };
        errorPopupHandler = new Handler();
        errorPopupHandler.postDelayed(errorPopupRunnable, 10000);
    }

    public static void cancelErrorPopup() {
        if (errorPopupHandler != null) {
            Log.d(THIS_FILE, "cancelErrorPopup !!");
            if (errorPopupRunnable != null) {
                errorPopupHandler.removeCallbacks(errorPopupRunnable);
                errorPopupRunnable = null;
            }
            errorPopupHandler = null;
        }
    }

    private void checkMandatoryPermissions() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPermissionContact = checkSelfPermission( Manifest.permission.READ_CONTACTS );
            int hasPermissionAudio = checkSelfPermission( Manifest.permission.RECORD_AUDIO );
            int hasPermissionState = checkSelfPermission( Manifest.permission.READ_PHONE_STATE );

//					int hasPermissionWrite = checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE );
            int hasPermissionRead = checkSelfPermission( Manifest.permission.READ_EXTERNAL_STORAGE );
            //int hasPermissionAlert= checkSelfPermission( Manifest.permission.SYSTEM_ALERT_WINDOW );
            int hasPermissionCallPhone= checkSelfPermission( Manifest.permission.CALL_PHONE );

            int hasPermissionBluetooth = checkSelfPermission(Manifest.permission.BLUETOOTH);

            List<String> permissions = new ArrayList<String>();

            if( hasPermissionContact != PackageManager.PERMISSION_GRANTED ) {
                permissions.add( Manifest.permission.READ_CONTACTS );
            }
            if( hasPermissionAudio != PackageManager.PERMISSION_GRANTED ) {
                permissions.add( Manifest.permission.RECORD_AUDIO );
            }
            if( hasPermissionState != PackageManager.PERMISSION_GRANTED ) {
                permissions.add( Manifest.permission.READ_PHONE_STATE );
            }

            if(hasPermissionBluetooth!=PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.BLUETOOTH);
            }

            if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) { } else {
                int hasPermissionSTATE = checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS);

                if(hasPermissionSTATE != PackageManager.PERMISSION_GRANTED) {
                    permissions.add(Manifest.permission.READ_PHONE_NUMBERS);
                }

            }

            if(Build.VERSION.SDK_INT<Build.VERSION_CODES.O) {
                int hasPermissionWrite = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if( hasPermissionWrite != PackageManager.PERMISSION_GRANTED ) {
                    permissions.add( Manifest.permission.READ_EXTERNAL_STORAGE );
                }
            }

            if( hasPermissionRead != PackageManager.PERMISSION_GRANTED ) {
                permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
            }
            if( hasPermissionCallPhone != PackageManager.PERMISSION_GRANTED ) {
                permissions.add( Manifest.permission.CALL_PHONE );
            }
            if( !permissions.isEmpty() ) {
                requestPermissions( permissions.toArray( new String[permissions.size()] ), REQUEST_CODE_PERMISSIONS );

                if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                            || shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

                    }
                } else {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_NUMBERS)
                            || shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
                            || shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
                    if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                    } else {
                        Toast.makeText(DialMain.this, "원활한 전화 수신을 위해 필수 권한을 허용해주세요.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                    } else {
                        Toast.makeText(DialMain.this, "원활한 전화 수신을 위해 필수 권한을 허용해주세요.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}