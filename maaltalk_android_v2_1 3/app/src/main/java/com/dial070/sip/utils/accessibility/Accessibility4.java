/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils.accessibility;

import android.content.Context;
import android.view.accessibility.AccessibilityManager;

public class Accessibility4 extends AccessibilityWrapper {

	private AccessibilityManager accessibilityManager = null;
	
	@Override
	public void init(Context context) {
		if(accessibilityManager == null) {
			accessibilityManager = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
		}
	}

	@Override
	public boolean isEnabled() {
		return accessibilityManager.isEnabled();
	}

}
