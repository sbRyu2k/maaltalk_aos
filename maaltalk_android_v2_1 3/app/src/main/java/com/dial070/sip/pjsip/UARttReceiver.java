package com.dial070.sip.pjsip;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.widget.Toast;

import org.pjsip.pjsua.MobileRttHandlerCallback;
import org.pjsip.pjsua.SWIGTYPE_p_int;

import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.SipService;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class UARttReceiver extends MobileRttHandlerCallback {
	private static String THIS_FILE = "Dial070-RTT";
	private Context ctx = null;
	public UARttReceiver(Context ctx) {
		this.ctx = ctx;
	}

	@Override
	public void on_rtt_changed(int rtt, int loss) {
		// TODO Auto-generated method stub
		SipService.RTT = rtt;
		SipService.LOSS = loss;
		Log.d(THIS_FILE, "RTT CHANGED : " + rtt + ", LOSS : " + loss);
		DebugLog.d("test_rtt on_rtt_changed rtt: "+rtt);

		FirebaseCrashlytics.getInstance().setCustomKey("rtt_value", rtt);

		Intent regStateChangedIntent = new Intent(SipManager.ACTION_SIP_REGISTRATION_CHANGED);
		ctx.sendBroadcast(regStateChangedIntent);
		super.on_rtt_changed(rtt, loss);
	}

    @Override
    public void on_rtt_reported(int rtt, int loss, int index) {
		DebugLog.d("test_rtt on_rtt_reported rtt: "+rtt);

        Log.d(THIS_FILE, "RTT REPORT : " + rtt + ", LOSS : " + loss + ", INDEX : " + index);
		Intent regStateChangedIntent = new Intent(SipManager.ACTION_SIP_RTT_REPORTED);
		ctx.sendBroadcast(regStateChangedIntent);
        super.on_rtt_reported(rtt, loss, index);
    }
}
