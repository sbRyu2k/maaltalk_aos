/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * Copyright (C) 2006 The Android Open Source Project
 * 
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.widget.Toast;

import com.dial070.sip.models.*;
import com.dial070.status.RingerStatus;
import com.dial070.utils.*;
import com.dial070.view.entitiy.MediaRingerData;

/**
 * Ringer manager for the Phone app.
 */
public class Ringer
{
	private static final String THIS_FILE = "Ringer";

	private static final int VIBRATE_LENGTH = 1000; // ms
	private static final int PAUSE_LENGTH = 1000; // ms
	long[] patterns = {500, 1000, 500, 1000};

	private int savedMode;
	// Uri for the ringtone.
	Uri customRingtoneUri;

	Ringtone ringtone = null; // [sentinel]
	Vibrator vibrator;
	VibratorThread vibratorThread;
	RingerThread ringerThread;
	Context context;
	MediaPlayer mediaPlayer;

	private boolean isRinging;

	ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);

	public Ringer(Context aContext) {
		DebugLog.d("caller --> "+aContext.getClass().getSimpleName());
		context = aContext;
		vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

	}

	/**
	 * Starts the ringtone and/or vibrator.
	 * 
	 */
	public void ring(String remoteContact, String defaultRingtone, boolean isBluetoothConnected) {
//		Log.d(THIS_FILE, "==> ring() called...");
		DebugLog.d("ring() called remoteContact --> "+remoteContact);
		DebugLog.d("ring() called defaultRingTone --> "+defaultRingtone);
		DebugLog.d("ring() called bluetooth connected --> "+isBluetoothConnected);

		synchronized (this) {
			AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			int ringerMode = audioManager.getRingerMode();

			DebugLog.d("ring() called ringer mode --> "+ringerMode);

			if(isBluetoothConnected) {
				try {
					DebugLog.d("Ringer bluetooth is connected. in ring()");

					if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
						vibrator.vibrate(VibrationEffect.createWaveform(patterns, 1));
					} else {
						vibrator.vibrate(patterns, 1);
					}
					vibratorThread = new VibratorThread();
					isRinging = true;

					if(audioManager.getStreamVolume(AudioManager.STREAM_RING) == 0) {
						isRinging = true;
						RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(true));
						return;
					}

					if(!(mediaPlayer!=null && mediaPlayer.isPlaying())) {
						DebugLog.d("Ringer bluetooth connected condition true. in ring()");
						AudioAttributes attr = new AudioAttributes.Builder()
								.setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
								.setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION_SIGNALLING)
								.build();

						mediaPlayer = MediaPlayer.create(context, getContactRingtone(remoteContact, defaultRingtone),
								null, attr, AudioManager.AUDIO_SESSION_ID_GENERATE);
						mediaPlayer.start();
						isRinging = true;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				DebugLog.d("Ringer bluetooth isn't connected. in ring()");

				if(ringerMode==AudioManager.RINGER_MODE_SILENT) {
					if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
						mediaPlayer.stop();
						isRinging = false;
					}
					if(vibrator!=null) {
						vibrator.cancel();
						vibratorThread = null;
					}

					DebugLog.d("Ringer bluetooth isn't connected. --> mode silent");
					isRinging = true;
					RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(true));
					return;
				}

				int vibrateSetting = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);
				if(vibrateSetting==AudioManager.VIBRATE_SETTING_ON || ringerMode==AudioManager.RINGER_MODE_VIBRATE) {
					DebugLog.d("Ringer bluetooth isn;t connected. --> mode vibration");

					if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
						mediaPlayer.stop();
						isRinging = false;
					}
					if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
						vibrator.vibrate(VibrationEffect.createWaveform(patterns, 1));
						vibratorThread = new VibratorThread();
					} else {
						vibrator.vibrate(patterns, 1);
						vibratorThread = new VibratorThread();
					}
					isRinging = true;
					RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(true));
					return;
				}

				if(audioManager.getStreamVolume(AudioManager.STREAM_RING) == 0) {
					isRinging = true;
					RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(true));
					return;
				}

				try {
					if(vibrator!=null) {
						vibrator.cancel();
					}
					DebugLog.d("Ringer bluetooth isn't connected. --> mode ringtone");

					AudioAttributes attr = new AudioAttributes.Builder()
							.setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
							.setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION_SIGNALLING)
							.build();

					if(!(mediaPlayer!=null && mediaPlayer.isPlaying())) {
						mediaPlayer = MediaPlayer.create(context, getContactRingtone(remoteContact, defaultRingtone),
								null, attr, AudioManager.AUDIO_SESSION_ID_GENERATE);
						mediaPlayer.start();
						isRinging = true;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			isRinging = true;
			RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(true));

		}
	}

	public boolean isRinging() {
		if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
			return true;
		} else if(vibratorThread != null) {
			return true;
		} else if(isRinging) {
			return true;
		} else {
			return false;
		}
	}

	public void pauseRing() {
		DebugLog.d("pauseRing() called...");

		if(mediaPlayer!=null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
		}
	}

	public void resumeRing() {
		DebugLog.d("resumeRing() called...");

		if(mediaPlayer!=null && !mediaPlayer.isPlaying()) {
			mediaPlayer.start();
		}
	}

	/**
	 * Stops the ringtone and/or vibrator if any of these are actually
	 * ringing/vibrating.
	 */
	public void stopRing() {
		synchronized (this) {
//			Log.d(THIS_FILE, "==> stopRing() called...");
			DebugLog.d("stopRing() called...");

			if(mediaPlayer!=null) {
				mediaPlayer.stop();
				mediaPlayer.release();
				mediaPlayer = null;
			}

			if(vibrator!=null) {
				vibrator.cancel();
			}

			isRinging = false;
			RingerStatus.INSTANCE.updateMediaRinger(new MediaRingerData(false));

			if(vibratorThread != null) {
				vibratorThread = null;
			}

			// Immediately cancel any vibration in progress.
			// vibrator.cancel();
			// ringtone.stop();

//			if (vibratorThread != null) {
//				vibratorThread.interrupt();
//				try {
//					vibratorThread.join(250); // Should be plenty long (typ.)
//				}
//				catch (InterruptedException e) {
//				} // Best efforts (typ.)
//				vibratorThread = null;
//			}
//
//			if (ringerThread != null)
//			{
//				ringerThread.interrupt();
//				try
//				{
//					ringerThread.join(250);
//				}
//				catch (InterruptedException e)
//				{
//				}
//				ringerThread = null;
//			}
		}
	}

	private class VibratorThread extends Thread {
		public void run() {
			try {
				while (true) {
					vibrator.vibrate(VIBRATE_LENGTH);
					Thread.sleep(VIBRATE_LENGTH + PAUSE_LENGTH);
				}
			}
			catch (InterruptedException ex) {
//				Log.d(THIS_FILE, "Vibrator thread interrupt");
				DebugLog.d("Vibrator thread interrupt");
			}
			finally {
				vibrator.cancel();
//				toneGenerator.stopTone();
			}
//			Log.d(THIS_FILE, "Vibrator thread exiting");
			DebugLog.d("Vibrator thread exiting");
		}
	}

	private class RingerThread extends Thread {
		public void run() {
			try {
				while (true) {
					ringtone.play();
					while (ringtone.isPlaying()) {
						Thread.sleep(100);
					}

				}
			}
			catch (InterruptedException ex) {
//				Log.d(THIS_FILE, "Ringer thread interrupt");
				DebugLog.d("Ringer thread interrupt");
			}
			catch (IllegalStateException ex) {
//				Log.d(THIS_FILE, "Ringer thread interrupt");
				DebugLog.d("Ringer thread interrupt");
			}
			finally {
				try {
					/**
					 *  안드로이드10 벨소리 관련 수정 2020.05.29 맹완석
					 *  모드를 원래대로 돌리지않으면 삼성단말기 모닝콜알람등 울리지않음.
					 */
//					Log.i(THIS_FILE,"savedMode:"+savedMode);
					DebugLog.d("savedMode --> "+savedMode);

					AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
					audioManager.setMode(savedMode);

					if (ringtone.isPlaying())
						ringtone.stop();
				}
				catch (IllegalStateException e)
				{
				}
			}
//			Log.d(THIS_FILE, "Ringer thread exiting");
			DebugLog.d("Ringer thread exiting");
		}
	}

	private Ringtone getRingtone(String remoteContact, String defaultRingtone) {
		Uri ringtoneUri = Uri.parse(defaultRingtone);

		// TODO - Should this be in a separate thread? We would still have to
		// wait for
		// it to complete, so at present, no.
		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);

		if (callerInfo != null && callerInfo.contactExists && callerInfo.contactRingtoneUri != null) {
//			Log.d(THIS_FILE, "Found ringtone for " + callerInfo.name);
			DebugLog.d("Found ringtone for --> "+callerInfo.name);
			ringtoneUri = callerInfo.contactRingtoneUri;
		}

		return RingtoneManager.getRingtone(context, ringtoneUri);
	}

	private Uri getContactRingtone(String remoteContact, String defaultRingtone) {
		Uri ringtoneUri = Uri.parse(defaultRingtone);

//		if(remoteContact.equals("")) {
//			return ringtoneUri;
//		}

		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);
		if(callerInfo!=null && callerInfo.contactExists && callerInfo.contactRingtoneUri!=null) {
			return callerInfo.contactRingtoneUri;
		} else {
			return ringtoneUri;
		}
	}
}
