/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  This file and this file only is released under dual Apache license
 */
package com.dial070.sip.api;

public final class SipManager {
	// -------
	// Static constants
	// -------
	// ACTIONS
	public static final String ACTION_SIP_CALL_UI = "com.dial070.sip.phone.action.INCALL";
	public static final String ACTION_SIP_CALL_RETURN_UI = "com.dial070.sip.phone.action.INCALL.Return";
	public static final String ACTION_SIP_DIALER = "com.dial070.sip.phone.action.DIALER";
	public static final String ACTION_SIP_CALLLOG = "com.dial070.sip.phone.action.CALLLOG";
	public static final String ACTION_SIP_MESSAGES = "com.dial070.sip.phone.action.MESSAGES";
	public static final String ACTION_SIP_VMS = "com.dial070.sip.phone.action.VMS";
	public static final String ACTION_GET_EXTRA_CODECS = "com.dial070.sip.codecs.action.REGISTER_CODEC";
	
	// SERVICE BROADCASTS
	public static final String ACTION_SIP_CALL_CHANGED = "com.dial070.sip.service.CALL_CHANGED";
	public static final String ACTION_SIP_REGISTRATION_CHANGED = "com.dial070.sip.service.REGISTRATION_CHANGED";
	public static final String ACTION_SIP_MEDIA_CHANGED = "com.dial070.sip.service.MEDIA_CHANGED";
	public static final String ACTION_SIP_ACCOUNT_ACTIVE_CHANGED = "com.dial070.sip.service.ACCOUNT_ACTIVE_CHANGED";
	public static final String ACTION_SIP_MESSAGE_RECEIVED = "com.dial070.sip.service.MESSAGE_RECEIVED";
	public static final String ACTION_SIP_NAT_DETECT_CHANGED = "com.dial070.sip.service.NAT_DETECT_CHANGED";
	//TODO : message sent?
	public static final String ACTION_SIP_MESSAGE_STATUS = "com.dial070.sip.service.MESSAGE_STATUS";
	public static final String ACTION_SIP_RTT_REPORTED = "com.dial070.sip.service.RTT_REPORTED";

	public static final String ACTION_SIP_CALL_BOTTOMSHEET = "com.dial070.sip.service.BOTTOMSHEET";
	
	// EXTRAS
	public static final String EXTRA_CALL_INFO = "call_info";
	public static final String EXTRA_ACCOUNT_ID = "acc_id";
	public static final String EXTRA_ACTIVATE = "activate";
	public static final String EXTRA_NAT_TYPE = "nat_type";
	public static final String EXTRA_CALL_NUMBER = "call_number";
	
	
	// PERMISSION
	public static final String PERMISSION_USE_SIP = "android.permission.USE_SIP";
	public static final String PERMISSION_CONFIGURE_SIP = "android.permission.CONFIGURE_SIP";
	
	
    /**
     * Meta constant name for library name.
     */
    public static final String META_LIB_NAME = "lib_name";
    /**
     * Meta constant name for the factory name.
     */
    public static final String META_LIB_INIT_FACTORY = "init_factory";
    /**
     * Meta constant name for the factory deinit name.
     */
    public static final String META_LIB_DEINIT_FACTORY = "deinit_factory";	
}
