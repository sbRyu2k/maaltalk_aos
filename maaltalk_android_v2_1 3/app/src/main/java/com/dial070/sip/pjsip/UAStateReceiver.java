/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * Copyright (C) 2010 Chris McCormick (aka mccormix - chris@mccormick.cx)
 * This file is part of Dial070.
 * <p>
 * Dial070 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Dial070 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.pjsip;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pjsip.pjsua.Callback;
import org.pjsip.pjsua.SWIGTYPE_p_p_pjmedia_port;
import org.pjsip.pjsua.SWIGTYPE_p_pjmedia_stream;
import org.pjsip.pjsua.SWIGTYPE_p_pjsip_rx_data;
import org.pjsip.pjsua.pj_str_t;
import org.pjsip.pjsua.pj_stun_nat_detect_result;
import org.pjsip.pjsua.pjsip_event;
import org.pjsip.pjsua.pjsip_status_code;
import org.pjsip.pjsua.pjsua;
import org.pjsip.pjsua.pjsuaConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaRecorder.AudioSource;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.db.CTimeData;
import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.db.RecentCallsData;
import com.dial070.db.ScoreData;
import com.dial070.db.ZoneData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.service.Fcm;
import com.dial070.service.InBoundCallMediaService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipCallSession.StatusCode;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfile;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.db.DBAdapter;
import com.dial070.sip.models.SipMessage;
import com.dial070.sip.pjsip.PjSipCalls.UnavailableException;
import com.dial070.sip.service.SipNotifications;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.sip.utils.Threading;
import com.dial070.sip.utils.TimerWrapper;
import com.dial070.status.CallStatus;
import com.dial070.status.RingerStatus;
import com.dial070.status.WaitingStatus;
import com.dial070.ui.CallStat;
import com.dial070.ui.Dial;
import com.dial070.ui.InCall2;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.utils.ScoreUtils;
import com.dial070.utils.ServiceUtils;
import com.dial070.utils.Util;
import com.dial070.view.entitiy.CallActionData;
import com.dial070.view.entitiy.CallData;

public class UAStateReceiver extends Callback {
    static String THIS_FILE = "Dial070-UASTATE";

    final static String ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE";

    private SipNotifications notificationManager;
    private PjSipService pjService;
    // private ComponentName remoteControlResponder;
    private CountDownTimer mCountDownTimer = null;

    public void lockCpu() {
        if (eventLock != null) {
            eventLock.acquire();
        }
    }

    public void unlockCpu() {
        if (eventLock != null && eventLock.isHeld()) {
            eventLock.release();
        }
    }

    private int reject_call_id = -1;

    @Override
    public void on_incoming_call(final int acc_id, final int callId, SWIGTYPE_p_pjsip_rx_data rdata) {

        DebugLog.d("onIncomingCall callId --> "+callId);
        DebugLog.d("onInComingCall accId --> "+acc_id);

        DebugLog.d("UAStateReceiver on_incoming_call acc_id --> "+acc_id);
        DebugLog.d("UAStateReceiver on_incoming_call callId --> "+callId);
        DebugLog.d("UAStateReceiver on_incoming_call is created --> "+pjService.isCreated());

        Log.i(THIS_FILE,"on_incoming_call");
//        Crashlytics.logException(new Exception("on_incoming_call invite recv"));
        lockCpu();

        // Check if we have not already an ongoing call
        SipCallSession existingOngoingCall = getActiveCallInProgress();

        if (existingOngoingCall != null) {
            DebugLog.d("UAStateReceiver on_incoming_call already an ongoing call is exist");

            int callState = existingOngoingCall.getCallState();
            DebugLog.d("UAStateReceiver on_incoming_call current call state is "+callState);
            Toast.makeText(pjService.service, "current call state : "+callState, Toast.LENGTH_SHORT).show();
            if (callState >= SipCallSession.InvState.CALLING && callState <= SipCallSession.InvState.CONFIRMED) {
                Log.e(THIS_FILE, "UAStateReceiver on_incoming_call For now we do not support two call at the same time !!!");
//                Toast.makeText(pjService.service, "BUSY_HERE called int callstate state is not idle", Toast.LENGTH_SHORT).show();
                // If there is an ongoing call... For now decline TODO : should
                // here manage multiple calls
                reject_call_id = callId;
                pjsua.call_hangup(callId, StatusCode.BUSY_HERE, null, null);
                unlockCpu();
                /**
                 *  Android10 버그 관련
                 */
//                if (Build.VERSION.SDK_INT >= 29) {
//                    Fcm.cancelAutoRingStop();
//                    Fcm.stopRing();
////                    /**
////                     * ADDED
////                     * TEST
////                     */
////                    if(Fcm.mediaManager!=null) {
////                        Fcm.mediaManager.stopRing();
//////                        Fcm.mediaManager.stopService();
////                    }
//                    if(pjService.mediaManager!=null) {
//                        pjService.mediaManager.stopRing();
//                    }
//
//                    DialMain.settingNoti("말톡으로 통화중이라 전화를 수신할 수 없습니다.");
//                }
//                Crashlytics.logException(new Exception("can't call for another call!!"));

//                DialMain.settingNoti("말톡으로 통화중이라 전화를 수신할 수 없습니다.");
                return;
            }
        }
        DebugLog.d("UAStateReceiver on_incoming_call already an ongoing call is not exist");

        // Check GSM State
        DebugLog.d("UAStateReceiver Dial070-UASTATE current gsm state is "+PjSipService.lastGSMState);

        if (PjSipService.lastGSMState != TelephonyManager.CALL_STATE_IDLE) {
            Log.e(THIS_FILE, "gsm call is not idle p" + "state");
            DebugLog.d("UAStateReceiver on_incoming_call gsm call is not idle");

//            Toast.makeText(pjService.service, "BUSY_HERE called int GSM state is not idle", Toast.LENGTH_SHORT).show();

            reject_call_id = callId;
            pjsua.call_hangup(callId, StatusCode.BUSY_HERE, null, null);
            unlockCpu();
            /**
             *  Android10 버그 관련
             */
//            if (Build.VERSION.SDK_INT >= 29) {
//                Fcm.cancelAutoRingStop();
//                Fcm.stopRing();
//                /**
//                 * ADDED
//                 * TEST
//                 */
////                if(Fcm.mediaManager!=null) {
////                    Fcm.mediaManager.stopRing();
//////                    Fcm.mediaManager.stopService();
////                }
//                if(pjService.mediaManager!=null) {
//                    pjService.mediaManager.stopRing();
//                }
//
////                DialMain.settingNoti("네이티브 통화중이라 전화를 수신할 수 없습니다.");
//            }
//            DialMain.settingNoti("네이티브 통화중이라 전화를 수신할 수 없습니다.");
//            Crashlytics.logException(new Exception("can't call for another gsm call!!"));
            return;
        }

        DebugLog.d("UAStateReceiver on_incoming_call checker flag  --> before thread start");
        DebugLog.d("UAStateReceiver on_incoming_call cehcking call status --> "+ CallStatus.INSTANCE.getCurrentStatus());

       //unlockCpu();

        // by sgkim : 2015-08-24
        Thread t = new Thread() {
            public void run() {
                try {
                    DebugLog.d("UAStateReceiver on_incoming_call checker flag --> in thread run()");
                    SipCallSession callInfo = getCallInfo(callId, true);
                    DebugLog.d("Incoming call <<");
                    Log.d(THIS_FILE, "Incoming call <<");
                    DebugLog.d("UAStateReceiver on_incoming_call checker flag --> in thread run() : treatIncomingCall");

                    if(callInfo == null) {
                        DebugLog.d("Incoming call << callInfo is null...");
                    }

                    treatIncomingCall(acc_id, callInfo);
                    msgHandler.sendMessage(msgHandler.obtainMessage(ON_INCOMING_CALL, callInfo));
                    DebugLog.d("Incoming call >>");
                    Log.d(THIS_FILE, "Incoming call >>");

                } catch (NullPointerException npe) {
                    npe.printStackTrace();

//                    if (Build.VERSION.SDK_INT >= 29) {
//                        Fcm.cancelAutoRingStop();
//                        Fcm.stopRing();
//                        if(pjService.mediaManager!=null) {
//                            pjService.mediaManager.stopRing();
//                        }
////                        pjService.mediaManager.stopRing("on_incoming_call() thread");
//                        DialMain.settingNoti("말톡 전화수신 중에 문제가 생겨 전화를 수신하지 못했습니다.");
//                    }
//                    DialMain.settingNoti("말톡 전화수신 중에 문제가 생겨 전화를 수신하지 못했습니다.");
//                    Crashlytics.logException(new Exception("Incoming call nullpointer"));

                } catch (Exception e) {
                    e.printStackTrace();
                }

//                try { //BJH 2016.10.07 NullPointerException
//                    SipCallSession callInfo = getCallInfo(callId, true);
//                    DebugLog.d("Incoming call <<");
//                    Log.d(THIS_FILE, "Incoming call <<");
//
//                    if(callInfo == null) {
//                        DebugLog.d("Incoming call << callInfo is null...");
//                    }
//
//                    treatIncomingCall(acc_id, callInfo);
//                    msgHandler.sendMessage(msgHandler.obtainMessage(ON_INCOMING_CALL, callInfo));
//                    DebugLog.d("Incoming call >>");
//                    Log.d(THIS_FILE, "Incoming call >>");
//                }
//                catch (NullPointerException npe) {
//                    //
//                    //
//                    /**
//                     *  Android10 버그 관련
//                     */
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        Fcm.cancelAutoRingStop();
//                        Fcm.stopRing();
//                        /**
//                         * ADDED
//                         * TEST
//                         */
//                        if(Fcm.mediaManager!=null) {
//                            Fcm.mediaManager.stopRing();
//                        }
//
//                        DialMain.settingNoti("말톡 전화수신 중에 NullPointerException.");
//                    }
////                    Crashlytics.logException(new Exception("Incoming call nullpointer"));
//                }

            }

            ;
        };
        if (t != null)
            t.start();
        DebugLog.d("UAStateReceiver on_incoming_call checker flag  --> after thread start");

       unlockCpu(); // shlee move it to last for incoming

    }

    @Override
    public void on_call_state(int callId, pjsip_event e) {
        DebugLog.d("Ordering TEST override on_call_state --> "+callId);
        pjsua.css_on_call_state(callId, e);
        lockCpu();

        SipCallSession[] sessions = getCalls();
        if(sessions!=null) {
            DebugLog.d("Ordering TEST override on_call_state --> session size is "+sessions.length);
        } else {
            DebugLog.d("Ordering TEST override on_call_State --> session is null");
        }

        // SKIP REJECT CALL EVENT
        if (reject_call_id != -1 && reject_call_id == callId) {
            DebugLog.d("Ordering TEST override on_call_state --> SKIP REJECT CALL EVENT condition");
            reject_call_id = -1;
            unlockCpu();
            return;
        }

        DebugLog.d("Call state << callId --> "+callId);
        Log.d(THIS_FILE, "Call state <<");
        // Get current infos
        SipCallSession callInfo = getCallInfo(callId, true);
        int callState = callInfo.getCallState();
        if (callState == SipCallSession.InvState.DISCONNECTED) {
            Log.d(THIS_FILE, "Call DISCONNECTED : " + callInfo.getLastStatus() +" "+callInfo.getLastStatusText());
            DebugLog.d("Ordering TEST Call DISCONNECTED : "+callInfo.getLastStatus()+" "+callInfo.getLastStatusText());

            if (pjService.mediaManager != null) {
//                if (Build.VERSION.SDK_INT >= 29) {
//                    Fcm.cancelAutoRingStop();
//                    Fcm.stopRing();
//                    /**
//                     * ADDED
//                     * TEST
//                     */
////                    if(Fcm.mediaManager!=null) {
////                        Fcm.mediaManager.stopRing();
////                        Fcm.mediaManager.stopAnnoucing();
//////                        Fcm.mediaManager.stopService();
////                    }
//
//                    /**
//                     * 2020.08.18
//                     * 사용자가 블루투스 사용시 오디오 포커스 제어권을 해제하기 위한 처리 기능
//                     */
//                    pjService.mediaManager.stopAnnouncing();
//                    pjService.mediaManager.resetSettings();
//
//                }else {
//                    pjService.mediaManager.stopAnnouncing();
//                    pjService.mediaManager.resetSettings();
//                }

                pjService.mediaManager.stopAnnouncing();
                pjService.mediaManager.resetSettings();
            }
            if (incomingCallLock != null && incomingCallLock.isHeld()) {
                incomingCallLock.release();
            }

            // Call is now ended
            pjService.stopDialtoneGenerator();
            // TODO : should be stopped only if it's the current call.
            // BJH java.lang.NullPointerException 2016.06.09
            String rec_toggle = "";
            try {
                rec_toggle = mPrfs
                        .getPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE);
            } catch (NullPointerException npe) {
                //
            }
            if (rec_toggle != null && rec_toggle.equals("Y"))
                mPrfs.setPreferenceStringValue(AppPrefs.AUTO_REC_TOGGLE, "");
            stopRecording();

            pjService.service.state = 0;
        }

        msgHandler.sendMessage(msgHandler
                .obtainMessage(ON_CALL_STATE, callInfo));
        Log.d(THIS_FILE, "Call state >>");
        unlockCpu();
    }

    @Override
    public void on_buddy_state(int buddy_id) {
        lockCpu();
        Log.d(THIS_FILE, "On buddy state");
        // buddy_info = pjsua.buddy_get_info(buddy_id, new pjsua_buddy_info());

        unlockCpu();
    }

    @Override
    public void on_pager(int call_id, pj_str_t from, pj_str_t to,
                         pj_str_t contact, pj_str_t mime_type, pj_str_t body) {
        lockCpu();

        long date = System.currentTimeMillis();
        String sFrom = SipUri.getCanonicalSipContact(from.getPtr());
        SipMessage msg = new SipMessage(sFrom, to.getPtr(), contact.getPtr(),
                body.getPtr(), mime_type.getPtr(), date,
                SipMessage.MESSAGE_TYPE_INBOX);

        // Insert the message to the DB
        DBAdapter database = new DBAdapter(pjService.service);
        database.open();
        database.insertMessage(msg);
        database.close();

        // Broadcast the message
        Intent intent = new Intent(SipManager.ACTION_SIP_MESSAGE_RECEIVED);
        // TODO : could be parcelable !
        intent.putExtra(SipMessage.FIELD_FROM, msg.getFrom());
        intent.putExtra(SipMessage.FIELD_BODY, msg.getBody());
        pjService.service.sendBroadcast(intent);

        // Notify android os of the new message
        notificationManager.showNotificationForMessage(msg);
        unlockCpu();
    }

    @Override
    public void on_pager_status(int call_id, pj_str_t to, pj_str_t body,
                                pjsip_status_code status, pj_str_t reason) {
        lockCpu();
        // TODO : treat error / acknowledge of messages
        int messageType = (status.equals(pjsip_status_code.PJSIP_SC_OK) || status
                .equals(pjsip_status_code.PJSIP_SC_ACCEPTED)) ? SipMessage.MESSAGE_TYPE_SENT
                : SipMessage.MESSAGE_TYPE_FAILED;
        String sTo = SipUri.getCanonicalSipContact(to.getPtr());

        Log.d(THIS_FILE, "SipMessage in on pager status " + status.toString()
                + " / " + reason.getPtr());

        // Update the db
        DBAdapter database = new DBAdapter(pjService.service);
        database.open();
        database.updateMessageStatus(sTo, body.getPtr(), messageType,
                status.swigValue(), reason.getPtr());
        database.close();

        // Broadcast the information
        Intent intent = new Intent(SipManager.ACTION_SIP_MESSAGE_RECEIVED);
        intent.putExtra(SipMessage.FIELD_FROM, sTo);
        pjService.service.sendBroadcast(intent);
        unlockCpu();
    }

    @Override
    public void on_reg_state(int accountId) {
        DebugLog.d("UAStateReceiver override on_reg_state accountId --> "+accountId);
        final int _accountId = accountId;
        Thread t = new Thread() {
            public void run() {
                try {
                    on_reg_state_process(_accountId);
                } catch (Exception e) {
                    //
                    e.printStackTrace();
                }
            }
        };
        if (t != null)
            t.start();
    }

    private void on_reg_state_process(final int _accountId) {
        DebugLog.d("UAStateReceiver override on_reg_state_process --> "+_accountId);
        SipProfileState accountInfo = pjService.service.getSipProfileState(0);
//        SipProfileState accountInfo = pjService.service.getSipProfileState(_accountId);
        if (accountInfo == null) {
            SipCallSession activeCall = pjService.getActiveCallInProgress();
            if (activeCall != null) {
                startPlay();
            }
            return;
        }

        int statusCode = accountInfo.getStatusCode();
        DebugLog.d("UAStateReceiver override on_reg_state_process account statusCode --> "+statusCode);
        if (statusCode == 401) {
            return;
        }

        lockCpu();

        Log.d(THIS_FILE, "****** >> NEW REG : ACCOUND = " + String.valueOf(_accountId) + ", STATUS = " + String.valueOf(statusCode));
        SipCallSession activeCall = pjService.getActiveCallInProgress();

        if (activeCall != null && !PjSipService.isHold && statusCode == 200) {
            Log.d(THIS_FILE, "****** >> AUTO : REINVITE");
            pjService.callReinvite(activeCall.getCallId(), true);
        }

        if (msgHandler != null)
            msgHandler.sendMessage(msgHandler.obtainMessage(
                    ON_REGISTRATION_STATE, _accountId));

        unlockCpu();

        if (activeCall != null) {
            if (statusCode == 200) {
                stopPlay();
            } else {
                startPlay();
            }
        }
    }

    @Override
    public void on_call_media_state(int callId) {
        DebugLog.d("UAStateReceiver override on_call_media_state callId --> "+callId);
        pjsua.css_on_call_media_state(callId);
        lockCpu();
        if (pjService.mediaManager != null) {
//            pjService.mediaManager.stopRing();
            pjService.mediaManager.stopOnlyRing("on_call_media_state");
        }

        if (Build.VERSION.SDK_INT >= 29) {
//            Fcm.cancelAutoRingStop();
//            Fcm.stopRing();
            /**
             * ADDED
             * TEST
             */
//            if(Fcm.mediaManager!=null) {
//                Fcm.mediaManager.stopRing();
////                Fcm.mediaManager.stopService();
//            }
        }

        if (incomingCallLock != null && incomingCallLock.isHeld()) {
            incomingCallLock.release();
        }

        SipCallSession callInfo = getCallInfo(callId, true);

        if (callInfo.getMediaStatus() == SipCallSession.MediaState.ACTIVE) {
            pjsua.conf_connect(callInfo.getConfPort(), 0);
            pjsua.conf_connect(0, callInfo.getConfPort());

            // SPEAKER
            float speakerLevel = pjService.prefsWrapper.getSpeakerLevel();
            pjsua.conf_adjust_tx_level(0, speakerLevel);
            Log.d(THIS_FILE, "conf_adjust_tx_level : " + speakerLevel);

            // MIC
            float micLevel = pjService.prefsWrapper.getMicLevel();
            if (pjService.mediaManager != null && pjService.mediaManager.isUserWantMicrophoneMute()) {
                //micLevel = 0; //BJH 2016.10.27 간혹 상대방에서 목소리가 들리지 않는 문제 원인??
            }
            pjsua.conf_adjust_rx_level(0, micLevel);
            Log.d(THIS_FILE, "conf_adjust_rx_level : " + micLevel);

            // Auto record
            /*
			 * if (recordedCall == INVALID_RECORD &&
			 * pjService.prefsWrapper.getPreferenceBooleanValue
			 * (PreferencesWrapper.AUTO_RECORD_CALLS)) { startRecording(callId);
			 * }
			 */
        }

        msgHandler.sendMessage(msgHandler.obtainMessage(ON_MEDIA_STATE,
                callInfo));
        unlockCpu();
    }

    @Override
    public void on_mwi_info(int acc_id, pj_str_t mime_type, pj_str_t body) {
        lockCpu();
        // Treat incoming voice mail notification.

        String msg = body.getPtr();
        // Log.d(THIS_FILE, "We have a message :: " + acc_id + " | " +
        // mime_type.getPtr() + " | " + body.getPtr());

        boolean hasMessage = false;
        int numberOfMessages = 0;
        String voiceMailNumber = "";

        String lines[] = msg.split("\\r?\\n");
        // Decapsulate the application/simple-message-summary
        // TODO : should we check mime-type?
        // rfc3842
        Pattern messWaitingPattern = Pattern.compile(
                ".*Messages-Waiting[ \t]?:[ \t]?(yes|no).*",
                Pattern.CASE_INSENSITIVE);
        Pattern messAccountPattern = Pattern.compile(
                ".*Message-Account[ \t]?:[ \t]?(.*)", Pattern.CASE_INSENSITIVE);
        Pattern messVoiceNbrPattern = Pattern.compile(
                ".*Voice-Message[ \t]?:[ \t]?([0-9]*)/[0-9]*.*",
                Pattern.CASE_INSENSITIVE);

        for (String line : lines) {
            Matcher m;
            m = messWaitingPattern.matcher(line);
            if (m.matches()) {
                Log.w(THIS_FILE, "Matches : " + m.group(1));
                if ("yes".equalsIgnoreCase(m.group(1))) {
                    Log.d(THIS_FILE, "Hey there is messages !!! ");
                    hasMessage = true;

                }
                continue;
            }
            m = messAccountPattern.matcher(line);
            if (m.matches()) {
                voiceMailNumber = m.group(1);
                Log.d(THIS_FILE, "VM acc : " + voiceMailNumber);
                continue;
            }
            m = messVoiceNbrPattern.matcher(line);
            if (m.matches()) {
                try {
                    numberOfMessages = Integer.parseInt(m.group(1));
                } catch (NumberFormatException e) {
                    Log.w(THIS_FILE, "Not well formated number " + m.group(1));
                }
                Log.d(THIS_FILE, "Nbr : " + numberOfMessages);
                continue;
            }
        }

        if (hasMessage && numberOfMessages > 0) {
            SipProfile acc = pjService.getAccountForPjsipId(acc_id);
            if (acc != null) {
                Log.d(THIS_FILE,
                        acc_id + " -> Has found account "
                                + acc.getDefaultDomain() + " " + acc.id
                                + " >> " + acc.getProfileName());
                notificationManager.showNotificationForVoiceMail(acc,
                        numberOfMessages, voiceMailNumber);
            }
        }
        unlockCpu();
    }

    @Override
    public void on_nat_detect(pj_stun_nat_detect_result res) {
        lockCpu();
        Log.d(THIS_FILE, "on_nat_detect : " + res.getNat_type_name());
        Intent intent = new Intent(SipManager.ACTION_SIP_NAT_DETECT_CHANGED);
        intent.putExtra(SipManager.EXTRA_NAT_TYPE, res.getNat_type()
                .swigValue());
        pjService.service.sendBroadcast(intent);
        unlockCpu();
    }

    @Override
    public int on_validate_audio_clock_rate(int clock_rate) {
        // if (clock_rate != ServicePrefs.CLOCK_RATE) return -1;
        // return 0;
        return super.on_validate_audio_clock_rate(clock_rate);
    }

    @Override
    public void on_setup_audio(int beforeInit) {
        DebugLog.d("on_setup_audio beforeInit --> pjService.setAudioInCall(beforeInit) "+beforeInit);

        if (pjService != null) {
            pjService.setAudioInCall(beforeInit);
        }
    }

    @Override
    public void on_teardown_audio() {
        DebugLog.d("on_teardown_audio --> pjService.unsetAudioInCall()");

        if (pjService != null) {
            pjService.unsetAudioInCall();
        }
    }

    @Override
    public int timer_schedule(int entry, int entryId, int time) {
        return TimerWrapper.schedule(entry, entryId, time);
    }

    @Override
    public int timer_cancel(int entry, int entryId) {
        return TimerWrapper.cancel(entry, entryId);
    }

    @Override
    public void on_stream_created(int call_id, SWIGTYPE_p_pjmedia_stream strm,
                                  long stream_idx, SWIGTYPE_p_p_pjmedia_port p_port) {
        // TODO Auto-generated method stub
        super.on_stream_created(call_id, strm, stream_idx, p_port);
    }

    @Override
    public void on_stream_destroyed(int call_id,
                                    SWIGTYPE_p_pjmedia_stream strm, long stream_idx) {
        // TODO Auto-generated method stub
        super.on_stream_destroyed(call_id, strm, stream_idx);
    }

    @Override
    public void on_dtmf_digit(int call_id, int digit) {
        // TODO Auto-generated method stub
        super.on_dtmf_digit(call_id, digit);
    }

    @Override
    public void on_typing(int call_id, pj_str_t from, pj_str_t to,
                          pj_str_t contact, int is_typing) {
        // TODO Auto-generated method stub
        super.on_typing(call_id, from, to, contact, is_typing);
    }

    @Override
    public int on_set_micro_source() {
        // Except for galaxy S II
        if (!Compatibility.isCompatible(11)
                && android.os.Build.DEVICE.toUpperCase(Locale.getDefault())
                .startsWith("GT-I9100")) {
            return AudioSource.MIC;
        }

        if (Compatibility.isCompatible(10)) {
            // Note that in APIs this is only available from level 11.
            // VOICE_COMMUNICATION
            return 0x7;
        }
        return AudioSource.MIC;
    }

    // -------
    // Current call management -- assume for now one unique call is managed
    // -------
    private HashMap<Integer, SipCallSession> callsList = new HashMap<Integer, SipCallSession>();

    // private long currentCallStart = 0;

    public void updateCallInfoFromStack(SipCallSession call) {
        // Update from pjsip
        try {
            PjSipCalls.updateSessionFromPj(call, pjService);
        } catch (UnavailableException e) {
            // TODO : treat error
            Log.e(THIS_FILE, "Call does not exist anymore " + call.getCallId());
        }
    }

    public SipCallSession getCallInfo(Integer callId, boolean update) {
        Log.d(THIS_FILE, "Get call info callId:"+callId);
        SipCallSession callInfo;
        synchronized (callsList) {
            callInfo = callsList.get(callId);
            if (callInfo == null) {
                callInfo = PjSipCalls.getCallInfo(callId, pjService);
                callsList.put(callId, callInfo);
            } else {
                if (update) {
                    Log.d(THIS_FILE, "UPDATE CALL INFOS !!!");
                    updateCallInfoFromStack(callInfo);
                }
            }
        }
        return callInfo;
    }

    public SipCallSession[] getCalls() {
        if (callsList != null) {

            SipCallSession[] callsInfos = new SipCallSession[callsList.size()];
            int i = 0;
            for (Entry<Integer, SipCallSession> entry : callsList.entrySet()) {
                callsInfos[i] = entry.getValue();
                i++;
            }
            return callsInfos;
        }
        return null;
    }

    private WorkerHandler msgHandler;
    private HandlerThread handlerThread;
    private WakeLock incomingCallLock;
    private WakeLock eventLock;

    private AppPrefs mPrfs;
    private PreferencesWrapper mSysPrefs;

    private static final int ON_INCOMING_CALL = 1;
    private static final int ON_CALL_STATE = 2;
    private static final int ON_MEDIA_STATE = 3;
    private static final int ON_REGISTRATION_STATE = 4;
    private static final int ON_PAGER = 5;

    static int counter = 0;

    private static Timer timerErrorStatus;

    private class WorkerHandler extends Handler {

        public WorkerHandler(Looper looper) {
            super(looper);
            Log.d(THIS_FILE, "Create async worker !!!");
            DebugLog.d("Create async worker for CALL Process");
        }

        public void handleMessage(Message msg) {
            Log.d(THIS_FILE, "handleMessage msg.what:"+msg.what);
            DebugLog.d("Ordering TEST --> handleMessage --> "+msg.what);

            switch (msg.what) {
                case ON_INCOMING_CALL: {
                    DebugLog.d("Ordering TEST --> on_incoming_call in handleMessage");
                    // CallInfo callInfo = (CallInfo) msg.obj;
                    pjService.service.state = 1;

                    SipCallSession incomingCallInfo = (SipCallSession) msg.obj;
                    String callNum = "";

                    try {
                        DebugLog.d("remoteContact on_incoming_call TEST --> "+incomingCallInfo.getRemoteContact());
                        String[] divG = incomingCallInfo.getRemoteContact().split("@");
                        String[] divKey = divG[0].split(":");
                        callNum = divKey[1];
//                        String[] divBlank = incomingCallInfo.getRemoteContact().split(" ");
//                        String sipNumberIp = divBlank[1];
//                        String[] divIp = sipNumberIp.split("@");
//                        String sipNumber = divIp[0];
//                        String[] divKey = sipNumber.split(":");
//                        callNum = divKey[1];

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    DebugLog.d("remoteContact TEST on_incoming_call --> "+callNum);
                    DebugLog.d("remoteContact TEST on_incoming_call pushed Contact -->"+CallStatus.INSTANCE.getPushCallNumber());

                    DebugLog.d("handleMessage --> ON_INCOMING_CALL");
                    DebugLog.d("handleMessage isProtection is running --> "+ ServiceUtils.isRunningService(App.getGlobalApplicationContext(), InBoundCallMediaService.class));

                    CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.INCOMING, 0, callNum));
                    DebugLog.d("handleMessage --> ON_INCOMING_CALL waiting Call size --> "+ WaitingStatus.callWaitQueue.size());

                    if(!ServiceUtils.isRunningService(App.getGlobalApplicationContext(), InBoundCallMediaService.class)) {
                        Boolean snooze = new Boolean(true);
                        Intent intent = new Intent(App.getGlobalApplicationContext(), InBoundCallMediaService.class);
                        intent.putExtra(InBoundCallMediaService.Operations.ACTION_OPERATION_SNOOZE_START, snooze);
                        intent.putExtra("cid", callNum);

                        App.getGlobalApplicationContext().startService(intent);
                    }

                    pjService.service.disableKeyguard();
                    break;
                }
                case ON_CALL_STATE: {
                    DebugLog.d("ON_CALL_STATE");
                    DebugLog.d("Ordering TEST --> on_call_state in handleMessage : "+msg.what);

                    SipCallSession callInfo = (SipCallSession) msg.obj;

                    String callNum = "";

                    try {
                        DebugLog.d("remoteContact TEST on_call_state --> "+callInfo.getRemoteContact());

                        String[] divG = callInfo.getRemoteContact().split("@");
                        String[] divKey = divG[0].split(":");
                        callNum = divKey[1];

//                        String[] divBlank = callInfo.getRemoteContact().split(" ");
//                        String sipNumberIp = divBlank[1];
//                        String[] divIp = sipNumberIp.split("@");
//                        String sipNumber = divIp[0];
//                        String[] divKey = sipNumber.split(":");
//                        callNum = divKey[1];

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (mPrfs == null)
                        mPrfs = new AppPrefs(pjService.service);
                    if (mSysPrefs == null)
                        mSysPrefs = new PreferencesWrapper(pjService.service);

                    int callState = callInfo.getCallState();
                    DebugLog.d("callState --> "+callInfo.getCallState());

                    switch (callState) {
                        case SipCallSession.InvState.INCOMING:
                        case SipCallSession.InvState.CALLING:
                            DebugLog.d("launchCallHandler before in calling callInfo.getCallId --> "+callInfo.getCallId());
                            CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.CALLING, callInfo.getCallId(), callNum));

//                            pjService.service.disableKeyguard();
                            notificationManager.showNotificationForCall(callInfo);
                            launchCallHandler(callInfo);
                            pjService.service.disableKeyguard();

                            // 메모 초기화
                            mPrfs.setPreferenceStringValue(AppPrefs.LAST_CALL_MEMO, "");

                            // broadCastAndroidCallState("RINGING",
                            // callInfo.getRemoteContact());
                            break;
                        case SipCallSession.InvState.EARLY:
                            DebugLog.d("msg.what --> INV_STATE_EARLY "+msg.what);
                            CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.EARLY, callInfo.getCallId(), callNum));

                            // broadCastAndroidCallState("OFFHOOK",
                            // callInfo.getRemoteContact());
                            break;
                        case SipCallSession.InvState.CONFIRMED:
                            // broadCastAndroidCallState("OFFHOOK",
                            // callInfo.getRemoteContact());
                            DebugLog.d("msg.what --> INV_STATE_CONFIRMED "+msg.what);
                            CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.CONFIRMED, callInfo.getCallId(), callNum));
                            /**
                             * ADDED
                             */
                            Intent updateIntent = new Intent("in_call_notification_confirmed");
                            updateIntent.putExtra("cid", callNum);
                            updateIntent.setPackage(pjService.service.getApplicationContext().getPackageName());
                            App.getGlobalApplicationContext().sendBroadcast(updateIntent);

                            DebugLog.d("DEBUGGING ====");
                            if(pjService!=null && pjService.mediaManager!=null) {
//                                pjService.mediaManager.stopRing();
                                pjService.mediaManager.stopOnlyRing("call status in UAStateReceiver");
                            }

                            callInfo.callStart = System.currentTimeMillis();
                            break;
                        case SipCallSession.InvState.DISCONNECTED:
                            // TODO : should manage multiple calls
                            DebugLog.d("msg.what --> INV_STATE_DISCONNECTED "+msg.what);

                            CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.DISCONNECTED, 0, ""));
                            CallStatus.INSTANCE.updateCallAction(new CallActionData(0));
                            DebugLog.d("Ordering TEST --> DISCONNECTED");
                            DebugLog.d("Ordering TEST --> WaitingQueue size : "+WaitingStatus.callWaitQueue.size());

                            Intent intent = new Intent(App.getGlobalApplicationContext(), InBoundCallMediaService.class);
                            App.getGlobalApplicationContext().stopService(intent);

                            pjService.service.state = 0;
                            pjService.service.reenableKeyguard();
                            if (getActiveCallInProgress() == null) {
                                if (notificationManager!=null){
                                    notificationManager.cancelCalls();
                                }
                            }
                            Log.d(THIS_FILE, "Finish call2");
                            stopPlay();

                            final int Status = callInfo.getLastStatus();

                            if (!callInfo.isIncoming()) {
                                if (Status >= 400 && Status != 487) {// BJH 오류 코드 상태 Toast Message

                                    TimerTask tt = new TimerTask() {
                                        @Override
                                        public void run() {
                                            Log.e("1번태스크카운터:", String.valueOf(counter));
                                            counter++;

                                            if (App.isDialMainForeground){
                                                Log.d(THIS_FILE,"App.isDialMainForeground is true");

                                                DialMain.mErrorHandler.sendMessage(DialMain.mErrorHandler.obtainMessage(DialMain.ERROR_STATUS,
                                                        Status));

                                                if (timerErrorStatus!=null){
                                                    timerErrorStatus.cancel();
                                                    timerErrorStatus=null;
                                                    counter=0;
                                                }

                                            }else{
                                                Log.d(THIS_FILE,"App.isDialMainForeground is false");
                                            }

                                            if (counter>5){
                                                if (timerErrorStatus!=null){
                                                    timerErrorStatus.cancel();
                                                    timerErrorStatus=null;
                                                    counter=0;
                                                }
                                            }
                                        }
                                    };


                                    Intent dialIntent=new Intent(Dial.BC_DIALOG);
                                    dialIntent.putExtra("type","error");
                                    dialIntent.putExtra("status",Status);
                                    dialIntent.setPackage(pjService.service.getApplicationContext().getPackageName());
                                    pjService.service.sendBroadcast(dialIntent);

                                    if (!App.isDialForeground){
                                        if (timerErrorStatus == null){
                                            timerErrorStatus = new Timer();
                                            timerErrorStatus.schedule(tt, 0, 500);
                                        }
                                    }
                                }
                            }

                            DBManager database = new DBManager(pjService.service);
                            database.open();

                            ContentValues cv = RecentCallsData.getContentValues(
                                    pjService.service, callInfo, callInfo.callStart);
                            int call_type = cv.getAsInteger(RecentCallsData.FIELD_TYPE);
                            int duration = cv
                                    .getAsInteger(RecentCallsData.FIELD_DURATION);

					/*
					 * by sgkim : 2015-09-17 : 서버에서 갖고 오는 방식으로 수정됨.
					 * database.insertRecentCalls(cv);
					 */

                            ContentValues cv_favorite = FavoritesData.getContentValues(
                                    pjService.service, callInfo, callInfo.callStart);
                            String phone_number = cv_favorite
                                    .getAsString(FavoritesData.FIELD_PHONENUMBER);

                            phone_number = phone_number.replaceAll("\\-", "");

                            String display_name = ContactHelper
                                    .getContactsNameByPhoneNumber(pjService.service,
                                            phone_number);
                            if (display_name == null)
                                display_name = "";

                            // fix address '-' number
                        {
                            String _num = display_name.replaceAll("\\-", "");
                            if (_num.equals(phone_number)) {
                                display_name = phone_number;
                            }
                        }

                        // Save call number
                        // BJH 유심 관련 2016.06.29 불편사항 수정
                        //if (!phone_number.startsWith("+"))
                        //	phone_number = "+82" + phone_number;

                        if(phone_number.startsWith("00982")) {
                            phone_number = phone_number.replace("00982", "0");
                        }

                        mPrfs.setPreferenceStringValue(AppPrefs.LAST_CALL_NUMBER,
                                phone_number);

                        boolean existFavorite = false;
                        int density = 1;
                        Cursor c = database.getFavoriteByPhone(phone_number);
                        if (c != null) {
                            if (c.getCount() > 0) {
                                existFavorite = true;
                                if (c.moveToFirst()) {
                                    density = c
                                            .getInt(c
                                                    .getColumnIndex(FavoritesData.FIELD_DENSITY));
                                    density = density + 1;
                                }
                            }
                            c.close();
                        }

                        int fav_add = 0;
                        int fav_score = 0;

                        if (existFavorite) {
                            database.updateFavoriteDensity(phone_number, density);
                            fav_add = 1;
                            fav_score = 10;
                        } else {
                            boolean use_favorite = mSysPrefs
                                    .getPreferenceBooleanValue(PreferencesWrapper.USE_FAVORITE);
                            if (use_favorite) {
                                database.insertFavorites(cv_favorite);
                                fav_add = 1;
                                fav_score = 10;
                                existFavorite = true;
                            }
                        }

                        // score
                        long endTime = System.currentTimeMillis();
                        int score = 0;

                        int call_count = 1;
                        int call_count_score = 1;

                        // long call_start = 0;
                        int call_start_score = 4;

                        int call_state = 0;
                        int call_state_score = 0;

                        int call_dur = duration;
                        int call_dur_score = ScoreUtils.getScoreDuration(call_dur);

                        int con_add = 0;
                        int con_score = 0;

                        int loc_score = 0;

                        // 즐겨찾기에 있는 경우만 연산.
                        if (existFavorite) {
                            if (ServicePrefs.checkGps(pjService.service)) {
                                Double myLatitude = Util
                                        .parseDouble(mPrfs
                                                .getPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE));
                                Double myLongitude = Util
                                        .parseDouble(mPrfs
                                                .getPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE));
                                Double lastLatitude, lastLongitude;

                                // 위치정보 계산
                                boolean Found = false;
                                Cursor cur_loc = database
                                        .getZoneByPhone(phone_number);
                                if (cur_loc != null) {
                                    // 현재 위치와 등록된 사용자와의 거리를 계산
                                    if (cur_loc.moveToFirst()) {
                                        do {
                                            lastLatitude = cur_loc
                                                    .getDouble(cur_loc
                                                            .getColumnIndex(ZoneData.FIELD_LATITUDE));
                                            lastLongitude = cur_loc
                                                    .getDouble(cur_loc
                                                            .getColumnIndex(ZoneData.FIELD_LONGITUDE));
                                            // distance 계산
                                            Location locationA = new Location(
                                                    "point A");

                                            locationA.setLatitude(lastLatitude);
                                            locationA.setLongitude(lastLongitude);

                                            Location locationB = new Location(
                                                    "point B");

                                            locationB.setLatitude(myLatitude);
                                            locationB.setLongitude(myLongitude);

                                            float distance = locationA
                                                    .distanceTo(locationB);

                                            if (distance <= 500.0f) {
                                                // Found
                                                Found = true;
                                                loc_score = 10;
                                                break;
                                            }
                                        } while (cur_loc.moveToNext());
                                    }

                                    cur_loc.close();

                                    if (!Found) {
                                        // INSERT ZONE
                                        ZoneData zone = new ZoneData(phone_number,
                                                myLatitude, myLongitude, 0);
                                        database.insertZones(zone);
                                        loc_score = 10;
                                    }

                                } else {
                                    ZoneData zone = new ZoneData(phone_number,
                                            myLatitude, myLongitude, 0);
                                    database.insertZones(zone);
                                    loc_score = 10;
                                }

                                database.updateScoreLocation(phone_number,
                                        loc_score);
                            }

                            // 전화 시간대.
                            if (callInfo.callStart != 0) {
                                Date startDate = new Date(callInfo.callStart);

                                // 위치정보 계산
                                boolean Found = database.existCTimeByPhone(
                                        phone_number, startDate);

                                if (!Found) {
                                    CTimeData ctime = new CTimeData(phone_number,
                                            startDate);
                                    database.insertCTime(ctime);
                                }

                                call_start_score = 10;
                                database.updateScoreCTime(phone_number,
                                        call_start_score);
                            }

                            // 주소록 검색
                            if (display_name.length() > 0) {
                                con_add = 1;
                                con_score = 6;
                            }

                            if (call_type == RecentCallsData.MISSED_TYPE)
                                call_state_score = 10;
                            else
                                call_state_score = 6;

                            if (!database.existScoreByPhone(phone_number)) {
                                score = loc_score + call_count_score
                                        + call_start_score + call_dur_score
                                        + call_state_score + con_score + fav_score;

                                // 신규 추가
                                ContentValues score_cv = new ContentValues();
                                score_cv.put(ScoreData.FIELD_NUMBER, phone_number);
                                score_cv.put(ScoreData.FIELD_SCORE, score);
                                score_cv.put(ScoreData.FIELD_ZONE_ID, 0);
                                score_cv.put(ScoreData.FIELD_ZONE_SCORE, loc_score);
                                score_cv.put(ScoreData.FIELD_CALL_COUNT, call_count);
                                score_cv.put(ScoreData.FIELD_CALL_COUNT_SCORE,
                                        call_count_score);
                                score_cv.put(ScoreData.FIELD_CALL_START, endTime);
                                score_cv.put(ScoreData.FIELD_CALL_START_SCORE,
                                        call_start_score);
                                score_cv.put(ScoreData.FIELD_CALL_DUR, 1);
                                score_cv.put(ScoreData.FIELD_CALL_DUR_SCORE,
                                        call_dur_score);
                                score_cv.put(ScoreData.FIELD_CALL_STATE, call_state);
                                score_cv.put(ScoreData.FIELD_CALL_STATE_SCORE,
                                        call_state_score);
                                score_cv.put(ScoreData.FIELD_CON_ADD, con_add);
                                score_cv.put(ScoreData.FIELD_CON_ADD_SCORE,
                                        con_score);
                                score_cv.put(ScoreData.FIELD_FAV_ADD, fav_add);
                                score_cv.put(ScoreData.FIELD_FAV_ADD_SCORE,
                                        fav_score);
                                score_cv.put(ScoreData.FIELD_CAL_ADD, 0);
                                score_cv.put(ScoreData.FIELD_CAL_ADD_SCORE, 0);
                                score_cv.put(ScoreData.FIELD_F1, 0);
                                score_cv.put(ScoreData.FIELD_F1_SCORE, 0);
                                database.insertScores(score_cv);
                            } else {

                                Cursor cur = database.getScoreByPhone(phone_number);

                                if (cur != null) {
                                    if (cur.getCount() > 0 && cur.moveToFirst()) {
                                        call_count = cur
                                                .getInt(cur
                                                        .getColumnIndex(ScoreData.FIELD_CALL_COUNT));
                                        call_dur = cur
                                                .getInt(cur
                                                        .getColumnIndex(ScoreData.FIELD_CALL_DUR));
                                        // call_start =
                                        // cur.getLong(cur.getColumnIndex(ScoreData.FIELD_CALL_START));
                                    }
                                    cur.close();
                                }

                                // update
                                call_count = call_count + 1;
                                call_count_score = ScoreUtils
                                        .getScoreCallCount(call_count);

                                call_dur = call_dur + duration;
                                call_dur_score = ScoreUtils
                                        .getScoreDuration(call_dur);

                                score = loc_score + call_count_score
                                        + call_start_score + call_dur_score
                                        + call_state_score + con_score + fav_score;

                                database.updateScoreCall(phone_number, loc_score,
                                        call_count, endTime, call_state, call_dur,
                                        con_add, call_count_score,
                                        call_start_score, call_dur_score,
                                        call_state_score, con_score, score);
                            }
                        }

                        database.close();

                        Integer isNew = cv
                                .getAsInteger(RecentCallsData.FIELD_IDENTIFIER);

                        if (isNew != null && isNew == 1 && Status != 603) {
                            try {
                                if(cv!=null) {
                                    notificationManager.showNotificationForMissedCall(cv);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        callInfo.setIncoming(false);
                        callInfo.callStart = 0;

                        break;
                        default:
                            break;
                    }
                    onBroadcastCallState(callInfo);
                    break;
                }
                case ON_MEDIA_STATE: {
                    SipCallSession mediaCallInfo = (SipCallSession) msg.obj;
                    SipCallSession callInfo = callsList.get(mediaCallInfo
                            .getCallId());
                    callInfo.setMediaStatus(mediaCallInfo.getMediaStatus());
                    onBroadcastCallState(callInfo);
                    break;
                }
                case ON_REGISTRATION_STATE: {
                    Log.d(THIS_FILE, "In reg state");
                    // Update sip pjService (for notifications
                    // BJH java.lang.NullPointerException 2016.5.26
                    ArrayList<SipProfileState> activeAccountsInfos = new ArrayList<SipProfileState>();
                    try {
                        activeAccountsInfos = pjService.service
                                .getRegistrationsState();
                        Log.d(THIS_FILE, activeAccountsInfos + " ");
                    } catch (NullPointerException e) {
                        Log.e(THIS_FILE, "NullPointerException=" + e.getMessage());
                        // activeAccountsInfos = null;
                    }
                    ((SipService) pjService.service)
                            .updateRegistrationsState(activeAccountsInfos);
                    // Send a broadcast message that for an account

                    // registration state has changed
                    Intent regStateChangedIntent = new Intent(
                            SipManager.ACTION_SIP_REGISTRATION_CHANGED);

                    regStateChangedIntent.setPackage(pjService.service.getApplicationContext().getPackageName());
                    pjService.service.sendBroadcast(regStateChangedIntent);
                    break;
                }
                case ON_PAGER: {
                    // startSMSRing();s
                    // String message = (String) msg.obj;
                    // pjService.showMessage(message);
                    // Log.e(THIS_FILE, "yana you in CASE ON_PAGER");
                    // stopRing();
                    break;
                }
            }
        }
    }

    ;

    private void treatIncomingCall(int accountId, SipCallSession callInfo) {
        DebugLog.d("Ordering TEST --> treatIncomingCall");
        DebugLog.d("treatIncomingCall called... --> callInfo : "+callInfo.getRemoteContact());

//        WaitingStatus.callWaitQueue.add(RemoteContactParser.parseRemoteContact(callInfo.getRemoteContact()).getPhoneNumber());

        int callId = callInfo.getCallId();
        // Get lock while ringing to be sure notification is well done !
        if (incomingCallLock == null) {
            /**
             *  안드로이드10 관련 수정 2020.05.29 맹완석
             *  사장님 지시로 newWakeLock tag 수정
             */
            PowerManager pman = (PowerManager) pjService.service.getSystemService(Context.POWER_SERVICE);
            incomingCallLock = pman.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "maaltalk:com.dial070.sip.incomingCallLock");
            incomingCallLock.setReferenceCounted(false);
        }
        // Extra check if set reference counted is false ???
        if (!incomingCallLock.isHeld()) {
            incomingCallLock.acquire();
        }
        pjService.service.getSystemService(Context.POWER_SERVICE);

        String remContact = callInfo.getRemoteContact();
        callInfo.setIncoming(true);
        notificationManager.showNotificationForCall(callInfo);

        // Auto answer feature
        SipProfile acc = pjService.getAccountForPjsipId(accountId);
        boolean shouldAutoAnswer = pjService.service.shouldAutoAnswer(remContact, acc);

        // Or by api
        if (shouldAutoAnswer) {
            // Automatically answer incoming calls with 200/OK
            pjService.callAnswer(callId, 200);
        } else {

            // Automatically answer incoming calls with 180/RINGING
            pjService.callAnswer(callId, 180);

            if (pjService.service.getGSMCallState() == TelephonyManager.CALL_STATE_IDLE) {
                DebugLog.d("Ordering TEST --> treatIncomingCall TelephonyManager.CALL_STATE_IDLE");
                if (pjService.mediaManager != null) {
//                    if (Build.VERSION.SDK_INT >= 29) {
//                        /**
//                         * MODEFIED
//                         * 2020.08.19
//                         * Fcm 클래스에서 블루투스 활용을 위해 ringer 직접 사용 종속 제거
//                         * 따라서 BridgeMediaManager를 체크해야 함
//                         */
////                        if (Fcm.ringer==null || !Fcm.ringer.isRinging()){
//                        if (Fcm.mediaManager==null || !Fcm.mediaManager.isRinging() && !RingerStatus.INSTANCE.isMediaRingerIsRinging()){
//                            DebugLog.d("current state --> Fcm.ringer==null || !Fcm.ringer.isRinging()");
////                            DebugLog.d("Fcm.mediaManager.isRinging --> "+Fcm.mediaManager.isRinging());
////                            pjService.mediaManager.startRing(remContact);
//                            Fcm.stopRing();
//                            Intent connectionSuccessIntent = new Intent("treat_incoming_call");
//                            App.getGlobalApplicationContext().sendBroadcast(connectionSuccessIntent);
//
//                            pjService.mediaManager.startRing(remContact, "treatIncomingCall_step_1");
//
//                        } else {
//                            /**
//                             * ADDED
//                             * 2020.08.19 FullScreenIntent를 통한 애플리케이션 진입 시 Bridge로 제어되던 미디어 처리를
//                             * 핸드폰의 MediaManager가 가져옴.
//                             */
//                            DebugLog.d("current state --> !(Fcm.ringer==null || Fcm.ringer.isRinging())");
//                            DebugLog.d("Fcm.mediaManager.isRinging --> "+Fcm.mediaManager.isRinging());
//                            DebugLog.d("Fcm.ringer.isRinging --> "+Fcm.ringer.isRinging());
////                            Fcm.stopRing();
//
////                            Fcm.mediaManager.stopAnnoucing();
//                            /**
//                             * RING TEST
//                             */
//                            pjService.mediaManager.startRing(remContact, "treatIncomingCall_step_2");
//                        }
//                    } else {
//                        DebugLog.d("current state --> SDK_INT is under 29");
//                        if(!RingerStatus.INSTANCE.isMediaRingerIsRinging()) {
//                            Intent connectionSuccessIntent = new Intent("treat_incoming_call");
//                            App.getGlobalApplicationContext().sendBroadcast(connectionSuccessIntent);
//                            pjService.mediaManager.startRing(remContact, "treatIncomingCall_step_3");
//                        }
//                    }

//                    DebugLog.d("current state --> SDK_INT is under 29");
                    if(!RingerStatus.INSTANCE.isMediaRingerIsRinging()) {
                        Intent connectionSuccessIntent = new Intent("treat_incoming_call");
                        App.getGlobalApplicationContext().sendBroadcast(connectionSuccessIntent);
                        pjService.mediaManager.startRing(remContact, "treatIncomingCall_step_3");
                    }
                }
                // broadCastAndroidCallState("RINGING", remContact);
            } else {
                // Phone State Permission 관련 버그 Notification 처리
            }
        }
//        Crashlytics.logException(new Exception("treatcall ended !!!"));

        launchCallHandler(callInfo);
    }

    // -------
    // Public configuration for receiver
    // -------

    public void initService(PjSipService srv) {
        DebugLog.d("UAStateReceiver initService --> srv : "+srv.getClass().getSimpleName());
        pjService = srv;
        notificationManager = pjService.service.notificationManager;

        if (handlerThread == null) {
            handlerThread = new HandlerThread("UAStateAsyncWorker");
            handlerThread.start();
        }
        if (msgHandler == null) {
            msgHandler = new WorkerHandler(handlerThread.getLooper());
        }

        if (eventLock == null) {
            PowerManager pman = (PowerManager) pjService.service
                    .getSystemService(Context.POWER_SERVICE);
            eventLock = pman.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "maaltalk:com.dial070.sip.inEventLock");
            eventLock.setReferenceCounted(true);
        }
    }

    public void stopService() {
        DebugLog.d("UAStateReceiver stopService called...");

        Threading.stopHandlerThread(handlerThread);
        handlerThread = null;
        msgHandler = null;

        // Ensure lock is released since this lock is a ref counted one.
        if (eventLock != null) {
            while (eventLock.isHeld()) {
                eventLock.release();
            }
        }
    }

    // --------
    // Private methods
    // --------

    private void onBroadcastCallState(final SipCallSession callInfo) {
        Log.d(THIS_FILE,"onBroadcastCallState:"+callInfo.getCallId()+", Status:"+callInfo.getLastStatus());
        DebugLog.d("onBroadcastCallState callId --> "+callInfo.getCallId());
        DebugLog.d("onBroadcastCallState status --> "+callInfo.getLastStatus());

        // Internal event
        Intent callStateChangedIntent = new Intent(SipManager.ACTION_SIP_CALL_CHANGED);
        callStateChangedIntent.putExtra(SipManager.EXTRA_CALL_INFO, callInfo);
        pjService.service.sendBroadcast(callStateChangedIntent);

    }

    // private void broadCastAndroidCallState(String state, String number) {
    // //Android normalized event
    // Intent intent = new Intent(ACTION_PHONE_STATE_CHANGED);
    // intent.putExtra(TelephonyManager.EXTRA_STATE, state);
    // if (number != null) {
    // intent.putExtra(TelephonyManager.EXTRA_INCOMING_NUMBER, number);
    // }
    // intent.putExtra(pjService.service.getString(R.string.app_name), true);
    // pjService.service.sendBroadcast(intent,
    // android.Manifest.permission.READ_PHONE_STATE);
    // }
    //

    /**
     *
     * @param currentCallInfo2
     */
    private synchronized void launchCallHandler(SipCallSession currentCallInfo2) {
        Log.i(THIS_FILE, "InCall2.currentState:"+InCall2.currentState);
        Log.i(THIS_FILE, "InCall2.currentContext:"+InCall2.currentContext);

        DebugLog.d("InCall2.currentState --> "+InCall2.currentState);
        DebugLog.d("InCall2.currentState --> "+InCall2.currentContext);

//        if (InCall2.currentContext != null && InCall2.currentState != 0) {
////            Log.d(THIS_FILE, "SKIP : launchCallHandler");
//
//            DebugLog.d("pjService.service activity not executed...");
//
////            Crashlytics.logException(new Exception("pjService.service activity not executed"));
//            return;
//        }

        // 2017-12-05 : CHECK RTT & CODEC
        /*if (SipService.RTT >= 250) {
            String codec = "G729";
            Log.d(THIS_FILE, "ONCALL : SET CODEC=" + codec + ", RTT=" + SipService.RTT);
            pjService.prefsWrapper.setMediaCodec(codec);
            pjService.setCodecsPriorities();
        }*/

        DebugLog.d("launchCallHandler check received push call number --> "+CallStatus.INSTANCE.getPushCallNumber());
        DebugLog.d("launchCallHandler check current call info number  --> "+currentCallInfo2.getRemoteContact());
        DebugLog.d("Ordering TEST --> launchCallHandler");

        Intent callHandlerIntent = new Intent(SipManager.ACTION_SIP_CALL_UI);
        if (App.isReturnClicked){
            callHandlerIntent = new Intent(SipManager.ACTION_SIP_CALL_RETURN_UI);
        }

        if (currentCallInfo2 != null)
            callHandlerIntent.putExtra(SipManager.EXTRA_CALL_INFO, currentCallInfo2);

        callHandlerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP /*
//        callHandlerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK /*
         * |
         * Intent.FLAG_ACTIVITY_NO_ANIMATION
         */);

//        Log.d(THIS_FILE, "Anounce call activity");

        DebugLog.d("Announce InCall2 Activity");

        if (pjService.service==null){
            Log.d(THIS_FILE, "pjService.service==null");
//            Crashlytics.logException(new Exception("pjService.service==null"));

            DebugLog.d("don't launched InCall2 Activity");
            App.getGlobalApplicationContext().startActivity(callHandlerIntent);

        }else {

            DebugLog.d("launched InCall2 Activity");

            Log.d(THIS_FILE, "pjService.service.startActivity");
//            Crashlytics.logException(new Exception("pjService.service activity callpopup"));
            pjService.service.startActivity(callHandlerIntent);
        }

    }

    /**
     * Check if any of call infos indicate there is an active call in progress.
     */
    public SipCallSession getActiveCallInProgress() {
        if (callsList == null)
            return null;

        // getCallInfo 내부에서 lock을 한다.
        {
            for (Integer i : callsList.keySet()) {
                SipCallSession callInfo = getCallInfo(i, false);
                if (callInfo.isActive()) {
                    return callInfo;
                }
            }
        }
        return null;
    }

    public SipCallSession getIncomingCallInProgress() {
        if(callsList == null) {
            return null;
        }
        for(Integer i : callsList.keySet()) {
            SipCallSession callInfo = getCallInfo(i, false);
            if(callInfo.isIncoming()) {
                return callInfo;
            }
        }

        return null;
    }

    public int getIncomingCallCountInProgress() {
        int count = 0;

        if(callsList == null) {
            return count;
        }

        for(Integer i : callsList.keySet()) {
            SipCallSession callInfo = getCallInfo(i, false);
            if(callInfo.isIncoming()) {
                count += 1;
            }
        }

        return count;
    }

    /**
     * Check if any of call infos indicate there is an active call in progress.
     */
    public SipCallSession getActiveCallInfo() {
        if (callsList == null)
            return null;

        for (Integer i : callsList.keySet()) {
            SipCallSession callInfo = getCallInfo(i, false);

            /*Log.i(THIS_FILE,"getActiveCallInProgress getCallId:"+callInfo.getCallId());
            Log.i(THIS_FILE,"getActiveCallInProgress getCallState:"+callInfo.getCallState());
            Log.i(THIS_FILE,"getActiveCallInProgress getLastStatusText:"+callInfo.getLastStatusText());
            Log.i(THIS_FILE,"getActiveCallInProgress getLastStatus:"+callInfo.getLastStatus());
            Log.i(THIS_FILE,"getActiveCallInProgress getMediaStatus:"+callInfo.getMediaStatus());
            Log.i(THIS_FILE,"getActiveCallInProgress getRemoteContact:"+callInfo.getRemoteContact());
            Log.i(THIS_FILE,"getActiveCallInProgress isIncoming:"+callInfo.isIncoming());
            Log.i(THIS_FILE,"getActiveCallInProgress isActive:"+callInfo.isActive());
            Log.i(THIS_FILE,"getActiveCallInProgress isSecure:"+callInfo.isSecure());
            Log.i(THIS_FILE,"getActiveCallInProgress getConnectStart:"+callInfo.getConnectStart());
            Log.i(THIS_FILE,"getActiveCallInProgress getAccId:"+callInfo.getAccId());
            Log.i(THIS_FILE,"getActiveCallInProgress callStart:"+callInfo.callStart);*/

            if (callInfo.isActive()) {
                return callInfo;
            }
        }
        return null;
    }

    public void resetActiveCallSession() {
        // 네트워크가 비정상적인 경우 삭제.
        if (callsList == null)
            return;

        // getCallInfo 내부에서 lock을 한다.
        {
            for (Integer i : callsList.keySet()) {
                SipCallSession callInfo = getCallInfo(i, false);
                if (callInfo.isActive()) {
                    callInfo.setCallState(SipCallSession.InvState.NULL);
                }
            }
        }
    }

    /**
     * Broadcast the Headset button press event internally if there is any call
     * in progress.
     */
    public boolean handleHeadsetButton() {
        SipCallSession callInfo = getActiveCallInProgress();
        if (callInfo != null) {
            // Headset button has been pressed by user. If there is an
            // incoming call ringing the button will be used to answer the
            // call. If there is an ongoing call in progress the button will
            // be used to hangup the call or mute the microphone.
            int state = callInfo.getCallState();
            if (callInfo.isIncoming()
                    && (state == SipCallSession.InvState.INCOMING || state == SipCallSession.InvState.EARLY)) {
                pjService.callAnswer(callInfo.getCallId(),
                        pjsip_status_code.PJSIP_SC_OK.swigValue());
                return true;
            } else if (state == SipCallSession.InvState.INCOMING
                    || state == SipCallSession.InvState.EARLY
                    || state == SipCallSession.InvState.CALLING
                    || state == SipCallSession.InvState.CONFIRMED
                    || state == SipCallSession.InvState.CONNECTING) {
                //
                // In the Android phone app using the media button during
                // a call mutes the microphone instead of terminating the call.
                // We check here if this should be the behavior here or if
                // the call should be cleared.
                //
                switch (pjService.prefsWrapper.getHeadsetAction()) {
                    // TODO : add hold -
                    case PreferencesWrapper.HEADSET_ACTION_CLEAR_CALL:
                        pjService.callHangup(callInfo.getCallId(), 0);
                        break;
                    case PreferencesWrapper.HEADSET_ACTION_MUTE:
                        pjService.mediaManager.toggleMute();
                        break;
                }
                return true;
            }
        }
        return false;
    }

    private int playerId = -1;

    // private int playerConfPort = -1;
    // private int playPort = -1;

    public String getSoundPath() {
        String path = null;
        Context context = pjService.service.getApplicationContext();

        try {
            String url = "android.resource://" + context.getPackageName()
                    + "/raw/sound_notice"; // "/" + R.raw.sound_notice;
            Uri soundUri = Uri.parse(url);

            String[] proj = {MediaStore.MediaColumns.DATA};
            Cursor c = context.getContentResolver().query(soundUri, proj, null,
                    null, null);
            if (c != null) {
                c.moveToNext();
                path = c.getString(c
                        .getColumnIndex(MediaStore.MediaColumns.DATA));
                c.close();
            }
        } catch (Exception e) {
        }

        if (path != null) {
            File f = new File(path);
            return f.getAbsolutePath();
        }

        return null;
    }

    // Player
    private SoundPool sound_pool = null;
    private int sound_beep = 0;

    public synchronized void startPlay() {
        if (!ServicePrefs.DIAL_PLAY_OFFLINE)
            return;

        if (playerId == -1) {
            Log.d(THIS_FILE, "****** >> PLAY SOUND");

            Context context = pjService.service.getApplicationContext();
            if (sound_pool == null) {
                sound_pool = new SoundPool(5, AudioManager.STREAM_VOICE_CALL, 0);
                sound_pool
                        .setOnLoadCompleteListener(new OnLoadCompleteListener() {
                            @Override
                            public void onLoadComplete(SoundPool soundPool,
                                                       int sampleId, int status) {
                                soundPool.play(sampleId, 1f, 1f, 0, -1, 1f);
                            }
                        });
                sound_beep = sound_pool.load(context, R.raw.sound_call_offline,
                        1);
            }

            playerId = 1;
        }
    }

    public synchronized void stopPlay() {
        if (!ServicePrefs.DIAL_PLAY_OFFLINE)
            return;

        if (playerId != -1) {
            Log.d(THIS_FILE, "****** >> STOP SOUND");

            sound_pool.stop(sound_beep);
            sound_pool.release();
            sound_pool = null;

            // pjsua.player_destroy(playerId);
            playerId = -1;
        }
    }

    // Recorder
    private static int INVALID_RECORD = -1;
    private int recordedCall = INVALID_RECORD;
    private int recPort = -1;
    private int recorderId = -1;
    private int recordedConfPort = -1;

    public void startRecording(int callId) {
        // Ensure nothing is recording actually
        if (recordedCall == INVALID_RECORD) {
            SipCallSession callInfo = getCallInfo(callId, false);
            if (callInfo == null
                    || callInfo.getMediaStatus() != SipCallSession.MediaState.ACTIVE) {
                return;
            }

            File mp3File = getRecordFile(callInfo.getRemoteContact());
            if (mp3File != null) {
                int[] recId = new int[1];
                pj_str_t filename = pjsua
                        .pj_str_copy(mp3File.getAbsolutePath());
                int status = pjsua.recorder_create(filename, 0, (byte[]) null,
                        0, 0, recId);
                if (status == pjsuaConstants.PJ_SUCCESS) {
                    recorderId = recId[0];
                    Log.d(THIS_FILE, "Record started : " + recorderId);
                    recordedConfPort = callInfo.getConfPort();
                    recPort = pjsua.recorder_get_conf_port(recorderId);
                    pjsua.conf_connect(recordedConfPort, recPort);
                    pjsua.conf_connect(0, recPort);
                    recordedCall = callId;
                }
            } else {
                // TODO: toaster
                Log.w(THIS_FILE, "Impossible to write file");
            }
        }
    }

    public void stopRecording() {
        Log.d(THIS_FILE, "Stop recording " + recordedCall + " et " + recorderId);
        if (recorderId != -1) {
            pjsua.recorder_destroy(recorderId);
            recorderId = -1;
        }
        recordedCall = INVALID_RECORD;
    }

    public boolean canRecord(int callId) {
        if (recordedCall == INVALID_RECORD) {
            SipCallSession callInfo = getCallInfo(callId, false);
            if (callInfo == null
                    || callInfo.getMediaStatus() != SipCallSession.MediaState.ACTIVE) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int getRecordedCall() {
        return recordedCall;
    }

    private File getRecordFile(String remoteContact) {
        File dir = PreferencesWrapper.getRecordsFolder();
        if (dir != null) {
            Date d = new Date();
            File file = new File(dir.getAbsoluteFile() + File.separator
                    + sanitizeForFile(remoteContact) + "_"
                    + DateFormat.format("MM-dd-yy_kkmmss", d) + ".wav");
            Log.d(THIS_FILE, "Out dir " + file.getAbsolutePath());
            return file;
        }
        return null;
    }

    private String sanitizeForFile(String remoteContact) {
        String fileName = remoteContact;
        fileName = fileName.replaceAll("[\\.\\\\<>:; \"\'\\*]", "_");
        return fileName;
    }

    // BJH
    private void toastMessage(Context context, int status) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = inflater.inflate(R.layout.toast_view, null);
        TextView textView = (TextView) toastRoot.findViewById(R.id.toast_text);
        if (status == 403)
            textView.setText(status
                    + ": "
                    + context.getResources()
                    .getString(R.string.status_code_603)
                    + context.getResources().getString(
                    R.string.status_code_svc_center));
        else if (status == 404)
            textView.setText(status
                    + ": "
                    + context.getResources()
                    .getString(R.string.status_code_404)
                    + context.getResources().getString(
                    R.string.status_code_svc_center));
        else if (status == 486)
            textView.setText(status
                    + ": "
                    + context.getResources()
                    .getString(R.string.status_code_486)
                    + context.getResources().getString(
                    R.string.status_code_svc_center));
        else if (status == 603)
            textView.setText(status
                    + ": "
                    + context.getResources()
                    .getString(R.string.status_code_603)
                    + context.getResources().getString(
                    R.string.status_code_svc_center));
        else
            textView.setText(status
                    + ": "
                    + context.getResources()
                    .getString(R.string.status_code_etc)
                    + context.getResources().getString(
                    R.string.status_code_svc_center));

        final Toast toast = new Toast(context);
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        //BJH 2016.5.26
        mCountDownTimer = new CountDownTimer(3000, 1000 /* Tick duration */) {
            public void onTick(long millisUntilFinished) {
                toast.show();
            }

            public void onFinish() {
                toast.cancel();
            }
        };

        toast.show();
        mCountDownTimer.start();

    }

}
