/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils.audio;

import com.dial070.sip.service.*;
//import com.dial070.sip.utils.*;
import com.dial070.utils.*;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

public class AudioFocus3 extends AudioFocusWrapper {
	

	final static String PAUSE_ACTION = "com.android.music.musicservicecommand.pause";
	final static String TOGGLEPAUSE_ACTION = "com.android.music.musicservicecommand.togglepause";
	private static final String THIS_FILE = "AudioFocus3";
	
	private AudioManager audioManager;
	private SipService service;
	
	private boolean isMusicActive = false;
	private boolean isFocused = false;
	private HeadsetButtonReceiver headsetButtonReceiver;
	
	public void init(SipService aService, AudioManager manager) {
		DebugLog.d("init AudioFocus3 caller --> "+aService.getClass().getSimpleName());
		service = aService;
		audioManager = manager;
	}

	public void focus() {
		DebugLog.d("Focus again -->");

		if(!isFocused) {
			pauseMusic();
			registerHeadsetButton();
			isFocused = true;
		}
	}
	
	public void unFocus() {
		DebugLog.d("unFocus");

		if(isFocused) {
			restartMusic();
			unregisterHeadsetButton();
			isFocused = false;
		}
	}

	@Override
	public boolean hasFocus() {
		return isFocused;
	}

	private void pauseMusic() {
		DebugLog.d("pauseMusic --> PAUSE_ACTION broadcast");

		isMusicActive = audioManager.isMusicActive();
		if(isMusicActive && service.prefsWrapper.integrateWithMusicApp()) {
			service.sendBroadcast(new Intent(PAUSE_ACTION));
		}
	}
	
	private void restartMusic() {
		DebugLog.d("restartMusic --> TOGGLEPAUSE_ACTION broadcast");

		if(isMusicActive && service.prefsWrapper.integrateWithMusicApp()) {
			service.sendBroadcast(new Intent(TOGGLEPAUSE_ACTION));
		}
	}
	
	private void registerHeadsetButton() {
		DebugLog.d("Register media button --> headsetButtonReceiver");
//		Log.d(THIS_FILE, "Register media button");
		IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
		intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY +100 );
		if(headsetButtonReceiver == null) {
			headsetButtonReceiver = new HeadsetButtonReceiver();
			HeadsetButtonReceiver.setService(SipService.getUAStateReceiver());
		}
		service.registerReceiver(headsetButtonReceiver, intentFilter);
	}
	
	private void unregisterHeadsetButton() {
		DebugLog.d("unRegister media button --> ");
		try {
			service.unregisterReceiver(headsetButtonReceiver);
			HeadsetButtonReceiver.setService(null);
			headsetButtonReceiver = null;
		}catch(Exception e) {
			//Nothing to do else. just consider it has not been registered
		}
	}
	@Override 
	public String getProperty(String property) { //BJH 2016.07.18
	    return null; 
	}

}
