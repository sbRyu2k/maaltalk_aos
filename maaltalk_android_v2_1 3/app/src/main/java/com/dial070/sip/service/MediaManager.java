/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.dial070.global.ServicePrefs;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.Ringer;
import com.dial070.sip.utils.accessibility.AccessibilityWrapper;
import com.dial070.sip.utils.audio.AudioFocusWrapper;
import com.dial070.sip.utils.audio.MyAudioRecord;
import com.dial070.sip.utils.bluetooth.BluetoothWrapper;
import com.dial070.sip.utils.bluetooth.BluetoothWrapper.BluetoothChangeListener;
import com.dial070.status.CallStatus;
import com.dial070.status.RingerStatus;
import com.dial070.ui.CallStat;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

import org.pjsip.pjsua.pjsua;

import java.util.Locale;

import static com.dial070.sip.service.SipService.pjService;

public class MediaManager implements BluetoothChangeListener {

	final private static String THIS_FILE = "MediaManager";

	private static final int BLUETOOTH_DEVICE_CONNECTED = 2;
	private static final int BLUETOOTH_DEVICE_CONNECTING = 1;
	private static final int BLUETOOTH_DEVICE_DISCONNECTED = 0;

	private SipService service;
	private AudioManager audioManager;
	private Ringer ringer;

	// Locks
	private WifiLock wifiLock;
	private WakeLock screenLock;

	// Media settings to save / resore
//	private int savedVibrateRing, savedVibradeNotif, savedWifiPolicy, savedRingerMode;
//	private int savedVolume;
	// private boolean savedMicrophoneMute;
	private int savedRoute, savedMode;
	private boolean isSavedAudioState = false, isSetAudioMode = false;

	// By default we assume user want bluetooth.
	// If bluetooth is not available connection will never be done and then
	// UI will not show bluetooth is activated
	private boolean userWantBluetooth = true;
	private boolean userWantSpeaker = false;
	private boolean userWantMicrophoneMute = false;

	private Intent mediaStateChangedIntent;

	// Bluetooth related
	private BluetoothWrapper bluetoothWrapper;

	private AudioFocusWrapper audioFocusWrapper;

	private AccessibilityWrapper accessibilityManager;

	private static int MODE_SIP_IN_CALL = AudioManager.MODE_NORMAL;
	private boolean needSoundFix = false;
	private boolean startBeforeInit = false;

	/**
	 * ADDED
	 */
	private String remoteContact;

	private PhoneStateChangeReceiver mNativePhoneReceiver;
	private RingerModeChangeReceiver mRingerModeReceiver;
	private NotificationActionReceiver mNotificationActionReceiver;
	private ScreenChangeReceiver mScreenChangeReceiver;

	public static MediaManager INSTANCE = null;

	public static MediaManager getInstance(SipService aService) {
		if(INSTANCE == null) {
			INSTANCE = new MediaManager(aService);
		}
		return INSTANCE;
	}

	public static void destroy() {
		if(INSTANCE!=null) {
			INSTANCE = null;
		}
	}

	private MediaManager(SipService aService) {
		DebugLog.d("caller --> "+aService.getClass().getSimpleName());

		service = aService;
		audioManager = (AudioManager) service.getSystemService(Context.AUDIO_SERVICE);
		accessibilityManager = AccessibilityWrapper.getInstance();
		accessibilityManager.init(service);
		ringer = new Ringer(service);
		mediaStateChangedIntent = new Intent(SipManager.ACTION_SIP_MEDIA_CHANGED);

		/**
		 * ADDED
		 */
		mNativePhoneReceiver = new PhoneStateChangeReceiver();
		mRingerModeReceiver = new RingerModeChangeReceiver();
		mNotificationActionReceiver = new NotificationActionReceiver();
		mScreenChangeReceiver = new ScreenChangeReceiver();
	}

	public void startService() {
		DebugLog.d("startService() called --> initialize media manager");

		if (bluetoothWrapper == null) {
			bluetoothWrapper = BluetoothWrapper.getInstance(service);
			bluetoothWrapper.setBluetoothChangeListener(this);
			bluetoothWrapper.register();
		}
		if (audioFocusWrapper == null) {
			audioFocusWrapper = AudioFocusWrapper.getInstance();
			audioFocusWrapper.init(service, audioManager);
		}
		MODE_SIP_IN_CALL = service.prefsWrapper.getInCallMode();
		
		if (android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M11") 
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M13") 
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M190")
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("GT-I9000")
				)
			needSoundFix = true;
		else
			needSoundFix = false;

		service.registerReceiver(mHeadsetPlugReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
		// 네이티브 전화 수신 처리 리시버
		service.registerReceiver(mNativePhoneReceiver, new IntentFilter("android.intent.action.PHONE_STATE"));
		// 링거 모드 변경 수신 처리 리시버
		service.registerReceiver(mRingerModeReceiver, new IntentFilter(AudioManager.RINGER_MODE_CHANGED_ACTION));
		// InCall 노티피케이션 수신 처리 리시버
		IntentFilter filter = new IntentFilter("in_call_notification_decline");
		service.registerReceiver(mNotificationActionReceiver, filter);
		// Screen on/off 수신 처리 리시버
		IntentFilter screenFilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
		screenFilter.addAction(Intent.ACTION_SCREEN_ON);
		service.registerReceiver(mScreenChangeReceiver, screenFilter);
	}

	public void stopService() {
		Log.i(THIS_FILE, "Remove media manager....");
		DebugLog.d("stop service called --> remove media manager");

		try{
			if (bluetoothWrapper != null) {
				bluetoothWrapper.unregister();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try{
			if(mHeadsetPlugReceiver != null) {
				service.unregisterReceiver(mHeadsetPlugReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if(mNativePhoneReceiver != null) {
				service.unregisterReceiver(mNativePhoneReceiver);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if(mRingerModeReceiver != null) {
				service.unregisterReceiver(mRingerModeReceiver);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if(mNotificationActionReceiver != null) {
				service.unregisterReceiver(mNotificationActionReceiver);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if(mScreenChangeReceiver != null) {
				service.unregisterReceiver(mScreenChangeReceiver);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		destroy();
	}
	
	
	private BroadcastReceiver mHeadsetPlugReceiver = new BroadcastsHandler();
	public class BroadcastsHandler extends BroadcastReceiver {
		@Override public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equalsIgnoreCase(Intent.ACTION_HEADSET_PLUG)) {

//				String data = intent.getDataString();        
//				Bundle extraData = intent.getExtras();
				String nm = intent.getStringExtra("name");
				int st = intent.getIntExtra("state", 0);        
				int mic = intent.getIntExtra("microphone", 0);        
				String all = String.format(Locale.getDefault(), "st=%d, nm=%s, mic=%d", st, nm != null?nm:"", mic);
//				Log.d(THIS_FILE, "HEADSET_PLUG : (" + all + ")");
				DebugLog.d("HEADSET_PLUG --> "+all);
				
				if (needSoundFix && isSetAudioMode) {
					if (!userWantSpeaker) {
						MyAudioRecord.routingchanged = true;
					}
				}
			}
		}
	}

	public int getTargetMode() {
		return getAudioTargetMode();
	}

	private int getAudioTargetMode() {
		DebugLog.d("getAudioTargetMode() called");
		int targetMode = MODE_SIP_IN_CALL;
		
		if (needSoundFix) {
			if(audioManager.isWiredHeadsetOn() == true || userWantSpeaker == true) {
				DebugLog.d("return AudioManager.MODE_NORMAL");
				return AudioManager.MODE_NORMAL;

			} else {
				DebugLog.d("return AudioManager.MODE_IN_CALL");
				return AudioManager.MODE_IN_CALL;
			}
		}		
		
		if (service.prefsWrapper.getUseModeApi() /*|| android.os.Build.DEVICE.toUpperCase().startsWith("GT-I9000")*/) {
			Log.d(THIS_FILE, "User want speaker now..." + userWantSpeaker);
			DebugLog.d("User want speaker now..."+userWantSpeaker);

			if (!service.prefsWrapper.generateForSetCall()) {
				return userWantSpeaker ? AudioManager.MODE_NORMAL : AudioManager.MODE_IN_CALL;

			} else {
				return userWantSpeaker ? AudioManager.MODE_IN_CALL : AudioManager.MODE_NORMAL;
			}
		}

		DebugLog.d("targetMode --> "+targetMode);
		return targetMode;
	}

	public void setAudioInCall(boolean beforeInit) {
		DebugLog.d("setAudioInCall() called --> "+beforeInit);

	    if(!beforeInit || (beforeInit && startBeforeInit) ) {
	    	DebugLog.d("setAudioInCall() called --> navigate actualSetAudioInCall()");
	        actualSetAudioInCall();
	    }
	}

	public void unsetAudioInCall() {
		DebugLog.d("unsetAudioInCall() called --> navigate actualUnsetAudioInCall()");
		actualUnsetAudioInCall();
	}
	
	private boolean needSetMode(int mode) {
		int old = audioManager.getMode();
		return (old != mode);
	}
	
	/**
	 * Set the audio mode as in call
	 */
	private void actualSetAudioInCall() {
		Log.d(THIS_FILE, "actualSetAudioInCall : " + isSetAudioMode);
		DebugLog.d("actualSetAudioInCall() called setAudioMode is --> "+isSetAudioMode);
		
		// Ensure not already set
		if (isSetAudioMode) {
			return; 
		}

		DebugLog.d("actualSetAudioIncall ringer is Ringing --> "+ringer.isRinging());
		if(ringer!=null && ringer.isRinging()) {
			stopRing();
		}

//		stopRing();
		saveAudioState();

		// Set the rest of the phone in a better state to not interferate with
		// current call
		// audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,
		// AudioManager.VIBRATE_SETTING_ON);
		// audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION,
		// AudioManager.VIBRATE_SETTING_OFF);
		// audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

		// LOCKS
		// Wifi management if necessary
//		ContentResolver ctntResolver = service.getContentResolver();
//		Settings.System.putInt(ctntResolver, Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_NEVER);

		// Acquire wifi lock
		/* BJH 2017.03.08
		Error:Error: The WIFI_SERVICE must be looked up on the Application context or memory will leak on devices < Android N. Try changing to .getApplicationContext() [WifiManagerLeak]
		*/
		WifiManager wman = (WifiManager) service.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

		if (wifiLock == null) {
			wifiLock = wman.createWifiLock((Compatibility.isCompatible(9)) ? 3 : WifiManager.WIFI_MODE_FULL, "com.dial070.sip.InCallLock");
			wifiLock.setReferenceCounted(false);
		}
		WifiInfo winfo = wman.getConnectionInfo();

		if (winfo != null) {
			DetailedState dstate = WifiInfo.getDetailedStateOf(winfo.getSupplicantState());
			// We assume that if obtaining ip addr, we are almost connected so
			// can keep wifi lock
			if (dstate == DetailedState.OBTAINING_IPADDR || dstate == DetailedState.CONNECTED)
			{
				if (!wifiLock.isHeld() && !ServicePrefs.DIAL_TEST_SKIP_LOCK)
				{
					Log.d(THIS_FILE, "actualSetAudioInCall : ACQUIRE WIFI");
					wifiLock.acquire();
				}
			}

			// This wake lock purpose is to prevent PSP wifi mode
			if (service.prefsWrapper.keepAwakeInCall()) {
				if (screenLock == null) {
					PowerManager pm = (PowerManager) service.getSystemService(Context.POWER_SERVICE);
					screenLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "maaltalk:com.dial070.sip.onIncomingCall");
					screenLock.setReferenceCounted(false);
				}
				// Ensure single lock
				if (!screenLock.isHeld() && !ServicePrefs.DIAL_TEST_SKIP_LOCK) {
					screenLock.acquire();
				}

			}
		}
		
		// Audio routing
		int targetMode = getAudioTargetMode();
		Log.d(THIS_FILE, "Set mode audio in call to " + targetMode);
		DebugLog.d("Set mode audio in call to "+targetMode);

		if (service.prefsWrapper.generateForSetCall()) {
			ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 1);
			toneGenerator.startTone(ToneGenerator.TONE_CDMA_CONFIRM);
			toneGenerator.stopTone();
			toneGenerator.release();
		}

		/*
		if (!needSoundFix){
			// Set mode
			if (targetMode != AudioManager.MODE_IN_CALL) {
				setMode(AudioManager.MODE_IN_CALL);
			}
		}*/
		
		int apiLevel = Compatibility.getApiLevel();
		//SetAudioMode
		// ***IMPORTANT*** When the API level for honeycomb (H) has been
        // decided,
        // the condition should be changed to include API level 8 to H-1.
        if ( android.os.Build.BRAND.equalsIgnoreCase("Samsung") && (apiLevel >= 8) && (apiLevel < 11)) {
		//if ( android.os.Build.BRAND.equalsIgnoreCase("Samsung") && (apiLevel >= 8))
            int mode = 4;
            audioManager.setMode(mode);
            if (audioManager.getMode() != mode) {
            	mode = 3;
            	audioManager.setMode(mode);
                if (audioManager.getMode() != mode) {
                	audioManager.setMode(targetMode);
//                    Log.e(THIS_FILE, "Could not set /audio mode for Samsung device");
					DebugLog.d("Could not set /audio mode for Samsung device.");
                }
            }            
        }
        else if (apiLevel >= 11) {
            int mode = 3;
            audioManager.setMode(mode);
            if (audioManager.getMode() != mode) {
            	audioManager.setMode(targetMode);
                Log.e(THIS_FILE, "Could not set audio mode for mVoip");
				DebugLog.d("Could not set audio mode for mVoip.");
            }
        }
        else {
        	audioManager.setMode(targetMode);
        }
        
        // default speaker off
        userWantSpeaker = false;
//        if (userWantSpeaker)
        {
			// Routing
//			if (service.prefsWrapper.getUseRoutingApi())
//			{
//				audioManager.setRouting(targetMode, userWantSpeaker ? AudioManager.ROUTE_SPEAKER : AudioManager.ROUTE_EARPIECE, AudioManager.ROUTE_ALL);
//			}
//			else
			{
				audioManager.setSpeakerphoneOn(userWantSpeaker);
			}
        }

		audioManager.setMicrophoneMute(false);
		DebugLog.d("actualSetAudioInCall userWantBluetooth --> "+userWantBluetooth);
		DebugLog.d("actualSetAudioInCall canBluetooth --> "+bluetoothWrapper.canBluetooth());
		DebugLog.d("actualSetAudioInCall inBTHeadsetConnected --> "+bluetoothWrapper.isBTHeadsetConnected());


		if (bluetoothWrapper != null && userWantBluetooth && bluetoothWrapper.canBluetooth()) {
			Log.d(THIS_FILE, "Try to enable bluetooth");
			DebugLog.d("Try to enable bluetooth --> setBluetoothOn(true)");
			bluetoothWrapper.setBluetoothOn(true);
		}

		// Set stream solo/volume/focus
		int inCallStream = Compatibility.getInCallStream();
		if (!accessibilityManager.isEnabled()) {
			audioManager.setStreamSolo(inCallStream, true);
		}

		DebugLog.d("Request audioFocus --> audioFocusWrapper.focus() called.");
		audioFocusWrapper.focus();
		
		//savedVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);		
		//int vol = (int) (audioManager.getStreamMaxVolume(inCallStream) * service.prefsWrapper.getInitialVolumeLevel());

		// G Pro에서.
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("LG-F24")) {
			// SET MAX VOL
			int vol = audioManager.getStreamMaxVolume(inCallStream);
			setStreamVolume(inCallStream, vol, 0);
		}
		
		isSetAudioMode = true;
		// System.gc();		
	}

	/**
	 * Save current audio mode in order to be able to restore it once done
	 */
	private void saveAudioState() {
//		Log.d(THIS_FILE, "saveAudioState : " + isSavedAudioState);
		DebugLog.d("saveAudioState --> "+isSavedAudioState);
		
		if (isSavedAudioState) { return; }
		
//		ContentResolver ctntResolver = service.getContentResolver();
//		savedVibrateRing = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER);
//		savedVibradeNotif = audioManager.getVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION);
//		savedRingerMode = audioManager.getRingerMode();
//		savedWifiPolicy = android.provider.Settings.System.getInt(ctntResolver, android.provider.Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_DEFAULT);
		
		
//		savedVolume = audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		savedMode = audioManager.getMode();
		savedRoute = audioManager.getRouting(getAudioTargetMode());

		isSavedAudioState = true;

	}

	/**
	 * Reset the audio mode
	 */
	private void actualUnsetAudioInCall() {
		Log.d(THIS_FILE, "actualUnsetAudioInCall : " + isSetAudioMode);
		DebugLog.d("actualUnsetAudioInCall --> "+isSetAudioMode);
		
		if (!isSetAudioMode) { return; }
		

//		ContentResolver ctntResolver = service.getContentResolver();
//		Settings.System.putInt(ctntResolver, Settings.System.WIFI_SLEEP_POLICY, savedWifiPolicy);
//		audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER, savedVibrateRing);
//		audioManager.setVibrateSetting(AudioManager.VIBRATE_TYPE_NOTIFICATION, savedVibradeNotif);
//		audioManager.setRingerMode(savedRingerMode);

//		int targetMode = getAudioTargetMode();
//		if (service.prefsWrapper.getUseRoutingApi())
//		{
//			audioManager.setRouting(targetMode, savedRoute, AudioManager.ROUTE_ALL);
//		}
//		else
		{
			audioManager.setSpeakerphoneOn(false);
		}

		if (bluetoothWrapper != null) {
			// This fixes the BT activation but... but... seems to introduce a
			// lot of other issues
			// bluetoothWrapper.setBluetoothOn(true);
//			Log.d(THIS_FILE, "Unset bt");
			DebugLog.d("unset bluetooth --> bluetoothWrapper.setBluetoothOn(false) called.");
			bluetoothWrapper.setBluetoothOn(false);
		}

		audioManager.setMicrophoneMute(false);

		int inCallStream = Compatibility.getInCallStream();
		//setStreamVolume(inCallStream, savedVolume, 0);
		audioManager.setStreamSolo(inCallStream, false);

		DebugLog.d("Reset AudioManger Mode --> "+savedMode);
//		audioManager.setMode(savedMode);
		audioManager.setMode(AudioManager.MODE_NORMAL);
		
		if (wifiLock != null && wifiLock.isHeld()) {
			Log.d(THIS_FILE, "actualUnsetAudioInCall : ACQUIRE WIFI");
			DebugLog.d("ACQUIRED WIFI --> release() called");
			wifiLock.release();
		}
		if (screenLock != null && screenLock.isHeld())
		{
			Log.d(THIS_FILE, "Release screen lock");
			DebugLog.d("ACQUIRED SCREEN LOCK --> release() called");
			screenLock.release();
		}

		DebugLog.d("Request audioFocus --> audioFocusWrapper.unFocus() called");
		audioFocusWrapper.unFocus();

		isSavedAudioState = false;
		isSetAudioMode = false;

	}

	public boolean isRinging() {
		if(ringer!=null) {
			return ringer.isRinging();
		} else {
			return false;
		}

	}
	
	public void startRing(String remoteContact, String method) {
		DebugLog.d("ring --> savedAudioState() | audioFocusWrapper.focus() called.");
		DebugLog.d("startRing in MediaManager caller method --> ");
		DebugLog.d("startRing caller method --> "+method);
		this.remoteContact = remoteContact;

		if(ringer.isRinging()) {
			return;
		}

		saveAudioState();

		DebugLog.d("has Audio Focus --> "+audioFocusWrapper.hasFocus());
		DebugLog.d("has BTH connected --> "+bluetoothWrapper.isBTHeadsetConnected());
		DebugLog.d("has Ringtone --> "+service.getPrefs().getRingtone());

		if(!ringer.isRinging()) {
			DebugLog.d("ring()");

			if(bluetoothWrapper.isBTHeadsetConnected()) {
				bluetoothWrapper.setBluetoothOn(true);
			}

			if(!audioFocusWrapper.hasFocus()) {
				audioFocusWrapper.focus();
			}

			ringer.ring(remoteContact, service.getPrefs().getRingtone(), bluetoothWrapper.isBTHeadsetConnected());

		} else {
			DebugLog.d("Already Ringing...");
		}

	}

	public void updateRing(String remoteContact, String method) {
		DebugLog.d("updateRing() called");
		DebugLog.d("updateRing() bluetooth connected --> "+bluetoothWrapper.isBTHeadsetConnected());
		DebugLog.d("ring --> savedAudioState() | audioFocusWrapper.focus() called.");
		DebugLog.d("updateRing caller method --> "+method);

		this.remoteContact = remoteContact;

		DebugLog.d("has update Audio Focus --> "+audioFocusWrapper.hasFocus());
		DebugLog.d("has update BTH connected --> "+bluetoothWrapper.isBTHeadsetConnected());

		if(!audioFocusWrapper.hasFocus()) {
			audioFocusWrapper.focus();
		}

		if(bluetoothWrapper.isBTHeadsetConnected()) {
			bluetoothWrapper.setBluetoothOn(true);
		}
		ringer.ring(remoteContact, service.getPrefs().getRingtone(), bluetoothWrapper.isBTHeadsetConnected());
	}

	public void stopRing() {
		DebugLog.d("stopRing ring --> "+ringer.isRinging());

		if(ringer.isRinging()) {
			DebugLog.d("stopRing()");
			ringer.stopRing();
		}

		bluetoothWrapper.setBluetoothOn(false);
		audioManager.setMode(AudioManager.MODE_NORMAL);

		audioFocusWrapper.unFocus();
	}

	/**
	 * 발신시에만 처리 포커스를 따로 처리할 필요가 없음
	 * @param method
	 */
	public void stopOnlyRing(String method) {
		DebugLog.d("only ring --> "+ringer.isRinging()+" : "+method);

		if(ringer.isRinging()) {
			ringer.stopRing();
		}

	}

	public void stopAnnouncing() {
		DebugLog.d("stopAnnouncing --> stopRing() | setMode(savedMode) | unFocus");
		stopRing();
		//BJH 2017.11.29 갤럭시 이슈
		int inCallStream = Compatibility.getInCallStream();
		audioManager.setStreamSolo(inCallStream, false);
		bluetoothWrapper.setBluetoothOn(false);
		audioFocusWrapper.unFocus();
		audioManager.setMode(AudioManager.MODE_NORMAL);

//		/**
//		 * 2020.08.19
//		 * 블루투스 헤드셋이 연결중일 경우 Ring Stop 처리시 블루투스 소스 연결 종료 기능
//		 */
//		if(bluetoothWrapper.isBTHeadsetConnected()) {
////			setBluetoothOn(false);
//			setBluetoothOn(false);
//			bluetoothWrapper.setBluetoothOn(false);
////			audioManager.setBluetoothScoOn(false);
////			audioManager.stopBluetoothSco();
//		}
//
//		audioFocusWrapper.unFocus();
	}

	public void resetSettings() {
		DebugLog.d("resetSettings called.");
		userWantBluetooth = true;
		userWantMicrophoneMute = false;
		userWantSpeaker = false;
	}

	public void toggleMute() {
		DebugLog.d("toggleMute current --> "+userWantMicrophoneMute);
		setMicrophoneMute(!userWantMicrophoneMute);
	}

	public synchronized void setMicrophoneMute(boolean on) {
		DebugLog.d("setMicrophoneMute --> "+on);
		if (on != userWantMicrophoneMute) {
			float level = on ? 0 : service.prefsWrapper.getMicLevel();
			pjsua.conf_adjust_rx_level(0, level);
			Log.d(THIS_FILE, "conf_adjust_rx_level : " + level);
			userWantMicrophoneMute = on;
			broadcastMediaChanged();
		}
	}

	public synchronized void setSpeakerphoneOn(boolean on) {
		DebugLog.d("setSpeakerPhone on --> "+on);
		DebugLog.d("setSpeakerPhone userWantSpeaker --> "+userWantSpeaker);

		if (needSoundFix) {
			if (on != userWantSpeaker) {
				userWantSpeaker = on;
				
				int targetMode = getAudioTargetMode();
				if (needSetMode(targetMode)) {
					MyAudioRecord.routingchanged = true;
				}
				else {
					audioManager.setSpeakerphoneOn(userWantSpeaker);
				}
				broadcastMediaChanged();
			}
		}
		else {
			/*
			pjsua.set_no_snd_dev();
			userWantSpeaker = on;
			pjsua.set_snd_dev(0, 0);
			*/

			if(on==true) {
				if(bluetoothWrapper.isBTHeadsetConnected()) {
					DebugLog.d("setSpeakerPhoneOn setBluetooth --> false call");
					bluetoothWrapper.setBluetoothOn(false);
				}
				audioManager.setSpeakerphoneOn(true);

			} else {
				audioManager.setSpeakerphoneOn(false);
				if(bluetoothWrapper.isBTHeadsetConnected()) {
					DebugLog.d("setSpeakerPhoneOn setBluetooth --> true call");
					bluetoothWrapper.setBluetoothOn(true);
				}
			}

			userWantSpeaker = on;
//			audioManager.setSpeakerphoneOn(userWantSpeaker);
			broadcastMediaChanged();
		}
	}

	public synchronized void setBluetoothOn(boolean on) {
//		Log.d(THIS_FILE, "Set BT " + on);
		DebugLog.d("setBluetoothOn on--> "+on);
		//pjsua.set_no_snd_dev();
		userWantBluetooth = on;
		//pjsua.set_snd_dev(0, 0);
		broadcastMediaChanged();
	}

	public class MediaState {

		public boolean isMicrophoneMute = false;
		public boolean isSpeakerphoneOn = false;
		public boolean isBluetoothScoOn = false;
		public boolean canMicrophoneMute = true;
		public boolean canSpeakerphoneOn = true;
		public boolean canBluetoothSco = false;

		@Override
		public boolean equals(Object o)
		{

			if (o != null && o.getClass() == MediaState.class)
			{
				MediaState oState = (MediaState) o;
				if (oState.isBluetoothScoOn == isBluetoothScoOn && oState.isMicrophoneMute == isMicrophoneMute && oState.isSpeakerphoneOn == isSpeakerphoneOn && oState.canBluetoothSco == canBluetoothSco && oState.canSpeakerphoneOn == canSpeakerphoneOn && oState.canMicrophoneMute == canMicrophoneMute)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			return super.equals(o);
		}
	}

	public MediaState getMediaState()
	{
		MediaState mediaState = new MediaState();

		// Micro
		mediaState.isMicrophoneMute = userWantMicrophoneMute;
		mediaState.canMicrophoneMute = true; /* && !mediaState.isBluetoothScoOn */// Compatibility.isCompatible(5);

		// Speaker
		mediaState.isSpeakerphoneOn = userWantSpeaker;
		mediaState.canSpeakerphoneOn = true && !mediaState.isBluetoothScoOn; // Compatibility.isCompatible(5);

		// Bluetooth

		if (bluetoothWrapper != null)
		{
			mediaState.isBluetoothScoOn = bluetoothWrapper.isBluetoothOn();
			mediaState.canBluetoothSco = bluetoothWrapper.canBluetooth();
		}
		else
		{
			mediaState.isBluetoothScoOn = false;
			mediaState.canBluetoothSco = false;
		}

		return mediaState;
	}

	public void broadcastMediaChanged() {
		service.sendBroadcast(mediaStateChangedIntent);
	}

	private static final String ACTION_AUDIO_VOLUME_UPDATE = "org.openintents.audio.action_volume_update";
	private static final String EXTRA_STREAM_TYPE = "org.openintents.audio.extra_stream_type";
	private static final String EXTRA_VOLUME_INDEX = "org.openintents.audio.extra_volume_index";
	private static final String EXTRA_RINGER_MODE = "org.openintents.audio.extra_ringer_mode";
	private static final int EXTRA_VALUE_UNKNOWN = -9999;

	private void broadcastVolumeWillBeUpdated(int streamType, int index) {
		DebugLog.d("boradcastVolumeWillBeUpdated...stremType --> "+streamType);

		Intent notificationIntent = new Intent(ACTION_AUDIO_VOLUME_UPDATE);
		notificationIntent.putExtra(EXTRA_STREAM_TYPE, streamType);
		notificationIntent.putExtra(EXTRA_VOLUME_INDEX, index);
		notificationIntent.putExtra(EXTRA_RINGER_MODE, EXTRA_VALUE_UNKNOWN);

		service.sendBroadcast(notificationIntent, null);
	}

	public void setStreamVolume(int streamType, int index, int flags) {
		broadcastVolumeWillBeUpdated(streamType, index);
		audioManager.setStreamVolume(streamType, index, flags);
		Log.d(THIS_FILE, "setStreamVolume : " + streamType + ", " + index);
	}

	public void adjustStreamVolume(int streamType, int direction, int flags) {
		broadcastVolumeWillBeUpdated(streamType, EXTRA_VALUE_UNKNOWN);

		DebugLog.d("adjustStreamVolume current volume      --> "+audioManager.getStreamVolume(AudioManager.STREAM_RING));
		DebugLog.d("adjustStreamVolume current direction   --> "+direction);
		DebugLog.d("adjustStreamVolume current mode        --> "+audioManager.getMode());
		DebugLog.d("adjustStreamVolume current ringer mode --> "+audioManager.getRingerMode());

		if(bluetoothWrapper.isBTHeadsetConnected()) {
			if(audioManager.getStreamVolume(AudioManager.STREAM_RING)==1 && direction==-1) {
				ringer.pauseRing();

			} else if(audioManager.getStreamVolume(AudioManager.STREAM_RING)==0 && direction==1) {
				ringer.resumeRing();
			}
		}

		audioManager.adjustStreamVolume(streamType, direction, flags);

	}

	// Public accessor
	public boolean isUserWantMicrophoneMute() {
		Log.d(THIS_FILE, "isUserWantMicrophoneMute : " + userWantMicrophoneMute);
		return userWantMicrophoneMute;
	}
	
    @Override
    public void onBluetoothStateChanged(int status) {
		DebugLog.d("onBluetoothStateChanged... status --> "+status);

        setSoftwareVolume();
        broadcastMediaChanged();
    }

	@Override
	public void onBluetoothConnectionChanged(int status) {
		DebugLog.d("onBluetoothConnectionChanged status --> "+status);
		DebugLog.d("onBluetoothConnectionChanged current call state --> "+ CallStatus.INSTANCE.getCurrentStatus());
		DebugLog.d("onBluetoothConnectionChanged has focus --> "+audioFocusWrapper.hasFocus());

		switch (status) {
			case BLUETOOTH_DEVICE_CONNECTED:	// connected
				DebugLog.d("BT action HEADSET_STATE_CHANGE state --> connected");
				this.userWantBluetooth = true;

				if(CallStatus.INSTANCE.getCurrentStatus()== SipCallSession.InvState.EARLY || CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.INCOMING) {
					DebugLog.d("BT action HEAD_SET_IS_CONNECTED and SipSession EARLY and audio mode --> "+audioManager.getMode());
					bluetoothWrapper.setBluetoothOn(true);
					if(CallStatus.INSTANCE.getCallAction()==0) {
						return;
					}
					ringer.ring(remoteContact, service.getPrefs().getRingtone(), true);

				} else if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.CONFIRMED) {
					bluetoothWrapper.setBluetoothOn(true);

				} else if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.DISCONNECTED) {
					if(audioFocusWrapper.hasFocus()) {
						audioFocusWrapper.unFocus();
					}
					setBluetoothOn(false);
				}
				break;
			case BLUETOOTH_DEVICE_CONNECTING: // connecting
				DebugLog.d("BT action HEADSET_STATE_CHANGE state --> connecting");
				break;
			case BLUETOOTH_DEVICE_DISCONNECTED: // disconnect
				DebugLog.d("BT action HEADSET_STATE_CHANGE state --> disconnected");
				setBluetoothOn(false);
				if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY || CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.INCOMING) {
					if(CallStatus.INSTANCE.getCallAction()==0) {
						return;
					}
					ringer.ring(remoteContact, service.getPrefs().getRingtone(), false);
				}
				bluetoothWrapper.setBluetoothOn(false);
				if(audioFocusWrapper.hasFocus()) {
					audioFocusWrapper.unFocus();
				}

				if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.DISCONNECTED) {
					audioManager.setMode(AudioManager.MODE_NORMAL);
				}
				break;
		}
	}

	public void setSoftwareVolume(){
		
		if(service != null) {
			/*
			final boolean useBT = (bluetoothWrapper != null && bluetoothWrapper.isBluetoothOn());						
			String speaker_key = useBT ? SipConfigManager.SND_BT_SPEAKER_LEVEL : SipConfigManager.SND_SPEAKER_LEVEL;
			String mic_key = useBT ? SipConfigManager.SND_BT_MIC_LEVEL : SipConfigManager.SND_MIC_LEVEL;
			
			final float speakVolume = service.getPrefs().getPreferenceFloatValue(speaker_key);
			final float micVolume = userWantMicrophoneMute? 0 : service.getPrefs().getPreferenceFloatValue(mic_key);
			
			service.getExecutor().execute(new SipRunnable() {
				
				@Override
				protected void doRun() throws SameThreadException {
					service.confAdjustTxLevel(speakVolume);
					service.confAdjustRxLevel(micVolume);
					
					// Force the BT mode to normal
					if(useBT) {
						audioManager.setMode(AudioManager.MODE_NORMAL);
					}
				}
			});
			*/
		}
	}

	private class RingerModeChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			DebugLog.d("audio ringer mode changed --> "+audioManager.getRingerMode());
			DebugLog.d("audio current mode --> "+audioManager.getMode());
			if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY || CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.INCOMING) {
				DebugLog.d("audio ringer mode changed in condition --> startRing()");
				DebugLog.d("audio ringer mode change at call action is --> "+CallStatus.INSTANCE.getCallAction());
				if(CallStatus.INSTANCE.getCallAction()==0) {
					return;
				}
				if(RingerStatus.INSTANCE.isMediaRingerIsRinging()) {
					updateRing(remoteContact, "ringerModeChangeReceiver_onReceive");
				}
//				updateRing(remoteContact, "ringerModeChangeReceiver_onReceive");
			}
		}
	}

	private class NotificationActionReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if(action.equals("in_call_notification_decline")) {
				DebugLog.d("notification action received --> in_call_notification_decline at mediaManager");

			} else if(action.equals("in_call_notification_received_bye")) {
				DebugLog.d("notification action received --> incall_notification_received_bye");
				stopRing();

			} else if(action.equals("in_call_notification_confirmed")) {
				DebugLog.d("notification action received --> in_call_notification_confirmed");
				stopOnlyRing("action in_call_notification_confirmed");
			}
		}
	}

	private class ScreenChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			boolean screenOn = intent.getBooleanExtra("screen_state", false);

			if(intent.getAction().equals("android.intent.action.SCREEN_ON")) {
				screenOn = true;
			} else if(intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
				screenOn = false;
			}

			DebugLog.d("screen_state --> "+screenOn);
			int currentRingerMode = audioManager.getRingerMode();
			int currentCallStatus = CallStatus.INSTANCE.getCurrentStatus();
			int currentNativeCallStatus = CallStatus.INSTANCE.getNativeStatus();

			DebugLog.d("SCREEN condition check current screen state  --> "+screenOn);
			DebugLog.d("SCREEN condition check current intent action --> "+intent.getAction());
			DebugLog.d("SCREEN condition check current Ringer mode   --> "+currentRingerMode);
			DebugLog.d("SCREEN condition check current Call status   --> "+currentCallStatus);
			DebugLog.d("SCREEN condition check current Native status --> "+currentNativeCallStatus);

			if(screenOn) {
				if(currentCallStatus==SipCallSession.InvState.INCOMING || currentCallStatus==SipCallSession.InvState.EARLY) {
//					DebugLog.d("SCREEN condition check current ringer is ringing --> "+ringer.isRinging());
//					DebugLog.d("SCREEN condition check current ringerState --> "+ RingerStatus.INSTANCE.isMediaRingerIsRinging());

					if(!ringer.isRinging() && !RingerStatus.INSTANCE.isMediaRingerIsRinging()) {
//						ringer.ring(remoteContact, service.getPrefs().getRingtone(), bluetoothWrapper.isBTHeadsetConnected());
					}
				}
			} else {
				if(currentCallStatus==SipCallSession.InvState.EARLY || currentCallStatus==SipCallSession.InvState.INCOMING
						&& currentNativeCallStatus== TelephonyManager.CALL_STATE_IDLE
						&& currentRingerMode == AudioManager.RINGER_MODE_VIBRATE) {

					DebugLog.d("condition check --> true");
					if(!audioFocusWrapper.hasFocus()) {
						audioFocusWrapper.focus();
					}

				} else {
					DebugLog.d("condition check --> false");
				}
			}

		}
	}

	private class PhoneStateChangeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			MaalTalkPhoneStateListener phoneStateListener = new MaalTalkPhoneStateListener();
			telephonyManager.listen(phoneStateListener, phoneStateListener.LISTEN_CALL_STATE);
		}
	}

	private class MaalTalkPhoneStateListener extends PhoneStateListener {
		@Override
		public void onCallStateChanged(int state, String phoneNumber) {
			DebugLog.d("onCallStateChanged state --> "+state+" , incomingNumber : "+phoneNumber);
			DebugLog.d("MaalTalk Call state --> "+CallStatus.INSTANCE.getCurrentStatus());

			switch (state) {
				case TelephonyManager.CALL_STATE_IDLE: //0
					if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY) {
					}
					break;
				case TelephonyManager.CALL_STATE_RINGING: //1
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK: //2
					if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY) {
						pjService.callHangup(-1, 0);

					} else if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.CONFIRMED) {
						SipCallSession session = pjService.getActiveCallInProgress();
						try{
							if(session==null) {
								int callId = session.getCallId();
								pjService.callHangup(-1, 0);

							} else {
								int callId = session.getCallId();
								pjService.callHangup(callId, 0);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					break;
			}
			CallStatus.INSTANCE.setNativeStatus(state);

			DebugLog.d("phoneState : "+state+" current MaalTalk state --> "+CallStatus.INSTANCE.getCurrentStatus());
			DebugLog.d("phoneState : "+state+" current Audio Focus --> "+audioFocusWrapper.hasFocus());
			DebugLog.d("phoneState : "+state+" current audio mode --> "+audioManager.getMode());
			DebugLog.d("phoneState : "+state+" current bt is on --> "+bluetoothWrapper.isBluetoothOn());
			DebugLog.d("phoneState : "+state+" current bt dv connected --> "+bluetoothWrapper.isBTHeadsetConnected());
		}
	}

	public AudioManager getAudioManager() {
		return audioManager;
	}
}
