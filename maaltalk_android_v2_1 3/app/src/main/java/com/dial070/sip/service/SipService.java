/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.service;

//import java.net.InetAddress;
//import java.net.NetworkInterface;
//import java.net.SocketException;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyCallback;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.service.Fcm;
import com.dial070.sip.api.ISipConfiguration;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfile;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.api.SipUri.ParsedSipContactInfos;
import com.dial070.sip.db.DBAdapter;
import com.dial070.sip.models.SipMessage;
import com.dial070.sip.pjsip.PjSipService;
import com.dial070.sip.pjsip.UAStateReceiver;
import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.sip.utils.Threading;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.view.Te;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.pjsip.pjsua.pjsuaConstants;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

//import java.util.Enumeration;
//import java.util.Locale;
//import com.dial070.sip.ui.InCallMediaControl;

public class SipService extends Service {

	public static final String INTENT_SIP_CONFIGURATION = "com.dial070.sip.service.SipConfiguration";
	public static final String INTENT_SIP_SERVICE = "com.dial070.sip.service.SipService";
	public static final String INTENT_SIP_ACCOUNT_ACTIVATE = "com.dial070.sip.accounts.activate";

	// static boolean creating = false;
	private static final String THIS_FILE = "Dial070-SIPSERVICE";
	public static SipService currentService = null;
	public static int RTT = 0;
	public static int LOSS = 0;

	private SipWakeLock sipWakeLock;
	private boolean autoAcceptCurrent = false;
	private Object mReconnectLock = null;
	private SipProfile account = null;
	private AppPrefs mPrefs = null;

	public int state;

	// added
	private TelephonyCallback callStateCallback = null;
	private TelephonyCallback dataConnectionStateCallback = null;

	// Implement public interface for the service
	private final ISipService.Stub binder = new ISipService.Stub() {
		/**
		 * Start the sip stack according to current settings (create the stack)
		 */
		@Override
		public void sipStart() throws RemoteException {
			DebugLog.d("sip started");

			startSipStack();
		}

		/**
		 * Stop the sip stack (destroy the stack)
		 */
		@Override
		public void sipStop() throws RemoteException {
			DebugLog.d("sip Stopped");

			pjService.sipStop();
		}

		/**
		 * Force the stop of the service
		 */
		@Override
		public void forceStopService() throws RemoteException
		{
//			Log.d(THIS_FILE, "Try to force service stop");
			DebugLog.d("Try to force service stop");
			stopSelf();
		}

		/**
		 * Restart the service (threaded)
		 */
		@Override
		public void askThreadedRestart() throws RemoteException
		{
			Thread t = new Thread()
			{
				public void run()
				{
					SipService.this.restart();			
				}
			};
			if (t != null)
				t.start();
		};
		
		@Override
		public void reconnect() {
			DebugLog.d("SipService reconnect called");
			SipService.this.reconnect();
		}

		/**
		 * Populate pjsip accounts with accounts saved in sqlite
		 */
		@Override
		public void addAllAccounts() throws RemoteException {
			SipService.this.connect();
		}

		/**
		 * Unregister and delete accounts registered
		 */
		@Override
		public void removeAllAccounts() throws RemoteException
		{
			SipService.this.disconnect();
		}

		@Override
		public void setAccountRegistration(int accountId, int renew) throws RemoteException
		{
			// SipProfile account;
			// synchronized (db)
			// {
			// db.open();
			// account = db.getAccount(accountId);
			// db.close();
			// }
			SipService.this.setAccountRegistration(account, renew);
		}

		/**
		 * Get account and it's informations
		 * 
		 * @param accountId
		 *            the id (sqlite id) of the account
		 */
		@Override
		public SipProfileState getSipProfileState(int accountId) throws RemoteException
		{
			return SipService.this.getSipProfileState(accountId);
		}

		/**
		 * Switch in autoanswer mode
		 */
		@Override
		public void switchToAutoAnswer() throws RemoteException
		{
			setAutoAnswerNext(true);
		}

		/**
		 * Make a call
		 * 
		 * @param callee
		 *            remote contact ot call If not well formated we try to add
		 *            domain name of the default account
		 */
		@Override
		public int makeCall(String callee, int accountId) throws RemoteException {
			/*if (callee.equals("07079137913") || callee.equals("091412672185") || callee.equals("07079132199")){

			}else if (pjService.getActiveCallInProgress() != null)
			{
				ToastHandler.sendMessage(ToastHandler.obtainMessage(0, "전화통화가 진행중입니다."));
				return pjsuaConstants.PJ_SUCCESS;
			}*/


			// 2017-12-05 : CHECK RTT & CODEC
			/*if (RTT >= 250)
			{
				String codec = "G729";
				Log.d(THIS_FILE, "MAKECALL : SET CODEC=" + codec + ", RTT=" + RTT);
				pjService.prefsWrapper.setMediaCodec(codec);
				pjService.setCodecsPriorities();
			}*/

			// We have to ensure service is properly started and not just binded
			//SipService.this.startService(new Intent(SipService.this, SipService.class));
			int status = pjService.makeCall(callee, accountId);
			if (status != pjsuaConstants.PJ_SUCCESS)
			{
				ToastHandler.sendMessage(ToastHandler.obtainMessage(0, getString(R.string.makecall_error) + "\n(에러코드 : " + status + ")"));
				return status;
			}
			return status;
		}

		/**
		 * Send SMS using
		 */
		@Override
		public void sendMessage(String message, String callee, int accountId) throws RemoteException {
			// We have to ensure service is properly started and not just binded
			//SipService.this.startService(new Intent(SipService.this, SipService.class));
			Log.d(THIS_FILE, "will sms " + callee);

			ToCall called = pjService.sendMessage(callee, message, accountId);
			if (called != null)
			{
				SipMessage msg = new SipMessage(SipMessage.SELF, SipUri.getCanonicalSipContact(callee), SipUri.getCanonicalSipContact(called.getCallee()), message, "text/plain", System.currentTimeMillis(), SipMessage.MESSAGE_TYPE_QUEUED);
				msg.setRead(true);
				synchronized (db)
				{
					db.open();
					db.insertMessage(msg);
					db.close();
				}
				Log.d(THIS_FILE, "Inserted " + msg.getTo());
			}
			else
			{
				SipService.this.notifyUserOfMessage(getString(R.string.invalid_sip_uri) + " : " + callee);
			}
		}

		/**
		 * Answer an incoming call
		 * 
		 * @param callId
		 *            the id of the call to answer to
		 * @param status
		 *            the status code to send
		 */
		@Override
		public int answer(int callId, int status) throws RemoteException
		{
			return pjService.callAnswer(callId, status);
		}

		/**
		 * Hangup a call
		 * 
		 * @param callId
		 *            the id of the call to hang up
		 * @param status
		 *            the status code to send
		 */
		@Override
		public int hangup(int callId, int status) throws RemoteException
		{
			Log.d(THIS_FILE, "hangup:"+callId);
			return pjService.callHangup(callId, status);
		}

		@Override
		public int xfer(int callId, String callee) throws RemoteException
		{
			Log.d(THIS_FILE, "XFER");
			return pjService.callXfer(callId, callee);
		}

		@Override
		public int xferReplace(int callId, int otherCallId, int options) throws RemoteException
		{
			Log.d(THIS_FILE, "XFER-replace");
			return pjService.callXferReplace(callId, otherCallId, options);
		}

		@Override
		public int sendDtmf(int callId, int keyCode) throws RemoteException
		{
			return pjService.sendDtmf(callId, keyCode);
		}
		
		@Override
		public int sendDtmf2(int callId, String keyCode) throws RemoteException
		{
			return pjService.sendDtmf2(callId, keyCode);
		}		

		@Override
		public int hold(int callId) throws RemoteException
		{
			Log.d(THIS_FILE, "HOLDING");
			return pjService.callHold(callId);
		}

		@Override
		public int reinvite(int callId, boolean unhold) throws RemoteException
		{
			Log.d(THIS_FILE, "REINVITING");
			return pjService.callReinvite(callId, unhold);
		}

		@Override
		public SipCallSession getCallInfo(int callId) throws RemoteException
		{
			return pjService.getCallInfo(callId);
		}

		@Override
		public void setBluetoothOn(boolean on) throws RemoteException
		{
			pjService.setBluetoothOn(on);

		}

		@Override
		public void setMicrophoneMute(boolean on) throws RemoteException
		{
			pjService.setMicrophoneMute(on);
		}

		@Override
		public void setSpeakerphoneOn(boolean on) throws RemoteException
		{
			pjService.setSpeakerphoneOn(on);
		}

		@Override
		public SipCallSession[] getCalls() throws RemoteException
		{
			return pjService.getCalls();
		}

		@Override
		public void confAdjustTxLevel(int port, float value) throws RemoteException
		{
			pjService.confAdjustTxLevel(port, value);
		}

		@Override
		public void confAdjustRxLevel(int port, float value) throws RemoteException
		{
			pjService.confAdjustRxLevel(port, value);

		}

		@Override
		public void adjustVolume(SipCallSession callInfo, int direction, int flags) throws RemoteException
		{

			int state = callInfo.getCallState();
			
			boolean ringing = ((state == SipCallSession.InvState.INCOMING) || (state == SipCallSession.InvState.EARLY));

			// Mode ringing
			if (ringing && callInfo.isIncoming())
			{
				pjService.adjustStreamVolume(AudioManager.STREAM_RING, direction, AudioManager.FLAG_SHOW_UI);
			}
			else
			{
				// Mode in call
//				if (prefsWrapper.getPreferenceBooleanValue(PreferencesWrapper.USE_SOFT_VOLUME))
//				{
//					// Intent adjustVolumeIntent = new Intent(SipService.this,
//					// InCallMediaControl.class);
//					// adjustVolumeIntent.putExtra(Intent.EXTRA_KEY_EVENT,
//					// direction);
//					// adjustVolumeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//					// );
//					// startActivity(adjustVolumeIntent);
//				}
//				else
				{
					pjService.adjustStreamVolume(Compatibility.getInCallStream(), direction, flags);
				}
			}
		}

		@Override
		public void setEchoCancellation(boolean on) throws RemoteException
		{
			pjService.setEchoCancellation(on);
		}

		@Override
		public void startRecording(int callId) throws RemoteException
		{
			pjService.startRecording(callId);
		}

		@Override
		public void stopRecording() throws RemoteException
		{
			pjService.stopRecording();
		}

		@Override
		public int getRecordedCall() throws RemoteException
		{
			return pjService.getRecordedCall();
		}

		@Override
		public boolean canRecord(int callId) throws RemoteException
		{
			return pjService.canRecord(callId);
		}
		
		public boolean detect_nat_type()
		{
			return pjService.detect_nat_type();		
		}
	};

	private final ISipConfiguration.Stub binderConfiguration = new ISipConfiguration.Stub()
	{

		@Override
		public long addOrUpdateAccount(SipProfile acc) throws RemoteException
		{
			Log.d(THIS_FILE, ">>> addOrUpdateAccount from service");
			// long finalId = SipProfile.INVALID_ID;
			long finalId = 1;
			// synchronized (db)
			// {
			// db.open();
			// if (acc.id == SipProfile.INVALID_ID)
			// {
			// finalId = db.insertAccount(acc);
			// }
			// else
			// {
			// db.updateAccount(acc);
			// finalId = acc.id;
			// }
			// db.close();
			// }
			return finalId;
		}

		@Override
		public SipProfile getAccount(long accId) throws RemoteException
		{
			SipProfile result = account;

			// synchronized (db)
			// {
			// db.open();
			// result = db.getAccount(accId);
			// db.close();
			// }
			return result;
		}

		@Override
		public void setPreferenceBoolean(String key, boolean value) throws RemoteException
		{
			prefsWrapper.setPreferenceBooleanValue(key, value);
		}

		@Override
		public void setPreferenceFloat(String key, float value) throws RemoteException
		{
			prefsWrapper.setPreferenceFloatValue(key, value);

		}

		@Override
		public void setPreferenceString(String key, String value) throws RemoteException
		{
			prefsWrapper.setPreferenceStringValue(key, value);

		}

		@Override
		public String getPreferenceString(String key) throws RemoteException
		{
			return prefsWrapper.getPreferenceStringValue(key);

		}

		@Override
		public boolean getPreferenceBoolean(String key) throws RemoteException
		{
			return prefsWrapper.getPreferenceBooleanValue(key);

		}

		@Override
		public float getPreferenceFloat(String key) throws RemoteException
		{
			return prefsWrapper.getPreferenceFloatValue(key);
		}

	};

	protected DBAdapter db;
	private WakeLock wakeLock;
	private WifiLock wifiLock;
	private ServiceDeviceStateReceiver deviceStateReceiver;
	public PreferencesWrapper prefsWrapper;
	private ServicePhoneStateReceiver phoneConnectivityReceiver;
	private TelephonyManager telephonyManager;
//	private ConnectivityManager connectivityManager;

	public SipNotifications notificationManager;
	private MyExecutor mExecutor;
	
	public static PjSipService pjService = null;
	private static HandlerThread executorThread = null;

	// Broadcast receiver for the service
	private class ServiceDeviceStateReceiver extends BroadcastReceiver
	{
		private Timer mTimer = new Timer();
		private MyTimerTask mTask;
		private Boolean firstEvent = true;

		@Override		
		public void onReceive(final Context context, final Intent intent)
		{
			if (firstEvent)
			{
				firstEvent = false;
				return;
			}
			
			// Run the handler in MyExecutor to be protected by wake lock
			getExecutor().execute(new Runnable()
			{
				public void run()
				{
					onReceiveInternal(context, intent);
				}
			});
		}

		public void stop()
		{
			mTimer.cancel();
			mTimer.purge();
		}

		private void onReceiveInternal(Context context, Intent intent)
		{
			String action = intent.getAction();
			if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION))
			{
				Bundle b = intent.getExtras();
				if (b != null)
				{
					NetworkInfo netInfo = (NetworkInfo) b.get(ConnectivityManager.EXTRA_NETWORK_INFO);
					String type = netInfo.getTypeName();
					NetworkInfo.State state = netInfo.getState();

					NetworkInfo activeNetInfo = getActiveNetworkInfo();
					if (activeNetInfo != null)
					{
						Log.d(THIS_FILE, "active network: " + activeNetInfo.getTypeName() + ((activeNetInfo.getState() == NetworkInfo.State.CONNECTED) ? " CONNECTED" : " DISCONNECTED"));
					}
					else
					{
						Log.d(THIS_FILE, "active network: null");
					}
					if ((state == NetworkInfo.State.CONNECTED) && (activeNetInfo != null) && (activeNetInfo.getType() != netInfo.getType()))
					{
						Log.d(THIS_FILE, "ignore connect event: " + type + ", active: " + activeNetInfo.getTypeName());
						return;
					}

					if (state == NetworkInfo.State.CONNECTED)
					{
						Log.d(THIS_FILE, "Connectivity alert: CONNECTED " + type);
						onChanged(type, true);
					}
					else if (state == NetworkInfo.State.DISCONNECTED)
					{
						Log.d(THIS_FILE, "Connectivity alert: DISCONNECTED " + type);
						onChanged(type, false);
					}
					else
					{
						Log.d(THIS_FILE, "Connectivity alert not processed: " + state + " " + type);
					}
				}
			}
			else if (action.equals(SipManager.ACTION_SIP_ACCOUNT_ACTIVE_CHANGED))
			{
				final long accountId = intent.getLongExtra(SipManager.EXTRA_ACCOUNT_ID, -1);
				final boolean active = intent.getBooleanExtra(SipManager.EXTRA_ACTIVATE, false);
				// Should that be threaded?
				if (accountId != SipProfile.INVALID_ID)
				{
					// SipProfile account;
					// synchronized (db)
					// {
					// db.open();
					// account = db.getAccount(accountId);
					// db.close();
					// }
					if (account != null)
					{
						setAccountRegistration(account, active ? 1 : 0);
					}
				}
			}
			else if (action.equals(Intent.ACTION_SCREEN_ON))
			{
				Log.d(THIS_FILE, "ACTION_SCREEN_ON");
				if (DialMain.currentContext != null)
				{
					DialMain.currentContext.cancelAutoClose(); // 만약 자동종료를 할려고 했다면..
				}				
			}
			else if (action.equals(Intent.ACTION_SCREEN_OFF))
			{
				Log.d(THIS_FILE, "ACTION_SCREEN_OFF");
				
				if (pjService != null && pjService.getActiveCallInProgress() == null)
				{
					if (DialMain.currentContext != null)
					{
						//DialMain.currentContext.setAutoClose(30000); // 화면 꺼진 후 30초후 자동 종료
					}
				}
			}		
		}

		private NetworkInfo getActiveNetworkInfo() {
			ConnectivityManager cm = (ConnectivityManager) SipService.this.getSystemService(Context.CONNECTIVITY_SERVICE);
			return cm.getActiveNetworkInfo();
		}

		private void onChanged(String type, boolean connected) {
			synchronized (SipService.this) {
				// When turning on WIFI, it needs some time for network
				// connectivity to get stabile so we defer good news (because
				// we want to skip the interim ones) but deliver bad news
				// immediately
				if (connected) {
					if (mTask != null)
					{
						mTask.cancel();
					}
					mTask = new MyTimerTask(type, connected);
					mTimer.schedule(mTask, 2 * 1000L);
					// hold wakup lock so that we can finish changes before the
					// device goes to sleep
					sipWakeLock.acquire(mTask);
				}
				else {
					if ((mTask != null) && mTask.mNetworkType.equals(type)) {
						mTask.cancel();
						sipWakeLock.release(mTask);
					}
					// onConnectivityChanged(type, false);
					Log.d(THIS_FILE, "FROM onChanged");
					dataConnectionChanged();
				}
			}
		}

		private class MyTimerTask extends TimerTask {
			private boolean mConnected;
			private String mNetworkType;

			public MyTimerTask(String type, boolean connected) {
				mNetworkType = type;
				mConnected = connected;
			}

			// timeout handler
			@Override
			public void run() {
				// delegate to mExecutor
				getExecutor().execute(new Runnable()
				{
					public void run()
					{
						realRun();
					}
				});
			}

			private void realRun() {
				synchronized (SipService.this) {
					if (mTask != this) {
						Log.w(THIS_FILE, "  unexpected task: " + mNetworkType + (mConnected ? " CONNECTED" : "DISCONNECTED"));
						return;
					}
					mTask = null;
					Log.d(THIS_FILE, " deliver change for " + mNetworkType + (mConnected ? " CONNECTED" : "DISCONNECTED"));
					// onConnectivityChanged(mNetworkType, mConnected);
					
					Log.d(THIS_FILE, "FROM realRun");
					dataConnectionChanged();
					sipWakeLock.release(this);
				}
			}
		}

		/*
		 * 
		 * @Override public void onReceive(Context context, Intent intent) { //
		 * // ACTION_CONNECTIVITY_CHANGED // Connectivity change is used to
		 * detect changes in the overall // data network status as well as a
		 * switch between wifi and mobile // networks. // //
		 * ACTION_DATA_STATE_CHANGED // Data state change is used to detect
		 * changes in the mobile // network such as a switch of network type
		 * (GPRS, EDGE, 3G) // which are not detected by the Connectivity
		 * changed broadcast. // Log.d(THIS_FILE, "ServiceDeviceStateReceiver");
		 * String action = intent.getAction(); if(action == null) { return; }
		 * 
		 * if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) { //||
		 * intent.getAction( ).equals(ACTION_DATA_STATE_CHANGED)
		 * Log.d(THIS_FILE, "Connectivity or data state has changed"); // Thread
		 * it to be sure to not block the device if registration // take time
		 * Thread t = new Thread() {
		 * 
		 * @Override public void run() {
		 * 
		 * dataConnectionChanged();
		 * 
		 * } }; t.start(); }else
		 * if(action.equals(SipManager.ACTION_SIP_ACCOUNT_ACTIVE_CHANGED)) {
		 * final long accountId =
		 * intent.getLongExtra(SipManager.EXTRA_ACCOUNT_ID, -1); final boolean
		 * active = intent.getBooleanExtra(SipManager.EXTRA_ACTIVATE, false);
		 * //Should that be threaded? if(accountId != SipProfile.INVALID_ID) {
		 * SipProfile account; synchronized (db) { db.open(); account =
		 * db.getAccount(accountId); db.close(); } if(account != null) {
		 * setAccountRegistration(account, active?1:0); } } }
		 * 
		 * 
		 * }
		 */
	}

	private MyExecutor getExecutor() {
		// create mExecutor lazily
		if (mExecutor == null) {
			mExecutor = new MyExecutor();
		}
		return mExecutor;
	}

	@RequiresApi(api=Build.VERSION_CODES.S)
	private class CallStateChangeReceiver extends TelephonyCallback implements TelephonyCallback.CallStateListener {
		@Override
		public void onCallStateChanged(int state) {
			pjService.onGSMStateChanged(state, "");
		}
	}


	@RequiresApi(api = Build.VERSION_CODES.S)
	private class DataConnectionStateReceiver extends TelephonyCallback implements TelephonyCallback.DataConnectionStateListener {
		private Boolean firstEvent = true;

		@Override
		public void onDataConnectionStateChanged(int state, int networkType) {
			if(firstEvent) {
				firstEvent = false;
				return;
			}

			Log.d(THIS_FILE, "Data connection state changed : "+state);
			Thread t = new Thread()
			{
				@Override
				public void run()
				{
					dataConnectionChanged();
				}
			};
			if (t != null) {
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
		}
	}

	private class ServicePhoneStateReceiver extends PhoneStateListener {
		private Boolean firstEvent = true;
		@Override
		public void onDataConnectionStateChanged(int state) {
			if (firstEvent) {
				firstEvent = false;
				return;				
			}
			
			Log.d(THIS_FILE, "Data connection state changed : " + state);
			Thread t = new Thread()
			{
				@Override
				public void run()
				{
					dataConnectionChanged();
				}
			};
			if (t != null) {
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
			super.onDataConnectionStateChanged(state);
		}

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			Log.d(THIS_FILE, "Call state has changed !" + state + " : " + incomingNumber);

			pjService.onGSMStateChanged(state, incomingNumber);

			super.onCallStateChanged(state, incomingNumber);
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		Log.i(THIS_FILE, "Create SIP Service");
		
		currentService = this;
		mPrefs = new AppPrefs(this);
		db = new DBAdapter(this);
		prefsWrapper = new PreferencesWrapper(this);
		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//		connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		notificationManager = new SipNotifications(this);
		notificationManager.cancelAllNotification();
		
		sipWakeLock = new SipWakeLock((PowerManager) getSystemService(Context.POWER_SERVICE));

		mReconnectLock = new Object();
		account = null;
		pjService = new PjSipService(this);		
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
//		removeAccounts(false);
		disconnect();
		stopSipStack();
		stopSelf();
		super.onTaskRemoved(rootIntent);
	}

	@Override
	public void onDestroy() {
		Log.i(THIS_FILE, "Destroying SIP Service");
		if (deviceStateReceiver != null) {
			try {
				Log.d(THIS_FILE, "Unregister telephony receiver");
				unregisterReceiver(deviceStateReceiver);
				deviceStateReceiver.stop();
			}
			catch (Exception e) {
				// This is the case if already unregistered itself
				// Python style usage of try ;) : nothing to do here since it could
				// be a standard case
				// And in this case nothing has to be done
				Log.d(THIS_FILE, "Has not to unregister telephony receiver");
			}
			deviceStateReceiver = null;
		}
		if (phoneConnectivityReceiver != null) {
			Log.d(THIS_FILE, "Unregister telephony receiver");

			try {
				telephonyManager.listen(phoneConnectivityReceiver, PhoneStateListener.LISTEN_NONE);
				phoneConnectivityReceiver = null;
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.S) {
				if(callStateCallback!=null) {
					try {
						telephonyManager.unregisterTelephonyCallback(callStateCallback);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(dataConnectionStateCallback!=null) {
					try {
						telephonyManager.unregisterTelephonyCallback(dataConnectionStateCallback);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} else {
				try {
					telephonyManager.listen(phoneConnectivityReceiver, PhoneStateListener.LISTEN_NONE);
					phoneConnectivityReceiver = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (executorThread != null) {
			Threading.stopHandlerThread(executorThread);
			executorThread = null;
		}
		
		if (mExecutor != null) {
			mExecutor = null;
		}

		if (pjService != null) {
//			removeAccounts(false);
			disconnect();
			pjService.sipStop();
//			pjService = null;
		}
		
		if (notificationManager != null) {
			notificationManager.cancelAllNotification();
			notificationManager = null;
		}
		
		if (db != null) {
			if (db.isOpen()) db.close();
			db = null;
		}
		
		releaseResources();
		releaseWifi();		
		
		currentService = null;
		
		Log.i(THIS_FILE, "--- SIP SERVICE DESTROYED ---");
		
		super.onDestroy();	
	}
	
	/*
	@Override
	public void onStart(Intent intent, int startId)
	{
		Log.d(THIS_FILE, "onStart");
		super.onStart(intent, startId);
		
		// Autostart the stack
		if (loadAndConnectStack())
		{
			Thread t = new Thread()
			{
				public void run()
				{
					if ((ServicePrefs.mPrefs == null))
					{
						Log.d(THIS_FILE, "onStart : restart !!");
						processAutoLogin();			
					}					
					
					Log.d(THIS_FILE, "Start sip stack because start asked");
					startSipStack();
				}
			};
			if (t != null)
			{
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}		
		}
	}
	*/

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(THIS_FILE, "onStartCommand : " + flags);		
		Fcm.init();

		FirebaseCrashlytics.getInstance().log("SipService onStartCommand called.");
		
		// Autostart the stack
		if (loadAndConnectStack())
		{

			Thread t = new Thread()
			{
				public void run()
				{
					try
					{
						if ((!ServicePrefs.mLogin))
						{
							Log.d(THIS_FILE, "onStart : restart !!");
							processAutoLogin();			
						}					
						
						Log.d(THIS_FILE, "Start sip stack because start asked");
						startSipStack();
					}
					catch(Exception e)
					{
						Log.e(THIS_FILE, "ClientProtocolException", e);
						FirebaseCrashlytics.getInstance().recordException(e);
					}
				}
			};
			if (t != null)
			{
				t.setPriority(Thread.MAX_PRIORITY);
				t.start();
			}
		}

		super.onStartCommand(intent, flags, startId);
		return START_NOT_STICKY; // START_STICKY
	}

	private synchronized void processAutoLogin()
	{
		FirebaseCrashlytics.getInstance().log("SipService processAutoLogin called.");

		if (!ServicePrefs.mLogin)
		{
			// check id & password
			String str_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
			String str_password = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);
			String fcmToken = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);

			try {
				FirebaseCrashlytics.getInstance().setCustomKey("09id", str_id);
				FirebaseCrashlytics.getInstance().setCustomKey("password", str_password);
				FirebaseCrashlytics.getInstance().setCustomKey("fcmToken", fcmToken);

			} catch (Exception e) {
				e.printStackTrace();
			}

			if (str_id != null && str_id.length() > 0 && str_password != null && str_password.length() > 0)
			{
				Log.d(THIS_FILE, "LOGIN !!");
				int rs = ServicePrefs.login(this, str_id, str_password);
				if (rs < 0)
				{
					// 실패.
					if (rs == -2) // BJH 2017.10.27 기기변경
					{
						FirebaseCrashlytics.getInstance().recordException(new Exception("SipService processAutoLogin login result fail: "+rs));

						Looper.prepare();
						DialMain.currentContext.deviceChange();
						Looper.loop();
						return;
					}
				}
			}
		}

		else {
			String str_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
			String str_password = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);
			String fcmToken = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);

			try {
				FirebaseCrashlytics.getInstance().setCustomKey("09id", str_id);
				FirebaseCrashlytics.getInstance().setCustomKey("password", str_password);
				FirebaseCrashlytics.getInstance().setCustomKey("fcmToken", fcmToken);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
	
	private boolean loadAndConnectStack()
	{
		FirebaseCrashlytics.getInstance().log("SipService loadAndConnectStack() called.");
		// Ensure pjService exists
		if (pjService == null)
		{
			pjService = new PjSipService(this);
		}

		if (pjService.tryToLoadStack())
		{
			// Register own broadcast receiver
			if (deviceStateReceiver == null)
			{
				IntentFilter intentfilter = new IntentFilter();
				intentfilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
				intentfilter.addAction(SipManager.ACTION_SIP_ACCOUNT_ACTIVE_CHANGED);
				intentfilter.addAction(Intent.ACTION_SCREEN_ON);
				intentfilter.addAction(Intent.ACTION_SCREEN_OFF);
				deviceStateReceiver = new ServiceDeviceStateReceiver();
				registerReceiver(deviceStateReceiver, intentfilter);
			}
			if (phoneConnectivityReceiver == null)
			{
				Log.d(THIS_FILE, "Listen for phone state ");
				phoneConnectivityReceiver = new ServicePhoneStateReceiver();
				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.S) {

//					callStateCallback = (TelephonyCallback) (TelephonyCallback.CallStateListener) state -> {
//						pjService.onGSMStateChanged(state, ""); // incoming number 쓰는 곳 없음
//					};
//					if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)==PackageManager.PERMISSION_GRANTED) {
//						DebugLog.d("test_v2 phone_state_check VERSION_CODES_S condition called.");
//						telephonyManager.listen(phoneConnectivityReceiver, PhoneStateListener.LISTEN_DATA_CONNECTION_STATE | PhoneStateListener.LISTEN_CALL_STATE);
//					}

					callStateCallback = new CallStateChangeReceiver();
					dataConnectionStateCallback = new DataConnectionStateReceiver();

					if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)!=PackageManager.PERMISSION_GRANTED) {
						FirebaseCrashlytics.getInstance().recordException(new Exception("NOT_GRANTED READ_PHONE_STATE"));

					} else {
						telephonyManager.registerTelephonyCallback(getMainExecutor(), callStateCallback);
						telephonyManager.registerTelephonyCallback(getMainExecutor(), dataConnectionStateCallback);
					}

//					telephonyManager.registerTelephonyCallback(getMainExecutor(), callStateCallback);
//					telephonyManager.registerTelephonyCallback(getMainExecutor(), dataConnectionStateCallback);

				} else {
					try {
						telephonyManager.listen(phoneConnectivityReceiver,
								PhoneStateListener.LISTEN_DATA_CONNECTION_STATE | PhoneStateListener.LISTEN_CALL_STATE);
					} catch (Exception e) {
						e.printStackTrace();
						FirebaseCrashlytics.getInstance().recordException(e);
					}
//					telephonyManager.listen(phoneConnectivityReceiver,
//							PhoneStateListener.LISTEN_DATA_CONNECTION_STATE | PhoneStateListener.LISTEN_CALL_STATE);
				}
			}
			return true;
		}
		FirebaseCrashlytics.getInstance().recordException(new Exception("SipService loadAndConnectStack() failed."));
		return false;
	}

	@Override
	public IBinder onBind(Intent intent)
	{

		String serviceName = intent.getAction();
		Log.d(THIS_FILE, "Action is " + serviceName);
		if (serviceName == null || serviceName.equalsIgnoreCase(INTENT_SIP_SERVICE))
		{
			Log.d(THIS_FILE, "Service returned");
			return binder;
		}
		else if (serviceName.equalsIgnoreCase(INTENT_SIP_CONFIGURATION))
		{
			Log.d(THIS_FILE, "Conf returned");
			return binderConfiguration;
		}
		Log.d(THIS_FILE, "Default service (SipService) returned");
		return binder;
	}

	public void startSipStack()
	{
		if (!needToStartSip())
		{
			Intent intent = new Intent("connection_not_valid");
			sendBroadcast(intent);
			ToastHandler.sendMessage(ToastHandler.obtainMessage(0, R.string.connection_not_valid, 0));
			Log.e(THIS_FILE, "Not able to start sip stack");
			return;
		}
		
		acquireWifi();
		
		if (pjService.sipStart())
		{
			addAccounts();
			//updateRegistrationsState(getRegistrationsState());
		}
		else
		{
			if (account == null)
			{
				addAccounts();
			}
		}
	}

	public void stopSipStack()
	{
		pjService.sipStop();
		releaseResources();
		releaseWifi();
		/**
		 * add
		 */
//		removeAccounts(false);
	}
	
	public void connect()
	{
		synchronized (mReconnectLock)
		{
			addAccounts();
		}
	}
	
	public void disconnect()
	{
		synchronized(mReconnectLock)
		{
			removeAccounts(true);
		}
	}
	
	public void reconnect()
	{
		synchronized (mReconnectLock)
		{
			//The connection is valid?
			if (needToStartSip())
			{
				if (account != null)
				{
					updateAllAccounts(true);
				}
				else
				{
					addAccounts();					
				}
			}
			else
			{
				//ToastHandler.sendMessage(ToastHandler.obtainMessage(0, R.string.connection_not_valid, 0));
				removeAccounts(true);
			}			
		}
	}
	
	public synchronized void restart()
	{
		disconnect();
		pjService.sipStop();
		startSipStack();
	}

	public boolean needToStartSip()
	{
		// The connection is valid?
		return prefsWrapper.isValidConnectionForIncoming() || prefsWrapper.isValidConnectionForOutgoing();
	}

	public void notifyUserOfMessage(String msg)
	{
		ToastHandler.sendMessage(ToastHandler.obtainMessage(0, msg));
	}



	/**
	 * Add accounts from database
	 */
	private void addAccounts()
	{
		if (account != null)
		{
			Log.e(THIS_FILE, "addAccounts SKIP !!");
			return;		
		}
		
		Log.d(THIS_FILE, "We are adding all accounts right now....");
		boolean hasSomeSuccess = false;
		if (ServicePrefs.mLogin)
		{
			account = new SipProfile();
			account = ServicePrefs.buildAccount(account);
			if (account != null)
			{
				if (pjService.addAccount(account))
				{
					hasSomeSuccess = true;
				}
			}
		}

		if (hasSomeSuccess)
		{
			acquireResources();
		}
		else
		{
			releaseResources();
			if (notificationManager != null)
			{
				//notificationManager.cancelRegisters();
				notificationManager.notifyRegisteredAccounts(null);
			}
		}
	}

	private boolean setAccountRegistration(SipProfile account, int renew)
	{
		boolean status = pjService.setAccountRegistration(account, renew);

		// Send a broadcast message that for an account
		// registration state has changed
		Intent regStateChangedIntent = new Intent(SipManager.ACTION_SIP_REGISTRATION_CHANGED);
		sendBroadcast(regStateChangedIntent);

		updateRegistrationsState(getRegistrationsState());

		return status;
	}

	/**
	 * Remove accounts from database
	 */
	private void removeAccounts(boolean cancelNotification)
	{
		releaseResources();

		Log.d(THIS_FILE, "Remove all accounts");

		if (account != null)
		{
			setAccountRegistration(account, 0);
			account = null;
		}

		if (notificationManager != null && cancelNotification)
		{
			//notificationManager.cancelRegisters();
			notificationManager.notifyRegisteredAccounts(null);
		}
	}
	
	private void updateAllAccounts(boolean cancelNotification)
	{
		if (account != null)
		{
			pjService.updateAccount(account);
		}
		
		if (notificationManager != null && cancelNotification)
		{
			//notificationManager.cancelRegisters();
			notificationManager.notifyRegisteredAccounts(null);
		}
		
	}

	public SipProfileState getSipProfileState(int accountDbId)
	{
		// SipProfile account;
		// synchronized (db)
		// {
		// db.open();
		// account = db.getAccount(accountDbId);
		// db.close();
		// }
		return pjService.getAccountInfo(account);
	}

	/**
	 * ADDED 2020.08.18 블루투스 장비 연결이 동적을 변할 때를 위한 콜세션 함수
	 * @param callId
	 * @return
	 */
	public SipCallSession getSipCallSession(int callId) {
		return pjService.getCallInfo(callId);
	}

	public int getCallState() {
		return state;
	}
	
	public ArrayList<SipProfileState> getRegistrationsState()
	{
		return pjService.getAndUpdateActiveAccounts();
	}

	public void updateRegistrationsState(ArrayList<SipProfileState> activeAccountsInfos)
	{
		//ArrayList<SipProfileState> activeAccountsInfos = pjService.getAndUpdateActiveAccounts();
		// Handle status bar notification
		if (activeAccountsInfos != null && activeAccountsInfos.size() > 0)
		{
			/*BJH 2016.07.04 java.lang.NullPointerException
			notificationManager.notifyRegisteredAccounts(activeAccountsInfos);*/
			acquireResources();
		}
		else
		{
			/*BJH 2016.06.07 notifyRegisteredAccounts 아무것도 없으므로
			notificationManager.notifyRegisteredAccounts(null);*/
			//notificationManager.cancelRegisters();
			releaseResources();
		}
	}

	/**
	 * Get the currently instanciated prefsWrapper (to be used by
	 * UAStateReceiver)
	 * 
	 * @return the preferenceWrapper instanciated
	 */
	public PreferencesWrapper getPrefs()
	{
		// Is never null when call so ok, just not check...
		return prefsWrapper;
	}

	/**
	 * Ask to take the control of the wifi and the partial wake lock if
	 * configured
	 */
	private void acquireResources()
	{
		Log.d(THIS_FILE, "ACQUIRE START");
		// Add a wake lock
		if (prefsWrapper.usePartialWakeLock())
		{
			PowerManager pman = (PowerManager) getSystemService(Context.POWER_SERVICE);
			if (wakeLock == null)
			{
				wakeLock = pman.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "maaltalk:com.dial070.sip.SipService");
				wakeLock.setReferenceCounted(false);
			}
			// Extra check if set reference counted is false ???
			if (!wakeLock.isHeld())
			{
				Log.d(THIS_FILE, "ACQUIRE WAKELOCK");
				wakeLock.acquire();
			}
		}
		/* BJH 2017.03.08
		Error:Error: The WIFI_SERVICE must be looked up on the Application context or memory will leak on devices < Android N. Try changing to .getApplicationContext() [WifiManagerLeak]
		*/
		WifiManager wman = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if (wifiLock == null)
		{
			wifiLock = wman.createWifiLock("com.dial070.sip.SipService");
			wifiLock.setReferenceCounted(false);
		}
		
		if (prefsWrapper.getLockWifi() && !wifiLock.isHeld())
		{
			WifiInfo winfo = wman.getConnectionInfo();
			if (winfo != null)
			{
				DetailedState dstate = WifiInfo.getDetailedStateOf(winfo.getSupplicantState());
				// We assume that if obtaining ip addr, we are almost connected
				// so can keep wifi lock
				if (dstate == DetailedState.OBTAINING_IPADDR || dstate == DetailedState.CONNECTED)
				{
					if (!wifiLock.isHeld() && !ServicePrefs.DIAL_TEST_SKIP_LOCK)
					{
						Log.d(THIS_FILE, "ACQUIRE WIFILOCK");
						wifiLock.acquire();
					}
				}
			}
		}
		Log.d(THIS_FILE, "ACQUIRE END");
	}

	private void releaseResources()
	{
		Log.d(THIS_FILE, "RELEASE START");
		
		if (wakeLock != null && wakeLock.isHeld())
		{
			Log.d(THIS_FILE, "RELEASE WAKELOCK");
			wakeLock.release();
		}
		if (wifiLock != null && wifiLock.isHeld())
		{
			Log.d(THIS_FILE, "RELEASE WIFILOCK");
			wifiLock.release();
		}
		
		Log.d(THIS_FILE, "RELEASE END");
	}
	
	
	private boolean wifi_auto_close = false;
	private boolean acquireWifi()
	{
		//Log.d(THIS_FILE, "acquireWifi req");
		/* BJH 2017.03.08
		Error:Error: The WIFI_SERVICE must be looked up on the Application context or memory will leak on devices < Android N. Try changing to .getApplicationContext() [WifiManagerLeak]
		*/
		WifiManager wman = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if (wman == null) return false;
		
		if (wifi_auto_close) return false;
		
		if (prefsWrapper.getPreferenceBooleanValue(PreferencesWrapper.USE_LTE_MODE))
		{
			if (wman.isWifiEnabled())
			{
				wman.setWifiEnabled(false);
				wifi_auto_close = true;
				Log.d(THIS_FILE, "acquireWifi on");
				return true;
			}
		}		
		return false;
	}
	
	private boolean releaseWifi()
	{
		//Log.d(THIS_FILE, "releaseWifi req");
		/* BJH 2017.03.08
		Error:Error: The WIFI_SERVICE must be looked up on the Application context or memory will leak on devices < Android N. Try changing to .getApplicationContext() [WifiManagerLeak]
		*/
		WifiManager wman = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if (wman == null) return false;
		
		if (wifi_auto_close)
		{
			wifi_auto_close = false;
			
			if (!wman.isWifiEnabled())
			{
				wman.setWifiEnabled(true);
				Log.d(THIS_FILE, "releaseWifi on");
				return true;
			}
		}			
		return false;
	}
	
	
	public synchronized void wifiConfigChanged()
	{
		releaseWifi();
		acquireWifi();
	}
	
	private KeyguardManager.KeyguardLock screenLock = null;
	private Boolean isdisableKeyguard = false;
	public void disableKeyguard()
	{
		Log.d(THIS_FILE, "disableKeyguard");
		
		KeyguardManager km = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
		if (km == null) return;
		if (screenLock == null)
			screenLock = km.newKeyguardLock("com.dial070.sip.SipService");
		if (screenLock != null && !isdisableKeyguard)
		{
			isdisableKeyguard = true;
			screenLock.disableKeyguard();
		}
	}
	
	public void reenableKeyguard()
	{
		Log.d(THIS_FILE, "reenableKeyguard");
		if(screenLock !=null && isdisableKeyguard)
		{
			screenLock.reenableKeyguard();
			isdisableKeyguard = false;
		}
	}


	@SuppressLint("HandlerLeak")
	private Handler ToastHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			if (msg.arg1 != 0)
			{
				Toast.makeText(SipService.this, msg.arg1, Toast.LENGTH_LONG).show();
			}
			else
			{
				Toast.makeText(SipService.this, (String) msg.obj, Toast.LENGTH_LONG).show();
			}
		}
	};

	public static UAStateReceiver getUAStateReceiver()
	{
		return pjService.userAgentReceiver;
	}

//	private String getLocalIpAddress()
//	{
//		try
//		{
//			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
//			{
//				NetworkInterface intf = en.nextElement();
//				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
//				{
//					InetAddress inetAddress = enumIpAddr.nextElement();
//					if (!inetAddress.isLoopbackAddress()) 
//					{ 
//						return inetAddress.getHostAddress().toString(); 
//					}
//				}
//			}			
//		}
//		catch (SocketException ex)
//		{
//			Log.e(THIS_FILE, "Error while getting self IP", ex);
//		}
//		return null;
//	}
	
//	private String getWifiAddress()
//	{
//		WifiManager wifiMan = (WifiManager)getSystemService(Context.WIFI_SERVICE);
//		WifiInfo wifiInf = wifiMan.getConnectionInfo();
//		int ipAddress = wifiInf.getIpAddress();
//		String ip = String.format(Locale.getDefault(), "%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
//		return ip;
//	}

//	private String oldIPAddress = "0.0.0.0";	
	public synchronized void dataConnectionChanged()
	{
		boolean ipHasChanged = true;
		if (!pjService.isCreated())
		{
			// we was not yet started, so start now
			Thread t = new Thread()
			{
				public void run()
				{
					Log.d(THIS_FILE, "****** >> AUTO : RESTART");
					startSipStack();
				}
			};
			if (t != null)
			{
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
		}
		else if (ipHasChanged)
		{
			// Check if IP has changed between
			Thread t = new Thread()
			{
				public void run()
				{
					Log.d(THIS_FILE, "****** >> AUTO : RECONNECT");
					reconnect();
				}
			};
			if (t != null)
			{
				t.setPriority(Thread.MIN_PRIORITY);
				t.start();
			}
		}
		else
		{
			//Log.d(THIS_FILE, "Nothing done since already well registered");
		}
	}

	public int getGSMCallState() {
//		return telephonyManager.getCallState();
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.S) {
			if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
				return telephonyManager.getCallStateForSubscription();
			} else {

			}
		} else {
			return telephonyManager.getCallState();
		}
		FirebaseCrashlytics.getInstance().recordException(new Exception("Permission Exception READ_PHONE_STATE is Not Granted."));
		return TelephonyManager.CALL_STATE_IDLE;
	}

	public static final class ToCall
	{
		private Integer pjsipAccountId;
		private String callee;

		public ToCall(Integer acc, String uri)
		{
			pjsipAccountId = acc;
			callee = uri;
		}

		/**
		 * @return the pjsipAccountId
		 */
		public Integer getPjsipAccountId()
		{
			return pjsipAccountId;
		}

		/**
		 * @return the callee
		 */
		public String getCallee()
		{
			return callee;
		}
	};

	public SipProfile getAccount(int accountId)
	{
		return account;
//		synchronized (db)
//		{
//			return SipService.getAccount(accountId, db);
//		}
	}

//	public static SipProfile getAccount(int accountId, DBAdapter db)
//	{
//		// db.open();
//		// SipProfile account = db.getAccount(accountId);
//		// db.close();
//		return account;
//	}

	// Auto answer feature

	public void setAutoAnswerNext(boolean auto_response)
	{
		autoAcceptCurrent = auto_response;
	}

	public boolean shouldAutoAnswer(String remContact, SipProfile acc)
	{
		boolean shouldAutoAnswer = false;

		if (autoAcceptCurrent)
		{
			autoAcceptCurrent = false;
			return true;
		}

		if (acc != null && remContact != null)
		{
			/*
			Pattern p = Pattern.compile("^(?:\")?([^<\"]*)(?:\")?[ ]*(?:<)?sip(?:s)?:([^@]*@[^>]*)(?:>)?", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(remContact);
			String number = remContact;
			if (m.matches())
			{
				number = m.group(2);
			}
			shouldAutoAnswer = Filter.isAutoAnswerNumber(acc, number, pjService.service.db);
			*/
			
			String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTO_ANSWER);
			
			Log.d(THIS_FILE, "CHECK AUTO ANSWER FOR : " + cid);
			if (cid != null && cid.length() > 0)
			{				
				ParsedSipContactInfos uriInfos = SipUri.parseSipContact(remContact);
				if (uriInfos != null)
				{
					String number = uriInfos.userName;					
					Log.d(THIS_FILE, "CHECK AUTO ANSWER : " + cid + " vs " + number);
					if (number != null && cid.equalsIgnoreCase(number))
					{
						Log.d(THIS_FILE, "SET AUTO ANSWER OK : " + cid);
						shouldAutoAnswer = true;
					}					
				}
				else
				{
					if (remContact.startsWith(cid))
					{
						Log.d(THIS_FILE, "SET AUTO ANSWER OK : " + cid);
						shouldAutoAnswer = true;
					}
					
				}
				prefsWrapper.setPreferenceStringValue(AppPrefs.AUTO_ANSWER, ""); // REMOVE
			}
		}
		return shouldAutoAnswer;
	}

	private static Looper createLooper()
	{
		if (executorThread == null)
		{
			executorThread = new HandlerThread("SipService.Executor");
			executorThread.start();
		}
		return executorThread.getLooper();
	}

	// Executes immediate tasks in a single executorThread.
	// Hold/release wake lock for running tasks
	@SuppressLint("HandlerLeak")
	private class MyExecutor extends Handler
	{
		MyExecutor()
		{
			super(createLooper());
		}

		void execute(Runnable task)
		{
			sipWakeLock.acquire(task);
			Message.obtain(this, 0/* don't care */, task).sendToTarget();
		}

		@Override
		public void handleMessage(Message msg)
		{
			if (msg.obj instanceof Runnable)
			{
				executeInternal((Runnable) msg.obj);
			}
			else
			{
				Log.w(THIS_FILE, "can't handle msg: " + msg);
			}
		}

		private void executeInternal(Runnable task)
		{
			try
			{
				task.run();
			}
			catch (Throwable t)
			{
				Log.e(THIS_FILE, "run task: " + task, t);
			}
			finally
			{
				sipWakeLock.release(task);
			}
		}
	}
	//BJH 2016.6.21 결제 성공시에만
	public void changeSipStack()
	{
		boolean hasSomeSuccess = false;
		if (ServicePrefs.mLogin)
		{
			account = new SipProfile();
			account = ServicePrefs.buildAccount(account);
			if (account != null)
			{
				if (pjService.addAccount(account))
				{
					hasSomeSuccess = true;
				}
			}
		}

		if (hasSomeSuccess)
		{
			acquireResources();
		}
		else
		{
			releaseResources();
			if (notificationManager != null)
			{
				//notificationManager.cancelRegisters();
				notificationManager.notifyRegisteredAccounts(null);
			}
		}
	}

}
