/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 * <p>
 * Dial070 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Dial070 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.pjsip;

import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pjsip.pjsua.*;

import android.content.*;
import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;


import com.dial070.maaltalk.R;
import com.dial070.global.*;
import com.dial070.sip.api.*;
import com.dial070.sip.service.*;
import com.dial070.sip.service.SipService.*;
import com.dial070.sip.utils.*;
import com.dial070.utils.*;
import com.google.firebase.crashlytics.FirebaseCrashlytics;


public class PjSipService {
    private static final String THIS_FILE = "Dial070-SIP";
    public SipService service;


    private boolean created = false;

    public static boolean hasSipStack = false;
    private boolean sipStackIsCorrupted = false;
    public static boolean creating = false;
    private Integer udpTranportId, tcpTranportId, tlsTransportId;
    public PreferencesWrapper prefsWrapper;
    private PjStreamDialtoneGenerator dialtoneGenerator;

    private static Object creatingSipStack = new Object();
    private int hasBeenHoldByGSM = SipCallSession.INVALID_CALL_ID;
    public static boolean isHold = false;

    public UAStateReceiver userAgentReceiver = null;
    public UARttReceiver rttReceiver = null;
    public MediaManager mediaManager = null;

    // -------
    // Static constants
    // -------

    private static Object pjAccountsCreationLock = new Object();
    private static Object activeAccountsLock = new Object();
    private Object callActionLock = new Object();


    // Map active account id (id for sql settings database) with acc_id (id for
    // pjsip)
    private static HashMap<Integer, Integer> activeAccounts = new HashMap<Integer, Integer>();
    private static HashMap<Integer, Integer> accountsAddingStatus = new HashMap<Integer, Integer>();

    public PjSipService(SipService aService) {
        DebugLog.d("PjSipService constructor is called caller --> "+aService.getClass().getSimpleName());
        service = aService;
        prefsWrapper = service.prefsWrapper;
        creating = false;
        created = false;
        hasSipStack = false;
    }

    public boolean isCreated() {
        return created;
    }

    public boolean tryToLoadStack() {
        FirebaseCrashlytics.getInstance().log("PjSipService tryToLoadStack() called.");

        if (hasSipStack) {
            return true;
        }

        if (!sipStackIsCorrupted) {
            try {
                System.loadLibrary(NativeLibManager.STD_LIB_NAME);
                System.loadLibrary(NativeLibManager.STACK_NAME);

                hasSipStack = true;
                return true;
            } catch (UnsatisfiedLinkError e) {
                // If it fails we probably are running on a special hardware
                Log.e(THIS_FILE, "We have a problem with the current stack.... NOT YET Implemented", e);
                hasSipStack = false;
                sipStackIsCorrupted = true;

                FirebaseCrashlytics.getInstance().recordException(new Exception(e.getMessage()));

                service.notifyUserOfMessage("Can't load native library. CPU arch invalid for this build");
                return false;
            } catch (Exception e) {
                Log.e(THIS_FILE, "We have a problem with the current stack....", e);
                hasSipStack = false;

                FirebaseCrashlytics.getInstance().recordException(new Exception(e.getMessage()));

            }
        }
        return false;
    }


    // Start the sip stack according to current settings
    public synchronized boolean sipStart() {
//		Log.e(THIS_FILE, "Set log level to "+prefsWrapper.getLogLevel());

        DebugLog.d("sipStart...");
        FirebaseCrashlytics.getInstance().log("PjSipService startSip() called.");

        if (!hasSipStack) {
            Log.e(THIS_FILE, "We have no sip stack, we can't start");

            FirebaseCrashlytics.getInstance().log("PjSipService startSip() haven't sip stack.");

            return false;
        }

        //There is some active accounts?
        /*
		List<SipProfile> accs;
		synchronized (db) {
			db.open();
			accs = db.getListAccounts(true);
			db.close();
		}
		
		if(accs == null || accs.size() <= 0) {
			Log.w(THIS_FILE, "Useless to start since no account");
			return;
		}
		*/

        try {

            Log.i(THIS_FILE, "Will start sip : " + (!created /* && !creating */));
            DebugLog.d("Will start sip -->"+(!created /* && !creating */));

            synchronized (creatingSipStack) {
                // Ensure the stack is not already created or is being created
                if (!created/* && !creating */) {
//                if (true/* && !creating */) {
                    creating = true;
                    udpTranportId = null;
                    tcpTranportId = null;
                    tlsTransportId = null;

                    TimerWrapper.create(service);

                    int status;
                    status = pjsua.create();
                    Log.i(THIS_FILE, "Created " + status);
                    // General config
                    {
                        pjsua_config cfg = new pjsua_config();
                        pjsua_logging_config logCfg = new pjsua_logging_config();
                        pjsua_media_config mediaCfg = new pjsua_media_config();
                        csipsimple_config cssCfg = new csipsimple_config();

                        int apiLevel = Compatibility.getApiLevel();
                        if (android.os.Build.BRAND.equalsIgnoreCase("Samsung") && (apiLevel >= 18) && (apiLevel <= 19)) {
                            cssCfg.setApi_level(apiLevel);
                        } else {
                            // 오디오 렌더 쓰레드에서 동작하지 않도록 함.
                            cssCfg.setApi_level(9);
                        }

                        // codec
                        int codec_count = 0;
                        if (ServicePrefs.DIAL_CODEC_G729) {
                            dynamic_factory[] cssCodecs = cssCfg.getExtra_aud_codecs();

                            File codecLib = NativeLibManager.getBundledStackLibFile(service, "libpj_g729_codec.so");
                            cssCodecs[codec_count].setShared_lib_path(pjsua.pj_str_copy(codecLib.getAbsolutePath()));
                            cssCodecs[codec_count].setInit_factory_name(pjsua.pj_str_copy("pjmedia_codec_g729_init"));

                            cssCfg.setExtra_aud_codecs_cnt(++codec_count);
                        }

//                        if (ServicePrefs.DIAL_CODEC_OPUS) {
//                            dynamic_factory[] cssCodecs = cssCfg.getExtra_aud_codecs();
//
//                            File codecLib = NativeLibManager.getBundledStackLibFile(service, "libpj_opus_codec.so");
//                            cssCodecs[codec_count].setShared_lib_path(pjsua.pj_str_copy(codecLib.getAbsolutePath()));
//                            cssCodecs[codec_count].setInit_factory_name(pjsua.pj_str_copy("pjmedia_codec_opus_init"));
//                            cssCfg.setExtra_aud_codecs_cnt(++codec_count);
//                        }

                        // opensl
                        {
                            dynamic_factory audImp = cssCfg.getAudio_implementation();
                            audImp.setInit_factory_name(pjsua.pj_str_copy("pjmedia_opensl_factory"));
                            File openslLib = NativeLibManager.getBundledStackLibFile(service, "libpj_opensl_dev.so");
                            audImp.setShared_lib_path(pjsua.pj_str_copy(openslLib.getAbsolutePath()));
                            cssCfg.setAudio_implementation(audImp);
                            Log.d(THIS_FILE, "Use OpenSL-ES implementation");
                        }

                        // ZRTP
                        //cssCfg.setUse_zrtp(pjsuaConstants.PJ_TRUE);

                        // GLOBAL CONFIG
                        pjsua.config_default(cfg);
                        cfg.setCb(pjsuaConstants.WRAPPER_CALLBACK_STRUCT);
                        if (userAgentReceiver == null) {
                            userAgentReceiver = new UAStateReceiver();
                            userAgentReceiver.initService(this);
                        }
                        if (mediaManager == null) {
//                            mediaManager = new MediaManager(service);
                            mediaManager = MediaManager.getInstance(service);
                        }

                        mediaManager.startService();

                        pjsua.setCallbackObject(userAgentReceiver);


                        Log.d(THIS_FILE, "Attach is done to callback");

                        // MAIN CONFIG

                        cfg.setUser_agent(pjsua.pj_str_copy(ServicePrefs.getUserAgent()));
                        cfg.setThread_cnt(prefsWrapper.getThreadCount()); // one thread seems to be enough
                        // for now
                        cfg.setUse_srtp(prefsWrapper.getUseSrtp());
                        cfg.setSrtp_secure_signaling(0);
                        cfg.setStun_ignore_failure(pjsuaConstants.PJ_TRUE);


                        // DNS
                        if (prefsWrapper.enableDNSSRV() && !prefsWrapper.useIPv6()) {
                            pj_str_t[] nameservers = prefsWrapper.getNameservers();
                            if (nameservers != null) {
                                cfg.setNameserver_count(nameservers.length);
                                cfg.setNameserver(nameservers);
                            } else {
                                cfg.setNameserver_count(0);
                            }
                        }
                        // STUN
                        int isStunEnabled = prefsWrapper.getStunEnabled();
                        if (ServicePrefs.mProxyIP != null && ServicePrefs.mProxyIP.startsWith("192.168."))
                            isStunEnabled = 0; // FOR TB

                        if (isStunEnabled == 1) {
                            String StunServer = prefsWrapper.getStunServer();
                            if (StunServer != null) {
                                String[] servers = StunServer.split(",");
                                cfg.setStun_srv_cnt(servers.length);
                                pj_str_t[] stunServers = cfg.getStun_srv();
                                int i = 0;
                                for (String server : servers) {
                                    Log.d(THIS_FILE, "ADD STUN SERVER " + server.trim());
                                    stunServers[i] = pjsua.pj_str_copy(server.trim());
                                    i++;
                                }
                                cfg.setStun_srv(stunServers);
                            }
                        }


                        // LOGGING CONFIG
                        pjsua.logging_config_default(logCfg);

                        if (ServicePrefs.DIAL_RETAIL) {
                            logCfg.setMsg_logging(pjsuaConstants.PJ_FALSE);
                            logCfg.setConsole_level(0);
                            logCfg.setLevel(0);
                        } else {
                            logCfg.setMsg_logging(prefsWrapper.getLogLevel() > 1 ? pjsuaConstants.PJ_TRUE : pjsuaConstants.PJ_FALSE);
                            logCfg.setConsole_level(prefsWrapper.getLogLevel());
                            logCfg.setLevel(prefsWrapper.getLogLevel());
                        }

                        // MEDIA CONFIG
                        pjsua.media_config_default(mediaCfg);

                        // For now only this cfg is supported
                        mediaCfg.setChannel_count(1);
                        //mediaCfg.setSnd_auto_close_time(prefsWrapper.getAutoCloseTime());
                        //mediaCfg.setSnd_auto_close_time(0);

                        // Echo cancellation
                        mediaCfg.setEc_tail_len(prefsWrapper.getEchoCancellationTail());
                        mediaCfg.setEc_options(prefsWrapper.getEchoMode());
                        mediaCfg.setNo_vad(prefsWrapper.getNoVad());
                        mediaCfg.setQuality(prefsWrapper.getMediaQuality());
                        mediaCfg.setClock_rate(prefsWrapper.getClockRate());
                        mediaCfg.setSnd_clock_rate(prefsWrapper.getClockRate());
                        mediaCfg.setAudio_frame_ptime(prefsWrapper.getAudioFramePtime());
                        mediaCfg.setHas_ioqueue(prefsWrapper.getHasIOQueue());
                        //mediaCfg.setRx_drop_pct(10);
                        //mediaCfg.setTx_drop_pct(10);


                        //mediaCfg.setSnd_auto_close_time(-1);
                        //mediaCfg.setSnd_play_latency(150);
                        //mediaCfg.setSnd_rec_latency(150);
						
						/*
						mediaCfg.setJb_init(100);
						mediaCfg.setJb_min_pre(0);
						mediaCfg.setJb_max_pre(500);
						mediaCfg.setJb_max(500);
						*/

                        mediaCfg.setJb_max(1000);

                        // ICE
                        mediaCfg.setEnable_ice(prefsWrapper.getIceEnabled());
                        // TURN
                        int isTurnEnabled = prefsWrapper.getTurnEnabled();
                        if (isTurnEnabled == 1) {
                            mediaCfg.setEnable_turn(isTurnEnabled);
                            mediaCfg.setTurn_server(pjsua.pj_str_copy(prefsWrapper.getTurnServer()));
                        }

                        cssCfg.setTcp_keep_alive_interval(30);
                        cssCfg.setTls_keep_alive_interval(30);

                        cssCfg.setTsx_t1_timeout(2000);
                        cssCfg.setTsx_t2_timeout(4000);
                        cssCfg.setTsx_t4_timeout(5000);
						/*cssCfg.setTsx_t1_timeout(4000);
						cssCfg.setTsx_t2_timeout(8000);
						cssCfg.setTsx_t4_timeout(8000); */ //sgkim 2017.09.21
                        cssCfg.setTsx_td_timeout(32000);
						
						/*
						if (ServicePrefs.mTransport == 2)
							cssCfg.setUse_compact_form_headers(1);
							*/

                        // INITIALIZE
                        status = pjsua.csipsimple_init(cfg, logCfg, mediaCfg, cssCfg, service);
                        if (status != pjsuaConstants.PJ_SUCCESS) {
                            String msg = "Fail to init pjsua " + pjsua.get_error_message(status).getPtr();
                            Log.e(THIS_FILE, msg);
                            service.notifyUserOfMessage(msg);
                            pjsua.csipsimple_destroy(0);
                            created = false;
                            creating = false;

                            FirebaseCrashlytics.getInstance().recordException(new Exception("PjService pjsua.csipsimple_init status :"+status));

                            return false;
                        }

                        String rtt_ip = ServicePrefs.mRTT_IP;
                        int rtt_port = ServicePrefs.mRTT_PORT;

                        DebugLog.d("RTT rtt ip   -> "+rtt_ip);
                        DebugLog.d("RTT rtt port -> "+rtt_port);

                        FirebaseCrashlytics.getInstance().setCustomKey("rtt_ip", rtt_ip);
                        FirebaseCrashlytics.getInstance().setCustomKey("rtt_port", rtt_port);
                        FirebaseCrashlytics.getInstance().setCustomKey("is_stun_enabled", isStunEnabled);
                        FirebaseCrashlytics.getInstance().setCustomKey("stun_server", prefsWrapper.getStunServer());


                        // FOR RTT TEST => BJH 2017-08-24 향후 배포시에는 이 부분은 주석처리 아이폰과 동일
						/*if (rtt_port == 0) {
							rtt_ip = "211.253.29.1";
							rtt_port = 39999;
						}*/

                        // INIT RTT
                        if (rttReceiver == null)
                            rttReceiver = new UARttReceiver(this.service);
                        int rttHandlerResult = pjsua.mobile_rtt_handler_init();

                        FirebaseCrashlytics.getInstance().setCustomKey("rtt_handler_result", rttHandlerResult);

                        if (rtt_port != 0 && rtt_ip != null && rtt_ip.length() > 0) {
                            int setInfoResult = pjsua.mobile_rtt_handler_set_info(rtt_ip, rtt_port); // by sgkim : 2018-08-18
                            FirebaseCrashlytics.getInstance().setCustomKey("rtt_handler_set_info_result", setInfoResult);
                        } else {
                            FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService Rtt handler set info failed."));
                        }

                        pjsua.mobile_rtt_handler_set_callback(rttReceiver);
                    }

                    // Add transports
                    {
                        if (ServicePrefs.mTransport == 0) {
                            // UDP
                            if (prefsWrapper.isUDPEnabled()) {
                                pjsip_transport_type_e t = pjsip_transport_type_e.PJSIP_TRANSPORT_UDP;
                                if (prefsWrapper.useIPv6()) {
                                    t = pjsip_transport_type_e.PJSIP_TRANSPORT_UDP6;
                                }
                                udpTranportId = createTransport(t, prefsWrapper.getUDPTransportPort());
                                FirebaseCrashlytics.getInstance().setCustomKey("udp_transport", prefsWrapper.getUDPTransportPort());

                                DebugLog.d("udpTranportId --> "+udpTranportId);

                                if (udpTranportId == null) {
                                    pjsua.csipsimple_destroy(0);
                                    creating = false;
                                    created = false;

                                    FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService udp transport is null."));

                                    return false;
                                }
                                //int[] p_acc_id = new int[1];
                                //pjsua.acc_add_local(udpTranportId, pjsua.PJ_FALSE,p_acc_id);
                                // Log.d(THIS_FILE, "Udp account "+p_acc_id);

                            }
                        } else if (ServicePrefs.mTransport == 1) {
                            // TCP
                            if (prefsWrapper.isTCPEnabled() && !prefsWrapper.useIPv6()) {
                                pjsip_transport_type_e t = pjsip_transport_type_e.PJSIP_TRANSPORT_TCP;
                                if (prefsWrapper.useIPv6()) {
                                    t = pjsip_transport_type_e.PJSIP_TRANSPORT_TCP6;
                                }
                                tcpTranportId = createTransport(t, prefsWrapper.getTCPTransportPort());
                                FirebaseCrashlytics.getInstance().setCustomKey("tcp_transport", prefsWrapper.getTCPTransportPort());
                                if (tcpTranportId == null) {
                                    pjsua.csipsimple_destroy(0);
                                    creating = false;
                                    created = false;

                                    FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService tcp transport is null."));

                                    return false;
                                }
                            }

                        } else if (ServicePrefs.mTransport == 2) {
                            // TLS
                            if (prefsWrapper.isTLSEnabled()) {
                                tlsTransportId = createTransport(pjsip_transport_type_e.PJSIP_TRANSPORT_TLS, prefsWrapper.getTLSTransportPort());
                                FirebaseCrashlytics.getInstance().setCustomKey("tls_transport", prefsWrapper.getTCPTransportPort());

                                if (tlsTransportId == null) {
                                    pjsua.csipsimple_destroy(0);
                                    creating = false;
                                    created = false;

                                    FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService tls transport is null."));

                                    return false;
                                }
                                // int[] p_acc_id = new int[1];
                                // pjsua.acc_add_local(tlsTransportId, pjsua.PJ_FALSE,
                                // p_acc_id);
                            }
                        }

                    }

                    // Initialization is done, now start pjsua
                    status = pjsua.start();

                    if (status != pjsua.PJ_SUCCESS) {
                        String msg = "Fail to start pjsip  " + pjsua.get_error_message(status).getPtr();
                        DebugLog.d("test_v2 START Pjsua.start() failed. msg: "+pjsua.get_error_message(status).getPtr());

                        FirebaseCrashlytics.getInstance().setCustomKey("pjsua_status", msg);

                        Log.e(THIS_FILE, msg);
                        service.notifyUserOfMessage(msg);
                        pjsua.csipsimple_destroy(0);
                        creating = false;
                        created = false;

                        FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService pjsua.start() status is failed. message: "+msg));

                        return false;
                    } else {
                        DebugLog.d("test_v2 START pjsua.start() success.");
//                        FirebaseCrashlytics.getInstance().recordException(new Exception("pjsua.start success."));
//                        FirebaseCrashlytics.getInstance().recordException(new Exception("PjSipService pjsua.start() status is success."));
                    }

                    // Init media codecs
                    initCodecs();
                    setCodecsPriorities();

                    created = true;

                    // Add accounts
                    creating = false;

                    return true;
                }
            }

        } catch (IllegalMonitorStateException e) {
            Log.e(THIS_FILE, "Not able to start sip right now", e);
            creating = false;
            created = false;

            FirebaseCrashlytics.getInstance().recordException(e);
        }

        return false;
    }


    /**
     * Stop sip service
     */
    public synchronized void sipStop() {
        DebugLog.d("PjSipService sip stop will stop sip...");
        DebugLog.d("PjSipService sip stop will stop sip creatingSipStack --> "+creatingSipStack);
        DebugLog.d("PjSipService sip stop will stop sip created ---> "+created);
        DebugLog.d("PjSipService sip stop will stop sip pjAccountsCreationLock --> ");

        if (getActiveCallInProgress() != null) {
            Log.e(THIS_FILE, "We have a call in progress... DO NOT STOP !!!");
            //TODO : queue quit on end call;
            //return;
        }

        if(getIncomingCallInProgress() != null) {
            DebugLog.d("PjSipService we have a call in progress... DO NOT STOP !!!");
        }

        if (service.notificationManager != null) {
            //service.notificationManager.cancelRegisters();
            service.notificationManager.notifyRegisteredAccounts(null);
        }

        synchronized (creatingSipStack) {
            if (created) {
                Log.d(THIS_FILE, "Detroying...");
                // This will destroy all accounts so synchronize with accounts
                // management lock
                synchronized (pjAccountsCreationLock) {

                    pjsua.csipsimple_destroy(0);
                    synchronized (activeAccountsLock) {
                        accountsAddingStatus.clear();
                        activeAccounts.clear();
                    }
                }
                if (userAgentReceiver != null) {
                    userAgentReceiver.stopService();
                    userAgentReceiver = null;
                }

                if (mediaManager != null) {
                    mediaManager.stopService();
                    mediaManager = null;
                }

                TimerWrapper.destroy();

                created = false;
            }
        }

        Log.i(THIS_FILE, ">> Media m " + mediaManager);
    }


    //	private boolean copyResourceFile(int id, String dest) throws IOException
//	{
//		// CHECK FILE
//		File file = new File(dest);
//		if(file.exists()) return true;
//		
//		// COPY FILE
//	    InputStream myInput = service.getApplicationContext().getResources().openRawResource(id);
//    	OutputStream myOutput = new FileOutputStream(dest);
//
//    	// COPY
//    	byte[] buffer = new byte[1024];
//    	int length;
//    	while ((length = myInput.read(buffer))>0){
//    		myOutput.write(buffer, 0, length);
//    	}
// 
//    	//Close the streams
//    	myOutput.flush();
//    	myOutput.close();
//    	myInput.close();
//    	
//    	return true;
//	}
    private boolean checkCert(String dirName) {
        // 인증서를 포함하고 있으면 스토어 등록이 안된다.
        // 다은 방법을 연구해야 한다.
		/*
		try {
			copyResourceFile(R.raw.cert, dirName + "cert.pem");
			copyResourceFile(R.raw.key, dirName + "key.pem");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}		
		return true;
		*/
        return false;
    }


    /**
     * Utility to create a transport
     *
     * @return transport id or -1 if failed
     */
    private Integer createTransport(pjsip_transport_type_e type, int port) {
        pjsua_transport_config cfg = new pjsua_transport_config();
        int[] tId = new int[1];
        int status;
        pjsua.transport_config_default(cfg);
        cfg.setPort(port);

        if (type.equals(pjsip_transport_type_e.PJSIP_TRANSPORT_TLS)) {
            pjsip_tls_setting tlsSetting = cfg.getTls_setting();

            // CHECK RESOURCE FILE

            File dir = PreferencesWrapper.getConfigFolder();
            String dirName = dir.getAbsoluteFile() + File.separator;
            if (checkCert(dirName)) {
                String certFile = dirName + "cert.pem";
                String privKey = dirName + "key.pem";
                String tlsPwd = "1234";

                if (!TextUtils.isEmpty(certFile)) {
                    tlsSetting.setCert_file(pjsua.pj_str_copy(certFile));
                }
                if (!TextUtils.isEmpty(privKey)) {
                    tlsSetting.setPrivkey_file(pjsua.pj_str_copy(privKey));
                }
                if (!TextUtils.isEmpty(tlsPwd)) {
                    tlsSetting.setPassword(pjsua.pj_str_copy(tlsPwd));
                }
            }
			
			
			/*
			String serverName = prefsWrapper.getPreferenceStringValue(PreferencesWrapper.TLS_SERVER_NAME);
			if (!TextUtils.isEmpty(serverName)) {
				tlsSetting.setServer_name(pjsua.pj_str_copy(serverName));
			}
			String caListFile = prefsWrapper.getPreferenceStringValue(PreferencesWrapper.CA_LIST_FILE);
			if (!TextUtils.isEmpty(caListFile)) {
				tlsSetting.setCa_list_file(pjsua.pj_str_copy(caListFile));
			}
			String certFile = prefsWrapper.getPreferenceStringValue(PreferencesWrapper.CERT_FILE);
			if (!TextUtils.isEmpty(certFile)) {
				tlsSetting.setCert_file(pjsua.pj_str_copy(certFile));
			}
			String privKey = prefsWrapper.getPreferenceStringValue(PreferencesWrapper.PRIVKEY_FILE);
			if (!TextUtils.isEmpty(privKey)) {
				tlsSetting.setPrivkey_file(pjsua.pj_str_copy(privKey));
			}
			String tlsPwd = prefsWrapper.getPreferenceStringValue(PreferencesWrapper.TLS_PASSWORD);
			if (!TextUtils.isEmpty(tlsPwd)) {
				tlsSetting.setPassword(pjsua.pj_str_copy(tlsPwd));
			}
			boolean checkClient = prefsWrapper.getPreferenceBooleanValue(PreferencesWrapper.TLS_VERIFY_CLIENT);
			tlsSetting.setVerify_client(checkClient ? 1 : 0);
			
			*/

            tlsSetting.setMethod(0);
            boolean checkServer = false;
            tlsSetting.setVerify_server(checkServer ? 1 : 0);

            boolean checkClient = false;
            tlsSetting.setVerify_client(checkClient ? 1 : 0);

            cfg.setTls_setting(tlsSetting);
        }

        {
            Log.d(THIS_FILE, "Activate qos for this transport");
            pj_qos_params qosParam = cfg.getQos_params();
            qosParam.setDscp_val((short) prefsWrapper.getDSCPVal());
            qosParam.setFlags((short) 1); //DSCP
            cfg.setQos_params(qosParam);
        }


        // CREATE TRANSPORT LAYER : RANDOM
        //status = pjsua.transport_create(type, cfg, tId);
        status = -1; // FOR RANDOM
        if (status != pjsuaConstants.PJ_SUCCESS) {
            Random rnd = new Random(System.currentTimeMillis());
            port += (6060 + rnd.nextInt(1000)); // 6060 ~  7060 사이.

            // RETRY
            for (int i = 0; i < 100; i++) {
                port++;
                if ((port % 2) != 0) port++; // 짝수만.

                cfg.setPort(port);
                Log.d(THIS_FILE, "transport_create retry port : " + port);

                status = pjsua.transport_create(type, cfg, tId);
                if (status != pjsuaConstants.PJ_SUCCESS) {
                    continue;
                }
                Log.d(THIS_FILE, "transport_create port : " + port);
                break;
            }
        }

        if (status != pjsuaConstants.PJ_SUCCESS) {
            String errorMsg = pjsua.get_error_message(status).getPtr();
            String msg = "Fail to create transport " + errorMsg + " (" + status + ")";
            Log.e(THIS_FILE, msg);
            if (status == 120098) { /* Already binded */
                msg = service.getString(R.string.another_application_use_sip_port);
            }
            service.notifyUserOfMessage(msg);
            return null;
        }

        return tId[0];
    }

    public int unRegistAccount(SipProfile profile) {
        int status = 0;

        PjSipAccount account = new PjSipAccount(profile);
        Integer currentAccountId = null;
        synchronized (activeAccountsLock) {
            currentAccountId = activeAccounts.get(account.id);
        }

        if(currentAccountId!=null) {
            status = pjsua.acc_set_registration(currentAccountId, 0);
        }

        return status;
    }


    public boolean addAccount(SipProfile profile) {
        int status = pjsuaConstants.PJ_FALSE;
        synchronized (pjAccountsCreationLock) {
            if (!created) {
                Log.e(THIS_FILE, "PJSIP is not started here, nothing can be done");
                return status == pjsuaConstants.PJ_SUCCESS;

            }
            PjSipAccount account = new PjSipAccount(profile);
            account.applyExtraParams(service);

            Integer currentAccountId = null;
            synchronized (activeAccountsLock) {
                currentAccountId = activeAccounts.get(account.id);
            }

            // Force the use of a transport
            switch (account.transport) {
                case SipProfile.TRANSPORT_UDP:
                    if (udpTranportId != null) {
                        account.cfg.setTransport_id(udpTranportId);
                    }
                    break;
                case SipProfile.TRANSPORT_TCP:
                    if (tcpTranportId != null) {
                        //account.cfg.setTransport_id(tcpTranportId);
                    }
                    break;
                case SipProfile.TRANSPORT_TLS:
                    if (tlsTransportId != null) {
                        //account.cfg.setTransport_id(tlsTransportId);
                    }
                    break;
                default:
                    break;
            }

            DebugLog.d("udp transport --> "+udpTranportId);

            if (currentAccountId != null) {
                status = pjsua.acc_modify(currentAccountId, account.cfg);
                synchronized (activeAccountsLock) {
                    accountsAddingStatus.put(account.id, status);
                }
                if (status == pjsuaConstants.PJ_SUCCESS) {
                    status = pjsua.acc_set_registration(currentAccountId, 1);
                    if (status == pjsuaConstants.PJ_SUCCESS) {
                        pjsua.acc_set_online_status(currentAccountId, 1);
                    }
                }
            } else {
                int[] accId = new int[1];
                if (account.cfg.getReg_uri().getPtr().equals("localhost")) {
                    account.cfg.setReg_uri(pjsua.pj_str_copy(""));
                    account.cfg.setProxy_cnt(0);
                    status = pjsua.acc_add_local(udpTranportId, pjsuaConstants.PJ_FALSE, accId);
                } else {
                    status = pjsua.acc_add(account.cfg, pjsuaConstants.PJ_FALSE, accId);

                }
                synchronized (activeAccountsLock) {
                    accountsAddingStatus.put(account.id, status);
                    if (status == pjsuaConstants.PJ_SUCCESS) {
                        activeAccounts.put(account.id, accId[0]);
                        pjsua.acc_set_online_status(accId[0], 1);
                    }
                }
            }

        }

        return status == pjsuaConstants.PJ_SUCCESS;
    }

    public boolean updateAccount(SipProfile profile) {
        Integer currentAccountId = null;
        int status = pjsuaConstants.PJ_FALSE;
        synchronized (pjAccountsCreationLock) {
            if (!created) {
                Log.e(THIS_FILE, "PJSIP is not started here, nothing can be done");
                return status == pjsuaConstants.PJ_SUCCESS;

            }
            synchronized (activeAccountsLock) {
                currentAccountId = activeAccounts.get(profile.id);
            }
            if (currentAccountId == null) return false;

            status = pjsua.acc_set_registration(currentAccountId, 1);
            if (status != pjsuaConstants.PJ_SUCCESS) return false;

            pjsua.acc_set_online_status(currentAccountId, 1);
        }

        return true;
    }


    @SuppressWarnings("unchecked")
    public ArrayList<SipProfileState> getAndUpdateActiveAccounts() {
        SipProfileState info;
        Set<Integer> activeAccountsClone;
        synchronized (activeAccountsLock) {
            activeAccountsClone = ((HashMap<Integer, Integer>) activeAccounts.clone()).keySet();
        }

        ArrayList<SipProfileState> activeAccountsInfos = new ArrayList<SipProfileState>();
        for (int accountDbId : activeAccountsClone) {
            info = service.getSipProfileState(accountDbId);
            if (info != null) {
                Log.d(THIS_FILE, "ACCOUNT " + accountDbId + " : STATUS : " + info.getStatusCode() + ", EXP : " + info.getExpires());

                if (info.getWizard().equalsIgnoreCase("LOCAL")) {
                    activeAccountsInfos.add(info);
                } else {
                    if (info.getExpires() >= 0 && info.getStatusCode() <= SipCallSession.StatusCode.OK) {
                        activeAccountsInfos.add(info);
                    }
                }
            }
        }
        Collections.sort(activeAccountsInfos, SipProfileState.getComparator());
        return activeAccountsInfos;
    }


    public SipProfileState getAccountInfo(SipProfile account) {
        if (!created || account == null) {
            return null;
        }
        SipProfileState accountInfo;

        Integer activeAccountStatus = null;
        Integer activeAccountPjsuaId = null;
        synchronized (activeAccountsLock) {
            activeAccountStatus = accountsAddingStatus.get(account.id);
            if (activeAccountStatus != null) {
                activeAccountPjsuaId = activeAccounts.get(account.id);
            }
        }


        accountInfo = new SipProfileState(account);
        if (activeAccountStatus != null) {
            accountInfo.setAddedStatus(activeAccountStatus);
            if (activeAccountPjsuaId != null) {
                accountInfo.setPjsuaId(activeAccountPjsuaId);
                pjsua_acc_info pjAccountInfo = new pjsua_acc_info();
                // Log.d(THIS_FILE,
                // "Get account info for account id "+accountDbId+" ==> (active within pjsip as) "+activeAccounts.get(accountDbId));
                int success = pjsua.acc_get_info(activeAccountPjsuaId, pjAccountInfo);
                if (success == pjsuaConstants.PJ_SUCCESS) {
                    //Log.d(THIS_FILE, "GET : ACCOUNT " + activeAccountPjsuaId + " : STATUS : " + pjAccountInfo.getStatus().swigValue());
                    try {
                        //Should be fine : status code are coherent with RFC status codes
                        // by sgkim : 2011-04-04 : pjsua.acc_get_info 호출 결과가 200, 100 순서로 바뀌어 오는 경우가 있다.
                        int status = pjAccountInfo.getStatus().swigValue();
                        if (status < SipCallSession.StatusCode.OK && account.getLastStatus() == SipCallSession.StatusCode.OK) {
                            Log.d(THIS_FILE, "ACCOUNT " + activeAccountPjsuaId + " : STATUS : " + status + " -> FIX");
                            status = SipCallSession.StatusCode.OK;
                        } else {
                            account.setLastStatus(status);
                        }

                        accountInfo.setStatusCode(status);
                    } catch (IllegalArgumentException e) {
                        accountInfo.setStatusCode(SipCallSession.StatusCode.INTERNAL_SERVER_ERROR);
                    }
                    accountInfo.setStatusText(pjAccountInfo.getStatus_text().getPtr());
                    accountInfo.setExpires(pjAccountInfo.getExpires());

                }
            }
        }

        return accountInfo;
    }


    private static ArrayList<String> codecs = new ArrayList<String>();
    private static boolean codecs_initialized = false;

    public static void resetCodecs() {
        synchronized (codecs) {
            if (codecs_initialized) {
                codecs.clear();
                codecs_initialized = false;
            }
        }
    }

    private void initCodecs() {
        synchronized (codecs) {
            if (!codecs_initialized) {
                codecs_initialized = true;

                int nbrCodecs = pjsua.codecs_get_nbr();
                Log.d(THIS_FILE, "Codec nbr : " + nbrCodecs);
                for (int i = 0; i < nbrCodecs; i++) {
                    String codecId = pjsua.codecs_get_id(i).getPtr();
                    codecs.add(codecId);
                    Log.d(THIS_FILE, "Added codec " + codecId);
                }
            }
        }
    }

    public void setCodecsPriorities() {
        synchronized (codecs) {
            if (codecs_initialized) {
                String CodecList[] = {"PCMU", "PCMA", "G722/", "GSM", "G729", "ILBC", "speex/16000", "SILK/16000", "SILK/24000", "ISAC/16000", "opus/16000"};
//                short CodecPriority[] = {250, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                short CodecPriority[] = {250, 240, 0, 0, 230, 0, 0, 0, 0, 0, 0};

                if (ServicePrefs.DIAL_CODEC_G729) {
                    if (prefsWrapper.getMediaCodec().equals("G729")) {
                        // SET G.729
                        Log.d(THIS_FILE, "SET G729 !!");
                        CodecPriority[4] = 230;
                    }
                }

//                if (ServicePrefs.DIAL_CODEC_OPUS) {
//                    CodecPriority[10] = 250;
//                }

                if (ServicePrefs.DIAL_CODEC_PCMU) {
                    CodecPriority[0] = 250;
                }

                for (String codec : codecs) {
                    pjsua.codec_set_priority(pjsua.pj_str_copy(codec), (short) 0); // DISABLE
                    int len = CodecList.length;
                    for (int i = 0; i < len; i++) {
                        String Key = CodecList[i];
                        if (codec.startsWith(Key)) {
                            short priority = CodecPriority[i];

                            DebugLog.d("test_v2 code_priority "+codec+" : "+priority);

                            pjsua.codec_set_priority(pjsua.pj_str_copy(codec), priority);
                            break;
                        }
                    }
                }
            }
        }
    }


    // Call related

    /**
     * Answer a call
     *
     * @param callId
     *            the id of the call to answer to
     * @param code
     *            the status code to send in the response
     * @return
     */
    public int callAnswer(int callId, int code) {

        if (created) {
            synchronized (callActionLock) {
                return pjsua.call_answer(callId, code, null, null);
            }
        }
        return -1;
    }

    /**
     * Hangup a call
     *
     * @param callId
     *            the id of the call to hangup
     * @param code
     *            the status code to send in the response
     * @return
     */
    public int callHangup(int callId, int code) {
        if (created) {
            synchronized (callActionLock) {
                if (callId == -1) {
                    pjsua.call_hangup_all();
                    return pjsua.PJ_SUCCESS;
                } else {
                    return pjsua.call_hangup(callId, code, null, null);
                }
            }
        }
        return -1;
    }

    public int callXfer(int callId, String callee) {
        if (created) {
            synchronized (callActionLock) {

                ToCall toCall = sanitizeSipUri(callee, 0);
                pj_str_t uri;
                if (toCall != null) {
                    uri = pjsua.pj_str_copy(toCall.getCallee());
                    Log.d(THIS_FILE,"callXfer toCall != null");
                }else{
                    uri = pjsua.pj_str_copy(callee);
                }

                //return pjsua.call_xfer(callId, pjsua.pj_str_copy(callee), null);
                return pjsua.call_xfer(callId, uri, null);
            }
        }
        return -1;
    }

    public int callXferReplace(int callId, int otherCallId, int options) {
        if (created) {
            synchronized (callActionLock) {
                return pjsua.call_xfer_replaces(callId, otherCallId, options, null);
            }
        }
        return -1;
    }

    /**
     * Make a call
     *
     * @param callee
     *            remote contact ot call If not well formated we try to add
     *            domain name of the default account
     */
    public int makeCall(String callee, int accountId) {
        if (!created) {
            return -1;
        }

        // check id
        pjsua_acc_info accountInfo = new pjsua_acc_info();
        int success = pjsua.acc_get_info(accountId, accountInfo);
        if (success == pjsuaConstants.PJ_SUCCESS) {
            int online = accountInfo.getOnline_status();
            if (online == 0) {
                service.notifyUserOfMessage(service.getString(R.string.service_not_connected));
                return -1;
            }
        } else {
            service.notifyUserOfMessage(service.getString(R.string.service_not_connected));
            return -1;
        }

        ToCall toCall = sanitizeSipUri(callee, accountId);
        Log.d(THIS_FILE,"toCall Callee uri:"+toCall.getCallee());
        if (toCall != null) {

            pj_str_t uri = pjsua.pj_str_copy(toCall.getCallee());

            // Nothing to do with this values
            byte[] userData = new byte[1];
            int[] callId = new int[1];

            pjsua_call_setting cs = new pjsua_call_setting();
            pjsua.call_setting_default(cs);
            cs.setAud_cnt(1);
            cs.setVid_cnt(0);
            cs.setFlag(0);

            try {
                int status = pjsua.call_make_call((int) toCall.getPjsipAccountId(), uri, cs, userData, null, callId);
                if (status != pjsuaConstants.PJ_SUCCESS) {
                    //
                }
                return status;
            } catch (Exception e) {
                return -1;
            }
        } else {
            service.notifyUserOfMessage(service.getString(R.string.invalid_sip_uri) + " : " + callee);
        }
        return -1;
    }


    public String getKeyCode(int keyCode) {
        String str = "";
        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
                str = "0";
                break;
            case KeyEvent.KEYCODE_1:
                str = "1";
                break;
            case KeyEvent.KEYCODE_2:
                str = "2";
                break;
            case KeyEvent.KEYCODE_3:
                str = "3";
                break;
            case KeyEvent.KEYCODE_4:
                str = "4";
                break;
            case KeyEvent.KEYCODE_5:
                str = "5";
                break;
            case KeyEvent.KEYCODE_6:
                str = "6";
                break;
            case KeyEvent.KEYCODE_7:
                str = "7";
                break;
            case KeyEvent.KEYCODE_8:
                str = "8";
                break;
            case KeyEvent.KEYCODE_9:
                str = "9";
                break;
            case KeyEvent.KEYCODE_POUND:
                str = "#";
                break;
            case KeyEvent.KEYCODE_STAR:
                str = "*";
                break;
        }
        return str;
    }


    public int sendDtmf(int callId, int keyCode) {
        if (!created) {
            return -1;
        }
        int res = -1;

        //KeyCharacterMap km = KeyCharacterMap.load(KeyCharacterMap.NUMERIC);
        //String keyPressed = String.valueOf(km.getNumber(keyCode));
        String keyPressed = getKeyCode(keyCode);
        Log.d(THIS_FILE, "sendDtmf : " + keyPressed);
        if (keyPressed == null || keyPressed.length() == 0) return -1;


        pj_str_t pjKeyPressed = pjsua.pj_str_copy(keyPressed);
        synchronized (callActionLock) {
            if (prefsWrapper.useSipInfoDtmf()) {
                DebugLog.d("test_v2 dtmf_condition_1");


                res = pjsua.send_dtmf_info(callId, pjKeyPressed);
                Log.d(THIS_FILE, "Has been sent DTMF INFO : " + res);
            } else {
                DebugLog.d("test_v2 dtmf_condition_2");

                if (!prefsWrapper.forceDtmfInBand()) {
                    DebugLog.d("test_v2 dtmf_condition_3");
                    //Generate using RTP
                    res = pjsua.call_dial_dtmf(callId, pjKeyPressed);
                    Log.d(THIS_FILE, "Has been sent in RTP DTMF : " + res);
                }

                if (res != pjsua.PJ_SUCCESS && !prefsWrapper.forceDtmfRTP()) {
                    DebugLog.d("test_v2 dtmf_condition_4");
                    //Generate using analogic inband
                    if (dialtoneGenerator == null) {
                        dialtoneGenerator = new PjStreamDialtoneGenerator();
                    }
                    res = dialtoneGenerator.sendPjMediaDialTone(callId, keyPressed);
                    Log.d(THIS_FILE, "Has been sent DTMF analogic : " + res);
                }
            }
        }
        return res;
    }


    public int sendDtmf2(int callId, String keyPressed) {
        if (!created) {
            return -1;
        }
        int res = -1;

        Log.d(THIS_FILE, "sendDtmf : " + keyPressed);
        if (keyPressed == null || keyPressed.length() == 0) return -1;

        pj_str_t pjKeyPressed = pjsua.pj_str_copy(keyPressed);
        synchronized (callActionLock) {
            if (prefsWrapper.useSipInfoDtmf()) {
                DebugLog.d("test_v2 dtmf_condition_1");
                res = pjsua.send_dtmf_info(callId, pjKeyPressed);
                Log.d(THIS_FILE, "Has been sent DTMF INFO : " + res);
            } else {
                DebugLog.d("test_v2 dtmf_condition_2");
                if (!prefsWrapper.forceDtmfInBand()) {
                    DebugLog.d("test_v2 dtmf_condition_3");
                    //Generate using RTP
                    res = pjsua.call_dial_dtmf(callId, pjKeyPressed);
                    Log.d(THIS_FILE, "Has been sent in RTP DTMF : " + res);
                }

                if (res != pjsua.PJ_SUCCESS && !prefsWrapper.forceDtmfRTP()) {
                    DebugLog.d("test_v2 dtmf_condition_4");
                    //Generate using analogic inband
                    if (dialtoneGenerator == null) {
                        dialtoneGenerator = new PjStreamDialtoneGenerator();
                    }
                    res = dialtoneGenerator.sendPjMediaDialTone(callId, keyPressed);
                    Log.d(THIS_FILE, "Has been sent DTMF analogic : " + res);
                }
            }
        }
        return res;
    }


    /**
     * Send sms/message using SIP server
     */
    public ToCall sendMessage(String callee, String message, int accountId) {
        if (!created) {
            return null;
        }


        ToCall toCall = sanitizeSipUri(callee, accountId);
        if (toCall != null) {

            pj_str_t uri = pjsua.pj_str_copy(toCall.getCallee());
            pj_str_t text = pjsua.pj_str_copy(message);
            Log.d(THIS_FILE, "get for outgoing");
            if (accountId == -1) {
                accountId = pjsua.acc_find_for_outgoing(uri);
            }

            // Nothing to do with this values
            byte[] userData = new byte[1];

            int status = pjsua.im_send(toCall.getPjsipAccountId(), uri, null, text, null, userData);
            return (status == pjsuaConstants.PJ_SUCCESS) ? toCall : null;
        }
        return toCall;
    }


    public void stopDialtoneGenerator() {
        if (dialtoneGenerator != null) {
            dialtoneGenerator.stopDialtoneGenerator();
            dialtoneGenerator = null;
        }
    }

    public int callHold(int callId) {
        if (created) {
            synchronized (callActionLock) {
                isHold = true;
                return pjsua.call_set_hold(callId, null);
            }
        }
        return -1;
    }

    public int callReinvite(int callId, boolean unhold) {
        if (created) {
            synchronized (callActionLock) {
                isHold = false;
                return pjsua.call_reinvite(callId, unhold ? 9 : 1, null);
            }
        }
        return -1;
    }

    public int callReinvite2(int callId, int options) {
        if (created) {
            synchronized (callActionLock) {
                isHold = false;
                return pjsua.call_reinvite(callId, options, null);
            }
        }
        return -1;
    }


    public SipCallSession getCallInfo(int callId) {

        synchronized (creatingSipStack) {
            if (created/* && !creating */ && userAgentReceiver != null) {
                SipCallSession callInfo = userAgentReceiver.getCallInfo(callId, false);
                return callInfo;
            }
        }
        return null;
    }

    public void setBluetoothOn(boolean on) {
        if (created && mediaManager != null) {
            mediaManager.setBluetoothOn(on);
        }
    }

    public void setMicrophoneMute(boolean on) {
        if (created && mediaManager != null) {
            mediaManager.setMicrophoneMute(on);
        }
    }

    public void setSpeakerphoneOn(boolean on) {
        if (created && mediaManager != null) {
            mediaManager.setSpeakerphoneOn(on);
        }
    }

    public SipCallSession[] getCalls() {
        synchronized (creatingSipStack) {
            if (created && userAgentReceiver != null) {
                SipCallSession[] callsInfo = userAgentReceiver.getCalls();
                return callsInfo;
            }
        }
        return null;
    }

    public void confAdjustTxLevel(int port, float value) {
        if (created && userAgentReceiver != null) {
            pjsua.conf_adjust_tx_level(port, value);
            Log.d(THIS_FILE, "conf_adjust_tx_level : " + value);
        }
    }

    public void confAdjustRxLevel(int port, float value) {
        if (created && userAgentReceiver != null) {
            pjsua.conf_adjust_rx_level(port, value);
            Log.d(THIS_FILE, "conf_adjust_rx_level : " + value);
        }
    }


    public void setEchoCancellation(boolean on) {
        if (created && userAgentReceiver != null) {
            Log.d(THIS_FILE, "set echo cancelation " + on);
            pjsua.set_ec(on ? prefsWrapper.getEchoCancellationTail() : 0, prefsWrapper.getEchoMode());
        }
    }


    public void adjustStreamVolume(int stream, int direction, int flags) {
        if (mediaManager != null) {
            mediaManager.adjustStreamVolume(stream, direction, AudioManager.FLAG_SHOW_UI);
        }
    }

    public void startRecording(int callId) {
        if (created && userAgentReceiver != null) {
            userAgentReceiver.startRecording(callId);
        }
    }

    public void stopRecording() {
        if (created && userAgentReceiver != null) {
            userAgentReceiver.stopRecording();
        }
    }

    public int getRecordedCall() {
        if (created && userAgentReceiver != null) {
            return userAgentReceiver.getRecordedCall();
        }
        return -1;
    }

    public boolean canRecord(int callId) {
        if (created && userAgentReceiver != null) {
            return userAgentReceiver.canRecord(callId);
        }
        return false;
    }

    public boolean setAccountRegistration(SipProfile account, int renew) {
        int status = pjsuaConstants.PJ_FALSE;
        synchronized (pjAccountsCreationLock) {
            if (!created || account == null) {
                Log.e(THIS_FILE, "PJSIP is not started here, nothing can be done");
                return false;
            }
            if (activeAccounts.containsKey(account.id)) {
                int cAccId = activeAccounts.get(account.id);
                synchronized (activeAccountsLock) {
                    activeAccounts.remove(account.id);
                    accountsAddingStatus.remove(account.id);
                }

                if (renew == 1) {
                    pjsua.acc_set_online_status(cAccId, 1);
                    status = pjsua.acc_set_registration(cAccId, renew);
                } else {
                    // if(status == pjsuaConstants.PJ_SUCCESS && renew == 0) {
                    Log.d(THIS_FILE, "Delete account !!");
                    status = pjsua.acc_del(cAccId);
                }
            } else {
                if (renew == 1) {
                    addAccount(account);
                } else {
                    Log.w(THIS_FILE, "Ask to delete an unexisting account !!" + account.id);
                }

            }
        }
        return status == pjsuaConstants.PJ_SUCCESS;
    }


    @SuppressWarnings("unchecked")
    public SipProfile getAccountForPjsipId(int accId) {
        Set<Entry<Integer, Integer>> activeAccountsClone;
        synchronized (activeAccountsLock) {
            activeAccountsClone = ((HashMap<Integer, Integer>) activeAccounts.clone()).entrySet();
            // Quick quit
            if (!activeAccounts.containsValue(accId)) {
                return null;
            }
        }

        for (Entry<Integer, Integer> entry : activeAccountsClone) {
            if (entry.getValue().equals(accId)) {
                return service.getAccount(entry.getKey());
            }
        }

        return null;
    }

    public void setAudioInCall(int beforeInit) {
        if (mediaManager != null) {
            mediaManager.setAudioInCall(beforeInit == pjsuaConstants.PJ_TRUE);
        }
    }

    public void unsetAudioInCall() {

        if (mediaManager != null) {
            mediaManager.unsetAudioInCall();
        }
    }

    public SipCallSession getActiveCallInProgress() {
        synchronized (creatingSipStack) {
            if (created && userAgentReceiver != null) {
                return userAgentReceiver.getActiveCallInProgress();
            }
        }
        return null;
    }

    /**
     * ADDED
     */
    public SipCallSession getIncomingCallInProgress() {
        synchronized (creatingSipStack) {
            if(created && userAgentReceiver != null) {
                return userAgentReceiver.getIncomingCallInProgress();
            }
        }
        return null;
    }

    public void resetActiveCallSession() {
        synchronized (creatingSipStack) {
            if (created && userAgentReceiver != null) {
                userAgentReceiver.resetActiveCallSession();
            }
        }
    }


    // TO call utils

    private ToCall sanitizeSipUri(String callee, int accountId) {
        //accountId is the id in term of sip database
        //pjsipAccountId is the account id in term of pjsip adding
        int pjsipAccountId = SipProfile.INVALID_ID;

        // If this is an invalid account id
        if (accountId == SipProfile.INVALID_ID || !activeAccounts.containsKey(accountId)) {
            int defaultPjsipAccount = pjsua.acc_get_default();

            // If default account is not active
            if (!activeAccounts.containsValue(defaultPjsipAccount)) {
                for (Integer accId : activeAccounts.keySet()) {
                    // Use the first account as valid account
                    if (accId != null) {
                        accountId = accId;
                        pjsipAccountId = activeAccounts.get(accId);
                        break;
                    }
                }
            } else {
                // Use the default account
                for (Integer accId : activeAccounts.keySet()) {
                    if (activeAccounts.get(accId) == defaultPjsipAccount) {
                        accountId = accId;
                        pjsipAccountId = defaultPjsipAccount;
                        break;
                    }
                }
            }
        } else {
            //If the account is valid
            pjsipAccountId = activeAccounts.get(accountId);
        }

        if (pjsipAccountId == SipProfile.INVALID_ID) {
            Log.e(THIS_FILE, "Unable to find a valid account for this call");
            return null;
        }


        // Check integrity of callee field
        Pattern p = Pattern.compile("^.*(?:<)?(sip(?:s)?):([^@]*@[^>]*)(?:>)?$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(callee);

        if (!m.matches()) {
            // Assume this is a direct call using digit dialer

            Log.d(THIS_FILE, "default acc : " + accountId);
            SipProfile account = service.getAccount(accountId);
            String defaultDomain = account.getDefaultDomain();

            Log.d(THIS_FILE, "default domain : " + defaultDomain);
            p = Pattern.compile("^sip(s)?:[^@]*$", Pattern.CASE_INSENSITIVE);
            if (p.matcher(callee).matches()) {
                callee = "<" + callee + "@" + defaultDomain + ">";
            } else {
                //Should it be encoded?
                callee = "<sip:" + /*Uri.encode(*/callee/*)*/ + "@" + defaultDomain + ">";
            }
        } else {
            callee = "<" + m.group(1) + ":" + m.group(2) + ">";
        }

        Log.d(THIS_FILE, "will call " + callee);
        if (pjsua.verify_sip_url(callee) == 0) {
            //In worse worse case, find back the account id for uri.. but probably useless case
            if (pjsipAccountId == SipProfile.INVALID_ID) {
                pjsipAccountId = pjsua.acc_find_for_outgoing(pjsua.pj_str_copy(callee));
            }
            return new ToCall(pjsipAccountId, callee);
        }

        return null;
    }


    private void Sleep(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public int disconnectCallAndroid() {
        Runtime runtime = Runtime.getRuntime();
        int nResp = 0;
        try {
            Log.d(THIS_FILE, "service call phone 5 \n");
            runtime.exec("service call phone 5 \n");
        } catch (Exception exc) {
            Log.e(THIS_FILE, exc.getMessage());
            exc.printStackTrace();
        }
        return nResp;
    }

    public boolean killCall(Context context) {
        try {
            // Get the boring old TelephonyManager
            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);

            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =
                    Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);

        } catch (Exception ex) { // Many things can go wrong with reflection calls
            Log.d(THIS_FILE, "PhoneStateReceiver **" + ex.toString());
            return false;
        }
        return true;
    }


    public static int lastGSMState = TelephonyManager.CALL_STATE_IDLE;

    public void onGSMStateChanged(int state, String incomingNumber) {
        //Log.d(THIS_FILE, "GSMState : " + String.valueOf(state));

        lastGSMState = state;

        // Avoid ringing if new GSM state is not idle
        if (state != TelephonyManager.CALL_STATE_IDLE && mediaManager != null) {
            mediaManager.stopRing();
        }

        if (ServicePrefs.DIAL_AUTO_HANGUP) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                if (getActiveCallInProgress() != null) {
                    Executor eS = Executors.newSingleThreadExecutor();
                    eS.execute(new Runnable() {
                        @Override
                        public void run() {
                            //disconnectCallAndroid();
                            if (!killCall(service)) {
                                disconnectCallAndroid(); // retry
                            }

                            // RE-REG
                            if (SipService.currentService != null)
                                SipService.currentService.dataConnectionChanged();

                        }
                    });// code formatting with tohtml.com/java/
                    return;
                }
            } else {
                // SKIP
                return;
            }
        }


        // If new call state is not idle
        if (state != TelephonyManager.CALL_STATE_IDLE && userAgentReceiver != null) {
            SipCallSession currentActiveCall = userAgentReceiver.getActiveCallInProgress();
            if (currentActiveCall != null) {

                //currentActiveCall.setMediaStatus(MediaState.LOCAL_HOLD);

                //if(state != TelephonyManager.CALL_STATE_RINGING)
                if (hasBeenHoldByGSM == SipCallSession.INVALID_CALL_ID) {
                    //New state is not ringing nor idle... so off hook, hold current sip call
                    //Log.e(THIS_FILE, "HOLD");

                    final int _call_id = currentActiveCall.getCallId();

                    Log.e(THIS_FILE, "HOLD : " + String.valueOf(_call_id));
                    if (_call_id != SipCallSession.INVALID_CALL_ID) {
                        hasBeenHoldByGSM = _call_id;

                        Thread t = new Thread() {
                            public void run() {
								/*int status = */
                                callHold(_call_id);

                                AudioManager am = (AudioManager) service.getSystemService(Context.AUDIO_SERVICE);
                                am.setMode(AudioManager.MODE_IN_CALL);

                                // retry
								/*
								if (status > 0)
								{
									for(int i=0; i<10; i++)
									{
										Log.e(THIS_FILE, "HOLD RETRY : " + String.valueOf(i+1));
										status = callHold(_call_id);
										if (status <= 0) break;
										if (hasBeenHoldByGSM == SipCallSession.INVALID_CALL_ID) break;
										Sleep(500);
									}
								}
								*/
                            }

                            ;
                        };
                        if (t != null)
                            t.start();
                    }


                } else {
                    //We have a ringing incoming call.
                }
            }
        } else {
            //GSM is now back to an IDLE state, resume previously stopped SIP calls
            if (hasBeenHoldByGSM != SipCallSession.INVALID_CALL_ID && isCreated()) {
                //Log.e(THIS_FILE, "RESUME");

//				final int _callId = hasBeenHoldByGSM;
                hasBeenHoldByGSM = SipCallSession.INVALID_CALL_ID;

                Thread t = new Thread() {
                    public void run() {
                        isHold = false;
                        if (SipService.currentService != null)
                            SipService.currentService.dataConnectionChanged();

                        //Log.e(THIS_FILE, "setmode");
                        AudioManager am = (AudioManager) service.getSystemService(Context.AUDIO_SERVICE);
                        am.setMode(3);
                        Sleep(500);

                        //Log.e(THIS_FILE, "set_no_snd_dev");
                        pjsua.set_no_snd_dev();
                        Sleep(500);

                        //Log.e(THIS_FILE, "set_snd_dev");
                        pjsua.set_snd_dev(0, 0);

                        //Log.e(THIS_FILE, "callReinvite");
                        //callReinvite(_callId, true);
                        //isHold = false;
                    }

                    ;
                };
                if (t != null)
                    t.start();
            }
        }
    }


    public boolean detect_nat_type() {
        if (!created)
            return false;

        int status = pjsua.detect_nat_type();
        if (status != pjsuaConstants.PJ_SUCCESS)
            return false;
        return true;
    }
}
