/**
 * Copyright (C) 2010-2012 Regis Montoya (aka r3gis - www.r3gis.fr)
 * This file is part of CSipSimple.
 *
 *  CSipSimple is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  If you own a pjsip commercial license you can also redistribute it
 *  and/or modify it under the terms of the GNU Lesser General Public License
 *  as an android library.
 *
 *  CSipSimple is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with CSipSimple.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dial070.sip.utils.bluetooth;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothClass.Device;
import android.bluetooth.BluetoothClass.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;

import androidx.core.app.ActivityCompat;

import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

import java.util.List;
import java.util.Set;

@TargetApi(8)
public class BluetoothUtils8 extends BluetoothWrapper {

	private static final String THIS_FILE = "BT8";
	private AudioManager audioManager;
	
	private boolean isBluetoothConnected = false;
	
	private BroadcastReceiver mediaStateReceiver = new BroadcastReceiver() {
        @Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			DebugLog.d("Bt receiver action received --> "+intent.getAction());

//			IntentFilter filter = new IntentFilter();
//			filter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED);
//			filter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
//			filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
//			filter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);

			if(action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)) {
//				int headsetState = intent.getExtras().getInt("android.bluetooth.headset.extra.EXTRA_STATE");
				int headsetState = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE,  -1);
				DebugLog.d("BT receiver action headset state --> "+headsetState);

				btChangesListener.onBluetoothConnectionChanged(headsetState);

//				switch (headsetState) {
//					case 2:	// connected
//						DebugLog.d("BT action HEADSET_STATE_CHANGE state --> connected");
//						btChangesListener.onBluetoothConnectionChanged(2);
//						break;
//					case 1: // connecting
//						DebugLog.d("BT action HEADSET_STATE_CHANGE state --> connecting");
//						btChangesListener.onBluetoothConnectionChanged(1);
//						break;
//					case 0: // disconnect
//						DebugLog.d("BT action HEADSET_STATE_CHANGE state --> disconnected");
//						btChangesListener.onBluetoothConnectionChanged(0);
//						break;
//				}
			}

			if(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED.equals(action)) {
				Log.d(THIS_FILE, ">>> BT SCO state changed !!! ");
				DebugLog.d(">>> BT SCO state changed !!! ");

				int status = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, AudioManager.SCO_AUDIO_STATE_ERROR );
				Log.d(THIS_FILE, "BT SCO state changed : " + status + " target is " + targetBt);
				DebugLog.d("BT SCO state changed : " + status + " target is " + targetBt+" | setBluetoothScoOn(targetBt)");

				audioManager.setBluetoothScoOn(targetBt);

//				if(canBluetooth()) {
//					DebugLog.d("BT SCO request canBluetooth() targetValue --> true");
//					audioManager.setBluetoothScoOn(true);
//				} else {
//					DebugLog.d("BT SCO request canBluetooth() targetValue --> false");
//					audioManager.setBluetoothScoOn(targetBt);
//				}
				
				if(status == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
					DebugLog.d("AudioManager.SCO_AUDIO_STATE_CONNECTED");
					isBluetoothConnected = true;

				}else if(status == AudioManager.SCO_AUDIO_STATE_DISCONNECTED) {
					DebugLog.d("AudioManager.SCO_AUDIO_STATE_DISCONNECTED");
					isBluetoothConnected = false;
				}

				if(btChangesListener != null) {
				    btChangesListener.onBluetoothStateChanged(status);
				}
			}
		}
	};
	
	protected BluetoothAdapter bluetoothAdapter;

	@Override
	public void setContext(Context aContext){
		super.setContext(aContext);
		DebugLog.d("BT caller --> "+aContext.getPackageName());

		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if(bluetoothAdapter == null) {
    		try {
    			bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    		}catch(RuntimeException e) {
//    			Log.w(THIS_FILE, "Cant get default bluetooth adapter ", e);
				DebugLog.d("BT Can't get default bt adapter --> "+e.getMessage());
    		}
		}
	}
	
	public boolean canBluetooth() {
		// Detect if any bluetooth a device is available for call
		DebugLog.d("BT canBluetooth called");
		if (bluetoothAdapter == null) {
		    // Device does not support Bluetooth
			return false;
		}
		boolean hasConnectedDevice = false;
		//If bluetooth is on
		if(bluetoothAdapter.isEnabled()) {
			
			//We get all bounded bluetooth devices
			// bounded is not enough, should search for connected devices....

			if(ActivityCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
				return false;
			}
			Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
			for(BluetoothDevice device : pairedDevices) {
				BluetoothClass bluetoothClass = device.getBluetoothClass();
                if (bluetoothClass != null) {
                	int deviceClass = bluetoothClass.getDeviceClass();
                	if(bluetoothClass.hasService(Service.RENDER) ||
                		deviceClass == Device.AUDIO_VIDEO_WEARABLE_HEADSET ||
                		deviceClass == Device.AUDIO_VIDEO_CAR_AUDIO ||
                		deviceClass == Device.AUDIO_VIDEO_HANDSFREE ) {
	                    	//And if any can be used as a audio handset
	                    	hasConnectedDevice = true;
	                    	break;
                	}

                	DebugLog.d("BT connected device class --> "+deviceClass);
				}
			}
		}
		boolean retVal = hasConnectedDevice && audioManager.isBluetoothScoAvailableOffCall();
//		Log.d(THIS_FILE, "Can I do BT ? "+retVal);
		DebugLog.d("BT Can I do BT --> "+retVal);
		return retVal;
	}
	
	private boolean targetBt = false;
	public void setBluetoothOn(boolean on) {
//		Log.d(THIS_FILE, "Ask for "+on+" vs "+audioManager.isBluetoothScoOn());
		DebugLog.d("BT in BlueToothUtils8 setBluetoothOn called...");
		DebugLog.d("BT Ask for "+on+" vs "+audioManager.isBluetoothScoOn());
		DebugLog.d("BT isBluetoothConnected --> "+isBluetoothConnected);
		DebugLog.d("BT on --> "+on);
		DebugLog.d("BT audioManager.isBluetoothScoOn() --> "+audioManager.isBluetoothScoOn());
		targetBt = on;

		try {
			if(on != isBluetoothConnected) {
			    // BT SCO connection state is different from required activation
				if(on) {
				    // First we try to connect
					Log.d(THIS_FILE, "BT SCO on >>>");
					DebugLog.d("BT BT SCO on >>> on is true");
	                audioManager.startBluetoothSco();

				}else {
					Log.d(THIS_FILE, "BT SCO off >>>");
					DebugLog.d("BT BT SCO off >>> on is false");
					// We stop to use BT SCO
					audioManager.setBluetoothScoOn(false);
					// And we stop BT SCO connection
					audioManager.stopBluetoothSco();
				}

			}else if(on != audioManager.isBluetoothScoOn()) {
				DebugLog.d("BT SCO is already in desired connection state");
			    // BT SCO is already in desired connection state
			    // we only have to use it
				/**
				 * 2020.08.18 이미 블루투스가 연결된 환경에서 오디오소스 재처리를 의한 기능 수정
				 */
				DebugLog.d("BT state on!= audioManager.isBluetoothScoOn()");
			    audioManager.setBluetoothScoOn(on);
			    if(on) {
			    	audioManager.startBluetoothSco();
				} else {

				}

			} else {
				DebugLog.d("BT else case...");
				audioManager.setBluetoothScoOn(false);
			}
		}
		catch (Exception e) {
			Log.e(THIS_FILE, "setBluetoothOn : ", e);
			DebugLog.d("Exception e --> "+e.getMessage());
		}
	}
	
	public boolean isBluetoothOn() {
		return isBluetoothConnected;
	}
	
    public void register() {
//		Log.d(THIS_FILE, "Register BT media receiver");
		DebugLog.d("Register BT media receiver");
		/**
		 * TEST
		 */

		IntentFilter filter = new IntentFilter();
		filter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED);
		filter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
//		filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
		filter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);

		context.registerReceiver(mediaStateReceiver, filter);

//		context.registerReceiver(mediaStateReceiver , new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED));
	}

	public void unregister() {
		try {
//			Log.d(THIS_FILE, "Unregister BT media receiver");
			DebugLog.d("Unregister BT media receiver");
			if(mediaStateReceiver!=null) {
				context.unregisterReceiver(mediaStateReceiver);
			}
		}catch(Exception e) {
//			Log.w(THIS_FILE, "Failed to unregister media state receiver",e);
			DebugLog.d("Failed to unregister media state receiver --> "+e.getMessage());
		}
	}

    @Override
    public boolean isBTHeadsetConnected() {
        return canBluetooth();
    }
}
