/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils.audio;

import android.content.Context;
import android.media.AudioManager;

import com.dial070.sip.service.SipService;
//import com.dial070.sip.utils.*;
import com.dial070.sip.utils.Compatibility;
import com.dial070.utils.DebugLog;


public abstract class AudioFocusWrapper {
private static AudioFocusWrapper instance;
	
	public static AudioFocusWrapper getInstance() {
		DebugLog.d("init AudioFocusWrapper in BridgeMedia");
		/*if(instance == null) {
			String className = "com.dial070.sip.utils.audio.AudioFocus";
//			if(Compatibility.isCompatible(8)) {
//				className += "8";
//			}else {
//				className += "3";
//			}
			className += "3";
			try {
                Class<? extends AudioFocusWrapper> wrappedClass = Class.forName(className).asSubclass(AudioFocusWrapper.class);
                instance = wrappedClass.newInstance();
	        } catch (Exception e) {
	        	throw new IllegalStateException(e);
	        }
		}*/
		if (instance == null) { //BJH 2016.07.18
			if (Compatibility.isCompatible(17)) {
                instance = new com.dial070.sip.utils.audio.AudioFocus17(); 
            } else if (Compatibility.isCompatible(8)) { 
                instance = new com.dial070.sip.utils.audio.AudioFocus8(); 
            } else { 
                instance = new com.dial070.sip.utils.audio.AudioFocus3(); 
            } 
        } 
		
		return instance;
	}
	
	protected AudioFocusWrapper() {}
	
	
	public abstract void init(SipService service, AudioManager manager);
	public abstract void focus();
	public abstract void unFocus();
	public abstract String getProperty(String property);
	/**
	 * ADDED
	 */
	public abstract boolean hasFocus();
	
}
