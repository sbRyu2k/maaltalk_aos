/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.dial070.utils.*;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.net.Uri.Builder;


public class Compatibility
{
	private static final String THIS_FILE = "Compat";
	private static int currentApi = 0;

	public static int getApiLevel()
	{

		if (currentApi > 0) { return currentApi; }

		if (android.os.Build.VERSION.SDK.equalsIgnoreCase("3"))
		{
			currentApi = 3;
		}
		else
		{
			try
			{
				Field f = android.os.Build.VERSION.class.getDeclaredField("SDK_INT");
				currentApi = (Integer) f.get(null);
			}
			catch (Exception e)
			{
				return 0;
			}
		}

		return currentApi;
	}

	public static boolean isCompatible(int apiLevel)
	{
		return getApiLevel() >= apiLevel;
	}

	/**
	 * Get the stream id for in call track. Can differ on some devices. Current
	 * device for which it's different : Archos 5IT
	 * 
	 * @return
	 */
	public static int getInCallStream()
	{
		if (android.os.Build.BRAND.equalsIgnoreCase("archos"))
		{
			// Since archos has no voice call capabilities, voice call stream is
			// not implemented
			// So we have to choose the good stream tag, which is by default
			// falled back to music
			return AudioManager.STREAM_MUSIC;
		}
		// return AudioManager.STREAM_MUSIC;
		return AudioManager.STREAM_VOICE_CALL;
	}

	public static boolean shouldUseRoutingApi()
	{
		//Log.d(THIS_FILE, "Current device " + android.os.Build.BRAND + " - " + android.os.Build.DEVICE + "-" + android.os.Build.MODEL);

		// HTC evo 4G
		if (android.os.Build.PRODUCT.startsWith("htc_supersonic")) { return true; }

		if (!isCompatible(4))
		{
			// If android 1.5, force routing api use
			return true;
		}
		else
		{
			return false;
		}
	}

	public static boolean shouldUseModeApi()
	{
		//Log.d(THIS_FILE, "Current device " + android.os.Build.BRAND + " - " + android.os.Build.DEVICE);
		// ZTE blade
		if (android.os.Build.DEVICE.equalsIgnoreCase("blade")) { return true; }
		// Samsung GT-I5500
		if (android.os.Build.DEVICE.equalsIgnoreCase("GT-I5500")) { return true; }
		
		// HTC evo 4G
		if (android.os.Build.PRODUCT.equalsIgnoreCase("htc_supersonic")) { return true; }
		// LG P500
		if (android.os.Build.DEVICE.equalsIgnoreCase("LG-P500")) { return true; }
		// Huawei
		if (android.os.Build.DEVICE.equalsIgnoreCase("U8150") || android.os.Build.DEVICE.equalsIgnoreCase("U8110")) { return true; }

		return false;
	}

	public static String guessInCallMode()
	{
		if (android.os.Build.BRAND.equalsIgnoreCase("sdg")) { return "3"; }
		if (android.os.Build.DEVICE.equalsIgnoreCase("blade")) { return Integer.toString(AudioManager.MODE_IN_CALL); }

		if (!isCompatible(5)) { return Integer.toString(AudioManager.MODE_IN_CALL); }

		return Integer.toString(AudioManager.MODE_NORMAL);
	}

	public static String getCpuAbi()
	{
		if (isCompatible(4))
		{
			Field field;
			try
			{
				field = android.os.Build.class.getField("CPU_ABI");
				return field.get(null).toString();
			}
			catch (Exception e)
			{
				Log.w(THIS_FILE, "Announce to be android 1.6 but no CPU ABI field", e);
			}

		}
		return "armeabi";
	}

	private static boolean needPspWorkaround(PreferencesWrapper preferencesWrapper)
	{
		// Nexus one is impacted
		if (android.os.Build.DEVICE.equalsIgnoreCase("passion")) { return true; }
		// All htc except....
		if (android.os.Build.PRODUCT.toLowerCase(Locale.getDefault()).startsWith("htc") || android.os.Build.BRAND.toLowerCase(Locale.getDefault()).startsWith("htc") || android.os.Build.PRODUCT.toLowerCase(Locale.getDefault()).equalsIgnoreCase("inc") /*
																																																 * For
																																																 * Incredible
																																																 */)
		{
			if (android.os.Build.DEVICE.equalsIgnoreCase("hero") /* HTC HERO */
					|| android.os.Build.DEVICE.equalsIgnoreCase("magic") /*
																		 * Magic
																		 * Aka
																		 * Dev
																		 * G2
																		 */
					|| android.os.Build.DEVICE.equalsIgnoreCase("tatoo") /* Tatoo */
					|| android.os.Build.DEVICE.equalsIgnoreCase("dream") /*
																		 * Dream
																		 * Aka
																		 * Dev
																		 * G1
																		 */
					|| android.os.Build.DEVICE.equalsIgnoreCase("legend") /* Legend */

			) { return false; }
			return true;
		}
		// Dell streak
		if (android.os.Build.BRAND.toLowerCase(Locale.getDefault()).startsWith("dell") && android.os.Build.DEVICE.equalsIgnoreCase("streak")) { return true; }
		// Motorola milestone 2
		if (android.os.Build.DEVICE.toLowerCase(Locale.getDefault()).contains("milestone2")) { return true; }

		return false;
	}

	private static boolean needToneWorkaround(PreferencesWrapper prefWrapper)
	{
		if (android.os.Build.PRODUCT.toLowerCase(Locale.getDefault()).startsWith("gt-i5800") || android.os.Build.PRODUCT.toLowerCase(Locale.getDefault()).startsWith("gt-i5801")) { return true; }
		return false;
	}

	public static void setFirstRunParameters(PreferencesWrapper preferencesWrapper)
	{
//		String device = android.os.Build.DEVICE.toUpperCase(Locale.getDefault());
		
		// HTC PSP mode hack
		preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.KEEP_AWAKE_IN_CALL, needPspWorkaround(preferencesWrapper));

		// Proximity sensor inverted
		if (android.os.Build.PRODUCT.equalsIgnoreCase("SPH-M900") /* Sgs moment */)
		{
			preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.INVERT_PROXIMITY_SENSOR, true);
		}
		
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("NEXUS S"))
		{
			Log.d(THIS_FILE, "Nexus S default settings !");
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.ECHO_CANCELLATION, true);
			
		}
		
		if (android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M11")
				|| android.os.Build.DEVICE.toUpperCase(Locale.getDefault()).startsWith("SHW-M13"))
		{
			Log.d(THIS_FILE, "Galaxy S default settings !");
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.ECHO_CANCELLATION, true);
		}
		
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHW-M250") 
				|| android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHV-E110S")
				|| android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHV-E120"))
		{
			// 갤럭시S2  (3G모델 SHW-M250S/L/K)
			// 갤럭시S2 LTE (SKT전용 4G모델 SHV-E110S)
			// 갤럭시S2 HD LTE (3사통신사 출시 4G모델 SHV-E120S/L/K)
			Log.d(THIS_FILE, "Galaxy S2 default settings !");
			preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.ECHO_CANCELLATION, true);
		}
		
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHV-E3"))
		{
			Log.d(THIS_FILE, "Galaxy S4 default settings !");
			if (isCompatible(19))
			{
				// 4.4 킷캣
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.85);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			}
			else
			{
				// 4.3 젤리빈				
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.75);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			}
		}		
		else if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHV-E250"))
		{
			Log.d(THIS_FILE, "Galaxy Note 2 default settings !");
			if (isCompatible(19))
			{
				// 4.4
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.5);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.25);
			}
			else
			{
				// 4.3
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.5);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			}
		}			
		else if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SHV-E2"))
		{
			Log.d(THIS_FILE, "Galaxy S3 default settings !");
			if (isCompatible(19))
			{
				// 4.4
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.85);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			}
			else
			{
				// 4.3
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.75);
				preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.5);
			}
		}				
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("LG-F24"))
		{
			Log.d(THIS_FILE, "LG G2 default settings !");
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 2.0);		
		}
		
		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("SM-N900"))
		{
			Log.d(THIS_FILE, "Galaxy Note3 default settings !");
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.75);
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 1.2);		
		}

		if (android.os.Build.MODEL.toUpperCase(Locale.getDefault()).startsWith("IM-A9"))
		{
			// 펜텍 베가 계열, 수화음량이 너무 크면 에코가 생겨서 0.8 수준으로 교정함. 
			Log.d(THIS_FILE, "VEGA default settings !");
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL, (float) 0.8);
			preferencesWrapper.setPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL, (float) 0.8);		
		}
		
		// Use routing API?
		preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.USE_ROUTING_API, shouldUseRoutingApi());
		preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.USE_MODE_API, shouldUseModeApi());
		preferencesWrapper.setPreferenceBooleanValue(PreferencesWrapper.SET_AUDIO_GENERATE_TONE, needToneWorkaround(preferencesWrapper));
	}

	public static boolean useFlipAnimation()
	{
		if (android.os.Build.BRAND.equalsIgnoreCase("archos")) { return false; }
		return true;
	}

	public static List<ResolveInfo> getPossibleActivities(Context ctxt, Intent i)
	{
		PackageManager pm = ctxt.getPackageManager();
		try
		{
			return pm.queryIntentActivities(i, PackageManager.MATCH_DEFAULT_ONLY | PackageManager.GET_RESOLVED_FILTER);
		}
		catch (NullPointerException e)
		{
			return new ArrayList<ResolveInfo>();
		}
	}

	public static Intent getPriviledgedIntent(String number)
	{
		Intent i = new Intent("android.intent.action.CALL_PRIVILEGED");
		Builder b = new Uri.Builder();
		b.scheme("tel").appendPath(number);
		i.setData(b.build());
		return i;
	}

	private static List<ResolveInfo> callIntents = null;

	public final static List<ResolveInfo> getIntentsForCall(Context ctxt)
	{
		if (callIntents == null)
		{
			callIntents = getPossibleActivities(ctxt, getPriviledgedIntent("123"));
		}
		return callIntents;
	}

	public static boolean canResolveIntent(Context context, final Intent intent)
	{
		final PackageManager packageManager = context.getPackageManager();
		// final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	private static Boolean canMakeGSMCall = null;
	private static Boolean canMakeSkypeCall = null;

	public static boolean canMakeGSMCall(Context context)
	{
		PreferencesWrapper prefs = new PreferencesWrapper(context);
		if (prefs.getGsmIntegrationType() == PreferencesWrapper.GSM_TYPE_AUTO)
		{
			if (canMakeGSMCall == null)
			{
				//Intent intentMakePstnCall = new Intent(Intent.ACTION_CALL);
				Intent intentMakePstnCall = new Intent(Intent.ACTION_DIAL);
				intentMakePstnCall.setData(Uri.fromParts("tel", "12345", null));
				canMakeGSMCall = canResolveIntent(context, intentMakePstnCall);
			}
			return canMakeGSMCall;
		}
		if (prefs.getGsmIntegrationType() == PreferencesWrapper.GSM_TYPE_PREVENT) { return false; }
		return true;
	}

	public static boolean canMakeSkypeCall(Context context)
	{
		if (canMakeSkypeCall == null)
		{
			try
			{
				PackageInfo skype = context.getPackageManager().getPackageInfo("com.skype.raider", 0);
				if (skype != null)
				{
					canMakeSkypeCall = true;
				}
			}
			catch (NameNotFoundException e)
			{
				canMakeSkypeCall = false;
			}
		}
		return canMakeSkypeCall;
	}

	public static Intent getContactPhoneIntent()
	{
		Intent intent = new Intent(Intent.ACTION_PICK);
		/*
		 * intent.setAction(Intent.ACTION_GET_CONTENT);
		 * intent.setType(Contacts.Phones.CONTENT_ITEM_TYPE);
		 */
//		if (!isCompatible(5))
//		{
//			intent.setData(Contacts.People.CONTENT_URI);
//		}
//		else
		{
			intent.setData(Uri.parse("content://com.android.contacts/contacts"));
		}

		return intent;

	}

	public static void updateVersion(PreferencesWrapper prefWrapper, int lastSeenVersion, int runningVersion)
	{
	}

}
