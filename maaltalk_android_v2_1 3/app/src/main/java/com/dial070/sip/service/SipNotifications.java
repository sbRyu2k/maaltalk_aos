/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.service;

import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
//import android.provider.CallLog;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import com.dial070.App;
import com.dial070.db.RecentCallsData;
import com.dial070.sip.api.*;
import com.dial070.sip.models.*;
import com.dial070.ui.MarshmallowResponse;
import com.dial070.utils.ContactHelper;
//import com.dial070.widgets.RegistrationNotification;
import com.dial070.maaltalk.R;
import com.dial070.service.Fcm;
import com.dial070.utils.Log;

public class SipNotifications {
//	private static String THIS_FILE = "SIP-NOTIFICATION";

	private NotificationManager notificationManager;
//	private RegistrationNotification contentView;
	private Notification inCallNotification;
	private Context context;
	private Notification missedCallNotification;
	private Notification messageNotification;
	private Notification messageVoicemail;

	public static final int REGISTER_NOTIF_ID = 2001;
	public static final int CALL_NOTIF_ID = REGISTER_NOTIF_ID + 1;
	public static final int CALLLOG_NOTIF_ID = REGISTER_NOTIF_ID + 2;
	public static final int MESSAGE_NOTIF_ID = REGISTER_NOTIF_ID + 3;
	public static final int VOICEMAIL_NOTIF_ID = REGISTER_NOTIF_ID + 4;
	
	public SipNotifications(Context aContext) {
		context = aContext;
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= 26) {
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(CHANNEL_ID, context.getString(R.string.app_name), importance);
			notificationManager.createNotificationChannel(channel);
		}
	}
	
	//Announces

	//Register
	public int notifyRegisteredIcon = 0;
	public synchronized void notifyRegisteredAccounts(ArrayList<SipProfileState> activeAccountsInfos) 
	{
		/*
		int icon = R.drawable.noti_online;
		CharSequence tickerText = context.getString(R.string.service_ticker_registered_text);
		
		String user = null;
		if (activeAccountsInfos != null && activeAccountsInfos.size() > 0)
		{
			for (SipProfileState account : activeAccountsInfos)
			{
				if (account.getStatusCode() < SipCallSession.StatusCode.OK)
				{
					icon = R.drawable.noti_offline;
					tickerText = context.getString(R.string.service_ticker_connecting_text);
				}
				
				user = account.getDisplayName().toString();
				
				break;
			}			
		}		
		else
		{
			icon = R.drawable.noti_offline;
			tickerText = context.getString(R.string.service_ticker_unregistered_text);			
		}
		
		if (notifyRegisteredIcon == icon) return;
		notifyRegisteredIcon = icon;
		
		
		long when = System.currentTimeMillis();
	
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification notification = new Notification(icon, tickerText, when);		
		notification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
		//notification.flags = Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_SHOW_LIGHTS;
		// notification.flags = Notification.FLAG_FOREGROUND_SERVICE;
		
		String title = "";
		String status = "";
		if (user != null)
			status = user + "님 " + tickerText.toString();
		else
			status = tickerText.toString();			
		
		notification.setLatestEventInfo(context, title, status, contentIntent);

		notificationManager.notify(REGISTER_NOTIF_ID, notification);
		
		if (icon == R.drawable.noti_offline)
			notificationManager.cancel(REGISTER_NOTIF_ID);
			*/
	}

	private static final String CHANNEL_ID = "channel_01";

	// Calls
	public synchronized void showNotificationForCall(SipCallSession currentCallInfo2) {
		int icon = R.drawable.noti_outbound;
		CharSequence tickerText =  context.getText(R.string.outbound_call);
		
		if (currentCallInfo2.isIncoming())
		{
			icon = R.drawable.noti_inbound;
			tickerText =  context.getText(R.string.incomming_call);
		}

		long when = System.currentTimeMillis();
		
		String remoteContact = currentCallInfo2.getRemoteContact();
		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);
		String displayName = "";
		if (callerInfo != null)
		{
			displayName = callerInfo.name;
			if (displayName == null || displayName.length() == 0)
				displayName = callerInfo.phoneNumber;
		}
			
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_CALL_UI);
		if (App.isReturnClicked){
			notificationIntent = new Intent(SipManager.ACTION_SIP_CALL_RETURN_UI);
		}
		notificationIntent.putExtra(SipManager.EXTRA_CALL_INFO, currentCallInfo2);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		inCallNotification = MarshmallowResponse.createNotification(context, contentIntent, CHANNEL_ID, tickerText.toString(), displayName, icon);

//		notificationManager.notify(CALL_NOTIF_ID, inCallNotification);
	}
	
	public synchronized void showNotificationForMissedCall(ContentValues callLog) {
		Log.d("SipNotifications","showNotificationForMissedCall");
		int icon = R.drawable.noti_missed;
		CharSequence tickerText =  context.getText(R.string.missed_call);
		long when = System.currentTimeMillis();

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_CALLLOG);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		
		//String remoteContact = callLog.getAsString(CallLog.Calls.NUMBER);
		String remoteContact = callLog.getAsString(RecentCallsData.FIELD_PHONENUMBER);
		if (remoteContact == null)
		{
			// not found
			return;
		}
		
		CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(context, remoteContact);
		String displayName = remoteContact;
		if (callerInfo != null)
		{
			displayName = callerInfo.name;
			if (displayName == null || displayName.length() == 0)
				displayName = callerInfo.phoneNumber;
		} else { //BJH 2016.10.26 관리팀 요청
			displayName = ContactHelper.getContactsNameByPhoneNumber(context, remoteContact);
			if (displayName == null || displayName.length() == 0)
				displayName = remoteContact;
		}

		missedCallNotification = MarshmallowResponse.createNotification(context, contentIntent, CHANNEL_ID, context.getText(R.string.missed_call).toString(), displayName, icon);
		missedCallNotification.flags |= Notification.FLAG_AUTO_CANCEL; // BJH 2018.07.25 부재중 건의사항 자동 종료 안 되도록
		notificationManager.notify(CALLLOG_NOTIF_ID, missedCallNotification);
	}
	
	public synchronized void showNotificationForMessage(SipMessage msg) {
		//CharSequence tickerText = context.getText(R.string.instance_message);
		if(!msg.getFrom().equalsIgnoreCase(viewingRemoteFrom)) {
			String from = SipUri.getDisplayedSimpleContact(msg.getFrom());
			CharSequence tickerText = buildTickerMessage(context, from, msg.getBody());

			Intent notificationIntent = new Intent(SipManager.ACTION_SIP_MESSAGES);
			notificationIntent.putExtra(SipMessage.FIELD_FROM, msg.getFrom());
			notificationIntent.putExtra(SipMessage.FIELD_BODY, msg.getBody());
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

			messageNotification = MarshmallowResponse.createNotification(context, contentIntent, CHANNEL_ID, from, msg.getBody(), R.drawable.noti_newmsg);

			notificationManager.notify(MESSAGE_NOTIF_ID, messageNotification);
		}
	}
	
	

	public synchronized void showNotificationForVoiceMail(SipProfile acc, int numberOfMessages, String voiceMailNumber) {
		if(messageVoicemail == null) {
			messageVoicemail = new Notification(R.drawable.noti_vms, context.getString(R.string.voice_mail), System.currentTimeMillis());
			messageVoicemail.flags = Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_AUTO_CANCEL;
			messageVoicemail.defaults |= Notification.DEFAULT_SOUND;
			messageVoicemail.defaults |= Notification.DEFAULT_LIGHTS;
		}
		
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_VMS);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		messageNotification = MarshmallowResponse.createNotification(context, contentIntent, CHANNEL_ID, context.getString(R.string.voice_mail).toString(), Integer.toString(numberOfMessages) + context.getString(R.string.voice_mail_desc), R.drawable.noti_vms);

		notificationManager.notify(VOICEMAIL_NOTIF_ID, messageVoicemail);
		
		notificationManager.cancel(Fcm.FCM_NOTIF_VMS);
		
		// SET BADGE NUMBER;
		//ServicePrefs.setBadgeNumber(ServicePrefs.BADGE_VMS, Integer.toString(numberOfMessages));
		//ServicePrefs.fireBadgeEvent(context);		
	}
	
	private static String viewingRemoteFrom = null;
	public void setViewingMessageFrom(String remoteFrom) {
		viewingRemoteFrom = remoteFrom;
	}
	
	
    protected static CharSequence buildTickerMessage(
            Context context, String address, String body) {
        String displayAddress = address;

        StringBuilder buf = new StringBuilder(
                displayAddress == null
                ? ""
                : displayAddress.replace('\n', ' ').replace('\r', ' '));
        buf.append(':').append(' ');

        int offset = buf.length();

        if (!TextUtils.isEmpty(body)) {
            body = body.replace('\n', ' ').replace('\r', ' ');
            buf.append(body);
        }

        SpannableString spanText = new SpannableString(buf.toString());
        spanText.setSpan(new StyleSpan(Typeface.BOLD), 0, offset,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spanText;
    }
	
	
	// Cancels
	public synchronized void cancelRegisters() {
		notificationManager.cancel(REGISTER_NOTIF_ID);
	}
	
	public synchronized void cancelCalls() {
		notificationManager.cancel(CALL_NOTIF_ID);
	}
	
	public synchronized void cancelMissedCalls() {
		notificationManager.cancel(CALLLOG_NOTIF_ID);
	}
	
	public synchronized void cancelMessages() {
		notificationManager.cancel(MESSAGE_NOTIF_ID);
	}
	
	public synchronized void cancelVoicemails() {
		notificationManager.cancel(VOICEMAIL_NOTIF_ID);
	}
	
	
	public void cancelAllNotification() {
		//notificationManager.cancelAll();
		cancelCalls();
		cancelMessages();
		//cancelMissedCalls();
		cancelRegisters();
		cancelVoicemails();
	}

}
