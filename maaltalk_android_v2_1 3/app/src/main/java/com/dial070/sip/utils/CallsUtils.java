/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils;

import android.content.Context;

import com.dial070.maaltalk.R;
import com.dial070.sip.api.*;

public class CallsUtils {
	/**
	 * Get the corresponding string for a given state Can be used to translate
	 * or debug current state
	 * 
	 * @return the string reprensenting this call info state
	 */
	public static final String getStringCallState(SipCallSession session,
			Context context) {

		int callState = session.getCallState();
		switch (callState) {
		case SipCallSession.InvState.CALLING:
			return context.getString(R.string.call_state_calling);
		case SipCallSession.InvState.CONFIRMED:
			return context.getString(R.string.call_state_confirmed);
		case SipCallSession.InvState.CONNECTING:
			return context.getString(R.string.call_state_connecting);
		case SipCallSession.InvState.DISCONNECTED:
			return context.getString(R.string.call_state_disconnected);
		case SipCallSession.InvState.EARLY:
		{
			if (session.isIncoming())
				return context.getString(R.string.call_state_incoming);
			else
				return context.getString(R.string.call_state_calling);
		}
		case SipCallSession.InvState.INCOMING:
			return context.getString(R.string.call_state_incoming);
		case SipCallSession.InvState.NULL:
			return context.getString(R.string.call_state_null);
		}

		return "";
	}
}
