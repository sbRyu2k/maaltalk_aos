/**
 * Copyright (C) 2010 Dial Communications (www.dial070.co.kr)
 * This file is part of Dial070.
 *
 *  Dial070 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dial070 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dial070.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dial070.sip.utils.audio;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;

import com.dial070.sip.service.HeadsetButtonReceiver;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.Compatibility;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

@TargetApi(8) 
public class AudioFocus8 extends AudioFocusWrapper{
	
	
	protected static final String THIS_FILE = "AudioFocus 8";
	final static String PAUSE_ACTION = "com.android.music.musicservicecommand.pause";
	final static String TOGGLEPAUSE_ACTION = "com.android.music.musicservicecommand.togglepause";
	protected AudioManager audioManager; //BJH 2016.07.18 private -> protected
	private SipService service;
	private ComponentName headsetButtonReceiverName;
	
	private boolean isFocused = false;
	
	private OnAudioFocusChangeListener focusChangedListener = new OnAudioFocusChangeListener() {
		
		@Override
		public void onAudioFocusChange(int focusChange) {
			Log.d(THIS_FILE, "Focus changed");
			DebugLog.d("FocusChangedListener onAudioFocusChange --> "+focusChange);
		}
	};
	
	public void init(SipService aService, AudioManager manager) {
		DebugLog.d("init AudioFocus8 caller --> "+aService.getClass().getSimpleName());

		service = aService;
		audioManager = manager;
		headsetButtonReceiverName = new ComponentName(service.getPackageName(), 
				HeadsetButtonReceiver.class.getName());
	}

	
	public void focus() {
//		Log.d(THIS_FILE, "Focus again "+isFocused);
		DebugLog.d("Focus again --> "+isFocused);
		DebugLog.d("Focus again inCallStream --> "+Compatibility.getInCallStream());
		DebugLog.d("Focus again music active state --> "+audioManager.isMusicActive());

		if(!isFocused) {
			HeadsetButtonReceiver.setService(SipService.getUAStateReceiver());
			audioManager.registerMediaButtonEventReceiver(headsetButtonReceiverName);
			audioManager.requestAudioFocus(focusChangedListener, Compatibility.getInCallStream(), AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
			isFocused = true;
		}
	}
	
	public void unFocus() {
		DebugLog.d("unFocus");

		if(isFocused) {
			HeadsetButtonReceiver.setService(null);
			audioManager.unregisterMediaButtonEventReceiver(headsetButtonReceiverName);
			audioManager.abandonAudioFocus(focusChangedListener);
			isFocused = false;
		}
	}

	@Override
	public boolean hasFocus() {
		return isFocused;
	}

	@Override
    public String getProperty(String property) { //BJH 2016.07.18
        return null; 
    }

}
