package com.dial070.sip.utils.audio;

import java.nio.ByteBuffer;

//import android.content.Context;
import android.os.Handler;
import android.media.*;
import com.dial070.utils.*;

/**
 * The AudioRecord class manages the audio resources for Java applications
 * to record audio from the audio input hardware of the platform. This is
 * achieved by "pulling" (reading) the data from the AudioRecord object. The
 * application is responsible for polling the AudioRecord object in time using one of 
 * the following three methods:  {@link #read(byte[],int, int)}, {@link #read(short[], int, int)}
 * or {@link #read(ByteBuffer, int)}. The choice of which method to use will be based 
 * on the audio data storage format that is the most convenient for the user of AudioRecord.
 * <p>Upon creation, an AudioRecord object initializes its associated audio buffer that it will
 * fill with the new audio data. The size of this buffer, specified during the construction, 
 * determines how long an AudioRecord can record before "over-running" data that has not
 * been read yet. Data should be read from the audio hardware in chunks of sizes inferior to
 * the total recording buffer size.
 */
public class MyAudioRecord
{
	final private static String THIS_FILE = "AudioRecord";
	
    //---------------------------------------------------------
    // Member variables
    //--------------------
    public static boolean routingchanged;
    private AudioRecord mRecorder;
	private final int audioSource;
	private final int sampleRateInHz;
	private final int channelConfig;
	private final int audioFormat;
	private final int bufferSizeInBytes;
	
	private boolean audioStarted;


    //---------------------------------------------------------
    // Constructor, Finalize
    //--------------------
    /**
     * Class constructor.
     * @param audioSource the recording source. See {@link MediaRecorder.AudioSource} for
     *    recording source definitions.
     * @param sampleRateInHz the sample rate expressed in Hertz. Examples of rates are (but
     *   not limited to) 44100, 22050 and 11025.
     * @param channelConfig describes the configuration of the audio channels. 
     *   See {@link AudioFormat#CHANNEL_IN_MONO} and
     *   {@link AudioFormat#CHANNEL_IN_STEREO}
     * @param audioFormat the format in which the audio data is represented. 
     *   See {@link AudioFormat#ENCODING_PCM_16BIT} and 
     *   {@link AudioFormat#ENCODING_PCM_8BIT}
     * @param bufferSizeInBytes the total size (in bytes) of the buffer where audio data is written
     *   to during the recording. New audio data can be read from this buffer in smaller chunks 
     *   than this size. See {@link #getMinBufferSize(int, int, int)} to determine the minimum
     *   required buffer size for the successful creation of an AudioRecord instance. Using values
     *   smaller than getMinBufferSize() will result in an initialization failure.
     * @throws java.lang.IllegalArgumentException
     */
    public MyAudioRecord(int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, 
            int bufferSizeInBytes)
    throws IllegalArgumentException {   
		this.audioSource = audioSource;
		this.sampleRateInHz = sampleRateInHz;
		this.channelConfig = channelConfig;
		this.audioFormat = audioFormat;
		this.bufferSizeInBytes = bufferSizeInBytes;
		this.init();
    }

	private boolean init()
	{
		routingchanged = false;
		this.audioStarted = false;
		this.mRecorder = null;
		try
		{
			this.mRecorder = new AudioRecord(this.audioSource, this.sampleRateInHz, this.channelConfig, 
					this.audioFormat, this.bufferSizeInBytes);
			return true;
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		return false;
	}


	private boolean reinit() 
	{
		Log.d(THIS_FILE, "REINIT : START");
		if (this.mRecorder != null)
		{
			this.audioStarted = false;
			try
			{
				Log.d(THIS_FILE, "STOP");
				this.mRecorder.stop();
				Log.d(THIS_FILE, "RELEASE");
				this.mRecorder.release();
			}
			catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			}
			this.mRecorder = null;
		}
		
		Log.d(THIS_FILE, "CHECK AUDIO");
		//SipService.checkAudioInCall();
		
		Log.d(THIS_FILE, "INIT");
		if (!this.init())
		{
			Log.d(THIS_FILE, "INIT : ERROR");
			return false;
		}

		Log.d(THIS_FILE, "startRecording");
		if (!this.startRecording())
		{
			Log.d(THIS_FILE, "startRecording : ERROR");
			return false;
		}
		routingchanged = false;
		
		Log.d(THIS_FILE, "REINIT : END");
		
		return true;
	}

    /**
     * Releases the native AudioRecord resources.
     * The object can no longer be used and the reference should be set to null
     * after a call to release()
     */
    public void release() {
    	if (this.mRecorder != null)
    	{
    		Thread t = new Thread()
    		{
    			public void run()
    			{
    				MyAudioRecord.this.mRecorder.release();
    				MyAudioRecord.this.mRecorder = null;
    			}
    		};
    		if (t != null)
    			t.start();
    	}
    }


    //--------------------------------------------------------------------------
    // Getters
    //--------------------
    /**
     * Returns the configured audio data sample rate in Hz
     */
    public int getSampleRate() {
    	if (this.mRecorder == null) return 0;
        return this.mRecorder.getSampleRate();
    }
    
    /**
     * Returns the audio recording source. 
     * @see MediaRecorder.AudioSource
     */
    public int getAudioSource() {
    	if (this.mRecorder == null) return 0;
        return this.mRecorder.getAudioSource();
    }

    /**
     * Returns the configured audio data format. See {@link AudioFormat#ENCODING_PCM_16BIT}
     * and {@link AudioFormat#ENCODING_PCM_8BIT}.
     */
    public int getAudioFormat() {
    	if (this.mRecorder == null) return AudioFormat.ENCODING_PCM_16BIT;
        return this.mRecorder.getAudioFormat();
    }

    /**
     * Returns the configured channel configuration. 
     * See {@link AudioFormat#CHANNEL_IN_MONO}
     * and {@link AudioFormat#CHANNEL_IN_STEREO}.
     */
    public int getChannelConfiguration() {
    	if (this.mRecorder == null) return AudioFormat.CHANNEL_IN_MONO;
        return this.mRecorder.getChannelConfiguration();
    }

    /**
     * Returns the configured number of channels.
     */
    public int getChannelCount() {
    	if (this.mRecorder == null) return 0;
        return this.mRecorder.getChannelCount();
    }

    /**
     * Returns the state of the AudioRecord instance. This is useful after the
     * AudioRecord instance has been created to check if it was initialized 
     * properly. This ensures that the appropriate hardware resources have been
     * acquired.
     * @see AudioRecord#STATE_INITIALIZED
     * @see AudioRecord#STATE_UNINITIALIZED
     */
    public int getState() {
    	if (this.mRecorder == null) return AudioRecord.STATE_UNINITIALIZED;
        return this.mRecorder.getState();
    }

    /**
     * Returns the recording state of the AudioRecord instance.
     * @see AudioRecord#RECORDSTATE_STOPPED
     * @see AudioRecord#RECORDSTATE_RECORDING
     */
    public int getRecordingState() {
    	if (this.mRecorder == null) return AudioRecord.RECORDSTATE_STOPPED;
        return this.mRecorder.getRecordingState();
    }

    /**
     * Returns the notification marker position expressed in frames.
     */
    public int getNotificationMarkerPosition() {
    	if (this.mRecorder == null) return -1;
        return this.mRecorder.getNotificationMarkerPosition();
    }

    /**
     * Returns the notification update period expressed in frames.
     */
    public int getPositionNotificationPeriod() {
    	if (this.mRecorder == null) return -1;
        return this.mRecorder.getPositionNotificationPeriod();
    }

    /**
     * Returns the minimum buffer size required for the successful creation of an AudioRecord
     * object.
     * Note that this size doesn't guarantee a smooth recording under load, and higher values
     * should be chosen according to the expected frequency at which the AudioRecord instance
     * will be polled for new data.
     * @param sampleRateInHz the sample rate expressed in Hertz.
     * @param channelConfig describes the configuration of the audio channels. 
     *   See {@link AudioFormat#CHANNEL_IN_MONO} and
     *   {@link AudioFormat#CHANNEL_IN_STEREO}
     * @param audioFormat the format in which the audio data is represented. 
     *   See {@link AudioFormat#ENCODING_PCM_16BIT}.
     * @return {@link #ERROR_BAD_VALUE} if the recording parameters are not supported by the 
     *  hardware, or an invalid parameter was passed,
     *  or {@link #ERROR} if the implementation was unable to query the hardware for its 
     *  output properties, 
     *   or the minimum buffer size expressed in bytes.
     */
    static public int getMinBufferSize(int sampleRateInHz, int channelConfig, int audioFormat) {
		return AudioRecord.getMinBufferSize(sampleRateInHz, channelConfig, audioFormat);
    }


    //---------------------------------------------------------
    // Transport control methods
    //--------------------
    /**
     * Starts recording from the AudioRecord instance. 
     */
    public boolean startRecording()
    {
    	if (this.mRecorder == null) return false;
		try
		{
			if (this.mRecorder.getState() == AudioRecord.STATE_UNINITIALIZED)
			{
				Log.d(THIS_FILE, "startRecording : STATE_UNINITIALIZED");
				return false;
			}
			
			this.mRecorder.startRecording();
			this.audioStarted = true;
			return true;
		}
		catch (IllegalStateException e)
		{
			e.printStackTrace();
		}
		return false;		
    }



    /**
     * Stops recording.
     */
    public boolean stop()
    {
    	if (this.mRecorder == null) return false;
		try
		{
			this.audioStarted = false;
			this.mRecorder.stop();
			return true;
		}
		catch (IllegalStateException e)
		{
			e.printStackTrace();
		}
		return false;		
    }


    //---------------------------------------------------------
    // Audio data supply
    //--------------------
    /**
     * Reads audio data from the audio hardware for recording into a buffer.
     * @param audioData the array to which the recorded audio data is written.
     * @param offsetInBytes index in audioData from which the data is written expressed in bytes.
     * @param sizeInBytes the number of requested bytes.
     * @return the number of bytes that were read or or {@link #ERROR_INVALID_OPERATION}
     *    if the object wasn't properly initialized, or {@link #ERROR_BAD_VALUE} if
     *    the parameters don't resolve to valid data and indexes.
     *    The number of bytes will not exceed sizeInBytes.
     */    
    public int read(byte[] audioData, int offsetInBytes, int sizeInBytes) {
		if(routingchanged){
			this.reinit();
		}
		if (this.mRecorder == null) return -1;
		if (!this.audioStarted) return -1;
		return this.mRecorder.read(audioData, offsetInBytes, sizeInBytes);
    }


    /**
     * Reads audio data from the audio hardware for recording into a buffer.
     * @param audioData the array to which the recorded audio data is written.
     * @param offsetInShorts index in audioData from which the data is written expressed in shorts.
     * @param sizeInShorts the number of requested shorts.
     * @return the number of shorts that were read or or {@link #ERROR_INVALID_OPERATION}
     *    if the object wasn't properly initialized, or {@link #ERROR_BAD_VALUE} if
     *    the parameters don't resolve to valid data and indexes.
     *    The number of shorts will not exceed sizeInShorts.
     */    
    public int read(short[] audioData, int offsetInShorts, int sizeInShorts) {
		if(routingchanged){
			this.reinit();
		}
		if (this.mRecorder == null) return -1;
		if (!this.audioStarted) return -1;
		return this.mRecorder.read(audioData, offsetInShorts, sizeInShorts);
    }


    /**
     * Reads audio data from the audio hardware for recording into a direct buffer. If this buffer
     * is not a direct buffer, this method will always return 0.
     * @param audioBuffer the direct buffer to which the recorded audio data is written.
     * @param sizeInBytes the number of requested bytes.
     * @return the number of bytes that were read or or {@link #ERROR_INVALID_OPERATION}
     *    if the object wasn't properly initialized, or {@link #ERROR_BAD_VALUE} if
     *    the parameters don't resolve to valid data and indexes.
     *    The number of bytes will not exceed sizeInBytes.
     */    
    public int read(ByteBuffer audioBuffer, int sizeInBytes) {
		if(routingchanged){
			this.reinit();
		}
		if (this.mRecorder == null) return -1;
		if (!this.audioStarted) return -1;
		return this.mRecorder.read(audioBuffer, sizeInBytes);
    }


    //--------------------------------------------------------------------------
    // Initialization / configuration
    //--------------------  
    /**
     * Sets the listener the AudioRecord notifies when a previously set marker is reached or
     * for each periodic record head position update.
     * @param listener
     */
    public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener listener) {
//        this.mRecorder.setRecordPositionUpdateListener(listener);
    }

    /**
     * Sets the listener the AudioRecord notifies when a previously set marker is reached or
     * for each periodic record head position update.
     * Use this method to receive AudioRecord events in the Handler associated with another
     * thread than the one in which you created the AudioTrack instance.
     * @param listener

     * @param handler the Handler that will receive the event notification messages.
     */
    public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener listener, 
                                                    Handler handler) {
//		this.mRecorder.setRecordPositionUpdateListener(listener, handler);
    }


    /**
     * Sets the marker position at which the listener is called, if set with 
     * {@link #setRecordPositionUpdateListener(OnRecordPositionUpdateListener)} or 
     * {@link #setRecordPositionUpdateListener(OnRecordPositionUpdateListener, Handler)}.
     * @param markerInFrames marker position expressed in frames
     * @return error code or success, see {@link #SUCCESS}, {@link #ERROR_BAD_VALUE},
     *  {@link #ERROR_INVALID_OPERATION} 
     */
    public int setNotificationMarkerPosition(int markerInFrames) {
    	if (this.mRecorder == null) return -1;
        return this.mRecorder.setNotificationMarkerPosition(markerInFrames);
    }


    /**
     * Sets the period at which the listener is called, if set with
     * {@link #setRecordPositionUpdateListener(OnRecordPositionUpdateListener)} or 
     * {@link #setRecordPositionUpdateListener(OnRecordPositionUpdateListener, Handler)}.
     * @param periodInFrames update period expressed in frames
     * @return error code or success, see {@link #SUCCESS}, {@link #ERROR_INVALID_OPERATION}
     */
    public int setPositionNotificationPeriod(int periodInFrames) {
    	if (this.mRecorder == null) return -1;
        return this.mRecorder.setPositionNotificationPeriod(periodInFrames);
    }


    //---------------------------------------------------------
    // Interface definitions
    //--------------------
    /**
     * Interface definition for a callback to be invoked when an AudioRecord has
     * reached a notification marker set by {@link AudioRecord#setNotificationMarkerPosition(int)}
     * or for periodic updates on the progress of the record head, as set by
     * {@link AudioRecord#setPositionNotificationPeriod(int)}.
     */
    public interface OnRecordPositionUpdateListener  {
        /**
         * Called on the listener to notify it that the previously set marker has been reached
         * by the recording head.
         */
        void onMarkerReached(AudioRecord recorder);
        
        /**
         * Called on the listener to periodically notify it that the record head has reached
         * a multiple of the notification period.
         */
        void onPeriodicNotification(AudioRecord recorder);
    }

}