package com.dial070.oauth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.DialMain;
import com.dial070.maaltalk.R;
import com.dial070.ui.Setting;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.usermgmt.ApiErrorCode;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.data.OAuthLoginState;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.http.params.HttpConnectionParams.*;

/**
 * Created by DialCommunications on 2017-05-22.
 */

public class OauthLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private SessionCallback mSessionCallback = null;
    private static final String THIS_FILE = "OAUTH_LOGIN";
    private ImageButton mBtnNaviClose;
    private boolean mOauth = false;
    public static final String ACTIVITY_RESULT = "ActivityResult";
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private CallbackManager mCallbackManager = null;
    private AccessToken mAccessToken;
    private ProfileTracker mProfileTracker = null;
    private OAuthLogin mOAuthLoginModule;
    private Context mContext;
    private Activity mActivity;
    private String mUserID = null;
    private String mMyinfoURL = null;
    private String mCidTypeURL = null;
    private AppPrefs mPrefs;
    private String mOauthID = "";
    private String mOauthType = "";
    private String mOauthEmail = "";
    private String mCidType = "";
    private androidx.appcompat.app.AlertDialog pDialog;
    private LinearLayout activity_login_activity;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
        mPrefs = new AppPrefs(mContext);
        mUserID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        mMyinfoURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_MYINFO);
        mCidTypeURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE);
        setContentView(R.layout.activity_oauth_login);

        DebugLog.d("test_my_info url: OauthLoginActivity onCreate()<--");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Log.i(THIS_FILE,"KeyHash:"+getKeyHash(OauthLoginActivity.this));

        activity_login_activity = findViewById(R.id.activity_login_activity);

        mBtnNaviClose = (ImageButton) findViewById(R.id.btn_web_navi_close);
        mBtnNaviClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mOauth) {
                    //finishActivity(getResources().getString(R.string.pay_ok));
                    //return;
                }
                finish();
            }
        });

        //cofirmMyinfo();

        findViewById(R.id.login_kakao).setOnClickListener(this);
        findViewById(R.id.login_google).setOnClickListener(this);
        findViewById(R.id.login_facebook).setOnClickListener(this);
        findViewById(R.id.login_naver).setOnClickListener(this);
        findViewById(R.id.mt_pincode).setOnClickListener(this);
        findViewById(R.id.mt_cid_type).setOnClickListener(this);
        findViewById(R.id.mt_kt_friend).setOnClickListener(this);
        findViewById(R.id.mt_kt_inquire).setOnClickListener(this);

        callback = new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Toast.makeText(getApplicationContext(), errorResult.getErrorMessage(), Toast.LENGTH_LONG).show();
                Log.d(THIS_FILE,"errorResult.getErrorMessage():"+errorResult.getErrorMessage());
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {
                Toast.makeText(getApplicationContext(), "전송하였습니다.", Toast.LENGTH_LONG).show();
            }
        };
    }

    private void showLoadingDialog(){
        if (pDialog==null){
            pDialog =new AlertDialog.Builder(this).create();
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();

            TextView messageView = pDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);
        }

        if (pDialog.isShowing()){
            pDialog.dismiss();
        }
        pDialog.show();
    }

    private void hideLoadingDialog(){
        if (pDialog!=null){
            pDialog.dismiss();
        }
    }

    public void finishActivity(String p_strFinishMsg) {
        Log.d(THIS_FILE, "[OauthLoginActivity] finishActivity : " + p_strFinishMsg);

        if (p_strFinishMsg != null) {
            Intent intent = new Intent();
            intent.putExtra(ACTIVITY_RESULT, p_strFinishMsg);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED);
        }

        hideProgres();

        finish();
    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.d(THIS_FILE, "onSessionOpened " + "onSessionOpened");
            kakaoRedirectSignupActivity();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            Log.d(THIS_FILE, "onSessionOpenFailed " + "onSessionOpenFailed");
            if (exception != null) {
                Logger.e(exception);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Kakao
        try {
            if (Session.getCurrentSession().handleActivityResult(requestCode,
                    resultCode, data)) {
                Log.d(THIS_FILE, "onActivityResult " + "onActivityResult");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Google
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        //Facebook
        if (mCallbackManager != null)
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        if (progressDialog != null)
            return;

        switch (v.getId()) {
            case R.id.login_kakao:
                kakaoRedirectLoginActivity();
                break;
            case R.id.login_google:
                googleSignInApp();
                break;
            case R.id.login_facebook:
                facebookLoginApp();
                break;
            case R.id.login_naver:
                naverLoginApp();
                break;
            case R.id.mt_pincode:
                finishActivity("PINCODE");
                break;
            case R.id.mt_cid_type:
                setCidType();
                break;
            case R.id.mt_kt_friend:
                shareKakaoV2MT();
                break;
            case R.id.mt_kt_inquire:
                inquireKakaoMT();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    //google
    private void googleInit() {
        if(mGoogleApiClient != null)
            return;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.default_web_client_id))
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void googleSignInApp() {
        googleInit();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(THIS_FILE, "Google Login Status : " + result.isSuccess());
        if (result.isSuccess()) {
            if (progressDialog == null) {
                showProgress();
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.d(THIS_FILE, "Google userProfile " + acct.getId());
                Log.d(THIS_FILE, "Google userProfile " + acct.getDisplayName());
                Log.d(THIS_FILE, "Google userProfile " + acct.getEmail());
                Log.d(THIS_FILE, "Google userProfile " + acct.getPhotoUrl());
                mOauthID = acct.getId();
                mOauthType = "google";
                mOauthEmail = acct.getEmail();
                insertMyinfo();
            }
        } else {
            Log.d(THIS_FILE, "Google Status : " + result.getStatus());
            Alert(getString(R.string.setting_edns_fail));
        }
    }

    // [START signOut]
    private void googleSignOutApp() {
        googleInit();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Log.d(THIS_FILE, "Google signOut : " + status);
                        //Builds a fresh instance of GoogleApiClient
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void googleRevokeAccessApp() {
        googleInit();
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Log.d(THIS_FILE, "Google revokeAccess : " + status);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]


    //Kakao
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSessionCallback != null)
            Session.getCurrentSession().removeCallback(mSessionCallback);
        if (mProfileTracker != null)
            mProfileTracker.stopTracking();
    }


    protected void kakaoRedirectSignupActivity() {
        kakaoV2RequestMe();
    }

    /*private void kakaoRequestMe() {
        List<String> propertyKeys = new ArrayList<String>();
        propertyKeys.add("kaccount_email");
        propertyKeys.add("nickname");
        propertyKeys.add("profile_image");
        propertyKeys.add("thumbnail_image");
        UserManagement.requestMe(new MeResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                Logger.d(message);
                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
                if (result == ErrorCode.CLIENT_ERROR_CODE) {
                    Log.d(THIS_FILE, "Kakao onFailure : " + message);
                    finish();
                } else {
                    Alert(getString(R.string.setting_edns_fail));
                    //redirectLoginActivity();
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.d(THIS_FILE, "Kakao onSessionClosed");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                if (progressDialog == null) {
                    showProgress();
                    Log.d(THIS_FILE, "Kakao userProfile " + userProfile.getId());
                    Log.d(THIS_FILE, "Kakao userProfile " + userProfile.getNickname());
                    Log.d(THIS_FILE, "Kakao userProfile " + userProfile.getThumbnailImagePath());
                    Log.d(THIS_FILE, "Kakao userProfile " + userProfile.getProfileImagePath());
                    Log.d(THIS_FILE, "Kakao userProfile " + userProfile.getEmail());
                    mOauthID = Long.toString(userProfile.getId());
                    mOauthType = "kakao";
                    mOauthEmail = userProfile.getEmail();
                    insertMyinfo();
                }
            }

            @Override
            public void onNotSignedUp() {
                Log.d(THIS_FILE, "Kakao onNotSignedUp");
            }

        }, propertyKeys, false);

    }*/

    /**
     * 사용자의 상태를 알아 보기 위해 me API 호출을 한다.
     */
    protected void kakaoV2RequestMe() {
        List<String> propertyKeys = new ArrayList<String>();
        propertyKeys.add("kakao_account.email");
        propertyKeys.add("properties.nickname");
        propertyKeys.add("properties.profile_image");
        propertyKeys.add("properties.thumbnail_image");
        UserManagement.getInstance().me(propertyKeys, new MeV2ResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                Logger.d(message);

                int result = errorResult.getErrorCode();
                Log.d(THIS_FILE, "Kakao onFailure result code : " + result);
                if (result == ApiErrorCode.CLIENT_ERROR_CODE) {
                    Log.d(THIS_FILE, "Kakao onFailure : " + message);
                    finish();
                } else {
                    Alert(getString(R.string.setting_edns_fail));
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.d(THIS_FILE, "Kakao onSessionClosed");
            }

            @Override
            public void onSuccess(MeV2Response result) {
                if (progressDialog == null) {
                    showProgress();
                    Log.d(THIS_FILE, "Kakao userProfile " + result.getId());
                    Log.d(THIS_FILE, "Kakao userProfile " + result.getNickname());
                    mOauthID = Long.toString(result.getId());
                    mOauthType = "kakao";
                    mOauthEmail = result.getKakaoAccount().getEmail();
                    insertMyinfo();
                }
            }
        });
    }

    //Kakao
    public void kakaoRedirectLoginActivity() {
        //Kakao
        kakaoLogoutApp();
    }

    private void kakaoLogoutApp() {
        UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
                Log.d(THIS_FILE, "Kakao Logout");

                mSessionCallback = new SessionCallback();
                Session.getCurrentSession().addCallback(mSessionCallback);
                Session.getCurrentSession().checkAndImplicitOpen();
                new KakaoLoginControl(mContext).call();
            }
        });

    }

    /*private void shareKakaoMT() {
        {
            try {
                final KakaoLink kakaoLink = KakaoLink.getKakaoLink(this);
                final KakaoTalkLinkMessageBuilder kakaoBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();

                *//*메시지 추가*//*
                kakaoBuilder.addText(getString(R.string.mt_kt_desc));

                *//*이미지 가로/세로 사이즈는 80px 보다 커야하며, 이미지 용량은 500kb 이하로 제한된다.*//*
                String url = "http://info.maaltalk.com/maaltalk_user/img/mlink.png";
                kakaoBuilder.addImage(url, 144, 144);

                *//*앱 실행버튼 추가*//*
                kakaoBuilder.addAppButton(getString(R.string.mt_use));

                *//*메시지 발송*//*
                kakaoLink.sendMessage(kakaoBuilder, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    private void shareKakaoV2MT() {
        FeedTemplate params = FeedTemplate
                .newBuilder(ContentObject.newBuilder(getString(R.string.mt_kt_desc),
                        "http://info.maaltalk.com/maaltalk_user/img/mlink.png",
                        LinkObject.newBuilder().setWebUrl("https://developers.kakao.com")
                                .setMobileWebUrl("https://developers.kakao.com").build())
                        .build())
                .addButton(new ButtonObject("앱으로 보기", LinkObject.newBuilder()
                        .setAndroidExecutionParams("key1=value1")
                        .setIosExecutionParams("key1=value1")
                        .build()))
                .build();

        KakaoLinkService.getInstance().sendDefault(this, params, serverCallbackArgs, callback);
    }

    private void inquireKakaoMT() {
        PackageManager pm = mContext.getPackageManager();
        try {
            Intent intent = pm
                    .getLaunchIntentForPackage(Setting.KAKAO_PACKAGE_NAME);
            if (intent != null) {
                intent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("kakaoplus://plusfriend/friend/@말톡"));
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Uri uri = Uri.parse(DialMain.MAALTALK_EMAIL);
                intent = new Intent(Intent.ACTION_SENDTO, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                    .parse("https://play.google.com/store/apps/details?id=com.google.android.gm"));
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    //Facebook
    private void facebookLoginApp() {
        facebookLogoutApp();
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) { //로그인 성공시 호출되는 메소드
                Log.d(THIS_FILE, "onSuccess");
                if (progressDialog == null) {
                    showProgress();
                }
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getToken());
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getUserId());
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getPermissions() + " ");
                mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        Log.d(THIS_FILE, "ProfileTracker " + currentProfile);
                        mProfileTracker.stopTracking();
                        mAccessToken = loginResult.getAccessToken();
                        facebookGetProfile();
                    }
                };
                mProfileTracker.startTracking();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(THIS_FILE, "onError");
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
                Alert(getString(R.string.setting_edns_fail));
            }

            @Override
            public void onCancel() {
                Log.d(THIS_FILE, "onError");
            }
        });

    }

    private void facebookLogoutApp() {
        // Logout from Facebook
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
            Log.d(THIS_FILE, "Facebook Logout");
        }
    }

    private void facebookRevokeApp() {
        if (AccessToken.getCurrentAccessToken() != null) {
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions", null, HttpMethod.DELETE, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    boolean isSuccess = false;
                    try {
                        isSuccess = response.getJSONObject().getBoolean("success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (isSuccess && response.getError() == null) {
                        // Application deleted from Facebook account
                        Log.d(THIS_FILE, "Facebook Revoke");
                    }

                }
            }).executeAsync();

        }
    }

    private void facebookGetProfile() {
        Log.d(THIS_FILE, "facebookGetProfile");
        GraphRequest request = GraphRequest.newMeRequest(mAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.d(THIS_FILE, "Facebook " + object.toString());
                            Log.d(THIS_FILE, "Facebook " + object.optString("id"));
                            Log.d(THIS_FILE, "Facebook " + object.optString("name"));
                            Log.d(THIS_FILE, "Facebook " + object.optString("email"));
                            Profile profile = Profile.getCurrentProfile();
                            if(profile != null)
                                Log.d(THIS_FILE, profile.getProfilePictureUri(300, 300).toString() + " ");
                            mOauthID = object.optString("id");
                            mOauthType = "facebook";
                            mOauthEmail = object.optString("email");
                            insertMyinfo();
                        } catch (Exception e) {
                            Log.d(THIS_FILE, e.toString());
                            e.printStackTrace();
                        }
                    }

                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    //Naver
    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            if (success) {
                if (progressDialog == null) {
                    showProgress();
                    String accessToken = mOAuthLoginModule.getAccessToken(mContext);
                    String refreshToken = mOAuthLoginModule.getRefreshToken(mContext);
                    long expiresAt = mOAuthLoginModule.getExpiresAt(mContext);
                    String tokenType = mOAuthLoginModule.getTokenType(mContext);
                    Log.d(THIS_FILE, "Naver getState : " + mOAuthLoginModule.getState(mContext).toString());
                    new naverRequestApiTask().execute();
                }
            } else {
                String errorCode = mOAuthLoginModule.getLastErrorCode(mContext).getCode();
                String errorDesc = mOAuthLoginModule.getLastErrorDesc(mContext);
                Log.d(THIS_FILE, "Naver errorCode : " + errorCode + " errorDesc : " + errorDesc);
                if (errorCode != null && !errorCode.equals("user_cancel")) {
                    Alert(getString(R.string.setting_edns_fail));
                }
            }
        }

    };

    private void naverInit() {
        if(mOAuthLoginModule != null)
            return;

        mOAuthLoginModule = OAuthLogin.getInstance();
        mOAuthLoginModule.init(
                OauthLoginActivity.this
                , getString(R.string.naver_client_id)
                , getString(R.string.naver_client_secret)
                , "Maaltalk"
                //,OAUTH_CALLBACK_INTENT
                // SDK 4.1.4 버전부터는 OAUTH_CALLBACK_INTENT변수를 사용하지 않습니다.
        );
    }

    private void naverLoginApp() {
        //Naver
        naverInit();
        mOAuthLoginModule.startOauthLoginActivity((Activity) mContext, mOAuthLoginHandler);
    }

    private void naverLogoutApp() {
        naverInit();
        mOAuthLoginModule.logout(mContext);
        if (OAuthLogin.getInstance().getState(mContext) == OAuthLoginState.NEED_LOGIN) {
            Log.d(THIS_FILE, "Naver Logout");
        }
    }

    private void naverRevoke() {
        naverInit();
        mOAuthLoginModule.logout(mContext);
        new DeleteTokenTask().execute();
    }

    private class DeleteTokenTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            boolean isSuccessDeleteToken = mOAuthLoginModule.logoutAndDeleteToken(mContext);

            if (!isSuccessDeleteToken) {
                // 서버에서 토큰 삭제에 실패했어도 클라이언트에 있는 토큰은 삭제되어 로그아웃된 상태입니다.
                // 클라이언트에 토큰 정보가 없기 때문에 추가로 처리할 수 있는 작업은 없습니다.
                Log.d(THIS_FILE, "NavererrorCode : " + OAuthLogin.getInstance().getLastErrorCode(mContext));
                Log.d(THIS_FILE, "Naver errorDesc : " + OAuthLogin.getInstance().getLastErrorDesc(mContext));
            }

            return null;
        }

        protected void onPostExecute(Void v) {
            Log.d(THIS_FILE, "Naver Revoke");
        }
    }

    private class naverRequestApiTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            Log.d(THIS_FILE, "Naver RequestApiTask onPreExecute");
        }

        @Override
        protected String doInBackground(Void... params) {
            String url = "https://openapi.naver.com/v1/nid/getUserProfile.xml";
            String at = mOAuthLoginModule.getAccessToken(mContext);
            return mOAuthLoginModule.requestApi(mContext, at, url);
        }

        protected void onPostExecute(String content) {
            Log.d(THIS_FILE, "Naver RequestApiTask onPostExecute");
            String f_array[] = new String[9];
            try {
                XmlPullParserFactory parserCreator = XmlPullParserFactory
                        .newInstance();
                XmlPullParser parser = parserCreator.newPullParser();

                InputStream input = new ByteArrayInputStream(
                        content.getBytes("UTF-8"));
                parser.setInput(input, "UTF-8");


                int parserEvent = parser.getEventType();
                String tag;
                boolean inText = false;
                boolean lastMatTag = false;
                int colIdx = 0;

                while (parserEvent != XmlPullParser.END_DOCUMENT) {
                    switch (parserEvent) {
                        case XmlPullParser.START_TAG:
                            tag = parser.getName();
                            if (tag.compareTo("xml") == 0) {
                                inText = false;
                            } else if (tag.compareTo("data") == 0) {
                                inText = false;
                            } else if (tag.compareTo("result") == 0) {
                                inText = false;
                            } else if (tag.compareTo("resultcode") == 0) {
                                inText = false;
                            } else if (tag.compareTo("message") == 0) {
                                inText = false;
                            } else if (tag.compareTo("response") == 0) {
                                inText = false;
                            } else {
                                inText = true;
                            }
                            break;
                        case XmlPullParser.TEXT:
                            tag = parser.getName();
                            if (inText) {
                                if (parser.getText() == null) {
                                    f_array[colIdx] = "";
                                } else {
                                    f_array[colIdx] = parser.getText().trim();
                                }
                                colIdx++;
                            }
                            inText = false;
                            break;

                        case XmlPullParser.END_TAG:
                            tag = parser.getName();
                            inText = false;
                            break;
                    }
                    parserEvent = parser.next();

                }

            } catch (Exception e) {
                Log.e(THIS_FILE, "Error in network call", e);
                e.printStackTrace();
            }

            Log.d(THIS_FILE, "Naver userProfile " + f_array[0]);
            Log.d(THIS_FILE, "Naver userProfile " + f_array[2]);
            Log.d(THIS_FILE, "Naver userProfile " + f_array[7]);
            mOauthID = f_array[0];
            mOauthType = "naver";
            mOauthEmail = f_array[7];
            insertMyinfo();
        }

    }

    private void insertMyinfo() {
        (new AsyncTask<Void, Void, String>(){
            @Override
            protected String doInBackground(Void... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("name", mUserID));
                nameValuePairs.add(new BasicNameValuePair("oauth_id", mOauthID));
                nameValuePairs.add(new BasicNameValuePair("oauth_type", mOauthType));
                nameValuePairs.add(new BasicNameValuePair("email", mOauthEmail));
                String rs = postServer(mContext, mMyinfoURL, nameValuePairs); //postMyinfo(false);
                return rs;
            }

            @Override
            protected void onPostExecute(String rs) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(rs);
                    JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                    Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                    Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                    if (responseObject.getInt("RES_CODE") == 0) {
                        finishActivity(responseObject.getString("RES_DESC"));
                    } else {
                        finishActivity(responseObject.getString("RES_DESC"));
                    }
                } catch (Exception e) {
                    Log.e(THIS_FILE, "JSON", e);
                    Log.d(THIS_FILE, "JSON ERROR");
                    finishActivity("FAIL");
                }

                super.onPostExecute(rs);
            }
        }).execute();
    }

    private void cofirmMyinfo() {
        (new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                activity_login_activity.setVisibility(View.GONE);
                showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("name", mUserID));
                String rs = postServer(mContext, mMyinfoURL, nameValuePairs); //postMyinfo(true);
                return rs;
            }

            @Override
            protected void onPostExecute(String rs) {
                hideLoadingDialog();

                String result = "";
                JSONObject jObject;
                try {
                    jObject = new JSONObject(rs);
                    JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                    Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                    Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                    if (responseObject.getInt("RES_CODE") == 0) {
                        result = responseObject.getString("RES_DESC");
                    }
                } catch (Exception e) {
                    Log.e(THIS_FILE, "JSON", e);
                    Log.d(THIS_FILE, "JSON ERROR");
                    result = "FAIL";
                }

                if (result != null && result.length() > 0) {
                    finishActivity(result);
                }else {
                    activity_login_activity.setVisibility(View.VISIBLE);
                }

                super.onPostExecute(rs);
            }
        }).execute();
    }

    private void setCidType() {
        String dnid = mPrefs.getPreferenceStringValue(AppPrefs.CONF_NUM_070);//ServicePrefs.mUser070;
        if (dnid != null && dnid.length() > 0) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("name", mUserID));
            String rs = postServer(mContext, mCidTypeURL, nameValuePairs); //postCidType(false);
            JSONObject jObject;
            try {
                jObject = new JSONObject(rs);
                JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                if (responseObject.getInt("RES_CODE") == 0)
                    mCidType = responseObject.getString("CID_TYPE");
                else
                    mCidType = "";
            } catch (Exception e) {
                Log.e(THIS_FILE, "JSON", e);
                Log.d(THIS_FILE, "JSON ERROR");
                mCidType = "";
            }

            if (mCidType == "") {
                Alert(getResources().getString(R.string.setting_edns_fail));
                return;
            }

            final AlertDialog cidtypeBuilder = new AlertDialog.Builder(mContext).setPositiveButton(getResources().getString(R.string.change), null).setNegativeButton(getResources().getString(R.string.cancel), null).create();
            LayoutInflater inflater = OauthLoginActivity.this.getLayoutInflater();
            final View view_cid_type = inflater.inflate(R.layout.cid_type, null);

            final RadioGroup rg_dispatch = (RadioGroup) view_cid_type.findViewById(R.id.rg_cid_type);
            RadioButton rg_cid = (RadioButton) view_cid_type.findViewById(R.id.rg_cid);
            RadioButton rg_dnid = (RadioButton) view_cid_type.findViewById(R.id.rg_dnid);
            rg_cid.setText(mPrefs.getPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER));
            rg_dnid.setText(dnid);
            if (mCidType.equals("c"))
                rg_cid.setChecked(true);
            else
                rg_dnid.setChecked(true);

            cidtypeBuilder.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    Button positiveButton = ((AlertDialog) arg0).getButton(AlertDialog.BUTTON_POSITIVE);
                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            RadioButton selectRadio = (RadioButton) view_cid_type.findViewById(rg_dispatch
                                    .getCheckedRadioButtonId());
                            String cid_type = "";
                            String number = selectRadio.getText().toString();
                            if (number.substring(0, 3).equals("070"))
                                cid_type = "d";
                            else
                                cid_type = "c";
                            if (mCidType.equals(cid_type)) {
                                cidtypeBuilder.dismiss();
                                return;
                            } else {
                                mCidType = cid_type;
                                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                                nameValuePairs.add(new BasicNameValuePair("name", mUserID));
                                nameValuePairs.add(new BasicNameValuePair("cid_type", mCidType));
                                String rs = postServer(mContext, mCidTypeURL, nameValuePairs); //postCidType(true);
                                JSONObject jObject;
                                try {
                                    jObject =

                                            new JSONObject(rs);
                                    JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                                    Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                                    Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                                    if (responseObject.getInt("RES_CODE") == 0)
                                        mCidType = responseObject.getString("CID_TYPE");
                                    else
                                        mCidType = "";
                                } catch (Exception e) {
                                    Log.e(THIS_FILE, "JSON", e);
                                    Log.d(THIS_FILE, "JSON ERROR");
                                    mCidType = "";
                                }

                                String msg = getResources().getString(R.string.error_did);
                                if (mCidType != null && mCidType.length() > 0)
                                    msg = number + getResources().getString(R.string.mt_cid_type_success);
                                cidtypeBuilder.dismiss();
                                Alert(msg);
                            }
                        }
                    });
                    Button negativetiveButton = ((AlertDialog) arg0).getButton(AlertDialog.BUTTON_NEGATIVE);
                    negativetiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            cidtypeBuilder.dismiss();
                        }
                    });
                }
            });
            cidtypeBuilder.setView(view_cid_type);
            cidtypeBuilder.setTitle(getResources().getString(R.string.mt_cid_type));
            cidtypeBuilder.show();
        } else {
            Alert(getResources().getString(R.string.error_did));
        }
    }

    public static String postServer(Context context, String url, List<NameValuePair> nameValuePairs) {
        Log.d(THIS_FILE, "nameValuePairs" + nameValuePairs);
        if (url == null || url.length() == 0)
            return "";
        if (nameValuePairs == null)
            return "";

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            setConnectionTimeout(params, 20000);
            setSoTimeout(params, 20000);

            HttpPost httppost = new HttpPost(url);

            // Add your data
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
                    HTTP.UTF_8));
            // Execute HTTP Post Request
            // Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            Log.d(THIS_FILE, "HTTPS POST EXEC : " + sb.toString());

            // CHECK STATUS CODE
            if (response.getStatusLine().getStatusCode() != 200) {
                Log.e(THIS_FILE, "HTTPS POST STATUS : "
                        + response.getStatusLine().toString());
                return null;
            }

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;

    }

    private void Alert(String msg) {
        AlertDialog.Builder msgDialog = new AlertDialog.Builder(mContext);
        msgDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        msgDialog.setMessage(msg);
        msgDialog.setIcon(R.drawable.ic_launcher);
        msgDialog.setNegativeButton(
                mContext.getResources().getString(R.string.close), null);
        msgDialog.show();
    }

    private ProgressDialog progressDialog = null;
    private Handler autoHideHandler = null;
    private Runnable autoHideRunnable = null;

    private void showProgress() {
        Log.d(THIS_FILE, "showProgress");
        if (mActivity!=null && !mActivity.isFinishing()){
            progressDialog = ProgressDialog.show(mActivity, mActivity.getResources().getString(R.string.mt_login),
                    mActivity.getResources().getString(R.string.mt_myinfo_progress), true, true);
            progressDialog.show();

            autoHideRunnable = new Runnable() {
                @Override
                public void run() {
                    hideProgres();
                    finishActivity("FAIL");
                }
            };

            autoHideHandler = new Handler();
            autoHideHandler.postDelayed(autoHideRunnable, 10000);
        }
    }


    private void hideProgres() {
        Log.d(THIS_FILE, "hideProgres");
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (autoHideHandler != null) {
            autoHideHandler.removeCallbacks(autoHideRunnable);
            autoHideHandler = null;
            autoHideRunnable = null;
        }

    }

    private ResponseCallback<KakaoLinkResponse> callback;
    private Map<String, String> serverCallbackArgs = getServerCallbackArgs();

    private Map<String, String> getServerCallbackArgs() {
        Map<String, String> callbackParameters = new HashMap<>();
        return callbackParameters;
    }

    private String getKeyHash(final Context context) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Log.w(THIS_FILE, "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
        return null;
    }
}

