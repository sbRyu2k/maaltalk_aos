package com.dial070

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyCallback
import android.telephony.TelephonyManager
import androidx.appcompat.app.AppCompatActivity

class Test: AppCompatActivity() {

    val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            telephonyManager.registerTelephonyCallback(
                mainExecutor,
                object: TelephonyCallback(), TelephonyCallback.CallStateListener {
                    override fun onCallStateChanged(state: Int) {
                        TODO("Not yet implemented")
                    }
                }
            )
        }
    }


}