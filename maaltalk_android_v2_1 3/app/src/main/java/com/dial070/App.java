package com.dial070;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.dial070.global.ServicePrefs;
import com.dial070.oauth.KakaoSDKAdapter;
import com.dial070.service.Fcm;
import com.dial070.status.CallStatus;
import com.dial070.ui.AppLifeObserver;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.kakao.auth.KakaoSDK;


import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class App extends MultiDexApplication {
    private static final String THIS_FILE = "DIAL070_APP";
    public static boolean isDialMainForeground = false;
    public static boolean isDialForeground = false;
    public static boolean isDialClicked = false;
    public static boolean isReturnClicked = false;
    public static boolean isReturnContactClicked = false;
    public static boolean isCallTakeClicked = false;
    public static boolean isOauth = false;
    public static String INFO_JSON_VERSION = "201909160003";
    //BJH 2017.05.22
    private static volatile App obj = null;
    private static volatile Activity currentActivity = null;
    public Uri m_uriResult;
    public boolean b_type = false;
    public boolean isAd = false;    //배너 관련 변수
    //BJH 푸시 성공률 관련 변수
    public String mGcm = null;
    public String mPushy = null;
    public long mGcmTime = 0;
    public long mPushyTime = 0;
    private AppPrefs mPrefs;
    private int running = 0;
    private AppStatus mAppStatus = AppStatus.FOREGROUND;
    private TelephonyManager mTelephonyManager = null;
    // incoming 수신 플래그
    private static boolean incomingFlag = false;
    //public static boolean callBridgeFlag = false;
    //private static BridgePopupReturnService mBprs;
    //private static boolean isBridgePopupReturnService = false;
    public static int CallState = TelephonyManager.CALL_STATE_IDLE;

    ActivityLifecycleCallbacks mActivityLifecycleCallbacks =
            new ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                    Log.d(THIS_FILE, "onActivityCreated:" + activity.getClass().getSimpleName().trim());
                    if (activity.getClass().getSimpleName().trim().equals("Dial")) {
                        isDialForeground = true;
                        isDialClicked = false;
                    }
                }

                @Override
                public void onActivityStarted(Activity activity) {
                    Log.d(THIS_FILE, "onActivityStarted:" + activity.getClass().getSimpleName().trim());

                    if (++running == 1) {
                        // running activity is 1,
                        // app must be returned from background just now (or first launch)
                        mAppStatus = AppStatus.RETURNED_TO_FOREGROUND;
                    } else if (running > 1) {
                        // 2 or more running activities,
                        // should be foreground already.
                        mAppStatus = AppStatus.FOREGROUND;
                    }
/*   new version check shlee
                    if (Build.VERSION.SDK_INT >= 29) {
                        if (activity.getClass().getSimpleName().trim().equals("DialMain")) {
                            Fcm.stopRing();
                        }
                    }

 */
                }

                @Override
                public void onActivityResumed(Activity activity) {
                    Log.d(THIS_FILE, "onActivityResumed:" + activity.getClass().getSimpleName().trim());
                    if (activity.getClass().getSimpleName().trim().equals("DialMain")) {
                        isDialMainForeground = true;
                    } else if (activity.getClass().getSimpleName().trim().equals("Dial")) {
                        isDialForeground = true;
                    }
                }

                @Override
                public void onActivityPaused(Activity activity) {
                    Log.d(THIS_FILE, "onActivityPaused:" + activity.getClass().getSimpleName().trim());
                    if (activity.getClass().getSimpleName().trim().equals("DialMain")) {
                        isDialMainForeground = false;
                    }

                    if (activity.getClass().getSimpleName().trim().equals("Dial")) {
                        isDialForeground = false;
                    }
                }

                @Override
                public void onActivityStopped(Activity activity) {
                    Log.d(THIS_FILE, "onActivityStopped:" + activity.getClass().getSimpleName().trim());

                    if (--running == 0) {
                        // no active activity
                        // app goes to background
                        mAppStatus = AppStatus.BACKGROUND;
                    }

                    /*if (mAppStatus == AppStatus.BACKGROUND) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (Settings.canDrawOverlays(obj)) {
                                if (callBridgeFlag){
                                    //Intent intent = new Intent(obj, BridgePopupReturnService.class);
                                    //obj.startService(intent);
                                    if (isBridgePopupReturnService){
                                        mBprs.showView();
                                    }
                                }
                            }
                        }
                    }*/
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {
                }
            };

    public static App getGlobalApplicationContext() {
        return obj;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    /**
     * 2020.08.12
     * sbryu
     * debug log flag
     */
    public static boolean DEBUG = true;

    // Activity가 올라올때마다 Activity의 onCreate에서 호출해줘야한다.
    public static void setCurrentActivity(Activity currentActivity) {
        App.currentActivity = currentActivity;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * 2020.08.12
     * sbryu
     * 기능 : 디버그 로그 출력을 위한 모드 선택 함수
     */

    private boolean isDebuggable(Context context) {
        boolean debuggable = false;

        PackageManager pm = context.getPackageManager();

        try {
            ApplicationInfo appInfo = pm.getApplicationInfo(context.getPackageName(), 0);
            debuggable = (0 != (appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return debuggable;
//        return true;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        obj = this;
        KakaoSDK.init(new KakaoSDKAdapter());

        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifeObserver(this));

        this.DEBUG = isDebuggable(this);
        CallStatus.INSTANCE.init();

        DebugLog.d("onCreate() called...");

        mPrefs = new AppPrefs(this);
        FirebaseCrashlytics.getInstance().setUserId(mPrefs.getPreferenceStringValue(AppPrefs.USER_ID));
        //registerActivityLifecycleCallbacks (new MyActivityLifecycleCallbacks ());

        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks);

        //mTelephonyManager = (TelephonyManager) obj.getSystemService(Context.TELEPHONY_SERVICE);
        //mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public int requestNewGlobalInfo(final Context activity)    //20190101 새로운 info.json 로딩방식 적용
    {
        // SEND QUERY
        Log.d(THIS_FILE, "REQUEST NEW GLOBAL INFO : START");


        String localVersion = mPrefs.getPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL); //기존 말톡에 셋업된 info.json 버전 받아오기
        Log.d(THIS_FILE, "localInfoVersion : " + localVersion);
        if (localVersion.compareTo(INFO_JSON_VERSION) < 0) {
            setGlobalInfo();
            localVersion = mPrefs.getPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL); //기존 말톡에 셋업된 info.json 버전 받아오기
            Log.d(THIS_FILE, "setup localInfoVersion : " + localVersion);
        }

        String serverInfoJsonVer = mPrefs.getPreferenceStringValue(AppPrefs.LATEST_GLOBAL_INFO_SERIAL);
        Log.d(THIS_FILE, "INFO JSON SERVER VERSION=" + serverInfoJsonVer);
        if (serverInfoJsonVer.equals("201901010001")) {
            Log.d(THIS_FILE, "This position STOP...");
            return 0;
        }

        if (serverInfoJsonVer.compareTo(localVersion) < 1) {    //앞변수가 뒷변수보다 작거나 같으면 패스
            Log.d(THIS_FILE, "This position STOP");
            return 0;
        }

        String rs = postNewGlobalInfo();
        Log.d(THIS_FILE, "REQUEST NEW GLOBAL INFO : END");
        Log.d(THIS_FILE, "NewGlobalInfo RESULT : " + rs);

        DebugLog.d("REQUEST NEW GLOBAL INFO --> END");
        DebugLog.d("NewGlobalInfo RESULT --> "+rs);

        if (rs == null || rs.length() == 0) return -1;

        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);

            JSONObject infoObject = jObject.getJSONObject("INFO_URL");

            String serialAndroidInfo = infoObject.getString("S");
            Log.d("info_android.json serial", serialAndroidInfo);

            JSONArray urlItemArray = infoObject.getJSONArray("ITEMS");
            if (urlItemArray == null) return -1;

            Log.d(THIS_FILE, "This position PASS 1 !!!");
            mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL, serialAndroidInfo);

            String Name = null;
            String Url = null;
            String Serial = null;


            for (int i = 0; i < urlItemArray.length(); i++) {
                Name = urlItemArray.getJSONObject(i).getString("NAME").toString();
                Url = urlItemArray.getJSONObject(i).getString("URL").toString();

                Log.d(THIS_FILE, "NAME=" + urlItemArray.getJSONObject(i).getString("NAME"));
                Log.d(THIS_FILE, "URL=" + urlItemArray.getJSONObject(i).getString("URL"));

                if (Name == null) continue;


                if (Name.equalsIgnoreCase("REQ_CLIENT_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO, Url);
                } else if (Name.equalsIgnoreCase("REQ_SVC_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO, Url);
                }

                // 인증
                else if (Name.equalsIgnoreCase("REQ_VALIDCID_LOCL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL, Url);
                } else if (Name.equalsIgnoreCase("REQ_VALIDCID_CODE")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_CODE, Url);
                } else if (Name.equalsIgnoreCase("REQ_VALIDCID_INTL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL, Url);
                } else if (Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL, Url);
                } else if (Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL, Url);
                } else if (Name.equalsIgnoreCase("REQ_VALIDCID_LOCL_V2")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2, Url);
						/*Serial = "";
						if (urlItemArray.getJSONObject(i).has("ENC_V2"))
						{
							Serial = urlItemArray.getJSONObject(i).getString("ENC_V2").toString();
						}
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, Serial);*/
                } else if (Name.equalsIgnoreCase("ENC_V2"))    //20190110 json 일관성을 위해서 REQ_VALIDCID_LOCL_V2 안에 있던 ENC_V2을 따로 밖으로 뺌.
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, Url);
                } else if (Name.equalsIgnoreCase("REQ_VALIDCID_GLOBAL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL, Url);

                } else if (Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V2")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2, Url);
                }

                //BJH 2017.04.24 인증 변경
                else if (Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL_V2")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2, Url);
                } else if (Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V3")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3, Url);
                } else if (Name.equalsIgnoreCase("POPUP")) {
                    Serial = "";
                    if (urlItemArray.getJSONObject(i).has("SERIAL")) {
                        Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
                    }
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP, Url);
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP_SERIAL, Serial);
                }

                // 결제
                else if (Name.equalsIgnoreCase("REQ_BALANCE")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_BALANCE, Url);
                } else if (Name.equalsIgnoreCase("ADD_BALANCE")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE, Url);
                } else if (Name.equalsIgnoreCase("ADD_BALANCE_EXTRA")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA, Url);
                } else if (Name.equalsIgnoreCase("MY_INFORMATION")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION, Url);
                }

                // 통화기록
                else if (Name.equalsIgnoreCase("CDR_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_CDR_INFO, Url);
                } else if (Name.equalsIgnoreCase("REC_DIR")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REC_DIR, Url);
                } else if (Name.equalsIgnoreCase("MESSAGE_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO, Url);
                }

                // 로그
                else if (Name.equalsIgnoreCase("REQ_HISTORY")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_HISTORY, Url);
                }

                // 게시판
                else if (Name.equalsIgnoreCase("SERVICE_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_SERVICE_INFO, Url);
                } else if (Name.equalsIgnoreCase("MANUAL")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_MANUAL, Url);
                } else if (Name.equalsIgnoreCase("NOTICE")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_NOTICE, Url);
                } else if (Name.equalsIgnoreCase("FAQ")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_FAQ, Url);
                } else if (Name.equalsIgnoreCase("QUESTION")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_QUESTION, Url);
                } else if (Name.equalsIgnoreCase("KAKAO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_KAKAO, Url);
                } else if (Name.equalsIgnoreCase("RATE_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE_INFO, Url);
                } else if (Name.equalsIgnoreCase("RATE_INFO_URL")) // BJH 2017.01.20
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE, Url);
                } else if (Name.equalsIgnoreCase("TERMS")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS, Url);
                } else if (Name.equalsIgnoreCase("TERMS_TXT")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_TXT, Url);
                    if (urlItemArray.getJSONObject(i).has("INFO"))//BJH 2016.06.28 Terms 추가
                    {
                        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT, urlItemArray.getJSONObject(i).getString("INFO").toString());
                        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT, urlItemArray.getJSONObject(i).getString("USE").toString());
                    }
                }
                //BJH 광고, 메시지 보내기, 서버 업데이트
                else if (Name.equalsIgnoreCase("AD_INFO")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_AD_INFO, Url);
                } else if (Name.equalsIgnoreCase("MSG_SEND")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, Url);
                } else if (Name.equalsIgnoreCase("UPDATE_SVR")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_UPDATE_SVR, Url);
                } else if (Name.equalsIgnoreCase("USER_MSG"))//BJH
                {
                    Serial = "";
                    if (urlItemArray.getJSONObject(i).has("SERIAL")) {
                        Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
                    }
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG, Url);
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL, Serial);
                } else if (Name.equalsIgnoreCase("PUSH_MSG_INFO"))//BJH
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO, Url);
                } else if (Name.equalsIgnoreCase("REQ_FORWARDING_INFO"))//BJH 2016.11.09
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO, Url);
                } else if (Name.equalsIgnoreCase("REQ_FORWARDING"))//BJH 2016.11.09
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING, Url);
                } else if (Name.equalsIgnoreCase("PUSH_RECORD"))//BJH 2016.12.14
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_RECORD, Url);
                } else if (Name.equalsIgnoreCase("ANDROID_VERSION"))//BJH
                {
                    String version = "";
                    if (urlItemArray.getJSONObject(i).has("VERSION")) {
                        version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
                        mPrefs.setPreferenceStringValue(AppPrefs.NEW_VERSION, version);
                    }

                } else if (Name.equalsIgnoreCase("OPEN_DNS_SERVER"))//BJH
                {
                    String domain = "";
                    String server = "";
                    if (urlItemArray.getJSONObject(i).has("DOMAIN")) {
                        domain = urlItemArray.getJSONObject(i).getString("DOMAIN").toString();
                        mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN, domain);
                    }
                    if (urlItemArray.getJSONObject(i).has("SERVER")) {
                        server = urlItemArray.getJSONObject(i).getString("SERVER").toString();
                        mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER, server);
                    }
                } else if (Name.equalsIgnoreCase("USIM_INFO_VERSION"))//BJH 2017.01.05
                {
                    String version = "";
                    if (urlItemArray.getJSONObject(i).has("VERSION")) {
                        version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
                        mPrefs.setPreferenceStringValue(AppPrefs.INFO_USIM_VER, version);
                    }
                } else if (Name.equalsIgnoreCase("REQ_SVC_OUT"))//BJH
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_SVC_OUT, Url);
                } else if (Name.equalsIgnoreCase("USIM_POCKET_WIFI"))//BJH 2016.08.12
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI, Url);
                } else if (Name.equalsIgnoreCase("BRIDGE_PAYMENT"))//BJH 2016.11.24
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT, Url);
                } else if (Name.equalsIgnoreCase("USIM_LOOKUP"))//BJH 2017.02.02
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_LOOKUP, Url);
                } else if (Name.equalsIgnoreCase("USIM_ACTIVATION"))//BJH 2017.03.15
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_ACTIVATION, Url);
                } else if (Name.equalsIgnoreCase("REQ_MYINFO"))//BJH 2017.06.05
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_MYINFO, Url);
                } else if (Name.equalsIgnoreCase("REQ_CID_TYPE"))//BJH 2017.06.05
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE, Url);
                } else if (Name.equalsIgnoreCase("REQ_SVC_OUT_V2"))//BJH 2017.06.09
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2, Url);
                } else if (Name.equalsIgnoreCase("REQ_GET_POCKET"))//BJH 2017.11.08
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET, Url);
                } else if (Name.equalsIgnoreCase("COUPON_INFO"))// 2018.10.08
                {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, Url);
                } else if (Name.equalsIgnoreCase("USER_TIME")) {
                    mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_TIME, Url);
                }

            }
            // CHECK VALUE
            return 0;
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            return -1;
        }
    }

    public String getServerInfoJsonVersion() {
        String url = ServicePrefs.getIniAndroidURL();
        //String url = "http://14.63.167.218/ini_android.json";
        Log.d(THIS_FILE, "getServerInfoJsonVersion url : " + url);

        if (url == null || url.length() == 0) return null;

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            HttpPost httppost = new HttpPost(url);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            String serialAndroidInfo;
            try {
                JSONObject jObject = new JSONObject(sb.toString());
                serialAndroidInfo = jObject.getString("S");
                Log.d(THIS_FILE, "serialServerAndroidInfo:" + serialAndroidInfo);
                mPrefs.setPreferenceStringValue(AppPrefs.LATEST_GLOBAL_INFO_SERIAL, serialAndroidInfo);
                String versionAndroidClient = jObject.getString("V");
                Log.d(THIS_FILE, "versionServerAndroidClient:" + versionAndroidClient);
                mPrefs.setPreferenceStringValue(AppPrefs.LATEST_CLIENT_VER, versionAndroidClient);
            } catch (JSONException e) {
                Log.e(THIS_FILE, "JSON", e);
                return null;
            }

            return serialAndroidInfo;
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException : " + e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "getServerInfoJsonVersion Exception : " + e);
        }
        return null;
    }

    public int setGlobalInfo()    //20190101 info.json을 트래픽을 줄이기위하여 하드코딩해서 직접삽입
    {
        Log.d(THIS_FILE, "SETUP LOCAL GLOBAL INFO : START");

        mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL, INFO_JSON_VERSION);
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO, "https://if.maaltalk.com/maaltalk/req_client_info.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO, "https://if.maaltalk.com/maaltalk/req_svc_info.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_BALANCE, "https://if.maaltalk.com/maaltalk/req_balance.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL, "http://211.253.10.158/certcid_intl/req_valid_cid.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_CODE, "https://auth.maaltalk.com/certcid_global/req_valid_code.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL, "http://211.253.10.158/certcid_intl/req_account_info.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2, "https://auth.maaltalk.com/certcid_global/req_account_info_intl.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC/maaltalk_cert.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/in.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/mt_auth.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2, "https://auth.maaltalk.com/certcid_locl_v2/req_account_info.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3, "https://auth.maaltalk.com/certcid_global/req_account_info_locl.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET, "https://auth.maaltalk.com/certcid_global/req_get_pocket.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION, "http://info.maaltalk.com/maaltalk_user/maaltalk_login.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2, "http://info.maaltalk.com/maaltalk_user_v2/maaltalk_user.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE, "http://pay.maaltalk.com/payment/index.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI, "http://store.maaltalk.com/");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_ACTIVATION, "http://gi.esmplus.com/dial070/etc/after_update.jpg");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT, "http://pay.maaltalk.com/payment/bridge.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA, "http://pay.maaltalk.com/payment/payment_extra.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_SVC_OUT, "http://info.maaltalk.com/maaltalk_user/maaltalk_wait.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_HISTORY, "https://211.253.25.55/payment/req_history.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_CDR_INFO, "https://if.maaltalk.com/maaltalk/req_cdr.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO, "https://if.maaltalk.com/maaltalk/req_message.php");
        /**
         * MODIFIED
         * 2020.08.21
         */
//        mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, "https://if.maaltalk.com/toast/req_send.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, "https://if.maaltalk.com/maaltalk/req_send.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_AD_INFO, "https://if.maaltalk.com/maaltalk/req_ad.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_UPDATE_SVR, "https://if.maaltalk.com/maaltalk/req_svr_update.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO, "https://if.maaltalk.com/maaltalk/req_push_msg.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO, "https://if.maaltalk.com/maaltalk/req_forwarding_info.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING, "https://if.maaltalk.com/maaltalk/req_forwarding.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG, "http://info.maaltalk.com/maaltalk_user/userMsg/");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL, "201601180001");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_RECORD, "https://auth.maaltalk.com/push_record/req_record.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_LOOKUP, "http://info.maaltalk.com/maaltalk_usim/index.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_MYINFO, "https://if.maaltalk.com/maaltalk/req_myinfo.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE, "https://if.maaltalk.com/maaltalk/req_cid_type.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2, "https://if.maaltalk.com/maaltalk/req_svc_out.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_REC_DIR, "http://rec.maaltalk.com/maaltalk/rec");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_SERVICE_INFO, "http://www.maaltalk.com/intro.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_MANUAL, "http://www.maaltalk.com/use.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE, "http://www.maaltalk.com/charge_table.html");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_NOTICE, "https://store.maaltalk.com/board/list.php?bdId=notice");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_FAQ, "https://store.maaltalk.com/service/faq.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_QUESTION, "https://store.maaltalk.com/board/list.php?bdId=qa");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS, "https://auth.maaltalk.com/policy/agreement.html");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, "http://bit.ly/2H6aris");
        //mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, "http://www.naver.com");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_TXT, "http://www.maaltalk.com/terms.txt");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT, "http://www.maaltalk.com/terms_info.txt");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT, "http://www.maaltalk.com/terms_use.txt");
        mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER, "8.8.8.8");
        mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN, "cs2.maaltalk.com");
        mPrefs.setPreferenceStringValue(AppPrefs.INFO_USIM_VER, "1.0");

        mPrefs.setPreferenceStringValue(AppPrefs.URL_MT_AIRPORT, "http://www.maaltalk.com/mt_airport.php");
        mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_TIME, "http://info.maaltalk.com/maaltalk_user_v2/maaltalk_user_time.php");

        Log.d(THIS_FILE, "SETUP LOCAL GLOBAL INFO : END");
        return 0;
    }

	/*private String getLocalInfoJson() {
		String data = null;
		InputStream inputStream = getResources().openRawResource(R.raw.info);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		int i;
		try {
			i = inputStream.read();
			while (i != -1) {
				byteArrayOutputStream.write(i);
				i = inputStream.read();
			}

			data = new String(byteArrayOutputStream.toByteArray(),"MS949");
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}*/

    private String postNewGlobalInfo() {
        String url = ServicePrefs.getNewInfoURL();
        Log.d(THIS_FILE, "GLOBAL NEW INFO URL : " + url);

        if (url == null || url.length() == 0) return null;

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

            HttpPost httppost = new HttpPost(url);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    public enum AppStatus {
        BACKGROUND, // app is background
        RETURNED_TO_FOREGROUND, // app returned to foreground (or first launch)
        FOREGROUND; // app is foreground
    }

    /*static ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            // 서비스와 연결되었을 때 호출되는 메서드
            // 서비스 객체를 전역변수로 저장
            BridgeServiceBinder mb = (BridgeServiceBinder) service;
            mBprs = mb.getService(); // 서비스가 제공하는 메소드 호출하여
            // 서비스쪽 객체를 전달받을수 있슴
            isBridgePopupReturnService = true;
        }
        public void onServiceDisconnected(ComponentName name) {
            // 서비스와 연결이 끊겼을 때 호출되는 메서드
            isBridgePopupReturnService = false;
        }
    };*/

    /*public static void startBridgeService(){
        if (isBridgePopupReturnService){
            mBprs.showView();
        }else{
            Intent intent = new Intent(obj, BridgePopupReturnService.class);
            obj.startService(intent);
            obj.bindService(intent,conn,Context.BIND_AUTO_CREATE);
        }

    }

    public static void stopBridgeService(){
        if (isBridgePopupReturnService){
            obj.unbindService(conn);
            isBridgePopupReturnService=false;
        }else {
            Intent intent = new Intent(obj, BridgePopupReturnService.class);
            obj.stopService(intent);
        }
    }*/
}

