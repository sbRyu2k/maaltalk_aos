package com.dial070;

import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.ui.ContactsDataContainer;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

public class DialLogin extends Activity {
	private static final String THIS_FILE = "DialLogin";
	
	private AppPrefs mPrefs = null;
	private Context mContext;
	private InputMethodManager mImm;
	
	private ImageButton mLogin,mRegister;
	private EditText mId;
	private EditText mPassword;
	
	private static final int DIAL_LOGIN_ERROR 	= 3;
	
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case DIAL_LOGIN_ERROR:
				{
					if (DialLogin.this != null && !DialLogin.this.isFinishing())
					{
						AlertDialog.Builder msgDialog = new AlertDialog.Builder(DialLogin.this);
						msgDialog.setTitle(getResources().getString(R.string.app_name));
						//msgDialog.setMessage((String)msg.obj);
						msgDialog.setMessage(getResources().getString(R.string.error_login));
						msgDialog.setIcon(R.drawable.ic_launcher);
						msgDialog.setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								mPassword.setText("");
								mPassword.requestFocus();
							}
						});
						msgDialog.show();
					}										
				}
				break;
			}
		}
	};			
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		Log.d(THIS_FILE, "onCreate : START");
		mContext = this;
		
		setContentView(R.layout.dial_login);
		
		mPrefs = new AppPrefs(mContext);
		mLogin = (ImageButton) findViewById(R.id.btn_login);
		mRegister = (ImageButton) findViewById(R.id.btn_register);
		
		mId = (EditText) findViewById(R.id.edit_user_id);
		mPassword = (EditText) findViewById(R.id.edit_user_pwd);		
	
		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		
		if(!mPrefs.getPreferenceBooleanValue(AppPrefs.AUTO_LOGIN))
		{
			//아이디/패스워드가 저장된 경우 
			String id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
			if(id != null && id.length() > 0)
			{
				mId.setText(id);
				mPassword.requestFocus();
			}
			//mPassword.setText(mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD));			
		}
		
		//로그인
		mLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mImm.hideSoftInputFromWindow(mPassword.getWindowToken(), 0);
				
				goLogin();
			}
		});
		
		//회원가입
		mRegister.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				goRegister();
			}
		});
		
		

		
	}

	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}



	//로그인 처리
	private void goLogin()
	{
		String userId = mId.getText().toString();
		String userPwd = mPassword.getText().toString();
		int rs = ServicePrefs.login(this, userId, userPwd);
		
		if (rs >= 0)
		{
			// SAVE
			mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, userId);
			mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, userPwd);			
			
			//연락처 초기화
			ContactsDataContainer.clearContactsList();			
			
			goMain();
		}
		else
		{
			// 다시 시도.
			handler.sendEmptyMessage(DIAL_LOGIN_ERROR);
		}
	}
	
	//회원가입 홈페이지
	private void goRegister()
	{
		
	}
	
	
	private void goMain()
	{
		//메인 화면으로 이동
		Intent intent = new Intent(DialLogin.this, DialMain.class);
		startActivity(intent);	
		
		finish();
	}
	

	
}
