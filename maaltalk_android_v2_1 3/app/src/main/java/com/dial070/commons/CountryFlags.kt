package com.dial070.commons

const val COUNTRY_CODE_KOREA: String = "kr"
const val COUNTRY_CODE_JAPAN: String = "jp"
const val COUNTRY_CODE_CHINA: String = "cn"
const val COUNTRY_CODE_USA: String = "us"
