package com.dial070.exceptions;

public class SipStateException extends Exception {

    public SipStateException(String message) {
        super(message);
    }

    public SipStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
