package com.dial070.view.entitiy

import com.dial070.status.CallStatus

data class CallData(var callStatus: Int, var callId: Int, var callNumber: String) {}