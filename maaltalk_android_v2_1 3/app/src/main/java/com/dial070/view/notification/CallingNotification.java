package com.dial070.view.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.dial070.maaltalk.R;

import java.lang.reflect.Method;

public class CallingNotification extends NotificationWrapper {

    @Override
    public Notification buildPreHoneyCombNotificationWithBuilder(Context context, PendingIntent pendingIntent, String title, String desc, int iconId) {
        Notification notification = new Notification(iconId, "", System.currentTimeMillis());

        try {
            Method m = notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
            m.invoke(notification, context, title, context, pendingIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notification;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Notification buildNotificationWithBuilder(Context context, PendingIntent pendingIntent, String title, String desc, int iconId) {
        RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_remote_calling);
        Intent declineIntent = new Intent("in_call_notification_decline");
        PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
        notificationView.setTextViewText(R.id.desc, desc);
        notificationView.setTextViewText(R.id.title, title);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(desc)
                .setContentIntent(pendingIntent)
                .setSmallIcon(iconId)
                .setCategory(Notification.CATEGORY_CALL)
                .setContent(notificationView);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN) {
            return builder.build();
        } else {
            return builder.getNotification();
        }
    }

    @Override
    public Notification buildOverOreoNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channelId, String title, String desc, int iconId) {
        if(Build.VERSION.SDK_INT >= 26) {
            RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_remote_calling);
            Intent declineIntent = new Intent("in_call_notification_decline");
            PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
            notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
            notificationView.setTextViewText(R.id.title, title);
            notificationView.setTextViewText(R.id.desc, desc);

            Notification notification = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(iconId)
                    .setContentTitle(title)
                    .setContentText(desc)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setTimeoutAfter(100)
                    .setPriority(NotificationCompat.PRIORITY_MIN)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                    .setCustomContentView(notificationView)
                    .build();

            return notification;

        } else {
            return buildNotificationWithBuilder(context, pendingIntent, title, desc, iconId);
        }
    }

    @Override
    public Notification buildOverQNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channelId, String title, String desc, int iconId) {
        RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_remote_calling);
        Intent declineIntent = new Intent("in_call_notification_decline");
        PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
        notificationView.setTextViewText(R.id.title, title);
        notificationView.setTextViewText(R.id.desc, desc);

        if(Build.VERSION.SDK_INT >= 29) {
            Notification notification = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(iconId)
                    .setContentTitle(title)
                    .setContentText(desc)
                    .setFullScreenIntent(pendingIntent, true)
                    .setAutoCancel(true)
                    .setTimeoutAfter(100)
                    .setPriority(NotificationCompat.PRIORITY_MIN)
                    .setCategory(Notification.CATEGORY_CALL)
                    .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                    .setCustomContentView(notificationView)
                    .build();

            return notification;

        } else {
            return buildOverOreoNotificationWithBuilder(context, pendingIntent, channelId, title, desc, iconId);
        }
    }
}
