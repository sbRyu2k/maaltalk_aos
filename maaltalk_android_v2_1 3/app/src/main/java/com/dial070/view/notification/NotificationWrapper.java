package com.dial070.view.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;

public abstract class NotificationWrapper {

    public Notification createNotification(Context context, PendingIntent pendingIntent, String channelId, String title, String desc, int iconId) {
        Notification notification = null;

        if(Build.VERSION.SDK_INT >= 29) {
            notification = buildOverQNotificationWithBuilder(context, pendingIntent, channelId, title, desc, iconId);

        } else if(Build.VERSION.SDK_INT >= 26) {
            notification = buildOverOreoNotificationWithBuilder(context, pendingIntent, channelId, title, desc, iconId);

        } else {
            if(isNotificationBuilderSupported()) {
                notification = buildNotificationWithBuilder(context, pendingIntent, title, desc, iconId);

            } else {
                notification = buildPreHoneyCombNotificationWithBuilder(context, pendingIntent, title, desc, iconId);
            }
        }
        return notification;
    }

    private boolean isNotificationBuilderSupported() {
        try {
            return (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) && Class.forName("android.app.Notification.Builder") != null;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Notification buildPreHoneyCombNotificationWithBuilder(Context context, PendingIntent pendingIntent, String title, String desc, int iconId) {
        return null;
    }

    public Notification buildNotificationWithBuilder(Context context, PendingIntent pendingIntent, String title, String desc, int iconId) {
        return null;
    }

    public Notification buildOverQNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channelId, String title, String desc, int iconId) {
        return null;
    }

    public Notification buildOverOreoNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channelId, String title, String desc, int iconId) {
        return null;
    }

}
