package com.dial070.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.db.DBManager;
import com.dial070.db.SmsMsgData;
import com.dial070.maaltalk.R;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

public class ImageViewer extends Activity {
	private static final String THIS_FILE = "IMAGE_VIEWER";
	// private ImageView mImageView;
	// BJH 2016.10.31
	private Context mContext;
	private ContextWrapper mContextWrapper;
	private static FrameLayout mFTL, mFBL;
	private ImageButton mIBB, mIBS, mIBD;
	private TextView mTV;
	private ProgressDialog mLoagindDialog;
	private static ViewPager mPager;
	private static ArrayList<String> mList;
	private static ArrayList<Bitmap> mBit;
	private String mResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_view_activity);

		DebugLog.d("test_img_viewer onCreate() -->");

		// BJH 2016.10.31 수정
		mContext = this;
		mContextWrapper= new ContextWrapper(this);
		mList = new ArrayList<String>();
		mBit = new ArrayList<Bitmap>();
		mFTL = (FrameLayout) findViewById(R.id.image_top_layout);
		mFBL = (FrameLayout) findViewById(R.id.image_bottom_layout);
		mIBB = (ImageButton) findViewById(R.id.image_back);
		mTV = (TextView) findViewById(R.id.image_count);
		mIBS = (ImageButton) findViewById(R.id.image_share);
		mIBD = (ImageButton) findViewById(R.id.image_download);
		mPager = (ViewPager) findViewById(R.id.pager);
		
		mPager.setPageTransformer(false, new ViewPager.PageTransformer() {
		    @Override
		    public void transformPage(View page, float position) {
		    	// do transformation here
		    	setLayout();
		    	final float normalizedposition = Math.abs(Math.abs(position) - 1);
		        page.setScaleX(normalizedposition / 2 + 0.3f);
		        page.setScaleY(normalizedposition / 2 + 0.3f);
		    }
		});
		mIBB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		mIBS.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int position = mPager.getCurrentItem();
				if (getBit(position) == null)
					return;

				String dataUri = MediaStore.Images.Media.insertImage(
						getContentResolver(), getBit(position), "maaltalk_share", "");
				Log.d(THIS_FILE, dataUri);
				try {
					Intent msg = new Intent(Intent.ACTION_SEND);
					msg.putExtra(Intent.EXTRA_STREAM, Uri.parse(dataUri));
					msg.setType("image/*");
					startActivity(Intent.createChooser(msg, getResources()
							.getString(R.string.share)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		mIBD.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int position = mPager.getCurrentItem();
				File file = new File(mList.get(position));
				if (!file.exists())
					return;
				
				mLoagindDialog = ProgressDialog.show(mContext, getResources()
						.getString(R.string.image_download), getResources()
						.getString(R.string.image_downloading), true, false);
				saveBitmaptoJpeg(getBit(position));
			}
		});

		Intent intent = getIntent();
		if (intent != null) {
			String file = intent.getStringExtra("file");
			String number = intent.getStringExtra("number");
			setArrayImg(number, file);
			// String title = intent.getStringExtra("title");	
		}

	}
	
	protected void onDestroy() {
		Log.d(THIS_FILE, "onDestroy");
		mList = null;
		mBit = null;
		super.onDestroy();
	}

	public void saveBitmaptoJpeg(Bitmap bitmap) {						// dcim으로 변경
		long name = System.currentTimeMillis();
		String fileName = name+".jpg";

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			OutputStream fos = null;
			ContentResolver resolver = getContentResolver();
			ContentValues values = new ContentValues();
			values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
			values.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
			values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM);
			Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

			try {
				fos = resolver.openOutputStream(imageUri);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

				mLoagindDialog.dismiss();
				mLoagindDialog = null;
				Toast.makeText(
						getApplicationContext(),
						getResources().getString(
								R.string.image_download_complete),
						Toast.LENGTH_SHORT).show();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if(fos!=null) {
						fos.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			File tempFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), fileName);
			try {
				tempFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(tempFile);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
				fos.close();

				mLoagindDialog.dismiss();
				mLoagindDialog = null;
				Toast.makeText(
						getApplicationContext(),
						getResources().getString(
								R.string.image_download_complete),
						Toast.LENGTH_SHORT).show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

//		String ex_storage = Environment.getExternalStorageDirectory().getAbsolutePath();
//		String folder = "maaltalk";
//		String foler_name = "/" + folder + "/";
//		long name = System.currentTimeMillis();
//		String file_name = name + ".jpg";
//		String string_path = ex_storage + foler_name;
//
//		File file_path = null;
//		FileOutputStream out = null;
//		try {
//			file_path = new File(string_path);
//			if (!file_path.isDirectory()) {
//				file_path.mkdirs();
//			}
//			out = new FileOutputStream(string_path + file_name);
//			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//		} catch (IOException exception) {
//			Log.e(THIS_FILE, exception.getMessage());
//		} finally {
//			try {
//				out.close();
//				mLoagindDialog.dismiss();
//				mLoagindDialog = null;
//				Toast.makeText(
//						getApplicationContext(),
//						getResources().getString(
//								R.string.image_download_complete),
//						Toast.LENGTH_SHORT).show();
//				sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
//						Uri.parse("file://" + string_path + file_name)));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}
	
	public static int getCount() {
		return mList.size();
	}
	
	public static String getImgUrl(int position) {
		return mList.get(position);
	}
	
	public static Bitmap getBit(int position) {
		return mBit.get(position);
	}

	public void setArrayImg(String number, String file) {
		// TODO Auto-generated method stub
		DBManager database = new DBManager(mContext);
		database.open();

		Cursor c = database.getSmsMsgImg(number);

		if (c == null) {
			database.close();
			return;
		}

		if (!c.moveToFirst()) {
			c.close();
			database.close();
			return;
		}
		
		int id = 0;
		String file1 = null;
		String file2 = null;
		String file3 = null;

		do {
			id = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_ID));
			file1 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE1));
			if(file1 != null && file1.length() > 0)
				mList.add(getImageFileName(id, 1));
			file2 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE2));
			if(file2 != null && file2.length() > 0)
				mList.add(getImageFileName(id, 2));
			file3 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE3));
			if(file3 != null && file3.length() > 0)
				mList.add(getImageFileName(id, 3));
		} while (c.moveToNext());

		c.close();
		database.close();
		
		mResult = "/" + mList.size();
		int position = mList.indexOf(file);
		ImageViewerAdapter adapter = new ImageViewerAdapter(getLayoutInflater());
		mPager.addOnPageChangeListener(viewPagerPageChangeListener);
		mPager.setAdapter(adapter);
		mPager.setCurrentItem(position);
		
		return;
	}
	
	ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
	
	public void setLayout() {
		int position = mPager.getCurrentItem();
		int count = position + 1;
		String result = count + mResult;
		String tv = mTV.getText().toString();
		if(result.equals(tv))
			return;
		
		mTV.setText(result);
		setAlpha();
	}
	
	public static void setAlpha() {
		if (mFTL.getVisibility() == View.VISIBLE) {
			int position = mPager.getCurrentItem();
			Bitmap bit = mBit.get(position);
			float height = bit.getHeight();
			float width = bit.getWidth();
			float f = height / width;
			if(height > 600 && f > 1.7 ) {
				mFBL.getBackground().setAlpha(160);
				mFTL.getBackground().setAlpha(160);
			} else {
				mFBL.getBackground().setAlpha(255);
				mFTL.getBackground().setAlpha(255);
			}
		}
	}
	
	public static void setVisible() {
		if (mFTL.getVisibility() == View.VISIBLE) {
			mFBL.setVisibility(View.GONE);
			mFTL.setVisibility(View.GONE);
		} else {
			mFBL.setVisibility(View.VISIBLE);
			mFTL.setVisibility(View.VISIBLE);
			setAlpha();
		}
	}
	
	private String getImageFileName(final long mid, final int seq)
	{
		String homePath = mContextWrapper.getFilesDir().getPath();
		homePath = homePath + "/chat/media/";
		String aFile = homePath + "media_" + String.valueOf(mid) + "_" + String.valueOf(seq) + ".jpg";
		setBit(aFile);
		return aFile;		
	}
	
	public void setBit(String file) {
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(file, o);

			final int REQUIRED_WIDTH = 512;
			final int REQUIRED_HIGHT = 512;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
					&& o.outHeight / scale / 2 >= REQUIRED_HIGHT)
				scale *= 2;
			
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inSampleSize = scale;
			Bitmap bmp = BitmapFactory.decodeFile(file, options);
			if (bmp != null)
				mBit.add(bmp);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
