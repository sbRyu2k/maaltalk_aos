package com.dial070.ui;



import com.dial070.maaltalk.R;

import com.dial070.utils.ContactHelper;
import com.dial070.utils.HangulUtils;


import android.content.*;
//import android.content.res.*;
//import android.graphics.*;
//import android.graphics.Bitmap.Config;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.*;
import android.widget.*;


public class FavoritesListView extends LinearLayout {
	private Context mContext;
	private ImageView mUserPhoto,mUserPhotoEx;
	private ImageView mBadgeIcon,mBadgeMemo;
	private ImageView mBadgeDelete;
	private LinearLayout mUserPhoneLayout;
	private TextView mUserPhone,mUserName,mUserNameInitial;
	private FavoriteData mUserItem;
	
	public FavoritesListView(Context context, FavoriteData item)
	{
		super(context);
		mContext = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.favorites_list_item, this, true);
		
		mUserPhoto = (ImageView) findViewById(R.id.favorites_user_photo);
		mUserPhotoEx = (ImageView) findViewById(R.id.favorites_user_photo_ex);
		mBadgeIcon = (ImageView) findViewById(R.id.favorites_badge_auto);
		mBadgeMemo = (ImageView) findViewById(R.id.favorites_badge_memo);
		mBadgeDelete = (ImageView) findViewById(R.id.favorites_badge_delete);
		mUserName = (TextView) findViewById(R.id.favorites_user_name);
		mUserNameInitial = (TextView) findViewById(R.id.favorites_user_name_initial);
		mUserPhoneLayout = (LinearLayout) findViewById(R.id.favorites_user_phone_layout);
		mUserPhone = (TextView) findViewById(R.id.favorites_user_phone);
		
		this.mUserItem = item;
	}

	/*
	private Bitmap drawTextToBitmap(Context gContext, int gResId, String gText)
	{
	  Resources resources = gContext.getResources();
	  float scale = resources.getDisplayMetrics().density;
	  Bitmap bitmap = 
	      BitmapFactory.decodeResource(resources, gResId);
	 
	  android.graphics.Bitmap.Config bitmapConfig =
	      bitmap.getConfig();
	  // set default bitmap config if none
	  if(bitmapConfig == null) {
	    bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
	  }
	  // resource bitmaps are imutable, 
	  // so we need to convert it to mutable one
	  bitmap = bitmap.copy(bitmapConfig, true);
	 
	  Canvas canvas = new Canvas(bitmap);
	  // new antialised Paint
	  Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	  // text color - #3D3D3D
	  //paint.setColor(Color.rgb(61, 61, 61));
	  paint.setColor(Color.WHITE);
	  // text size in pixels
	  paint.setTextSize((int) (14 * scale));
	  // text shadow
	  paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
	 
	  Bitmap bitmap_photo = 
		      BitmapFactory.decodeResource(resources, R.drawable.favorites_photo_back);			  
	  
	  String title = gText;
	  if(title.length() > 11)
		  title = title.substring(0, 11);
	  
	  // draw text to the Canvas center
	  Rect bounds = new Rect();
	  paint.getTextBounds(title, 0, title.length(), bounds);
	  int x = (bitmap.getWidth() - bounds.width())/2;
	  int y = bitmap_photo.getHeight() + ((bitmap.getHeight() - bitmap_photo.getHeight() + bounds.height()) /2) - (int)(4 * scale);
	 
	  canvas.drawText(title, x , y , paint);
			 
	  return bitmap;
	}	
	*/
	
	/*
	private Bitmap drawableToBitmap (Drawable drawable,int id) {
		if(drawable == null) return null;
		
		if (drawable instanceof BitmapDrawable) {
	        return ((BitmapDrawable)drawable).getBitmap();
	    }
	  Resources resources = mContext.getResources();
	  float scale = resources.getDisplayMetrics().density;
	  Bitmap bm = 
	      BitmapFactory.decodeResource(resources, id);
		  
	    //Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
	    Bitmap bitmap = Bitmap.createBitmap((int)(bm.getWidth() * scale), (int)(bm.getHeight() * scale), Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap); 
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}
	*/
	
	private boolean isNumeric(String str)  
	{  
		if (str == null || str.length() == 0) return false;
		try  
		{  
			Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}
	
	public FavoriteData getData()
	{
		return mUserItem;
	}
	
	public void setData(FavoriteData item, int mode)
	{
		if(item != null)
		{
			//데이타 셋팅
			mUserItem = item;
			
			if( mode == Dial.FAVORITE_VIEW_MODE)
			{
				mBadgeDelete.setVisibility(GONE);
			}
			else
			{
				mBadgeDelete.setVisibility(VISIBLE);
			}
			
			if(item.getDisplayName().equalsIgnoreCase("new_add"))
			{
				mUserPhoto.setImageURI(null);
				//mUserPhoto.setImageResource(R.drawable.favorites_empty);
				mUserPhoto.setBackgroundResource(R.drawable.favorites_empty);
				mUserPhoto.setVisibility(View.VISIBLE);
				mBadgeIcon.setVisibility(GONE);
				mBadgeMemo.setVisibility(GONE);
				mBadgeDelete.setVisibility(GONE);
				mUserPhoneLayout.setVisibility(GONE);
				mUserPhotoEx.setVisibility(GONE);
				
				mUserNameInitial.setText("");
				mUserNameInitial.setVisibility(View.GONE);
				
				mUserName.setText("");
				mUserName.setVisibility(View.GONE);		
			}
			else
			{
				mUserPhoto.setImageURI(null);
				mUserPhoto.setBackgroundResource(R.drawable.favorites_user_photo);
				
				if(item.getRegisterType().equals("AUTO"))
				{
					mBadgeIcon.setVisibility(VISIBLE);
				}
				else
				{
					mBadgeIcon.setVisibility(GONE);
				}
				mUserPhoneLayout.setVisibility(VISIBLE);
				
				if(item.getMemo().length() > 0) mBadgeMemo.setVisibility(VISIBLE);
				else mBadgeMemo.setVisibility(GONE);
				
	        	Uri u = ContactHelper.getPhotoUriByPhoneNumber(mContext, item.getPhoneNumber());
				if (u != null) {
					mUserPhoto.setImageURI(u);
					
					mUserNameInitial.setText("");
					mUserNameInitial.setVisibility(View.GONE);
					
					mUserName.setText("");
					mUserName.setVisibility(View.GONE);					
					mUserPhotoEx.setVisibility(View.VISIBLE);
					mUserPhotoEx.bringToFront();

					//Log.e("Favorites", "PHOTO EXIST! ,NAME=" + item.getDisplayName());
					mUserPhone.setText(item.getDisplayName());
					
				} else {
					
					mUserName.setVisibility(View.VISIBLE);
					mUserNameInitial.setVisibility(View.VISIBLE);
					mUserPhotoEx.setVisibility(View.GONE);
					//mUserPhoto.bringToFront();
					
					if(isNumeric(item.getDisplayName()))
					{
						mUserPhoto.setBackgroundResource(R.drawable.favorites_user_photo_unknown);
						//mUserPhoto.setImageResource(R.drawable.favorites_user_photo_unknown);
						mUserNameInitial.setText("");
						mUserName.setText("");
					}
					else
					{
						mUserPhoto.setBackgroundResource(R.drawable.favorites_user_photo);
						
						mUserName.setText(item.getDisplayName());
						mUserNameInitial.setText(HangulUtils.getHangulInitialChoSung(item.getDisplayName()));
					}

					mUserPhoto.setVisibility(View.VISIBLE);

					mUserPhone.setText(item.getPhoneNumber());
				}				

			}
		}
		else
		{
			mUserItem = null;
			
			if( mode == Dial.FAVORITE_VIEW_MODE)
			{
				mUserPhoto.setBackgroundResource(R.drawable.favorites_empty);
				mUserPhoto.setVisibility(View.INVISIBLE);
			}
			else
			{
				mUserPhoto.setBackgroundResource(R.drawable.favorites_edit);
			}		
			mBadgeIcon.setVisibility(GONE);
			mBadgeDelete.setVisibility(GONE);
			mBadgeMemo.setVisibility(GONE);
		}	
	}
}
