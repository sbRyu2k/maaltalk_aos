package com.dial070.ui;

import java.io.BufferedReader;
//import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

public class TermsActivity extends Activity {

	private AppPrefs mPrefs;
	private Context mContext;
	private TextView m_TitleText, m_AppText;
	// private TextView mTermsText;
	private CheckBox m_All, m_Use, m_Info;
	private LinearLayout m_Layout_all, m_Layout_use, m_Layout_info,
			m_layout_button;
	private TextView mTermsUse, mTermsInfo;
	private Button mButtonAccept, mButtonReject;
	private ScrollView mScroll;
	private int mTop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		mContext = this;
		mPrefs = new AppPrefs(this);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		// BJH 2016-05-31 약관 수정
		setContentView(R.layout.activity_terms_v2);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.custom_title2);

		m_AppText = (TextView) findViewById(R.id.left_title);
		m_AppText.setTextColor(Color.WHITE);
		m_AppText.setTypeface(Typeface.DEFAULT_BOLD);
		m_AppText.setText(mContext.getResources().getString(R.string.app_name));
		//
		m_TitleText = (TextView) findViewById(R.id.right_title);
		m_TitleText.setTextColor(Color.WHITE);
		m_TitleText.setTypeface(Typeface.DEFAULT_BOLD);
		m_TitleText.setText(mContext.getResources().getString(
				R.string.title_terms));

		// mTermsText = (TextView)findViewById(R.id.txt_terms);
		m_All = (CheckBox) findViewById(R.id.checkbox_all);
		m_Use = (CheckBox) findViewById(R.id.checkbox_use);
		m_Info = (CheckBox) findViewById(R.id.checkbox_info);
		m_Layout_all = (LinearLayout) findViewById(R.id.layout_all_check);
		m_Layout_use = (LinearLayout) findViewById(R.id.layout_use_check);
		m_Layout_info = (LinearLayout) findViewById(R.id.layout_info_check);
		m_layout_button = (LinearLayout) findViewById(R.id.layout_button);
		mTermsUse = (TextView) findViewById(R.id.txt_terms_use);
		mTermsInfo = (TextView) findViewById(R.id.txt_terms_info);
		mScroll = (ScrollView) findViewById(R.id.scroll_checkbox);
		mButtonAccept = (Button) findViewById(R.id.btn_accept);
		mButtonReject = (Button) findViewById(R.id.btn_reject);

		// mTermsText.setText("");
		mTermsUse.setText("");
		mTermsInfo.setText("");
		//mTermsUse.setMovementMethod(new ScrollingMovementMethod());
		//mTermsInfo.setMovementMethod(new ScrollingMovementMethod());
		mTermsUse.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				mScroll.requestDisallowInterceptTouchEvent(true);
				// 스크롤뷰가 텍스트뷰의 터치이벤트를 가져가지 못하게 함
				return false;
			}
		});
		mTermsUse.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mScroll.requestDisallowInterceptTouchEvent(true);
				// 스크롤뷰가 텍스트뷰의 터치이벤트를 가져가지 못하게 함
				return false;
			}
		});
		mTermsInfo.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				mScroll.requestDisallowInterceptTouchEvent(true);
				// 스크롤뷰가 텍스트뷰의 터치이벤트를 가져가지 못하게 함
				return false;
			}
		});
		mTermsInfo.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				mScroll.requestDisallowInterceptTouchEvent(true);
				// 스크롤뷰가 텍스트뷰의 터치이벤트를 가져가지 못하게 함
				return false;
			}
		});
		// 동의
		mButtonAccept.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (m_Use.isChecked() && m_Info.isChecked()) {
					mPrefs.setPreferenceBooleanValue(AppPrefs.TERM_OK, true);
					setResult(RESULT_OK, null);
					finish();
				} else {
					mTop = m_Layout_info.getTop();
					if (!m_Use.isChecked())
						mScroll.scrollTo(0, 0);
					else
						mScroll.scrollTo(0, mTop);
					Toast toast = Toast.makeText(mContext, getResources().getString(R.string.terms_all_agree),
							Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});

		// 거절
		mButtonReject.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				setResult(RESULT_CANCELED);
				finish();
			}
		});

		// loadTerms("http://www.maaltalk.com/terms.txt");
		loadTermsUse(mPrefs.getPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT));
		loadTermsInfo(mPrefs.getPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT));

		mTermsUse.setTextIsSelectable(true);
		mTermsInfo.setTextIsSelectable(true);
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				mScroll.scrollTo(0, 0); // BJH 로딩이 완료되면 상단에 고정 되지 않는 문제..
				mTop = m_layout_button.getTop();
			}
		};
		Handler handler = new Handler();
		handler.postDelayed(runnable, 100);

		m_Layout_all.setClickable(true);
		m_Layout_all.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!m_All.isChecked()) {
					m_Use.setChecked(true);
					m_Info.setChecked(true);
					m_All.setChecked(true);
					mScroll.scrollTo(0, mTop);
				} else {
					m_Use.setChecked(false);
					m_Info.setChecked(false);
					m_All.setChecked(false);
				}
			}

		});

		m_Layout_use.setClickable(true);
		m_Layout_use.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!m_Use.isChecked()) {
					m_Use.setChecked(true);
					if (m_Info.isChecked()) {
						m_All.setChecked(true);
						mScroll.scrollTo(0, mTop);
					}
				} else {
					m_Use.setChecked(false);
					if (m_All.isChecked())
						m_All.setChecked(false);
				}
			}

		});

		m_Layout_info.setClickable(true);
		m_Layout_info.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!m_Info.isChecked()) {
					m_Info.setChecked(true);
					if (m_Use.isChecked()) {
						m_All.setChecked(true);
						mScroll.scrollTo(0, mTop);
					}
				} else {
					m_Info.setChecked(false);
					if (m_All.isChecked())
						m_All.setChecked(false);
				}
			}

		});

		m_All.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckBox check = (CheckBox) v;

				if (check.isChecked()) {
					m_Use.setChecked(true);
					m_Info.setChecked(true);
				} else {
					m_Use.setChecked(false);
					m_Info.setChecked(false);
				}
			}

		});
		m_All.setClickable(false);
		m_Use.setClickable(false);
		m_Info.setClickable(false);
		/*
		 * m_Use.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub CheckBox check = (CheckBox) v;
		 * 
		 * if (check.isChecked()) { //m_Use.setChecked(true);
		 * if(m_Info.isChecked()) m_All.setChecked(true); } else {
		 * //m_Use.setChecked(false); if(m_All.isChecked())
		 * m_All.setChecked(false); } }
		 * 
		 * });
		 * 
		 * m_Info.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub CheckBox check = (CheckBox) v;
		 * 
		 * if (check.isChecked()) { //m_Info.setChecked(true);
		 * if(m_Use.isChecked()) m_All.setChecked(true); } else {
		 * //m_Info.setChecked(false); if(m_All.isChecked())
		 * m_All.setChecked(false); } }
		 * 
		 * });
		 */

	}

	/*
	 * private void loadTerms(String url) { if (url == null || url.length() ==
	 * 0) return; try { URL terms_url = new URL(url);
	 * 
	 * BufferedReader in = new BufferedReader(new
	 * InputStreamReader(terms_url.openStream())); String str; while ((str =
	 * in.readLine()) != null) { //str = str.replaceAll("\t", "  ");
	 * mTermsUseText.append(str+"\n\n"); } in.close(); } catch
	 * (MalformedURLException e) { } catch (Exception e) { } }
	 */

	private void loadTermsUse(String url) {
		if (url == null || url.length() == 0)
			return;
		try {
			URL terms_url = new URL(url);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					terms_url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				// str = str.replaceAll("\t", "  ");
				mTermsUse.append(str + "\n\n");
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (Exception e) {
		}
	}

	private void loadTermsInfo(String url) {
		if (url == null || url.length() == 0)
			return;
		try {
			URL terms_url = new URL(url);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					terms_url.openStream()));
			String str;
			while ((str = in.readLine()) != null) {
				// str = str.replaceAll("\t", "  ");
				mTermsInfo.append(str + "\n\n");
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (Exception e) {
		}
	}
}
