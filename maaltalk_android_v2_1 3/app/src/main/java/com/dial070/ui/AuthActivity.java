package com.dial070.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;

public class AuthActivity extends Activity {
    private static final String THIS_FILE = "Auth";
    //	private ArrayAdapter<CharSequence> adspin;
    private Context mContext;
    private AppPrefs mPrefs;

    private LinearLayout mLayoutA, mLayoutB, mLayoutC, mLayoutKorea;
    //private Spinner m_SpinnerCountry;
    private Button m_ButtonCountry, m_ButtonCountryC, m_ButtonGom, m_ButtonManual;
    private EditText m_EditTextNumber, m_EditTextNumberC;
    private Button m_ButtonReq, m_ButtonReqC;
    private ProgressBar m_ProgressBar;
    private TextView m_TextView;
    private TextView m_TextNameView;
    private TextView m_TitleText;
    private CheckBox m_CheckBox;
    private CheckBox m_CheckBoxOther;

    private TextView m_TextDistView;
    private EditText m_EditTextDist, m_EditTextDistC;

    private Button m_ButtonLocal;
    private Button m_ButtonInter;

    private String m_CountryCode = "";
    private String m_CountryName = "";
    private String m_CountryFlag = "";
    private String m_PhoneNumber = "";
    private String m_DialNumber = "";
    //private String m_Serial = "";

    private static final int SELECT_COUNTRY = 1;
    private static final int SELECT_TERM = 2;
    private static final int SELECT_AUTH = 3;
    //BJH
    private static final int SELECT_COUNTRY_C = 4;

    private String m_Locale = "ko_KR";
    //BJH 2016.06.07
    public static final String KOREA_CODE = "+82";
    public static final String USA_CODE = "+1";
    public static final String CHINA_CODE = "+86";
    public static final String JAPAN_CODE = "+81";
    public static final String KOREA_FLAG = "kr";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //BJH ~+C 전화번호 입력을 하면 해외인증과 국내인증으로 구별하기 위해서 새롭게 만든 레이아웃 ex) ShowLocalAuth+C()
        super.onCreate(savedInstanceState);

        Log.d(THIS_FILE, "onCreate");

        mContext = this;
        //커스텀 타이틀 바
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

        setContentView(R.layout.activity_auth);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_left_title);

        mLayoutA = (LinearLayout) findViewById(R.id.linearLayoutA);
        mLayoutB = (LinearLayout) findViewById(R.id.linearLayoutB);
        mLayoutC = (LinearLayout) findViewById(R.id.linearLayoutC);
        mLayoutKorea = (LinearLayout) findViewById(R.id.linearLayoutKorea);
        SetMenuMode(0);


        m_ButtonLocal = (Button) findViewById(R.id.buttonLocal);
        m_ButtonLocal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ShowLocalAuth();
            }
        });

        m_ButtonInter = (Button) findViewById(R.id.buttonInter);
        m_ButtonInter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SetMenuMode(1);
                SetViewMode(0);
            }
        });

        //잔액정보 표시
        m_TitleText = (TextView) findViewById(R.id.left_title);
        m_TitleText.setTextColor(Color.WHITE);
        m_TitleText.setTypeface(Typeface.DEFAULT_BOLD);
        m_TitleText.setText(mContext.getResources().getString(R.string.app_name));

        mPrefs = new AppPrefs(this);
        //m_Serial = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2);
        m_Locale = Locale.getDefault().toString();
        if (m_Locale.equals("ko_KR"))//BJH 다국어 관련 국가 코드 언어 선택
            mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "한국어");
        else
            mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "영어");

        String locale = mPrefs.getPreferenceStringValue(AppPrefs.LOCALE);

        if (locale != null && locale.equals("한국어")) {
            m_CountryCode = KOREA_CODE;
            m_CountryName = getResources().getString(R.string.country_korea);
            m_CountryFlag = KOREA_FLAG;
        } else {
            if (m_Locale.startsWith("ja")) {
                m_CountryCode = JAPAN_CODE;
                m_CountryName = getResources().getString(R.string.country_japan);
                m_CountryFlag = "jp";
            } else if (m_Locale.startsWith("zh_CN")) {
                m_CountryCode = CHINA_CODE;
                m_CountryName = getResources().getString(R.string.country_china);
                m_CountryFlag = "cn";
            } else {
                m_CountryCode = USA_CODE;
                m_CountryName = getResources().getString(R.string.country_usa);
                m_CountryFlag = "us";
            }
        }
        m_PhoneNumber = "";

        if (ServicePrefs.DIAL_AUTH_TEST) {
            m_CountryCode = "+82";
            m_CountryName = getResources().getString(R.string.country_korea);
            m_CountryFlag = "kr";
            m_PhoneNumber = "";
        }

        //if (getPhoneState())
        //m_PhoneNumber = getMyPhoneNumber();

        if (currentCode != null) {
            m_CountryCode = currentCode;
        }
        if (currentCountry != null) {
            m_CountryName = currentCountry;
        }
        if (currentNumber != null)
            m_PhoneNumber = currentNumber;
        if (currentFlag != null)
            m_CountryFlag = currentFlag;

        m_ButtonCountry = (Button) findViewById(R.id.buttonCountry);
        m_ButtonCountry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ShowCountry();
            }
        });
        //BJH
        m_ButtonCountryC = (Button) findViewById(R.id.buttonCountryC);
        m_ButtonCountryC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ShowCountryC();
            }
        });

        m_EditTextNumber = (EditText) findViewById(R.id.editNumber);
        m_EditTextNumber.setText("");

        m_EditTextNumberC = (EditText) findViewById(R.id.editNumberC);
        m_EditTextNumberC.setText("");

        m_TextDistView = (TextView) findViewById(R.id.textEditDist);

        m_EditTextDist = (EditText) findViewById(R.id.editDist);
        m_EditTextDist.setText("");

        m_EditTextDistC = (EditText) findViewById(R.id.editDistC);
        m_EditTextDistC.setText("");
        //BJH
        m_EditTextNumberC.setText(m_PhoneNumber);
        if (m_CountryCode.equals("+82")) {
            m_EditTextNumberC.setVisibility(View.INVISIBLE);
            //m_EditTextNumberC.setText("");
            //m_EditTextNumberC.setEnabled(false);
            mLayoutKorea.setVisibility(View.VISIBLE);
        }
        //Alert(mContext,mContext.getResources().getString(R.string.ui_auth_type_desc));

        m_ButtonGom = (Button) findViewById(R.id.buttonDistGom);
        m_ButtonGom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                m_EditTextDistC.setText("s1000");
            }
        });

        m_ButtonManual = (Button) findViewById(R.id.buttonManual);
        m_ButtonManual.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ShowManual();
            }
        });


        m_ButtonReq = (Button) findViewById(R.id.buttonAuthReq);
        m_ButtonReq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (currentViewMode == 0) {
                    AuthRequest();
                } else if (currentViewMode == 3) {
                    String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTH_CID);
                    String code = m_EditTextNumber.getText().toString();
                    if (auth_check(cid, code) < 0) {
                        auth_cancel();
                        SetViewMode(0);
                        //BJH 인증 화면 변화
                        SetMenuMode(0);
                    }

                    currentViewMode = 0; // RESET
                }
            }
        });

        m_ButtonReqC = (Button) findViewById(R.id.buttonAuthReqC);
        m_ButtonReqC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String dist = m_EditTextDistC.getText().toString();
                dist = dist.replaceAll(" ", "");
                m_EditTextDist.setText(dist);
                String textNumber = m_EditTextNumberC.getText().toString();
                m_EditTextNumber.setText(textNumber);
                if (m_CountryCode != null && m_CountryCode.length() > 0) {
                    if (m_CountryCode.equals("+82")) {
                        ShowLocalAuthC();
                    } else {
                        if (textNumber != null && textNumber.length() > 0) {
                            SetMenuMode(1);
                            SetViewMode(0);
                            AuthRequest();
                        } else {
                            toastMessage();
                            return;
                        }
                    }
                } else {
                    toastMessage();
                    return;
                }
            }
        });

        m_ProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        m_TextView = (TextView) findViewById(R.id.textViewAuth);
        m_TextNameView = (TextView) findViewById(R.id.textEditName);

        m_CheckBox = (CheckBox) findViewById(R.id.cb_oversea);
        m_CheckBoxOther = (CheckBox) findViewById(R.id.cb_other);

        if (ServicePrefs.DIAL_AUTH_OTHER) {
            m_CheckBoxOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        m_EditTextNumber.setText("");
                    }
                }
            });
        } else {
            m_CheckBoxOther.setVisibility(View.GONE);
        }


        // CHECK TERM
        boolean termOk = mPrefs.getPreferenceBooleanValue(AppPrefs.TERM_OK);
        if (!termOk) {
            Intent intent = new Intent(AuthActivity.this, TermsActivity.class);
            startActivityForResult(intent, SELECT_TERM);
        } else {
            NewAuthWebView();
        }
    }

    private void toastMessage() {
        LayoutInflater inflater = getLayoutInflater();
        View toastRoot = inflater.inflate(R.layout.toast_view, null);
        TextView textView = (TextView) toastRoot.findViewById(R.id.toast_text);
        textView.setText(mContext.getResources().getString(R.string.ui_auth_desc));

        Toast toast = new Toast(mContext);
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.auth, menu);
        return true;
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onPause");
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        // by sgkim : 2015-08-26 : TODO : 향후 메인쓰레드에서 네트워크 관련 I/O는 수정할 예정입니다.
        // 임시로 조치함.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        Log.d(THIS_FILE, "onResume");

        if (m_DialNumber != null && m_DialNumber.length() > 0) {
            ContactHelper.removeDirectNumber(this, getResources().getString(R.string.auth_number_contact_name), m_DialNumber);
            m_DialNumber = null;
        }

        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTH_CID);
        if (cid != null && cid.length() > 0) {
            // 인증확인 필요
            if (currentViewMode == 0) {
				/*SetViewMode(1);
				if (auth_check(cid, null) < 0)
				{
					auth_cancel();
					SetViewMode(0);
				}*/
                //BJH
                auth_cancel();
                SetViewMode(0);
            } else if (currentViewMode == 2) {
                // 인증코드 입력
                SetViewMode(3);
            } else if (currentViewMode == 3) {
                // 인증코드 입력
                SetViewMode(3);
            } else {
                auth_cancel();
                SetViewMode(0);
            }
        } else {
            // 인증 입력 필드 표시
            SetViewMode(0);
        }
        //BJH
        UpdateCode();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onDestroy");
        super.onDestroy();
    }

    private String getMyPhoneNumber() {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumber;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            phoneNumber = "Not Found";
        }
        else  phoneNumber = mTelephonyMgr.getLine1Number();
        if (phoneNumber == null) return "";
        if (phoneNumber.startsWith("+82"))
        {
            phoneNumber = phoneNumber.replace("+82", "0");
        }
        return phoneNumber;
    }

    private boolean getPhoneState() {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        int state = mTelephonyMgr.getPhoneType();
        if (state == TelephonyManager.PHONE_TYPE_NONE) return false;

        state = mTelephonyMgr.getSimState();
        if (state != TelephonyManager.SIM_STATE_READY) return false;

        //state = mTelephonyMgr.getNetworkType();
        //if (state == TelephonyManager.NETWORK_TYPE_UNKNOWN ) return false;

        return true;
    }

    private void showMessage(String msg) {
        Resources res = getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(this).create();
        msgDialog.setMessage(msg);
        msgDialog.setButton(res.getString(R.string.confirm),
                new DialogInterface.OnClickListener() {
                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewAuthWebView();//BJH 새로운 인증 방법
                        return;
                    }
                });
        msgDialog.show();
    }

    private void confirmCall(final String number) {
        Resources res = getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(this).create();
        msgDialog.setTitle(getResources().getString(R.string.auth_number_tittle));
        msgDialog.setMessage(getResources().getString(R.string.auth_number_quest));
        msgDialog.setButton(res.getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // call
                        gsmCall(number);
                        return;
                    }
                });
        msgDialog.setButton2(res.getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    // @Override
                    public void onClick(DialogInterface dialog, int which) {
                        auth_cancel();
                        return;
                    }
                });
        msgDialog.show();
    }


    @Override
    public void onBackPressed() {
        //BJH 인증 화면 분류를 위해서
		/*if(m_Serial != null && m_Serial.length() > 0) {
			NewAuthWebView();
			return;
		} else {
			if (currentMenuMode != 0)
			{
				auth_cancel();
				SetMenuMode(0);
				return;
			}
		}*/
        if (currentMenuMode != 0)
        {
            auth_cancel();
            SetMenuMode(0);
            NewAuthWebView();
            return;
        }
        super.onBackPressed();
    }


    private static int currentMenuMode = 0;
    private void SetMenuMode(int mode)
    {
        switch (mode)
        {
            case 0:
            {
                mLayoutA.setVisibility(View.GONE);
                mLayoutB.setVisibility(View.GONE);
                mLayoutC.setVisibility(View.GONE);
                //BJH 새로운 인증 방법에서 사용자의 혼동을 주지 않기 위해
				/*if(m_Serial != null && m_Serial.length() > 0)
					mLayoutC.setVisibility(View.GONE);
				else
					mLayoutC.setVisibility(View.VISIBLE);*/
            }
            break;
            case 1:
            {
                mLayoutA.setVisibility(View.GONE);
                mLayoutB.setVisibility(View.VISIBLE);
                mLayoutC.setVisibility(View.GONE);
            }
            break;
        }
        currentMenuMode = mode;
    }


    private static int currentViewMode = 0;
    private void SetViewMode(int mode)
    {
        switch (mode)
        {
            case 0:
            {
                // 초기 입력 모드
                m_ButtonCountry.setVisibility(View.VISIBLE);
                m_TextNameView.setVisibility(View.GONE);
                m_EditTextNumber.setVisibility(View.VISIBLE);

                m_TextDistView.setVisibility(View.VISIBLE);
                m_EditTextDist.setVisibility(View.VISIBLE);

                m_ButtonReq.setVisibility(View.VISIBLE);
                m_ProgressBar.setVisibility(View.GONE);
                //m_EditTextNumber.setText(m_PhoneNumber);BJH
                m_TextView.setVisibility(View.VISIBLE);
                UpdateCode();
            }
            break;
            case 1:
            {
                // 인증중
                m_ButtonCountry.setVisibility(View.GONE);
                m_TextNameView.setVisibility(View.GONE);
                m_EditTextNumber.setVisibility(View.GONE);

                m_TextDistView.setVisibility(View.GONE);
                m_EditTextDist.setVisibility(View.GONE);

                m_ButtonReq.setVisibility(View.GONE);
                m_CheckBox.setVisibility(View.GONE);
                if (ServicePrefs.DIAL_AUTH_OTHER)
                    m_CheckBoxOther.setVisibility(View.GONE);

                m_ProgressBar.setVisibility(View.VISIBLE);
                m_TextView.setVisibility(View.VISIBLE);
                m_TextView.setText(getResources().getString(R.string.auth_number_process));
            }
            break;
            case 2:
            {
                // 수신전화 대기중..
                m_ButtonCountry.setVisibility(View.GONE);
                m_TextNameView.setVisibility(View.GONE);
                m_EditTextNumber.setVisibility(View.GONE);

                m_TextDistView.setVisibility(View.GONE);
                m_EditTextDist.setVisibility(View.GONE);

                m_ButtonReq.setVisibility(View.GONE);
                m_CheckBox.setVisibility(View.GONE);
                if (ServicePrefs.DIAL_AUTH_OTHER)
                    m_CheckBoxOther.setVisibility(View.GONE);

                m_ProgressBar.setVisibility(View.VISIBLE);
                m_TextView.setVisibility(View.VISIBLE);
                m_TextView.setText(getResources().getString(R.string.auth_number_wait_call));
            }
            break;
            case 3:
            {
                // 인증코드 입력
                m_ButtonCountry.setVisibility(View.GONE);
                m_TextNameView.setVisibility(View.VISIBLE);
                m_EditTextNumber.setVisibility(View.VISIBLE);

                m_TextDistView.setVisibility(View.GONE);
                m_EditTextDist.setVisibility(View.GONE);

                m_ButtonReq.setVisibility(View.VISIBLE);
                m_ProgressBar.setVisibility(View.GONE);
                m_EditTextNumber.setText("");
                m_CheckBox.setVisibility(View.GONE);
                if (ServicePrefs.DIAL_AUTH_OTHER)
                    m_CheckBoxOther.setVisibility(View.GONE);

                m_TextView.setVisibility(View.VISIBLE);
                m_TextView.setText(getResources().getString(R.string.auth_number_input_code));
            }
            break;
        }
        currentViewMode = mode; // save
    }

    private void UpdateCode()
    {
        // SET BUTTON
        m_ButtonCountry.setText(m_CountryCode);
        //m_ButtonCountry.setText(m_CountryName + "(" + m_CountryCode + ")");
        m_ButtonCountryC.setText(m_CountryCode);

        if(m_CountryCode.equals("+82")) {
            m_EditTextNumberC.setVisibility(View.INVISIBLE);
            mLayoutKorea.setVisibility(View.VISIBLE);
            //m_EditTextNumberC.setText("");
            //m_EditTextNumberC.setEnabled(false);
        }
        else {
            mLayoutKorea.setVisibility(View.GONE);
            m_EditTextNumberC.setText(m_PhoneNumber);
            m_EditTextNumberC.setEnabled(true);
            m_EditTextNumberC.setVisibility(View.VISIBLE);
            m_EditTextNumberC.requestFocus();
        }

        int flag_id = getResources().getIdentifier(m_CountryFlag, "drawable", mContext.getPackageName());
        if (flag_id != 0)
        {
            Drawable icon= getResources().getDrawable(flag_id);
            m_ButtonCountry.setCompoundDrawablesWithIntrinsicBounds( icon, null, null, null );
            m_ButtonCountryC.setCompoundDrawablesWithIntrinsicBounds( icon, null, null, null );
        }

        String code = m_CountryCode;
        Log.e(THIS_FILE, "CODE : " + code);


        if (code !=null && code.equals("+82"))
        {
            m_TextView.setText(getResources().getString(R.string.auth_number_call_quest));
            m_CheckBox.setVisibility(View.VISIBLE);
            m_CheckBox.setChecked(false);

            if (ServicePrefs.DIAL_AUTH_OTHER)
            {
                m_CheckBoxOther.setVisibility(View.VISIBLE);
                m_CheckBoxOther.setChecked(false);
            }
        }
        else
        {
            m_TextView.setText(getResources().getString(R.string.auth_number_call_oversea));
            m_CheckBox.setVisibility(View.GONE);
            m_CheckBox.setChecked(false);

            if (ServicePrefs.DIAL_AUTH_OTHER)
            {
                m_CheckBoxOther.setVisibility(View.VISIBLE);
                m_CheckBoxOther.setChecked(false);
            }
        }
    }

    private void AuthRequest()
    {
        String number = m_EditTextNumber.getText().toString();

        // 순수 전화번호만.
        try
        {
            number = PhoneNumberUtils.stripSeparators(number);
        }
        catch(Exception e)
        {
        }

        // 공백은 제거
        number = number.replaceAll(" ", "");

        if (number.length() == 0)
        {
            showMessage(getResources().getString(R.string.auth_number_input_number));
            m_EditTextNumber.requestFocus();
            m_EditTextNumber.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    InputMethodManager keyboard = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.showSoftInput(m_EditTextNumber, 0);
                }
            },200);
            return;
        }

        String dist = m_EditTextDist.getText().toString();

        // 공백은 제거
        dist = dist.replaceAll(" ", "");


		/*
		if (dist.length() == 0)
		{
			showMessage(getResources().getString(R.string.auth_number_input_dcode));
			m_EditTextDist.requestFocus();
			m_EditTextDist.postDelayed(new Runnable() {
	            @Override
	            public void run() {
	                // TODO Auto-generated method stub
	                InputMethodManager keyboard = (InputMethodManager)
	                getSystemService(Context.INPUT_METHOD_SERVICE);
	                keyboard.showSoftInput(m_EditTextDist, 0);
	            }
	        },200);
			return;
		}*/

        // 번호인증
        auth(number,dist);
    }

    public static String postData(Context context, String url)
    {
        Log.d(THIS_FILE, "REQ : " + url);

        try
        {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 20000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpPost httppost = new HttpPost(url);


            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }
            br.close();
            return sb.toString();
        }
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private static String currentCode = null;
    private static String currentCountry = null;
    private static String currentNumber = null;
    private static String currentFlag = null;
//	private static String currentDist = null;

    private Boolean isKorea()
    {
        // 발신이 가능한지 검사
        //if (!getPhoneState()) return false;

        if (ServicePrefs.DIAL_AUTH_OTHER)
        {
            if (m_CheckBoxOther.isChecked())
                return false;
        }

        if (currentCode != null && currentCode.equals("+82"))
            return true;
        return false;
    }
    public int auth(String number, String dist)
    {
        // SAVE
        currentCode = m_CountryCode;;
        currentCountry = m_CountryName;;
        currentNumber = number;
        currentFlag = m_CountryFlag;
//		currentDist = dist;

        String code = m_CountryCode;

        String url = null;
        if (!isKorea())
        {
            url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_VALIDCID_INTL ERROR");
                showMessage("URL_REQ_VALIDCID_INTL ERROR");
                return -1;
            }

        }
        else
        {
            url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_VALIDCID_LOCL ERROR");
                showMessage("URL_REQ_VALIDCID_LOCL ERROR");
                return -1;
            }
        }

        // url encoding
        try {
            number = URLEncoder.encode(number, "utf-8");
            dist = URLEncoder.encode(dist, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String qry_url = url + "?country_code="+code+"&callerid="+number + "&distributor_id="+dist;

        String str = postData(mContext, qry_url);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String dialNumber = null;
        String cid = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject certObject = jObject.getJSONObject("CERT_INFO");
                if(certObject.has("DIAL_NUMBER"))
                    dialNumber = certObject.getString("DIAL_NUMBER");
                if(certObject.has("CID"))
                    cid = certObject.getString("CID");
            }
            else
            {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (isKorea())
        {
            if (dialNumber == null || dialNumber.length() == 0)
            {
                // 인증요청 오류
                showMessage(getResources().getString(R.string.auth_number_error_contact));
                return -1;
            }

            if (cid == null || cid.length() == 0)
            {
                // 인증요청 오류
                showMessage(getResources().getString(R.string.auth_number_error_cid));
                return -1;
            }

            // 공백은 제거
            cid = cid.replaceAll(" ", "");

            // SAVE ID
            mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, cid);

            // CALL
            confirmCall(dialNumber);
        }
        else
        {
            if (cid == null || cid.length() == 0)
            {
                // 인증요청 오류
                showMessage(getResources().getString(R.string.auth_number_error_cid));
                return -1;
            }

            // 공백은 제거
            cid = cid.replaceAll(" ", "");


            // SAVE ID
            mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, cid);

            // 체크박스
//			if (ServicePrefs.DIAL_AUTH_OTHER && m_CheckBoxOther.isChecked())
//			{
//				SetViewMode(3);
//			}
//			else
            {
                // WAIT FOR INCOMMING CALL
                SetViewMode(2);

                // 발신이 안되는 전화기라면 바로 인증코드 입력으로 넘어가자.
                if (!getPhoneState())
                {
                    // 약 2초후에 자동 이동.
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            SetViewMode(3);
                        }
                    },2000);
                }
            }
        }
        return 0;
    }

    private void gsmCall(String number) {
        Log.i(THIS_FILE, "gsmCall : " + number);


        if (!m_CheckBox.isChecked())
            number = number.replaceAll("\\+82", "0");

        m_DialNumber = number;
        ContactHelper.addDirectNumber(this, getResources().getString(R.string.auth_number_contact_name), number);

        //Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(String.format("tel:%s", number)));
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", number)));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public int auth_check(String number, String code)
    {
        if (ServicePrefs.DIAL_TEST)
        {
            if (ServicePrefs.login(this, "", "") >= 0)
            {
                Intent intent = new Intent(AuthActivity.this, DialMain.class);
                startActivity(intent);
                finish();
            }
            return 0;
        }

        if (number != null)
            number = number.replaceAll(" ", "");


        String qry_url = null;
        if (code != null)
        {
            String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO_INTL ERROR");
                showMessage("URL_REQ_ACCOUNT_INFO_INTL ERROR");
                return -1;
            }
            qry_url = url + "?cid="+number + "&valid_code=" + code;
        }
        else
        {
            String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO_LOCL ERROR");
                showMessage("URL_REQ_ACCOUNT_INFO_LOCL ERROR");
                return -1;
            }
            qry_url = url + "?cid="+number;
        }

        String str = postData(mContext, qry_url);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String id = null;
        String password = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                if(certObject.has("ID"))
                    id = certObject.getString("ID");
                if(certObject.has("PASSWORD"))
                    password = certObject.getString("PASSWORD");
            }
            else
            {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (id == null || id.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_id));
            return -1;
        }

        if (password == null || password.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_pwd));
            return -1;
        }

        // SAVE NUMBER
        String cid = mPrefs.getPreferenceStringValue(AppPrefs.AUTH_CID);
        mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, cid);
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);

        // DO LOGIN
        if (ServicePrefs.login(this, id, password) >= 0)
        {
            Intent intent = new Intent(AuthActivity.this, DialMain.class);
            startActivity(intent);
            finish();
        }
        else
        {
            showMessage(getResources().getString(R.string.error_login));
            return -1;
        }
        return 0;
    }

    public void auth_cancel()
    {
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");
    }

    private void ShowCountry()
    {
        Intent intent = new Intent(AuthActivity.this, CountryList.class);
        startActivityForResult(intent, SELECT_COUNTRY);
    }

    private void ShowCountryC()
    {
        Intent intent = new Intent(AuthActivity.this, CountryList.class);
        startActivityForResult(intent, SELECT_COUNTRY_C);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_COUNTRY:
                if (resultCode == RESULT_OK) {
                    Log.d(THIS_FILE, "RESULT : " + data.toString());

                    m_CountryCode = data.getStringExtra("COUNTRY_CODE");
                    m_CountryName = data.getStringExtra("COUNTRY_NAME");
                    m_CountryFlag = data.getStringExtra("COUNTRY_FLAG");
                    //BJH 최초 인증 받은 국가 코드를 Dial.java에서 세팅하기 위해서
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE, m_CountryCode);
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG, m_CountryFlag);

                    if(m_CountryCode.equals("+82")) {
                        ShowLocalAuth();
                    } else {
                        UpdateCode();
                    }

				/*
				mCountryCode = data.getStringExtra("COUNTRY_CODE");
				mCountryName = data.getStringExtra("COUNTRY_NAME");
				mCountryGmt = data.getStringExtra("COUNTRY_GMT");
				countryName.setText(mCountryCode + " " + mCountryName);
				countryTime.setText(data.getStringExtra("COUNTRY_TIME"));
				mSaveNumber = mCountryCode;
				mSaveName = null;
				*/
                }
                break;
            case SELECT_TERM:
                if (resultCode != RESULT_OK)
                {
                    finish();
                } else {
                    //BJH 새로운 인증 화면
                    NewAuthWebView();
                }
                break;
            case SELECT_AUTH:
            {

				if (!ServicePrefs.DIAL_RETAIL && ServicePrefs.DIAL_AUTH_SGKIM)
				{
					String cid = "01024654366";
    				String country_code = "82";
    				String distributor_id = "";
    				// 완료
    				if (auth_local(cid, country_code, distributor_id) < 0)
    				{
    					SetMenuMode(0);
    				}
    				return;
				}

                if (resultCode == RESULT_OK)
                {
                    String cert_cid = data.getStringExtra("cid");
                    String country_code = data.getStringExtra("country_code");
                    String distributor_id = data.getStringExtra("distributor_id");
                    String birth_date = data.getStringExtra("birth_date");
                    String auth_type = data.getStringExtra("auth_type");
                    String flag = data.getStringExtra("flag");
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE, "+" + country_code);
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG, flag);
                    /*if(auth_type.equals("local")) {
                        //KCP 본인 인증 완료
                        if (auth_local(cert_cid, country_code, distributor_id) < 0)
                        {
                            SetMenuMode(0);
                        }
                    } else {
                        m_PhoneNumber = cert_cid;
                        m_EditTextDist.setText(distributor_id);
                        m_EditTextNumber.setText(cert_cid);
                        m_CountryCode = country_code;
                        SetMenuMode(1);
                        SetViewMode(0);
                        AuthRequest();
                    }*/
                    // BJH 2017.04.03 => 2017.04.27 인증 문자가 안되는 경우 해외전화로 전화 인증
                    if (new_auth(cert_cid, country_code, distributor_id, birth_date, auth_type) < 0) {
                        SetMenuMode(0);
                    }
                }
                else
                {
                    SetMenuMode(0);
                    //BJH
                    //NewAuthWebView();
					/*if(m_Serial != null && m_Serial.length() > 0)
						finish();*/
                    finish();
                }
            }
            break;
            case SELECT_COUNTRY_C:
                if (resultCode == RESULT_OK) {
                    Log.d(THIS_FILE, "RESULT : " + data.toString());

                    m_CountryCode = data.getStringExtra("COUNTRY_CODE");
                    m_CountryName = data.getStringExtra("COUNTRY_NAME");
                    m_CountryFlag = data.getStringExtra("COUNTRY_FLAG");
                    //BJH 최초 인증 받은 국가 코드를 Dial.java에서 세팅하기 위해서
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE, m_CountryCode);
                    mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG, m_CountryFlag);

                    UpdateCode();

				/*
				mCountryCode = data.getStringExtra("COUNTRY_CODE");
				mCountryName = data.getStringExtra("COUNTRY_NAME");
				mCountryGmt = data.getStringExtra("COUNTRY_GMT");
				countryName.setText(mCountryCode + " " + mCountryName);
				countryTime.setText(data.getStringExtra("COUNTRY_TIME"));
				mSaveNumber = mCountryCode;
				mSaveName = null;
				*/
                }
                break;
            default:
                break;
        }
        return;
    }

    private void ShowLocalAuth()
    {
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2);
        Log.d(THIS_FILE,"ShowLocalAuth url:"+url);
        if (url == null || url.length() == 0)
            return;
        String urlString = url;
        Intent intent = new Intent(AuthActivity.this, WebClient.class);
        intent.putExtra("url", urlString);
        intent.putExtra("title",getResources().getString(R.string.auth_number_self));
        startActivityForResult(intent, SELECT_AUTH);
    }

    private void ShowLocalAuthC()
    {
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2);
        Log.d(THIS_FILE,"ShowLocalAuthC url:"+url);
        if (url == null || url.length() == 0)
            return;
        String urlString = url + "?distributor_id=" + m_EditTextDist.getText().toString();
        Intent intent = new Intent(AuthActivity.this, WebClient.class);
        intent.putExtra("url", urlString);
        intent.putExtra("title",getResources().getString(R.string.auth_number_self));
        startActivityForResult(intent, SELECT_AUTH);
    }

    private void ShowManual()
    {
		/*String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2);
		if (url == null || url.length() == 0)
			return;*/
        String urlString = "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/maaltalk_cert_check.php";
        Intent intent = new Intent(AuthActivity.this, WebClient.class);
        intent.putExtra("url", urlString);
        intent.putExtra("title",getResources().getString(R.string.auth_number_self));
        startActivityForResult(intent, SELECT_AUTH);
    }

    public int auth_local(String number, String country_code, String distributor_id)
    {
        String qry_url = null;
        {
            String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO_LOCL_V2 ERROR");
                showMessage("URL_REQ_ACCOUNT_INFO_LOCL_V2 ERROR");
                return -1;
            }
            qry_url = url + "?cid="+number + "&country_code=" + country_code + "&distributor_id=" + distributor_id;
        }


        String str = postData(mContext, qry_url);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String id = null;
        String password = null;
        String cid = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                if(certObject.has("ID"))
                    id = certObject.getString("ID");
                if(certObject.has("PASSWORD"))
                    password = certObject.getString("PASSWORD");
                if(certObject.has("CID"))
                    cid = certObject.getString("CID");

            }
            else
            {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (id == null || id.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_id));
            return -1;
        }

        if (password == null || password.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_pwd));
            return -1;
        }

        // SAVE NUMBER
        mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, cid);
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);

        // DO LOGIN
        if (ServicePrefs.login(this, id, password) >= 0)
        {
            Intent intent = new Intent(AuthActivity.this, DialMain.class);
            startActivity(intent);
            finish();
        }
        else
        {
            showMessage(getResources().getString(R.string.error_login));
            return -1;
        }
        return 0;
    }

    private void ShowNewAuth(String url)//BJH 새로운 방식의 인증 페이지
    {
        if (url == null || url.length() == 0)
            return;
        Intent intent = new Intent(AuthActivity.this, WebClient.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);//BJH 2016.09.20 본인인증 관련 수정
        intent.putExtra("url", url);
        intent.putExtra("title",getResources().getString(R.string.auth_number_self));
        startActivityForResult(intent, SELECT_AUTH);
    }

    private void NewAuthWebView()
    {
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2);//BJH 새로운 인증 관련
        if(url != null && url.length() > 0) {
            if(!m_CountryCode.startsWith("+"))
                m_CountryCode = "+" + m_CountryCode;
            String urlStr = url+ "?locale="+m_Locale+"&country_code="+m_CountryCode+"&phone_number="+getMyPhoneNumber()+"&v2=y&ver=2";
            ShowNewAuth(urlStr);
        }
    }

    //BJH 2017.04.03
    public int new_auth(String number, String country_code, String distributor_id, String birth_date, String auth_type)
    {
        String qry_url = null;
        {
            String url = null;
            if(auth_type.equals("local"))
                url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3);
            else
                url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2);
            if (url == null || url.length() == 0)
            {
                Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO ERROR");
                showMessage("URL_REQ_ACCOUNT_INFO ERROR");
                return -1;
            }
            qry_url = url + "?cid="+number + "&country_code=" + country_code + "&distributor_id=" + distributor_id + "&birth_date=" + birth_date;
        }


        String str = postData(mContext, qry_url);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String id = null;
        String password = null;
        String cid = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                if(certObject.has("ID"))
                    id = certObject.getString("ID");
                if(certObject.has("PASSWORD"))
                    password = certObject.getString("PASSWORD");
                if(certObject.has("CID"))
                    cid = certObject.getString("CID");

            }
            else
            {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (id == null || id.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_id));
            return -1;
        }

        if (password == null || password.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_pwd));
            return -1;
        }

        // SAVE NUMBER
        mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, cid);
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);

        // DO LOGIN
        if (ServicePrefs.login(this, id, password) >= 0)
        {
            Intent intent = new Intent(AuthActivity.this, DialMain.class);
            startActivity(intent);
            finish();
        }
        else
        {
            showMessage(getResources().getString(R.string.error_login));
            return -1;
        }
        return 0;
    }

}
