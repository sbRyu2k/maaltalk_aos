package com.dial070.ui;


//import java.util.List;

import java.util.List;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.telephony.TelephonyManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.dial070.widgets.IndexScrollView;
import com.dial070.widgets.IndexScrollView.OnScrollChangeListener;

import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
//import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class CountryList extends AppCompatActivity implements OnScrollChangeListener {
	
//	private static final String THIS_FILE = "GCALL-COUNTRY";

//	private static final int NORMAL_COUNTRY_COLOR = 0x005ca2cd;
//	private static final int PRESSED_COUNTRY_COLOR = 0xff5ca2cd;
	
	private Context mContext;
	private EditText mCountrySearch;
	private ListView mCountryListView;	

	private CountryListAdapter mCountryAdapter;

	private CountrySearchAdapter mSearchAdapter;
//	private InputMethodManager mImm;
	private CountryItem mCountryItem;
	private String oldSearchKey;
	private String searchKey;	
	private boolean mSearchMode;
	private IndexScrollView mScrollView;
	private AppPrefs mPrefs;
	
//	private String getMyPhoneCountry() {
//
//		String roaming_code = null;
//		TelephonyManager mTelephonyMgr;
//		mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//		roaming_code = mTelephonyMgr.getNetworkCountryIso();
//
//		Log.d(THIS_FILE, "ROAMING : " + roaming_code);
//
//		return roaming_code;
//	}
//
//	private String getSIMCountry() {
//		TelephonyManager mTelephonyMgr;
//		mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//		String sim_code = mTelephonyMgr.getSimCountryIso();
//		Log.d(THIS_FILE, "SIM : " + sim_code);
//		return sim_code;
//	}

//	private Boolean isRoaming() {
//		String country = getMyPhoneCountry();
//		if (country == null)
//			return false;
//		if (country.equalsIgnoreCase("kr"))
//			return false;
//		return true;
//	}
//
//	private Boolean isOverSeas() {		
//		String country = getSIMCountry();
//		if (country == null)
//			return false;
//		if (country.equalsIgnoreCase("kr"))
//			return false;
//		return true;
//	}

//	private Integer getBillCode(Boolean bill) {
//		Integer code = 0;
//		if (bill)
//			code = 1;
//		if (isRoaming()) {
//			if (isOverSeas()) {
//				code = 3;
//			} else {
//				code = 2;
//			}
//		}
//		return code;
//	}

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final androidx.appcompat.app.ActionBar abar = getSupportActionBar();
		androidx.appcompat.app.ActionBar.LayoutParams params = new androidx.appcompat.app.ActionBar.LayoutParams(//Center the textview in the ActionBar !
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				Gravity.CENTER);
		ImageButton imgBtnBack = viewActionBar.findViewById(R.id.imgBtnBack);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(false);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		imgBtnBack.setVisibility(View.VISIBLE);
		imgBtnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setTitle(getString(R.string.ui_select_country_code));

		setContentView(R.layout.country_list);
		
		mContext = this;
		mPrefs = new AppPrefs(this);
		
//		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				
		mCountryListView = (ListView) findViewById(R.id.country_list_view);
				
		//mBtnCountry.setBackgroundResource(android.R.drawable.)
		
		mCountrySearch = (EditText) findViewById(R.id.country_search);
		mScrollView = (IndexScrollView) findViewById(R.id.indexscrollview);
		mScrollView.setOnScrollChangeListener(this);
		//mScrollView.setLocale(mPrefs.getPreferenceStringValue(AppPrefs.LOCALE));
		
		searchKey = "";
		oldSearchKey = "";		
		mSearchMode = false;
		mCountryItem = null;
		
		mSearchAdapter = new CountrySearchAdapter(this);
		mCountryAdapter = new CountryListAdapter(this);
		mCountryAdapter.setData(CountryContainer.getFullCountry(mContext,mPrefs.getPreferenceStringValue(AppPrefs.LOCALE),false));
		mSearchAdapter.setData(CountryContainer.getFullCountry(mContext,mPrefs.getPreferenceStringValue(AppPrefs.LOCALE),false));
		mCountryListView.setAdapter(mCountryAdapter);		
		
	
		try {
			mCountrySearch.addTextChangedListener(new TextWatcher() {

				//@Override
				public void afterTextChanged(Editable s) {
					// 초성 검색 
					try {
						
						searchKey = mCountrySearch.getText().toString();
						if(!oldSearchKey.equalsIgnoreCase(searchKey))
						{
							oldSearchKey = searchKey;
							doSearch();
						}
						

					} catch (Exception e) {
						Log.e("ContactsActivity",e.getMessage(),e);
					}					
				}

				//@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					//do nothing
				}

				//@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					//do nothing
				}
				
			});
		} catch (Exception e) {
			//
		}
		
		mCountryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			//@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				if(mSearchMode)
					mCountryItem = (CountryItem) mSearchAdapter.getItem(arg2);
				else
					mCountryItem = (CountryItem) mCountryAdapter.getItem(arg2);
				if(mCountryItem != null)
				{
					intent.putExtra(mCountryItem.FIELD_COUNTRY_NAME, mCountryItem.getName());
					intent.putExtra(mCountryItem.FIELD_COUNTRY_CODE, mCountryItem.Code);
					intent.putExtra(mCountryItem.FIELD_COUNTRY_FLAG, mCountryItem.Flag);
					intent.putExtra(mCountryItem.FIELD_COUNTRY_TIME, mCountryItem.getGmtTime(mCountryItem.Gmt));
					intent.putExtra(mCountryItem.FIELD_COUNTRY_GMT, mCountryItem.Gmt);
					setResult(RESULT_OK,intent);
				}
				else
				{
					setResult(RESULT_CANCELED);
				}

				finish();					
			}
			
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case android.R.id.home:
				onBackPressed();
				break;
		}
		return true;
	}

	@Override
	protected void onDestroy() {
		mSearchAdapter = null;
		mCountryAdapter = null;
		super.onDestroy();
	}	
	
	private boolean doSearch()
	{
		if(searchKey.length() == 0) 
		{
			mSearchMode = false;
			mScrollView.setVisibility(View.VISIBLE);
			mCountryListView.setAdapter(mCountryAdapter); 
			mCountryAdapter.notifyDataSetChanged();
			
			return true;
		}
		List<CountryItem> contactsSearchList = null;//BJH 수정
		mSearchMode = true;
		mScrollView.setVisibility(View.GONE);
		contactsSearchList = mSearchAdapter.getSearchItem(searchKey);
		
		mSearchAdapter.notifyDataSetChanged();
		
		mCountryListView.setAdapter(mSearchAdapter);   
		
		return true;
	}	

	//@Override
	public void onTrigger(String section) {
		// TODO Auto-generated method stub
		if(searchKey.length() == 0) 
		{
			int position = mCountryAdapter.getSectionPosition(section);

			mCountryListView.setSelection(position);
		}			
	}
	
}
