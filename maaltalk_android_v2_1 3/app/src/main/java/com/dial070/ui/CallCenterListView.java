package com.dial070.ui;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
//import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.utils.PhoneNumberTextWatcher;

public class CallCenterListView extends LinearLayout {

//	private Context mContext;
//	private ImageView mIcon;
	private TextView mTitle;
	private TextView mAddr;	
	private TextView mPhone;	
	//private NaverResult mResultItem;
	
	public CallCenterListView(Context context)
	{
		super(context);
//		mContext = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.call_center_list_item, this, true);
		//mIcon = (ImageView) findViewById(R.id.naver_icon);
		//mIcon.setVisibility(View.GONE);
		
		mTitle = (TextView) findViewById(R.id.call_center_title);
		mPhone = (TextView) findViewById(R.id.call_center_telephone);
		mAddr = (TextView) findViewById(R.id.call_center_addr);
		PhoneNumberTextWatcher digitFormater = new PhoneNumberTextWatcher();
		mPhone.addTextChangedListener(digitFormater);			
		//this.mResultItem = item;
	}
	
	public void setData(String title, String addr, String phone_number)
	{
        //Spanned s = Html.fromHtml(title);

        mTitle.setText(Html.fromHtml(title));
		//mTitle.setText(title);
        mAddr.setText(addr);
		mPhone.setText(phone_number);
	}
	
	public TextView getTitle()
	{
		return mTitle;
	}
	
	public TextView getPhone()
	{
		return mPhone;
	}	
	
//	public NaverResult getItem()
//	{
//		return mResultItem;
//	}
	

	
	
}
