package com.dial070.ui;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.dial070.maaltalk.R;

import com.dial070.db.DBManager;
import com.dial070.sip.api.*;
import com.dial070.sip.service.SipService;
import com.dial070.utils.Log;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class SmsMsgContsDetail extends Activity {
	private static final String THIS_FILE = "SmsMsgContsDetail";
	private TextView leftTitle;
	private TextView rightTitle;
	private Context mContext;
	
	private TextView mMsgView,mNameView;
	private TextView mDateView;
	private TextView mPhoneView;
	private ImageButton mSipCall, mReply, mDelete;
		
	private int mMsgId;
	private String mMsg,mPhoneNumber,mUserName;
	
	private static final int ALERT_DIALOG = 1;
	@SuppressLint("HandlerLeak")
	private Handler msgHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case ALERT_DIALOG:
				{
					AlertDialog.Builder msgDialog = new AlertDialog.Builder(SmsMsgContsDetail.this);
					msgDialog.setTitle(getResources().getString(R.string.app_name));
					if (msg.obj != null) msgDialog.setMessage((String) msg.obj);
					else msgDialog.setMessage(msg.arg1);
					msgDialog.setIcon(R.drawable.ic_launcher);
					msgDialog.setNegativeButton(getResources().getString(R.string.close), null);
					msgDialog.show();
				}
					break;
			}
		}
	};			
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;

		// Bind to the service
		if (getParent() != null)
		{
			contextToBindTo = getParent();
		}		
		
		//커스텀 타이틀 바 
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);		
		
		setContentView(R.layout.sms_msg_conts_detail);
		
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

		leftTitle = (TextView)findViewById(R.id.left_title);
		rightTitle = (TextView)findViewById(R.id.right_title);
		rightTitle.setText(R.string.app_name);
		
		leftTitle.setTextColor(Color.WHITE);
		leftTitle.setTypeface(Typeface.DEFAULT_BOLD);
		
		rightTitle.setTextColor(Color.WHITE);
		rightTitle.setTypeface(Typeface.DEFAULT_BOLD);			
		leftTitle.setText(getResources().getString(R.string.msg));
		
		mMsgView = (TextView) findViewById(R.id.sms_detail_msg);
		mDateView = (TextView) findViewById(R.id.sms_detail_date);
		mPhoneView = (TextView) findViewById(R.id.sms_detail_number);
		mNameView = (TextView) findViewById(R.id.sms_detail_name);
		
		
		mSipCall = (ImageButton) findViewById(R.id.btn_sip_call);
		mSipCall.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sipCall(mPhoneNumber);
			}
		});
		
		mReply = (ImageButton) findViewById(R.id.btn_sms_reply);
		mReply.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SmsMsgContsDetail.this, SmsWrite.class);
				intent.putExtra("phone", mPhoneNumber);
				intent.putExtra("name", mUserName);
				mContext.startActivity(intent);			
			}
		});
		
		mDelete = (ImageButton) findViewById(R.id.btn_sms_delete);
		mDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mMsgId > 0)
				{
					deleteMsg(mMsgId);
					finish();
				}
			}
		});
		
		PhoneNumberFormattingTextWatcher digitFormater = new PhoneNumberFormattingTextWatcher();
		mPhoneView.addTextChangedListener(digitFormater);		
		
		Intent intent = getIntent();
		mMsgId = intent.getIntExtra("id", 0);
		mMsg = intent.getStringExtra("msg");
		mPhoneNumber = intent.getStringExtra("phone");
		String date = intent.getStringExtra("date");
		mUserName = intent.getStringExtra("name");
		
		if(mUserName == null)
		{
			mNameView.setVisibility(View.GONE);
		}
		else
		{
			mNameView.setText(mUserName);
		}
		
		long time = 0;
		try {
			time  = Long.parseLong(date.trim());
		} catch (NumberFormatException e)
		{
		}

		String smsMsgDate = dateFormatTime(time);
		
		mMsgView.setText(mMsg);
		mDateView.setText(smsMsgDate);
		mPhoneView.setText(mPhoneNumber);
		
	}
	
	private boolean deleteMsg(int id)
	{
		DBManager database = new DBManager(mContext);
		
		try {
			database.open();
			//개별 메세지 삭제 
			if(database.deleteSmsMsg(id))
			{
				//메제지 카운트가 (헤더 제외) 0이면 문자목록 리스트에서 삭제 
				int c = database.getSmsMsgCount(mPhoneNumber);
				if(c == 0)
				{
					database.deleteSmsList(mPhoneNumber);
				}
			}
			
		} 
		catch(SQLException e)
		{
			e.printStackTrace();
		}	
		database.close();
		return true;
	}
	

	
	private String dateFormatTime(long time)
	{
		String rs = "";
		if(time <= 0) return rs;
		Date temp = new Date(time);	
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd h:mm a",Locale.getDefault());
	    rs = formatter.format(temp);
		return rs;
	}
	
	
	public boolean sipCall(String number)
	{
		// check number
		if (number == null) 
		{
			return false;
		}

		// check service
		if (mSipService == null)
		{
			Log.i(THIS_FILE, "SERIVE BIND ERROR");
			msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG, R.string.sip_make_call_error, 0));
			return false;
		}

		final String toCall = PhoneNumberUtils.stripSeparators(number);		
		Thread t = new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					SipProfileState accountInfo;
					accountInfo = mSipService.getSipProfileState(0);
					if (accountInfo != null && accountInfo.isValidForCall())
					{
						mSipService.makeCall(toCall, 0);
					}
					else
					{
						Log.e(THIS_FILE, "Service can't be called to make the call");						
						//Message Box
						msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG, R.string.sip_make_call_error, 0));
					}
				}
				catch (RemoteException e)
				{
					Log.e(THIS_FILE, "Service can't be called to make the call");
					msgHandler.sendMessage(msgHandler.obtainMessage(ALERT_DIALOG, R.string.sip_make_call_error, 0));
				}

			}
		};
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
		
		return true;
	}
	
	
	private Activity contextToBindTo = this;
	private ISipService mSipService;
	private ServiceConnection connection = new ServiceConnection()
	{

		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1)
		{
			mSipService = ISipService.Stub.asInterface(arg1);
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0)
		{
			mSipService = null;
		}
	};

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// Unbind service
		try
		{
			contextToBindTo.unbindService(connection);
		}
		catch (Exception e)
		{
			// Just ignore that -- TODO : should be more clean
			Log.w(THIS_FILE, "Unable to un bind", e);
		}				
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// Bind service
		contextToBindTo.bindService(new Intent(contextToBindTo, SipService.class), connection, Context.BIND_AUTO_CREATE);

	}
		
}
