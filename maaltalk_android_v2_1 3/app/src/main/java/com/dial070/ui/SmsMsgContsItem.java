package com.dial070.ui;

public class SmsMsgContsItem {
	private int mId;
	private String mFrom;
	private String mTo;
	private String mChatMsg;
	private String mTime;
	private int mType;
	
	private String mFile1;
	private String mFile2;
	private String mFile3;

	private boolean mShowDay; // 날짜 구분선 표시여부.
	//BJH 2016.10.13
	private String mVMS;
	
	public SmsMsgContsItem(int id, String from, String to, String msg, String time, int type, String file1, String file2, String file3, String vms)
	{
		mId = id;
		mFrom = from;
		mTo = to;
		mChatMsg = msg;
		mTime = time;
		mType = type;
		
		mFile1 = file1;
		mFile2 = file2;
		mFile3 = file3;
		
		mVMS = vms;
	}

	public int getSmsMsgId()
	{
		return mId;
	}
		
	
	public String getSmsMsgFrom()
	{
		return mFrom;
	}
	
	public String getSmsMsgTo()
	{
		return mTo;
	}	
	
	public String getSmsMsg()
	{
		return mChatMsg;
	}
	
	public String getSmsMsgTime()
	{
		return mTime;
	}	
	
	public int getSmsMsgType()
	{
		return mType;
	}	
	
	public String getSmsFile1()
	{
		return mFile1;
	}		
	public String getSmsFile2()
	{
		return mFile2;
	}	
	
	public String getSmsFile3()
	{
		return mFile3;
	}
	
	public boolean getShowDay()
	{
		return mShowDay;
	}
		
	
	public void setFile1(String file)
	{
		mFile1 = file;	
	}
	
	public void setFile2(String file)
	{
		mFile2 = file;	
	}
	
	public void setFile3(String file)
	{
		mFile3 = file;	
	}	
	
	public void setShowDay(boolean showday)
	{
		mShowDay = showday;
	}
	//BJH 2016.10.13
	public String getVMS()
	{
		return mVMS;
	}
}
