package com.dial070.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dial070.DialSplash;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;

import java.util.ArrayList;
import java.util.List;

public class KisaActivity extends AppCompatActivity {

	private TelephonyManager mTelephonyManager;
	private AppPrefs mPrefs;
	private Context mContext;
	private Button mConfirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		DebugLog.d("onCreate()-->");

		mContext = this;
		mPrefs = new AppPrefs(this);

		setContentView(R.layout.activity_kisa);

		// BJH 2017.10.18 CHECK USIM
		if(mTelephonyManager == null)
			mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (mTelephonyManager.getSimState() != TelephonyManager.SIM_STATE_READY && ServicePrefs.DIAL_RETAIL){
			new AlertDialog.Builder(this)
					.setTitle(getResources().getString(R.string.app_name))
					.setMessage(getResources().getString(R.string.mt_usim_none) + getResources().getString(R.string.mt_cs_maaltalk))
					.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							finish();
						}
					})
					.show();
		}

		mConfirm = (Button) findViewById(R.id.btn_confirm);

		mConfirm.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					int hasPermissionContact = checkSelfPermission( Manifest.permission.READ_CONTACTS );
					int hasPermissionAudio = checkSelfPermission( Manifest.permission.RECORD_AUDIO );
					int hasPermissionState = checkSelfPermission( Manifest.permission.READ_PHONE_STATE );

//					int hasPermissionWrite = checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE );
					int hasPermissionRead = checkSelfPermission( Manifest.permission.READ_EXTERNAL_STORAGE );
					//int hasPermissionAlert= checkSelfPermission( Manifest.permission.SYSTEM_ALERT_WINDOW );
					int hasPermissionCallPhone= checkSelfPermission( Manifest.permission.CALL_PHONE );

					int hasPermissionBluetooth = checkSelfPermission(Manifest.permission.BLUETOOTH);

					List<String> permissions = new ArrayList<String>();

					if( hasPermissionContact != PackageManager.PERMISSION_GRANTED ) {
						permissions.add( Manifest.permission.READ_CONTACTS );
					}
					if( hasPermissionAudio != PackageManager.PERMISSION_GRANTED ) {
						permissions.add( Manifest.permission.RECORD_AUDIO );
					}
					if( hasPermissionState != PackageManager.PERMISSION_GRANTED ) {
						permissions.add( Manifest.permission.READ_PHONE_STATE );
					}

					if(hasPermissionBluetooth!=PackageManager.PERMISSION_GRANTED) {
						permissions.add(Manifest.permission.BLUETOOTH);
					}

					if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) { } else {
						int hasPermissionSTATE = checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS);

						if(hasPermissionSTATE != PackageManager.PERMISSION_GRANTED) {
							permissions.add(Manifest.permission.READ_PHONE_NUMBERS);
						}

					}

					if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.S) {
						int hasPermissionBlConnect = checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT);

						if(hasPermissionBlConnect!=PackageManager.PERMISSION_GRANTED) {
							permissions.add(Manifest.permission.BLUETOOTH_CONNECT);
						}
					}

					if(Build.VERSION.SDK_INT<Build.VERSION_CODES.O) {
						int hasPermissionWrite = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

						if( hasPermissionWrite != PackageManager.PERMISSION_GRANTED ) {
							permissions.add( Manifest.permission.READ_EXTERNAL_STORAGE );
						}
					}

					if( hasPermissionRead != PackageManager.PERMISSION_GRANTED ) {
						permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
					}
					if( hasPermissionCallPhone != PackageManager.PERMISSION_GRANTED ) {
						permissions.add( Manifest.permission.CALL_PHONE );
					}
					if( !permissions.isEmpty() ) {
						requestPermissions( permissions.toArray( new String[permissions.size()] ), REQUEST_CODE_PERMISSIONS );

						if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
							if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
								//showMarshmallowAlert(); // BJH 2017.11.14 굳이 할 필요가 없음
							}
						} else if(Build.VERSION.SDK_INT<Build.VERSION_CODES.S) {
							if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_NUMBERS)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
							}
						} else {
							if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_NUMBERS)
									|| shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
									|| shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
							}
						}

					} else {
						goSplash();
					}
				} else {
					goSplash();
				}
			}
		});

	}

	private static final int REQUEST_CODE_PERMISSIONS = 1;
	private AlertDialog mMsgDialog = null;
	private void showMarshmallowAlert() {
		if(mMsgDialog != null)
			return;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(mContext.getResources().getString(
				R.string.permission_setting));
		builder.setMessage(getResources().getString(R.string.permission_desc));
		builder.setCancelable(false);
		builder.setPositiveButton(
				getResources().getString(R.string.yes),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0,
										int arg1) {
						mMsgDialog = null;
						Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
						Uri uri = Uri.fromParts("package", getPackageName(), null);
						intent.setData(uri);
						startActivity(intent);
						finish();
					}
				});
		builder.setNegativeButton(
				getResources().getString(R.string.no),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog,
										int which) {
						mMsgDialog = null;
						finish();
					}
				});
		mMsgDialog=builder.create();
		mMsgDialog.show();
	}
	@TargetApi(Build.VERSION_CODES.M)
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case REQUEST_CODE_PERMISSIONS:
				if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
					if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
						goSplash();
					} else {
						showMarshmallowAlert();
					}
				} else {
					if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
						goSplash();
					} else {
						showMarshmallowAlert();
					}
				}
				break;
		}
	}

	private void goSplash() {
		mPrefs.setPreferenceBooleanValue(AppPrefs.INCLUDE_ADVICE_KISA, true);
		Intent IntroIntent = new Intent(this, DialSplash.class);
		IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(IntroIntent);
		finish();
		return;
	}

}
