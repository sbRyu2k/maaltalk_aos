package com.dial070.ui;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;



//import com.dial070.utils.Log;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.service.MessageService;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.dial070.service.Fcm;
import com.dial070.utils.OnSingleClickListener;

public class Sms extends AppCompatActivity {
	static String THIS_FILE = "MESSAGE";

	private Context mContext;
	private ListView mListView;
	private TextView mListEmptyView;
	private TextView mPushListEmptyView;//BJH
	private SmsListAdapter mAdapter;
	private ImageButton mWriteMsg;

	private static final int DIAL070_UPDATE_START = 1;
	private static final int DIAL070_UPDATE = 2;
	private static final int DIAL070_UPDATE_END = 3;
	
	//BJH 2016.09.26
	private NotificationManager notificationManager;

	private EditText contacts_search;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DIAL070_UPDATE_START: {
				View emptyView = mListView.getEmptyView();
				if (emptyView != null)
					emptyView.setVisibility(View.GONE);
			}
				break;
			case DIAL070_UPDATE: {
				reloadListView();
			}
				break;
			case DIAL070_UPDATE_END: {
				if (mAdapter != null) {
					mAdapter.notifyDataSetChanged();
				}
			}
				break;

			}
		}
	};

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final ActionBar abar = getSupportActionBar();
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
				ActionBar.LayoutParams.WRAP_CONTENT,
				ActionBar.LayoutParams.MATCH_PARENT,
				Gravity.CENTER);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(true);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		mContext = this;

		setTitle(getString(R.string.message));

		setContentView(R.layout.sms_activity);

		contacts_search=findViewById(R.id.contacts_search);
		contacts_search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Sms.this,SmsSearch.class));
			}
		});

		mWriteMsg = (ImageButton) findViewById(R.id.btn_new_msg);
		mWriteMsg.setOnClickListener(new OnSingleClickListener() {
			@Override
			public void onSingleClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Sms.this, SmsWrite.class);
				startActivity(intent);
			}
		});

		mListView = (ListView) findViewById(R.id.sms_list);

		mListEmptyView = (TextView) findViewById(R.id.empty_result);
		mPushListEmptyView = (TextView) findViewById(R.id.push_empty_result);//BJH
		
		mListView.setEmptyView(mListEmptyView);

		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				SmsMsgListView sv = (SmsMsgListView) arg1;
				String number = null;
				String name = null;
				if (sv != null) {
					number = sv.getNumber();
					name = sv.getName();
				}

				Intent intent = new Intent(Sms.this, SmsMsgContsActivity.class);
				intent.putExtra("name", name);
				intent.putExtra("number", number);
				startActivity(intent);
			}
		});

		mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
				SmsMsgListView sv = (SmsMsgListView) arg1;
				String number = null;
				String name = null;
				if (sv != null) {
					number = sv.getNumber();
					name = sv.getName();
				}
				dialogSelect(0, name, number);
				return true;
			}
		});

		mAdapter = new SmsListAdapter(this);
		mListView.setAdapter(mAdapter);
		
		//BJH 2016.09.26
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mAdapter != null)
			mAdapter.destroyAdapter();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// 백버튼 비활성화
		DialMain.mainBackPressed(mContext);
		// super.onBackPressed();
	}

	private boolean setChangeCursor(int option) {
		if (mAdapter != null) {
			mAdapter.setQueryOption(option);
			mAdapter.loadSmsMsgData();
			mAdapter.notifyDataSetChanged();
		}
		return true;
	}

	private static final int MENU_OPTION_DELETE = Menu.FIRST + 2;
	private static final int MENU_OPTION_SORTING = Menu.FIRST + 3;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*if (menu != null) {
			menu.add(Menu.NONE, MENU_OPTION_SORTING, Menu.NONE,
					R.string.opt_menu_sorting);
			menu.add(Menu.NONE, MENU_OPTION_DELETE, Menu.NONE,
					R.string.opt_menu_delete);
		}*/
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		/*if (menu != null) {
			menu.clear();

			menu.add(Menu.NONE, MENU_OPTION_SORTING, Menu.NONE,
					R.string.opt_menu_sorting);
			menu.add(Menu.NONE, MENU_OPTION_DELETE, Menu.NONE,
					R.string.opt_menu_delete);

		}*/
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_OPTION_SORTING: {
			AlertDialog.Builder msgDialog = new AlertDialog.Builder(mContext);
			msgDialog.setTitle(R.string.opt_menu_display);
			msgDialog.setSingleChoiceItems(R.array.sms_select_options, -1, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					setChangeCursor(which);

					dialog.dismiss();
				}
			});
			msgDialog.show();
		}
			break;
		case MENU_OPTION_DELETE:
			Intent intent = new Intent(Sms.this, SmsEdit.class);
			startActivity(intent);
			break;
		default:
			break;
		}

		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//bindToService();
		reloadData();
		//BJH 2016.09.26
		cancelAllNoti();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unbindFromService();
		super.onPause();
	}

	private boolean bindToService() {
		registerReceiver(messageReceiver, new IntentFilter(MessageService.ACTION_NEW_MESSAGE));
		return true;
	}

	private void unbindFromService() {
		try {
			unregisterReceiver(messageReceiver);
		} catch (Exception e) {
			// That's the case if not registered (early quit)
		}
	}

	private BroadcastReceiver messageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.equals(MessageService.ACTION_NEW_MESSAGE)) {
				handler.sendMessage(handler.obtainMessage(DIAL070_UPDATE));
			}
		}
	};

	private void reloadData() {
		reloadListView();
		loadFromServer();
	}

	// private boolean isLoaded = false;
	private synchronized void reloadListView() {
		// CHECK MSG_TYPE
		//BJH 0707913 대역이 아니라도 푸시 메시지는 받을 수 있게 하기 위하여
		if (ServicePrefs.mUseMSG) {
			mListEmptyView.setText(getResources().getString(R.string.msg_empty));
			mListView.setEmptyView(mListEmptyView);
			if (ServicePrefs.DIAL_MSG_SEND)
				mWriteMsg.setVisibility(View.VISIBLE);
			else
				mWriteMsg.setVisibility(View.GONE);
		} else {
			mListEmptyView.setText(getResources().getString(R.string.msg_use_info));
			mListView.setEmptyView(mListEmptyView);
			mWriteMsg.setVisibility(View.GONE);
		}

		// if (mAdapter.isChanged() || !isLoaded)
		if (mAdapter != null) {
			// isLoaded = true;
			if (mAdapter.loadSmsMsgData()) {
				mAdapter.notifyDataSetChanged();
				//BJH
				if (!ServicePrefs.mUseMSG && mListView.getCount() > 0) {
					mPushListEmptyView.setVisibility(View.VISIBLE);
					mPushListEmptyView.setText(getResources().getString(R.string.msg_use_info));
					if(mListView.getCount() > 1)
						mPushListEmptyView.setVisibility(View.GONE);
				}else 
					mPushListEmptyView.setVisibility(View.GONE);
			}
		}
	}

	private void loadFromServer() {
		Intent intent = new Intent(Sms.this, MessageService.class);
		intent.setAction(MessageService.ACTION_START);
		startService(intent);
	}
	
	//BJH 메시지 길이 구하기
	public static int getRealSize(String text)  
	{  
	    int size = -1;  
	    try {  
	        size = text.getBytes("EUC-KR").length;  
	    } catch (UnsupportedEncodingException e1){  
	      
	    }  
	  
	    return size;  
	}  

	public static String postMessageData(Context context, String number, String msg, ArrayList<String> list) {
		AppPrefs mPrefs = new AppPrefs(context);
		String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MSG_SEND);

//		String query_url = url;
		String query_url = "https://if.maaltalk.com/maaltalk/req_send.php";
//		String query_url = "https://if.maaltalk.com/toast/rs.php";
//		String query_url = "211.253.24.194/maaltalk/req_send.php";
//		String query_url = "211.253.24.194/maaltalk/req_send.php";
		Log.d(THIS_FILE, "REQ_MESSAGE : " + query_url);

		String user_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
		String user_pwd = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);

		String key = user_id + user_pwd;
		Log.d(THIS_FILE, "SEND MSG KEY : " + key);

		String auth = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP);

		String file1 = "";
		String file2 = "";
		String file3 = "";
		int fileCount = 0;

		if (list != null && list.size() > 0) {
			int seq = 1;
			for (String fileName : list) {
				InputStream inputStream = null;
				try {
					inputStream = new FileInputStream(fileName);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					continue;
				}

				// You can get an inputStream using any IO API
				byte[] buffer = new byte[8192];
				int bytesRead;
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				try {
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						output.write(buffer, 0, bytesRead);
					}
				} catch (IOException e) {
					e.printStackTrace();
					try {
						inputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					continue;
				}

				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String data = Base64.encodeToString(output.toByteArray(), Base64.NO_WRAP);
				if (seq == 1) {
					file1 = data;
					fileCount++;
				} else if (seq == 2) {
					file2 = data;
					fileCount++;
				} else if (seq == 3) {
					file3 = data;
					fileCount++;
				}

				seq++;
			}
		}

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(context);

			// SET TIMEOUT
			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 15000);
			HttpConnectionParams.setSoTimeout(params, 15000);

			HttpPost httppost = new HttpPost(query_url);

			String type = "SMS";
			if (fileCount > 0)
				type = "MMS";
			else if (getRealSize(msg) > 90)
				type = "LMS";
			
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("param1", type));
			nameValuePairs.add(new BasicNameValuePair("param2", number));
			nameValuePairs.add(new BasicNameValuePair("param3", auth));
			nameValuePairs.add(new BasicNameValuePair("param4", msg));
			nameValuePairs.add(new BasicNameValuePair("param5", file1));
			nameValuePairs.add(new BasicNameValuePair("param6", file2));
			nameValuePairs.add(new BasicNameValuePair("param7", file3));

			DebugLog.d("SMS number --> "+number);
			DebugLog.d("SMS url    --> "+query_url);
			DebugLog.d("SMS url param 1: "+type);
			DebugLog.d("SMS url param 2: "+number);
			DebugLog.d("SMS url param 3: "+auth);
			DebugLog.d("SMS url param 4: "+msg);


			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			Log.d(THIS_FILE, "HTTPS POST EXEC OK");
			String line = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			return sb.toString();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}

	// logout을 하지 않고 수행함.
	public static int sendMessageToServer(Context context, String number, String msg, ArrayList<String> list) {
		String str = postMessageData(context, number, msg, list);
		if (str == null || str.length() == 0) {
			Log.e(THIS_FILE, "NOT RESPONSE");
			return -99; // for retry
		}
		Log.d(THIS_FILE, "SEND MSG : " + str);

		try {
			JSONObject jObject = new JSONObject(str);
			JSONObject responseObject = jObject.getJSONObject("RESPONSE");
			Log.d("RES_CODE", responseObject.getString("RES_CODE"));
			Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
			Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

			if (responseObject.getInt("RES_CODE") == 0) {
				// 성공

			} else {
				Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="
						+ responseObject.getString("RES_DESC"));
				String res_str = responseObject.getString("RES_DESC");//BJH 오류 코드를 사용자한테 알리기 위해서
				if(res_str.equals("DID"))
					return -2;
				else if(res_str.equals("BALANCE"))
					return -3;
				else if(res_str.equals("EXPIRED"))
					return -4;
				else if(res_str.equals("VERIFIED"))
					return -5;
				else if(res_str.equals("STATUS"))
					return -6;
				else 
					return -1;
			}

		} catch (Exception e) {
			Log.e(THIS_FILE, "JSON", e);
			return -1;
		}

		return 0;
	}

	private void dialogSelect(final int id, String name, final String number){
		final String user_phone = number;
		final String user_name = name;

		ArrayList<String> listMenu = new ArrayList<>();
		listMenu.add(getString(R.string.opt_menu_delete));

		final String[] menus = listMenu.toArray(new String[listMenu.size()]);

		String title;
		if(name != null && name.length() > 16) title = name.substring(0,16);

		if (name != null && name.length() > 0)
			title=name;
		else
			title=number;

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle(title);
		adb.setItems(menus, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (menus[which].equals(getString(R.string.opt_menu_delete))){
					if (mAdapter != null)
					{
						mAdapter.delete(number);
						mAdapter.loadSmsMsgData();
						mAdapter.notifyDataSetChanged();
						//BJH 메시지 삭제 후 layout 반영
						reloadData();
					}
					dialog.cancel();
				}
			}
		});

		adb.show();
	}
	
	//BJH 메시지 전송이 되지 않는 부분을 알려주는 Alert창
	public static void AlertFail(Context context, String str) {
		/*AlertDialog.Builder msgDialog = new AlertDialog.Builder(context);
		msgDialog.setTitle(context.getResources().getString(R.string.app_name));
		if(str.equals("did"))
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail_did));
		else if(str.equals("balance"))
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail_balance));
		else if(str.equals("expired"))
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail_expired));
		else if(str.equals("verified"))
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail_verified));
		else if(str.equals("status"))
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail_status));
		else
			msgDialog.setMessage(context.getResources().getString(R.string.msg_fail));
		msgDialog.setIcon(R.drawable.ic_launcher);
		msgDialog.setNegativeButton(context.getResources().getString(R.string.close), null);
		msgDialog.show();*/

		String msg;

		if(str.equals("did"))
			msg=context.getResources().getString(R.string.msg_fail_did);
		else if(str.equals("balance"))
			msg=context.getResources().getString(R.string.msg_fail_balance);
		else if(str.equals("expired"))
			msg=context.getResources().getString(R.string.msg_fail_expired);
		else if(str.equals("verified"))
			msg=context.getResources().getString(R.string.msg_fail_verified);
		else if(str.equals("status"))
			msg=context.getResources().getString(R.string.msg_fail_status);
		else
			msg=context.getResources().getString(R.string.msg_fail);

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(context.getResources().getString(
				R.string.app_name));
		builder.setMessage(msg);
		builder.setPositiveButton(
				context.getResources().getString(R.string.close),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0,
										int arg1) {

					}
				});
		builder.create();
		builder.show();
	}
	
	public static boolean deleteItem(Context context, Integer mid)
	{
		DBManager db = new DBManager(context);
		db.open();
		if(mid != 0 && mid > 0)
			db.deleteSmsMsg(mid);
		db.close();
		
		return true;		
	}
	
	public static void updateDeleteList(Context context, String number)
	{
		DBManager db = new DBManager(context);
		db.open();
		Cursor c = db.getLastSmsMsg(number);
		if(c == null)
		{
			return;
		}
		
		if(!c.moveToFirst())
		{
			db.deleteSmsList(number);
			c.close();
			return;
		}
		
    	String display_name = ContactHelper.getContactsNameByPhoneNumber(context, number);
    	if (display_name == null) display_name = "";

		String text = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
		Integer msgType = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_MSG_TYPE));
		db.updateMsgSmsList(number, display_name, text, msgType, 0);
		//long time = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));
		//db.updateMsgSmsList(number, display_name, text, msgType, 0, time);
		
		c.close();
	
	}
	
	//BJH 2016.09.26
	public synchronized void cancelMsg() {
		notificationManager.cancel(Fcm.FCM_NOTIF_MSG);
	}
	
	public synchronized void cancelVMS() {
		notificationManager.cancel(Fcm.FCM_NOTIF_VMS);
	}
	
	public synchronized void cancelPush() { //BJH 2016.10.10
		notificationManager.cancel(Fcm.FCM_NOTIF_NOTICE);
	}
	
	private void cancelAllNoti() {
		cancelMsg();
		cancelVMS();
		cancelPush();
	}

}
