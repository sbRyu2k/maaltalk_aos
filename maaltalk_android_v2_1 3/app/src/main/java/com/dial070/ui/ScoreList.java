package com.dial070.ui;


import android.app.Activity;
//import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import com.dial070.maaltalk.R;

public class ScoreList extends Activity {

	private ScoreListAdapter mAdapter;
//	private Context mContext;
	private ListView mListView;
	
	public ScoreList() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.score_activity);
		
//		mContext = this;

		mListView = (ListView)findViewById(R.id.score_list_view);
		
		mAdapter = new ScoreListAdapter(this);
		mListView.setAdapter(mAdapter);
		
		mAdapter.buildScoreData();
		
	}

}
