package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.EnglishUtils;
import com.dial070.utils.HangulUtils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class CountrySearchAdapter extends BaseAdapter {
	private Context mContext;

	private List<CountryItem> mMatchItem = new ArrayList<CountryItem>();
	private static Map<String, ArrayList<CountryItem>> mCountryItemList;
	private AppPrefs mPrefs;
	private String mLocale;
	
	public CountrySearchAdapter(Context context)
	{
		mContext = context;
		mPrefs = new AppPrefs(mContext);
		mLocale = mPrefs.getPreferenceStringValue(AppPrefs.LOCALE);
		mCountryItemList = null;
	}
		
	public void setData(Map<String, ArrayList<CountryItem>> data)
	{
		if(data == null) return;
		
		mCountryItemList = data;
	}	
	
	private boolean isNumberString(String str)
	{
		boolean rs = true;
		for(int i=0; i < str.length(); i++)
		{
			if('0' > str.charAt(i) || '9' < str.charAt(i))
			{
				rs = false;
				break;
			}
		}
		return rs;
	}
	
	//검색키에 해당하는 리스트 아이템을 개수를 구함 
	public List<CountryItem> getSearchItem(String searchKey)
	{
		//초기화 
		mMatchItem.clear();

		String findName;
//		int position = 0;
		CountryItem data = null;
		ArrayList<CountryItem> item = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{
				if(i == mCountryItemList.size()-1)
					item = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					item = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			else
			{
				if(i == mCountryItemList.size()-1)
					item = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					item = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);				
			}
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(item != null && item.size() > 0)
			{		
				if(isNumberString(searchKey))
				{
					for(int j=0; j < item.size(); j++)
					{
						data = item.get(j);
						
						if(data.Code.startsWith("+" + searchKey))
						{ 
							mMatchItem.add(new CountryItem(data.NameKor,data.NameEng,data.Code,data.Gmt,data.Flag,data.FreeMobile,data.FreeWired,mLocale));
						}	
					}
				}
				else
				{
					
					if(mLocale.equalsIgnoreCase("한국어"))
					{
						for(int j=0; j < item.size(); j++)
						{
							data = item.get(j);
							findName = HangulUtils.getHangulInitialSound(data.NameKor, searchKey);
							if(findName.indexOf(searchKey) >= 0)
							{ 
								mMatchItem.add(new CountryItem(data.NameKor,data.NameEng,data.Code,data.Gmt,data.Flag,data.FreeMobile,data.FreeWired,mLocale));
							}	
						}
					}
					else
					{
						for(int j=0; j < item.size(); j++)
						{
							data = item.get(j);
							findName = data.NameEng.toUpperCase(Locale.getDefault());
							if(findName.indexOf(searchKey.toUpperCase(Locale.getDefault())) >= 0)
							{ 
								mMatchItem.add(new CountryItem(data.NameKor,data.NameEng,data.Code,data.Gmt,data.Flag,data.FreeMobile,data.FreeWired,mLocale));
							}	
						}
					}					
				}
			}

		}
		return mMatchItem;
	}	
		
	
	private CountryItem read(int offset)
	{
		if (offset < 0 || offset >= mMatchItem.size()) return null;
			
		return mMatchItem.get(offset);
	}	
	
	//@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mMatchItem.size();
	}

	//@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return read(position);
	}

	//@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CountryListView countryView;
		if(convertView == null)
		{
			countryView = new CountryListView(mContext, read(position));
		}
		else
		{
			countryView = (CountryListView) convertView;

			CountryItem item = read(position);
			if(item != null)
			{
				countryView.setFlagIcon(mContext.getResources().getIdentifier(item.Flag, "drawable", mContext.getPackageName()));
				countryView.setText(0, item.getName());
				countryView.setText(1, item.Code);	
				countryView.setGmtTime(item.Gmt);
			}
		}
	
		if( (position % 2) == 0)
		{
			countryView.setBackgroundResource(R.drawable.list_item_even_background);
			
		}
		else
		{
			countryView.setBackgroundResource(R.drawable.list_item_odd_background);
		}			
		
		return countryView;
	}

}
