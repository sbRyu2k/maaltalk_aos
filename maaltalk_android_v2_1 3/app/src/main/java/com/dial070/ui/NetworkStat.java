package com.dial070.ui;

import java.util.Timer;
import java.util.TimerTask;


import com.dial070.maaltalk.R;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipProfileState;

import com.dial070.sip.service.SipService;
import com.dial070.utils.Log;

import android.app.Activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

public class NetworkStat extends Activity {
	
	private static String THIS_FILE = "NETWORK STAT";
	private Timer connectTimer = null;
	
	private ImageView callUserAnt;
	private TextView mTextStat;

	public NetworkStat() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.network_stat);
		
		callUserAnt = (ImageView) findViewById(R.id.call_user_ant);
		mTextStat = (TextView) findViewById(R.id.txt_stat);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		timerStop();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unbindFromService();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		bindToService();
		super.onResume();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
	    finish();
	    return super.dispatchTouchEvent(ev);
	}
	
	private synchronized void timerStart()
	{
		if (connectTimer != null) return;
		try
		{
			connectTimer = new Timer();
			TimerTask task = new TimerTask() {
	            @Override
	            public void run() {
	            	// UPDATE
	            	if (handler != null)
	            		handler.sendMessage(handler.obtainMessage(UPDATE_FROM_TIMER));
	            }
	        };				
			connectTimer.schedule(task, 100, 1000);
		}
		catch (Exception e)
		{
		}
	}
	
	private synchronized void timerStop()
	{
		if (connectTimer == null) return;
		connectTimer.cancel();
		connectTimer.purge();
		connectTimer = null;
	}
	
	private static final int UPDATE_FROM_TIMER = 1;
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case UPDATE_FROM_TIMER:
					updateUIFromTimer();
					break;
				default:
					super.handleMessage(msg);
			}
		}
	};	
	
	private boolean checkOnline()
	{
		if (mSipService == null) return false;		
		SipProfileState accountInfo = null;
		try
		{
			accountInfo = mSipService.getSipProfileState(0);
		}
		catch (RemoteException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return (accountInfo != null && accountInfo.isValidForCall()); 				
	}	
	
	private void updateUIFromTimer()
	{
		if (checkOnline())
		{
			int RTT = SipService.RTT;		
			
			Log.d(THIS_FILE, "RTT : " + RTT);
			
			int antena = R.drawable.img_call_0;
			String msg = getResources().getString(R.string.net_stat_offline);
			
	        if (RTT < 100)
	        {
	            antena = R.drawable.img_call_4;
	            msg = getResources().getString(R.string.net_stat_4);
	        }
	        else if (RTT < 150)
	        {
	            antena = R.drawable.img_call_3;
	            msg = getResources().getString(R.string.net_stat_3);
	        }
	        else if (RTT < 200)
	        {
	            antena = R.drawable.img_call_2;
	            msg = getResources().getString(R.string.net_stat_2);
	        }
	        else
	        {
	            antena = R.drawable.img_call_1;
	            msg = getResources().getString(R.string.net_stat_1);
	        }
			
	        callUserAnt.setBackgroundResource(antena);
			mTextStat.setText(msg + " (" + RTT + "ms)");
		}
		else
		{
	        callUserAnt.setBackgroundResource(R.drawable.img_call_0);
			mTextStat.setText(getResources().getString(R.string.net_stat_offline));
		}
	}	
	
	private boolean bindToService()
	{
		boolean bound = bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);
		return bound;
	}
	
	private void unbindFromService()
	{
		timerStop();
		try
		{
			unbindService(mSipConnection);
		}
		catch (Exception e)
		{
		}
		mSipService = null;
	}	
	
	/**
	 * Service binding
	 */
	private ISipService mSipService = null;
	private ServiceConnection mSipConnection = new ServiceConnection()
	{
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1)
		{
			mSipService = ISipService.Stub.asInterface(arg1);
			timerStart();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0)
		{
			timerStop();
			mSipService = null;
		}
	};	
	

}
