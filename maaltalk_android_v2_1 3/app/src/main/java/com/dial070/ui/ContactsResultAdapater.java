package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ContactsResultAdapater extends BaseAdapter {

	private Context mContext;
	private List<ContactsData> mResultData; 	
	
	public ContactsResultAdapater(Context context)
	{
		mContext = context;
		mResultData = new ArrayList<ContactsData>();
	}		
	
	public void setData(ArrayList<ContactsData> data)
	{
		mResultData = data;
	}	
	
	private ContactsData read(int offset)
	{
		if(mResultData == null || mResultData.size() == 0) return null;
		
		if (offset < 0 || offset >= mResultData.size()) return null;
			
		return mResultData.get(offset);
	}		
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mResultData == null) return 0;
		return mResultData.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return read(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if(mResultData == null) return null;
		
		ContactsListView contactView = null;
		ContactsData item = read(arg0);
		if(item != null)
		{	
			if(arg1 == null)
			{
				contactView = new ContactsListView(mContext, item);
			}
			else
			{
				contactView = (ContactsListView) arg1;
			}
			
			if(item != null)
			{
				contactView.setData(item.getDisplayName(), item.getPhoneNumber(), item.getContactId(), null);
				contactView.setItem(item);
			}
			
		}
		return contactView;
	}

}
