package com.dial070.ui;

import com.dial070.db.*;
import com.dial070.maaltalk.R;
import com.dial070.utils.ContactHelper;

import android.content.*;
import android.database.*;
import android.telephony.*;
import android.view.*;
import android.widget.*;

public class FavoritesListAdapter extends BaseAdapter {

	static String THIS_FILE = "Favorites List Adpater";
	private static final int FAVORITES_DISPLAY_COUNT = 3 * 8;
//	private static final int FAVORITES_AUTO_DISPLAY_COUNT = 20;
//	private static final int FAVORITES_MANUAL_DISPLAY_COUNT = 4;
	private static final int FAVORITE_VIEW_MODE = 0;
//	private static final int FAVORITE_EDIT_MODE = 1;
	// private int mFavoritesPages;
	private Context mContext;
	//private Cursor mCursor;
	private int mFavoriteCount = 0;
	
	private DBManager mDatabase;
	private int mEditMode;

//	private FavoriteData[] mFavoritesTemp;
	private FavoriteData[] mFavoritesItems;

	public FavoritesListAdapter(Context context)
	{
		mContext = context;

		// mFavoritesPages = 1;
		mEditMode = FAVORITE_VIEW_MODE;
		//mCursor = null;
		mFavoritesItems = null;
//		mFavoritesTemp = null;

		// 즐겨찾기 데이타
		mFavoritesItems = new FavoriteData[FAVORITES_DISPLAY_COUNT];
//		mFavoritesTemp = new FavoriteData[FAVORITES_DISPLAY_COUNT];

		// Db
		mDatabase = new DBManager(mContext);

		buildFavoritesData();
	}

	public void destroyAdapter()
	{
		if (mDatabase != null)
		{
			mDatabase.close();
			mDatabase = null;
		}
	}

	public void buildFavoritesData()
	{
		if (mDatabase == null) return;

		if (!mDatabase.isOpen()) mDatabase.open();

//		// 자동 즐겨찾기 조회
//		if (mCursor != null) mCursor.close();
//		mCursor = mDatabase.getAutoFavorites();
//		
//		int offset = 0;
//		if (mCursor != null)
//		{
//			int id = 0;
//			String uid = null;
//			String name = null;
//			String phone = null;
//			String photo = null;
//			String type = null;
//			String memo = null;
//
//			
//			if (mCursor.moveToFirst())
//			{
//				do
//				{
//					id = mCursor.getInt(mCursor.getColumnIndex(FavoritesData.FIELD_PK));
//					name = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_DISPLAYNAME));
//					phone = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_PHONENUMBER));
//					uid = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_UID));
//					photo = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_IDENTIFIER));
//					type = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_FIELD1));
//					memo = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_FIELD2));
//					mFavoritesItems[offset] = new FavoriteData(id,name, phone, uid, photo, type,memo);
//
//					offset++;
//				}
//				while (mCursor.moveToNext() && (offset < FAVORITES_AUTO_DISPLAY_COUNT) );
//			}
//		}
//		
//		//수돟 등록 즐겨찾기 
//		if (mCursor != null) mCursor.close();
//		mCursor = mDatabase.getManualFavorites();
//		
//		if (mCursor != null)
//		{
//			int id = 0;
//			String uid = null;
//			String name = null;
//			String phone = null;
//			String photo = null;
//			String type = null;
//			String memo = null;
//
//			
//			if (mCursor.moveToFirst())
//			{
//				do
//				{
//					id = mCursor.getInt(mCursor.getColumnIndex(FavoritesData.FIELD_PK));
//					name = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_DISPLAYNAME));
//					phone = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_PHONENUMBER));
//					uid = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_UID));
//					photo = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_IDENTIFIER));
//					type = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_FIELD1));
//					memo = mCursor.getString(mCursor.getColumnIndex(FavoritesData.FIELD_FIELD2));
//					mFavoritesItems[offset] = new FavoriteData(id,name, phone, uid, photo, type,memo);
//
//					offset++;
//				}
//				while (mCursor.moveToNext() && (offset < FAVORITES_MANUAL_DISPLAY_COUNT) );
//			}
//		}	
		
		// 점수기반 즐겨찾기 조회

		Cursor c = mDatabase.getScoredFavorites();
		//mCursor = mDatabase.getAllFavorites();
		
		int offset = 0;
		if (c != null)
		{
			int id = 0;
			String uid = null;
			String name = null;
			String phone = null;
			String photo = null;
			String type = null;
			String memo = null;
			String contact_name = null;

			
			if (c.moveToFirst())
			{
				do
				{
					id = c.getInt(c.getColumnIndex(FavoritesData.FIELD_PK));
					
					name = c.getString(c.getColumnIndex(FavoritesData.FIELD_DISPLAYNAME));
					
					phone = c.getString(c.getColumnIndex(FavoritesData.FIELD_PHONENUMBER));
					
					contact_name = ContactHelper.getContactsNameByPhoneNumber(mContext,phone);
					if(contact_name != null)
					{
						// fix
						{
							String _num = contact_name.replaceAll("\\-", "");
							if (_num.equals(phone))
							{
								contact_name = phone;
							}
						}						
						name = contact_name;
					}
					
					uid = c.getString(c.getColumnIndex(FavoritesData.FIELD_UID));
					photo = c.getString(c.getColumnIndex(FavoritesData.FIELD_IDENTIFIER));
					type = c.getString(c.getColumnIndex(FavoritesData.FIELD_FIELD1));
					memo = c.getString(c.getColumnIndex(FavoritesData.FIELD_FIELD2));
					mFavoritesItems[offset] = new FavoriteData(id,name, phone, uid, photo, type,memo);

					offset++;
				}
				while (c.moveToNext() && (offset < FAVORITES_DISPLAY_COUNT) );
			}
			c.close();
		}
		
		//For New Add
		if(offset == FAVORITES_DISPLAY_COUNT)
		{
			//마지막 데이타는 신규 추가로 변경
			mFavoritesItems[offset-1].setId(0);
			mFavoritesItems[offset-1].setDisplayName("new_add");
			mFavoritesItems[offset-1].setPhoneNumber("");
			mFavoritesItems[offset-1].setContactId("");
			mFavoritesItems[offset-1].setPhotoId("");
			mFavoritesItems[offset-1].setRegisterType("MANUAL");
			mFavoritesItems[offset-1].setMemo(mContext.getResources().getString(R.string.memo_hint));
			mFavoriteCount = offset;
			
		}
		else if (offset < FAVORITES_DISPLAY_COUNT)
		{
			mFavoritesItems[offset] = new FavoriteData(0,"new_add", "", "", "", "MANUAL","메모를 입력해주세요.");		
			mFavoriteCount = offset + 1;
		}

	}

	public void setAdpaterMode(int mode)
	{
		mEditMode = mode;
	}

	public static String fullNumber(String number, boolean localBypass)
	{
		if (number == null || number.length() == 0) return "";

		number = PhoneNumberUtils.stripSeparators(number);

		// 최소 5자리는 되어야 한다.
		if (number.length() <= 5) return number;

		String n1 = number.substring(0, 1);

		if (n1.equals("+")) return number;
		if (n1.equals("0"))
		{
			// 01로 시작
			String n2 = number.substring(1, 2);
			if (n2.equals("0"))
			{
				// 3자리 번호 : 001, 002, 005, 006, 008
				String n3 = number.substring(2, 3);
				int num;
				try
				{
					num = Integer.parseInt(n3);
					if (num == 1 || num == 2 || num == 5 || num == 6 || num == 8) return "+" + number.substring(3, number.length());
				}
				catch (NumberFormatException e)
				{
					return number;
				}

				// 5자리 번호 : 00300, 00700, 00770, 00777, 00365, 00321, 00755,
				// 00707, 00765, 00766, 00782
				String etc = number.substring(2, 5);
				try
				{
					num = Integer.parseInt(etc);
					if (num == 300 || num == 700 || num == 770 || num == 777 || num == 365 || num == 321 || num == 755 || num == 707 || num == 765 || num == 766 || num == 782) { return "+" + number.substring(5, number.length()); }
				}
				catch (NumberFormatException e)
				{
					return number;
				}
			}
			else
			{
				if (localBypass) return number;
				else return "+82" + number.substring(1, number.length());
			}			
		}
		else
		{
			return "+" + number;			
		}

		return number;
	}	
	
	public boolean insertFavorite(int density, String displayname, String phone, String id, String indentifier, String searhNumber)
	{
		// indentifier 필드에 포토 아이디를 저장함..
		if (mDatabase == null) return false;

		String phone_number;// = fullNumber(searhNumber, false);
		phone_number = searhNumber.replaceAll("\\+82", "0");
		phone_number = phone_number.replaceAll("\\-", "");
		
		FavoritesData favorite = new FavoritesData(displayname, phone, id, indentifier, phone_number, "MANUAL", "",density);

		if (mDatabase.insertFavorites(favorite) < 0) return false;

		return true;
	}

//	private int getFid(ContactsData item)
//	{
//		int rs = -1;
//
//		if (item.getContactId() == null) return rs;
//
//		for (int i = 0; i < FAVORITES_DISPLAY_COUNT; i++)
//		{
//			if (mFavoritesItems[i] == null) continue;
//
//			String uid = mFavoritesItems[i].getContactId();
//			if (uid == null) continue;
//			if (mFavoritesItems[i].getContactId().equalsIgnoreCase(item.getContactId())) return i;
//		}
//
//		return rs;
//	}

	public boolean deleteFavorites(FavoriteData item)
	{
		if (mDatabase == null) return false;
		
		int fid = item.getId();

		if (!mDatabase.deleteFavorite(fid)) return false;

		//Score에서 점수 삭제
		mDatabase.updateScoreFavorite(item.getPhoneNumber(), 0, 0);
		
		return true;
	}

	private FavoriteData read(int offset)
	{
		if (offset < 0 || offset >= FAVORITES_DISPLAY_COUNT) return null;

		return mFavoritesItems[offset];
	}

	//@Override
	public int getCount()
	{
		return mFavoriteCount;
	}

	//@Override
	public Object getItem(int position)
	{
		return read(position);
	}

	//@Override
	public long getItemId(int position)
	{
		return position;
	}

	//@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		FavoritesListView favoritesView;
		FavoriteData item = read(position);
		//if(item == null) return null;
		
		if (convertView == null)
		{
			favoritesView = new FavoritesListView(mContext, read(position));
		}
		else
		{
			favoritesView = (FavoritesListView) convertView;
		}
		favoritesView.setData(item, mEditMode);


		return favoritesView;
	}

}
