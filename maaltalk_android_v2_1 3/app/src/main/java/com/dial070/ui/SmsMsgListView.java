package com.dial070.ui;

import com.dial070.maaltalk.R;

import com.dial070.db.SmsListData;
import com.dial070.utils.CircleImageView;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.Log;
import com.dial070.utils.PhoneNumberTextWatcher;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SmsMsgListView extends LinearLayout {
//	private int	mId;
	private String mName;
	private String mNumber;
	private CircleImageView mUserView;
	private ImageView mIconView;
	private TextView mPhoneNumber;
	private TextView mDateView;
	private TextView mMsgView;
	private TextView mNewCountView;
	private CheckBox mSelectView;
	
	private Context mContext;
	
	PhoneNumberTextWatcher digitFormater;
	
	public SmsMsgListView(Context context) {
		
		super(context);
		
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.sms_msg_item, this, true);
		
		// internal.
//		mId = 0;
		mNumber = "";
		mName = "";
		
		mUserView = findViewById(R.id.sms_user_photo);
		//mUserView.setVisibility(View.GONE);
		
		
		mMsgView = (TextView) findViewById(R.id.sms_message);
		mPhoneNumber = (TextView) findViewById(R.id.sms_phone_number);
		mIconView = (ImageView) findViewById(R.id.sms_msg_type);
		mDateView = (TextView) findViewById(R.id.sms_date);		
		mNewCountView = (TextView) findViewById(R.id.sms_new_count);
		mSelectView = (CheckBox) findViewById(R.id.cb_select_item);	
		
		digitFormater = new PhoneNumberTextWatcher();
		mPhoneNumber.addTextChangedListener(digitFormater);	
	}
	
	
	public void setView(boolean mode, boolean selected)
	{
		if(mode)
		{
			mSelectView.setVisibility(View.VISIBLE);
			mSelectView.setChecked(selected);
		}
		else mSelectView.setVisibility(View.GONE);
	}
	
	public void setItemChecked(boolean selected)
	{
		mSelectView.setChecked(selected);
	}
	
	//smsListlView.setData(number, sms_type, last_time, last_msg, sms_new);
	public void setData(String number, int sms_type, long last_time, String last_msg, int sms_new)
	{
		mNumber = number;
		Log.i("SmsMsgListView","mNumber:"+mNumber);

		String name="";
		String sPhoto = "";
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, mNumber);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				name = c.getString(c.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
				sPhoto = c.getString(c.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI));
			}
			c.close();
		}


		Log.i("SmsMsgListView","sPhoto:"+sPhoto);

		Drawable photo = getResources().getDrawable(R.drawable.ic_contact_default_photo);

		if(sPhoto != null && sPhoto.trim().length()>0) {
			mUserView.setImageURI(Uri.parse(sPhoto));
		} else {
			mUserView.setImageDrawable(photo);
		}



		
		//String name = ContactHelper.getContactsNameByPhoneNumber(mContext, number);
		mName = name;
		if(name !=null && name.length() > 0)
		{
			mPhoneNumber.removeTextChangedListener(digitFormater);
			//mPhoneNumber.setText(name.concat(" ("+number+")"));//메시지함에서 이름 과 전화번호가 보이도록
			mPhoneNumber.setText(name);//메시지함에서 이름 과 전화번호가 보이도록
		}
		else
		{
			mPhoneNumber.addTextChangedListener(digitFormater);		
			mPhoneNumber.setText(number);
		}
		
		// Set the date/time field by mixing relative and absolute times.
//		int flags = DateUtils.FORMAT_ABBREV_RELATIVE;

        CharSequence dateClause = DateUtils.formatDateRange(mContext, last_time, last_time, DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME  | DateUtils.FORMAT_24HOUR);
        mDateView.setText(dateClause);
		
        // Receive msg
        if (sms_type == SmsListData.RECEIVE_TYPE) {
        	mIconView.setImageResource(R.drawable.inbound);
        }		
        else
        {
        	mIconView.setImageResource(R.drawable.outbound);
        }
        mMsgView.setText(last_msg);
        
        if (sms_new > 0)
        {
        	//mNewCountView.setText(String.format("(새 메시지 : %d)", sms_new));
        	mNewCountView.setText(String.valueOf(sms_new));
        	mNewCountView.setVisibility(View.VISIBLE);
        }
        else
        {
        	mNewCountView.setText("");
        	mNewCountView.setVisibility(View.GONE);
        }

	}
	
//    private String formatDuration(long elapsedSeconds) {
//        long minutes = 0;
//        long seconds = 0;
//
//        if (elapsedSeconds >= 60) {
//            minutes = elapsedSeconds / 60;
//            elapsedSeconds -= minutes * 60;
//        }
//        seconds = elapsedSeconds;
//
//        return mContext.getString(R.string.callDetailsDurationFormat, minutes, seconds);
//    }	
	
	public String getNumber() {
		return mNumber;
	}
	
	public String getName() {
		return mName;
	}

    
}
