package com.dial070.ui;

import java.util.ArrayList;

import com.dial070.maaltalk.R;
import com.dial070.utils.Log;
import com.dial070.db.DBManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

public class SmsEdit extends Activity {
//	private static final String THIS_FILE = "SMS DELETE LIST";	

	private Context mContext;

	private LinearLayout mSelectAllItem;
	private CheckBox mCheckBoxAllItem;
	private ListView mSmsListView;
	private Button mCancel;
	private Button mDelete;
	
	SmsListAdapter mAdapter;
	
	private ProgressDialog mProgressDialog;
	
	private boolean mAllItemChecked;
//	private boolean mItemChecked;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		setContentView(R.layout.sms_edit_activity);
		
		mAllItemChecked = false;
//		mItemChecked=false;
		
		
		mDelete = (Button) findViewById(R.id.btn_sms_edit_delete);
		mDelete.setEnabled(false);
		mDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//선택한 항목이 삭제됩니다.
				Resources res = mContext.getResources();
				AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
				msgDialog.setTitle(R.string.delete_sms);
				msgDialog.setMessage(res.getString(R.string.delete_question));
				msgDialog.setButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteSmsList();
						return;
					}
				});
				msgDialog.setButton2(res.getString(R.string.no), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});		
				msgDialog.show();
			}
		});
				
	
		mCancel = (Button) findViewById(R.id.btn_sms_edit_cancel);
		mCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		mSmsListView = (ListView) findViewById(R.id.sms_edit_list);
		mSmsListView.setClickable(true);
		mSmsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			//arg2-position arg3-id
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
								
				mCheckBoxAllItem.setChecked(false);
				
				SmsMsgListView view = (SmsMsgListView) arg1;
				CheckBox chk_select = (CheckBox)view.findViewById(R.id.cb_select_item);
				//UI 변경 
				chk_select.setChecked(!chk_select.isChecked());
				//데이타 변경 
				mAdapter.setCheckedItem(view.getNumber(), chk_select.isChecked());	
				
				if(mAdapter.getCheckedCount() > 0) mDelete.setEnabled(true);
				else mDelete.setEnabled(false);
				
				if(mAdapter.getCheckedCount() == mAdapter.getCount()) mCheckBoxAllItem.setChecked(true);
			}

		});		
		
		
		mSelectAllItem = (LinearLayout) findViewById(R.id.allitem);
		mCheckBoxAllItem = (CheckBox) findViewById(R.id.check_all_item);
		mCheckBoxAllItem.setClickable(false);
		
		mSelectAllItem.setClickable(true);
		mSelectAllItem.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCheckBoxAllItem.setChecked(!mCheckBoxAllItem.isChecked());		
				
				mAllItemChecked = mCheckBoxAllItem.isChecked();
	
				mDelete.setEnabled(mAllItemChecked);
				
				mAdapter.setCheckedAllItem(mAllItemChecked);
				
				mAdapter.notifyDataSetChanged();
			}
		});
		

		mAdapter = new SmsListAdapter(this);
		mAdapter.loadSmsMsgData();
		mAdapter.setDeleteMode(true);
		mSmsListView.setAdapter(mAdapter);		
	}
	private Handler handler = new Handler();
	
	private void deleteSmsList()
	{
		Resources res = mContext.getResources();
		String title = res.getString(R.string.delete_select_item);
		String description = res.getString(R.string.delete_select_item_desc);
		
		mProgressDialog = ProgressDialog.show(this, title, description,false);
		
		// DELETE RECENT CALLS
		Thread t = new Thread()
		{
			public void run()
			{
				deleteSelectItems();
				handler.post(mDeleteResults);
			};
		};
		t.start();				
		

	}
	
	final Runnable mDeleteResults = new Runnable()
	{
		public void run()
		{
			if (mProgressDialog != null && mProgressDialog.isShowing())
				mProgressDialog.dismiss();
			finish();
		}
	};	
		
	
	private boolean deleteSelectItems()
	{
		DBManager db = new DBManager(mContext);
		db.open();
		
		if(mCheckBoxAllItem.isChecked()) 
		{
			if(db.deleteAllSmsMsg())
				db.deleteAllSmsList(); //전체 아이템 삭제
		}
		else
		{
			ArrayList<String> list = mAdapter.getCheckedList();
			for(int i=0; i < list.size(); i++)
			{
				String phone_number = list.get(i);	
				if(db.deleteSmsMsg(phone_number))
					db.deleteSmsList(phone_number);
			}
			mAdapter.checkClear();
		}
		db.close();
		
		return true;		
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mAdapter.destroyAdapter();
	}
	
}
