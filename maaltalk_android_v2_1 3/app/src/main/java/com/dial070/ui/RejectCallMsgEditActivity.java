package com.dial070.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

public class RejectCallMsgEditActivity extends Activity {
    private AppPrefs mPrefs;
    private EditText edtMsg;
    private ArrayList arrayListMsg;
    private androidx.appcompat.app.AlertDialog sweetAlertDialog;
    private TextView txtLimit;
    private final static String THIS_FILE="RejectCallMsgEditActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_call_msg_edit);

        mPrefs = new AppPrefs(this);

        Intent intent=getIntent();
        final int index=intent.getIntExtra("index",0);
        arrayListMsg=intent.getStringArrayListExtra("list");

        TextView txtClose=findViewById(R.id.btnClose);
        TextView txtSave=findViewById(R.id.btnSave);
        edtMsg=findViewById(R.id.edtMsg);
        txtLimit=findViewById(R.id.txtLimit);

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RejectCallMsgEditActivity.this.finish();
            }
        });
        txtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMsg.getText().toString().trim().length()>0){
                    arrayListMsg.set(index,edtMsg.getText().toString());

                    JSONObject wrapObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray(arrayListMsg);
                    try {
                        wrapObject.put("list", jsonArray);
                        mPrefs.setPreferenceStringValue(mPrefs.REJECT_CALL_MSG,wrapObject.toString());
                    }catch (JSONException e){
                        Log.d(THIS_FILE,"JSONException:"+e.getMessage());
                    }

                    RejectCallMsgEditActivity.this.finish();
                }
            }
        });

        edtMsg.setText(arrayListMsg.get(index).toString());
        int msg_len = 0;
        try {
            msg_len = arrayListMsg.get(index).toString().getBytes("KSC5601").length;
        } catch (UnsupportedEncodingException e) {
            //e.printStackTrace();
        }
        int remain_byte = 140 - (msg_len);
        String msg_length = String.format(Locale.getDefault(),"(%d/%d)" , remain_byte, 140);
        txtLimit.setText(msg_length);

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String msg = s.toString();
                int msg_len = 0;
                String msg_length = "";
                int remain_byte=0;
                try {
                    msg_len = msg.getBytes("KSC5601").length;
                    if (msg_len > 140){
                        msg_len =140;
                        s.delete(s.length()-2, s.length()-1);
                        if (sweetAlertDialog==null){
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(RejectCallMsgEditActivity.this);
                            builder.setTitle(getResources().getString(R.string.app_name));
                            builder.setMessage("최대 글자 수에 도달하였습니다.");
                            builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            sweetAlertDialog=builder.create();
                            sweetAlertDialog.show();
                        }else{
                            if(!sweetAlertDialog.isShowing()){
                                sweetAlertDialog.show();
                            }
                        }
                    }

                    remain_byte = 140 - (msg_len);
                    msg_length = String.format(Locale.getDefault(),"(%d/%d)" , remain_byte, 140);

                    txtLimit.setText(msg_length);
                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                }
            }
        });
    }
}
