package com.dial070.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.dial070.global.ServicePrefs;
import com.dial070.utils.Log;



public class NaverParser {
	
	private static final String THIS_FILE = "DIAL070_NAVER_PARSER"; 
	
	ArrayList<NaverResult> parseResult;
	String tag;
	
	NaverParser () {
		parseResult = new ArrayList<NaverResult>();
	}

	
	ArrayList<NaverResult> getResult()
	{
		return parseResult;
	}

	
	int parseXml(String keyword)
	{
		int rs = -1;
		int read_count = 0;
		
		parseResult.clear();

		String target = "";
		String query = "";
		try {
			query = URLEncoder.encode(keyword, "utf-8");
			target = URLEncoder.encode("local", "utf-8"); 
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}				
		
		try {
					
			//String url = String.format("http://openapi.naver.com/search?key=%s&query=%s&target=local&start=1&display=%d", ServicePrefs.NAVER_KEY,query,ServicePrefs.NAVER_RESULT_CNT); 
			String url = String.format("http://openapi.naver.com/search?key=%s&query=%s&target=%s&start=1&display=%d", ServicePrefs.NAVER_KEY,query,target,ServicePrefs.NAVER_RESULT_CNT);
			URL targetURL = new URL(url);
			
			InputStream is = targetURL.openStream();

			//Parser
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			
			if(is == null) { Log.e(THIS_FILE, "InputStream is NULL!!"); return rs;}
		
			rs = 0;
			parser.setInput(is, "utf-8");

			
			NaverResult nr = null;
			boolean startParsing = false;

			// NAVER API
			int parserEvent = parser.getEventType();
			while((parserEvent != XmlPullParser.END_DOCUMENT) && (read_count < 1000)) {
				switch(parserEvent) {
				case XmlPullParser.END_TAG:
					tag = parser.getName();
					if(tag.compareTo("item") == 0) {
						String number = nr.getTelephone();
						if (number != null && number.length() > 0)
							parseResult.add(nr);
						startParsing = false;
					}
				
					break;
				case XmlPullParser.START_TAG:
					tag = parser.getName();
					if(tag.compareTo("item") == 0) {
						nr = new NaverResult();
						startParsing = true;
					} 
					break;
				case XmlPullParser.TEXT:
					if(startParsing) {
						if(tag.compareTo("title") == 0)
						{
							nr.setTitle(parser.getText());
						}
						else if(tag.compareTo("description") == 0)
						{
							nr.setDescription(parser.getText());
						}
						else if(tag.compareTo("telephone") == 0)
						{
							nr.setTelephone(parser.getText());
						}	
						else if(tag.compareTo("address") == 0)
						{
							nr.setAddress(parser.getText());
						}						
						else if(tag.compareTo("mapx") == 0)
						{
							nr.setMapX(parser.getText());
						}	
						else if(tag.compareTo("mapy") == 0)
						{
							nr.setMapY(parser.getText());
						}	
					}
					break;
				}

				parserEvent = parser.next();
				read_count = read_count + 1;
			}			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		return rs;
	}	
	
	int parseNewXml(String keyword) // BJH 2016.12.27 Naver API 종료
	{
		int rs = -1;
		int read_count = 0;
		
		parseResult.clear();
        
		try {
			String text = URLEncoder.encode(keyword, "UTF-8");
			//String target = URLEncoder.encode("local", "utf-8");  
	        String apiURL = String.format("https://openapi.naver.com/v1/search/local.xml?query=%s&display=%d", text, ServicePrefs.NAVER_RESULT_CNT);
			Log.d(THIS_FILE,"apiURL"+apiURL);
	        URL url = new URL(apiURL);
	        HttpURLConnection con = (HttpURLConnection)url.openConnection();
	        con.setRequestMethod("GET");
	        con.setRequestProperty("X-Naver-Client-Id", ServicePrefs.NAVER_CLIENT_ID);
	        con.setRequestProperty("X-Naver-Client-Secret", ServicePrefs.NAVER_CLIENT_SECRET);
	        int responseCode = con.getResponseCode();

	        InputStream is = null;
	        if(responseCode==200) { // 정상 호출
	        	is = con.getInputStream();
	        } else {  // 에러 발생
	            return rs;
	        }

			//Parser
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
		
			rs = 0;
			parser.setInput(is, "utf-8");

			
			NaverResult nr = null;
			boolean startParsing = false;

			// NAVER API
			int parserEvent = parser.getEventType();
			while((parserEvent != XmlPullParser.END_DOCUMENT) && (read_count < 1000)) {
				switch(parserEvent) {
				case XmlPullParser.END_TAG:
					tag = parser.getName();
					if(tag.compareTo("item") == 0) {
						String number = nr.getTelephone();
						if (number != null && number.length() > 0)
							parseResult.add(nr);
						startParsing = false;
					}
				
					break;
				case XmlPullParser.START_TAG:
					tag = parser.getName();
					if(tag.compareTo("item") == 0) {
						nr = new NaverResult();
						startParsing = true;
					} 
					break;
				case XmlPullParser.TEXT:
					if(startParsing) {
						if(tag.compareTo("title") == 0)
						{
							nr.setTitle(parser.getText());
						}
						else if(tag.compareTo("description") == 0)
						{
							nr.setDescription(parser.getText());
						}
						else if(tag.compareTo("telephone") == 0)
						{
							nr.setTelephone(parser.getText());
						}	
						else if(tag.compareTo("address") == 0)
						{
							nr.setAddress(parser.getText());
						}						
						else if(tag.compareTo("mapx") == 0)
						{
							nr.setMapX(parser.getText());
						}	
						else if(tag.compareTo("mapy") == 0)
						{
							nr.setMapY(parser.getText());
						}	
					}
					break;
				}

				parserEvent = parser.next();
				read_count = read_count + 1;
			}			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		return rs;
	}	
}
