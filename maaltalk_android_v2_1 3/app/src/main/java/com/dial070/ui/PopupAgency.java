package com.dial070.ui;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;

import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class PopupAgency extends Activity {

	private Button mBtnCallfw, mBtnCallfwCancel, mBtnCancel;
	private String mAgency = null;
	private Context mContext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_agency);

		mContext = this;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mAgency = extras.getString("AGENCY");
			if(mAgency == null)
				finish();
		}
		LayoutParams params = getWindow().getAttributes();
	    DisplayMetrics dm = getResources().getDisplayMetrics();
	    int x = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				300, dm);
	    params.width = x;
	    getWindow().setAttributes((LayoutParams) params);

		final Resources res = mContext.getResources();

		mBtnCallfw = (Button) findViewById(R.id.btnCallfw);
		mBtnCallfw.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String msg = res.getString(R.string.ask_agency_callfw);
				showCallfwAlert(res, "'" + ServicePrefs.mUser070 + "' " + msg, "tel:*71" + ServicePrefs.mUser070);
			}
		});

		mBtnCallfwCancel = (Button) findViewById(R.id.btnCallfwCancel);
		mBtnCallfwCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String msg = res.getString(R.string.ask_agency_callfw_cancel);
				//if(mAgency.startsWith("sktelecom")) {
				if(mAgency.equals("45005") || mAgency.equals("45011") || mAgency.equals("45012")) { // SKTelecom
					showCallfwAlert(res, msg, "tel:*73");
				} else {
					showCallfwAlert(res, msg, "tel:*710");
				}
			}
		});
		
		mBtnCancel = (Button) findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	private AlertDialog mMsgDialog = null;
	private void showCallfwAlert(Resources res, String msg, final String number) {
		if(mMsgDialog != null)
			return;
		mMsgDialog = new AlertDialog.Builder(
				mContext).create();
		mMsgDialog
				.setTitle(R.string.title_agency);
		mMsgDialog.setMessage(msg);
		mMsgDialog.setButton(
				res.getString(R.string.yes),
				new DialogInterface.OnClickListener() {
					// @Override
					public void onClick(
							DialogInterface dialog,
							int which) {
						mNumber = number;
						makeCall();
						/*if (Build.VERSION.SDK_INT >= 23) {
							String[] PERMISSIONS = {android.Manifest.permission.CALL_PHONE};
							if (!hasPermissions(mContext, PERMISSIONS)) {
								ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST_CODE_PERMISSIONS );
							} else {
								makeCall();
							}
						} else {
							makeCall();
						}*/
						mMsgDialog = null;
						return;
					}
				});
		mMsgDialog.setButton2(
				res.getString(R.string.no),
				new DialogInterface.OnClickListener() {

					// @Override
					public void onClick(
							DialogInterface dialog,
							int which) {

						mMsgDialog = null;
						return;
					}
				});
		mMsgDialog.show();
	}

	private void makeCall() {
		//Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(mNumber));
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(mNumber));
		startActivity(intent);
	}

	private static final int REQUEST_CODE_PERMISSIONS = 1;
	private String mNumber;

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_CODE_PERMISSIONS: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					makeCall();
				} else {
					MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(this);
					builder.setTitle(getResources().getString(R.string.permission_setting));
					builder.setMessage(getResources().getString(R.string.permission_desc));
					builder.setCancelable(false);
					builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							mMsgDialog = null;
							Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
							Uri uri = Uri.fromParts("package", getPackageName(), null);
							intent.setData(uri);
							startActivity(intent);
							finish();
						}
					});
					builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							mMsgDialog = null;
							finish();
						}
					});
					builder.show();
				}
			}
		}
	}

	private static boolean hasPermissions(Context context, String... permissions) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}
	
}
