package com.dial070.ui;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.service.BridgePopupService;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class TransparentActivity extends Activity {
    private Handler autoCloseHandler = null;
    private Runnable autoCloseRunnable = null;
    private Context context;
    private TextView txtTime, txtDesc;
    private Intent intent;
    KeyguardManager km;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyLock = km.newKeyguardLock(Context.KEYGUARD_SERVICE);
        keyLock.disableKeyguard();    // 기본 잠금화면 없애기*/

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            km.requestDismissKeyguard(this,null);
        }*/

        setContentView(R.layout.activity_transparent);

        context=this;

        RelativeLayout root=findViewById(R.id.root);
        TextView txtTime=findViewById(R.id.txtTime);
        TextView txtDesc=findViewById(R.id.txtDesc);

        intent=getIntent();
        final String name=intent.getStringExtra("NAME");
        final long now=intent.getLongExtra("TIME",0);
        final String dial_number=intent.getStringExtra("dial_number");
        final String cid=intent.getStringExtra("CID");

        SimpleDateFormat mFormatter = new SimpleDateFormat("aa hh:mm");
        mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        String time=mFormatter.format(now);
        txtTime.setText(time);
        txtDesc.setText(name+" 님으로부터 전화가 왔습니다. 이곳을 터치해주세요.");
        setAutoClose(60000);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAutoClose();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    km.requestDismissKeyguard(TransparentActivity.this,null);
                }
                Intent intent3 = new Intent(context, BridgePopupService.class);
                intent3.putExtra("CID",cid);
                intent3.putExtra("TIME", now);
                intent3.putExtra("dial_number", dial_number);
                intent3.putExtra("NAME", name);
                context.startService(intent3);
                TransparentActivity.this.finish();
            }
        });

        /*Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String cid=intent.getStringExtra("CID");
                Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + cid));
                context.startActivity(intent2);

                Intent intent3 = new Intent(context, BridgePopupService.class);
                intent3.putExtra("TIME", now);
                context.startService(intent3);
            }
        }, 1000);*/


    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setFlags(
                //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,

                //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void setAutoClose(long delayMS) {
        cancelAutoClose();
        autoCloseRunnable = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };
        autoCloseHandler = new Handler();
        autoCloseHandler.postDelayed(autoCloseRunnable, delayMS);
    }

    private void cancelAutoClose() {
        if (autoCloseHandler != null) {
            if (autoCloseRunnable != null) {
                autoCloseHandler.removeCallbacks(autoCloseRunnable);
                autoCloseRunnable = null;
            }
            autoCloseHandler = null;
        }
    }
}
