package com.dial070.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dial070.db.DBManager;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.Log;

public class SmsWrite extends AppCompatActivity {
	
	private static final String THIS_FILE = "SMS-WRITE";
	private static final String CHAR_SET = "KSC5601";
	private static final int SMS_MAX_MSG_LENGTH = 90;
	protected static final int SMS_SELECT_CONTACT = 1;
	protected static final int SMS_SELECT_PICTURE = 2;

	private TextView mMsgLength;
	
	private Context mContext;
	private ContextWrapper mContextWrapper;
	
	private Button mSendMsg;
	private ImageButton mSendContact;
	private ImageButton mMsgAdd;
	
	private EditText mMessage;
	private EditText mReceiverInput;
	
	private ArrayList<Bitmap> mImageList;
	
	private ProgressDialog mProgressDialog;
	//BJH
	private Integer smsMid = 0;
	private int mResult = 0;
	private int heightEditMsgOrigin;

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final ActionBar abar = getSupportActionBar();
		androidx.appcompat.app.ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
				androidx.appcompat.app.ActionBar.LayoutParams.WRAP_CONTENT,
				androidx.appcompat.app.ActionBar.LayoutParams.WRAP_CONTENT,
				Gravity.CENTER);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(true);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		mContextWrapper= new ContextWrapper(mContext);

		setTitle(getString(R.string.message_new));

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.sms_write_activity);
		
		mImageList = new ArrayList<Bitmap>();

		mReceiverInput = (EditText) findViewById(R.id.receiver_input);
		
		
		mMsgLength = (TextView) findViewById(R.id.sms_conts_length);
		mMessage = (EditText) findViewById(R.id.edit_message);

		mReceiverInput.setVisibility(View.VISIBLE);
		
		//InputFilter[] maxLengthFilter = new InputFilter[] {new ByteLengthFilter(SMS_MAX_MSG_LENGTH,CHAR_SET)};
		//mMessage.setFilters(maxLengthFilter);

		try {
			mMessage.addTextChangedListener(new TextWatcher() {
	
				@Override
				public void afterTextChanged(Editable editable) {
					String msg = editable.toString();
					int index = 0;
					int start = 0;
					int num_chars = msg.length()-1;
					int media_count = 0;
					//BJH
					int result_value = 0;
					int count = 0;
					int excepiton = 0;
					for (index = start; index < start+num_chars; index++) 
					{
						char c = msg.charAt(index);
						if (c == 0xFFFF)
						{
							String key = msg.subSequence(index+1, index + 2).toString();
							try {
								
								int value = Integer.parseInt(key);
								result_value = result_value + value;
								count++;
							}catch (NumberFormatException e) {
								excepiton = 1;
							}
						}
					}
					
					if(excepiton == 0) {
						if(mImageList.size() == 1) {
							if(count < 1)
								mImageList.remove(0);
						}else if(mImageList.size() == 2) {
							if(count < 2) {
								if(result_value == 0)
									mImageList.remove(1);
								else if(result_value == 1) {
									mImageList.remove(0);
									mResult = 1;
								}else if(result_value == 2) {
									mImageList.remove(0);
									mResult = 2;
								}
							}
						}else if(mImageList.size() == 3) {
							if(count < 3) {
								if(result_value == 1)
									mImageList.remove(2);
								else if(result_value == 2) {
									mImageList.remove(1);
									mResult = 1;
								}
								else if(result_value == 3) {
									mImageList.remove(0);
									mResult = 1;
								}
							}
						}
					}

					
					index = 0;
					start = 0;
					num_chars = msg.length()-1;
					for (index = start; index < start+num_chars; index++) 
					{
						//CharSequence c = editable.subSequence(index, index + 1);
						char c = msg.charAt(index);
						if (c == 0xFFFF)
						{
							String key = msg.subSequence(index+1, index + 2).toString();
							//int length = 1;
							try {
								int value = Integer.parseInt(key);
								if(value == 1 && mResult == 1)
									value = value - 1;
								if(value == 2 && mResult == 1)
									value = value - 1;
								if(value == 2 && mResult == 2)
									value = value - 2;
								Bitmap bmp = mImageList.get(value);
								Drawable dr = new BitmapDrawable(bmp);
								//dr.setBounds(0,0,bmp.getWidth(),bmp.getHeight());
								dr.setBounds(0,0,mMessage.getLineHeight()*3, mMessage.getLineHeight()*3);
								editable.setSpan(new ImageSpan(dr), index, index+2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
								media_count++;
							}catch (NumberFormatException e) {
								
							}
						}
							
					}
					
					// 순수 텍스트.
					msg = getTextMessage();
					int msg_len = 0;
					try {
						msg_len = msg.getBytes(CHAR_SET).length;
			        } catch (UnsupportedEncodingException e) {
			            //e.printStackTrace();
			        }	
					String msg_length = "";
					
					if (media_count > 0)
					{
						msg_length = "MMS";
					}
					else if (msg_len <= SMS_MAX_MSG_LENGTH)//BJH SMS 길이는 90까지
					{
						int remain_byte = SMS_MAX_MSG_LENGTH - (msg_len);
						msg_length = String.format(Locale.getDefault(),"%d/%d" , remain_byte, SMS_MAX_MSG_LENGTH);
					}
					else
					{
						msg_length = "LMS";
					}
					mMsgLength.setText(msg_length);
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					//do nothing
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before,
						int count) {
					//do nothing					
				}
			});
		} catch (Exception e) {
			//
		}
		
		Intent intent = getIntent();
		String phone = intent.getStringExtra("phone");
		//String name = intent.getStringExtra("name");		
		
		if(phone != null)
		{
			mReceiverInput.setText(phone);
			mMessage.post(new Runnable() {
				@Override
				public void run() {
					mMessage.setFocusableInTouchMode(true);
					mMessage.requestFocus();
					heightEditMsgOrigin=mMessage.getHeight();

					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

					imm.showSoftInput(mMessage,0);

				}
			});
		}else {
			mMessage.post(new Runnable() {
				@Override
				public void run() {
					heightEditMsgOrigin=mMessage.getHeight();
				}
			});
		}
		
		
		//메세지 전송 
		mSendMsg = findViewById(R.id.btn_message_send);
		mSendMsg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<Bitmap> list = new ArrayList<Bitmap>();
				String msg = getTextMessage(list);
				if(msg.length() == 0 && list.size() == 0) return;
				String number = mReceiverInput.getText().toString();//BJH 번호가 없으면 전송되지 않도록
				if(number.length() == 0) return;
				mMessage.setText("");

				//전송중 프로그래스 다이얼로그 표시 
				sendMessage(msg, list);
				
			}
		});
		
		// 연락처
		mSendContact = (ImageButton) findViewById(R.id.btn_msg_contact);
		mSendContact.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				goContacts();
			}
		});
		
		
		//메세지 첨부.
		mMsgAdd = (ImageButton) findViewById(R.id.btn_msg_conts_add);
		mMsgAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				return;
				//selectImage();
			}
		});

		mMessage.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
				if( heightEditMsgOrigin == bottom-top )
				{
					mMsgLength.setVisibility(View.INVISIBLE);
				}else {
					mMsgLength.setVisibility(View.VISIBLE);
				}
			}
		});
				
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case android.R.id.home:
				onBackPressed();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case SMS_SELECT_CONTACT:
			if (resultCode == RESULT_OK)
			{
				String number = data.getStringExtra("PhoneNumber");
				number = number.replace("-", "");
				mReceiverInput.setText(number);
			}
			break;				
		case SMS_SELECT_PICTURE:
			if (resultCode == RESULT_OK) 
			{
				if (data == null) {
					Log.w(THIS_FILE,
							"Null data, but RESULT_OK, from image picker!");
					// Toast t = Toast.makeText(this,
					// R.string.no_photo_picked,
					// Toast.LENGTH_SHORT);
					// t.show();
					return;
				}

				final Bundle extras = data.getExtras();
				if (extras != null) 
				{
					Bitmap bmp = extras.getParcelable("data");
					if (bmp == null) return;
					mImageList.add(bmp);
					addImage();
				}
				else
				{
					Uri selectedImage = data.getData();
					final String[] filePathColumn = { MediaColumns.DATA, MediaColumns.DISPLAY_NAME };
					Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
					// some devices (OS versions return an URI of com.android instead of com.google.android
					if (selectedImage.toString().startsWith("content://com.android.gallery3d.provider"))  {
						// use the com.google provider, not the com.android provider.
						selectedImage = Uri.parse(selectedImage.toString().replace("com.android.gallery3d","com.google.android.gallery3d"));
					}
					if (cursor != null) {
						cursor.moveToFirst();
						int columnIndex = cursor.getColumnIndex(MediaColumns.DATA);
						// if it is a picasa image on newer devices with OS 3.0 and up
						if (selectedImage.toString().startsWith("content://com.google.android.gallery3d")){
							columnIndex = cursor.getColumnIndex(MediaColumns.DISPLAY_NAME);
							if (columnIndex != -1) {
								//progress_bar.setVisibility(View.VISIBLE);
								final Uri uriurl = selectedImage;
								// Do this in a background thread, since we are fetching a large image from the web
								new Thread(new Runnable() {
									public void run() {
										//Bitmap bmp = getBitmap("image_file_name.jpg", uriurl);
										Bitmap bmp = loadImage(uriurl.toString());
										if (bmp == null) return;
										mImageList.add(bmp);
										addImage();
									}
								}).start();
							}
						} else { // it is a regular local image file
							String filePath = cursor.getString(columnIndex);
							cursor.close();
							Bitmap bmp = loadImage(filePath);
							if (bmp == null) return;
							mImageList.add(bmp);
							addImage();
						}
					}
					else if (selectedImage != null && selectedImage.toString().length() > 0) {
						//progress_bar.setVisibility(View.VISIBLE);
						final Uri uriurl = selectedImage;
						// Do this in a background thread, since we are fetching a large image from the web
						new Thread(new Runnable() {
							public void run() {
								//Bitmap bmp = getBitmap("image_file_name.jpg", uriurl);
								Bitmap bmp = loadImage(uriurl.toString());
								if (bmp == null) return;
								mImageList.add(bmp);
								addImage();
							}
						}).start();
					}
				}
			}
			break;
		default:
			break;		
		}
		return;    
	}

	private Handler handler = new Handler();
	
	private int sendSmsMessage(final String msg, final ArrayList<Bitmap> list)
	{
		String name = null;
		long mid = 0;
		final String to = mReceiverInput.getText().toString();
		
		// INSERT DATABASE
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			
			name = ContactHelper.getContactsNameByPhoneNumber(mContext,to);
			if(name == null) name = to;
			//insertRecentCalls(database,"me",to,name);
			mid = insertSmsList(database,"me",to,name,msg, list);
			
		} 
		catch(SQLException e)
		{
			e.printStackTrace();
		}	
		database.close();
		
		// set filename
		ArrayList<String> flist = null;
		if (list != null && list.size() > 0)
		{
			flist = new ArrayList<String>();
			int seq = 0;
			for (Bitmap bmp : list)
			{
				String fileName = getImageFileName(mid, ++seq);
				flist.add(fileName);
			}
		}
		
		
		final ArrayList<String> fileList = flist;
		Thread t = new Thread()
		{
			public void run()
			{
				// SEND TO SERVER //BJH 메시지 결과 값 출력
				int i = Sms.sendMessageToServer(mContext, to, msg, fileList);
				clearImage();
				if(i < 0) {
					String str = "";
					if(i == -2){
						str = "did";
					} else if(i == -3) {
						str = "balance";
					} else if(i == -4) {
						str = "expired";
					} else if(i == -5) {
						str = "verified";
					} else if(i == -6) {
						str = "status";
					}
					Sms.deleteItem(mContext,smsMid);
					String number = mReceiverInput.getText().toString();
					Sms.updateDeleteList(mContext,number);
					
					Looper.prepare();
			
					Sms.AlertFail(mContext, str);
					
					Looper.loop();
				} else {	
					handler.post(mSentResults);
				}
			};
		};
		t.start();
		
		return 0;
		
	}

    /* BJH 2017.09.28 문자 관련 */
    private ProgressDialog mSMSProgressDialog;
    final Runnable mResults = new Runnable()
    {
        public void run()
        {
            if (mSMSProgressDialog != null && mSMSProgressDialog.isShowing()) {
                mSMSProgressDialog.dismiss();
                mSMSProgressDialog = null;
            }
        }
    };

	private void sendMessage(final String msg, final ArrayList<Bitmap> list)
	{

//		Resources res = mContext.getResources();
//		String title = res.getString(R.string.sms_send);
//		String description = res.getString(R.string.sms_send_desc);
//		mProgressDialog = ProgressDialog.show(this, title, description,false);

        mSMSProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.sms_send), getResources().getString(R.string.sms_send_desc), false);
		Thread t = new Thread()
		{
			public void run()
			{
				int rs = sendSmsMessage(msg, list);
                handler.post(mResults);
				if (rs < 0)
				{
					// 실패.
				}
				//clearImage();
				
				//handler.post(mSentResults);
			};
		};
		t.start();					
	}
	
	final Runnable mSentResults = new Runnable()
	{
		public void run()
		{
//			if (mProgressDialog != null && mProgressDialog.isShowing())
//				mProgressDialog.dismiss();
			
			String number = mReceiverInput.getText().toString();
			Intent intent = new Intent(SmsWrite.this, SmsMsgContsActivity.class);
			intent.putExtra("number", number);
			startActivity(intent);
			 
			finish();
		}
	};	
			
	
//	private void insertRecentCalls(DBManager db, String from, String to, String name)
//	{
////		ContentValues cv = RecentCallsData.getContentValues(mContext, "me", to, name);
////		db.insertRecentCalls(cv);
//	}
	
	private String getImageFileName(final long mid, final int seq)
	{
		String homePath = mContextWrapper.getFilesDir().getPath();
		homePath = homePath + "/chat/media/";
		File file = new File(homePath);
	    if ( !file.exists() )
	    {
	        // 디렉토리가 존재하지 않으면 디렉토리 생성
	        file.mkdirs();
	    }
		String aFile = homePath + "media_" + String.valueOf(mid) + "_" + String.valueOf(seq) + ".jpg";
		return aFile;
	}

	
	private long insertSmsList(DBManager db, String from, String to, String name, String msg, ArrayList<Bitmap> list)
	{
		String user_name = ContactHelper.getContactsNameByPhoneNumber(mContext,to);
		
		int newCount = db.getSmsNewMsgCount(to);
		
		if(db.existSmsList(to))
		{
			db.updateSmsList(to,user_name, msg, SmsListData.SEND_TYPE, newCount);
		}
		else
		{
			//내부번호 인지 체크 2:친구목록 
			int userType = SmsListData.USER_TYPE_CONTACT;
			if(isInternal(to)) userType = SmsListData.USER_TYPE_BUDDY;
			
			SmsListData sms_list = new SmsListData(userType,to,name,SmsListData.SEND_TYPE,msg,user_name,System.currentTimeMillis(), newCount);
			db.insertSmsList(sms_list);
		}
		String callback = ServicePrefs.mUser070;
		//if(mChangedSender) callback = mSender.getText().toString();
		
		long cdate = System.currentTimeMillis();
		SmsMsgData sms_data = new SmsMsgData(0, from,to,callback,SmsMsgData.SEND_TYPE,SmsMsgData.MSG_NEW, "", msg, cdate);
		long mid = db.insertSmsMsg(sms_data);
		//BJH
		smsMid = (int)mid;
		
		if (list != null && list.size() > 0)
		{
			// SAVE
			int seq = 1;
			for (Bitmap bmp : list)
			{
				String fileName = getImageFileName(mid, seq);
				try {
					File file = new File(fileName);										
					FileOutputStream fOut = new FileOutputStream(file);
					boolean rs = bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
					fOut.flush();
					fOut.close();
					if (!rs) continue;
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				}
				
				if (seq == 1)
					sms_data.file1 = fileName;
				else if (seq == 2)
					sms_data.file2 = fileName;
				else if (seq == 3)
					sms_data.file3 = fileName;
				
				seq++;
			}
			
			db.updateSmsMsgFile(sms_data);
		}

		int count = db.getSmsNewMsgCount(from);
		db.updateSmsList(to,name, msg, SmsListData.SEND_TYPE, count);
		
		return mid;
	}
	
	private boolean isInternal(String number)
	{
		boolean rs = false;
		
//		String InternalPrefix = null;
//		
//		if (ServicePrefs.mPrefs != null)
//		{
//			try
//			{
//				if (ServicePrefs.mPrefs.has("INTERNALPREFIX"))
//					InternalPrefix = ServicePrefs.mPrefs.getString("INTERNALPREFIX");
//			}
//			catch (Exception e)
//			{
//				return rs;
//			}
//		}	
//		
//		if (InternalPrefix != null && number.startsWith(InternalPrefix))
//		{
//			rs = true;
//		}		
		
		return rs;
	}	
	
	
	//연락처 검색
	private void goContacts()
	{
		Intent intent = new Intent(SmsWrite.this, Contacts.class);
		intent.putExtra("SelectType", 0);	//0:contacts ,1 :favorites
		startActivityForResult(intent, SMS_SELECT_CONTACT);	
	}
	
	private void selectImage() 
	{
		if (mImageList.size() >= 3)
		{
			return;
		}
		
		final int REQUIRED_WIDTH = 320;
		final int REQUIRED_HIGHT = 220;
		try 
		{
			Resources res = mContext.getResources();
			Intent intent = new Intent();
			intent.setType("image/*");
			//intent.setAction(Intent.ACTION_GET_CONTENT); // BJH 작동되지않는 기기들이 있음.
			intent.setAction(Intent.ACTION_PICK);
			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", REQUIRED_WIDTH);
			intent.putExtra("aspectY", REQUIRED_HIGHT);
			//intent.putExtra("outputX", REQUIRED_WIDTH);
			//intent.putExtra("outputY", REQUIRED_HIGHT);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);
			intent.putExtra("outputFormat",Bitmap.CompressFormat.JPEG.toString());
			intent.putExtra("noFaceDetection", true);
			
			// "이미지 선택"
			try 
			{
				startActivityForResult(
						Intent.createChooser(intent,
								res.getString(R.string.opt_menu_image)),
						SMS_SELECT_PICTURE);
			} 
			catch (ActivityNotFoundException e) {
			}
		} 
		catch (Exception e) {
		}
	}


	private String getTextMessage()
	{
		return getTextMessage(null);
	}
	
	private String getTextMessage(ArrayList list)
	{
		String msg = mMessage.getText().toString();
		int index = 0;
		int start = 0;
		int num_chars = msg.length()-1;
		int count = 0;
		int offset = 0;
		for (index = start; index < start+num_chars; index++) 
		{
			char c = msg.charAt(index);
			if (c == 0xFFFF)
			{
				if (list != null)
				{
					String key = msg.subSequence(index+1, index + 2).toString();
					//int length = 1;
					try {//BJH
						int value = Integer.parseInt(key);
						if(value == 1 && mResult == 1)
							value = value - 1;
						if(value == 2 && mResult == 1)
							value = value - 1;
						if(value == 2 && mResult == 2)
							value = value - 2;
						Bitmap bmp = mImageList.get(value);
						list.add(bmp);
					}catch (NumberFormatException e) {
					}
					
				}
				count++;
				index++;
			}
		}
		if (count > 0)
		{
			offset = count*2 + 1;
		}		
		
		if (offset > 0)
		{
			if (offset < msg.length())
				msg = msg.substring(offset, msg.length());
			else
				msg = "";
		}
		
		return msg;
	}
	
	private void clearImage()
	{
		if (mImageList != null)
		{
			for (Bitmap bmp : mImageList)
			{
				bmp.recycle();
			}
			mImageList.clear();
		}
	}
	
	private boolean addImage()
	{
		int imageCount = mImageList.size();
		if (imageCount > 0)
		{
			String msg = getTextMessage();
			
			SpannableStringBuilder builder = new SpannableStringBuilder();
			if (imageCount > 0)
			{

				for(int i=0; i < imageCount; i++)
				{
					char c = 0xFFFF;
					builder.append(c);
					builder.append(String.valueOf(i));
				}
				builder.append("\n"); 
			}
		    builder.append(msg);
		    mMessage.setText(builder);
		    Editable editObj =  mMessage.getText();
		    int position = editObj.length();
		    Selection.setSelection(editObj, position);
		}
		else
		{
			String msg = mMessage.getText().toString();
			msg = msg.replaceFirst("\n","").trim();
		    mMessage.setText(msg);
		    Editable editObj =  mMessage.getText();
		    int position = editObj.length();
		    Selection.setSelection(editObj, position);
		}
	    
	    return true;
	}
	
	public static int getOrientation(Context context, Uri photoUri) {
	    /* it's on the external media. */
	    Cursor cursor = context.getContentResolver().query(photoUri,
	            new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

	    if (cursor == null || cursor.getCount() != 1) {
	        return -1;
	    }

	    cursor.moveToFirst();
	    int orientation = cursor.getInt(0);
	    cursor.close();
	    
	    return orientation;
	}


	private Bitmap loadImage(String aFile)
	{
		if (aFile == null || aFile.length() == 0)
		{
			return null;			
		}
		
		int orientation  = getOrientation(mContext, Uri.fromFile(new File(aFile)));
		
		// LOAD FROM FILE
		try
		{
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(aFile, o);

			final int REQUIRED_WIDTH = 320;
			final int REQUIRED_HIGHT = 220;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
				scale *= 2;			
			
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inSampleSize = scale;
			
			boolean retry = false;
			Bitmap bmp = null;
			try
			{
				bmp = BitmapFactory.decodeFile(aFile, options);
			}
			catch (OutOfMemoryError e) 
			{
				retry = true;
				e.printStackTrace();
			}
			
			if (retry)
			{
				scale *= 2;
				options.inSampleSize = scale;
				bmp = BitmapFactory.decodeFile(aFile, options);
			}
			
			// CHECK RESIZE
			if (bmp != null)
			{
				int w = options.outWidth;
				int h = options.outHeight;
				
				if (w > REQUIRED_WIDTH)
				{
					int v =  (h * REQUIRED_WIDTH) / w;
					
					Bitmap resized = Bitmap.createScaledBitmap(bmp, REQUIRED_WIDTH, v, true);
					if (resized != null)
					{
						bmp.recycle();
						bmp = resized;
						w = REQUIRED_WIDTH;
						h = v;
					}
				}
				
				if (h > REQUIRED_HIGHT)
				{
					int v =  (w * REQUIRED_HIGHT) / h;
					
					Bitmap resized = Bitmap.createScaledBitmap(bmp, v, REQUIRED_HIGHT, true);
					if (resized != null)
					{
						bmp.recycle();
						bmp = resized;
						w = v;
						h = REQUIRED_HIGHT;
					}
				}
				
				
			   if (orientation > 0) 
			   {
			        Matrix matrix = new Matrix();
			        matrix.postRotate(orientation);

			        Bitmap resized = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
					if (resized != null)
					{
						bmp.recycle();
						bmp = resized;
					}
			    }
			}
			
			return bmp;
			
		} 
		catch (OutOfMemoryError e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	
		return null;
	}
	

}
