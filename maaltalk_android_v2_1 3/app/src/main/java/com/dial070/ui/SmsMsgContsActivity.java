package com.dial070.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
//import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.db.DBManager;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.service.MessageService;
import com.dial070.sip.api.SipManager;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;


public class SmsMsgContsActivity extends AppCompatActivity {
    private static final String THIS_FILE = "SMS-CONTS";
    private static final int SMS_MAX_MSG_LENGTH = 90;
    private static final String CHAR_SET = "KSC5601";

    private static final int SELECT_PICTURE = 1;

    public static SmsMsgContsActivity currentContext = null;

    private Context mContext;
    private ContextWrapper mContextWrapper;
    private SmsMsgContsListAdapter mAdapter;

    private TextView mMsgLength;
    private EditText mMessage;
    private ImageButton mMsgAdd;
    private Button mSendMsg;
    private TextView mUserName;
    private TextView mUserNumber;

    private ListView mSmsMsgListView;
    private LinearLayout mLayoutSmsContsSend;
    private LinearLayout mLayoutDeleteAll, mLayoutSmsContsDelete;

    private LinearLayout mSelectAllItem;
    private CheckBox mCheckBoxAllItem;
    private ProgressDialog mProgressDialog;

    private boolean mAllItemChecked;

    private Button mCancel;
    private Button mDelete;

    private boolean mDeleteMode;
    private String mName;
    private String mNumber;

    private boolean mNeedScrollToBottm;

    private ArrayList<Bitmap> mImageList;


    private static final int DIAL070_UPDATE_START = 1;
    private static final int DIAL070_UPDATE = 2;
    private static final int DIAL070_UPDATE_END = 3;

    //BJH
    private Integer smsMid = 0;
    private int mResult = 0;
    //BJH 2016.09.26
    private static DownloadManager downloadManager;
    private static DownloadManager.Request request;

    private Toolbar mToolbar; // BJH 2017.02.28

    private int heightEditMsgOrigin;

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DIAL070_UPDATE_START: {
//					View emptyView = mListView.getEmptyView();
//					if (emptyView != null)
//						emptyView.setVisibility(View.GONE);
                }
                break;
                case DIAL070_UPDATE: {
                    mNeedScrollToBottm = true;
                    reloadListView();
                }
                break;
                case DIAL070_UPDATE_END: {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
                break;

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        mContext = this;
        mContextWrapper = new ContextWrapper(mContext);
        mImageList = new ArrayList<Bitmap>();

        setContentView(R.layout.sms_msg_conts_activity);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setBackgroundColor(Color.parseColor("#00A1C8"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //mToolbar.setLogo(R.drawable.message);
        //mToolbar.setVisibility(View.VISIBLE);

        mSmsMsgListView = (ListView) findViewById(R.id.sms_msg_conts_list);

        mNeedScrollToBottm = true;
        mSmsMsgListView.setOnTouchListener(new View.OnTouchListener() {
            float height;

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                int action = event.getAction();
                float height = event.getY();
                if (action == MotionEvent.ACTION_DOWN) {
                    this.height = height;
                } else if (action == MotionEvent.ACTION_UP) {
                    if (this.height < height) {
                        Log.v(THIS_FILE, "Scrolled up");
                        mNeedScrollToBottm = false;
                    } else if (this.height > height) {
                        Log.v(THIS_FILE, "Scrolled down");
                        mNeedScrollToBottm = false;
                    }
                }
                return false;
            }
        });


        mMsgLength = (TextView) findViewById(R.id.sms_conts_length);
        mMessage = (EditText) findViewById(R.id.sms_conts_msg);
        mUserName = (TextView) findViewById(R.id.sms_conts_name);
        mUserNumber = (TextView) findViewById(R.id.sms_conts_number);
//		mUserPhone = (TextView) findViewById(R.id.sms_conts_phone);
//		mUserPhoto = (ImageView) findViewById(R.id.sms_conts_avatar);

        mMsgLength.setVisibility(View.INVISIBLE);

        mLayoutSmsContsSend = (LinearLayout) findViewById(R.id.lay_sms_conts_send);
        //if (ServicePrefs.DIAL_MSG_SEND) // BJH
        if (ServicePrefs.DIAL_MSG_SEND && ServicePrefs.mUseMSG)
            mLayoutSmsContsSend.setVisibility(View.VISIBLE);
        else
            mLayoutSmsContsSend.setVisibility(View.GONE);

        mLayoutSmsContsDelete = (LinearLayout) findViewById(R.id.lay_sms_conts_delete);
        mLayoutSmsContsDelete.setVisibility(View.GONE);


        //mLayoutSmsContsInfo = (LinearLayout) findViewById(R.id.lay_sms_conts_info);
        mLayoutDeleteAll = (LinearLayout) findViewById(R.id.allitem);
        //mLayoutSmsContsInfo.setVisibility(View.VISIBLE);
        mLayoutDeleteAll.setVisibility(View.GONE);


        PhoneNumberFormattingTextWatcher digitFormater = new PhoneNumberFormattingTextWatcher();
        mUserName.addTextChangedListener(digitFormater);

        Intent intent = getIntent();
        mName = intent.getStringExtra("name");
        mNumber = intent.getStringExtra("number");
        //BJH
        if (mNumber.equals("PUSH"))
            mLayoutSmsContsSend.setVisibility(View.GONE);

        if ((mName == null || mName.length() == 0) && (mNumber != null && mNumber.length() > 0)) {
            mName = ContactHelper.getContactsNameByPhoneNumber(mContext, mNumber);
        }

        //BJH 메시지함에서 이름 과 전화번호가 보이도록
        if (mName != null && mName.length() > 0) {
            //mUserName.setText(mName.concat(" (" + mNumber + ")"));//메시지함에서 이름 과 전화번호가 보이도록
            mUserName.setText(mName);//메시지함에서 이름 과 전화번호가 보이도록
            mUserNumber.setText(mNumber);
        } else {
            mUserName.setText(mNumber);
            mUserNumber.setText(mNumber);
        }


        //BJH 2017.02.28
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(mUserName.getText().toString());
        if (!mNumber.equals("PUSH") && mName!=null){
            getSupportActionBar().setSubtitle(mUserNumber.getText().toString());
        }

        mMessage.post(new Runnable() {
            @Override
            public void run() {
                mMessage.setFocusableInTouchMode(true);
                mMessage.requestFocus();
                heightEditMsgOrigin=mMessage.getHeight();
            }
        });

        mDeleteMode = false;
        mAllItemChecked = false;
//		mItemChecked=false;

        mDelete = (Button) findViewById(R.id.btn_sms_conts_delete);
        mDelete.setEnabled(false);
        mDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //선택 메세지 삭제
                Resources res = mContext.getResources();
                AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                msgDialog.setTitle(R.string.delete_sms);
                msgDialog.setMessage(res.getString(R.string.delete_question));
                msgDialog.setButton(Dialog.BUTTON_POSITIVE,res.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSmsMsg();
                        return;
                    }
                });
                msgDialog.setButton(Dialog.BUTTON_NEGATIVE,res.getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                msgDialog.show();

            }
        });

        mCancel = (Button) findViewById(R.id.btn_sms_conts_cancel);
        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setViewMode();

            }
        });

        //Load Photo
        /*
        mUserPhoto.setImageResource(R.drawable.default_user_photo);
		if(mNumber != null)
		{
			if(isInternal(mNumber))
			{
				//친구목록의 아바타 이미지 ...
				//sms 에서는 아바타 이미지를 보여주지 않음.
			}
			else
			{
				Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, mNumber);
				if(c != null && c.getCount() > 0)
				{
					if(c.moveToFirst())
					{
						long id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
						long photo_id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_ID));
						c.close();
						if( id > 0) 
						{
							Drawable photo = ContactHelper.getPhoto(mContext,Long.toString(id),true);
							if(photo != null && photo_id > 0) 
								mUserPhoto.setImageDrawable(photo);
						}
					}
				}
			}
		}
		*/

        //InputFilter[] maxLengthFilter = new InputFilter[] {new ByteLengthFilter(SMS_MAX_MSG_LENGTH,CHAR_SET)};
        //mMessage.setFilters(maxLengthFilter);

        mMessage.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                /*Log.i(THIS_FILE,"left:"+left);
                Log.i(THIS_FILE,"top:"+top);
                Log.i(THIS_FILE,"right:"+right);
                Log.i(THIS_FILE,"bottom:"+bottom);
                Log.i(THIS_FILE,"oldLeft:"+oldLeft);
                Log.i(THIS_FILE,"oldTop:"+oldTop);
                Log.i(THIS_FILE,"oldRight:"+oldRight);
                Log.i(THIS_FILE,"oldBottom:"+oldBottom);
                Log.i(THIS_FILE,"v.getHeight:"+v.getHeight());*/
                if( heightEditMsgOrigin == bottom-top )
                {
                    mMsgLength.setVisibility(View.INVISIBLE);
                }else {
                    mMsgLength.setVisibility(View.VISIBLE);
                }
            }
        });

        try {
            mMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable editable) {
                    String msg = editable.toString();
                    int index = 0;
                    int start = 0;
                    int num_chars = msg.length() - 1;
                    int media_count = 0;
                    //BJH
                    int result_value = 0;
                    int count = 0;
                    int excepiton = 0;
                    for (index = start; index < start + num_chars; index++) {
                        //CharSequence c = editable.subSequence(index, index + 1);
                        char c = msg.charAt(index);
                        if (c == 0xFFFF) {
                            String key = msg.subSequence(index + 1, index + 2).toString();
                            try {
                                int value = Integer.parseInt(key);
                                result_value = result_value + value;
                                count++;
                            } catch (NumberFormatException e) {
                                excepiton = 1;
                            }
                        }
                    }

                    if (excepiton == 0) {
                        if (mImageList.size() == 1) {
                            if (count < 1)
                                mImageList.remove(0);
                        } else if (mImageList.size() == 2) {
                            if (count < 2) {
                                if (result_value == 0)
                                    mImageList.remove(1);
                                else if (result_value == 1) {
                                    mImageList.remove(0);
                                    mResult = 1;
                                } else if (result_value == 2) {
                                    mImageList.remove(0);
                                    mResult = 2;
                                }
                            }
                        } else if (mImageList.size() == 3) {
                            if (count < 3) {
                                if (result_value == 1)
                                    mImageList.remove(2);
                                else if (result_value == 2) {
                                    mImageList.remove(1);
                                    mResult = 1;
                                } else if (result_value == 3) {
                                    mImageList.remove(0);
                                    mResult = 1;
                                }
                            }
                        }
                    }

                    index = 0;
                    start = 0;
                    num_chars = msg.length() - 1;
                    for (index = start; index < start + num_chars; index++) {
                        //CharSequence c = editable.subSequence(index, index + 1);
                        char c = msg.charAt(index);
                        if (c == 0xFFFF) {
                            String key = msg.subSequence(index + 1, index + 2).toString();
                            //int length = 1;
                            try {
                                int value = Integer.parseInt(key);
                                if (value == 1 && mResult == 1)
                                    value = value - 1;
                                if (value == 2 && mResult == 1)
                                    value = value - 1;
                                if (value == 2 && mResult == 2)
                                    value = value - 2;
                                Bitmap bmp = mImageList.get(value);
                                Drawable dr = new BitmapDrawable(bmp);
                                //dr.setBounds(0,0,bmp.getWidth(),bmp.getHeight());
                                dr.setBounds(0, 0, mMessage.getLineHeight() * 3, mMessage.getLineHeight() * 3);
                                editable.setSpan(new ImageSpan(dr), index, index + 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                media_count++;
                            } catch (NumberFormatException e) {

                            }
                        }
                    }

                    // 순수 텍스트.
                    msg = getTextMessage();
                    int msg_len = 0;
                    try {
                        msg_len = msg.getBytes(CHAR_SET).length;
                    } catch (UnsupportedEncodingException e) {
                        //e.printStackTrace();
                    }
                    String msg_length = "";

                    if (media_count > 0) {
                        msg_length = "MMS";
                    } else if (msg_len <= SMS_MAX_MSG_LENGTH)//BJH SMS 최대 길이는 90이기 때문에
                    {
                        int remain_byte = SMS_MAX_MSG_LENGTH - (msg_len);
                        msg_length = String.format(Locale.getDefault(), "%d/%d", remain_byte, SMS_MAX_MSG_LENGTH);
                    } else {
                        msg_length = "LMS";
                    }
                    mMsgLength.setText(msg_length);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    //do nothing
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    //do nothing
                }
            });
        } catch (Exception e) {
            //
        }


        //메세지 첨부.
        mMsgAdd = (ImageButton) findViewById(R.id.btn_msg_conts_add);
        mMsgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
				String msg = mMessage.getText().toString();
				msg = msg.replaceFirst("\n","").trim();
				SpannableStringBuilder builder = new SpannableStringBuilder();
				builder.append(" ");
				builder.setSpan(new ImageSpan(mContext, R.drawable.btn_msg_write), 0, 1, 0);
			    builder.append("\n");
			    builder.append(msg);
			    mMessage.setText(builder);
			    Editable editObj =  mMessage.getText();
			    int position = editObj.length();
			    Selection.setSelection(editObj, position);
			    */
                return;
                //selectImage();
            }
        });


        //메세지 전송
        mSendMsg = findViewById(R.id.btn_msg_conts_send);
        mSendMsg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ArrayList<Bitmap> list = new ArrayList<Bitmap>();
                String msg = getTextMessage(list);
                if (msg.length() == 0 && list.size() == 0) return;

                mMessage.setText("");

                //전송중 프로그래스 다이얼로그 표시
                sendMessage(msg, list);
            }
        });

        mCheckBoxAllItem = (CheckBox) findViewById(R.id.check_all_item);
        mCheckBoxAllItem.setClickable(false);

        mSelectAllItem = (LinearLayout) findViewById(R.id.allitem);
        mSelectAllItem.setClickable(false);
        mSelectAllItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mCheckBoxAllItem.setChecked(!mCheckBoxAllItem.isChecked());

                mAllItemChecked = mCheckBoxAllItem.isChecked();

                mDelete.setEnabled(mAllItemChecked);

                mAdapter.setCheckedAllItem(mAllItemChecked);

                mAdapter.notifyDataSetChanged();
            }
        });


        mSmsMsgListView.setClickable(true);
        mSmsMsgListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                // TODO Auto-generated method stub
                mCheckBoxAllItem.setChecked(false);

                SmsMsgContsListView view = (SmsMsgContsListView) arg1;
                CheckBox chk_select = null;
                SmsMsgContsItem item = view.getItem();
                if (view != null && item != null) {
                    if (item.getSmsMsgFrom().equalsIgnoreCase("me")) {
                        chk_select = (CheckBox) view.findViewById(R.id.chk_sms_msg_to);
                    } else {
                        chk_select = (CheckBox) view.findViewById(R.id.chk_sms_msg_from);
                    }
                    if (chk_select != null) {
                        //UI 변경
                        chk_select.setChecked(!chk_select.isChecked());
                        //데이타 변경
                        mAdapter.setCheckedItem(item.getSmsMsgId(), chk_select.isChecked());

                        if (mAdapter.getCheckedCount() > 0) mDelete.setEnabled(true);
                        else mDelete.setEnabled(false);
                    }
                }
                if (mAdapter.getCheckedCount() == mAdapter.getCount())
                    mCheckBoxAllItem.setChecked(true);
            }
        });


        mAdapter = new SmsMsgContsListAdapter(mContext);
        mAdapter.loadSmsMsgContsData(mNumber);
        mAdapter.setRead(mNumber);
        if (mName != null) mAdapter.setUserName(mName);
        mSmsMsgListView.setAdapter(mAdapter);

        if (mSmsMsgListView.getCount() > 0)
            mSmsMsgListView.setSelection(mSmsMsgListView.getCount() - 1);

        //BJH 2016.09.26
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        if (mAdapter != null) {
            mAdapter.destroyAdapter();
            mAdapter = null;
        }
        super.onDestroy();
    }

    /* BJH 2017.09.28 문자 관련 */
    private ProgressDialog mSMSProgressDialog;
    final Runnable mResults = new Runnable()
    {
        public void run()
        {
            if (mSMSProgressDialog != null && mSMSProgressDialog.isShowing()) {
                mSMSProgressDialog.dismiss();
                mSMSProgressDialog = null;
            }
        }
    };

    private void sendMessage(final String msg, final ArrayList<Bitmap> list) {
        mSMSProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.sms_send), getResources().getString(R.string.sms_send_desc), false);
        Thread t = new Thread() {
            public void run() {
                int rs = sendSmsMessage(msg, list);
                handler.post(mResults);
                if (rs < 0) {
                    // 실패.
                }
                clearImage(); //BJH

                handler.post(mSendResults);
                handler.sendMessage(handler.obtainMessage(DIAL070_UPDATE));
            }

            ;
        };
        t.start();
    }

    private int sendSmsMessage(final String msg, final ArrayList<Bitmap> list) {
        String name = null;
        long mid = 0;

        // INSERT DATABASE
        DBManager database = new DBManager(mContext);
        try {
            database.open();
            String to = mNumber;
            name = ContactHelper.getContactsNameByPhoneNumber(mContext, to);
            if (name == null) name = to;
            //insertRecentCalls(database,"me",to,name);//BJH
            mid = insertSmsList(database, "me", to, name, msg, list);
            smsMid = (int) mid;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();

        // set filename
        ArrayList<String> flist = null;
        if (list != null && list.size() > 0) {
            flist = new ArrayList<String>();
            int seq = 0;
            for (Bitmap bmp : list) {
                String fileName = getImageFileName(mid, ++seq);
                flist.add(fileName);
            }
        }


        final ArrayList<String> fileList = flist;
        Thread t = new Thread() {
            public void run() {
                // SEND TO SERVER
                int i = Sms.sendMessageToServer(mContext, mNumber, msg, fileList);
                DebugLog.d("test_sms return code: "+i);
                if (i < 0) {
                    String str = "";
                    if (i == -2) {
                        str = "did";
                    } else if (i == -3) {
                        str = "balance";
                    } else if (i == -4) {
                        str = "expired";
                    } else if (i == -5) {
                        str = "verified";
                    } else if (i == -6) {
                        str = "status";
                    }
                    Sms.deleteItem(mContext, smsMid);
                    Sms.updateDeleteList(mContext, mNumber);
                    handler.post(mDeleteResults);
                    Looper.prepare();

                    Sms.AlertFail(mContext, str);

                    Looper.loop();
                }
            }

            ;
        };
        t.start();

        return 0;

    }


    private void setViewMode() {
        mDeleteMode = false;

        mSelectAllItem.setClickable(false);
        mSmsMsgListView.setClickable(false);

        //mLayoutSmsContsInfo.setVisibility(View.VISIBLE);
        mToolbar.setVisibility(View.VISIBLE);

        mLayoutDeleteAll.setVisibility(View.GONE);

        mLayoutSmsContsSend.setVisibility(View.GONE);
        mLayoutSmsContsDelete.setVisibility(View.GONE);

        if (mNumber != null) {
            mAdapter.setSelectMode(mDeleteMode);
            mAdapter.loadSmsMsgContsData(mNumber);
            mAdapter.notifyDataSetChanged();

            if (mSmsMsgListView.getCount() > 0)
                mSmsMsgListView.setSelection(mSmsMsgListView.getCount() - 1);
            //BJH 삭제 후 전송 버튼이 보이지 않음
            if (ServicePrefs.DIAL_MSG_SEND && ServicePrefs.mUseMSG)
                mLayoutSmsContsSend.setVisibility(View.VISIBLE);

        }
    }

//	private void insertRecentCalls(DBManager db, String from, String to, String name)
//	{
//		ContentValues cv = RecentCallsData.getContentValues(mContext, "me", to, name);
//		db.insertRecentCalls(cv);
//	}

    private String getImageFileName(final long mid, final int seq) {
        String homePath = mContextWrapper.getFilesDir().getPath();
        homePath = homePath + "/chat/media/";
        File file = new File(homePath);
        if (!file.exists()) {
            // 디렉토리가 존재하지 않으면 디렉토리 생성
            file.mkdirs();
        }
        String aFile = homePath + "media_" + String.valueOf(mid) + "_" + String.valueOf(seq) + ".jpg";
        return aFile;
    }

    private long insertSmsList(DBManager db, String from, String to, String name, String msg, ArrayList<Bitmap> list) {
        int new_flag = 1;

        if (from.equalsIgnoreCase("me"))
            new_flag = 0;

        String callback = ServicePrefs.mUser070;
        long cdate = System.currentTimeMillis();
        SmsMsgData sms_data = new SmsMsgData(0, from, to, callback, SmsListData.SEND_TYPE, new_flag, "", msg, cdate);
        long mid = db.insertSmsMsg(sms_data);
        if (list != null && list.size() > 0) {
            // SAVE
            int seq = 1;
            for (Bitmap bmp : list) {
                String fileName = getImageFileName(mid, seq);
                try {
                    File file = new File(fileName);
                    FileOutputStream fOut = new FileOutputStream(file);
                    boolean rs = bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    if (!rs) continue;

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    continue;
                }

                if (seq == 1)
                    sms_data.file1 = fileName;
                else if (seq == 2)
                    sms_data.file2 = fileName;
                else if (seq == 3)
                    sms_data.file3 = fileName;

                seq++;
            }

            db.updateSmsMsgFile(sms_data);
        }

        int count = db.getSmsNewMsgCount(from);
        db.updateSmsList(to, name, msg, SmsListData.SEND_TYPE, count);

        return mid;
    }

//	private boolean isInternal(String number)
//	{
//		boolean rs = false;
//		
//		String InternalPrefix = null;
//		if (ServicePrefs.mPrefs != null)
//		{
//			try
//			{
//				if (ServicePrefs.mPrefs.has("INTERNALPREFIX"))
//					InternalPrefix = ServicePrefs.mPrefs.getString("INTERNALPREFIX");
//			}
//			catch (Exception e)
//			{
//				return rs;
//			}
//		}	
//		
//		if (InternalPrefix != null && number.startsWith(InternalPrefix))
//		{
//			rs = true;
//		}		
//		
//		return rs;
//	}


    private void deleteSmsMsg() {
        Resources res = mContext.getResources();
        String title = res.getString(R.string.delete_select_item);
        String description = res.getString(R.string.delete_select_item_desc);

        mProgressDialog = ProgressDialog.show(this, title, description, false);

        // DELETE RECENT CALLS
        Thread t = new Thread() {
            public void run() {
                deleteSelectItems();
                handler.post(mDeleteResults);
            }

            ;
        };
        t.start();


    }

    final Runnable mDeleteResults = new Runnable() {
        public void run() {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            setViewMode();
        }
    };

    final Runnable mSendResults = new Runnable() {
        public void run() {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();

            mAdapter.loadMoreSmsMsgContsData(mNumber);
            mAdapter.setRead(mNumber);
            mAdapter.notifyDataSetChanged();

            if (mSmsMsgListView.getCount() > 0)
                mSmsMsgListView.setSelection(mSmsMsgListView.getCount() - 1);
            mMessage.setText("");
        }
    };

    private boolean deleteSelectItems() {
        DBManager db = new DBManager(mContext);
        db.open();

        if (mCheckBoxAllItem.isChecked()) {
            if (mNumber != null) {
                db.deleteSmsList(mNumber);
                db.deleteSmsMsg(mNumber); //전체 아이템 삭제
            }
        } else {
            ArrayList<Integer> list = mAdapter.getCheckedList();
            for (int i = 0; i < list.size(); i++) {
                Integer id = list.get(i);
                db.deleteSmsMsg(id);
            }
            mAdapter.checkClear();
            //BJH
            Sms.updateDeleteList(mContext, mNumber);
        }
        db.close();

        return true;
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onPause");
        currentContext = null;
        unbindFromService();
        //BJH 2016.09.26
        unregisterReceiver(completeReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        Log.d(THIS_FILE, "onResume");
        currentContext = this;

        //BJH 2016.09.26 다운완료
        IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(completeReceiver, completeFilter);

        bindToService();
        reloadData();
    }


    private boolean bindToService() {
        registerReceiver(messageReceiver, new IntentFilter(MessageService.ACTION_NEW_MESSAGE));
        return true;
    }

    private void unbindFromService() {
        try {
            unregisterReceiver(messageReceiver);
        } catch (Exception e) {
            // That's the case if not registered (early quit)
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(MessageService.ACTION_NEW_MESSAGE)) {
                handler.sendMessage(handler.obtainMessage(DIAL070_UPDATE));
            }
        }
    };


    public void reloadData() {
        reloadListView();
        loadFromServer();
    }

    private synchronized void reloadListView() {
        if (mNumber != null) {
            mAdapter.loadSmsMsgContsData(mNumber);
            mAdapter.setRead(mNumber);
            mAdapter.notifyDataSetChanged();
            mSmsMsgListView.invalidateViews();

            if (mNeedScrollToBottm && mSmsMsgListView.getCount() > 0)
                mSmsMsgListView.setSelection(mSmsMsgListView.getCount() - 1);

        }
    }

    private void loadFromServer() {
        Intent intent = new Intent(SmsMsgContsActivity.this, MessageService.class);
        intent.setAction(MessageService.ACTION_START);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sms_menu, menu);
        if (mName != null && mName.length() > 0) {
            menu.findItem(R.id.action_contacts).setVisible(false);
        }else {
            menu.findItem(R.id.action_contacts).setVisible(true);
        }

        return true;
    }

    private final static int ADD_CONTACTS = 2;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_call:
                Intent intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
                intent.putExtra(SipManager.EXTRA_CALL_NUMBER, mNumber);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;

            case R.id.action_contacts:
                Intent contacts_intent = new Intent(Intent.ACTION_INSERT);
                contacts_intent.setData(Uri.parse("content://com.android.contacts/contacts"));
                contacts_intent.putExtra(ContactsContract.Intents.Insert.PHONE, mNumber);
                try {
                    startActivityForResult(contacts_intent, ADD_CONTACTS);
                } catch (ActivityNotFoundException e) {

                }
                break;

            case R.id.action_delete:
                mDeleteMode = true;
                mSelectAllItem.setClickable(true);
                mSmsMsgListView.setClickable(true);

                mAdapter.setSelectMode(mDeleteMode);
                mAdapter.loadSmsMsgContsData(mNumber);
                mAdapter.notifyDataSetChanged();

                //mLayoutSmsContsInfo.setVisibility(View.GONE);
                mToolbar.setVisibility(View.GONE);
                mLayoutDeleteAll.setVisibility(View.VISIBLE);

                mLayoutSmsContsSend.setVisibility(View.GONE);
                mLayoutSmsContsDelete.setVisibility(View.VISIBLE);
                break;

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

    public String getNumber() {
        return mNumber;
    }

    private void selectImage() {
        if (mImageList.size() >= 3) {
            return;
        }

        final int REQUIRED_WIDTH = 320;
        final int REQUIRED_HIGHT = 220;
        try {
            Resources res = mContext.getResources();
            Intent intent = new Intent();
            intent.setType("image/*");
            //intent.setAction(Intent.ACTION_GET_CONTENT); // BJH 작동되지않는 기기들이 있음.
            intent.setAction(Intent.ACTION_PICK);
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", REQUIRED_WIDTH);
            intent.putExtra("aspectY", REQUIRED_HIGHT);
            //intent.putExtra("outputX", REQUIRED_WIDTH);
            //intent.putExtra("outputY", REQUIRED_HIGHT);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            intent.putExtra("noFaceDetection", true);

            // "이미지 선택"
            try {
                startActivityForResult(
                        Intent.createChooser(intent,
                                res.getString(R.string.opt_menu_image)),
                        SELECT_PICTURE);
            } catch (ActivityNotFoundException e) {
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    if (data == null) {
                        Log.w(THIS_FILE,
                                "Null data, but RESULT_OK, from image picker!");
                        // Toast t = Toast.makeText(this,
                        // R.string.no_photo_picked,
                        // Toast.LENGTH_SHORT);
                        // t.show();
                        return;
                    }

                    final Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap bmp = extras.getParcelable("data");
                        if (bmp == null) return;
                        mImageList.add(bmp);
                        addImage();
                    } else {
                        Uri selectedImage = data.getData();
                        final String[] filePathColumn = {MediaColumns.DATA, MediaColumns.DISPLAY_NAME};
                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        // some devices (OS versions return an URI of com.android instead of com.google.android
                        if (selectedImage.toString().startsWith("content://com.android.gallery3d.provider")) {
                            // use the com.google provider, not the com.android provider.
                            selectedImage = Uri.parse(selectedImage.toString().replace("com.android.gallery3d", "com.google.android.gallery3d"));
                        }
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(MediaColumns.DATA);
                            // if it is a picasa image on newer devices with OS 3.0 and up
                            if (selectedImage.toString().startsWith("content://com.google.android.gallery3d")) {
                                columnIndex = cursor.getColumnIndex(MediaColumns.DISPLAY_NAME);
                                if (columnIndex != -1) {
                                    //progress_bar.setVisibility(View.VISIBLE);
                                    final Uri uriurl = selectedImage;
                                    // Do this in a background thread, since we are fetching a large image from the web
                                    new Thread(new Runnable() {
                                        public void run() {
                                            //Bitmap bmp = getBitmap("image_file_name.jpg", uriurl);
                                            Bitmap bmp = loadImage(uriurl.toString());
                                            if (bmp == null) return;
                                            mImageList.add(bmp);
                                            addImage();
                                        }
                                    }).start();
                                }
                            } else { // it is a regular local image file
                                String filePath = cursor.getString(columnIndex);
                                cursor.close();
                                Bitmap bmp = loadImage(filePath);
                                if (bmp == null) return;
                                mImageList.add(bmp);
                                addImage();
                            }
                        } else if (selectedImage != null && selectedImage.toString().length() > 0) {
                            //progress_bar.setVisibility(View.VISIBLE);
                            final Uri uriurl = selectedImage;
                            // Do this in a background thread, since we are fetching a large image from the web
                            new Thread(new Runnable() {
                                public void run() {
                                    //Bitmap bmp = getBitmap("image_file_name.jpg", uriurl);
                                    Bitmap bmp = loadImage(uriurl.toString());
                                    if (bmp == null) return;
                                    mImageList.add(bmp);
                                    addImage();
                                }
                            }).start();
                        }
                    }
                }
                break;
            //신규 연락처 등록
            case ADD_CONTACTS:
                ContactHelper.listClear();
                mName = ContactHelper.getContactsNameByPhoneNumber(mContext, mNumber);
                if (mName != null && mName.length() > 0) {
                    mUserName.setText(mName);
                    mUserNumber.setText(mNumber);
                    getSupportActionBar().setTitle(mUserName.getText().toString());
                    getSupportActionBar().setSubtitle(mUserNumber.getText().toString());
                }

                break;
        }
        return;
    }

//	private Bitmap getBitmap(String tag, Uri url) 
//	{
//		File cacheDir;
//		// if the device has an SD card
//		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
//			cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
//		} else {
//			// it does not have an SD card
//		   	cacheDir=ActivityPicture.this.getCacheDir();
//		}
//		if(!cacheDir.exists())
//		    	cacheDir.mkdirs();
//
//		File f=new File(cacheDir, tag);
//
//		try {
//			Bitmap bitmap=null;
//			InputStream is = null;
//			if (url.toString().startsWith("content://com.google.android.gallery3d")) {
//				is=getContentResolver().openInputStream(url);
//			} else {
//				is=new URL(url.toString()).openStream();
//			}
//			OutputStream os = new FileOutputStream(f);
//			Utils.CopyStream(is, os);
//			os.close();
//			return decodeFile(f);
//		} catch (Exception ex) {
//			Log.d(Utils.DEBUG_TAG, "Exception: " + ex.getMessage());
//			// something went wrong
//			ex.printStackTrace();
//			return null;
//		}
//	}

    private boolean addImage() {
        int imageCount = mImageList.size();
        if (imageCount > 0) {
            String msg = getTextMessage();

            SpannableStringBuilder builder = new SpannableStringBuilder();
            if (imageCount > 0) {
                for (int i = 0; i < imageCount; i++) {
                    char c = 0xFFFF;
                    builder.append(c);
                    builder.append(String.valueOf(i));
                }
                builder.append("\n");
            }

            builder.append(msg);
            mMessage.setText(builder);
            Editable editObj = mMessage.getText();
            int position = editObj.length();
            Selection.setSelection(editObj, position);
        } else {
            String msg = mMessage.getText().toString();
            msg = msg.replaceFirst("\n", "").trim();
            mMessage.setText(msg);
            Editable editObj = mMessage.getText();
            int position = editObj.length();
            Selection.setSelection(editObj, position);
        }

        return true;
    }

    public static int getOrientation(Context context, Uri photoUri) {
	    /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor == null || cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        int orientation = cursor.getInt(0);
        cursor.close();

        return orientation;
    }

    private Bitmap loadImage(String aFile) {
        if (aFile == null || aFile.length() == 0) {
            return null;
        }

        int orientation = getOrientation(mContext, Uri.fromFile(new File(aFile)));

        // LOAD FROM FILE
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(aFile, o);

            final int REQUIRED_WIDTH = 320;
            final int REQUIRED_HIGHT = 220;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = scale;

            boolean retry = false;
            Bitmap bmp = null;
            try {
                bmp = BitmapFactory.decodeFile(aFile, options);
            } catch (OutOfMemoryError e) {
                retry = true;
                e.printStackTrace();
            }

            if (retry) {
                scale *= 2;
                options.inSampleSize = scale;
                bmp = BitmapFactory.decodeFile(aFile, options);
            }

            // CHECK RESIZE
            if (bmp != null) {
                int w = options.outWidth;
                int h = options.outHeight;

                if (w > REQUIRED_WIDTH) {
                    int v = (h * REQUIRED_WIDTH) / w;

                    Bitmap resized = Bitmap.createScaledBitmap(bmp, REQUIRED_WIDTH, v, true);
                    if (resized != null) {
                        bmp.recycle();
                        bmp = resized;
                        w = REQUIRED_WIDTH;
                        h = v;
                    }
                }

                if (h > REQUIRED_HIGHT) {
                    int v = (w * REQUIRED_HIGHT) / h;

                    Bitmap resized = Bitmap.createScaledBitmap(bmp, v, REQUIRED_HIGHT, true);
                    if (resized != null) {
                        bmp.recycle();
                        bmp = resized;
                        w = v;
                        h = REQUIRED_HIGHT;
                    }
                }


                if (orientation > 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(orientation);

                    Bitmap resized = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                    if (resized != null) {
                        bmp.recycle();
                        bmp = resized;
                    }
                }
            }

            return bmp;

        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private String getTextMessage() {
        return getTextMessage(null);
    }

    private String getTextMessage(ArrayList list) {
        String msg = mMessage.getText().toString();
        int index = 0;
        int start = 0;
        int num_chars = msg.length() - 1;
        int count = 0;
        int offset = 0;
        for (index = start; index < start + num_chars; index++) {
            char c = msg.charAt(index);
            if (c == 0xFFFF) {
                if (list != null) {
                    String key = msg.subSequence(index + 1, index + 2).toString();
                    //int length = 1;
                    try {//BJH
                        int value = Integer.parseInt(key);
                        if (value == 1 && mResult == 1)
                            value = value - 1;
                        if (value == 2 && mResult == 1)
                            value = value - 1;
                        if (value == 2 && mResult == 2)
                            value = value - 2;
                        Bitmap bmp = mImageList.get(value);
                        list.add(bmp);
                    } catch (NumberFormatException e) {
                    }
                }
                count++;
                index++;
            }
        }
        if (count > 0) {
            offset = count * 2 + 1;
        }

        if (offset > 0) {
            if (offset < msg.length())
                msg = msg.substring(offset, msg.length());
            else
                msg = "";
        }

        return msg;
    }

    private void clearImage() {
        if (mImageList != null) {
            for (Bitmap bmp : mImageList) {
                bmp.recycle();
            }
            mImageList.clear();
        }
    }

    //BJH 2016.09.26 VMS DOWNLOAD
    public static void VMSDownload(Context context, String vms_file) {
        Uri urlToDownload = Uri.parse(vms_file);
        List<String> pathSegments = urlToDownload.getPathSegments();
        request = new DownloadManager.Request(urlToDownload);
        String mFileName = pathSegments.get(pathSegments.size() - 1);
        String[] fileName = mFileName.split("-");
        mFileName = fileName[0].concat("-".concat(fileName[3]));
        request.setTitle(mFileName);
        request.setDescription(context.getResources().getString(R.string.vms_file_download));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            request.setShowRunningNotification(true);
        } else {
            request.setNotificationVisibility(request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, mFileName);
//        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
        downloadManager.enqueue(request);
    }

    private BroadcastReceiver completeReceiver = new BroadcastReceiver() {//다운로드 완료되었을 때

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, context.getResources().getString(R.string.record_file_download_ok), Toast.LENGTH_SHORT).show();
        }

    };

}
