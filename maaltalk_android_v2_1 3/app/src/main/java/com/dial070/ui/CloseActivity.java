package com.dial070.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

public class CloseActivity extends Activity {
	private static final String THIS_FILE = "MAALTALK_CLOSE";
	private Context mContext;
	private Button mButtonClose, mButtonStart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
				
		setContentView(R.layout.dial_close);

		mButtonClose = (Button) findViewById(R.id.buttonClose);
		mButtonClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		mButtonStart = (Button) findViewById(R.id.buttonStart);
		mButtonStart.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, DialMain.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_CLEAR_TASK
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		});
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
	@Override
	protected void onStop() 
    {
        super.onStop();
    }
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	protected void onUserLeaveHint() {
		//여기서 감지
		Log.d(THIS_FILE, "Home Button Touch");
		super.onUserLeaveHint();
		finish();
	}
	

}
