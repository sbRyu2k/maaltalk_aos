package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
//import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import com.dial070.maaltalk.R;
//import com.dial070.utils.AppPrefs;
import com.dial070.utils.HangulUtils;

public class SearchDataAdapter extends BaseAdapter {

	public final static int TYPE_SECTION_CALL_CENTER_HEADER = 0;
	public final static int TYPE_SECTION_CONTACT_HEADER = 1;
	public final static int TYPE_SECTION_WEB_HEADER = 2;

	public final static int TYPE_SECTION_CALL_CENTER 	= 3;
	public final static int TYPE_SECTION_CONTACT 	= 4;
	public final static int TYPE_SECTION_WEB 	 	= 5;
	
	
	private List<NaverResult> mResultData; 		
	private Context mContext;
//	private Resources mResources;
	private int mSearchCount;
	
	private ArrayAdapter<String> mHeaders;
	
	private List<SearchContactsInfo> mMatchData; 	
	private Map<String, ArrayList<ContactsData>> mSectionItemList;
	//BJH
	private List<CallCenterResult> mCallCenterResultData; 
		
//	private AppPrefs mPrefs;	
	
	public SearchDataAdapter(Context context)
	{
		mContext = context;
//		mResources = mContext.getResources();
//		mPrefs = new AppPrefs(mContext);

		mSearchCount = 0;
		mMatchData = new ArrayList<SearchContactsInfo>();
		
		mHeaders = new ArrayAdapter<String>(context, R.layout.contacts_list_header);
		//SECTION_CALL CENTER BJH
		mHeaders.add(context.getResources().getString(R.string.call_center));
		//SECTION_CONTACT
		mHeaders.add(context.getResources().getString(R.string.contact));
		//SECTION_WEB
		mHeaders.add(context.getResources().getString(R.string.portal));
	}		
	
	public void setContactData(Map<String, ArrayList<ContactsData>> data)
	{
		mSectionItemList = data;
	}
	
	public void setWebData(ArrayList<NaverResult> data)
	{
		mResultData = data;
	}
	//BJH
	public void setCallCenterData(ArrayList<CallCenterResult> data)
	{
		mCallCenterResultData = data;
	}
	
	public void setSearchCount(int searchCount)
	{
		mSearchCount = searchCount;
	}	
		
	public int getSectionPosition(int section)
	{
		int position = 0;
		if(section == TYPE_SECTION_CALL_CENTER) return position;
		else if(section == TYPE_SECTION_CONTACT) {
			if( mCallCenterResultData != null) {
				position =  mCallCenterResultData.size() + 1;
			}
		}
		else if(section == TYPE_SECTION_WEB)
		{
			if(mCallCenterResultData != null && mMatchData != null)
			{
				position = mCallCenterResultData.size() + mMatchData.size() + 2;
			}
		}
		
		return position;
	}	
	
	public int getItemType(int position)
	{
		int type = TYPE_SECTION_CALL_CENTER_HEADER;
		
		if(position == 0) return TYPE_SECTION_CALL_CENTER_HEADER;
		else if(position == mCallCenterResultData.size() + 1) return TYPE_SECTION_CONTACT_HEADER;
		else if(position ==  mCallCenterResultData.size() + mMatchData.size() + 2) return TYPE_SECTION_WEB_HEADER;
		else if(position > 0 && position <= mCallCenterResultData.size()) type = TYPE_SECTION_CALL_CENTER;
		else if(position > mCallCenterResultData.size() + 1 && position <= mCallCenterResultData.size() + mMatchData.size() + 1) type = TYPE_SECTION_CONTACT;
		else type = TYPE_SECTION_WEB;
			
		return type;
	}			
	
	
	private ContactsData get_contact_item(int offset)
	{
		if (offset < 0 || offset >= mSearchCount) return null;
			
		SearchContactsInfo search_data = mMatchData.get(offset);
		
//		ContactsData item = null;		
			
		ArrayList<ContactsData> contacts = mSectionItemList.get(search_data.section);

		if(contacts != null && contacts.size() > 0)
		{
			return contacts.get(search_data.index);
		}
		return null;

	}			
	
	private NaverResult get_web_item(int offset)
	{
		if(mResultData == null || mResultData.size() == 0) return null;
		
		if (offset < 0 || offset >= mResultData.size()) return null;
			
		return mResultData.get(offset);
	}		
	
	//BJH
	private CallCenterResult get_call_center_item(int offset)
	{
		if(mCallCenterResultData == null || mCallCenterResultData.size() == 0) return null;
		
		if (offset < 0 || offset >= mCallCenterResultData.size()) return null;
			
		return mCallCenterResultData.get(offset);
	}		
	
	//검색키에 해당하는 리스트 아이템을 개수를 구함 
	public List<SearchContactsInfo> getSearchItem(String searchKey)
	{
		if (mSectionItemList == null) return null;
		
		if(mMatchData == null) return null;
		
		//초기화 
		mMatchData.clear();
		
		if(searchKey.length() == 0) return mMatchData;
		
		String contactName;
		String findName;
		
		String sectionStr = null;
		ContactsData item = null;		
		ArrayList<ContactsData> contacts = null;
		
//		boolean searchKeyIsHangul = false;
//		if(HangulUtils.getHangulInitialSound(searchKey).length() > 0 ) searchKeyIsHangul = true;
		
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{

				if(i == mSectionItemList.size()-1) sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
				else sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[i];


			contacts = mSectionItemList.get(sectionStr);
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(contacts != null && contacts.size() > 0)
			{
				for(int j=0; j < contacts.size() ; j++)
				{
					item = contacts.get(j);
					contactName = item.getDisplayName();
					
						findName = HangulUtils.getHangulInitialSound(contactName, searchKey);
						if(findName.indexOf(searchKey) >= 0)
						{ 
							mMatchData.add(new SearchContactsInfo(sectionStr,j));
						}		

				}
			}
		}

		return mMatchData;
	}			
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//헤더사이즈 초기값
		int count = 3;
		if(mCallCenterResultData == null) return count;
		count = count + mCallCenterResultData.size();

		if(mMatchData == null) return count;
		count = count + mMatchData.size(); //mSearchCount;

		if(mResultData == null) return count;
		count = count + mResultData.size();		
		
		return count;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub

		int type = getItemType(arg0);
		
		if(type == TYPE_SECTION_CALL_CENTER_HEADER || type == TYPE_SECTION_CONTACT_HEADER || type == TYPE_SECTION_WEB_HEADER) return null;
		else if(type == TYPE_SECTION_CALL_CENTER)
		{
			return get_call_center_item(arg0 - 1);
		}
		else if(type == TYPE_SECTION_CONTACT)
		{
			return get_contact_item(arg0 - ( 2 + mCallCenterResultData.size()));
		}
		else if(type == TYPE_SECTION_WEB)
		{
			return get_web_item(arg0 - (3 + mCallCenterResultData.size() + mMatchData.size()));
		}		
		
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if( mSectionItemList == null) return null;
		
		ContactsListView contactsView = null;
		NaverListView naverView = null;
		ContactsData contact_item = null;
		NaverResult web_item = null;
		CallCenterListView callCenterView = null;
		CallCenterResult call_center_item = null;

		int type = getItemType(arg0);

		if(type == TYPE_SECTION_CALL_CENTER_HEADER) return mHeaders.getView(0, arg1, arg2);
		else if(type == TYPE_SECTION_CONTACT_HEADER) return mHeaders.getView(1, arg1, arg2);
		else if(type == TYPE_SECTION_WEB_HEADER) return mHeaders.getView(2, arg1, arg2);
		else if(type == TYPE_SECTION_CALL_CENTER)
		{
			call_center_item = get_call_center_item(arg0 - 1);
			if(arg1 == null)
			{
				callCenterView = new CallCenterListView(mContext);
			}
			else
			{
				callCenterView = (CallCenterListView) arg1;
			}
			
			if(callCenterView != null && call_center_item != null)
			{
				callCenterView.setData(call_center_item.getTitle(), call_center_item.getAddress(), call_center_item.getTelephone());	
				return callCenterView;
			}
		}
		else if(type == TYPE_SECTION_CONTACT)
		{
			contact_item = get_contact_item(arg0 - (2 + mCallCenterResultData.size()));
			if(arg1 == null)
			{
				contactsView = new ContactsListView(mContext, contact_item);
			}
			else
			{
				contactsView = (ContactsListView) arg1;
			}
			
			if(contactsView != null && contact_item != null)
			{
				contactsView.setData(contact_item.getDisplayName(), contact_item.getPhoneNumber(), contact_item.getContactId(), contact_item.getPhotoId());
				contactsView.setItem(contact_item);
				return contactsView;
			}
		}
		else if(type == TYPE_SECTION_WEB)
		{
			web_item = get_web_item(arg0 - (3 + mCallCenterResultData.size() + mMatchData.size()));
			if(arg1 == null)
			{
				naverView = new NaverListView(mContext);
			}
			else
			{
				naverView = (NaverListView) arg1;
			}
			
			if(naverView != null && web_item != null)
			{
				naverView.setData(web_item.getTitle(), web_item.getAddress(), web_item.getTelephone());	
				return naverView;
			}
		}
		return null;
	}
	
	@Override
	public int getItemViewType(int position) {

		return getItemType(position);
	}


	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		//Header and Item
		return 6;
	}	
	
	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return (getItemViewType(position) != TYPE_SECTION_CALL_CENTER_HEADER) || (getItemViewType(position) != TYPE_SECTION_CONTACT_HEADER) || (getItemViewType(position) != TYPE_SECTION_WEB_HEADER);
	}	
}
