package com.dial070.ui;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.utils.CircleImageView;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.Log;
import com.dial070.utils.PhoneNumberTextWatcher;


public class ContactsListView extends LinearLayout {
    private Context mContext;
    private CircleImageView mUserIcon;
    private TextView mUserName;
    private TextView mUserPhone;
    private TextView mUserId;
    private ContactsData mUserItem;

    public ContactsListView(Context context, ContactsData item)
    {
        super(context);
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.contacts_list_item, this, true);
        mUserIcon = findViewById(R.id.contacts_user_icon);
        mUserName = (TextView) findViewById(R.id.contacts_user_name);
        mUserPhone = (TextView) findViewById(R.id.contacts_user_phone);
        PhoneNumberTextWatcher digitFormater = new PhoneNumberTextWatcher();
        mUserPhone.addTextChangedListener(digitFormater);
        mUserId = (TextView) findViewById(R.id.contacts_user_id);
        this.mUserItem = item;
    }

    private static Drawable imagePhoto = null;
    public void setData(String user_name, String phone_number, String user_id, String photo_id)
    {
        mUserName.setText(user_name);
        mUserPhone.setText(Html.fromHtml(phone_number));
        mUserId.setText(user_id);

        if (imagePhoto == null)
            imagePhoto = getResources().getDrawable(R.drawable.ic_contact_default_photo);


        if(user_id == null)
        {
            mUserIcon.setImageDrawable(imagePhoto);
        }
        else
        {
            /*Uri u = ContactHelper.getPhotoUriByPhoneNumber(mContext, phone_number);
            if (u != null) {*/
            if(photo_id != null) {
                //mUserIcon.setImageURI(u);
                Log.i("ContactListView","photo_id:"+photo_id);
                mUserIcon.setImageURI(Uri.parse(photo_id));
            } else {
                mUserIcon.setImageDrawable(imagePhoto);
            }
        }

//		if(photo_id == null)
//			mUserIcon.setImageResource(R.drawable.default_user_photo);
//		else
//			mUserIcon.setImageDrawable(ContactHelper.getPhoto(mContext,user_id,true));
    }

    public TextView getUserName()
    {
        return mUserName;
    }

    public TextView getUserPhone()
    {
        return mUserPhone;
    }

    public void setItem(ContactsData item)
    {
        mUserItem = item;
    }

    public ContactsData getItem()
    {
        return mUserItem;
    }

    public void setUserIcon(Drawable icon)
    {
        mUserIcon.setImageDrawable(icon);
    }


}
