package com.dial070.ui;

public class ScoreItem {

	private String UserName;
	private String PhoneNumber;
	private int TotalScore,CallCountScore,ZoneScore,CallStartScore,CallDurScore,CallStateScore,ConAddScore,FavAddScore;
	
	//	이름 번호 총점 통화수 통화시간 즐겨찾기 주소록 통화상태 거리 통화시작
	//displayName,phoneNumber,score,zone_score,call_count_score,call_start_score,call_dur_score,call_state_score,con_add_score,fav_add_score
	public ScoreItem(String name,String number,int total_score, int count_score, int zone_score, int start_score, int dur_score, int state_score, int con_score, int fav_score) {
		// TODO Auto-generated constructor stub
		UserName = name;
		PhoneNumber = number;
		TotalScore = total_score;
		CallCountScore = count_score;
		ZoneScore = zone_score;
		CallStartScore = start_score;
		CallDurScore = dur_score;
		CallStateScore = state_score;
		ConAddScore = con_score;
		FavAddScore = fav_score;
	}

	public String getName()
	{
		return UserName;
	}
	
	public String getNumber()
	{
		return PhoneNumber;
	}
	
	public int getTotalScore()
	{
		return TotalScore;
	}
	
	public int getCountScore()
	{
		return CallCountScore;
	}

	public int getZoneScore()
	{
		return ZoneScore;
	}
	
	public int getStartScore()
	{
		return CallStartScore;
	}
	
	public int getDurScore()
	{
		return CallDurScore;
	}
	
	public int getStateScore()
	{
		return CallStateScore;
	}
	
	public int getConScore()
	{
		return ConAddScore;
	}
		
	public int getFavScore()
	{
		return FavAddScore;
	}	
}
