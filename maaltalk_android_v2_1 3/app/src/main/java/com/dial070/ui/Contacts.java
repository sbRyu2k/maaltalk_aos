package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.*;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.utils.Compatibility;
import com.dial070.widgets.IndexScrollView;
import com.dial070.widgets.IndexScrollView.OnScrollChangeListener;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import com.dial070.db.FavoritesData;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;


public class Contacts extends AppCompatActivity implements OnScrollChangeListener {
//	private static final String THIS_FILE = "Contacts";	
	private final static int ADD_CONTACTS = 1;
	
	private final static int DIAL_CONTACTS = 0;
//	private final static int FAVORITE_CONTACTS = 1;
	
//	private static final int NORMAL_COUNTRY_COLOR = 0x005ca2cd;
//	private static final int PRESSED_COUNTRY_COLOR = 0xff5ca2cd;
	
	private Context mContext;
	private EditText mContactsSearch;
	private ListView mContactsListView;
	private IndexScrollView mScrollView;
	private ImageButton mAddContacts;
	
	//국제전화용 아답터 필터링 (전화번호 조회)
	private ContactsDataAdapter mAdapter;
	private ContactsDataSearchAdapter mSearchAdapter;
	
	private InputMethodManager mImm;
	private ContactsData mContactsItem;
	private String oldSearchKey;
	private String searchKey;	
	private boolean mSearchMode;
//	private int mFavoritesPosition;
	private int mSelectType;

	private String mType;
	
//	private AppPrefs mPrefs;	
	
	/** Called when the activity                                                                                                                                                                                                                                                                                                                                                                                                                                           is first created. */
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final androidx.appcompat.app.ActionBar abar = getSupportActionBar();
		androidx.appcompat.app.ActionBar.LayoutParams params = new androidx.appcompat.app.ActionBar.LayoutParams(//Center the textview in the ActionBar !
				androidx.appcompat.app.ActionBar.LayoutParams.WRAP_CONTENT,
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				Gravity.CENTER);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(true);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(getString(R.string.contact));

		setContentView(R.layout.contacts_activity);
		mContext = this;
//		mPrefs = new AppPrefs(this);
		
		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
	
		mContactsListView = (ListView) findViewById(R.id.contacts_list_view);
		
		mAddContacts = (ImageButton) findViewById(R.id.btn_add_contacts);
		mContactsSearch = (EditText) findViewById(R.id.contacts_search);
		mScrollView = (IndexScrollView) findViewById(R.id.indexscrollview);
		mScrollView.setOnScrollChangeListener(this);
		//setEditableSearch(false);

		mContactsItem = null;
		oldSearchKey = "";	
		searchKey = "";
		mSearchMode = false;
		
		//For 즐겨찾기 
		Intent intent = getIntent();
//		mFavoritesPosition = intent.getIntExtra("Position", -1);		
			
		//select type 0:contact 1:favorites
		mSelectType = intent.getIntExtra("SelectType", -1);
		mType = intent.getStringExtra("Type");
		
		//국제전화용 아답터 필터링 (전화번호 조회)
		//////////////////////////////////////////////
		mAdapter = new ContactsDataAdapter(this);


		
		mContactsSearch.setOnClickListener( new View.OnClickListener() {
			
			//@Override
			public void onClick(View v) {
				//setEditableSearch(true);
			}
		});
		
		
		try {
			mContactsSearch.addTextChangedListener(new TextWatcher() {

				//@Override
				public void afterTextChanged(Editable s) {
					// 초성 검색 
					try {
						
						searchKey = mContactsSearch.getText().toString();
						
						if(!oldSearchKey.equalsIgnoreCase(searchKey))
						{
							oldSearchKey = searchKey;
							doSearch();
						}
						

					} catch (Exception e) {
						Log.e("ContactsActivity",e.getMessage(),e);
					}
					
				}

				//@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					//do nothing
				}

				//@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					//do nothing
				}
				
			});
		} catch (Exception e) {
			//
		}

		

		mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			//@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(mSearchMode)
					mContactsItem = (ContactsData) mSearchAdapter.getItem(arg2);
				else
					mContactsItem = (ContactsData) mAdapter.getItem(arg2);
				

				if(mContactsItem != null)
				{
					ArrayList<PhoneItem> phones = mContactsItem.getUserPhoneList();
					ArrayList<ContactsPhoneList> phoneList = new ArrayList<ContactsPhoneList>();
					
					PhoneItem phoneItem;
					//Log.i("ContactsActivity","phones.size():"+phones.size());
					for(int i = 0; i < phones.size(); i++)
					{
						phoneItem = phones.get(i);
						ContactsPhoneList item = new ContactsPhoneList(mContactsItem.getContactId(),phoneItem.PhoneType, phoneItem.PhoneNumber);
						phoneList.add(item);
					}

					
					String alertDialogTitle ="";
					String alertDialogDesc = "";
					//연락처에 이름만 등록된 경우 
					if(phoneList.size() == 0)
					{
							
						if (mSelectType == DIAL_CONTACTS)
						{
							alertDialogTitle = mContext.getResources().getString(R.string.select_contact);
							alertDialogDesc = mContext.getResources().getString(R.string.no_exist_contact_info);
						}
						else
						{
							alertDialogTitle = mContext.getResources().getString(R.string.add_favorite);
							alertDialogDesc = mContext.getResources().getString(R.string.no_exist_contact_info);							
						}
						AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
						msgDialog.setTitle(alertDialogTitle);
						msgDialog.setMessage(alertDialogDesc);
						msgDialog.setButton(Dialog.BUTTON_POSITIVE,mContext.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
							//@Override
							public void onClick(DialogInterface dialog, int which) {
								//do nothing
								return;
							}
						});	

						msgDialog.show();	
						setResult(RESULT_CANCELED);
						return;
					}
					
					if(phoneList.size() == 1)
					{
						if (ServicePrefs.mUseMSG && mType!=null && mType.equals("POPUP")) {
							dialogSelect(mContactsItem.getDisplayName(), mContactsItem.getPhoneNumber());
						}else{
							Intent intent = new Intent();
							if (mSelectType == DIAL_CONTACTS)
							{
								intent.putExtra("PhoneNumber", mContactsItem.getPhoneNumber());
								intent.putExtra("displayName", mContactsItem.getDisplayName());
							}
							else
							{
								intent.putExtra(FavoritesData.FIELD_DISPLAYNAME, mContactsItem.getDisplayName());
								intent.putExtra(FavoritesData.FIELD_PHONENUMBER, mContactsItem.getPhoneNumber());
								intent.putExtra(FavoritesData.FIELD_UID, mContactsItem.getContactId());
								intent.putExtra(FavoritesData.FIELD_IDENTIFIER, mContactsItem.getPhotoId());
								intent.putExtra(FavoritesData.FIELD_SEARCH_NUMBER, mContactsItem.getPhoneNumber());
								intent.putExtra("PhotoId", mContactsItem.getPhotoId());
							}

							setResult(RESULT_OK,intent);
							finish();
						}
					}
					else
					{
					    final String[] phoneNums = new String[phoneList.size()];
					    for (int i = 0; i < phoneList.size(); i++) {
					    	phoneItem = phones.get(i);
					    	phoneNums[i] = phoneItem.PhoneType + "  " + phoneItem.PhoneNumber;
					    }
						
						if (mSelectType == DIAL_CONTACTS)
						{
							alertDialogTitle = mContext.getResources().getString(R.string.choose_contact);
						}
						else
						{
							alertDialogTitle = mContext.getResources().getString(R.string.choose_add_contact);						
						}					    
					    
					    AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
					    dialog.setTitle(alertDialogTitle);
					    dialog.setSingleChoiceItems(phoneNums, 0,
					        new DialogInterface.OnClickListener() {

					            //@Override
					            public void onClick(DialogInterface dialog,
					                int which) {
					            // get selected item and close the dialog
					            String selectedNumber = phoneNums[which];

							    /*Intent intent = new Intent();
								if (mSelectType == DIAL_CONTACTS)
								{
									intent.putExtra("PhoneNumber", mContactsItem.getPhoneNumber());
									intent.putExtra("displayName", mContactsItem.getDisplayName());
								}
								else
								{
									intent.putExtra(FavoritesData.FIELD_UID, mContactsItem.getContactId());
									intent.putExtra(FavoritesData.FIELD_IDENTIFIER, mContactsItem.getPhotoId());
									intent.putExtra(FavoritesData.FIELD_DISPLAYNAME, mContactsItem.getDisplayName());
									intent.putExtra(FavoritesData.FIELD_PHONENUMBER, selectedNumber);

									intent.putExtra(FavoritesData.FIELD_FIELD1, selectedNumber);
									intent.putExtra("PhotoId", mContactsItem.getPhotoId());				
								}

								setResult(RESULT_OK,intent);
								finish();*/

                                    if (mSelectType == DIAL_CONTACTS)
                                    {
										dialogSelect(mContactsItem.getDisplayName(), mContactsItem.getPhoneNumber());
                                    }else{
										dialogSelect(mContactsItem.getDisplayName(), selectedNumber);
                                    }
					            }
					        });						
						dialog.setNeutralButton(mContext.getResources().getString(R.string.cancel), null);
						AlertDialog alert = dialog.create();
						alert.show();
					}
				}
				else
				{
					setResult(RESULT_CANCELED);
					finish();		
				}
		
			}
			
		});


		mAddContacts.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 새 주소록 추가 
				Intent intent = new Intent(Intent.ACTION_INSERT);
				intent.setData(Uri.parse("content://com.android.contacts/contacts"));
				try {
					//BJH 2016.06.17 주소록 추가 후에도 연락처가 보이도록
					startActivityForResult(intent,ADD_CONTACTS);
					//startActivity(intent);
				}
				catch(ActivityNotFoundException e)
				{
					
				}
			}
		});
		
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mContactsListView!=null){
			final int index = mContactsListView.getFirstVisiblePosition();
			View v = mContactsListView.getChildAt(0);
			final int top = (v == null) ? 0 : (v.getTop() - mContactsListView.getPaddingTop());

			//필터링시
			//boolean prefix = true;
			//BJH 연락처 등록 바로 반영 2016.5.26
			ContactsDataContainer.clearContactsList();
			mAdapter.setData(ContactsDataContainer.getFiteringData(this));
			mContactsListView.setAdapter(mAdapter);
			if (Compatibility.getApiLevel() > 10)
				mContactsListView.setSelector(R.drawable.list_selector);
			mContactsListView.setFocusableInTouchMode(true);
			mContactsListView.setClickable(true);
			//mContactsListView.setCacheColorHint(Color.TRANSPARENT);

			mSearchAdapter = new ContactsDataSearchAdapter(this);
			mSearchAdapter.setData(ContactsDataContainer.getFiteringData(this));

			mContactsListView.post(new Runnable() {
				@Override
				public void run() {
					mContactsListView.setSelectionFromTop(index, top);
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		if (mType!=null && mType.equals("POPUP")){
			DialMain.mainBackPressed(mContext);
		}else {
			super.onBackPressed();
		}
	}

	private void dialogSelect(String name, final String number){
		final String user_phone = number;
		final String user_name = name;

		ArrayList<String> listMenu = new ArrayList<>();
		listMenu.add(getString(R.string.popup_call_call));
		if(ServicePrefs.mUseMSG){
			listMenu.add(getString(R.string.popup_call_sms));
		}

		final String[] menus = listMenu.toArray(new String[listMenu.size()]);

		String title;
		if(name != null && name.length() > 16) title = name.substring(0,16);

		if (name != null && name.length() > 0)
			title=name;
		else
			title=number;

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle(title);
		adb.setItems(menus, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String number=user_phone;
				if (menus[which].equals(getString(R.string.popup_call_call))){

					if (number.trim().startsWith("+")){
						number="+"+number.trim().replaceAll("[^0-9#]", "");
					}else{
						number=number.trim().replaceAll("[^0-9#]", "");
					}

					if(number != null && SipUri.isPhoneNumber(number))
					{
						makeCall(number);
						dialog.cancel();
					}
				}else if (menus[which].equals(getString(R.string.popup_call_sms))){
					if(number != null && SipUri.isPhoneNumber(number))
					{
						goSMSWrite(number, user_name);
						dialog.cancel();
					}
				}
			}
		});

		adb.show();
	}

	private void makeCall(String number)
	{
		Log.d("Contacts","number:"+number);

		//전화걸기
		if (number != null && SipUri.isPhoneNumber(number) )
		{
            Intent intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
            intent.putExtra(SipManager.EXTRA_CALL_NUMBER, number);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
		}else{
			new MaterialAlertDialogBuilder(mContext)
					.setTitle(mContext.getResources().getString(R.string.make_gsm_call))
					.setMessage(mContext.getResources().getString(R.string.wrong_number))
					.setPositiveButton(mContext.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {

						}
					})
					.show();
		}
	}

	private void goSMSWrite(String number, String name) {
		if(number != null && number.length() > 0) {
			number = number.replace("-", "");
			Intent intent = new Intent(this, SmsWrite.class);
			if(number.startsWith("00182"))
				number = number.replace("00182", "0");
			intent.putExtra("phone", number);
			intent.putExtra("name", name);
			mContext.startActivity(intent);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	private boolean doSearch()
	{
		if(searchKey.length() == 0) 
		{
			//setEditableSearch(false);
			mSearchMode = false;
			mContactsListView.setAdapter(mAdapter); 
			mAdapter.notifyDataSetChanged();

			return true;
		}
		
		//국제전화용 조회 필터링 (전화번호 조회)
		//////////////////////////////////////////////
		List<SearchContactsInfo> contactsSearchList = null;
		
		
		mSearchMode = true;

		contactsSearchList = mSearchAdapter.getSearchItem(searchKey);
		if(contactsSearchList != null)
		{
			mSearchAdapter.setSearchCount(contactsSearchList.size());
			mSearchAdapter.notifyDataSetChanged();
		}
		
		mContactsListView.setAdapter(mSearchAdapter);   

		return true;
	}	
	
	//@Override
	public void onTrigger(String section) {
		// TODO Auto-generated method stub
		if(searchKey.length() == 0) 
		{
			int position = mAdapter.getSectionPosition(section);
			if (position >= 0)
				mContactsListView.setSelection(position);
		}			
	}
//	private void setEditableSearch(boolean editable)
//	{
//		
//		if(editable)
//		{
//			mContactsSearch.setFocusableInTouchMode(editable);
//			mContactsSearch.requestFocus();
//			mImm.showSoftInput(mContactsSearch, 0);
//		}
//		else
//		{
//			mImm.hideSoftInputFromWindow(mContactsSearch.getWindowToken(), 0);
//			mContactsSearch.setFocusableInTouchMode(editable);
//			mContactsSearch.setFocusable(editable);
//		}
//		mContactsSearch.setLongClickable(editable);		
//
//	}
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		mImm.hideSoftInputFromWindow(mContactsSearch.getWindowToken(), 0);
		super.onPause();
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			//신규 연락처 등록 
			case ADD_CONTACTS:
				/*if (resultCode == RESULT_OK)
				{
					//BJH 2016.06.17 주소록 추가 후에도 연락처가 보이고 검색가능하도록
					ContactsDataContainer.clearContactsList();
					mAdapter.setData(ContactsDataContainer.getFiteringData(this));
					mContactsListView.setAdapter(mAdapter);   
					mAdapter.notifyDataSetChanged();
					mSearchAdapter.setData(ContactsDataContainer.getFiteringData(this));
					mSearchAdapter.notifyDataSetChanged();
				}*/ //BJH 2017.03.02 주소록 추가 후에도 연락처가 보이고 검색가능하도록
				ContactsDataContainer.clearContactsList();
				mAdapter.setData(ContactsDataContainer.getFiteringData(this));
				mContactsListView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
				mSearchAdapter.setData(ContactsDataContainer.getFiteringData(this));
				mSearchAdapter.notifyDataSetChanged();
				break;
			default:
				break;
		}
		return;
	}		


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.contact_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub		
		switch(item.getItemId())
		{
		case R.id.action_add:
		{
			// 새 주소록 추가 
			Intent intent = new Intent(Intent.ACTION_INSERT);
			intent.setData(Uri.parse("content://com.android.contacts/contacts"));
			try {
				startActivityForResult(intent,ADD_CONTACTS);
			}
			catch(ActivityNotFoundException e)
			{

			}
		}
			return true;
		default:
			break;
		}

		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
	
//		if(menu != null)
//		{
//			menu.clear();
//			menu.add(Menu.NONE, MENU_OPTION_ADD, Menu.NONE, R.string.opt_menu_contact_add).setIcon(
//					R.drawable.ic_menu_edit);	
//		}

		return true;
	}
	
}
