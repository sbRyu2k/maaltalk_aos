package com.dial070.ui;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.oauth.OauthLoginActivity;
import com.dial070.service.Fcm;
import com.dial070.service.MessageService;
import com.dial070.service.StartForegroundService;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.CustomProgress;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.dial070.utils.OnSingleClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.opencsv.CSVWriter;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

import static org.apache.http.params.HttpConnectionParams.setConnectionTimeout;
import static org.apache.http.params.HttpConnectionParams.setSoTimeout;

public class Setting extends AppCompatActivity {
    public static final String KAKAO_PACKAGE_NAME = "com.kakao.talk";
    public static final String MAALTALK_DATABASE_ROOT = "/data/data/com.dial070.maaltalk/databases/com.dail070.database"; //BJH 2016.11.10
    private static final String THIS_FILE = "MAALTALK_SETTING";
    private static final int SELECT_PAYMENT = 1;
    private static final int SELECT_MYINFO = 2;
    private static final int SELECT_USER_MSG = 3;// BJH
    private static final int SELECT_SVC_OUT = 4;// BJH
    private final static int SELECT_SEARCH = 5; // BJH
    private final static int SELECT_RINGTONE = 6; // BJH
    private final static int SELECT_BRIDGE = 7; // BJH 2016.10.11 사장님 요청 사항
    private final static int SELECT_USIM = 8;
    private final static int SELECT_OVERLAY_PERMISSION = 9; // BJH 2017.03.22 android.permission.SYSTEM_ALERT_WINDOW
    private final static int SELECT_REQ_MYINFO = 10; // BJH 2017.05.29
    private final static int SELECT_NETWORK_STATS = 11; // sgkim 2017.08.21
    // BJH
    public static String lookupIp;
    private static LinearLayout mPushDB;
    DialogInterface mPopupDlg = null;
    private Context mContext;
    private AppPrefs mPrefs;
    private PreferencesWrapper mPrefsWrapper;
    private PreferencesWrapper mSysPrefs;
    private Switch swForeground, swSensor, swNetwork, swLteMode, swEcho;
    private TextView mMyPhone, mVersion, mSendVal, mRecvVal,
            mVerDesc;// BJH
    private LinearLayout mMyNumber, mQuit, mDial070, mHelp, mNotice, mFaq,
            mMyInfoLayout, mBoard, mKakao, mTerms, mSendVolumn, mRecvVolumn,
            mMyPointLayout, mLteModeLayout, mMySvrLayout, mRate, mPayment,
            mUserMsg, mNewVersion, mSvcOut, mUsimPocket, mRingTone, mBridge, mNetworkStat, mRejectCallMsg, mLayoutArsTimeSetting;
    private View viewDividerLineLteMode, viewLineArsTimeSetting;
    private int mSendSpeakerLevel, mSendMicLevel;
    private String mVer = null;
    private androidx.appcompat.app.AlertDialog pDialog;
    private HttpURLConnection mConnection = null;
    private ProgressDialog progressDialog = null;

    private static void Alert(Context context, String msg) {
        new MaterialAlertDialogBuilder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(context.getResources().getString(R.string.close),null)
                .show();
    }

    public static String postCSV(Context context, File csv, String qna) {
        if(qna != null && qna.length() > 0)
            qna = qna.replaceAll("(\\r\\n|\\r|\\n|\\n\\r)", "<br>");

        AppPrefs mPrefs = new AppPrefs(context);
        String post_url = mPrefs.getPreferenceStringValue(AppPrefs.URL_PUSH_RECORD);
        String user_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();


        HttpPost httpPost = new HttpPost(post_url);

        try {
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            Log.i(THIS_FILE,"qna:"+qna);
            Log.i(THIS_FILE,"csv:"+csv.getAbsolutePath());

            nameValuePairs.add(new BasicNameValuePair("name", user_id));
            nameValuePairs.add(new BasicNameValuePair("qna", qna));
            nameValuePairs.add(new BasicNameValuePair("os_type", "ANDROID"));
            nameValuePairs.add(new BasicNameValuePair("os_ver", android.os.Build.VERSION.SDK));
            nameValuePairs.add(new BasicNameValuePair("app_ver", ServicePrefs.getLoginVersion()));
            nameValuePairs.add(new BasicNameValuePair("device_model", ServicePrefs.getModel()));

            entity.addPart("userfile", new FileBody(csv));

            for(int index=0; index < nameValuePairs.size(); index++) {
                entity.addPart(nameValuePairs.get(index).getName(), new StringBody(nameValuePairs.get(index).getValue(), Charset.forName("UTF-8")));
            }

            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost, localContext);
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            Log.d(THIS_FILE, "HTTPS POST EXEC : " + sb.toString());

            // CHECK STATUS CODE
            if (response.getStatusLine().getStatusCode() != 200) {
                Log.e(THIS_FILE, "HTTPS POST STATUS : "
                        + response.getStatusLine().toString());
                return null;
            }

            return sb.toString();
        } catch (IOException e) {
            Log.e(THIS_FILE, "error: " + e.getMessage(), e);
        }
        return null;
    }

    public static void setPushLayout() { //BJH 2016.12.14
        if (mPushDB == null)
            return;
        if (mPushDB.getVisibility() == View.GONE)
            mPushDB.setVisibility(View.VISIBLE);
    }

    public static String postServer(Context context, String url, List<NameValuePair> nameValuePairs) {
        Log.i(THIS_FILE,"postServer:"+url);
        Log.d(THIS_FILE, "nameValuePairs" + nameValuePairs);
        if (url == null || url.length() == 0)
            return "";
        if (nameValuePairs == null)
            return "";

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            setConnectionTimeout(params, 20000);
            setSoTimeout(params, 20000);

            HttpPost httppost = new HttpPost(url);

            // Add your data
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
                    HTTP.UTF_8));
            // Execute HTTP Post Request
            // Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            Log.d(THIS_FILE, "HTTPS POST EXEC : " + sb.toString());

            // CHECK STATUS CODE
            if (response.getStatusLine().getStatusCode() != 200) {
                Log.e(THIS_FILE, "HTTPS POST STATUS : "
                        + response.getStatusLine().toString());
                return null;
            }

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;

    }

    public void setTitle(String title){
        View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
        final androidx.appcompat.app.ActionBar abar = getSupportActionBar();
        androidx.appcompat.app.ActionBar.LayoutParams params = new androidx.appcompat.app.ActionBar.LayoutParams(//Center the textview in the ActionBar !
                androidx.appcompat.app.ActionBar.LayoutParams.WRAP_CONTENT,
                androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        txtTitle.setText(title);
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setHomeButtonEnabled(true);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setTitle(getString(R.string.setting));

        setContentView(R.layout.dial_setting_v3);

        mContext = this;
        mPrefs = new AppPrefs(this);
        mSysPrefs = new PreferencesWrapper(this);
        mPrefsWrapper = new PreferencesWrapper(this);

        preConfirmMyinfo();

        mMyPhone = (TextView) findViewById(R.id.txt_my_phone);
        mVersion = (TextView) findViewById(R.id.txt_version);
        mVerDesc = (TextView) findViewById(R.id.txt_version_desc);// BJH

        swNetwork=findViewById(R.id.swNetwork);
        swLteMode=findViewById(R.id.swLteMode);
        swEcho = findViewById(R.id.swEcho);

        swForeground=findViewById(R.id.swForeground);
        swForeground.setChecked(mPrefs
                .getPreferenceBooleanValue(AppPrefs.START_FOREGROUND));

        // BJH 센서 On/Off 체크
        swSensor=findViewById(R.id.swSensor);
        swSensor.setChecked(mPrefs
                .getPreferenceBooleanValue(AppPrefs.SENSOR_ENABLE));

        swNetwork.setChecked(mSysPrefs
                .getPreferenceBooleanValue(PreferencesWrapper.USE_MOBILE));

        mLteModeLayout = (LinearLayout) findViewById(R.id.layout_lte_mode);
        viewDividerLineLteMode = findViewById(R.id.viewDividerLineLteMode);
        swLteMode.setChecked(mSysPrefs
                .getPreferenceBooleanValue(PreferencesWrapper.USE_LTE_MODE));

        if (swNetwork.isChecked()) {
            mLteModeLayout.setVisibility(View.VISIBLE);
            viewDividerLineLteMode.setVisibility(View.VISIBLE);
        }
        else {
            mLteModeLayout.setVisibility(View.GONE);
            viewDividerLineLteMode.setVisibility(View.GONE);
        }

        swEcho.setChecked(mSysPrefs
                .getPreferenceBooleanValue(PreferencesWrapper.ECHO_CANCELLATION));


        mLayoutArsTimeSetting=findViewById(R.id.layout_ars_time_setting);
        mLayoutArsTimeSetting.setClickable(true);
        mLayoutArsTimeSetting.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_USER_TIME);
                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                Log.i(THIS_FILE,"url:"+url);
                if (url == null || url.length() == 0)
                    return;
                if (uid == null || uid.length() == 0)
                    return;
                String urlString = String.format("%s?name=%s", url, uid);
                Intent intent = new Intent(Setting.this, WebClient.class);
                intent.putExtra("url", urlString);
                intent.putExtra("title",
                        getResources().getString(R.string.title_ars_time_setting));
                startActivity(intent);
            }
        });

        viewLineArsTimeSetting = findViewById(R.id.viewLineArsTimeSetting);

        String distributor_id = mPrefs.getPreferenceStringValue(AppPrefs.DISTRIBUTOR_ID);
        if(distributor_id != null && distributor_id.length() > 0 && (distributor_id.startsWith("office") || distributor_id.startsWith("nowon") || distributor_id.startsWith("lottechilsung"))){
            mLayoutArsTimeSetting.setVisibility(View.VISIBLE);
            viewLineArsTimeSetting.setVisibility(View.VISIBLE);
        }else {
            mLayoutArsTimeSetting.setVisibility(View.GONE);
            viewLineArsTimeSetting.setVisibility(View.GONE);
        }


        mMyPointLayout = (LinearLayout) findViewById(R.id.layout_mypoint);
        mMyPointLayout.setClickable(true);
        mMyPointLayout.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE);
                if (ServicePrefs.mPayType != null
                        && ServicePrefs.mPayType.equalsIgnoreCase("1"))
                    url = mPrefs.getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA);

                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                if (ServicePrefs.DIAL_PAY_TEST)
                    url = "http://110.45.220.168/maaltalk/order_mobile_app_v2.php";

                if (url == null || url.length() == 0)
                    return;
                if (uid == null || uid.length() == 0)
                    return;

                String urlString = String.format("%s?name=%s&refresh=%s", url, uid,System.currentTimeMillis()+"");
                Intent intent = new Intent(Setting.this, PayClient.class);
                intent.putExtra("url", urlString);
                intent.putExtra("title",
                        getResources().getString(R.string.title_my_point));
                startActivityForResult(intent, SELECT_PAYMENT);
            }
        });

		/*
         * BJH 2016.08.02 USIM/POCKET WIFI
		 */
        mUsimPocket = (LinearLayout) findViewById(R.id.layout_usim_pocket);
        mUsimPocket.setClickable(true);
        mUsimPocket.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI);

                if (url == null || url.length() == 0)
                    return;

				Intent intent = new Intent(Setting.this, PayClient.class);
				intent.putExtra("url", url);
				intent.putExtra("title",
						getResources().getString(R.string.title_usim_pocket));
				startActivityForResult(intent, SELECT_USER_MSG);
                /*try {
                    Intent intent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
                }*/
            }
        });

		/*
		 * BJH 2016.08.02 USIM/POCKET WIFI
		 */
		/*mBridge = (LinearLayout) findViewById(R.id.layout_bridge);
		mBridge.setClickable(true);
		mBridge.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = mPrefs
						.getPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT);

				String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

				if (url == null || url.length() == 0)
					return;
				if (uid == null || uid.length() == 0)
					return;

				String urlString = String.format("%s?name=%s", url, uid);
				Intent intent = new Intent(Setting.this, PayClient.class);
				intent.putExtra("url", urlString);
				intent.putExtra("title",
						getResources().getString(R.string.title_bridge_payment));
				startActivityForResult(intent, SELECT_BRIDGE);
			}
		});*/

        mMyInfoLayout = (LinearLayout) findViewById(R.id.layout_myinfo);
        mMyInfoLayout.setClickable(true);
        mMyInfoLayout.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                /*String mt_res_myinfo = mPrefs.getPreferenceStringValue(AppPrefs.MT_RES_MYINFO); // BJH 2017.05.29
                if (mt_res_myinfo != null && mt_res_myinfo.length() > 0 && mt_res_myinfo.equals("Y")) {
                    //String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION);
                    String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
                    String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                    if (url == null || url.length() == 0)
                        return;
                    if (uid == null || uid.length() == 0)
                        return;

                    String urlString = String.format("%s?name=%s", url, uid);
                    Intent intent = new Intent(Setting.this, PayClient.class);
                    intent.putExtra("url", urlString);
                    intent.putExtra("title",
                            getResources().getString(R.string.title_myinfo));
                    intent.putExtra("popup", true);// BJH 나의 정보에서 사용자별 설명서로 이동하므로
                    startActivityForResult(intent, SELECT_USER_MSG);
                } else {
                    Intent intent = new Intent(Setting.this, OauthLoginActivity.class);
                    startActivityForResult(intent, SELECT_REQ_MYINFO);
                }*/
                if (App.isOauth){
                    String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
                    String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                    if (url == null || url.length() == 0)
                        return;
                    if (uid == null || uid.length() == 0)
                        return;

                    String urlString = String.format("%s?name=%s", url, uid);

                    DebugLog.d("test_my_info url: "+urlString);

                    Intent intent = new Intent(Setting.this, PayClient.class);
                    intent.putExtra("url", urlString);
                    intent.putExtra("title",
                            getResources().getString(R.string.title_myinfo));
                    intent.putExtra("popup", true);// BJH 나의 정보에서 사용자별 설명서로 이동하므로
                    startActivityForResult(intent, SELECT_USER_MSG);
                }else {
                    cofirmMyinfo();
                }
            }
        });

        // BJH Google DNS 외국일 때만 서버 변경할 수 있게
        mMySvrLayout = (LinearLayout) findViewById(R.id.layout_mysvr);
        mMySvrLayout.setClickable(true);
        mMySvrLayout.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                // resolve();
                // resoloveServer();
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Setting.this);
                //LayoutInflater inflater = Setting.this.getLayoutInflater();
                //View view_svr = inflater.inflate(R.layout.svr_dialog, null);
                //builder.setView(view_svr);
                builder.setMessage(getString(R.string.conts_my_svr_check));
                builder.setPositiveButton(
                        getResources().getString(R.string.change),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO Auto-generated method stub
                                ServicePrefs.DNSLookUp(mContext);
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                dialog.cancel();
                            }
                        });
                builder.setTitle(mContext.getResources().getString(
                        R.string.title_my_svr));
				/*
				 * CheckBox mCheckSvr = (CheckBox) view_svr
				 * .findViewById(R.id.checkbox_svr); mCheckSvr.setChecked(mPrefs
				 * .getPreferenceBooleanValue(AppPrefs.FOREIGN_USE_CHECK));
				 * mCheckSvr.setOnClickListener(new View.OnClickListener() {
				 *
				 * @Override public void onClick(View v) { // TODO
				 * Auto-generated method stub CheckBox check = (CheckBox) v; if
				 * (check.isChecked()) { mPrefs.setPreferenceBooleanValue(
				 * AppPrefs.FOREIGN_USE_CHECK, true);
				 *
				 * } else { mPrefs.setPreferenceBooleanValue(
				 * AppPrefs.FOREIGN_USE_CHECK, false); } } });
				 */
                builder.show();
            }
        });

        // 내 전화번호
        mMyNumber = (LinearLayout) findViewById(R.id.layout_mynumber);
        if (!ServicePrefs.DIAL_TEST) {
            mMyNumber.setClickable(true);
            mMyNumber.setOnClickListener(new OnSingleClickListener() {

                @Override
                public void onSingleClick(View v) {
                    // TODO Auto-generated method stub

                    if (!ServicePrefs.DIAL_RETAIL) {
                        Resources res = mContext.getResources();

                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(res.getString(
                                R.string.title_auth_phone));
                        builder.setMessage(res
                                .getString(R.string.question_auth_phone));
                        builder.setPositiveButton(
                                getResources().getString(R.string.yes),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        doAuth();

                                        return;
                                    }
                                });
                        builder.setNegativeButton(
                                getResources().getString(R.string.no),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                        builder.show();

                    } else {
                        return;
                    }
                }
            });
        }

        //BJH 2017.01.20 종량제 요율 정보
        mRate = (LinearLayout) findViewById(R.id.layout_rate);
        mRate.setClickable(true);
        mRate.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_RATE);
                if (url == null || url.length() == 0)
                    return;
                Intent intent = new Intent(Setting.this, WebClient.class);
                intent.putExtra("url", url);
                intent.putExtra("title",
                        getResources().getString(R.string.title_rate));
                startActivity(intent);
            }

        });
        // 자동 로그인 설정
		/*
		 * mAutoLogin.setOnClickListener(new View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub CheckBox check = (CheckBox)v; if(check.isChecked())
		 * mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true); else
		 * mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, false); } });
		 */

        // 네트워크 설정
        swNetwork.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // 3G/LTE에서도 사용하도록 함.
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.USE_MOBILE, true);

                    mLteModeLayout.setVisibility(View.VISIBLE);
                    viewDividerLineLteMode.setVisibility(View.VISIBLE);

                } else {
                    // WiFi에서만 사용하도록 함.
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.USE_MOBILE, false);

                    swLteMode.setChecked(false);
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.USE_LTE_MODE, false);

                    mLteModeLayout.setVisibility(View.GONE);
                    viewDividerLineLteMode.setVisibility(View.GONE);

                    if (SipService.currentService != null)
                        SipService.currentService.wifiConfigChanged();
                }

                if (SipService.currentService != null)
                    SipService.currentService.dataConnectionChanged();
            }
        });

        swSensor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Sensor On/Off
                    mPrefs.setPreferenceBooleanValue(AppPrefs.SENSOR_ENABLE,
                            true);
                } else {
                    //
                    mPrefs.setPreferenceBooleanValue(AppPrefs.SENSOR_ENABLE,
                            false);
                }
            }
        });

        swLteMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // 통화시 3G/LTE에서만 사용
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.USE_LTE_MODE, true);
                } else {
                    //
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.USE_LTE_MODE, false);
                }

                if (SipService.currentService != null)
                    SipService.currentService.wifiConfigChanged();
            }
        });

        // 에코캔슬러 사용
        swEcho.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // 에코제거 사용함
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.ECHO_CANCELLATION, true);
                } else {
                    // 사용안함
                    mSysPrefs.setPreferenceBooleanValue(
                            PreferencesWrapper.ECHO_CANCELLATION, false);
                }
            }
        });

        // BJH StartForeground 해제 할때만 알림창으로 안내
        swForeground.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    // 사용함
                    mPrefs.setPreferenceBooleanValue(AppPrefs.START_FOREGROUND,
                            true);
                    Log.d(THIS_FILE, "StartForeground SERVICE START !!");
                    Intent intent = new Intent(mContext,
                            StartForegroundService.class);
                    if (Build.VERSION.SDK_INT >= 26)
                        startForegroundService(intent);
                    else
                        startService(intent);
                } else {
                    // 해제 시 알림창
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            Setting.this);
                    builder.setTitle(mContext.getResources().getString(
                            R.string.title_foreground));
                    builder.setMessage(getString(R.string.conts_start_foreground));
                    /*LayoutInflater inflater = Setting.this.getLayoutInflater();
                    View view_svr = inflater.inflate(R.layout.foreground_dialog, null);
                    builder.setView(view_svr);*/
                    builder.setPositiveButton(
                            getResources().getString(R.string.release),
                            new DialogInterface.OnClickListener() {
                                // 사용 안함
                                @Override
                                public void onClick(DialogInterface arg0,
                                                    int arg1) {
                                    // TODO Auto-generated method stub
                                    mPrefs.setPreferenceBooleanValue(
                                            AppPrefs.START_FOREGROUND, false);
                                    stopService(new Intent(mContext,
                                            StartForegroundService.class));
                                }
                            });
                    builder.setNegativeButton(
                            getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    // TODO Auto-generated method stub
                                    dialog.cancel();
                                    swForeground.setChecked(true);
                                    mPrefs.setPreferenceBooleanValue(
                                            AppPrefs.START_FOREGROUND, true);
                                }
                            });
                    builder.create();
                    builder.show();
                }
            }
        });

        mSendVolumn = (LinearLayout) findViewById(R.id.layout_send_volumn);
        mSendVolumn.setClickable(true);
        mSendVolumn.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Setting.this);
                LayoutInflater inflater = Setting.this.getLayoutInflater();
                View view_sb = inflater.inflate(R.layout.seekbar_dialog, null);
                builder.setView(view_sb);
                builder.setPositiveButton(
                        getResources().getString(R.string.setting),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO Auto-generated method stub
                                // 볼륨 저장
                                mPrefsWrapper.setPreferenceFloatValue(
                                        PreferencesWrapper.SND_MIC_LEVEL,
                                        (float) mSendMicLevel / 100.0f);
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                dialog.cancel();
                            }
                        });

                builder.setTitle(mContext.getResources().getString(
                        R.string.send_volumn_level));
                SeekBar sbVal = (SeekBar) view_sb.findViewById(R.id.sbVal);
                mRecvVal = (TextView) view_sb.findViewById(R.id.tvVal);

                final int min_vol = 10;
                float volumn = mPrefsWrapper
                        .getPreferenceFloatValue(PreferencesWrapper.SND_MIC_LEVEL);
                // 10-200
                mSendMicLevel = (int) (volumn * 100.0f);
                if (mSendMicLevel > 200)
                    mSendMicLevel = 200;

                sbVal.incrementProgressBy(5);
                sbVal.setMax(200 - min_vol);
                sbVal.setProgress(mSendMicLevel - min_vol);
                sbVal.setKeyProgressIncrement(5);
                mRecvVal.setText(String.valueOf(mSendMicLevel) + "%");

                sbVal.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // TODO Auto-generated method stub
                        progress = progress / 5;
                        progress = progress * 5;
                        mSendMicLevel = progress + min_vol;
                        // mSendVal.setText(String.valueOf( (float)progress /
                        // 100.0f));
                        mRecvVal.setText(String.valueOf(mSendMicLevel) + "%");
                    }
                });

                builder.create();
                builder.show();

            }
        });

        mRecvVolumn = (LinearLayout) findViewById(R.id.layout_recv_volumn);
        mRecvVolumn.setClickable(true);
        mRecvVolumn.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Setting.this);
                LayoutInflater inflater = Setting.this.getLayoutInflater();
                View view_sb = inflater.inflate(R.layout.seekbar_dialog, null);
                builder.setView(view_sb);
                builder.setPositiveButton(
                        getResources().getString(R.string.setting),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO Auto-generated method stub
                                // 볼륨 저장
                                mPrefsWrapper.setPreferenceFloatValue(
                                        PreferencesWrapper.SND_SPEAKER_LEVEL,
                                        (float) mSendSpeakerLevel / 100.0f);
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                mSendSpeakerLevel = 0;
                                dialog.cancel();
                            }
                        });

                builder.setTitle(mContext.getResources().getString(
                        R.string.recv_volumn_level));
                SeekBar sbVal = (SeekBar) view_sb.findViewById(R.id.sbVal);
                mSendVal = (TextView) view_sb.findViewById(R.id.tvVal);
                float volumn = mPrefsWrapper
                        .getPreferenceFloatValue(PreferencesWrapper.SND_SPEAKER_LEVEL);
                // 10-200
                final int min_vol = 10;
                mSendSpeakerLevel = (int) (volumn * 100.0f);
                if (mSendSpeakerLevel > 200)
                    mSendSpeakerLevel = 200;

                sbVal.setMax(200 - min_vol);
                sbVal.incrementProgressBy(5);
                sbVal.setProgress(mSendSpeakerLevel - min_vol);
                mSendVal.setText(String.valueOf(mSendSpeakerLevel) + "%");

                sbVal.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {

                        progress = progress / 5;
                        progress = progress * 5;
                        // TODO Auto-generated method stub
                        mSendSpeakerLevel = progress + min_vol;
                        // mSendVal.setText(String.valueOf( (float)progress /
                        // 100.0f));
                        mSendVal.setText(String.valueOf(mSendSpeakerLevel)
                                + "%");
                    }
                });

                builder.create();
                builder.show();

            }
        });
        // BJH 2016.09.20
        mRingTone = (LinearLayout) findViewById(R.id.layout_ringtone);
        mRingTone.setClickable(true);
        mRingTone.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
                String uri = mPrefs.getPreferenceStringValue(AppPrefs.RINGTONE_URI);
                if (uri != null && uri.length() > 0)
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(uri));
                else
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE));
                startActivityForResult(intent, SELECT_RINGTONE);
            }
        });

        // Dail070 종료
        mQuit = (LinearLayout) findViewById(R.id.layout_quit);
        mQuit.setClickable(true);
        mQuit.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                //DialMain.AppCloseRequest(mContext);
                getParent().onBackPressed();
            }
        });

        // 서비스 소개
		/*
		 * mDial070 = (LinearLayout) findViewById(R.id.layout_dial070);
		 * mDial070.setClickable(true); mDial070.setOnClickListener(new
		 * View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub String url = mPrefs
		 * .getPreferenceStringValue(AppPrefs.URL_SERVICE_INFO); if (url == null
		 * || url.length() == 0) return; Intent intent = new
		 * Intent(Setting.this, WebClient.class); intent.putExtra("url", url);
		 * intent.putExtra("title",
		 * getResources().getString(R.string.title_dial070));
		 * startActivity(intent); } });
		 *
		 * // 이용방법 mHelp = (LinearLayout) findViewById(R.id.layout_help);
		 * mHelp.setClickable(true); mHelp.setOnClickListener(new
		 * View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub String url = mPrefs
		 * .getPreferenceStringValue(AppPrefs.URL_MANUAL); if (url == null ||
		 * url.length() == 0) return; Intent intent = new Intent(Setting.this,
		 * WebClient.class); intent.putExtra("url", url);
		 * intent.putExtra("title",
		 * getResources().getString(R.string.title_help));
		 * startActivity(intent); } });
		 *
		 * // 요금제 mPayment = (LinearLayout) findViewById(R.id.layout_payment);
		 * mPayment.setClickable(true); mPayment.setOnClickListener(new
		 * View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub String url = "http://www.maaltalk.com/charge.php"; if (url ==
		 * null || url.length() == 0) return; Intent intent = new
		 * Intent(Setting.this, WebClient.class); intent.putExtra("url", url);
		 * intent.putExtra("title",
		 * getResources().getString(R.string.title_payment));
		 * startActivity(intent); } });
		 */

        // 사용자별 설명서
        mUserMsg = (LinearLayout) findViewById(R.id.layout_user_msg);
        mUserMsg.setClickable(true);
        mUserMsg.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_USER_MSG);
                if (url == null || url.length() == 0)
                    return;
                String urlString = String.format("%s?name=%s", url, uid);
                Intent intent = new Intent(Setting.this, PayClient.class);
                intent.putExtra("url", urlString);
                intent.putExtra("title",
                        getResources().getString(R.string.title_user_msg));
                intent.putExtra("popup", true);
                startActivityForResult(intent, SELECT_USER_MSG);
            }
        });

        // 공지사항
        mNotice = (LinearLayout) findViewById(R.id.layout_notice);
        mNotice.setClickable(true);
        mNotice.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_NOTICE);
                if (url == null || url.length() == 0)
                    return;
                Intent intent = new Intent(Setting.this, WebClient.class);
                intent.putExtra("url", url);
                intent.putExtra("title",
                        getResources().getString(R.string.title_notice));
                startActivity(intent);
            }
        });

        // 자주하는 질문
        mFaq = (LinearLayout) findViewById(R.id.layout_faq);
        mFaq.setClickable(true);
        mFaq.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_FAQ);
                if (url == null || url.length() == 0)
                    return;
                Intent intent = new Intent(Setting.this, WebClient.class);
                intent.putExtra("url", url);
                intent.putExtra("title",
                        getResources().getString(R.string.title_faq));
                startActivity(intent);
            }
        });

        // 사용자 문의 게시판
		/*
		 * mBoard = (LinearLayout) findViewById(R.id.layout_board);
		 * mBoard.setClickable(true); mBoard.setOnClickListener(new
		 * View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub String url = mPrefs
		 * .getPreferenceStringValue(AppPrefs.URL_QUESTION); if (url == null ||
		 * url.length() == 0) return; Intent intent = new Intent(Setting.this,
		 * WebClient.class); intent.putExtra("url", url);
		 * intent.putExtra("title",
		 * getResources().getString(R.string.title_board));
		 * startActivity(intent); } });
		 */

        // 카톡으로 질의하기
        mKakao = (LinearLayout) findViewById(R.id.layout_kakao);
        mKakao.setClickable(true);
        mKakao.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {

                // BJH 카카오톡 호출
                PackageManager pm = mContext.getPackageManager();
                try {
                    Intent intent = pm
                            .getLaunchIntentForPackage(KAKAO_PACKAGE_NAME);
                    if (intent != null) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse("kakaoplus://plusfriend/friend/@말톡"));
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    } else {
                        Uri uri = Uri.parse(DialMain.MAALTALK_EMAIL);
                        intent = new Intent(Intent.ACTION_SENDTO, uri);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }
                } catch (ActivityNotFoundException e) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse("https://play.google.com/store/apps/details?id=com.google.android.gm"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            }
        });

        //BJH 2016.11.10
        mPushDB = (LinearLayout) findViewById(R.id.layout_db_data);
        mPushDB.setClickable(true);
        if (mPrefs.getPreferenceStringValue(AppPrefs.PUSH_RECORD_FLAG) != null && mPrefs.getPreferenceStringValue(AppPrefs.PUSH_RECORD_FLAG).length() > 0)
            mPushDB.setVisibility(View.VISIBLE);
        else
            mPushDB.setVisibility(View.GONE);
        mPushDB.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Setting.this);
                LayoutInflater inflater = Setting.this.getLayoutInflater();
                final View view = inflater.inflate(R.layout.report_dialog, null);
                final EditText edtReport=view.findViewById(R.id.edtReport);
                builder.setView(view);
                builder.setPositiveButton(
                        getResources().getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0,
                                                int arg1) {
                                DBManager database = new DBManager(mContext); // BJH 2016.11.29
                                database.open();
                                int count = database.getPushRecordCount();
                                database.close();
                                if (count > 0) {
                                    String qna = edtReport.getText().toString();
                                    initializePush(qna);
                                } else
                                    Alert(mContext, getResources().getString(R.string.db_no_data));
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                            }
                        });
                builder.setTitle(mContext.getResources().getString(
                        R.string.title_db_data));
                builder.show();
            }
        });

        // 이용약관
        mTerms = (LinearLayout) findViewById(R.id.layout_terms);
        mTerms.setClickable(true);
        mTerms.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_TERMS);
                if (url == null || url.length() == 0)
                    return;
                Intent intent = new Intent(Setting.this, WebClient.class);
                intent.putExtra("url", url);
                intent.putExtra("title",
                        getResources().getString(R.string.title_terms));
                startActivity(intent);
            }
        });

        // 정보 셋팅(폰번호)
        UpdateMyNumber();

        // 정보 셋팅(잔액정보)
        // @@@sgkim
        // 잔액정보 가져와야함

        mVerDesc.setText(getString(R.string.title_version));

        mNewVersion = (LinearLayout) findViewById(R.id.layout_version);
        mNewVersion.setClickable(true);
        mNewVersion.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
				/*BJH 2016-06-01 중국 사용자인 경우 GCM 값이 없을 수가 있기 때문에*/
                if (Fcm.mRegistrationID == null || Fcm.mRegistrationID.length() == 0)
                    return;
                new Version().execute();
            }

        });

        String ver = "";
        if (ServicePrefs.getSIMCountry(this) != null
                && ServicePrefs.getSIMCountry(this).length() > 0)
            ver = ServicePrefs.getVersion() + " ("
                    + ServicePrefs.getSIMCountry(this) + ")";
        else
            ver = ServicePrefs.getVersion();
        mVersion.setText(ver);

        // BJH SERVICE OUT
        mSvcOut = (LinearLayout) findViewById(R.id.layout_svc_out);
        mSvcOut.setClickable(true);
        mSvcOut.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                // TODO Auto-generated method stub
                /*String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_SVC_OUT);
                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                if (url == null || url.length() == 0)
                    return;
                if (uid == null || uid.length() == 0)
                    return;
                //url = "http://info.maaltalk.com/maaltalk_user/maaltalk_wait.php";
                String urlString = String.format("%s?name=%s", url, uid);
                Intent intent = new Intent(Setting.this, PayClient.class);
                intent.putExtra("url", urlString);
                intent.putExtra("title",
                        getResources().getString(R.string.title_svc_out));
                startActivityForResult(intent, SELECT_SVC_OUT);*/
                Intent intent = new Intent(Setting.this, ServiceOutActivity.class);
                startActivityForResult(intent, SELECT_SVC_OUT);
            }
        });


        // NETWORK
        mNetworkStat = (LinearLayout) findViewById(R.id.layout_network_stat);
        /*if(ServicePrefs.DIAL_RETAIL)
            mNetworkStat.setVisibility(View.GONE); // BJH 2017.09.18 팀장님 => 2017.10.30 팀장님 => 2017.10.31 팀장님*/
        mNetworkStat.setClickable(true);
        mNetworkStat.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                DialMain.skipClose = true;
                DialMain.skipCallview = true;
                Intent intent = new Intent(Setting.this, NetworkActivity.class);
                startActivityForResult(intent, SELECT_NETWORK_STATS);
            }
        });

        // 수신거절 메시지
        mRejectCallMsg = (LinearLayout) findViewById(R.id.layout_reject_call_msg);
        mRejectCallMsg.setClickable(true);
        mRejectCallMsg.setOnClickListener(new OnSingleClickListener() {

            @Override
            public void onSingleClick(View v) {
                Intent intent=new Intent(mContext,RejectCallMsgActivity.class);
                startActivity(intent);
            }
        });
    }

    private void Alert(String msg) {
        AlertDialog.Builder msgDialog = new AlertDialog.Builder(mContext);
        msgDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        msgDialog.setMessage(msg);
        msgDialog.setIcon(R.drawable.ic_launcher);
        msgDialog.setNegativeButton(
                mContext.getResources().getString(R.string.close), null);
        msgDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String msg = ""; // BJH 2016.10.11
        int rs = 0; // BJH 2016.10.11
        Log.i(THIS_FILE, "onActivityResult:" + requestCode);
        switch (requestCode) {
            //BJH 2016.06.21 결제 후 서버 변경
            case SELECT_PAYMENT:
                if (resultCode == RESULT_OK) {
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if (msg.equals(getResources().getString(R.string.pay_ok))) {
                        Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                        intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                        intent.setPackage(getPackageName());
                        sendBroadcast(intent);
                        rs = ServicePrefs.login_check(this);// BJH
                        UpdateMyNumber();
                        if (rs >= 0)
                            SipService.currentService.changeSipStack();
                    }
                } else {
                    msg = getResources().getString(R.string.setting_pay_cancel);
                }
                Alert(this, msg);
                break;
            case SELECT_MYINFO: {
                // REALOD
                /*rs = ServicePrefs.login_check(this);
                if (rs >= 0) {
                    UpdateMyNumber();
                } else if (rs == -90 || rs == -91) {
                    UpdateMyNumber();
                    //doAuth(rs);
                }*/
                if (resultCode == RESULT_OK) {
                    ServicePrefs.login_check(this); // BJH 2017.10.27
                    UpdateMyNumber();
                } else {
                    UpdateMyNumber();
                }

            }
            break;
            // BJH 사용자별 설명서에서 결제가 되었을 경우와 서버 변경을 선택했을 경우
            case SELECT_USER_MSG:
                // BJH 나의 정보에서 사용자별 설명서를 이동하기 때문에
                /*rs = ServicePrefs.login_check(this);// BJH
                if (rs >= 0) {
                    UpdateMyNumber();
                } else if (rs == -90 || rs == -91) {
                    UpdateMyNumber();
                    //doAuth(rs);
                }*/
                ServicePrefs.login_check(this); // BJH 2017.10.27
                UpdateMyNumber();

                if (resultCode == RESULT_OK) {
                    // BJH 2016.06.21 결제 후 서버 변경
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if (!msg.equals("popup") && !msg.equals("maaltalk_server")) { // 사용자별 설명서 팝업창 닫기가 아닐 경우
                        if (msg.equals(getResources().getString(R.string.pay_ok))) {
                            Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                            intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                            intent.setPackage(getPackageName());
                            sendBroadcast(intent);
                            if (rs >= 0)
                                SipService.currentService.changeSipStack();
                        }
                        Alert(this, msg);
                    }
                    if (msg.equals("maaltalk_server")) { // 사용자별 설명서에서 서버 이동을 눌렀을 경우
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                Setting.this);
                        //LayoutInflater inflater = Setting.this.getLayoutInflater();
                        //View view_svr = inflater.inflate(R.layout.svr_dialog, null);
                        //builder.setView(view_svr);
                        builder.setMessage(getString(R.string.conts_my_svr_check));
                        builder.setPositiveButton(
                                getResources().getString(R.string.change),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {
                                        // TODO Auto-generated method stub
                                        ServicePrefs.DNSLookUp(mContext);
                                    }
                                });
                        builder.setNegativeButton(
                                getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // TODO Auto-generated method stub
                                        dialog.cancel();
                                    }
                                });
                        builder.setTitle(mContext.getResources().getString(
                                R.string.title_my_svr));
                        builder.show();
                    }
                }
                break;
            case SELECT_SVC_OUT:
                if (resultCode == RESULT_OK) {
                    // msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    // Alert(this, msg);
                    //serviceOut();
                    String res = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if (res != null && res.length() > 0 && res.equals("FAIL")) {
                        Alert(this, getResources().getString(R.string.setting_edns_fail));
                    }
                }
                break;
            case SELECT_SEARCH:
                if (resultCode == RESULT_OK) // BJH 번호를 선택했다면 바로 통화가 되도록
                {
                    String number = "";
                    number = data.getStringExtra("PhoneNumber");
				/*if (number.startsWith("+820"))//BJH 2016.06.16 603 Error
					number = number.replace("+82", "");
				if (!number.startsWith("+8210"))//BJH 2016.07.04
					number = number.replace("+82", "");*/ // BJH 2016.12.29
                    Intent intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
                    intent.putExtra(SipManager.EXTRA_CALL_NUMBER, number);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    intent.setPackage(getPackageName());
                    startActivity(intent);
                }
                break;
            case SELECT_RINGTONE: // BJH 2016.09.20
                if (resultCode == RESULT_OK) {
                    Uri ringtoneUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    String ex_ringtone = mPrefs.getPreferenceStringValue(AppPrefs.RINGTONE_URI);
                    if (!ringtoneUri.toString().equals(ex_ringtone)) {
                        Cursor c = getContentResolver().query(ringtoneUri, null, null, null, null);
                        if (c.moveToNext()) {
                            String path = c.getString(c.getColumnIndex("_data"));
                            c.close();
                            mPrefs.setPreferenceStringValue(AppPrefs.RINGTONE_URI, ringtoneUri.toString());
                            mPrefs.setPreferenceStringValue(AppPrefs.RINGTONE_DIR, path);
                            Alert(this, getResources().getString(R.string.ringtone_setting));
                        } else {
                            Alert(this, getResources().getString(R.string.setting_edns_fail));
                        }
                    }
                }
                break;
            case SELECT_BRIDGE: // BJH 2016.10.11
                if (resultCode == RESULT_OK) {
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                    intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                    intent.setPackage(getPackageName());
                    sendBroadcast(intent);
                    rs = ServicePrefs.login_check(this);
                    if (rs >= 0)
                        SipService.currentService.changeSipStack();

                    Alert(this, msg);
                }
                break;
            case SELECT_USIM:
                if (resultCode == RESULT_OK) {
                    Alert(this, data.getExtras().getString(PayClient.ACTIVITY_RESULT));
                }
                break;
            case SELECT_OVERLAY_PERMISSION:
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(mContext)) {
                        DialMain.setApnService(mContext);
                    } else {
                        showMarshmallowAlert();
                    }
                }*/
                break;
            case SELECT_REQ_MYINFO:
                (new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        ServicePrefs.login_check(mContext); // BJH 2017.10.27
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        UpdateMyNumber();

                        if (resultCode == RESULT_OK) {
                            String res = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                            if (res != null && res.length() > 0 && res.equals("SUCCESS")) {
                                //String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION);
                                String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
                                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                                if (url == null || url.length() == 0)
                                    return;
                                if (uid == null || uid.length() == 0)
                                    return;

                                String urlString = String.format("%s?name=%s&refresh=%s", url, uid, System.currentTimeMillis() + "");
                                Intent intent = new Intent(Setting.this, PayClient.class);
                                intent.putExtra("url", urlString);
                                intent.putExtra("title",
                                        getResources().getString(R.string.title_myinfo));
                                intent.putExtra("popup", true);// BJH 나의 정보에서 사용자별 설명서로 이동하므로
                                startActivityForResult(intent, SELECT_USER_MSG);
                            } else if (res != null && res.length() > 0 && res.equals("PINCODE")) {
                                String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA);
                                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                                if (url == null || url.length() == 0)
                                    return;
                                if (uid == null || uid.length() == 0)
                                    return;

                                String urlString = String.format("%s?name=%s", url, uid);
                                Intent intent = new Intent(Setting.this, PayClient.class);
                                intent.putExtra("url", urlString);
                                intent.putExtra("title",
                                        getResources().getString(R.string.title_my_point));
                                startActivityForResult(intent, SELECT_PAYMENT);
                            } else if (res != null && res.length() > 0 && res.equals("FAIL")) {
                                Alert(mContext, getResources().getString(R.string.setting_edns_fail));
                            }
                        }
                    }
                }).execute();


                break;
            default:
                break;
        }
        return;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // 백버튼 비활성화
        // super.onBackPressed();
        DialMain.mainBackPressed(mContext);
    }

    private void doAuth(int rs) {
        String errMsg = getResources().getString(R.string.error_login);
        if (rs == -90) {
            errMsg = getResources().getString(R.string.setting_sim_not_found);
        } else if (rs == -91) {
            errMsg = getResources().getString(R.string.setting_sim_changed);
        }

        AlertDialog.Builder msgDialog = new AlertDialog.Builder(Setting.this);
        msgDialog.setTitle(getResources().getString(R.string.app_name));
        // msgDialog.setMessage((String)msg.obj);
        msgDialog.setMessage(errMsg);
        msgDialog.setIcon(R.drawable.ic_launcher);
        msgDialog.setNegativeButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        doAuth();
                    }
                });
        msgDialog.show();
    }

    private void doAuth() {
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, false);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, "");
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, "");
        mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, ""); // GCM-RE

        Intent intent = new Intent(Setting.this, AuthActivityV2.class);
        startActivity(intent);
        finish();

    }

    private void UpdateMyNumber() {
        if (mMyPhone == null)
            return;

        if (!ServicePrefs.DIAL_TEST) {
            String number = mPrefs
                    .getPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER);
            if (number != null && number.length() > 0) {
                if (ServicePrefs.mUser070 != null
                        && ServicePrefs.mUser070.length() > 0) {
                    if (ServicePrefs.mUse070) {
                        mMyPhone.setText(ServicePrefs.mUser070);
                    } else {
                        mMyPhone.setText(number);
                    }
                    // mMyPhone.setText(String.format("%s (%s)", number,
                    // ServicePrefs.mUser070));
                } else {
                    mMyPhone.setText(number);
                }
            } else if (ServicePrefs.mUser070 != null
                    && ServicePrefs.mUser070.length() > 0) {
                mMyPhone.setText(ServicePrefs.mUser070);
            }
        } else {
            mMyPhone.setText(ServicePrefs.DIAL_TEST_ID);
        }
    }

    private void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(mContext, R.style.Theme_AppCompat_Light_Dialog);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle(mContext.getResources().getString(R.string.version_request));
            progressDialog.setMessage(mContext.getResources().getString(R.string.version_progress));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    progressDialog = null;
                    if (mConnection!=null){
                        mConnection.disconnect();
                        mConnection = null;
                    }

                    mVer = "0";
                }
            });
            progressDialog.show();

            /*progressDialog = ProgressDialog.show(mContext, mContext
                            .getResources().getString(R.string.version_request),
                    mContext.getResources()
                            .getString(R.string.version_progress), true, true,
                    new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            progressDialog = null;
                            if (mConnection!=null){
                                mConnection.disconnect();
                                mConnection = null;
                            }

                            mVer = "0";
                        }
                    });*/

        }
    }

    private void showUSIMProgress() { // BJH 2017.01.04
        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(mContext, mContext
                            .getResources().getString(R.string.usim_info_update),
                    mContext.getResources()
                            .getString(R.string.usim_info_update), true, true);
            progressDialog.show();
        }
    }

    public void serviceOut() {// BJH 서비스 해제
        // BJH Webview Session Clear
        CookieSyncManager cookieSyncManager = CookieSyncManager
                .createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();
        cookieSyncManager.sync();
        // DB Data Clear
        DBManager db = new DBManager(mContext);
        db.open();
        db.deleteAllRecentCalls();
        db.deleteAllSmsList();
        db.deleteAllSmsMsg();
        db.deleteAllFavorites();
        db.close();
        // Stop Service
        stopService(new Intent(mContext, MessageService.class));
        stopService(new Intent(mContext, SipService.class));
        stopService(new Intent(mContext, StartForegroundService.class));

        if (ServicePrefs.mLogin) {
            ServicePrefs.logout();
        }
        // FOR SAFE
        SipService.currentService = null;
        // Preference Clear
        mPrefs.resetAllDefaultValues();
        mSysPrefs.resetAllDefaultValues();
        // Go Main
        // Intent intent = new Intent(mContext, DialMain.class);
        Intent intent = new Intent(mContext, CloseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void DataUsage(boolean wifi) {
        float receiveByteByMobile, transmitByteByMobile, txM, rxM, totalResult, rxRate, txRate;
        if (!wifi) {
            receiveByteByMobile = TrafficStats.getMobileRxBytes();
            transmitByteByMobile = TrafficStats.getMobileTxBytes();
            rxM = Float.parseFloat(String.format("%.2f", receiveByteByMobile
                    / (1024 * 1024)));
            txM = Float.parseFloat(String.format("%.2f", transmitByteByMobile
                    / (1024 * 1024)));
            totalResult = Float.parseFloat(String.format("%.2f",
                    (receiveByteByMobile + transmitByteByMobile)
                            / (1024 * 1024)));
            mPrefsWrapper.setPreferenceFloatValue(PreferencesWrapper.DATA_RX,
                    rxM);
            mPrefsWrapper.setPreferenceFloatValue(PreferencesWrapper.DATA_TX,
                    txM);
        } else {
            rxM = mPrefsWrapper
                    .getPreferenceFloatValue(PreferencesWrapper.DATA_RX);
            txM = mPrefsWrapper
                    .getPreferenceFloatValue(PreferencesWrapper.DATA_TX);
            totalResult = Float.parseFloat(String.format("%.2f", (rxM + txM)));
        }

        rxRate = Float.parseFloat(String.format("%.2f", rxM / totalResult));
        txRate = Float.parseFloat(String.format("%.2f", txM / totalResult));

        AlertDialog.Builder lteBuilder = new AlertDialog.Builder(Setting.this);
        LayoutInflater inflater = Setting.this.getLayoutInflater();
        View view_lte = inflater.inflate(R.layout.lte_dialog, null);

        lteBuilder.setView(view_lte);
        lteBuilder.setPositiveButton(
                mContext.getResources().getString(R.string.confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        arg0.cancel();
                    }
                });
        lteBuilder.setTitle(mContext.getResources().getString(
                R.string.apn_usim_data_use_3g_4g));

        lteBuilder.create();
        lteBuilder.show();

        CustomProgress rxProgress = (CustomProgress) view_lte
                .findViewById(R.id.rxProgress);
        rxProgress.setMaximumPercentage(rxRate);
        rxProgress.useRoundedRectangleShape(30.0f);
        rxProgress.setTextSize(18);
        rxProgress.setTextColor(getResources().getColor(R.color.white));
        rxProgress.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
        rxProgress.setText(mContext.getResources().getString(
                R.string.apn_usim_data_rx)
                + " : " + rxM + "MB");
        rxProgress.setProgressColor(getResources().getColor(R.color.red_500));
        rxProgress.setProgressBackgroundColor(getResources().getColor(
                R.color.red_200));

        CustomProgress txProgress = (CustomProgress) view_lte
                .findViewById(R.id.txProgress);
        txProgress.setMaximumPercentage(txRate);
        txProgress.useRoundedRectangleShape(30.0f);
        txProgress.setTextSize(18);
        txProgress.setTextColor(getResources().getColor(R.color.white));
        txProgress.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
        txProgress.setText(mContext.getResources().getString(
                R.string.apn_usim_data_tx)
                + " : " + txM + "MB");
        txProgress.setProgressColor(getResources().getColor(R.color.blue_500));
        txProgress.setProgressBackgroundColor(getResources().getColor(
                R.color.blue_200));

        CustomProgress totalProgress = (CustomProgress) view_lte
                .findViewById(R.id.totalProgress);
        totalProgress.setMaximumPercentage(1.0f);
        totalProgress.useRoundedRectangleShape(30.0f);
        totalProgress.setTextSize(18);
        totalProgress.setTextColor(getResources().getColor(R.color.white));
        totalProgress.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
        totalProgress.setText(mContext.getResources().getString(
                R.string.apn_usim_data_use)
                + " : " + totalResult + "MB");
        totalProgress.setProgressColor(getResources().getColor(
                R.color.purple_500));
        totalProgress.setProgressBackgroundColor(getResources().getColor(
                R.color.purple_200));
    }

    public void initializePush(String qna) { //BJH 2016.11.10 => 2018.01.24 문의 추가
        if(qna == null || qna.length() == 0)
            qna = "";

//        String storage = Environment.getExternalStorageDirectory().getAbsolutePath();
        String storage = getFilesDir().getAbsolutePath();

        String folder = "maaltalk";
        String foler_name = "/" + folder + "/";
        long name = System.currentTimeMillis();
        final String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String file_name = uid + "_" + name + ".csv";
        String string_path = storage + foler_name;
        File file_path = null;
        file_path = new File(string_path);
        if (!file_path.isDirectory()) {
            file_path.mkdirs();
        }

        boolean result = false;
        final File outfile = new File(string_path + file_name);
        try {
            outfile.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(outfile));
            DBManager database = new DBManager(mContext);
            database.open();
            Cursor curCSV = database.exportPushRecord();
            csvWrite.writeNext(curCSV.getColumnNames());
            SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
            while (curCSV.moveToNext()) {
                String push_time = "";
                String receive_time = "";
                String response_time = "";
                if (curCSV.getLong(1) > 0)
                    push_time = mFormatter.format(curCSV.getLong(1));
                if (curCSV.getLong(2) > 0)
                    receive_time = mFormatter.format(curCSV.getLong(2));
                if (curCSV.getLong(3) > 0)
                    response_time = mFormatter.format(curCSV.getLong(3));
                String arrStr[] = {curCSV.getString(0), push_time,
                        receive_time, response_time, curCSV.getString(4),
                        curCSV.getString(5), curCSV.getString(6),
                        curCSV.getString(7), curCSV.getString(8),
                        curCSV.getString(9)};
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
            database.close();
            result = true;
        } catch (Exception sqlEx) {
            Log.e(THIS_FILE, sqlEx.getMessage(), sqlEx);
        }

        if (result) {
            String str = postCSV(mContext, outfile, qna);
            JSONObject jObject;
            try {
                jObject = new JSONObject(str);
                JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                if (responseObject.getInt("RES_CODE") == 0) {
                    Alert(mContext, getResources().getString(R.string.db_data_post));
                    return;
                }
            } catch (Exception e) {
                Log.e(THIS_FILE, "JSON", e);
                Log.d(THIS_FILE, "JSON ERROR");
            }
            Alert(mContext, getResources().getString(R.string.setting_edns_fail));
        } else {
            Alert(mContext, getResources().getString(R.string.setting_edns_fail));
        }
    }

    /*
     * 배너 관련 추가
     */
    private String postChangeDidMode(String mode) {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String url="https://if.maaltalk.com/maaltalk/req_did_mode.php";
        String urlString = String.format("%s?name=%s&did_mode=%s", url, uid,mode);

        Log.d(THIS_FILE, "postChangeDidMode URL=" + urlString);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httppost = new HttpPost(urlString);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private void cofirmMyinfo() {
        (new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                String userID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                String myinfoURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_MYINFO);

                DebugLog.d("test_my_info url: "+myinfoURL);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("name", userID));
                String rs = postServer(mContext, myinfoURL, nameValuePairs); //postMyinfo(true);
                return rs;
            }

            @Override
            protected void onPostExecute(String rs) {

                String result = "";
                JSONObject jObject;
                try {
                    jObject = new JSONObject(rs);
                    JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                    Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                    Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                    if (responseObject.getInt("RES_CODE") == 0) {
                        result = responseObject.getString("RES_DESC");
                    }
                } catch (Exception e) {
                    Log.e(THIS_FILE, "JSON", e);
                    Log.d(THIS_FILE, "JSON ERROR");
                    result = "FAIL";
                    App.isOauth=false;
                }

                if (result != null && result.length() > 0) {
                    ServicePrefs.login_check(mContext); // BJH 2017.10.27
                    UpdateMyNumber();

                    hideLoadingDialog();

                    String res = result;
                    if(res != null && res.length() > 0 && res.equals("SUCCESS")) {
                        App.isOauth=true;
                        //String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION);
                        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2); // BJH 2017.06.22
                        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                        if (url == null || url.length() == 0)
                            return;
                        if (uid == null || uid.length() == 0)
                            return;

                        String urlString = String.format("%s?name=%s&refresh=%s", url, uid, System.currentTimeMillis()+"");
                        Intent intent = new Intent(Setting.this, PayClient.class);
                        intent.putExtra("url", urlString);
                        intent.putExtra("title",
                                getResources().getString(R.string.title_myinfo));
                        intent.putExtra("popup", true);// BJH 나의 정보에서 사용자별 설명서로 이동하므로
                        startActivityForResult(intent, SELECT_USER_MSG);
                    } else if(res != null && res.length() > 0 && res.equals("PINCODE")) {
                        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA);
                        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                        if (url == null || url.length() == 0)
                            return;
                        if (uid == null || uid.length() == 0)
                            return;

                        String urlString = String.format("%s?name=%s", url, uid);
                        Intent intent = new Intent(Setting.this, PayClient.class);
                        intent.putExtra("url", urlString);
                        intent.putExtra("title",
                                getResources().getString(R.string.title_my_point));
                        startActivityForResult(intent, SELECT_PAYMENT);
                    } else if(res != null && res.length() > 0 && res.equals("FAIL")) {
                        App.isOauth=false;
                        Alert(mContext, getResources().getString(R.string.setting_edns_fail));
                    }
                }else {
                    hideLoadingDialog();
                    App.isOauth=false;
                    Intent intent = new Intent(Setting.this, OauthLoginActivity.class);
                    startActivityForResult(intent, SELECT_REQ_MYINFO);
                }

                super.onPostExecute(rs);
            }
        }).execute();
    }

    private void preConfirmMyinfo() {
        (new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                String userID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                String myinfoURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_MYINFO);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("name", userID));
                String rs = postServer(mContext, myinfoURL, nameValuePairs); //postMyinfo(true);
                return rs;
            }

            @Override
            protected void onPostExecute(String rs) {
                String result = "";
                JSONObject jObject;
                try {
                    jObject = new JSONObject(rs);
                    JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                    Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
                    Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
                    if (responseObject.getInt("RES_CODE") == 0) {
                        result = responseObject.getString("RES_DESC");
                    }
                } catch (Exception e) {
                    App.isOauth=false;
                    Log.e(THIS_FILE, "JSON", e);
                    Log.d(THIS_FILE, "JSON ERROR");
                    result = "FAIL";
                }

                if (result != null && result.length() > 0) {
                    (new AsyncTask<String, Void, String>(){
                        @Override
                        protected String doInBackground(String... params) {
                            ServicePrefs.login_check(mContext); // BJH 2017.10.27
                            return params[0];
                        }

                        @Override
                        protected void onPostExecute(String rs) {
                            UpdateMyNumber();

                            String res = rs;
                            if(res != null && res.length() > 0 && res.equals("SUCCESS")) {
                                App.isOauth=true;
                            } else  {
                                App.isOauth=false;
                            }
                            super.onPostExecute(rs);
                        }
                    }).execute(result);



                }else {
                    App.isOauth=false;
                }
                super.onPostExecute(rs);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showLoadingDialog(){
        if (pDialog!=null && pDialog.isShowing()){
            pDialog.dismiss();
        }
        pDialog =new MaterialAlertDialogBuilder(this).create();
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        TextView messageView = pDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }

    private void hideLoadingDialog(){
        if (pDialog!=null){
            pDialog.dismiss();
        }
    }

    private class Version extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            showProgress();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            // Confirmation of market information in the Google Play Store
            String ver = null;

            String url = "https://play.google.com/store/apps/details?id="
                    + DialMain.MAALTALK_PACKAGE_NAME+"&hl=en";
            try {
                Document doc = Jsoup.connect(url).get();
                Elements Version = doc.select(".htlgb");

                for (int i = 0; i < 10 ; i++) {
                    ver = Version.get(i).text();
                    if (Pattern.matches("^[0-9]{1}.[0-9]{1,3}.[0-9]{1,3}$", ver)) {
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            return ver;
        }

        @Override
        protected void onPostExecute(String result) {
            // Version check the execution application.
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (result != null && result.length() > 0)
                mVer = result;
            else {
                if (!mVer.equals("0"))
                    mVer = null;
            }

            //BJH NullPointerException 2016.5.26 => 2017.05.04
            if (mVer == null) {
                Alert(mContext, getString(R.string.new_version_fail));
                return;
            }

            Log.i(THIS_FILE,"mVer:"+mVer);
            if (mVer != null && mVer.compareTo(ServicePrefs.DIAL_VERSION) > 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        Setting.this);
                builder.setMessage("말톡을 최신버전으로 업데이트해주세요.");
                builder.setPositiveButton(
                        getResources().getString(R.string.version_update),
                        new DialogInterface.OnClickListener() {
                            // 사용 안함
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO Auto-generated method stub
                                if (Fcm.mRegistrationID != null
                                        && Fcm.mRegistrationID.length() > 0) {
                                    try {
                                        mContext.startActivity(new Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse("market://details?id="
                                                        + DialMain.MAALTALK_PACKAGE_NAME)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        mContext.startActivity(new Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse("http://play.google.com/store/apps/details?id="
                                                        + DialMain.MAALTALK_PACKAGE_NAME)));
                                    }
                                } else {
                                    try {
                                        mContext.startActivity(new Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse(DialMain.MAALTALK_APK_URL)));
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        });
                builder.setNegativeButton(
                        getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // TODO Auto-generated method stub
                                dialog.cancel();
                            }
                        });
                builder.setTitle(mContext.getResources().getString(
                        R.string.title_new_version));

                builder.create();
                builder.show();
            } else {
                if (mVer != null && mVer.length() > 0) {
                    if (!mVer.equals("0"))
                        Alert(mContext, getString(R.string.new_version_status));
                    else
                        Alert(mContext, getString(R.string.new_version_cancel));
                } else
                    Alert(mContext, getString(R.string.new_version_fail));
            }

            super.onPostExecute(result);
        }

    }
}
