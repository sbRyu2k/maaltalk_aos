package com.dial070.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dial070.dao.DaoCategory;
import com.dial070.dao.DaoTrip;
import com.dial070.db.DBManager;
import com.dial070.db.TravelCategoryData;
import com.dial070.db.TravelStepData;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.dial070.utils.RecyclerAdapter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

public class CategoryActivityBackup extends Fragment implements SwipeRefreshLayout.OnRefreshListener, RecyclerAdapter.OnLoadMoreListener {
    final int REQUEST_CODE_PERMISSIONS = 1;
    final int REQUEST_CODE_PERMISSIONS_MORE = 2;
    private final String TAG = "CategoryActivity";
    private ArrayList<DaoCategory> mCategoryArrayList;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    private ProgressDialog progressDialog;
    private GetPhotoTask getPhotoTask;
    private GetPhotoTaskMore getPhotoTaskMore;
    private Context mContext;
    private AppPrefs mPrefs;
    SwipeRefreshLayout swipeRefresh;
    private Date startDate;

    private String userID;
    DBManager database;

    boolean isLoadDB=false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        mContext = getActivity();
        database = new DBManager(mContext);
        mPrefs = new AppPrefs(mContext);
        userID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

        mCategoryArrayList = new ArrayList<>();

        super.onCreate(savedInstanceState);
    }


    private void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPermissionRead = mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            List<String> permissions = new ArrayList<String>();
            if (hasPermissionRead != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_PERMISSIONS);
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                }
            } else {
                loadCategory();
            }
        } else {
            loadCategory();
        }
    }

    private void loadCategory(){

        database.open();
        database.createTravelTable();

        Cursor cursor = database.getTravelCategoryData();
        DaoCategory category;
        mCategoryArrayList=new ArrayList<>();
        //Log.i(TAG,"cursor count:"+cursor.getCount());
        if (cursor!=null && cursor.getCount() != 0) {
            isLoadDB=true;
            cursor.moveToFirst();
            do {
                String path=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_PATH));
                String country_name=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_COUNTRY_NAME));
                String country_code=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_COUNTRY_CODE));
                String start_date=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_STARTDATE));
                String end_date=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_ENDDATE));
                String category_num=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_CATEGORY_NUM));

                category=new DaoCategory();
                category.setFilepath1(path);
                category.setTitle(country_name);
                category.setCountryCode(country_code);
                category.setStartDate(start_date);
                category.setEndDate(end_date);
                category.setCategoryNum(category_num);
                mCategoryArrayList.add(category);
            } while (cursor.moveToNext());
            if (mCategoryArrayList!=null && mCategoryArrayList.size()>0){
                loadData();
            }

            database.close();
        }else {
            database.close();
            getPhotoTask = new GetPhotoTask();
            getPhotoTask.execute();
        }


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tabView = null;
        if (this.getTag().equals("travel")) {
            tabView = setTabViewTravel(inflater);

            checkPermission();
        }
        return tabView;
    }

    private View setTabViewTravel(LayoutInflater inflater) {
        View tabView = inflater.inflate(R.layout.activity_trip, null);

        Button btnRegister = tabView.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                Intent intent = new Intent(mContext, WebClient.class);
                intent.putExtra("url", "http://211.43.13.195:4001/user/?name=" + uid);
                intent.putExtra("title", "여행의 발자취");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP /*| Intent.FLAG_ACTIVITY_NO_ANIMATION*/);
                startActivity(intent);
            }
        });

        swipeRefresh = tabView.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        recyclerView = tabView.findViewById(R.id.listTrip);
        RecyclerDecoration spaceDecoration = new RecyclerDecoration(20);
        recyclerView.addItemDecoration(spaceDecoration);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(mContext, this);
        recyclerView.setAdapter(adapter);

        adapter.setLinearLayoutManager(layoutManager);
        adapter.setRecyclerView(recyclerView);

        return tabView;
    }

    /*@Override
    protected void onStart() {
        if (recyclerView!=null && adapter!=null){
            if (recyclerView.getVisibility()!=View.VISIBLE){
                adapter.notifyDataSetChanged();
                recyclerView.setVisibility(View.VISIBLE);
            }
        }

        super.onStart();
    }

    @Override
    protected void onStop() {
        if (recyclerView!=null){
            recyclerView.setVisibility(View.INVISIBLE);
        }

        super.onStop();
    }*/

    private ArrayList<DaoCategory> getCategory(String sStartDate, String sEndDate) {
        ArrayList<String> result = new ArrayList<>();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};

        int j = 1;
        int count = 0;
        ArrayList<DaoTrip> photoArrayList;
        Date endDate = new Date();
        Date startDate = new Date();

        do {
            if (j == 1) {
                endDate = new Date();
            } else {
                endDate = startDate;
            }

            long startTime=System.currentTimeMillis();
            Log.i(TAG,"##### "+j+" start #####");
            Log.i(TAG,"cursor start time:"+startTime);

            startDate = getPreviousYear(endDate);

            Log.i(TAG, "startDate:" + startDate.toString());

            Cursor cursor = mContext.getContentResolver().query(uri, projection, MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?", new String[]{"" + startDate.getTime(), "" + endDate.getTime()}, MediaStore.Images.ImageColumns.DATE_TAKEN + " desc");
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int lastIndex;
            while (cursor.moveToNext()) {
                String absolutePathOfImage = cursor.getString(columnIndex);
                String nameOfFile = cursor.getString(columnDisplayname);
                String dateTake = cursor.getString(columnDateTaken);

                lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
                lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;

                if (!TextUtils.isEmpty(absolutePathOfImage)) {
                    result.add(absolutePathOfImage);
                }
            }

            Log.i(TAG,"cursor count:"+cursor.getCount());
            long endTime=System.currentTimeMillis();
            Log.i(TAG,"cursor end time:"+endTime);
            Log.i(TAG,"cursor loading duration:"+(endTime-startTime)/1000);
            Log.i(TAG,"##### "+j+" end #####");

            photoArrayList = new ArrayList<>();
            for (String string : result) {
                //Log.i(TAG,"fileName:"+string);
                if (string != null && string.trim().length() > 0) {
                    DaoTrip daoTravelPhoto = getPhotoMetaData(string);
                    if (daoTravelPhoto!=null){
                        Log.i(TAG, "daoTravelPhoto getCountryCode:" + daoTravelPhoto.getCountryCode());
                        photoArrayList.add(daoTravelPhoto);
                    }

                }
            }
            count = photoArrayList.size();
            Log.i(TAG, "photoArrayList size:" + count);
            j++;
            if (j == 11) {
                break;
            }
            this.startDate=startDate;
        } while (count == 0);


        /*if (photoArrayList.size() > 0) {
            Collections.sort(photoArrayList, new Comparator<DaoTrip>() {
                @Override
                public int compare(DaoTrip p1, DaoTrip p2) {
                    return p2.getDatetime().compareTo(p1.getDatetime());
                }
            });
        }*/


        ArrayList<DaoCategory> categoryArrayList = new ArrayList<>();
        String prevCountryState = "";
        DaoCategory daoCategory;
        int size = photoArrayList.size();
        database.open();
        String categoryNum="";
        TravelStepData data;
        for (int i = 0; i < size; i++) {
            DaoTrip daoTrip = photoArrayList.get(i);
            daoCategory = new DaoCategory();
            if (daoTrip.getLocation_country_state().equals(prevCountryState)) {
                DaoCategory prevDaoCategory = categoryArrayList.get(categoryArrayList.size() - 1);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                daoCategory.setTitle(prevCountryState);
                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                daoCategory.setEndDate(daoTrip.getDate());
                categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);

                /*Log.i(TAG,"category prevCountryState:"+prevCountryState);
                Log.i(TAG,"category getTitle:"+daoCategory.getTitle());
                Log.i(TAG,"category getFilepath1:"+daoCategory.getFilepath1());
                Log.i(TAG,"category getEndDate:"+daoCategory.getEndDate());*/
            } else {
                categoryNum=userID+"_"+daoTrip.getCountryCode()+"_"+System.currentTimeMillis();

                prevCountryState = daoTrip.getLocation_country_state();
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(daoTrip.getCountryCode());
                daoCategory.setFilepath1(daoTrip.getFilepath());
                daoCategory.setStartDate(daoTrip.getDate());
                daoCategory.setEndDate(daoTrip.getDate());
                daoCategory.setCategoryNum(categoryNum);
                categoryArrayList.add(daoCategory);
            }

            try {
                data=new TravelStepData();
                data.setPath(daoTrip.getFilepath());
                data.setUri_path(getStringUriFromPath(daoTrip.getFilepath()));
                data.setAddress(daoTrip.getAddress());
                data.setCountry_name(daoTrip.getLocation_country());
                data.setCountry_code(daoTrip.getCountryCode());
                data.setLatitude(daoTrip.getLatitude());
                data.setLongitude(daoTrip.getLongitude());
                data.setOrigin_date(daoTrip.getDatetime());
                data.setSimple_date(daoTrip.getDate());
                data.setCategory_num(categoryNum);
                Log.i(TAG,"daoTrip country:"+daoTrip.getLocation_country());
                Log.i(TAG,"daoTrip path:"+daoTrip.getFilepath());
                Log.i(TAG,"daoTrip Category_num:"+categoryNum);
                database.insertTravelStep(data);
            }
            catch(SQLException e)
            {
                Log.e(TAG,"error:"+e.getLocalizedMessage());
            }

        }
        database.close();
        /*size = categoryArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoCategory category = categoryArrayList.get(i);
            String imgArray[] = category.getFilepath1().split("######");
            if (imgArray.length > 3) {
                category.setRandomNumber(randomNumber(imgArray.length));
                categoryArrayList.set(i, category);
            }
        }*/

        /*for (DaoCategory category:categoryArrayList){
            Log.i(TAG,"category title:"+category.getTitle());
            Log.i(TAG,"category filepath:"+category.getFilepath1());
            Log.i(TAG,"category start:"+category.getStartDate());
            Log.i(TAG,"category end:"+category.getEndDate());
        }*/

        /*Collection<DaoTrip> dataPointsCalledJohn =
                Collections2.filter(photoArrayList, countryStateEqualsTo("대한민국 대전광역시"));

        ArrayList<DaoTrip> arrayList=new ArrayList<>(dataPointsCalledJohn);
        for (DaoTrip daoTrip:arrayList){
            Log.i(TAG,"daoTrip:"+daoTrip.getAddress());
        }*/

        return categoryArrayList;
    }

    private ArrayList<DaoCategory> getCategoryMore() {
        ArrayList<String> result = new ArrayList<>();
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};

        int j = 1;
        int count = 0;
        ArrayList<DaoTrip> photoArrayList=new ArrayList<>();
        Date endDate = this.startDate;
        Date startDate2 = this.startDate;
        do {
            if (j == 1) {
                endDate = this.startDate;
            } else {
                endDate = startDate2;
            }

            startDate2 = getPreviousYear(endDate);

            Log.i(TAG, "startDate2:" + startDate2.toString());

            if (startDate2.getTime()<1260434943742.0f){
                break;
            }

            long startTime=System.currentTimeMillis();
            Log.i(TAG,"##### "+j+" more start #####");
            Log.i(TAG,"cursor start time:"+startTime);

            Cursor cursor = mContext.getContentResolver().query(uri, projection, MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?", new String[]{"" + startDate2.getTime(), "" + endDate.getTime()}, MediaStore.Images.ImageColumns.DATE_TAKEN + " desc");
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int lastIndex;
            while (cursor.moveToNext()) {
                String absolutePathOfImage = cursor.getString(columnIndex);
                String nameOfFile = cursor.getString(columnDisplayname);
                String dateTake = cursor.getString(columnDateTaken);

                lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
                lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;

                if (!TextUtils.isEmpty(absolutePathOfImage)) {
                    result.add(absolutePathOfImage);
                }
            }

            Log.i(TAG,"cursor count:"+cursor.getCount());
            long endTime=System.currentTimeMillis();
            Log.i(TAG,"cursor end time:"+endTime);
            Log.i(TAG,"cursor loading duration:"+(endTime-startTime)/1000);
            Log.i(TAG,"##### "+j+" more end #####");

            photoArrayList = new ArrayList<>();
            for (String string : result) {
                //Log.i(TAG,"fileName:"+string);
                if (string != null && string.trim().length() > 0) {
                    DaoTrip daoTravelPhoto = getPhotoMetaData(string);
                    if (daoTravelPhoto != null) {
                        //Log.i(TAG,"daoTravelPhoto getLocation_country_state:"+daoTravelPhoto.getLocation_country_state());
                        //Log.i(TAG,"daoTravelPhoto getFilepath1:"+daoTravelPhoto.getFilepath1());
                        if (daoTravelPhoto!=null){
                            Log.i(TAG, "daoTravelPhoto getCountryCode:" + daoTravelPhoto.getCountryCode());
                            photoArrayList.add(daoTravelPhoto);
                        }
                    }
                }
            }
            count = photoArrayList.size();
            Log.i(TAG, "photoArrayList size:" + count);
            j++;
            if (j == 11) {
                break;
            }
            this.startDate=startDate2;
        } while (count == 0);


        /*if (photoArrayList.size() > 0) {
            Collections.sort(photoArrayList, new Comparator<DaoTrip>() {
                @Override
                public int compare(DaoTrip p1, DaoTrip p2) {
                    return p2.getDatetime().compareTo(p1.getDatetime());
                }
            });
        }*/


        ArrayList<DaoCategory> categoryArrayList = new ArrayList<>();
        String prevCountryState = "";
        DaoCategory daoCategory;
        database.open();
        String categoryNum="";
        TravelStepData data;
        int size = photoArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoTrip daoTrip = photoArrayList.get(i);
            daoCategory = new DaoCategory();
            if (daoTrip.getLocation_country_state().equals(prevCountryState)) {
                DaoCategory prevDaoCategory = categoryArrayList.get(categoryArrayList.size() - 1);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                daoCategory.setEndDate(daoTrip.getDate());
                categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);

                /*Log.i(TAG,"category prevCountryState:"+prevCountryState);
                Log.i(TAG,"category getTitle:"+daoCategory.getTitle());
                Log.i(TAG,"category getFilepath1:"+daoCategory.getFilepath1());
                Log.i(TAG,"category getEndDate:"+daoCategory.getEndDate());*/
            } else {
                categoryNum=userID+"_"+daoTrip.getCountryCode()+"_"+System.currentTimeMillis();

                prevCountryState = daoTrip.getLocation_country_state();
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(daoTrip.getCountryCode());
                daoCategory.setFilepath1(daoTrip.getFilepath());
                daoCategory.setStartDate(daoTrip.getDate());
                daoCategory.setEndDate(daoTrip.getDate());
                daoCategory.setCategoryNum(categoryNum);
                categoryArrayList.add(daoCategory);
            }

            try {
                data=new TravelStepData();
                data.setPath(daoTrip.getFilepath());
                data.setUri_path(getStringUriFromPath(daoTrip.getFilepath()));
                data.setAddress(daoTrip.getAddress());
                data.setCountry_name(daoTrip.getLocation_country());
                data.setCountry_code(daoTrip.getCountryCode());
                data.setLatitude(daoTrip.getLatitude());
                data.setLongitude(daoTrip.getLongitude());
                data.setOrigin_date(daoTrip.getDatetime());
                data.setSimple_date(daoTrip.getDate());
                data.setCategory_num(categoryNum);
                Log.i(TAG,"daoTrip country:"+daoTrip.getLocation_country());
                Log.i(TAG,"daoTrip path:"+daoTrip.getFilepath());
                Log.i(TAG,"daoTrip uripath:"+getStringUriFromPath(daoTrip.getFilepath()));
                database.insertTravelStep(data);
            }
            catch(SQLException e)
            {
                Log.e(TAG,"error:"+e.getLocalizedMessage());
            }
        }

        database.close();

        /*size = categoryArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoCategory category = categoryArrayList.get(i);
            String imgArray[] = category.getFilepath1().split("######");
            if (imgArray.length > 3) {
                category.setRandomNumber(randomNumber(imgArray.length));
                categoryArrayList.set(i, category);
            }
        }*/

        /*for (DaoCategory category:categoryArrayList){
            Log.i(TAG,"category title:"+category.getTitle());
            Log.i(TAG,"category filepath:"+category.getFilepath1());
            Log.i(TAG,"category start:"+category.getStartDate());
            Log.i(TAG,"category end:"+category.getEndDate());
        }*/

        /*Collection<DaoTrip> dataPointsCalledJohn =
                Collections2.filter(photoArrayList, countryStateEqualsTo("대한민국 대전광역시"));

        ArrayList<DaoTrip> arrayList=new ArrayList<>(dataPointsCalledJohn);
        for (DaoTrip daoTrip:arrayList){
            Log.i(TAG,"daoTrip:"+daoTrip.getAddress());
        }*/


        return categoryArrayList;
    }

    private DaoTrip getPhotoMetaData(String file) {
        /*long startTime=System.currentTimeMillis();
        Log.i(TAG,"##### getPhotoMetaData start #####");
        Log.i(TAG,"getPhotoMetaData start time:"+startTime);*/

        DaoTrip daoTravelPhoto = null;
        try {
            ExifInterface exifInterface = new ExifInterface(file);

            if (exifInterface != null) {
                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                String datetime = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null && datetime != null) {
                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);

                    SimpleDateFormat sFormatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                    sFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    SimpleDateFormat convertFormat = new SimpleDateFormat("yyyy.MM.dd");
                    convertFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                    //Log.i(TAG, "latitude:" + latitude);
                    //Log.i(TAG, "longitude:" + longitude);
                    if ((latitude < 33.11111111 || latitude > 43.00972222) || (longitude < 124.19583333 || longitude > 131.87222222)) {
                        DaoTrip daoTravelPhotoAddressInfo = convertToAddress(latitude, longitude);
                        if (daoTravelPhotoAddressInfo != null && daoTravelPhotoAddressInfo.getLocation_country() !=null) {
                            if (daoTravelPhotoAddressInfo.getAddress() != null) {
                                daoTravelPhoto = new DaoTrip();
                                daoTravelPhoto.setFilepath(file);
                                daoTravelPhoto.setDatetime(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                                daoTravelPhoto.setLatitude(daoTravelPhotoAddressInfo.getLatitude());
                                daoTravelPhoto.setLongitude(daoTravelPhotoAddressInfo.getLongitude());
                                daoTravelPhoto.setAddress(daoTravelPhotoAddressInfo.getAddress());
                                daoTravelPhoto.setLocation_country(daoTravelPhotoAddressInfo.getLocation_country());
                                daoTravelPhoto.setCountryCode(daoTravelPhotoAddressInfo.getCountryCode());
                                String state = daoTravelPhotoAddressInfo.getLocation_state();
                                daoTravelPhoto.setLocation_country_state(daoTravelPhotoAddressInfo.getLocation_country());
                                daoTravelPhoto.setLocation_state(state);

                                Date d = null;
                                try {
                                    d = sFormatter.parse(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                                    String dateFormat = convertFormat.format(d);
                                    daoTravelPhoto.setDate(dateFormat);
                                } catch (ParseException e) {
                                    Log.e(TAG, "error:" + e.getLocalizedMessage());
                                    daoTravelPhoto.setDate("");
                                }

                                /*long endTime=System.currentTimeMillis();
                                Log.i(TAG,"getPhotoMetaData end time:"+endTime);
                                Log.i(TAG,"getPhotoMetaData duration:"+(endTime-startTime)/1000);
                                Log.i(TAG,"##### getPhotoMetaData end #####");*/

                                return daoTravelPhoto;
                            }
                        }
                    }


                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            Log.e(TAG,"error:"+e.getMessage());
        }

        return daoTravelPhoto;
    }

    private DaoTrip getPhotoMetaDataBackup(String file) {
        DaoTrip daoTravelPhoto = null;
        try {
            ExifInterface exifInterface = new ExifInterface(file);

            if (exifInterface != null) {
                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                String datetime = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null && datetime != null) {
                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);

                    DaoTrip daoTravelPhotoAddressInfo = convertToAddress(latitude, longitude);
                    if (daoTravelPhotoAddressInfo != null) {
                        if (daoTravelPhotoAddressInfo.getAddress() != null) {
                            daoTravelPhoto = new DaoTrip();
                            daoTravelPhoto.setFilepath(file);
                            daoTravelPhoto.setDatetime(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                            daoTravelPhoto.setLatitude(daoTravelPhotoAddressInfo.getLatitude());
                            daoTravelPhoto.setLongitude(daoTravelPhotoAddressInfo.getLongitude());
                            daoTravelPhoto.setAddress(daoTravelPhotoAddressInfo.getAddress());
                            daoTravelPhoto.setLocation_country(daoTravelPhotoAddressInfo.getLocation_country());
                            daoTravelPhoto.setLocation_state(daoTravelPhotoAddressInfo.getLocation_state());

                            return daoTravelPhoto;
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            //Log.e(TAG,"error:"+e.getMessage());
        }

        return daoTravelPhoto;
    }

    /*private double[] getLocation(String file) {
        try {
            androidx.exifinterface.media.ExifInterface exifInterface = new androidx.exifinterface.media.ExifInterface(file);
            if (exifInterface != null) {
                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null) {
                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);
                    return new double[]{latitude, longitude};
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            Log.e(TAG, "error:" + e.getMessage());
        }
        return null;
    }*/

    private static double convertRationalLatLonToDouble(String rationalString, String ref) {
        try {
            String[] parts = rationalString.split(",", -1);

            String[] pair;
            pair = parts[0].split("/", -1);
            double degrees = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            pair = parts[1].split("/", -1);
            double minutes = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            pair = parts[2].split("/", -1);
            double seconds = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            double result = degrees + (minutes / 60.0) + (seconds / 3600.0);
            if ((ref.equals("S") || ref.equals("W"))) {
                return -result;
            } else if (ref.equals("N") || ref.equals("E")) {
                return result;
            } else {
                // Not valid
                throw new IllegalArgumentException();
            }
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            // Not valid
            throw new IllegalArgumentException();
        }
    }

    private DaoTrip convertToAddress(double latitude, double longitude) {
        DaoTrip daoTravelPhoto = null;
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(mContext, Locale.KOREA);

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            Log.e(TAG, "error:" + e.getMessage());
        }

        Address address = addresses.get(0);
        ArrayList<String> addressFragments = new ArrayList<String>();
        String sAddress = "";
        if (addresses != null || addresses.size() > 0) {
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
                Log.i(TAG, "address:" + address.getAddressLine(i));
            }
            sAddress = address.getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            //String city = address.getLocality();
            String state = address.getAdminArea();
            String country = address.getCountryName();
            String countryCode = address.getCountryCode();

            /*Log.i(TAG,"country:"+country);
            Log.i(TAG,"state:"+state);
            Log.i(TAG,"city:"+city);
            Log.i(TAG,"countryCode:"+countryCode);
            Log.i(TAG,"adminArea:"+adminArea);
            Log.i(TAG,"subAdminArea:"+subAdminArea);
            Log.i(TAG,"subThoroughfare:"+subThoroughfare);
            Log.i(TAG,"featureName:"+featureName);
            Log.i(TAG,"subLocality:"+subLocality);
            Log.i(TAG,"thoroughfare:"+thoroughfare);
            Log.i(TAG,"premises:"+premises);*/

            daoTravelPhoto = new DaoTrip();
            daoTravelPhoto.setAddress(sAddress);
            daoTravelPhoto.setLocation_country(country);
            daoTravelPhoto.setLocation_state(state);
            daoTravelPhoto.setLatitude(latitude);
            daoTravelPhoto.setLongitude(longitude);
            daoTravelPhoto.setCountryCode(countryCode);
        }

        return daoTravelPhoto;
    }

    public void loading() {
        //로딩
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog = new ProgressDialog(mContext);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("잠시만 기다려 주세요");
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.setCancelable(true);
                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                if (getPhotoTask != null && !getPhotoTask.isCancelled()) {
                                    getPhotoTask.cancel(true);
                                }
                            }
                        });
                        progressDialog.show();
                    }
                }, 0);
    }

    public void loadingEnd() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 0);
    }

    @Override
    public void onRefresh() {
        database.open();
        database.dropTravelData();
        database.close();

        isLoadDB=false;

        adapter.setInitPreviousTotalItemCount();
        adapter.setMoreLoading(false);

        checkPermission();

        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onLoadMore() {
        Log.i(TAG,"onLoadMore");
        if (!isLoadDB){
            adapter.setProgressMore(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ///////이부분에을 자신의 프로젝트에 맞게 설정하면 됨
                    //다음 페이지? 내용을 불러오는 부분
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int hasPermissionRead = mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                        List<String> permissions = new ArrayList<String>();
                        if (hasPermissionRead != PackageManager.PERMISSION_GRANTED) {
                            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }

                        if (!permissions.isEmpty()) {
                            requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_PERMISSIONS);
                            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                            }
                        } else {
                            getPhotoTaskMore = new GetPhotoTaskMore();
                            getPhotoTaskMore.execute();
                        }
                    } else {
                        getPhotoTaskMore = new GetPhotoTaskMore();
                        getPhotoTaskMore.execute();
                    }
                }
            }, 500);
        }

    }

    private void loadData() {
        if (mCategoryArrayList!=null){
            adapter.addAll(mCategoryArrayList);
        }

    }

    class GetPhotoTask extends AsyncTask<Void, Integer, ArrayList<DaoCategory>> {
        @Override
        protected void onPreExecute() {
            loading();

            super.onPreExecute();
        }

        @Override
        protected ArrayList<DaoCategory> doInBackground(Void... voids) {
            String sStartDate = "2019-12-01 00:00:00";
            String sEndDate = "2019-12-31 23:59:59";
            /**
             * 설정된 날짜와 상관없이 현재는 최근 1년 사진만 가져오게 변경
             *
             */
            ArrayList<DaoCategory> photoArrayList = getCategory(sStartDate, sEndDate);
            if (photoArrayList!=null && photoArrayList.size()>0){
                try {
                    database.open();
                    TravelCategoryData data=new TravelCategoryData();
                    for (DaoCategory category:photoArrayList){
                        data.setPath(category.getFilepath1());
                        data.setCountry_name(category.getTitle());
                        data.setStart_date(category.getStartDate());
                        data.setEnd_date(category.getEndDate());
                        data.setCategory_num(category.getCategoryNum());
                        data.setPath(category.getFilepath1());
                        String imgArray[]=category.getFilepath1().split("######");
                        data.setPhoto_count(imgArray.length);
                        database.insertTravelCategory(data);
                    }
                    database.close();
                }
                catch(SQLException e)
                {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }
            }


            return photoArrayList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        protected void onPostExecute(ArrayList<DaoCategory> result) {
            loadingEnd();
            mCategoryArrayList = result;
            loadData();
            //adapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }

        //Task가 취소되었을때 호출
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    class GetPhotoTaskMore extends AsyncTask<Void, Integer, ArrayList<DaoCategory>> {
        @Override
        protected void onPreExecute() {
            //loading();
            super.onPreExecute();
        }

        @Override
        protected ArrayList<DaoCategory> doInBackground(Void... voids) {
            Log.i(TAG,"GetPhotoTaskMore doInBackground");
            String sStartDate = "2019-12-01 00:00:00";
            String sEndDate = "2019-12-31 23:59:59";
            /**
             * 설정된 날짜와 상관없이 현재는 최근 1년 사진만 가져오게 변경
             *
             */
            ArrayList<DaoCategory> photoArrayList = getCategoryMore();
            if (photoArrayList!=null && photoArrayList.size()>0){
                try {
                    database.open();
                    TravelCategoryData data=new TravelCategoryData();
                    for (DaoCategory category:photoArrayList){
                        data.setPath(category.getFilepath1());
                        data.setCountry_name(category.getTitle());
                        data.setStart_date(category.getStartDate());
                        data.setEnd_date(category.getEndDate());
                        data.setCategory_num(category.getCategoryNum());
                        data.setPath(category.getFilepath1());
                        String imgArray[]=category.getFilepath1().split("######");
                        data.setPhoto_count(imgArray.length);
                        database.insertTravelCategory(data);
                    }
                    database.close();
                }
                catch(SQLException e)
                {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }
            }


            return photoArrayList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        protected void onPostExecute(ArrayList<DaoCategory> result) {
            Log.i(TAG,"GetPhotoTaskMore onPostExecute:"+result.size());
            if (result!=null && result.size()>0){
                //loadingEnd();
                mCategoryArrayList.clear();
                adapter.setProgressMore(false);
                adapter.setMoreLoading(false);
                mCategoryArrayList = result;
                //adapter.notifyDataSetChanged();

                adapter.addItemMore(mCategoryArrayList);


                int totalHeight = recyclerView.getHeight();

                View v=LayoutInflater.from(mContext).inflate(R.layout.trip_item,null,false);
                v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                int height = v.getMeasuredHeight();
                Log.i(TAG,"height:"+height);
                Log.i(TAG,"totalHeight:"+totalHeight);

                if (totalHeight>0 && height>0 && adapter.getItemCount()>0){
                    if (totalHeight>height*adapter.getItemCount()){
                        adapter.setMoreLoading(true);
                        onLoadMore();
                        //adapter.setProgressMore(true);
                        //adapter.setProgressMore(false);
                    }
                }
            }else {
                adapter.setProgressMore(false);
                adapter.setMoreLoading(false);
            }


            super.onPostExecute(result);
        }

        //Task가 취소되었을때 호출
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    /**
     * i년 전의 날을 구한다.
     */
    public Date getPreviousYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -1);

        return cal.getTime();
    }

    private List<Integer> randomNumber(int size) {
        int a[] = new int[3];
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i <= 2; i++) {
            a[i] = r.nextInt(size);

            for (int j = 0; j < i; j++) {
                if (a[i] == a[j]) {
                    i--;
                }
            }
        }

        for (int value : a) {
            list.add(value);
        }

        //Collections.sort(list);
        return list;

    }

    @Override
    public void onDestroy() {
        if (getPhotoTask != null && !getPhotoTask.isCancelled()) {
            getPhotoTask.cancel(true);
        }

        if (getPhotoTaskMore != null && !getPhotoTaskMore.isCancelled()) {
            getPhotoTaskMore.cancel(true);
        }

        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    getPhotoTask = new GetPhotoTask();
                    getPhotoTask.execute();
                } else {
                    Toast.makeText(mContext, "사진을 불러 올 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUEST_CODE_PERMISSIONS_MORE:
                if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    getPhotoTaskMore = new GetPhotoTaskMore();
                    getPhotoTaskMore.execute();
                } else {
                    Toast.makeText(mContext, "사진을 불러 올 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public String getStringUriFromPath(String path){
        Uri fileUri = Uri.parse( path );
        String filePath = fileUri.getPath();
        Cursor cursor = mContext.getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + filePath + "'", null, null );
        cursor.moveToNext();
        int id = cursor.getInt( cursor.getColumnIndex( "_id" ) );
        Uri uri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id );

        return uri.toString();
    }
}
