package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class NaverListAdapter extends BaseAdapter {

	private Context mContext;
	
	private List<NaverResult> mResultData; 	
		
	public NaverListAdapter(Context context)
	{
		mContext = context;
		mResultData = null;
	}		
	
	public void clear()
	{
		if(mResultData != null && mResultData.size() > 0) mResultData.clear();
		notifyDataSetChanged();
	}
	
	
	public void setData(ArrayList<NaverResult> data)
	{
		mResultData = data;
	}

	
	private NaverResult read(int offset)
	{
		if(mResultData == null || mResultData.size() == 0) return null;
		
		if (offset < 0 || offset >= mResultData.size()) return null;
			
		return mResultData.get(offset);
	}		
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mResultData == null) return 0;
		return mResultData.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return read(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if(mResultData == null) return null;
		
		NaverListView naverView = null;
		NaverResult item = read(arg0);
		if(item != null)
		{	
			if(arg1 == null)
			{
				//naverView = new NaverListView(mContext, item);
				naverView = new NaverListView(mContext);
			}
			else
			{
				naverView = (NaverListView) arg1;
			}
			
			if(item != null)
				naverView.setData(item.getTitle(), item.getAddress(), item.getTelephone());
			
		}
		return naverView;
	}

}
