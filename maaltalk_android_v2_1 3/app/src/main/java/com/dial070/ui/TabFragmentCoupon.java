package com.dial070.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

public class TabFragmentCoupon extends Fragment {
    public final static String BROADCAST_MESSAGE = "RefreshTabFragmentCoupon";
    private static final String THIS_FILE = "TabFragmentCoupon";
    private String url;
    private BroadcastReceiver mReceiver = null;
    private WebView webView;
    private ProgressBar pbLoading;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        url = getArguments().getString("url");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tabView=null;
        if (this.getTag().equals("coupon")){
            tabView=setTabViewCoupon(inflater);
            //tabView = inflater.inflate(R.layout.tab_coupon, null);
        }
        return tabView;
    }

    private View setTabViewCoupon(LayoutInflater inflater){
        final View tabView = inflater.inflate(R.layout.tab_coupon, null);
        webView = tabView.findViewById(R.id.web_view);
        pbLoading=tabView.findViewById(R.id.pbLoading);
        pbLoading.setVisibility(View.VISIBLE);
        webView.setVisibility(View.VISIBLE);
        WebSettings set = webView.getSettings();
        set.setSavePassword(false);
        set.setJavaScriptEnabled(true);
        set.setDomStorageEnabled(true);
        set.setAllowFileAccess(false);
        set.setSupportZoom(true);
        set.setBuiltInZoomControls(true);
        //set.setAppCacheEnabled(true);
        set.setJavaScriptCanOpenWindowsAutomatically(true);
        set.setLoadWithOverviewMode(true);
        set.setUseWideViewPort(true);


        getActivity().getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        set.setRenderPriority(WebSettings.RenderPriority.HIGH);
        set.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webView.setVerticalScrollbarOverlay(true);
        // mWeb.setBackgroundColor(Color.TRANSPARENT);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // BJH 2017.02.22 안드로이드(Android 5.0) Lollipop Webview issue
            webView.getSettings().setMixedContentMode(WebSettings
                    .MIXED_CONTENT_ALWAYS_ALLOW);

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }
        webView.loadUrl(url);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                pbLoading.setProgress(newProgress);
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(THIS_FILE,"url:"+url);
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pbLoading.setVisibility(View.INVISIBLE);

                super.onPageFinished(view, url);
            }
        });

        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                //This is the filter
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;


                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        getActivity().onBackPressed();
                    }

                    return true;
                }

                return false;
            }
        });

        return tabView;
    }

    @Override
    public void onResume() {
        super.onResume();

        registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    /** 동적으로(코드상으로) 브로드 캐스트를 등록한다. **/
    private void registerReceiver(){
        /** 1. intent filter를 만든다
         *  2. intent filter에 action을 추가한다.
         *  3. BroadCastReceiver를 익명클래스로 구현한다.
         *  4. intent filter와 BroadCastReceiver를 등록한다.
         * */
        if(mReceiver != null) return;

        final IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(BROADCAST_MESSAGE);

        this.mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int receviedData = intent.getIntExtra("value",0);
                if(intent.getAction().equals(BROADCAST_MESSAGE)){
                    //Toast.makeText(context, "recevied Data : "+receviedData, Toast.LENGTH_SHORT).show();
                    webView.reload();
                    if (pbLoading!=null){
                        pbLoading.setVisibility(View.VISIBLE);
                    }
                }
            }
        };

        getActivity().registerReceiver(mReceiver, theFilter);

    }

    /** 동적으로(코드상으로) 브로드 캐스트를 종료한다. **/
    private void unregisterReceiver() {
        if(mReceiver != null){
            getActivity().unregisterReceiver(mReceiver);
            mReceiver = null;
        }

    }
}
