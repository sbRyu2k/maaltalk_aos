package com.dial070.ui;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


//import com.dial070.utils.AppPrefs;
import com.dial070.db.DBManager;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

public class SmsListAdapter extends BaseAdapter {
	static String THIS_FILE = "DIAL070_SMSLIST_ADAPTER";
	private Context mContext;
//	private AppPrefs mPrefs;
	
	private DBManager mDatabase;
	private Cursor mCursor;
	private int mOption;
	private boolean mDeleteMode;
//	private boolean mAllItemChecked;
	
	private ArrayList<String> mSelectedList;
	
	public SmsListAdapter(Context context)
	{
		mContext = context;
//		mPrefs = new AppPrefs(context);
	
		//default all desc...
		mOption = 0;
		
		mDeleteMode = false;
//		mAllItemChecked = false;
		
		mCursor = null;
		mSelectedList = new ArrayList<String>();
		
		// Db
		mDatabase = new DBManager(mContext);
		mDatabase.open();
				
	}
	
	public void destroyAdapter()
	{
		if(mCursor != null) mCursor.close();
		if(mDatabase != null && mDatabase.isOpen()) mDatabase.close();
	}
	
	public void delete(String number)
	{
		mDatabase.deleteSmsMsg(number);
		mDatabase.deleteSmsList(number);
	}
	
	public void setQueryOption(int option)
	{
		mOption = option;
	}
	
	public void setDeleteMode(boolean mode)
	{
		mDeleteMode = mode;
	}
		
	public boolean loadSmsMsgData()
	{
		if(mCursor != null) 
		{
			mCursor.close();
			mCursor = null;
		}
		
		//if (ServicePrefs.mUseMSG) //BJH 푸시메시지 갱신을 위하여
		mCursor = mDatabase.getAllSmsList(mOption);
		
		if(mCursor == null)
		{
			return false;
		}		
		
		return true;
	}
	
	private boolean move(int offset)
	{
		if(mCursor == null) return false;
		return mCursor.moveToPosition(offset);		
	}
	
	public boolean setCheckedItem(String number, boolean selected)
	{
		if(mSelectedList == null) return false;

		if (selected)
		{
			if (!mSelectedList.contains(number))
				mSelectedList.add(number);
		}
		else
		{
			if (mSelectedList.contains(number))
				mSelectedList.remove(number);			
		}
		
		return true;
	}	
	
	public boolean setCheckedAllItem(boolean selected)//MWS 메시지 삭제 체크 박스 오류 수정
	{
		if(mSelectedList == null) return false;
		mSelectedList.clear();
		
		if (selected)
		{
			if (mCursor != null && mCursor.getCount() != 0){
				mCursor.moveToFirst();
				do {
					mSelectedList.add(mCursor.getString(mCursor.getColumnIndex(SmsListData.FIELD_PHONE_NUMBER)));
				} while (mCursor.moveToNext());
			}
		}
						
		return true;
	}	
	
	public ArrayList<String> getCheckedList()
	{
		return mSelectedList;
	}
	
	public int getCheckedCount()
	{
		return mSelectedList.size();
	}
	
	public void checkClear()
	{
		mSelectedList.clear();	
	}	
	
	@Override
	public int getCount() {
		if(mCursor == null) return 0;
		return mCursor.getCount();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SmsMsgListView smsListlView = null;

		//GET VIEW
		if(convertView == null)
		{
			smsListlView = new SmsMsgListView(mContext);
		}
		else
		{
			smsListlView = (SmsMsgListView) convertView;
		}		
		
		
		//GET DATA
		if (!move(position) ) return null;
		
		String number = mCursor.getString(mCursor.getColumnIndex(SmsListData.FIELD_PHONE_NUMBER));
		String last_msg = mCursor.getString(mCursor.getColumnIndex(SmsListData.FIELD_LAST_MSG));
		long last_time = mCursor.getLong(mCursor.getColumnIndex(SmsListData.FIELD_LAST_DATE));
		int sms_type = mCursor.getInt(mCursor.getColumnIndex(SmsListData.FIELD_MSG_TYPE));
		int sms_new = mCursor.getInt(mCursor.getColumnIndex(SmsListData.FIELD_NEW_MSG));
		
		//BJH 2016.09.22
		if(last_msg.startsWith(mContext.getString(R.string.pns_vms_title)+"|")){
			String[] strArr = last_msg.split("\\|");
			last_msg = strArr[0];
		}
		
		smsListlView.setView(mDeleteMode,mSelectedList.contains(number));
		
		smsListlView.setData(number, sms_type, last_time, last_msg, sms_new);

		/*if( (position % 2) == 0)
		{
			smsListlView.setBackgroundResource(R.drawable.list_item_even_background);
		}
		else
		{
			smsListlView.setBackgroundResource(R.drawable.list_item_odd_background);
		}*/
		return smsListlView;
	}
}
