package com.dial070.ui;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.dial070.utils.Log;

import java.lang.reflect.Method;

/**
 * Created by DialCommunications on 2017-02-21.
 */

public class MarshmallowResponse {

    public static void init() {
        // for memory load.
    }

    public static Notification createNotification(Context context, PendingIntent pendingIntent, String channel_id, String title, String text, int iconId) {
        Log.i("MarshmallowResponse","createNotification:"+title+",channel_id:"+channel_id);
        Notification notification;
        if (Build.VERSION.SDK_INT >= 29) { // 2020.01.28 android 10 이슈
            notification = buildAndroidQNotificationWithBuilder(context, pendingIntent, channel_id, title, text, iconId);
        }else if (Build.VERSION.SDK_INT >= 26) { // 2018.03.12 오레오 이슈
            notification = buildOreoNotificationWithBuilder(context, pendingIntent, channel_id, title, text, iconId);
        } else {
            notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
//            if (isNotificationBuilderSupported()) {
//                notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
//            } else {
//                notification = buildNotificationPreHoneycomb(context, pendingIntent, title, text, iconId);
//            }
        }
        return notification;
    }

    public static Notification createSFSNotification(Context context, PendingIntent pendingIntent, String channel_id, String title, String text, int iconId) {
        Notification notification;
        if (Build.VERSION.SDK_INT >= 26) { // 2018.03.12 오레오 이슈
            notification = buildOreoSFSNotificationWithBuilder(context, pendingIntent, channel_id, title, text, iconId);
        } else {
            notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
//            if (isNotificationBuilderSupported()) {
//                notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
//            } else {
//                notification = buildNotificationPreHoneycomb(context, pendingIntent, title, text, iconId);
//            }
        }
        return notification;
    }

//    public static boolean isNotificationBuilderSupported() {
//        try {
//            return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) && Class.forName("android.app.Notification.Builder") != null;
//        } catch (ClassNotFoundException e) {
//            return false;
//        }
//    }

    @SuppressWarnings("deprecation")
    private static Notification buildNotificationPreHoneycomb(Context context, PendingIntent pendingIntent, String title, String text, int iconId) {
        Notification notification = new Notification(iconId, "", System.currentTimeMillis());
        try {
            // try to call "setLatestEventInfo" if available
            Method m = notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
            m.invoke(notification, context, title, text, pendingIntent);
        } catch (Exception e) {
            // do nothing
        }
        return notification;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressWarnings("deprecation")
    private static Notification buildNotificationWithBuilder(Context context, PendingIntent pendingIntent, String title, String text, int iconId) {
        android.app.Notification.Builder builder = new android.app.Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setSmallIcon(iconId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return builder.build();
        } else {
            return builder.getNotification();
        }
    }

    /**
     * ADDED category 2020.08.20
     */
    private static Notification buildAndroidQNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channel_id, String title, String text, int iconId) {
        if (Build.VERSION.SDK_INT >= 29) {
            Notification notification = new NotificationCompat.Builder(context, channel_id)
                    .setSmallIcon(iconId)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setFullScreenIntent(pendingIntent,true)
                    .setAutoCancel(true)
                    // .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_CALL)
                    .build();

            return notification;
        } else
            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
    }


    private static Notification buildOreoNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channel_id, String title, String text, int iconId) {
        if (Build.VERSION.SDK_INT >= 26) {
            Notification notification = new NotificationCompat.Builder(context, channel_id)
                    .setSmallIcon(iconId)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setContentIntent(pendingIntent)
                   // .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .build();

            return notification;
        } else
            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
    }

    private static Notification buildOreoSFSNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channel_id, String title, String text, int iconId) {
        if (Build.VERSION.SDK_INT >= 26) {
            Notification notification = new NotificationCompat.Builder(context, channel_id)
                    .setSmallIcon(iconId)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setContentIntent(pendingIntent)
                    // .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_MIN)
                    .build();

            return notification;
        } else
            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId);
    }

}
