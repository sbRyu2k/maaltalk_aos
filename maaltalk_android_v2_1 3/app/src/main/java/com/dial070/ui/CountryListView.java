package com.dial070.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CountryListView extends LinearLayout {
	
	private ImageView mFlagIcon;
	private TextView mCountryName;
	private TextView mCountryCode;	
	private TextView mCountryTime;
	//private TextView mCountryFreeCall;
	
	private CountryItem mCountryItem;
	private Context mContext;
	
	public CountryListView(Context context, CountryItem item)
	{
		super(context);
		mContext = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.country_list_item, this, true);
		
		mFlagIcon = (ImageView) findViewById(R.id.country_flag_icon);
		mFlagIcon.setImageResource(context.getResources().getIdentifier(item.Flag, "drawable", mContext.getPackageName()));
		mCountryName = (TextView) findViewById(R.id.country_name);
		mCountryName.setText(item.getName());
		mCountryCode = (TextView) findViewById(R.id.country_code);
		mCountryCode.setText(item.Code);
		mCountryTime = (TextView) findViewById(R.id.country_time);
		mCountryTime.setText(getGmtTime(item.Gmt));
		//mCountryFreeCall = (TextView) findViewById(R.id.country_free_call);

//		String freeCallType = "";
//		if(item.FreeMobile > 0 && item.FreeWired > 0)
//			freeCallType = mContext.getResources().getString(R.string.mobile) + "/" + mContext.getResources().getString(R.string.wired);		
//		else if(item.FreeWired > 0)
//			freeCallType = mContext.getResources().getString(R.string.wired);	
//		else if(item.FreeMobile > 0)
//			freeCallType = mContext.getResources().getString(R.string.mobile);	
//		mCountryFreeCall.setText(freeCallType);
		
		this.mCountryItem = item;
	}
	
	public void setText(int index, String data)
	{
		if(index == 0)
		{
			mCountryName.setText(data);
		}
		else if(index == 1)
		{
			mCountryCode.setText(data);
		}

	}

	
	public TextView getCountryCode()
	{
		return mCountryCode;
	}	
	
	public CountryItem getItem()
	{
		return mCountryItem;
	}
	
	public void setFlagIcon(int res)
	{
		mFlagIcon.setImageResource(res);
	}	
	
	public void setGmtTime(String aGmt)
	{
		mCountryTime.setText(getGmtTime(aGmt));		
	}
	
	public String getGmtTime(String aGmt)
	{
		if ( !(aGmt.length() == 5 && (aGmt.startsWith("+") || aGmt.startsWith("-")))) return "";

		String gmtZone = String.format("GMT%s:%s", aGmt.substring(0, 3), aGmt.substring(3, aGmt.length()));

		Log.d("GMT TIME","COUNTRY CODE=" + mCountryCode.getText() + " GMT OFFSET=" +gmtZone);
		
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("MMM-dd aa HH:mm", Locale.getDefault());
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone(gmtZone));

		//Local time zone   
//		SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm",Locale.getDefault());
	
	    return dateFormatGmt.format(new Date());	
	}
	
	public void setFreeCall(int wired, int mobile)
	{
//		String freeCallType = "";
//
//		if(mobile > 0 && wired > 0)
//			freeCallType = mContext.getResources().getString(R.string.mobile) + "/" + mContext.getResources().getString(R.string.wired);		
//		else if(wired > 0)
//			freeCallType = mContext.getResources().getString(R.string.wired);	
//		else if(mobile > 0)
//			freeCallType = mContext.getResources().getString(R.string.mobile);			
//		mCountryFreeCall.setText(freeCallType);
	}
	
}
