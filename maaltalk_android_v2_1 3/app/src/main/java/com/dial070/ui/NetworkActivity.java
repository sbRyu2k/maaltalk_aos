package com.dial070.ui;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.artfulbits.aiCharts.ChartView;
import com.artfulbits.aiCharts.Base.ChartArea;
import com.artfulbits.aiCharts.Base.ChartAxis;
import com.artfulbits.aiCharts.Base.ChartPoint;
import com.artfulbits.aiCharts.Base.ChartSeries;
import com.artfulbits.aiCharts.Types.ChartTypes;

import com.dial070.DialMain;
import com.dial070.sip.api.SipManager;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

import com.dial070.sip.service.SipService;

import com.dial070.maaltalk.R;

import org.pjsip.pjsua.pjsua;

public class NetworkActivity extends Activity {
    private static final String THIS_FILE = "NETWORK STAT";
    private AppPrefs mPrefs;
    private Context mContext;
    private TextView m_TitleText, m_AppText;
    private TextView mTextRTT, mTextLOSS;
    private ImageButton mBtnNaviClose, mBtnNaviReload;

    private ChartView mChart;
    //private ChartArea mArea;
    //private ChartSeries mSeries;
    private ArrayList<StatItem> mStatList;


    private static final int NETWORK_CHART_UPDATE = 1;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case NETWORK_CHART_UPDATE: {
                    chartUpdate();
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mPrefs = new AppPrefs(this);

        setContentView(R.layout.activity_network);
        /*
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_network);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title2);

        m_AppText = (TextView) findViewById(R.id.left_title);
        m_AppText.setTextColor(Color.WHITE);
        m_AppText.setTypeface(Typeface.DEFAULT_BOLD);
        m_AppText.setText(mContext.getResources().getString(R.string.app_name));

        m_TitleText = (TextView) findViewById(R.id.right_title);
        m_TitleText.setTextColor(Color.WHITE);
        m_TitleText.setTypeface(Typeface.DEFAULT_BOLD);
        m_TitleText.setText(mContext.getResources().getString(
                R.string.title_network_stat));
        */
        //mWebTitle = (TextView) findViewById(R.id.web_title);

        mBtnNaviClose = (ImageButton) findViewById(R.id.btn_web_navi_close);
        mBtnNaviReload = (ImageButton) findViewById(R.id.btn_web_navi_reload);

        mBtnNaviClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                DialMain.skipCallview = false;
                finish();
            }
        });

        mBtnNaviReload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                workStart();
            }
        });


        mTextRTT = (TextView) findViewById(R.id.text_rtt);
        mTextLOSS = (TextView) findViewById(R.id.text_loss);

        mStatList = new ArrayList<StatItem>();
        mChart = (ChartView) findViewById(R.id.chart);
        mChart.setVisibility(View.VISIBLE);


        // test
        StatItem item = new StatItem(0, 0, 0, "1");
        mStatList.add(item);
    }

    @Override
    protected void onDestroy() {
        DialMain.skipCallview = false;
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        bindToService();
        LoadStatList();
        workStart();
        super.onResume();
    }

    @Override
    protected void onPause() {
        workStop();
        unbindFromService();
        super.onPause();
    }

    private void Alert(String msg) {
        AlertDialog.Builder msgDialog = new AlertDialog.Builder(mContext);
        msgDialog.setTitle(mContext.getResources().getString(R.string.app_name));
        msgDialog.setMessage(msg);
        msgDialog.setIcon(R.drawable.ic_launcher);
        msgDialog.setNegativeButton(
                mContext.getResources().getString(R.string.close), null);
        msgDialog.show();
    }

    private Activity mContextToBindTo = this;
    // private ISipService mSipService = null;
    private ServiceConnection mSipConnection = null;

    private BroadcastReceiver rttReportReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.e(THIS_FILE, "RTT REPORT RECV");
            workStop();
            Alert("품질측정이 완료되었습니다.");
        }
    };
    private boolean bindToService() {
        if (mSipConnection == null) {
            mSipConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName name, IBinder binder) {
                    // mSipService = ISipService.Stub.asInterface(binder);
                }

                public void onServiceDisconnected(ComponentName name) {
                    // mSipService = null;
                }
            };
        }

        mContextToBindTo.bindService(new Intent(mContextToBindTo,
                SipService.class), mSipConnection, BIND_AUTO_CREATE);

        registerReceiver(rttReportReceiver, new IntentFilter(
                SipManager.ACTION_SIP_RTT_REPORTED));

        return true;
    }

    private void unbindFromService() {
        if (mSipConnection != null) {
            mContextToBindTo.unbindService(mSipConnection);
        }

        try {
            unregisterReceiver(rttReportReceiver);
        } catch (Exception e) {
            Log.w(THIS_FILE, "Unable to unregisterReceiver", e);
        }
    }

    private void LoadStatList() {
        //set last cur consum
        //mLastCurConsum = mConsumData.getCurHourConsum();
        //float cur_consum = (float)mLastCurConsum / 1000.0f;
        //mTextCurConsum.setText(String.format("%.3f", cur_consum));

        mChart.getSeries().clear();
        mChart.getAreas().clear();

        ChartArea Area = new ChartArea();


        ChartSeries Series = new ChartSeries(ChartTypes.Line);
        Series.setLineWidth(5);

        ChartSeries Series2 = new ChartSeries(ChartTypes.Line);
        Series2.setLineWidth(5);

        float density = getResources().getDisplayMetrics().density;

        Area.getDefaultXAxis().getScale().setMaximum(1.0);
        Area.getDefaultXAxis().getScale().setMinimum(0.0);
        Area.getDefaultXAxis().getScale().setInterval(1.0);
        Area.getDefaultXAxis().setGridLineColor(Color.BLACK);

        //Label
        Area.getDefaultYAxis().getScale().setMaximum(getMaxConsum() + getMaxConsum() * 0.75);
        Area.getDefaultYAxis().getScale().setMinimum(0.0);
        Area.getDefaultYAxis().setGridLineColor(Color.BLACK);


        Area.getDefaultYAxis().getLabelPaint().setTextSize(density * 8.0f);
        Area.getDefaultYAxis().getLabelPaint().setColor(Color.BLACK);

        Area.getDefaultYAxis().getTitlePaint().setTextSize(density * 10.0f);
        Area.getDefaultYAxis().setTitle("RTT (ms)");
        Area.getDefaultYAxis().getTitlePaint().setColor(Color.BLACK);


        Area.getDefaultXAxis().getLabelPaint().setTextSize(density * 8.0f);
        Area.getDefaultXAxis().getLabelPaint().setColor(Color.BLACK);
        //Area.getDefaultXAxis().setLabelsAdapter(ChartAxis.SMART_LABELS_ADAPTER);


        Area.getDefaultXAxis().getTitlePaint().setTextSize(density * 10.0f);
        Area.getDefaultXAxis().setTitle("시간 (sec)");
        Area.getDefaultXAxis().getTitlePaint().setColor(Color.BLACK);

        Series.setShowLabel(false);
        Series2.setShowLabel(false);

//		You need to use Paint.setTextSize method to set text size. To get Paint objects try to use :
//			- ChartAxis.getLabelPaint() - axis labels paint object
//			- ChartAxis.getTitlePaint() - axis title paint object
//			- ChartSeries.setMarkerPaint() - series labels paint object (need to create new object)
//			- ChartPoint.setMarkerPaint() - single point label paint object (need to create new object)

        mChart.getSeries().add(Series);
        mChart.getSeries().add(Series2);
        mChart.getAreas().add(Area);

        //Area.getDefaultXAxis().setLabelAngle(45);

        chartUpdate();
    }

    private void chartUpdate() {

        ChartSeries Series = mChart.getSeries().get(0); // rtt
        ChartSeries Series2 = mChart.getSeries().get(1); // loss
        ChartArea Area = mChart.getAreas().get(0);

        Series.getPoints().clear();
        Series2.getPoints().clear();

        Area.getDefaultXAxis().getScale().setMaximum((double)mStatList.size());
        Area.getDefaultXAxis().getScale().setMinimum(0.0);
        Area.getDefaultXAxis().getScale().setInterval(1.0);

        Area.getDefaultYAxis().getScale().setMaximum(getMaxConsum() + getMaxConsum() * 0.75);
        Area.getDefaultYAxis().getScale().setMinimum(0.0);
        //Area.getDefaultYAxis().getScale().setInterval(1.0);

        StatItem item = null;
        //Drawable marker = getResources().getDrawable( R.drawable.marker );

        int total_rtt = 0;
        int total_loss = 0;

        for (int i = 0; i < mStatList.size(); i++) {
            item = mStatList.get(i);

            // rtt
            ChartPoint point;
            if (i==0)
            {
                point = Series.getPoints().addXY(i, (double) item.getRTT() / 1000.0f);
                point.setAxisLabel(item.getStatDate());
            }
            point = Series.getPoints().addXY(i+1, (double) item.getRTT() / 1000.0f);
            point.setAxisLabel(item.getStatDate());


            // loss
            ChartPoint point2;
            if (i==0)
            {
                point2 = Series2.getPoints().addXY(i, (double) item.getLOSS());
                point2.setAxisLabel(item.getStatDate());
            }
            point2 = Series2.getPoints().addXY(i+1, (double) item.getLOSS());
            point2.setAxisLabel(item.getStatDate());


            total_rtt += item.getRTT();
            total_loss += item.getLOSS();

        }

        double avg_rtt = 0;
        double avg_loss = 0;

        if (mStatList.size() > 0) {
            avg_rtt = (double) total_rtt / 1000.0f / mStatList.size();
            avg_loss = (double)total_loss * 100 / (mStatList.size() * 50);
        }

        mTextRTT.setText(String.format("%.3f", avg_rtt));
        mTextLOSS.setText(String.format("%.3f", avg_loss));

    }

    private double getMaxConsum() {
        double max = 0.0;
        StatItem item = null;
        for (int i = 0; i < mStatList.size(); i++) {
            item = mStatList.get(i);
            if (max < (double) item.getRTT() / 1000.0f)
                max = (double) item.getRTT() / 1000.0f;
        }
        return max;
    }


    private Boolean isWorking= false;
    private Thread workThread = null;
    private void workStart()
    {
        if (isWorking) return;
        isWorking = true;

        mBtnNaviReload.setVisibility(View.INVISIBLE);

        mStatList.clear();
        chartUpdate();

        workThread = new Thread() {
            public void run() {
                workProcess();
                workThread = null;
            };
        };
        workThread.start();
    }

    private void workWait() {
        if (workThread != null) {
            try {
                workThread.join(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void workStop()
    {
        if (!isWorking) return;
        isWorking = false;
        workWait();
        mBtnNaviReload.setVisibility(View.VISIBLE);
    }
    private void sleep(int delay)
    {
        try
        {
            Thread.sleep(delay);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void workProcess()
    {
        if (SipService.currentService == null) return;
        if (SipService.pjService == null) return;
        pjsua.mobile_rtt_handler_send(20, 500);

        //int rtt_table[] = new int[10];
        //int loss_table[] = new int[10];

        int recv_count = 0;
        int old_block = 0;
        while(isWorking)
        {
            // check
            int count = pjsua.mobile_rtt_handler_get_recv_count();

            DebugLog.d("test_v2 ff_anthena work process called. count: "+count);

            int block = count / 50;
            if (block <= old_block)
            {
                sleep(100);
                continue;
            }
            old_block = block;

            //String srtt = "";
            //String sloss = "";

            mStatList.clear();
            for (int i=0; i<block; i++)
            {
                int idx = i+1;

                int bc = idx * 50;
                int rtt_sum = 0;
                int loss_sum = 0;
                int rtt_count = 0;
                for (int x=(bc-50); x<bc; x++)
                {
                    int rtt = pjsua.mobile_rtt_handler_get_rtt(x + 1);
                    int flag = pjsua.mobile_rtt_handler_get_flag(x + 1);
                    if (flag == 0) loss_sum++;
                    rtt_sum = rtt_sum + rtt;
                    rtt_count++;
                }
                rtt_sum = rtt_sum / rtt_count;

                //srtt = srtt + rtt_sum + ",";
                //sloss = sloss + loss_sum + ",";

                DebugLog.d("test_v2 ff_anthena"+" index: "+idx+"rtt_sum: "+rtt_sum+" loss_sum: "+loss_sum);

                StatItem item = new StatItem(rtt_sum, loss_sum, idx, ""+idx);
                mStatList.add(item);
            }

            //Log.e(THIS_FILE, "INDEX:" + block + ", RTT:" + srtt);
            //Log.e(THIS_FILE, "INDEX:" + block + ", LOSS:" + sloss);

            // DISPLAY
            mHandler.sendEmptyMessage(NETWORK_CHART_UPDATE);

            // update
            sleep(500);
        }
    }


}
