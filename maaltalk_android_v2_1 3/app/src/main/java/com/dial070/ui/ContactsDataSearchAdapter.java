package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
//import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

//import com.dial070.utils.AppPrefs;
import com.dial070.utils.HangulUtils;
import com.dial070.utils.Log;

class SearchContactsInfo {
	String section;
	int index;
	SearchContactsInfo(String sec, int idx)
	{
		section= sec;
		index = idx;
	}
}

public class ContactsDataSearchAdapter extends BaseAdapter {

	private Context mContext;
//	private Resources mResources;
	private int mSearchCount;
	
	private List<SearchContactsInfo> mMatchData; 	
	private Map<String, ArrayList<ContactsData>> mSectionItemList;	
		
//	private AppPrefs mPrefs;
	//private String mLocale;	
	
	public ContactsDataSearchAdapter(Context context)
	{
		mContext = context;
//		mResources = mContext.getResources();
//		mPrefs = new AppPrefs(mContext);

		mSearchCount = 0;
		mMatchData = new ArrayList<SearchContactsInfo>();
	}	
	
	public void setData(Map<String, ArrayList<ContactsData>> data)
	{
		mSectionItemList = data;
	}
	
	public void setSearchCount(int searchCount)
	{
		mSearchCount = searchCount;
	}	
	
	//검색키에 해당하는 리스트 아이템을 개수를 구함 
	public List<SearchContactsInfo> getSearchItem(String searchKey)
	{
		if (mSectionItemList == null) return null;
		
		if(mMatchData == null) return null;
		
		//초기화 
		mMatchData.clear();
		
		if(searchKey.length() == 0) return mMatchData;
		
		String contactName;
		String findName;
		String contactNumber;
		
		String sectionStr = null;
		ContactsData item = null;		
		ArrayList<ContactsData> contacts = null;
		
//		boolean searchKeyIsHangul = false;
//		if(HangulUtils.getHangulInitialSound(searchKey).length() > 0 ) searchKeyIsHangul = true;

		
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{

				if(i == mSectionItemList.size()-1) sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
				else sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[i];
					
			contacts = mSectionItemList.get(sectionStr);
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(contacts != null && contacts.size() > 0)
			{
				for(int j=0; j < contacts.size() ; j++)
				{
					item = contacts.get(j);
					contactName = item.getDisplayName();
					contactNumber = item.getPhoneNumber();
					
						findName = HangulUtils.getHangulInitialSound(contactName, searchKey);
						findName = findName.toLowerCase();//BJH 대소문자 구별없이 검색가능
						searchKey = searchKey.toLowerCase();
						if(findName.indexOf(searchKey) >= 0)
						{ 
							mMatchData.add(new SearchContactsInfo(sectionStr,j));
						}
						else if(contactNumber.replaceAll("[^0-9]", "").indexOf(searchKey) >= 0)
						{
							mMatchData.add(new SearchContactsInfo(sectionStr,j));
						}

				}
			}
		}

		return mMatchData;
	}		
	
	private ContactsData read(int offset)
	{
		if (offset < 0 || offset >= mSearchCount) return null;
			
		SearchContactsInfo search_data = mMatchData.get(offset);

//		ContactsData item = null;		
			
		ArrayList<ContactsData> contacts = mSectionItemList.get(search_data.section);

		if(contacts != null && contacts.size() > 0)
		{
			return contacts.get(search_data.index);
		}
		return null;

	}	
	
	//@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mMatchData == null) return 0;
		
		return mMatchData.size(); //mSearchCount;
	}

	//@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return read(position);
	}

	//@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if( mSectionItemList == null) return null;
		
		ContactsListView contactsView = null;
		ContactsData item = read(position);
		if(item != null)
		{	
			if(convertView == null)
			{
				contactsView = new ContactsListView(mContext, item);
			}
			else
			{
				contactsView = (ContactsListView) convertView;
			}
			
			if(item != null)
			{
				contactsView.setData(item.getDisplayName(), item.getPhoneNumber(), item.getContactId(), item.getPhotoId());
				contactsView.setItem(item);
			}
			
//			if( (position % 2) == 0)
//			{
//				contactsView.setBackgroundColor(mResources.getColor(R.color.list_even_background));
//			}
//			else
//			{
//				contactsView.setBackgroundColor(mResources.getColor(R.color.list_odd_background));
//			}					
			
		}
		return contactsView;
	}

}
