package com.dial070.ui;

import java.util.Timer;
import java.util.TimerTask;

import com.dial070.maaltalk.R;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.pjsip.PjSipCalls;
import com.dial070.sip.service.SipService;
import com.dial070.utils.Log;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.view.MotionEvent;
import android.widget.TextView;

public class CallStat extends Activity {
	
	private static String THIS_FILE = "CALL STAT";
	private Timer connectTimer = null;
	private TextView mTextStat;

	public CallStat() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.call_stat);
		
		mTextStat = (TextView) findViewById(R.id.txt_stat);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		timerStop();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unbindFromService();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		bindToService();
		super.onResume();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
	    finish();
	    return super.dispatchTouchEvent(ev);
	}
	
	private synchronized void timerStart()
	{
		if (connectTimer != null) return;
		try
		{
			connectTimer = new Timer();
			TimerTask task = new TimerTask() {
	            @Override
	            public void run() {
	            	// UPDATE
	            	if (handler != null)
	            		handler.sendMessage(handler.obtainMessage(UPDATE_FROM_TIMER));
	            }
	        };				
			connectTimer.schedule(task, 100, 1000);
		}
		catch (Exception e)
		{
		}
	}
	
	private synchronized void timerStop()
	{
		if (connectTimer == null) return;
		connectTimer.cancel();
		connectTimer.purge();
		connectTimer = null;
	}
	
	private static final int UPDATE_FROM_TIMER = 1;
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case UPDATE_FROM_TIMER:
					updateUIFromTimer();
					break;
				default:
					super.handleMessage(msg);
			}
		}
	};	
	
	
	private SipCallSession getCurrentCallInfo()
	{
		SipCallSession currentCallInfo = null;
		if (callsInfo == null) { return null; }
		for (SipCallSession callInfo : callsInfo)
		{
			int state = callInfo.getCallState();
			switch (state)
			{
				case SipCallSession.InvState.NULL:
				case SipCallSession.InvState.DISCONNECTED:
					break;
				default:
					currentCallInfo = callInfo;
					break;
			}
			if (currentCallInfo != null)
			{
				break;
			}
		}
		return currentCallInfo;
	}
	
	private void updateUIFromTimer()
	{
		if (mSipService == null) 
		{
			finish();
			return;
		}
		SipCallSession currentCallInfo = getCurrentCallInfo();
		if (currentCallInfo == null) 
		{
			finish();
			return;		
		}
		
		int callId = currentCallInfo.getCallId();
		String msg = PjSipCalls.dumpCallInfo(callId);
		
		mTextStat.setText(msg);
		//Log.e(THIS_FILE, "DUMP : \n"+ msg);
	}	
	
	
	private boolean bindToService()
	{
		boolean bound = bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);
		registerReceiver(callStateReceiver, new IntentFilter(SipManager.ACTION_SIP_CALL_CHANGED));
		registerReceiver(callStateReceiver, new IntentFilter(SipManager.ACTION_SIP_MEDIA_CHANGED));		
		return bound;
	}
	
	private void unbindFromService()
	{
		timerStop();
		try
		{
			unbindService(mSipConnection);
		}
		catch (Exception e)
		{
		}
		mSipService = null;

		try
		{
			unregisterReceiver(callStateReceiver);
		}
		catch (IllegalArgumentException e)
		{
			// That's the case if not registered (early quit)
		}
	}	
	
	/**
	 * Service binding
	 */
	private ISipService mSipService = null;
	private SipCallSession[] callsInfo = null;
	private ServiceConnection mSipConnection = new ServiceConnection()
	{
		@Override
		public void onServiceConnected(ComponentName arg0, IBinder arg1)
		{
			mSipService = ISipService.Stub.asInterface(arg1);
			try {
				callsInfo = mSipService.getCalls();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			timerStart();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0)
		{
			timerStop();
			mSipService = null;
		}
	};	
	
	private BroadcastReceiver callStateReceiver = new BroadcastReceiver()
	{

		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();

			if (action.equals(SipManager.ACTION_SIP_CALL_CHANGED))
			{
				if (mSipService != null)
				{
					try
					{
						callsInfo = mSipService.getCalls();
					}
					catch (RemoteException e)
					{
						Log.e(THIS_FILE, "Not able to retrieve calls");
					}
				}
				handler.sendMessage(handler.obtainMessage(UPDATE_FROM_TIMER));
			}
			else if (action.equals(SipManager.ACTION_SIP_MEDIA_CHANGED))
			{
				handler.sendMessage(handler.obtainMessage(UPDATE_FROM_TIMER));
			}
		}
	};	
		

}
