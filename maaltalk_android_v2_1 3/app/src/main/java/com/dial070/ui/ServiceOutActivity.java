package com.dial070.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.dial070.db.DBManager;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.oauth.KakaoLoginControl;
import com.dial070.oauth.OauthLoginActivity;
import com.dial070.service.MessageService;
import com.dial070.service.StartForegroundService;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.UnLinkResponseCallback;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.data.OAuthLoginState;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by DialCommunications on 2017-05-22.
 */

public class ServiceOutActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private SessionCallback mSessionCallback;
    private static final String THIS_FILE = "SERVICE_OUT";
    private ImageButton mBtnNaviClose;
    private boolean mOauth = false;
    public static final String ACTIVITY_RESULT = "ActivityResult";
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private CallbackManager mCallbackManager;
    private AccessTokenTracker mAccessTokenTracker = null;
    private AccessToken mAccessToken;
    private OAuthLogin mOAuthLoginModule;
    private Context mContext;
    private String mUserID = null;
    private String mMyinfoURL = null;
    private String mSvcOutURL = null;
    private String mOauthType = "";
    private AppPrefs mPrefs;
    private PreferencesWrapper mSysPrefs;
    private LinearLayout mLayout;
    private CheckBox mChb;
    private Button mSvcOutBt;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mContext = this;
        mPrefs = new AppPrefs(mContext);
        mSysPrefs = new PreferencesWrapper(this);
        mUserID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        mMyinfoURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_MYINFO);
        mSvcOutURL = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2);
        setContentView(R.layout.activity_svc_out);
        mLayout = (LinearLayout) findViewById(R.id.layout_check);
        mChb = (CheckBox) findViewById(R.id.checkbox_svc_out);
        mSvcOutBt = (Button) findViewById(R.id.bt_svc_out);

        mBtnNaviClose = (ImageButton) findViewById(R.id.btn_web_navi_close);
        mBtnNaviClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mOauth) {
                    //finishActivity(getResources().getString(R.string.pay_ok));
                    //return;
                }
                finish();
            }
        });

        mLayout.setClickable(true);
        mLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!mChb.isChecked()) {
                    mChb.setChecked(true);
                    mSvcOutBt.setEnabled(true);
                    mSvcOutBt.setBackgroundColor(getResources().getColor(R.color.blue_pressed));
                } else {
                    mChb.setChecked(false);
                    mSvcOutBt.setEnabled(false);
                    mSvcOutBt.setBackgroundColor(getResources().getColor(R.color.blue_semi_transparent));
                }
            }

        });

        mChb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                CheckBox check = (CheckBox) v;

                if (check.isChecked()) {
                    mSvcOutBt.setEnabled(true);
                    mSvcOutBt.setBackgroundColor(getResources().getColor(R.color.blue_pressed));
                } else {
                    mSvcOutBt.setEnabled(false);
                    mSvcOutBt.setBackgroundColor(getResources().getColor(R.color.blue_semi_transparent));
                }
            }

        });

        mSvcOutBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources res = mContext.getResources();
                AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                msgDialog.setTitle(R.string.mt_svc_out);
                msgDialog.setMessage(res.getString(R.string.mt_svc_out_agree));
                msgDialog.setButton(Dialog.BUTTON_POSITIVE,res.getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            // @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mOauthType == null || mOauthType.length() == 0) {
                                    serviceOut();
                                } else {

                                    Resources res = mContext.getResources();
                                    AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                                    msgDialog.setTitle(R.string.mt_svc_out);
                                    msgDialog.setMessage(mOauthType + res.getString(R.string.mt_svc_out_oauth));
                                    msgDialog.setButton(Dialog.BUTTON_POSITIVE,res.getString(R.string.confirm),
                                            new DialogInterface.OnClickListener() {
                                                // @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (mOauthType.equals("kakao")) {
                                                        kakaoUnlinkApp();
                                                    } else if (mOauthType.equals("google")) {
                                                        googleRevokeAccessApp();
                                                    } else if (mOauthType.equals("facebook")) {
                                                        facebookRevokeApp();
                                                    } else if (mOauthType.equals("naver")) {
                                                        naverRevoke();
                                                    }
                                                    return;
                                                }
                                            });
                                    msgDialog.show();
                                }
                                return;
                            }
                        });

                msgDialog.setButton(Dialog.BUTTON_NEGATIVE,res.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                msgDialog.show();
            }

        });

        mChb.setClickable(false);
        mSvcOutBt.setEnabled(false);

        mOauthType = cofirmMyinfo();

        //Google
        //findViewById(R.id.sign_in_button).setOnClickListener(this);

        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.default_web_client_id))
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Facebook
        mCallbackManager = CallbackManager.Factory.create();
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                Log.d(THIS_FILE, "Facebook AccessToken " + currentAccessToken);
            }
        };
        // If the access token is available already assign it.
        mAccessToken = AccessToken.getCurrentAccessToken();

        //Naver
        mOAuthLoginModule = OAuthLogin.getInstance();
        mOAuthLoginModule.init(
                ServiceOutActivity.this
                , getString(R.string.naver_client_id)
                , getString(R.string.naver_client_secret)
                , "Maaltalk"
                //,OAUTH_CALLBACK_INTENT
                // SDK 4.1.4 버전부터는 OAUTH_CALLBACK_INTENT변수를 사용하지 않습니다.
        );
    }

    public void finishActivity(String p_strFinishMsg) {
        Log.d(THIS_FILE, "[PayDemoActivity] finishActivity : " + p_strFinishMsg);

        if (p_strFinishMsg != null) {
            Intent intent = new Intent();
            intent.putExtra(ACTIVITY_RESULT, p_strFinishMsg);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.d(THIS_FILE, "onSessionOpened " + "onSessionOpened");
            /*UserManagement.requestUnlink(new UnLinkResponseCallback() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    Logger.e(errorResult.toString());
                    finishActivity("FAIL");
                }

                @Override
                public void onSessionClosed(ErrorResult errorResult) {
                    finishActivity("FAIL");
                }

                @Override
                public void onNotSignedUp() {
                    serviceOut();
                }

                @Override
                public void onSuccess(Long userId) {
                    serviceOut();
                }
            });*/

            UserManagement.getInstance().requestUnlink(new UnLinkResponseCallback() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    Logger.e(errorResult.toString());
                    finishActivity("FAIL");
                }

                @Override
                public void onSessionClosed(ErrorResult errorResult) {
                    finishActivity("FAIL");
                }

                @Override
                public void onNotSignedUp() {
                    serviceOut();
                }

                @Override
                public void onSuccess(Long userId) {
                    serviceOut();
                }
            });


        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            Log.d(THIS_FILE, "onSessionOpenFailed " + "onSessionOpenFailed");
            if (exception != null) {
                Logger.e(exception);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Kakao
        if (Session.getCurrentSession().handleActivityResult(requestCode,
                resultCode, data)) {
            Log.d(THIS_FILE, "onActivityResult " + "onActivityResult");
            return;
        }
        //Google
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        //Facebook
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    //Google
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void googleSignInApp() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(THIS_FILE, "Google Login Status : " + result.isSuccess());
        if (result.isSuccess()) {
            serviceOut();
        } else {
            finishActivity("FAIL");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // [START signOut]
    private void googleSignOutApp() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Log.d(THIS_FILE, "Google signOut : " + status);
                        //Builds a fresh instance of GoogleApiClient
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void googleRevokeAccessApp() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        // [END_EXCLUDE]
                        if (status.getStatusCode() == 0) {
                            Log.d(THIS_FILE, "Google revokeAccess");
                            serviceOut();
                        } else {
                            Log.d(THIS_FILE, "Google revokeAccess Error");
                            googleSignInApp();
                        }
                    }
                });
    }
    // [END revokeAccess]


    //Kakao
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mSessionCallback != null)
            Session.getCurrentSession().removeCallback(mSessionCallback);
        if (mAccessTokenTracker != null)
            mAccessTokenTracker.stopTracking();
    }

    //Kakao
    public void kakaoRedirectLoginActivity() {
        new KakaoLoginControl(this).call();
    }


    private void kakaoLogoutApp() {
        /*UserManagement.requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
                Log.d(THIS_FILE, "Kakao Logout");
            }
        });*/

        UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
                Log.d(THIS_FILE, "Kakao Logout");
            }
        });

    }

    private void kakaoInit() {
        //Kakao
        mSessionCallback = new SessionCallback();
        Session.getCurrentSession().addCallback(mSessionCallback);
        Session.getCurrentSession().checkAndImplicitOpen();
        kakaoRedirectLoginActivity();
    }

    private void kakaoUnlinkApp() {
        final String appendMessage = getString(R.string.com_kakao_confirm_unlink);
        new AlertDialog.Builder(this)
                .setMessage(appendMessage)
                .setPositiveButton(getString(R.string.com_kakao_ok_button),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /*UserManagement.requestUnlink(new UnLinkResponseCallback() {
                                    @Override
                                    public void onFailure(ErrorResult errorResult) {
                                        Logger.e(errorResult.toString());
                                        kakaoInit();
                                    }

                                    @Override
                                    public void onSessionClosed(ErrorResult errorResult) {
                                        kakaoInit();
                                    }

                                    @Override
                                    public void onNotSignedUp() {
                                        serviceOut();
                                    }

                                    @Override
                                    public void onSuccess(Long userId) {
                                        serviceOut();
                                    }
                                });*/
                                UserManagement.getInstance().requestUnlink(new UnLinkResponseCallback() {
                                    @Override
                                    public void onFailure(ErrorResult errorResult) {
                                        Logger.e(errorResult.toString());
                                        kakaoInit();
                                    }

                                    @Override
                                    public void onSessionClosed(ErrorResult errorResult) {
                                        kakaoInit();
                                    }

                                    @Override
                                    public void onNotSignedUp() {
                                        serviceOut();
                                    }

                                    @Override
                                    public void onSuccess(Long userId) {
                                        serviceOut();
                                    }
                                });
                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(getString(R.string.com_kakao_cancel_button),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();

    }

    //Facebook
    private void facebookLoginApp() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) { //로그인 성공시 호출되는 메소드
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getToken());
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getUserId());
                Log.d(THIS_FILE, "Facebook " + loginResult.getAccessToken().getPermissions() + " ");
                mAccessToken = loginResult.getAccessToken();
                facebookRevokeApp(); // 권한 해제를 위해서

            }

            @Override
            public void onError(FacebookException error) {
                finishActivity("FAIL");
            }

            @Override
            public void onCancel() {
                finishActivity("FAIL");
            }
        });
    }

    private void facebookLogoutApp() {
        // Logout from Facebook
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
            Log.d(THIS_FILE, "Facebook Logout");
        }
    }

    private void facebookRevokeApp() {
        if (AccessToken.getCurrentAccessToken() != null) {
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions", null, HttpMethod.DELETE, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    boolean isSuccess = false;
                    try {
                        isSuccess = response.getJSONObject().getBoolean("success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (isSuccess && response.getError() == null) {
                        // Application deleted from Facebook account
                        Log.d(THIS_FILE, "Facebook Revoke");
                        serviceOut();
                    } else {
                        Log.d(THIS_FILE, "Facebook Revoke Error");
                        facebookLoginApp();
                    }

                }
            }).executeAsync();

        } else {
            facebookLoginApp(); // 토큰이 없는 경우
        }
    }

    //Naver
    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            if (success) {
                String accessToken = mOAuthLoginModule.getAccessToken(mContext);
                String refreshToken = mOAuthLoginModule.getRefreshToken(mContext);
                long expiresAt = mOAuthLoginModule.getExpiresAt(mContext);
                String tokenType = mOAuthLoginModule.getTokenType(mContext);
                Log.d(THIS_FILE, "Naver getState : " + mOAuthLoginModule.getState(mContext).toString());
                naverRevoke(); // 권한 해제를 위해서
            } else {
                String errorCode = mOAuthLoginModule.getLastErrorCode(mContext).getCode();
                String errorDesc = mOAuthLoginModule.getLastErrorDesc(mContext);
                Log.d(THIS_FILE, "Naver errorCode : " + errorCode + " errorDesc : " + errorDesc);
                finishActivity("FAIL"); // 로그인 실패
            }
        }

        ;
    };

    private void naverLoginApp() {
        mOAuthLoginModule.startOauthLoginActivity((Activity) mContext, mOAuthLoginHandler);
    }

    private void naverLogoutApp() {
        mOAuthLoginModule.logout(mContext);
        if (mOAuthLoginModule.getState(mContext) == OAuthLoginState.NEED_LOGIN) {
            Log.d(THIS_FILE, "Naver Logout");
        }
    }

    private void naverRevoke() {
        new DeleteTokenTask().execute();
    }

    private class DeleteTokenTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            boolean isSuccessDeleteToken = mOAuthLoginModule.logoutAndDeleteToken(mContext);
            if (!isSuccessDeleteToken) {
                // 서버에서 토큰 삭제에 실패했어도 클라이언트에 있는 토큰은 삭제되어 로그아웃된 상태입니다.
                // 클라이언트에 토큰 정보가 없기 때문에 추가로 처리할 수 있는 작업은 없습니다.
                Log.d(THIS_FILE, "NavererrorCode : " + OAuthLogin.getInstance().getLastErrorCode(mContext));
                Log.d(THIS_FILE, "Naver errorDesc : " + OAuthLogin.getInstance().getLastErrorDesc(mContext));
            }
            return isSuccessDeleteToken;
        }

        protected void onPostExecute(Boolean b) {
            if (b) {
                Log.d(THIS_FILE, "Naver Revoke");
                serviceOut();
            } else {
                Log.d(THIS_FILE, "Naver Revoke Error");
                naverLoginApp();
            }
        }
    }

    private String cofirmMyinfo() {
        String result = "";
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("name", mUserID));
        String rs = OauthLoginActivity.postServer(mContext, mMyinfoURL, nameValuePairs); //postMyinfo(true);
        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
            Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
            if (responseObject.getInt("RES_CODE") == 0)
                result = responseObject.getString("RES_OAUTH_TYPE");
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            Log.d(THIS_FILE, "JSON ERROR");
            result = "FAIL";
        }
        return result;
    }

    private void serviceOut() {
        showProgress();
        boolean result = false;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("name", mUserID));
        nameValuePairs.add(new BasicNameValuePair("oauth_type", mOauthType));
        String rs = OauthLoginActivity.postServer(mContext, mSvcOutURL, nameValuePairs); //postMyinfo(true);
        JSONObject jObject;
        try {
            jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
            Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
            if (responseObject.getInt("RES_CODE") == 0)
                result = true;
        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            Log.d(THIS_FILE, "JSON ERROR");

        }

        if (result) {
            CookieSyncManager cookieSyncManager = CookieSyncManager
                    .createInstance(this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.removeSessionCookie();
            cookieManager.removeAllCookie();
            cookieSyncManager.sync();
            // DB Data Clear
            DBManager db = new DBManager(mContext);
            db.open();
            db.deleteAllRecentCalls();
            db.deleteAllSmsList();
            db.deleteAllSmsMsg();
            db.deleteAllFavorites();
            db.close();
            // Stop Service
            stopService(new Intent(mContext, MessageService.class));
            stopService(new Intent(mContext, SipService.class));
            stopService(new Intent(mContext, StartForegroundService.class));

            if (ServicePrefs.mLogin) {
                ServicePrefs.logout();
            }
            // FOR SAFE
            SipService.currentService = null;
            // Preference Clear
            mPrefs.resetAllDefaultValues();
            mSysPrefs.resetAllDefaultValues();
            // Go Main
            // Intent intent = new Intent(mContext, DialMain.class);
            Intent intent = new Intent(mContext, CloseActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            hideProgres();
            finish();
        } else {
            hideProgres();
            finishActivity("FAIL");
        }
    }

    private ProgressDialog progressDialog = null;
    private Handler autoHideHandler = null;
    private Runnable autoHideRunnable = null;

    private void showProgress() {
        if (progressDialog != null)
            return;

        Log.d(THIS_FILE, "showProgress");
        progressDialog = ProgressDialog.show(mContext, mContext.getResources().getString(R.string.mt_svc_out),
                mContext.getResources().getString(R.string.mt_svc_out_progress), true, true);
        progressDialog.show();

        autoHideRunnable = new Runnable() {
            @Override
            public void run() {
                hideProgres();
                finishActivity("FAIL");
            }
        };

        autoHideHandler = new Handler();
        autoHideHandler.postDelayed(autoHideRunnable, 20000);
    }


    private void hideProgres() {
        Log.d(THIS_FILE, "hideProgres");
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (autoHideHandler != null) {
            autoHideHandler.removeCallbacks(autoHideRunnable);
            autoHideHandler = null;
            autoHideRunnable = null;
        }

    }

}

