package com.dial070.ui;


import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;

public class PopupUSIM extends Activity {

	//private String mType = null;
	
	//private LinearLayout mStepFirst, mStepTwo;
	private TextView mReceive;
	private Button /*mBtnIncheon, mBtnDialC, */mBtnMap, mBtnCancel;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_usim);	
		
		LayoutParams params = getWindow().getAttributes();
	    DisplayMetrics dm = getResources().getDisplayMetrics();
	    int x = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				300, dm);
	    params.width = x;
	    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
	    
		//mStepFirst = (LinearLayout) findViewById(R.id.receive_step_first);
		//mStepTwo = (LinearLayout) findViewById(R.id.receive_step_two);
		mReceive = (TextView) findViewById(R.id.type_receive);
		mReceive.setText(getResources().getString(
				R.string.receive_type_incheon));
		/*mBtnIncheon = (Button) findViewById(R.id.btnIncheon);
		mBtnIncheon.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mStepFirst.setVisibility(View.GONE);
				mStepTwo.setVisibility(View.VISIBLE);
				mReceive.setText(getResources().getString(
						R.string.receive_type_incheon));
				mType = "i";
			}
		});
		
		mBtnDialC = (Button) findViewById(R.id.btnDialC);
		mBtnDialC.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mStepFirst.setVisibility(View.GONE);
				mStepTwo.setVisibility(View.VISIBLE);
				mReceive.setText(getResources().getString(
						R.string.receive_type_dial));
				mType = "d";
			}
		});*/
		
		mBtnMap = (Button) findViewById(R.id.btnMap);
		mBtnMap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(mType == null || mType.length() == 0)
					return;
				if(mType.equals("i")) {
					intent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://map.naver.com/?mapmode=0&lng=e22ec27099a30908fad928c9d2f0e985&pinId=36721630&pinType=site&lat=5fe285e585f8b20b5b81b8b8ee887cf5&dlevel=11&enc=b64"));
				} else if(mType.equals("d")) {
					intent = new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://map.naver.com/?mapmode=0&pinId=13107110&pinType=site&lng=4dac17be297d422523e88bdd7436a6e5&lat=2788df28e2f8de825c9f3f76dab52a07&dlevel=11&enc=b64"));
				} 
				mType = null;*/
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://map.naver.com/?mapmode=0&lng=e22ec27099a30908fad928c9d2f0e985&pinId=36721630&pinType=site&lat=5fe285e585f8b20b5b81b8b8ee887cf5&dlevel=11&enc=b64"));
				intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(intent);
				finish();
			}
		});
		
		mBtnCancel = (Button) findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
}
