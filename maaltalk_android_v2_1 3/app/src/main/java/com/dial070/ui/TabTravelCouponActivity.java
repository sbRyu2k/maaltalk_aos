package com.dial070.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.FragmentTabHost;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

public class TabTravelCouponActivity extends FragmentActivity {
    private FragmentTabHost mTabHost;
    private static final String THIS_FILE = "TabTravelCouponActivity";
    private ImageButton btn_web_navi_close;
    private ImageButton btn_web_navi_reload;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_coupon_search);

        DebugLog.d("test_travel onCreate()<--");

        btn_web_navi_close=findViewById(R.id.btn_web_navi_close);
        btn_web_navi_reload=findViewById(R.id.btn_web_navi_reload);

        mTabHost = findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        btn_web_navi_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_web_navi_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentTabTag=mTabHost.getCurrentTabTag();
                if (currentTabTag.equals("travel")){
                    sendBroadcast(new Intent(CategoryActivity.BROADCAST_MESSAGE));
                }else if(currentTabTag.equals("coupon")){
                    sendBroadcast(new Intent(TabFragmentCoupon.BROADCAST_MESSAGE));
                }
            }
        });
        btn_web_navi_reload.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String currentTabTag=mTabHost.getCurrentTabTag();
                if (currentTabTag.equals("travel")){
                    sendBroadcast(new Intent(CategoryActivity.BROADCAST_LONG_MESSAGE));
                    return true;
                }
                return false;
            }
        });
        //btn_web_navi_reload.setVisibility(View.GONE);

        Intent intent=getIntent();
        String url=intent.getStringExtra("url");

        Bundle args = new Bundle();
        args.putString("url",url);

        mTabHost.addTab(
                mTabHost.newTabSpec("travel").setIndicator(getResources().getString(R.string.title_travel), null),
                CategoryActivity.class, null);
        mTabHost.addTab(
                mTabHost.newTabSpec("coupon").setIndicator(getResources().getString(R.string.title_coupon), null),
                TabFragmentCoupon.class, args);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                Log.d(THIS_FILE,"tabId:"+tabId);
                if (tabId.equals("coupon")){
                    btn_web_navi_reload.setVisibility(View.VISIBLE);
                }else if (tabId.equals("travel")){
                    btn_web_navi_reload.setVisibility(View.VISIBLE);
                }

                for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
                    TextView tv = mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); // 탭에 있는 TextView 지정후
                    if (i == mTabHost.getCurrentTab()) {
                        tv.setTextColor(Color.parseColor("#ffffff")); // 탭이 선택되어 있으면 FontColor를 검정색으로
                        tv.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                    }
                    else {
                        tv.setTextColor(Color.parseColor("#6ADEE8")); // 선택되지 않은 탭은 하얀색으로.
                        tv.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                    }
                }
            }
        });

        /*mTabHost.getTabWidget().getChildAt(0)
                .setBackgroundColor(Color.parseColor("#0282a1"));
        mTabHost.getTabWidget().getChildAt(1)
                .setBackgroundColor(Color.parseColor("#0282a1"));*/

        mTabHost.setCurrentTab(0);
        TextView temp = mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        temp.setTextColor(Color.parseColor("#ffffff"));
        temp.setTypeface(Typeface.DEFAULT,Typeface.BOLD);

        temp = mTabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        temp.setTextColor(Color.parseColor("#6ADEE8"));
        temp.setTypeface(Typeface.DEFAULT,Typeface.NORMAL);
    }

    @Override
    public void onBackPressed() {
        String currentTabTag=mTabHost.getCurrentTabTag();
        if (currentTabTag.equals("travel")){
            if (CategoryActivity.completeAsyncTaskCount!=-1 && CategoryActivity.completeAsyncTaskCount!=CategoryActivity.AsyncTaskMaxCount){
                AlertDialog msgDialog = new AlertDialog.Builder(this).create();
                msgDialog.setTitle("알림");
                msgDialog.setMessage("이미지를 불러오는 중입니다. 취소하고 이전 화면으로 돌아시겠습니까?");
                msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                        new DialogInterface.OnClickListener() {
                            // @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                msgDialog.show();
                return;
            }

        }
        super.onBackPressed();
    }
}
