package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.dial070.maaltalk.R;
import com.dial070.sip.utils.Compatibility;

public class Search extends Activity {

	private Context mContext;
	private InputMethodManager mImm;
	private SearchDataAdapter mSearchAdapter;	
	//private NaverListAdapter mWebSearchAdapter;
	private ListView mContactsListView ;//, mNaverListView;
	private ImageButton mBtnSearch;
	private EditText mSearchText;
//	private ImageButton mBtnWebSearch;
	
	private ArrayList<NaverResult> mNaverResult;
	//BJH
	private ArrayList<CallCenterResult> mCallCenterResult;
	private ProgressDialog mProgressDialog;
	//private boolean isShowing;
	
	private static final int DIAL070_NAVER_SEARCH_START 	= 1;
	private static final int DIAL070_NAVER_SEARCH_OK 		= 2;
	private static final int DIAL070_NAVER_SEARCH_ERROR 	= 3;

	
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case DIAL070_NAVER_SEARCH_START:
				{
					String title = getResources().getString(R.string.naver);
					String description = getResources().getString(R.string.naver_search_wait);

					if(mNaverResult != null) mNaverResult.clear();
					mProgressDialog = ProgressDialog.show(mContext, title, description,false);					
				}
					break;
				case DIAL070_NAVER_SEARCH_OK:
				{
					if (mProgressDialog != null && mProgressDialog.isShowing())
						mProgressDialog.dismiss();
					
					applyResult();
					//mLoadNaver = false;
				}
					break;
				case DIAL070_NAVER_SEARCH_ERROR:
				{
					if (Search.this != null && !Search.this.isFinishing())
					{
						AlertDialog.Builder msgDialog = new AlertDialog.Builder(Search.this);
						msgDialog.setTitle(getResources().getString(R.string.app_name));
						msgDialog.setMessage(getResources().getString(R.string.naver_search_error));
						msgDialog.setIcon(R.drawable.ic_launcher);
						msgDialog.setNegativeButton(getResources().getString(R.string.close), null);
						msgDialog.show();

					}
					//mLoadNaver = false;
				}
					break;

			}
		}
	};		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.search_activity);
		
		mContext = this;
		
		mNaverResult = null; //new ArrayList<NaverResult>();
		//BJH
		mCallCenterResult = null;
		
		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		mSearchAdapter = new SearchDataAdapter(this);
		mSearchAdapter.setContactData(ContactsDataContainer.getFiteringData(this));			
		
		mBtnSearch = (ImageButton) findViewById(R.id.btn_search);
		mContactsListView = (ListView) findViewById(R.id.search_list_view);
		mSearchText = (EditText) findViewById(R.id.edit_search);
		mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
		        if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
		        	mImm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
		            return doSearch();
		        }
				return false;
			}
		});		
		
		mContactsListView.setEmptyView(findViewById(R.id.empty_search_result));
		
		mContactsListView.setAdapter(mSearchAdapter);
		//BJH
		CallCenterParser parse = new CallCenterParser();
		int rs = parse.parseXml(mContext);
		if(rs == 0) {
			mCallCenterResult = parse.getResult();
			if(mCallCenterResult != null) {
				mSearchAdapter.setCallCenterData(mCallCenterResult);
				mSearchAdapter.notifyDataSetChanged();
			}
		}
		
		if (Compatibility.getApiLevel() > 10)
			mContactsListView.setSelector(R.drawable.list_selector);
		mContactsListView.setFocusableInTouchMode(true);
		
		//검색
		mBtnSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				mImm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
				doSearch();
			}
		});
		
		//웹으로 검색
//		mBtnWebSearch.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				doWebSearch();
//			}
//		});
		
		//
		mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				int type = mSearchAdapter.getItemType(arg2);
				
				if(type == SearchDataAdapter.TYPE_SECTION_CALL_CENTER_HEADER || type == SearchDataAdapter.TYPE_SECTION_CONTACT_HEADER || type == SearchDataAdapter.TYPE_SECTION_WEB_HEADER) return;
				if(type == SearchDataAdapter.TYPE_SECTION_CALL_CENTER)
				{
					CallCenterResult resultItem = (CallCenterResult) mSearchAdapter.getItem(arg2);
					if(resultItem != null)
					{
						setResult(resultItem.getCode() + resultItem.getTelephone(), "call_center");//BJH 국가번호도 같이 결과 값 출력 2016.05.02
					}
				}
				else if(type == SearchDataAdapter.TYPE_SECTION_CONTACT)
				{
				
					ContactsData contactsItem = (ContactsData) mSearchAdapter.getItem(arg2);
					
					if(type == SearchDataAdapter.TYPE_SECTION_CALL_CENTER)
					{
						CallCenterResult resultItem = (CallCenterResult) mSearchAdapter.getItem(arg2);
						if(resultItem != null)
						{
							setResult(resultItem.getTelephone(), "");
						}
					}
					else if(contactsItem != null)
					{
						ArrayList<PhoneItem> phones = contactsItem.getUserPhoneList();
						ArrayList<ContactsPhoneList> phoneList = new ArrayList<ContactsPhoneList>();
						
						PhoneItem phoneItem;
						for(int i = 0; i < phones.size(); i++)
						{
							phoneItem = phones.get(i);
							ContactsPhoneList item = new ContactsPhoneList(contactsItem.getContactId(),phoneItem.PhoneType, phoneItem.PhoneNumber);
							phoneList.add(item);
						}

						
						String alertDialogTitle ="";
						String alertDialogDesc = "";
						//연락처에 이름만 등록된 경우 
						if(phoneList.size() == 0)
						{
								
							alertDialogTitle = mContext.getResources().getString(R.string.select_contact);
							alertDialogDesc = mContext.getResources().getString(R.string.no_exist_contact_info);
		
							AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
							msgDialog.setTitle(alertDialogTitle);
							msgDialog.setMessage(alertDialogDesc);
							msgDialog.setButton(mContext.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
								//@Override
								public void onClick(DialogInterface dialog, int which) {
									//do nothing
									return;
								}
							});	

							msgDialog.show();	
							return;
						}
						
						if(phoneList.size() == 1)
						{
							setResult(contactsItem.getPhoneNumber(), "");
						}
						else
						{
						    final String[] phoneNums = new String[phoneList.size()];
						    for (int i = 0; i < phoneList.size(); i++) {
						    	phoneItem = phones.get(i);
						    	phoneNums[i] = phoneItem.PhoneType + "  " + phoneItem.PhoneNumber;
						    }
							
							alertDialogTitle = mContext.getResources().getString(R.string.choose_contact);
						    
						    AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
						    dialog.setTitle(alertDialogTitle);
						    dialog.setSingleChoiceItems(phoneNums, 0,
						        new DialogInterface.OnClickListener() {

						            //@Override
						            public void onClick(DialogInterface dialog,
						                int which) {
						            // get selected item and close the dialog
						            String selectedNumber = phoneNums[which];
						            setResult(selectedNumber, "");						            	
						            }
						        });						
							dialog.setNeutralButton(mContext.getResources().getString(R.string.cancel), null);
							AlertDialog alert = dialog.create();
							alert.show();

						}
					}
					
				}
				else if(type == SearchDataAdapter.TYPE_SECTION_WEB)
				{
					NaverResult resultItem = (NaverResult) mSearchAdapter.getItem(arg2);
					if(resultItem != null)
					{
						setResult(resultItem.getTelephone(), "naver");
					}
				}
				
			}
			
		});
		
	}
	
	
//	private boolean sipCall(String number)
//	{
//		// check number
//		if (number == null) return false; 
//		if(number.length() == 0) return false;
//		
//		//Log.e("SEARCH_DATA", number);
//		
//		
//		//즐겨찾기에 추가
//		Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
//		intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
//		intent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
//		sendBroadcast(intent);
//	
//		return true;
//	}	
	
	private void setResult(String number, String type)
	{
		Intent intent = new Intent();
		intent.putExtra("PhoneNumber", number);
		intent.putExtra("Type", type); // BJH 2016.12.01 사장님 요청
		//intent.putExtra("displayName", number);
		setResult(RESULT_OK,intent);
		finish();		
	}
	
	private boolean doSearch()
	{
		String searchKey = mSearchText.getText().toString();
		List<SearchContactsInfo> contactsSearchList = null;
		
		contactsSearchList = mSearchAdapter.getSearchItem(searchKey);
		if(contactsSearchList != null)
		{
			mSearchAdapter.setSearchCount(contactsSearchList.size());
			mSearchAdapter.notifyDataSetChanged();
		}
		
		mContactsListView.setAdapter(mSearchAdapter);   
		
		//포털 검색
		doWebSearch();
		
		return true;
	}
	
	private void doWebSearch()
	{
		searchProcess(mSearchText.getText().toString());
	}
	
	
	private void searchProcess(String keyword)
	{
		if(keyword == null) return;

		final String searchKey = keyword;
		//네이버 검색 
		Thread t = new Thread()
		{
			public void run()
			{
				handler.sendEmptyMessage(DIAL070_NAVER_SEARCH_START);
				
				NaverParser parse = new NaverParser();
				//int rs = parse.parseXml(searchKey);
				int rs = parse.parseNewXml(searchKey);
				if(rs < 0) return;
				mNaverResult = parse.getResult();
	
				if (mNaverResult == null)
				{
					handler.sendEmptyMessage(DIAL070_NAVER_SEARCH_ERROR);
					return;
				}
				handler.sendEmptyMessage(DIAL070_NAVER_SEARCH_OK);
				
			};
		};
		if (t != null)
		{
			t.start();
		}
	}	
	
	private void applyResult()
	{
		//네이버 결과 적용
		if(mNaverResult == null) return;

		mSearchAdapter.setWebData(mNaverResult);
		mSearchAdapter.notifyDataSetChanged();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//mImm.showSoftInput(mSearchText, InputMethodManager.SHOW_IMPLICIT);
		mSearchText.requestFocus();
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		mImm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
		super.onPause();
	}
	
//	@Override
//	public void onBackPressed() {
//		// TODO Auto-generated method stub
//		//백버튼 비활성화
//		DialMain.mainBackPressed(mContext);
//		//super.onBackPressed();
//	}

}

