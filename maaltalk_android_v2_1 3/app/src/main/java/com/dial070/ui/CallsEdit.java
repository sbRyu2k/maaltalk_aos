package com.dial070.ui;

import java.util.ArrayList;

import com.dial070.maaltalk.R;
import com.dial070.db.DBManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

public class CallsEdit extends Activity {

	CallsListAdapter mAdapter;	
	
	private Context mContext;

	private LinearLayout mSelectAllItem;
	private CheckBox mCheckBoxAllItem;
	private ListView mCallsListView;
	private Button mCancel;
	private Button mDelete;	
	
	private ProgressDialog mProgressDialog;
	
	private boolean mAllItemChecked;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		mContext = this;
		
		setContentView(R.layout.calls_edit_activity);
		
		mAllItemChecked = false;

		mDelete = (Button) findViewById(R.id.btn_calls_delete);
		mDelete.setEnabled(false);
		mDelete.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				//선택한 항목이 삭제됩니다.
				Resources res = mContext.getResources();
				AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
				msgDialog.setTitle(R.string.delete_calls);
				msgDialog.setMessage(res.getString(R.string.delete_question));
				msgDialog.setButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						deleteRecentCalls();
					}
				});
				msgDialog.setButton2(res.getString(R.string.no), new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});		
				msgDialog.show();				
			}
		});
		
		
		mCancel = (Button) findViewById(R.id.btn_calls_cancel);
		mCancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
						
		mCallsListView = (ListView) findViewById(R.id.calls_list);
		mCallsListView.setClickable(true);
		mCallsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(arg1 == null) return; 

				
				CallsListView view = (CallsListView) arg1;
				CheckBox chk_select = (CheckBox)view.findViewById(R.id.cb_select_item);
				if(chk_select == null) return;
				mCheckBoxAllItem.setChecked(false);
				
				//UI 변경 
				chk_select.setChecked(!chk_select.isChecked());
				//데이타 변경 
				mAdapter.setCheckedItem(view.getId(), chk_select.isChecked());	
				
				
				if(mAdapter.getCheckedCount() > 0) mDelete.setEnabled(true);
				else mDelete.setEnabled(false);
				
				if(mAdapter.getCheckedCount() == mAdapter.getCount()) mCheckBoxAllItem.setChecked(true);				
			}
			
		});
		
		mSelectAllItem = (LinearLayout) findViewById(R.id.allitem);
		mCheckBoxAllItem = (CheckBox) findViewById(R.id.check_all_item);
		mCheckBoxAllItem.setClickable(false);
		
		mSelectAllItem.setClickable(true);
		mSelectAllItem.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCheckBoxAllItem.setChecked(!mCheckBoxAllItem.isChecked());		
				
				mAllItemChecked = mCheckBoxAllItem.isChecked();
				
				mDelete.setEnabled(mAllItemChecked);
				
				mAdapter.setCheckedAllItem(mAllItemChecked);
				
				mAdapter.notifyDataSetChanged();				
			}
		});
		

		mAdapter = new CallsListAdapter(this);
		mAdapter.loadRecentCallsData();
		mCallsListView.setAdapter(mAdapter);			
		
	}

	private Handler handler = new Handler();
	
	private void deleteRecentCalls()
	{
		Resources res = mContext.getResources();
		String title = res.getString(R.string.delete_select_item);
		String description = res.getString(R.string.delete_select_item_desc);
		
		mProgressDialog = ProgressDialog.show(this, title, description,false);
		
		// DELETE CALLS
		Thread t = new Thread()
		{
			public void run()
			{
				deleteSelectItems();
				handler.post(mDeleteResults);
			};
		};
		if (t != null)
			t.start();				
	}	

	final Runnable mDeleteResults = new Runnable()
	{
		public void run()
		{
			if (mProgressDialog != null && mProgressDialog.isShowing())
				mProgressDialog.dismiss();
			finish();
		}
	};	
	
	
	private boolean deleteSelectItems()
	{
		DBManager db = new DBManager(mContext);
		try
		{
			db.open();
			
			if(mCheckBoxAllItem.isChecked())
			{
				db.deleteAllRecentCalls(); //전체 아이템 삭제
			}
			else
			{
				ArrayList<Integer> list = mAdapter.getCheckedList();
				for(int i=0; i < list.size(); i++)
				{
					Integer id = list.get(i);	
					db.deleteRecentCalls(id);
				}
				mAdapter.checkClear();
			}
		} finally {
			db.close();
		}
		return true;		
	}
			
}
