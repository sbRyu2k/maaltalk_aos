package com.dial070.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.*;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dial070.DialMain;
import com.dial070.maaltalk.R;
import com.dial070.db.*;
import com.dial070.global.ServicePrefs;
import com.dial070.sip.api.SipUri;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.HangulUtils;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import android.content.*;
import android.database.*;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.*;
import android.util.Base64;
import android.view.*;
import android.widget.*;

public class CallsListAdapter extends BaseAdapter {
	private static final String THIS_FILE = "DIAL070_CALLS_ADAPTER";
	private Context mContext;
	private AppPrefs mPrefs;
	
	private DBManager mDatabase;
	private Cursor mCursor;

	private ArrayList<Integer> mSelectedList;
    private ArrayList<RecentCallsData> mOriginalRecentCallsDataArrayList;
	private ArrayList<RecentCallsData> mRecentCallsDataArrayList;
	
	public CallsListAdapter(Context context)
	{
		mContext = context;
		mPrefs = new AppPrefs(context);
		
		//초기화
		mRecentCallsDataArrayList=new ArrayList<>();
		mSelectedList = new ArrayList<Integer>();		
		
		// Db
		mCursor = null;
		mDatabase = new DBManager(mContext);
		open();
	}
	
	public void destroyAdapter()
	{
		close();
	}

	private void open()
	{
		if(!mDatabase.isOpen()) mDatabase.open();
		
	}
	
	private void close()
	{
		clear();
		
		if(mDatabase != null)
		{
			if (mDatabase.isOpen())
				mDatabase.close();
		}
	}
	
	public void clear()
	{
		if(mCursor != null)
		{
			mCursor.close();
			mCursor = null;
		}
	}
	
	public boolean isChanged()
	{
		if (mDatabase == null) return false;		
		return mDatabase.isRecentChanged();
	}	
	
	public Cursor loadRecentCallsData()
	{
	    Log.d(THIS_FILE,"loadRecentCallsData");
		clear();

		mCursor = mDatabase.getAllRecentNormalCalls();
		if(mCursor == null)
		{
			//BJH 2016.06.10 java.lang.IllegalStateException
			//mDatabase.close();
			mRecentCallsDataArrayList=new ArrayList<>();
			return null;
		}else{
			if (mCursor.moveToFirst()) {
				mRecentCallsDataArrayList=new ArrayList<>();
				RecentCallsData recentCallsData;
				do {
					recentCallsData=new RecentCallsData();
					recentCallsData.setID(mCursor.getInt(mCursor.getColumnIndex(RecentCallsData.FIELD_PK)));
					String number=mCursor.getString(mCursor.getColumnIndex(RecentCallsData.FIELD_PHONENUMBER));
					recentCallsData.setPhone_number(number);
					recentCallsData.setComposite_name(ContactHelper.getContactsNameByPhoneNumber(mContext,number));
					recentCallsData.setCall_date(mCursor.getLong(mCursor.getColumnIndex(RecentCallsData.FIELD_DATE)));
					recentCallsData.setType(mCursor.getInt(mCursor.getColumnIndex(RecentCallsData.FIELD_TYPE)));
					recentCallsData.setDuration(mCursor.getLong(mCursor.getColumnIndex(RecentCallsData.FIELD_DURATION)));
					recentCallsData.setCall_type(0);
					recentCallsData.setHidden(0);
					recentCallsData.setCall_memo("");
					recentCallsData.setCall_rec(mCursor.getString(mCursor.getColumnIndex(RecentCallsData.FIELD_CALL_REC)));
					mRecentCallsDataArrayList.add(recentCallsData);
				} while (mCursor.moveToNext());

                mOriginalRecentCallsDataArrayList=new ArrayList<>();
                mOriginalRecentCallsDataArrayList.addAll(mRecentCallsDataArrayList);
			}

		}
		
		return mCursor;
	}

	/*public void searchRecentCallsData(String searchkey){
		mCursor = mDatabase.searchRecentNormalCalls(searchkey);
		Log.d(THIS_FILE,"searchRecentCallsData count:"+mCursor.getCount());
	}*/
	
	public boolean setCheckedItem(int id, boolean selected)
	{
		if(mSelectedList == null) return false;

		if (selected)
		{
			if (!mSelectedList.contains(id))
			{
				mSelectedList.add(id);
			}
		}
		else
		{
			if (mSelectedList.contains(id))
			{
				int index = mSelectedList.indexOf((Integer)id);
				if(index < 0) return false;
				mSelectedList.remove(index);
			}
		}
		
		return true;
	}		
	
	public boolean setCheckedAllItem(boolean selected)
	{
		if(mSelectedList == null) return false;
		mSelectedList.clear();
		
		if (selected)
		{
			if (mCursor.moveToFirst())
			{
				do
				{
					mSelectedList.add(mCursor.getInt(mCursor.getColumnIndex(RecentCallsData.FIELD_PK)));
				}
				while(mCursor.moveToNext());
			}
		}
						
		return true;
	}		
	
	public ArrayList<Integer> getCheckedList()
	{
		return mSelectedList;
	}
	
	public int getCheckedCount()
	{
		return mSelectedList.size();
	}
	
	public void checkClear()
	{
		mSelectedList.clear();	
	}		
	
	private boolean move(int offset)
	{
		if(mCursor == null) return false;
		return mCursor.moveToPosition(offset);		
	}
		
	
	private void makeCall(String number)
	{
		Log.d(THIS_FILE,"number:"+number);
		//전화걸기
		if (number != null && SipUri.isPhoneNumber(number) )
		{
			if(number.startsWith("00982")) {
				number = number.replace("00982", "0");
			}
			Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
			intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
			intent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
			mContext.sendBroadcast(intent);
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mRecentCallsDataArrayList == null) return 0;
		return mRecentCallsDataArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		CallsListView recentCallView = null;
			
		
		//GET VIEW
		if(convertView == null)
		{
			recentCallView = new CallsListView(mContext);
		}
		else
		{
			recentCallView = (CallsListView) convertView;
		}
		
		String number = mRecentCallsDataArrayList.get(position).getPhone_number();
		String cachedName = mRecentCallsDataArrayList.get(position).getComposite_name();
		long duration = mRecentCallsDataArrayList.get(position).getDuration();
		int callType = mRecentCallsDataArrayList.get(position).getCall_type();
		int id = mRecentCallsDataArrayList.get(position).getID();
		int hidden = mRecentCallsDataArrayList.get(position).getHidden();
		String memo = mRecentCallsDataArrayList.get(position).getCall_memo();
		String rec = mRecentCallsDataArrayList.get(position).getCall_rec();
		
		hidden = 0;

		String remoteContact = number;
		String phoneNumber = number;
	
		if(TextUtils.isEmpty(cachedName)) {
			// Reformat number
			Pattern sipUriSpliter = Pattern.compile("^(?:\")?([^<\"]*)(?:\")?[ ]*(?:<)?sip(?:s)?:([^@]*)@[^>]*(?:>)?");
			Matcher m = sipUriSpliter.matcher(number);
			if (m.matches()) {
				if (!TextUtils.isEmpty(m.group(2))) {
					remoteContact = m.group(2);
				} else if (!TextUtils.isEmpty(m.group(1))) {
					remoteContact = m.group(1);
				}
			}
			remoteContact = ContactHelper.getContactsNameByPhoneNumber(mContext,remoteContact);
		} else {
			remoteContact = ContactHelper.getContactsNameByPhoneNumber(mContext,number);
			if(remoteContact == null || remoteContact.length() == 0) remoteContact = cachedName;
		}
		
		
		int type = mRecentCallsDataArrayList.get(position).getType();
		long date = mRecentCallsDataArrayList.get(position).getCall_date();

		final String call_number = phoneNumber;
		//mMemo = (ImageButton) findViewById(R.id.btn_history_memo);

		ImageButton btnMakeCall = (ImageButton) recentCallView.findViewById(R.id.imgBtnCall);
		btnMakeCall.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				makeCall(call_number);
			}
		});
		btnMakeCall.setFocusable(false);
		btnMakeCall.setFocusableInTouchMode(false);

		recentCallView.setData(id, remoteContact, type, date, duration, callType, number, hidden, memo, rec);

		return recentCallView;
	}

	// LOAD FROM SERVER
	public int loadFromServer()
	{
		String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
		if (uid == null || uid.length() == 0) return 0;
		
		// MAKE TOKEN
		String str_date = "00000000000000";
		
		String last_ts = mPrefs.getPreferenceStringValue(AppPrefs.CDR_TS);
		if (last_ts != null && last_ts.length() > 0)
			str_date = last_ts;
		
		String key = uid + str_date;
		
		Log.d(THIS_FILE, "CDR KEY : " + key);
		
		
		String token = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP); 
		return requestData(token);
	}
	
	
	private int toInt(String s)
	{
		int v = 0;
		try
		{
			v =  Integer.parseInt(s);
		}
		catch (Exception e)
		{
		}    
		return v;
	}
	
	private int requestData(String token)
	{
		// SEND QUERY
		Log.d(THIS_FILE, "REQUEST RECENT DATA : START");
		
		String rs = postData(token);
		
		Log.d(THIS_FILE, "REQUEST RECENT DATA : END");
		Log.d(THIS_FILE, "RESULT : " + rs);		
		if (rs == null || rs.length() == 0) return 0;
		
		//SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
		int count = 0;
		
		JSONObject jObject;
		try
		{
		    jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0"))
            {
                JSONObject infoObject = jObject.getJSONObject("CDR_INFO");
                JSONArray itemArray = infoObject.getJSONArray("ITEMS");
                for (int i = 0; i < itemArray.length(); i++) {
                	
                	String dcon = itemArray.getJSONObject(i).getString("DCONTEXT").toString();
                	String cdate = itemArray.getJSONObject(i).getString("CALLDATE").toString();
                	String cid = itemArray.getJSONObject(i).getString("CID").toString();
                	String dur = itemArray.getJSONObject(i).getString("BILLSEC").toString();
                	String rec = itemArray.getJSONObject(i).getString("REC").toString();
                	
                	
                	int callType = 0;
                	if (dcon.equalsIgnoreCase("i"))
                		callType = RecentCallsData.INCOMING_TYPE;
                	else if (dcon.equalsIgnoreCase("o"))
                		callType = RecentCallsData.OUTGOING_TYPE;
                	else if (dcon.equalsIgnoreCase("v")) // BJH 2016.09.23
                		callType = RecentCallsData.VMS_TYPE;
                	else if (dcon.equalsIgnoreCase("a"))
                		callType = RecentCallsData.MISSED_TYPE;


					if (cid.startsWith("00982")){
						cid=cid.replace("00982","0");
					}

                	String display_name = ContactHelper.getContactsNameByPhoneNumber(mContext, cid);;
                	if (display_name == null) display_name = "";
                	int identifier = 0;
                	int duration = toInt(dur);

                	
        			long callDate = System.currentTimeMillis();
        			if (cdate != null && cdate.length() > 0)
        			{
                 		try
                		{
            				Date d = mFormatter.parse(cdate);
            				callDate = d.getTime();
            				if(i == 0) { // BJH 2016.08.26 정확한 통화기록 갱신을 위해서
            					//SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
            					SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss");
            					_ormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
                    			String last_ts = _ormatter.format(d);
                				mPrefs.setPreferenceStringValue(AppPrefs.CDR_TS, last_ts);
            				}
                		} catch (Exception e) 
                		{
                			e.printStackTrace();
                		}      				
        			}
        			
                	RecentCallsData cdr = new RecentCallsData(display_name, cid, callDate, identifier, callType, duration, 0, 0,"", rec);
                	
            		try
            		{
            			// check
            			if (!mDatabase.checkRecentCalls(callDate))
            			{
            				mDatabase.insertRecentCalls(cdr);
            				count++;
            			}
            		} catch (Exception e) 
            		{
            			Log.e(THIS_FILE, "DB", e);
            		}
                }	
                
                if(itemArray.length() > 0)
                {
                	Log.d(THIS_FILE, "DATA SIZE ="+ Integer.toString(itemArray.length()));

                }
            }
            else
            {
            	Log.d(THIS_FILE, responseObject.getString("RES_TYPE") + "ERROR ="+ responseObject.getString("RES_DESC"));
            }
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return 0;
		}		
		
		
		/*if (count > 0)
		{
			// GET LAST TIMESTAMP			
    		try
    		{
    			long lastCallDate = mDatabase.getLastCallDate();
    			if (lastCallDate != 0)
    			{
    				Log.d(THIS_FILE, "CALL DATE : " + lastCallDate);
    				
    				Date d = new Date(lastCallDate);
    				
    				SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
    				String last_ts = _ormatter.format(d);
    				mPrefs.setPreferenceStringValue(AppPrefs.CDR_TS, last_ts);
    			}
    		} 
    		catch (Exception e) 
    		{
    			Log.e(THIS_FILE, "DB", e);
    		}				

		}*/

		return count;
	}
	
	private String postData(String token)
	{
		String url_cdr_info = mPrefs.getPreferenceStringValue(AppPrefs.URL_CDR_INFO);
		String recents_url = url_cdr_info + "?token="+token;
		Log.d(THIS_FILE,"recents_url:"+recents_url);
		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(mContext);

			HttpParams params = httpclient.getParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpConnectionParams.setConnectionTimeout(params, 10000);
			HttpConnectionParams.setSoTimeout(params, 10000);

			HttpPost httppost = new HttpPost(recents_url);
			
			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String line = null;
			StringBuilder sb = new StringBuilder();			
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}

	private void showMessage(String msg) {
		new MaterialAlertDialogBuilder(mContext)
				.setTitle(mContext.getResources().getString(R.string.app_name))
				.setMessage(msg)
				.setPositiveButton(mContext.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				})
				.show();
	}

    public void filter(String searchKey) {
        mRecentCallsDataArrayList.clear();
        if(searchKey.length() == 0)
        {
            mRecentCallsDataArrayList.addAll(mOriginalRecentCallsDataArrayList);
        }
        else
        {
            String findName="";
            for( RecentCallsData item : mOriginalRecentCallsDataArrayList)
            {
                if (item.getComposite_name()!=null && item.getComposite_name().trim().length()!=0){
					findName = HangulUtils.getHangulInitialSound(item.getComposite_name(), searchKey);
					findName = findName.toLowerCase();
				}

                searchKey = searchKey.toLowerCase();
                if(findName.indexOf(searchKey) >= 0)
                {
                    mRecentCallsDataArrayList.add(item);
                }
                else if(item.getPhone_number().replaceAll("[^0-9]", "").indexOf(searchKey) >= 0)
                {
                    mRecentCallsDataArrayList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }


}
