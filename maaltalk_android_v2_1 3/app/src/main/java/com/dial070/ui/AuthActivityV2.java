package com.dial070.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AuthActivityV2 extends AppCompatActivity {
    private static final String THIS_FILE = "AuthV2";
    //	private ArrayAdapter<CharSequence> adspin;
    private Context mContext;
    private AppPrefs mPrefs;

    private TextView m_TitleText;

    private String m_CountryCode = "";
    private String m_CountryName = "";
    private String m_CountryFlag = "";
    private String m_PhoneNumber = "";

    private static final int SELECT_TERM = 2;
    private static final int SELECT_AUTH = 3;
    private static final int SELECT_AUTH_CALL = 4;

    private String m_Locale = "ko_KR";
    //BJH 2016.06.07
    public static final String KOREA_CODE = "+82";
    public static final String USA_CODE = "+1";
    public static final String CHINA_CODE = "+86";
    public static final String JAPAN_CODE = "+81";
    public static final String KOREA_FLAG = "kr";

    private TelephonyManager mTelephonyManager; // BJH 2017.10.18

    private androidx.appcompat.app.AlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //BJH ~+C 전화번호 입력을 하면 해외인증과 국내인증으로 구별하기 위해서 새롭게 만든 레이아웃 ex) ShowLocalAuth+C()
        super.onCreate(savedInstanceState);

        DebugLog.d("onCreate()-->");

        mContext = this;
        //커스텀 타이틀 바
        //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

        setContentView(R.layout.activity_auth);
        //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_left_title);
        // 레이아웃
        LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayoutC);
        layout.setVisibility(View.GONE);

        //잔액정보 표시
        /*m_TitleText = (TextView) findViewById(R.id.left_title);
        m_TitleText.setTextColor(Color.WHITE);
        m_TitleText.setTypeface(Typeface.DEFAULT_BOLD);
        m_TitleText.setText(mContext.getResources().getString(R.string.app_name));*/

        mPrefs = new AppPrefs(this);
        m_Locale = Locale.getDefault().toString();
        if (m_Locale.equals("ko_KR"))//BJH 다국어 관련 국가 코드 언어 선택
            mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "한국어");
        else
            mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "영어");

        String locale = mPrefs.getPreferenceStringValue(AppPrefs.LOCALE);

        if (locale != null && locale.equals("한국어")) {
            m_CountryCode = KOREA_CODE;
            m_CountryName = getResources().getString(R.string.country_korea);
            m_CountryFlag = KOREA_FLAG;
        } else {
            if (m_Locale.startsWith("ja")) {
                m_CountryCode = JAPAN_CODE;
                m_CountryName = getResources().getString(R.string.country_japan);
                m_CountryFlag = "jp";
            } else if (m_Locale.startsWith("zh_CN")) {
                m_CountryCode = CHINA_CODE;
                m_CountryName = getResources().getString(R.string.country_china);
                m_CountryFlag = "cn";
            } else {
                m_CountryCode = USA_CODE;
                m_CountryName = getResources().getString(R.string.country_usa);
                m_CountryFlag = "us";
            }
        }
        m_PhoneNumber = "";

        if (ServicePrefs.DIAL_AUTH_TEST) {
            m_CountryCode = "+82";
            m_CountryName = getResources().getString(R.string.country_korea);
            m_CountryFlag = "kr";
            m_PhoneNumber = "";
        }

        if (currentCode != null) {
            m_CountryCode = currentCode;
        }
        if (currentCountry != null) {
            m_CountryName = currentCountry;
        }
        if (currentNumber != null)
            m_PhoneNumber = currentNumber;
        if (currentFlag != null)
            m_CountryFlag = currentFlag;

        // BJH 2017.10.18 CHECK USIM
        if (mTelephonyManager == null)
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        boolean termOk = mPrefs.getPreferenceBooleanValue(AppPrefs.TERM_OK);

        DebugLog.d("termsOk check --> "+termOk);

        if (!termOk) {
            navigateTerms();
//            Intent intent = new Intent(AuthActivityV2.this, TermsActivity.class);
//            startActivityForResult(intent, SELECT_TERM);
        } else {
            NewAuthWebView();
        }
    }

    private void navigateTerms() {
        Intent intent = new Intent(this, TermsActivity.class);
        startActivityForResult(intent, SELECT_TERM);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.auth, menu);
        return true;
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onPause");
        unregisterReceiver(mSMSReceiver); // BJH 2017.04.07
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        DebugLog.d("onResume()");

        // by sgkim : 2015-08-26 : TODO : 향후 메인쓰레드에서 네트워크 관련 I/O는 수정할 예정입니다.
        // 임시로 조치함.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        registerReceiver(mSMSReceiver, new IntentFilter(
                "android.provider.Telephony.SMS_RECEIVED"));

        Log.d(THIS_FILE, "onResume");
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onDestroy");
        DebugLog.d("onDestroy()");

        if (pDialog!=null && pDialog.isShowing()){
            pDialog.dismiss();
        }
        super.onDestroy();
    }

    private String getMyPhoneNumber() {
        DebugLog.d("getMyPhoneNumber()");

        if (mTelephonyManager == null)
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        String phoneNumber;

        if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                phoneNumber = "Not Found";
            }
            else {
                phoneNumber = mTelephonyManager.getLine1Number();
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                phoneNumber = "Not Found";
            }
            else {
                phoneNumber = mTelephonyManager.getLine1Number();
            }
        }


        if (phoneNumber == null) return "";
        if (phoneNumber.startsWith("+82")) {
            phoneNumber = phoneNumber.replace("+82", "0");
        }
        return phoneNumber;
    }

    private void showMessage(String msg) {
        DebugLog.d("showMessage()");

        new MaterialAlertDialogBuilder(AuthActivityV2.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        NewAuthWebView();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static String postData(Context context, String url) {
        DebugLog.d("postData() --> "+url);

        Log.d(THIS_FILE, "REQ : " + url);
        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(params, 20000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpPost httpPost = new HttpPost(url);


            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httpPost);
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    private static String currentCode = null;
    private static String currentCountry = null;
    private static String currentNumber = null;
    private static String currentFlag = null;

    private String mAuthCid = null;
    private String mAuthCountryCode = null;
    private String mAuthDistributorID = null;
    private String mAuthBirthDate = null;
    private String mAuthUserName = null;
    private String mAuthGender = null;
    private String mAuthAuthType = null;
    private String mAuthValidCode = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        DebugLog.d("onActivityResult() --> requestCode : "+requestCode);

        switch (requestCode) {
            case SELECT_TERM:
                if (resultCode != RESULT_OK) {
                    finish();
                } else {
                    NewAuthWebView();
                }
                break;
            case SELECT_AUTH: {
                if (resultCode == RESULT_OK) {
                    String cert_cid = data.getStringExtra("cid");
                    String distributor_id = data.getStringExtra("distributor_id");
                    String country_code = data.getStringExtra("country_code");
                    String birth_date = data.getStringExtra("birth_date");
                    String user_name = data.getStringExtra("user_name");
                    if (user_name != null && user_name.length() > 0)
                        user_name = Uri.encode(user_name);
                    String gender = data.getStringExtra("gender");
                    String auth_type = data.getStringExtra("auth_type");
                    String valid_code = data.getStringExtra("valid_code");
                    String adm_auth = data.getStringExtra("adm_auth"); //BJH 2017.01.04 해외인증 관련
                    int result = compareNumber(cert_cid);

                    //BJH 2017.11.08 사장님 요청사항으로 유심/포켓 구매자인 경우 번호 확인하지 않음
                    boolean pocket_usim = false;
                    if (result > 1 && distributor_id != null && distributor_id.length() > 0 && country_code != null && country_code.length() > 0 && country_code.equals("82") && (distributor_id.equals("a8000") || distributor_id.equals("usim30")))
                        pocket_usim = getPocketInfo(cert_cid, distributor_id);

                    if (result == 1 || pocket_usim || !ServicePrefs.DIAL_RETAIL || (adm_auth != null && adm_auth.equals("Y"))) {
                        //if (result == 11) {
                        String flag = data.getStringExtra("flag");
                        mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE, "+" + country_code);
                        mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG, flag);
                        // BJH 2017.04.03
                        if (auth_type.equals("inter_call")) { // 해외 전화 인증
                            if (inter_auth(cert_cid, valid_code, birth_date, user_name, gender) < 0) {
                                //다시 인증
                            }
                        } else {
                            if (new_auth(cert_cid, country_code, distributor_id, birth_date, user_name, gender, auth_type) < 0) {
                                //다시 인증
                            }
                        }
                    } else {
                        mAuthCid = cert_cid;
                        mAuthCountryCode = country_code;
                        mAuthDistributorID = distributor_id;
                        mAuthBirthDate = birth_date;
                        mAuthUserName = user_name;
                        mAuthGender = gender;
                        mAuthAuthType = auth_type;
                        mAuthValidCode = valid_code;

                        new MaterialAlertDialogBuilder(AuthActivityV2.this)
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getResources().getString(R.string.mt_auth_call))
                                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        reqNumberAuth();
                                    }
                                })
                                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        NewAuthWebView();
                                    }
                                })
                                .show();
                    }
                } else {
                    finish();
                }
            }
            break;
            case SELECT_AUTH_CALL:
                if (resultCode == RESULT_OK) {
                    String id = data.getStringExtra("id");
                    String pwd = data.getStringExtra("pwd");

                    Intent intent = new Intent(AuthActivityV2.this, DialMain.class);
                    startActivity(intent);
                    finish();
                } else {

                }
                break;
            default:
                break;
        }
        return;
    }

    public void reqNumberAuth() {
        DebugLog.d("reqNumberAuth()");

        Intent intent = new Intent(AuthActivityV2.this, AuthCallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("country_code", mAuthCountryCode);
        intent.putExtra("callerid", mAuthCid);
        intent.putExtra("distributor_id", mAuthDistributorID);
        intent.putExtra("birth_date", mAuthBirthDate);
        intent.putExtra("user_name", mAuthUserName);
        intent.putExtra("gender", mAuthGender);
        //startActivityForResult(intent,SELECT_AUTH_CALL);
        startActivity(intent);
        finish();

    }

    private void NewAuthWebView() {
        DebugLog.d("newAuthWebView()");

        //인증페이지 변경 테스트
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL); // BJH 2017.06.22
        //String url = "http://211.253.24.69/auth/certcid_maaltalk/SMART_ENC_V2/mt_auth_tb.php"; // 2018.10.02

        Log.d(THIS_FILE, "NewAuthWebView url:" + url);
        //url = "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/new_cert_tb.php";
        if (url != null && url.length() > 0) {
            if (!m_CountryCode.startsWith("+"))
                m_CountryCode = "+" + m_CountryCode;
            //String mynumber=getMyPhoneNumber();
            //mynumber = Base64.encodeToString(mynumber.getBytes(), Base64.NO_WRAP);
            //String urlStr = url + "?locale=" + m_Locale + "&country_code=" + m_CountryCode + "&phone_number=" + mynumber;
            String urlStr = url + "?locale=" + m_Locale + "&country_code=" + m_CountryCode;
            Log.d(THIS_FILE, "NewAuthWebView urlStr:" + urlStr);
            Intent intent = new Intent(AuthActivityV2.this, WebClient.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);//BJH 2016.09.20 본인인증 관련 수정
            intent.putExtra("url", urlStr);
            intent.putExtra("title", getResources().getString(R.string.auth_number_self));
            intent.putExtra("url_post", url);
            intent.putExtra("locale", m_Locale);
            intent.putExtra("country_code", m_CountryCode);
            intent.putExtra("phone_number", getMyPhoneNumber());
            startActivityForResult(intent, SELECT_AUTH);
        }
    }

    //BJH 2017.04.28 해외전화인증
    public int inter_auth(String number, String code, String birth_date, String user_name, String gender) {
        DebugLog.d("inter_auth() --> number : "+number+" | code : "+code);
        if (ServicePrefs.DIAL_TEST) {
            if (ServicePrefs.login(this, "", "") >= 0) {
                Intent intent = new Intent(AuthActivityV2.this, DialMain.class);
                startActivity(intent);
                finish();
            }
            return 0;
        }

        if (number != null)
            number = number.replaceAll(" ", "");


        String qry_url = null;

        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL);
        //url = "https://110.45.190.100/certcid_intl/req_account_info_tb.php";
        if (url == null || url.length() == 0) {
            Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO_INTL ERROR");
            showMessage("URL_REQ_ACCOUNT_INFO_INTL ERROR");
            return -1;
        }
        // BJH 2017.04.27 생년월일 추가 2017.07.18 이름 추가 2017.11.15 성별 추가
        qry_url = url + "?cid=" + number + "&valid_code=" + code + "&birth_date=" + birth_date + "&user_name=" + user_name + "&gender=" + gender;

        String str = postData(mContext, qry_url);
        if (str == null) {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String id = null;
        String password = null;
        String exist = null;
        String device = null;

        try {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0) {
                JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                if (certObject.has("ID"))
                    id = certObject.getString("ID");
                if (certObject.has("PASSWORD"))
                    password = certObject.getString("PASSWORD");
                if (certObject.has("EXIST"))
                    exist = certObject.getString("EXIST");
                if (certObject.has("DEVICE"))
                    device = certObject.getString("DEVICE");
            } else {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR =" + responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (id == null || id.length() == 0) {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_id));
            return -1;
        }

        if (password == null || password.length() == 0) {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_pwd));
            return -1;
        }

        // SAVE NUMBER
        mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, number);
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);

        // DO LOGIN
        if (ServicePrefs.login(this, id, password) >= 0) {
            String model = android.os.Build.MODEL;
            if (exist != null && exist.equals("EXIST") && model != null && model.length() > 0 && device != null && device.length() > 0 && !device.contains(model)) {
                logoutAlert();
            } else {
                Intent intent = new Intent(AuthActivityV2.this, DialMain.class);
                startActivity(intent);
                finish();
            }
        } else {
            showMessage(getResources().getString(R.string.error_login));
            return -1;
        }
        return 0;
    }

    //BJH 2017.04.03
    public int new_auth(String number, String country_code, String distributor_id, String birth_date, String user_name, String gender, String auth_type) {
        DebugLog.d("new_auth()");

        MyReqAccountInfoPostTask myReqAccountInfoPostTask =new MyReqAccountInfoPostTask();
        String[] params=new String[7];
        params[0]=number;
        params[1]=country_code;
        params[2]=distributor_id;
        params[3]=birth_date;
        params[4]=user_name;
        params[5]=gender;
        params[6]=auth_type;

        myReqAccountInfoPostTask.execute(params);


        return 0;
    }

    private int compareNumber(String cid) { // BJH 2017.10.18
        DebugLog.d("compareNumber() --> "+cid);

        if (mTelephonyManager == null)
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return 2;
            }
        } else {
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS)!=PackageManager.PERMISSION_GRANTED) {
                return 2;
            }
        }

        String usim_cid = mTelephonyManager.getLine1Number();
        if (usim_cid == null || usim_cid.length() == 0)
            return 2;

        /*int length = 8;
        if (cid.length() == 7)
            length = 7;
        else if (cid.length() == 6)
            length = 6;
        else if (cid.length() == 5)
            length = 5;

        if (usim_cid.length() >= length && cid.substring(cid.length() - length).equals(usim_cid.substring(usim_cid.length() - length)))*/ //BJH 2018.01.24
        if (usim_cid != null && usim_cid.substring(usim_cid.length() - 8, usim_cid.length()).equals(cid.substring(cid.length() - 8, cid.length())))
            return 1;
        else
            return 3;
    }

    private boolean getPocketInfo(String cid, String distributor_id) {
        DebugLog.d("getPocketInfo() --> cid : "+cid+" | distributor_id : "+distributor_id);

        String qry_url = null;
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET);
        if (url == null || url.length() == 0) {
            return false;
        }

        qry_url = url + "?cid=" + cid + "&distributor_id=" + distributor_id;

        String str = postData(mContext, qry_url);
        if (str == null) {
            Log.e(THIS_FILE, "NOT RESPONSE");
            return false;
        }

        String desc = null;

        try {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0) {
                return true;
            } else {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR =" + responseObject.getString("RES_DESC"));
                desc = responseObject.getString("RES_DESC");
                return false;
            }

        } catch (Exception e) {
            Log.e(THIS_FILE, "JSON", e);
            return false;
        }
    }

    private void alertMsg(String msg) {
        DebugLog.d("alertMsg()");

        msg = msg + getResources().getString(R.string.mt_cs_maaltalk);

        new MaterialAlertDialogBuilder(AuthActivityV2.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        NewAuthWebView();
                    }
                })
                .show();
    }

    private static final int REQUEST_CODE_PERMISSIONS = 1;

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if (checkSelfPermission(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    sendSMS();
                } else {
                    showMarshmallowAlert();
                }
                break;
        }
    }

    private androidx.appcompat.app.AlertDialog mMsgDialog = null;

    private void showMarshmallowAlert() {
        DebugLog.d("showMarshMallowAlert()");

        if (mMsgDialog != null)
            return;

        MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(AuthActivityV2.this);
        builder.setTitle(getResources().getString(R.string.permission_setting));
        builder.setMessage(getResources().getString(R.string.permission_desc));
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mMsgDialog = null;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                        finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mMsgDialog = null;
                        finish();
                    }
                });
        mMsgDialog = builder.create();
        mMsgDialog.show();
    }

    private androidx.appcompat.app.AlertDialog mSMSProgressDialog;
    private Handler autoCloseSMS = null;
    private Runnable autoCloseSMSRunnable = null;

    public void setAutoCloseSMS(final long delayMS) {
        DebugLog.d("setAutoCloseSMS() --> "+delayMS);

        if (autoCloseSMS != null)
            return;
        Log.d(THIS_FILE, "autoCloseSMS (" + delayMS + ")");

        MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(AuthActivityV2.this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.sms_send_desc));
        builder.setCancelable(false);
        mSMSProgressDialog = builder.create();
        mSMSProgressDialog.show();

        autoCloseSMSRunnable = new Runnable() {
            @Override
            public void run() {
                //SMS 인증 실패
                cancelAutoCloseSMS();
                Log.d(THIS_FILE, "TIME OUT");
                String result = null;
                result = "TIME OUT (" + delayMS + ")";
                AuthError(result);
                alertMsg(getString(R.string.mt_sms_count_error));
            }
        };
        autoCloseSMS = new Handler();
        autoCloseSMS.postDelayed(autoCloseSMSRunnable, delayMS);
    }

    public void cancelAutoCloseSMS() {
        DebugLog.d("cancelAutoCloseSMS()");

        if (autoCloseSMS != null) {
            Log.d(THIS_FILE, "cancelAutoClose !!");
            if (autoCloseSMSRunnable != null) {
                autoCloseSMS.removeCallbacks(autoCloseSMSRunnable);
                autoCloseSMSRunnable = null;
            }
            if (mSMSProgressDialog != null && mSMSProgressDialog.isShowing()) {
                mSMSProgressDialog.cancel();
                mSMSProgressDialog = null;
            }
            autoCloseSMS = null;
        }
    }

    private String mSMSMsg = null;

    private void sendSMS() {
        DebugLog.d("sendSMS()");

        setAutoCloseSMS(20000);

        String SENT = "SMS_SENT";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0 | PendingIntent.FLAG_IMMUTABLE);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String result = null;
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        result = "SMS sent";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        int errorCode = arg1.getIntExtra("errorCode", -1);
                        result = "Generic failure : "+ errorCode;
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        result = "No service";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        result = "Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        result = "Radio off";
                        break;
                }

                if (result == null || !result.equals("SMS sent")) {
                    cancelAutoCloseSMS();
                    Log.d(THIS_FILE, "SMS RESULT : " + result);
                    AuthError(result);
                    alertMsg("Error : " + result + "\n" + getString(R.string.mt_sms_error));
                }
            }
        }, new IntentFilter(SENT));

        Date d = new Date();

        SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        String current_date = _ormatter.format(d);
        String key = current_date + mAuthCid;
        mSMSMsg = md5(key);
        String number = mAuthCid;
        if (!mAuthCountryCode.equals("82"))
            number = mAuthCountryCode + mAuthCid;
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(number, null, mSMSMsg, sentPI, null);
    }

    private static String md5(String s) {
        DebugLog.d("md5() --> "+s);
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex Strin
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(s.getBytes(), Base64.NO_WRAP);
    }

    private BroadcastReceiver mSMSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (autoCloseSMS == null)
                return;
            final String action = intent.getAction();
            if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
                Bundle bundle = intent.getExtras();

                if (bundle != null) {
                    Object messages[] = (Object[]) bundle.get("pdus");
                    SmsMessage smsMessage[] = new SmsMessage[messages.length];

                    for (int i = 0; i < messages.length; i++) {
                        // PDU 포맷으로 되어 있는 메시지를 복원합니다.
                        smsMessage[i] = SmsMessage.createFromPdu((byte[]) messages[i]);
                    }

                    String origNumber = smsMessage[0].getOriginatingAddress();
                    if (origNumber == null || origNumber.length() == 0)
                        return;

                    origNumber = origNumber.trim();
                    origNumber = origNumber.replaceAll("\\p{Z}", "");
                    origNumber = origNumber.replaceAll("-", "");

                    String message = smsMessage[0].getMessageBody().toString();

                    if (origNumber != null && origNumber.substring(origNumber.length() - 8, origNumber.length()).equals(mAuthCid.substring(mAuthCid.length() - 8, mAuthCid.length()))) {
                        if (message.contains(mSMSMsg)) {
                            // 성공
                            cancelAutoCloseSMS();
                            Log.d(THIS_FILE, "AUTH SMS SUCCESS");
                            if (mAuthAuthType.equals("inter_call")) { // 해외 전화 인증
                                if (inter_auth(mAuthCid, mAuthValidCode, mAuthBirthDate, mAuthUserName, mAuthGender) < 0) {
                                    //다시 인증
                                }
                            } else {
                                if (new_auth(mAuthCid, mAuthCountryCode, mAuthDistributorID, mAuthBirthDate, mAuthUserName, mAuthGender, mAuthAuthType) < 0) {
                                    //다시 인증
                                }
                            }
                        }
                    } else { // ERROR
                        if (message.contains(mSMSMsg)) {
                            String result = null;
                            result = "Auth CID = " + mAuthCid + "\n" + "SMS Number = " + origNumber;
                            AuthError(result);
                            cancelAutoCloseSMS();
                            alertMsg(getString(R.string.mt_usim_error));
                        }
                    }
                }
            }
        }
    };

    private void AuthError(String result) { // BJH 2017.01.08 인증 실패 원인 파악
        DebugLog.d("authError() --> "+result);

//        Log.d(THIS_FILE, "AuthError : " + result);
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_HISTORY);
        if (url == null || url.length() == 0) {
            return;
        }

        String model = ServicePrefs.getModel();

        Log.d(THIS_FILE, "AuthError : " + url);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(mContext);

            // SET TIMEOUT
            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 15000);
            HttpConnectionParams.setSoTimeout(params, 15000);

            HttpPost httppost = new HttpPost(url);

            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("param1", mAuthCid));
            nameValuePairs.add(new BasicNameValuePair("param2", ServicePrefs.getVersion() + " " + model));
            nameValuePairs.add(new BasicNameValuePair("param3", "AuthError"));
            nameValuePairs.add(new BasicNameValuePair("param4", result));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            Log.d(THIS_FILE, "HTTPS POST EXEC OK");
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
    }

    private void logoutAlert() {
        DebugLog.d("logoutAlert()");

        new MaterialAlertDialogBuilder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(getResources().getString(R.string.mt_logout_notice))
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(AuthActivityV2.this, DialMain.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }

    public class MyReqAccountInfoPostTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(AuthActivityV2.this);
            builder.setMessage("Loading");
            builder.setCancelable(false);
            pDialog = builder.create();
            pDialog.show();

            TextView messageView = pDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);
        }

        @Override
        protected String doInBackground(String... params) {
            String number=params[0];
            String country_code=params[1];
            String distributor_id=params[2];
            String birth_date=params[3];
            String user_name=params[4];
            String gender=params[5];
            String auth_type=params[6];


            String qry_url = null;
            {
                String url = null;
                if (auth_type.equals("local")) {
                    url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3);
                    //url = "https://auth.maaltalk.com/certcid_global/req_account_info_locl_tb.php";
                } else {
                    url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2);
                    //url = "https://auth.maaltalk.com/certcid_global/req_account_info_intl_tb.php";
                }
                if (url == null || url.length() == 0) {
                    Log.e(THIS_FILE, "URL_REQ_ACCOUNT_INFO ERROR");
                    showMessage("URL_REQ_ACCOUNT_INFO ERROR");
                    return "";
                }

                if (number != null)
                    number = number.replaceAll(" ", "");

                qry_url = url + "?cid=" + number + "&country_code=" + country_code + "&distributor_id=" + distributor_id + "&birth_date=" + birth_date + "&user_name=" + user_name + "&gender=" + gender;

                DebugLog.d("test_v2 AuthActivityV2 query_url: "+qry_url);
            }


            String str = postData(mContext, qry_url);
            DebugLog.d("test_v2 AuthActivityV2 postData result: "+str);
            return str;
        }

        @Override
        protected void onProgressUpdate(Integer... params) {

        }

        @Override
        protected void onPostExecute(String str) {

            Log.d(THIS_FILE,"JSON:"+str);
            if (str == null || str.trim().length()==0) {
                Log.e(THIS_FILE, "NOT RESPONSE");
                if (! AuthActivityV2.this.isFinishing()) {
                    if (pDialog!=null && pDialog.isShowing()){
                        pDialog.dismiss();
                    }

                    showMessage(getResources().getString(R.string.error_server));
                }

                return ;
            }

            String id = null;
            String password = null;
            String cid = null;
            String exist = null; // BJH 2017.10.25
            String device = null;

            try {
                JSONObject jObject = new JSONObject(str);
                JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                Log.d("RES_CODE", responseObject.getString("RES_CODE"));
                Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
                Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

                if (responseObject.getInt("RES_CODE") == 0) {
                    JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                    if (certObject.has("ID"))
                        id = certObject.getString("ID");
                    if (certObject.has("PASSWORD"))
                        password = certObject.getString("PASSWORD");
                    if (certObject.has("CID"))
                        cid = certObject.getString("CID");
                    if (certObject.has("EXIST"))
                        exist = certObject.getString("EXIST");
                    if (certObject.has("DEVICE"))
                        device = certObject.getString("DEVICE");
                } else {
                    Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR =" + responseObject.getString("RES_DESC"));
                    pDialog.dismiss();
                    showMessage(responseObject.getString("RES_DESC"));
                    return ;
                }

            } catch (Exception e) {
                Log.e(THIS_FILE, "JSON", e);
                pDialog.dismiss();
                showMessage(getResources().getString(R.string.error_data_recv));
                return ;
            }

            if (id == null || id.length() == 0) {
                // 인증요청 오류
                pDialog.dismiss();
                showMessage(getResources().getString(R.string.auth_number_error_id));
                return ;
            }

            if (password == null || password.length() == 0) {
                // 인증요청 오류
                pDialog.dismiss();
                showMessage(getResources().getString(R.string.auth_number_error_pwd));
                return ;
            }

            // SAVE NUMBER
            mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, cid);
            mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

            mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
            mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
            mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
            mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);




            MyReqSvcInfoPostTask myReqSvcInfoPostTask =new MyReqSvcInfoPostTask();
            String[] params=new String[5];
            params[0]=id;
            params[1]=password;
            params[2]=cid;
            params[3]=exist;
            params[4]=device;

            myReqSvcInfoPostTask.execute(params);

            super.onPostExecute(str);
        }
    }

    public class MyReqSvcInfoPostTask extends AsyncTask<String, Integer, Integer> {

        String id;
        String password;
        String cid;
        String exist;
        String device;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!pDialog.isShowing()){
                MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(AuthActivityV2.this);
                builder.setMessage("Loading");
                builder.setCancelable(false);
                pDialog = builder.create();
                pDialog.show();
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            id=params[0];
            password=params[1];
            cid=params[2];
            exist=params[3];
            device=params[4];

            // DO LOGIN

            int result=ServicePrefs.login(AuthActivityV2.this, id, password);
            return Integer.valueOf(result);
        }

        @Override
        protected void onProgressUpdate(Integer... params) {

        }

        @Override
        protected void onPostExecute(Integer params) {
            if (params >= 0) {
                //pDialog.dismiss();
                String model = android.os.Build.MODEL;
                if (exist != null && exist.equals("EXIST") && model != null && model.length() > 0 && device != null && device.length() > 0 && !device.contains(model)) {
                    logoutAlert();
                } else {
                    Intent intent = new Intent(AuthActivityV2.this, DialMain.class);
                    startActivity(intent);
                    finish();
                }
            } else {
                pDialog.dismiss();
                showMessage(getResources().getString(R.string.error_login));
            }

            super.onPostExecute(params);
        }
    }
}
