package com.dial070.ui;

import android.text.Html;

public class NaverResult {
	private String title;
	//private String link;
	private String description;
	private String telephone;
	private String address;
	private String mapx;
	private String mapy;
	
	public NaverResult()
	{
		//초기화
		this.title = "";
		this.description = "";
		this.telephone = "";
		this.address = "";
		this.mapx = "";
		this.mapy = "";
	}
	
	public String stripHtml(String html) {
	    return Html.fromHtml(html).toString();
	}	
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	public void setDescription(String desc)
	{
		this.description = stripHtml(desc);
	}
	public void setTelephone(String phone)
	{
		this.telephone = phone;
	}	
	public void setAddress(String addr)
	{
		this.address = addr;
	}	
	public void setMapX(String x)
	{
		this.mapx = x;
	}	
	public void setMapY(String y)
	{
		this.mapy = y;
	}	
	
	
	public String getTitle()
	{
		return this.title;
	}
	public String getDescription()
	{
		return this.description;
	}
	public String getTelephone()
	{
		return this.telephone;
	}	
	public String getAddress()
	{
		return this.address;
	}	
	public String getMapX()
	{
		return this.mapx;
	}	
	public String getMapY()
	{
		return this.mapy;
	}		
}
