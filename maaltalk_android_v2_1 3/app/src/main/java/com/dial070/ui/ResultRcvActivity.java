package com.dial070.ui;

import com.dial070.App;
import com.dial070.utils.Log;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ResultRcvActivity extends Activity
{
	private static final String THIS_FILE = "PAY CLIENT";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.d(THIS_FILE, "[ResultRcvActivity] called__onCreate" );

        super.onCreate(savedInstanceState);

        // TODO Auto-generated method stub
        App myApp    = (App)getApplication();
        Intent            myIntent = getIntent();

        Log.d(THIS_FILE, "[ResultRcvActivity] launch_uri=[" + myIntent.getData().toString() + "]" );

        if ( myIntent.getData().getScheme().equals( "dialpay" ) == true )
        {
            myApp.b_type      = true;
            myApp.m_uriResult = myIntent.getData();
        }
        else
        {
            myApp.m_uriResult = null;
        }
        
        finish();
    }
}