package com.dial070.ui;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;


class PhoneItem {
	String PhoneType;
	String PhoneNumber;
	PhoneItem(String type, String number)
	{
		PhoneType= type;
		PhoneNumber = number;
	}
}

public class ContactsData implements Parcelable {
	private String mDisplayName;
	private String mPhoneNumber;
	private String mContactId;
	private String mPhotoId;
	ArrayList<PhoneItem> mUserPhoneList;
	private boolean mSelected = false;	
	
	public ContactsData(String user_name, String phone_number, String user_id, String photo_id, ArrayList<PhoneItem> phone_list)
	{
		mDisplayName = user_name;
		mPhoneNumber = phone_number;
		mContactId = user_id;
		mPhotoId = photo_id;		
		mUserPhoneList = phone_list;
		mSelected = false;
	}
	
	public ContactsData(Parcel in)
	{
		mDisplayName = in.readString();
		mPhoneNumber = in.readString();
		mContactId = in.readString();
		mPhotoId = in.readString();
		mUserPhoneList = null;
	}
	
    public static final Parcelable.Creator<ContactsData> CREATOR =
            new Parcelable.Creator<ContactsData>() {
 
        @Override
        public ContactsData createFromParcel(Parcel source) {
            return new ContactsData(source);
        }
 
        @Override
        public ContactsData[] newArray(int size) {
            return new ContactsData[size];
        }
 
    };

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		arg0.writeString(mDisplayName);
		arg0.writeString(mPhoneNumber);
		arg0.writeString(mContactId);
		arg0.writeString(mPhotoId);

	}

	public ContactsData(String header)
	{
		mDisplayName = header;
		mPhoneNumber = null;
		mContactId = null;
		mPhotoId = null;		
		mUserPhoneList = null;
		mSelected = false;
	}	
	
	public String getDisplayName()
	{
		return mDisplayName;
	}
	
	public String getPhoneNumber()
	{
		return mPhoneNumber;
	}
		
	public String getContactId()
	{
		return mContactId;
	}

	public String getPhotoId()
	{
		return mPhotoId;
	}	
	
	public void setDisplayName(String name)
	{
		mDisplayName = name;
	}
	
	public void setPhoneNumber(String phone)
	{
		mPhoneNumber = phone;
	}
		
	public void setContactId(String id)
	{
		mContactId = id;
	}

	public void setPhotoId(String photo_id)
	{
		mPhotoId = photo_id;
	}	
	
	
	public ArrayList<PhoneItem> getUserPhoneList()
	{
		return mUserPhoneList;
	}
	
	public void setSelected(boolean selected)
	{
		mSelected = selected;
	}
	
	public boolean getSelected()
	{
		return mSelected;
	}
		
}
