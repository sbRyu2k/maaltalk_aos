package com.dial070.ui;

import com.dial070.maaltalk.R;

import com.dial070.utils.ContactHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SmsPhoneListView extends LinearLayout {

	private TextView mNumberView;	
	private String mPhoneNumber;
	private Context mContext;
	
	public SmsPhoneListView(Context context, String number)
	{
		super(context);
		mContext = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.sms_phone_list_item, this, true);		
		
		mNumberView = (TextView) findViewById(R.id.sms_phone_list_number);
		
		String name = ContactHelper.getContactsNameByPhoneNumber(mContext,number);
		if(name == null)
		{
			name = number;
		}
	
		mNumberView.setText(name);
		
		mPhoneNumber = number;
	}
	
	public void setNumber(String number)
	{
		String name = ContactHelper.getContactsNameByPhoneNumber(mContext,number);
		if(name == null)
		{
			name = number;
		}
		mNumberView.setText(name);
		mPhoneNumber = number;
	}	
	
	public String getNumber()
	{
		return mPhoneNumber;
	}	
}
