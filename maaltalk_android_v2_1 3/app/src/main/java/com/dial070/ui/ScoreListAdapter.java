package com.dial070.ui;

import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.db.ScoreData;
import com.dial070.utils.ContactHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ScoreListAdapter extends BaseAdapter {

	private static final int SCORE_DISPLAY_COUNT = 24;
	
	private Context mContext;
	private DBManager mDatabase;	
	private ScoreItem[] mScoreList;
	private int mScoreCount;
	
	public ScoreListAdapter(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		
		mScoreCount = 0;
		
		mDatabase = new DBManager(mContext);
		
		mScoreList = new ScoreItem[SCORE_DISPLAY_COUNT];
	}

	public void destroyAdapter()
	{
		if (mDatabase != null)
		{
			mDatabase.close();
			mDatabase = null;
		}
	}

	public void buildScoreData()
	{
		if (mDatabase == null) return;

		if (!mDatabase.isOpen()) mDatabase.open();

		Cursor c = mDatabase.getScoreList();

		int offset = 0;
		if (c != null)
		{
			String contact_name = null;
			String UserName = null;
			String PhoneNumber = null;
			int TotalScore,CallCountScore,ZoneScore,CallStartScore,CallDurScore,CallStateScore,ConAddScore,FavAddScore;
			
			if (c.moveToFirst())
			{
				do
				{

					UserName = c.getString(c.getColumnIndex(FavoritesData.FIELD_DISPLAYNAME));
					
					PhoneNumber = c.getString(c.getColumnIndex(FavoritesData.FIELD_PHONENUMBER));
					
					contact_name = ContactHelper.getContactsNameByPhoneNumber(mContext,PhoneNumber);
					if(contact_name != null) UserName = contact_name; 
					
					TotalScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_SCORE));
					CallCountScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_COUNT_SCORE));
					ZoneScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_ZONE_SCORE));
					CallStartScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_START_SCORE));
					CallDurScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_DUR_SCORE));
					CallStateScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_STATE_SCORE));
					ConAddScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_CON_ADD_SCORE));
					FavAddScore = c.getInt(c.getColumnIndex(ScoreData.FIELD_FAV_ADD_SCORE));

					mScoreList[offset] = new ScoreItem(UserName,PhoneNumber, TotalScore, CallCountScore, ZoneScore, CallStartScore,CallDurScore,CallStateScore,ConAddScore,FavAddScore);

					offset++;
				}
				while (c.moveToNext() && (offset < SCORE_DISPLAY_COUNT) );
			}
			c.close();
		}
		
		mScoreCount = offset;
		
	}	
	
	private ScoreItem read(int offset)
	{
		if (offset < 0 || offset >= SCORE_DISPLAY_COUNT) return null;

		return mScoreList[offset];		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mScoreCount;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return read(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		ScoreListView scoreView;
		ScoreItem item = read(arg0);
		//if(item == null) return null;
		
		if (arg1 == null)
		{
			scoreView = new ScoreListView(mContext, read(arg0));
		}
		else
		{
			scoreView = (ScoreListView) arg1;
		}
		scoreView.setData(item);


		return scoreView;
	}

}
