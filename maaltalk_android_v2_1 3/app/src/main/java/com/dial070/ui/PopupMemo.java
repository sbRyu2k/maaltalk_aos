package com.dial070.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.maaltalk.R;

public class PopupMemo extends Activity {

	private Context mContext;
	private int mId;
	private String mType,mName, mNumber,mMemo,mOldMemo;	
	private TextView mTextName, mTextPhone/*, mTextMemo*/;
	private EditText mEditMemo;
	private ImageButton mButtonClose, mButtonDelete, mButtonShare;
//	private InputMethodManager mImm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.popup_memo_detail);		
		
		mContext = this;	
		
		Intent intent = getIntent();
		mType = intent.getStringExtra("TYPE");
		mName = intent.getStringExtra("NAME");
		mNumber = intent.getStringExtra("PHONE");
		mMemo = intent.getStringExtra("MEMO");
		mId = intent.getIntExtra("ID",0);
		mOldMemo = mMemo;		
		
//		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		
		mTextName = (TextView) findViewById(R.id.txt_user_name);
		mTextPhone = (TextView) findViewById(R.id.txt_user_phone);
		mEditMemo = (EditText) findViewById(R.id.edit_user_memo);
		//초기화
		if (mNumber == null) mName = "";
		if (mNumber == null) mNumber = "";	
		
		mTextName.setText(mName);
		mTextPhone.setText(mNumber);
		
		if(mMemo != null)
		{
			if(mMemo.length() > 0)
				mEditMemo.setText(mMemo);
			else
				mEditMemo.setHint(getResources().getString(R.string.memo_hint));
			}
		else mEditMemo.setHint(getResources().getString(R.string.memo_hint));
			
		
		mButtonClose = (ImageButton) findViewById(R.id.btn_popup_close);
		mButtonDelete = (ImageButton) findViewById(R.id.btn_popup_delete);
		mButtonShare = (ImageButton) findViewById(R.id.btn_popup_share);	
		//mButtonClose = (Button) findViewById(R.id.btn_popup_close);
		//mButtonDelete = (Button) findViewById(R.id.btn_popup_delete);
		//mButtonShare = (Button) findViewById(R.id.btn_popup_share);	
		
		if(mId == 0)
		{
			mButtonDelete.setEnabled(false);
			mButtonShare.setEnabled(false);
		}
		
		mButtonClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mId > 0)
				{				
					updateMemo(mId,mEditMemo.getText().toString());
					//if(mType.equals("FAVORITES")) 
					{
						Intent intent = new Intent();
						intent.putExtra("MEMO", mEditMemo.getText().toString());
						setResult(RESULT_OK,intent);
					}
					finish();
				}
				else
				{
					Intent intent = new Intent();
					intent.putExtra("MEMO", mEditMemo.getText().toString());
					setResult(RESULT_OK,intent);
					finish();					
				}
			}
		});
		
		mButtonDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mId >0)
				{
					deleteMemo(mId);
					Intent intent = new Intent();
					intent.putExtra("MEMO", "");
					setResult(RESULT_OK,intent);					
					finish();
				}
			}
		});
		
		mButtonShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String memo = mEditMemo.getText().toString();
				
				if(memo.length() == 0) return;
				
				DialMain.skipClose = true;
				
				try {
					Intent msg = new Intent(Intent.ACTION_SEND);
					msg.addCategory(Intent.CATEGORY_DEFAULT);
					msg.putExtra(Intent.EXTRA_SUBJECT, mNumber);
					msg.putExtra(Intent.EXTRA_TEXT, memo);
					msg.putExtra(Intent.EXTRA_TITLE, mName);
					msg.setType("text/plain");    
					startActivity(Intent.createChooser(msg, getResources().getString(R.string.share)));
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		/*
		mEditMemo.requestFocus();
		mImm.showSoftInput(mEditMemo, 0);
		*/		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		mEditMemo.requestFocus();
		mEditMemo.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                InputMethodManager keyboard = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(mEditMemo, 0);
            }
        },200);
		
		
	}
	
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		if(!mOldMemo.equalsIgnoreCase(mEditMemo.getText().toString()))
		{
			AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
			msgDialog.setTitle(getResources().getString(R.string.memo_input));
			msgDialog.setMessage(getResources().getString(R.string.memo_save_quest));
			msgDialog.setButton(mContext.getResources().getString(R.string.yes), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					mButtonClose.performClick();
					return;
				}
			});
			msgDialog.setButton2(mContext.getResources().getString(R.string.no), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					finish();
					return;
				}
			});
			msgDialog.show();			
		}
		else
		{
			super.onBackPressed();
		}
	}

	private void updateMemo(int id,String memo)
	{
		if(id <= 0) return;
		
		DBManager database = new DBManager(mContext);

		try
		{
			database.open();
			if(mType.equals("CALLS")) database.updateCallsMemo(id, memo);
			else if(mType.equals("FAVORITES")) database.updateFavoritesMemo(id, memo);
		} finally {
			database.close();
		}
	}
	
	
	private void deleteMemo(int id)
	{
		if(id <= 0) return;
		
		DBManager database = new DBManager(mContext);

		try
		{
			database.open();
			if(mType.equals("CALLS")) database.deleteCallsMemo(id);
			else if(mType.equals("FAVORITES")) database.deleteFavoritesMemo(id);
		} finally {
			database.close();
		}
	}
	
	
}
