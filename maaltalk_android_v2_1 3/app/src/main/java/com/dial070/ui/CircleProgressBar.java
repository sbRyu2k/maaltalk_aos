package com.dial070.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import androidx.annotation.NonNull;

import com.dial070.maaltalk.R;

public class CircleProgressBar extends Dialog {

    public CircleProgressBar(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.progress_circle_white);
    }

}
