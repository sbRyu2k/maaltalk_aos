package com.dial070.ui;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dial070.db.DBManager;
import com.dial070.db.SmsMsgData;
import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

public class SmsMsgContsListAdapter extends BaseAdapter {
//	private static final String THIS_FILE="SmsMsgContsListAdapter";
	private Context mContext;
	private DBManager mDatabase;
	
//	private Resources mResources;
	private String mUserName;
	private boolean mSelectMode;
	
	private ArrayList<SmsMsgContsItem> mSmsMsgContsItems;
	public final static int TYPE_SECTION_HEADER = 0;
	public final ArrayAdapter<String> mHeaders;
	private ArrayList<Integer> mSelectedList;
	private ArrayList<SmsMsgContsListView> mViewList;
	
	public SmsMsgContsListAdapter(Context context)
	{
		mContext = context;
		mDatabase = new DBManager(context);
		if(!mDatabase.isOpen()) mDatabase.open();
		
		
//		mResources = mContext.getResources();
		mSelectMode = false;
		mHeaders = new ArrayAdapter<String>(context, R.layout.contacts_list_header);
		mUserName = null;
		mSelectedList = new ArrayList<Integer>();
		mSmsMsgContsItems = new ArrayList<SmsMsgContsItem>();		
		
		mViewList = new ArrayList<SmsMsgContsListView>();
	}
	
	
	public void destroyAdapter()
	{
		if (mDatabase != null)
		{
			mDatabase.close();
			mDatabase = null;
		}
		
		if (mViewList != null)
		{
			for (SmsMsgContsListView view : mViewList)
			{
				view.clearResource();
			}
			mViewList.clear();
			mViewList = null;
		}
	}
	
	public void delete(int id)
	{
		if (mDatabase != null)
		{
			mDatabase.deleteSmsMsg(id);
		}
		for (SmsMsgContsItem item : mSmsMsgContsItems)
		{
			if (item.getSmsMsgId() == id)
			{
				int index = mSmsMsgContsItems.indexOf(item);
				mSmsMsgContsItems.remove(index);
				break;
			}
		}
	}

	
	private long getLong(String s)
	{
		long v = 0;
		if (s == null) return 0;
		s = s.trim();
		if (s.length() == 0) return 0;
		
		try {
			v = Long.parseLong(s);
		} catch (NumberFormatException e) {
		}		
		return v;
	}
	
	
	private boolean compareDay(String a, String b)
	{
		if (a == null || a.length() == 0)
			return false;
		if (b == null || b.length() == 0)
			return false;
		
		Date d1 = new Date(getLong(a));
		Date d2 = new Date(getLong(b));
	
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd",Locale.getDefault());
		return fmt.format(d1).equals(fmt.format(d2));
		
	}

	public int loadSmsMsgContsData(String number)
	{
		int count = 0;
		
		if(number == null || number.length() == 0) return count;
		
		if(mSmsMsgContsItems != null)
			mSmsMsgContsItems.clear();
		else
			return count;
		
		
		// Db
		DBManager database = new DBManager(mContext);
		database.open();
		
		Cursor c = database.getSmsMsg(number);		
		
		if(c == null)
		{
			database.close();
			return count;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			database.close();
			return count;			
		}
		int id = 0;
		String time = null;
		String from = null;
		String to = null;
		String msg = null;
		int type = 0;
		
		String file1 = null;
		String file2 = null;
		String file3 = null;
		
		String oldtime = null;
		//BJH 2016.10.13
		String vms = null;
		
		do {
			id = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_ID));
			time = c.getString(c.getColumnIndex(SmsMsgData.FIELD_DATE));
			from = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FROM));
			to =  c.getString(c.getColumnIndex(SmsMsgData.FIELD_TO));
			msg = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
			type = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_MSG_TYPE));
			file1 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE1));
			file2 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE2));
			file3 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE3));		
			
			boolean showday = !compareDay(oldtime, time);
			oldtime = time;
			//BJH 2016.10.13
			if(msg != null && msg.length() > 0 && msg.startsWith(mContext.getString(R.string.pns_vms_title))) {
				String[] strArr = msg.split("\\|");
				msg = strArr[0];
				vms = strArr[1];
			} else {
				vms = null;
			}
			
			addSmsMsgItem(id,from,to,msg,time,type,file1, file2, file3, showday, vms);

		} while(c.moveToNext());
		
		c.close();
		database.close();
		return count;
	}
	
	
	public int loadMoreSmsMsgContsData(String number)
	{
		int count = 0;
		// Db
		DBManager database = new DBManager(mContext);
		database.open();
		
		Cursor c = database.getMoreSmsMsg(number);		
		
		if(c == null)
		{
			database.close();
			return count;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			database.close();
			return count;			
		}
		
		int id = 0;
		String time = null;
		String from = null;
		String to = null;
		String msg = null;
		String file1 = null;
		String file2 = null;
		String file3 = null;
		//BJH 2016.10.13
		String vms = null;
		
		int type = 0;
		
		do {
			id = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_ID));
			time = c.getString(c.getColumnIndex(SmsMsgData.FIELD_DATE));
			from = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FROM));
			to =  c.getString(c.getColumnIndex(SmsMsgData.FIELD_TO));
			msg = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
			type = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_MSG_TYPE));
			file1 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE1));
			file2 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE2));
			file3 = c.getString(c.getColumnIndex(SmsMsgData.FIELD_FILE3));
			//BJH 2016.10.13
			if(msg != null && msg.length() > 0 && msg.startsWith(mContext.getString(R.string.pns_vms_title))) {
				String[] strArr = msg.split("\\|");
				msg = strArr[0];
				vms = strArr[1];
			} else {
				vms = null;
			}

			count++;
			addSmsMsgItem(id,from,to,msg,time,type, file1, file2, file3, false, vms);

		} while(c.moveToNext());
		
		c.close();
		database.close();
		return count;		
		
	}
	//BJH 2016.10.13
	public synchronized boolean addSmsMsgItem(int id, String from, String to, String msg, String time, int type, String file1, String file2, String file3, boolean showday, String vms)
	{
		if(mSelectMode && type == SmsMsgData.HEADER_TYPE) return true;
		
		SmsMsgContsItem item = new SmsMsgContsItem(id, from,to,msg,time, type, file1, file2, file3, vms);
		item.setShowDay(showday);
		
		return mSmsMsgContsItems.add(item);			
	}
	
	public void setRead(String number)
	{
//		boolean rs = false;
		DBManager database = new DBManager(mContext);
		database.open();
		database.updateReadSmsMsg(number);
		database.close();
	}
	
	public boolean setCheckedItem(int id, boolean selected)
	{
		if(mSelectedList == null) return false;

		if (selected)
		{
			if (!mSelectedList.contains(id))
			{
				mSelectedList.add(id);
			}
		}
		else
		{
			if (mSelectedList.contains(id))
			{
				int index = mSelectedList.indexOf(id);
				if(index < 0) return false;
				mSelectedList.remove(index);
			}
		}
		
		return true;
	}		
	
	public boolean setCheckedAllItem(boolean selected)
	{
		if(mSelectedList == null) return false;
		mSelectedList.clear();
		
		if (selected)
		{
			for(int i=0; i < mSmsMsgContsItems.size(); i++)
			{
				SmsMsgContsItem item = mSmsMsgContsItems.get(i);
				if(item != null)
					mSelectedList.add(item.getSmsMsgId());
			}
		}
						
		return true;
	}
	
	public ArrayList<Integer> getCheckedList()
	{
		return mSelectedList;
	}
		
	public int getCheckedCount()
	{
		return mSelectedList.size();
	}
	
	public void checkClear()
	{
		mSelectedList.clear();	
	}
	
	public void setSelectMode(boolean mode)
	{
		mSelectMode = mode;
	}
	
	public boolean deleteSmsMsgConts(String number)
	{
//		SQLiteDatabase db = new ChatDBConnector(mContext).getWritableDatabase();
//		if(db.delete(Constants.TABLE_CHAT_MESSAGE, Constants.TABLE_CHAT_MESSAGE_FIELD_JID + "='" +  number + "'",null) > 0)
//		{
//			db.delete(Constants.TABLE_CHAT_LIST, Constants.TABLE_CHAT_LIST_FIELD_JID + "='" +  number + "'",null);
//		}
//		db.close();
		return true;
	}
	
	public void setUserName(String name)
	{
		mUserName = name;
	}
	
	private void detailMsgConts(int id, String msg, String phone, String time)
	{
		Intent intent = new Intent(mContext, SmsMsgContsDetail.class);
		intent.putExtra("id", id);
		intent.putExtra("msg", msg);
		intent.putExtra("date", time);		
		intent.putExtra("phone", phone);
		intent.putExtra("name", mUserName);
		mContext.startActivity(intent);			
	}
	
	private SmsMsgContsItem read(int offset)
	{
		if (offset < 0 || offset >= mSmsMsgContsItems.size()) return null;
		
		return mSmsMsgContsItems.get(offset);
	}		
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mSmsMsgContsItems == null) return 0;
		return mSmsMsgContsItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return read(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SmsMsgContsListView smsMsgContsView = null;
		SmsMsgContsItem item = read(position);
		
		if(convertView == null)
		{
			smsMsgContsView = new SmsMsgContsListView(mContext, this, mDatabase, item);
			if (!mViewList.contains(smsMsgContsView))
				mViewList.add(smsMsgContsView);
		}
		else
		{
			smsMsgContsView = (SmsMsgContsListView) convertView;
		}
		
		if(smsMsgContsView != null && item != null)
		{
			smsMsgContsView.setSmsMsgContsData(item, mSelectMode, mSelectedList.contains(item.getSmsMsgId()));
			
			
			TextView smsMsgTo = (TextView) smsMsgContsView.findViewById(R.id.sms_msg_to);
			
			final long id = item.getSmsMsgId();
			final String msg = item.getSmsMsg();
			final String time = item.getSmsMsgTime();
			final String phone = item.getSmsMsgTo();
//			final String from = item.getSmsMsgFrom();
//			final SmsMsgContsListView view = smsMsgContsView;
			
			if(!mSelectMode)
			{
				smsMsgTo.setOnClickListener(null);
				
//				smsMsgTo.setOnClickListener(new View.OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						detailMsgConts(id,msg,phone,time);
//					}
//				});
			}
			else
			{
				//mSelectedList.contains(id);
				smsMsgTo.setOnClickListener(null);

//				smsMsgTo.setOnClickListener(new View.OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						setCheckedItem(id, !mSelectedList.contains(id));
//						view.setCheckBox(from,mSelectedList.contains(id));
//					}
//				});				
			}
		}
				
		return smsMsgContsView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return mSelectMode;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return mSelectMode;
	}	
	
}
