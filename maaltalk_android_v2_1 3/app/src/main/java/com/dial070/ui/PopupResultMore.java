package com.dial070.ui;

import java.util.ArrayList;

import com.dial070.maaltalk.R;
import com.dial070.sip.utils.Compatibility;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class PopupResultMore extends AppCompatActivity {

	private ListView mListView;
	private Button mCancel;
	
	private ArrayList<ContactsData> mContactList;

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final ActionBar abar = getSupportActionBar();
		androidx.appcompat.app.ActionBar.LayoutParams params = new androidx.appcompat.app.ActionBar.LayoutParams(//Center the textview in the ActionBar !
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				Gravity.CENTER);
		ImageButton imgBtnBack = viewActionBar.findViewById(R.id.imgBtnBack);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(false);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		imgBtnBack.setVisibility(View.VISIBLE);
		imgBtnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_result_more);

		
		Intent intent = getIntent();
		mContactList = intent.getParcelableArrayListExtra("RESULT");

		setTitle(getString(R.string.ui_search_result)+": "+mContactList.size()+getString(R.string.ui_search_result_count_unit));

		mListView = (ListView) findViewById(R.id.result_more_list);
		
		mCancel = (Button) findViewById(R.id.btn_cancel_result_more);
		mCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		ContactsResultAdapater adapter = new ContactsResultAdapater(this);
		adapter.setData(mContactList);
		if (Compatibility.getApiLevel() > 10)
			mListView.setSelector(R.drawable.list_selector);
		mListView.setAdapter(adapter);
		
		
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				//
				ContactsListView view = (ContactsListView)arg1;
				if(view == null) return;
				ContactsData item = (ContactsData) view.getItem();
				if(item != null)
				{
					Intent intent = new Intent();
					intent.putExtra("PhoneNumber", Html.fromHtml(item.getPhoneNumber()).toString());
					intent.putExtra("DisplayName", Html.fromHtml(item.getDisplayName()).toString());
					setResult(RESULT_OK,intent);					
					finish();
				}		
			}
		});
	}

	
	
	
}
