package com.dial070.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.Gravity;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;

import com.dial070.dao.DaoTrip;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CategorySelectPhotoActivity extends Activity {
    final int REQUEST_CODE_PERMISSIONS = 1;
    final int REQUEST_CODE_PERMISSIONS_MORE = 2;
    private final static String TAG = "CategorySelectPhotoActivity";
    ArrayList<Uri> path;
    private Context mContext;
    private AppPrefs mPrefs;
    private Activity activity;
    ArrayList<Uri> uriArraylist;
    private ArrayList<DaoTrip> tripArrayList;
    private ArrayList<Integer> selectedPositions;
    private ArrayList<DaoTrip> selectedTripArrayList;
    private boolean isReturn=false;
    private String uid;
    private static int completeAsyncTaskCount=0;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_category_select_photo);

        activity=this;
        mContext=this;
        mPrefs = new AppPrefs(mContext);

        pDialog=new ProgressDialog(mContext);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage("Photo Uploading...");

        path=new ArrayList<>();
        uriArraylist =new ArrayList<>();

        uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

        String agree=mPrefs.getPreferenceStringValue(AppPrefs.PHOTO_UPLOAD_AGREE);
        if (agree!=null && agree.length()>0){
//            start();
        }else {
            AlertDialog msgDialog = new AlertDialog.Builder(this).create();
            msgDialog.setTitle("알림");
            msgDialog.setMessage("다음화면에서 사진을 선택뒤 업로드가 시작됩니다.사진 업로드에 동의하시겠습니까?동의하시면 이후에 해당 대화창은 보이지 않습니다.");
            msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                    new DialogInterface.OnClickListener() {
                        // @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_UPLOAD_AGREE,""+System.currentTimeMillis());
//                            start();
                        }
                    });
            msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            msgDialog.show();
        }

        /*MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(mContext);
        builder.setMessage("Uploading...");
        builder.setCancelable(false);
        pDialog = builder.create();*/
    }

//    private void start(){
//        Intent intent=getIntent();
//        tripArrayList= (ArrayList<DaoTrip>) intent.getSerializableExtra("Trip");
//
//        Uri uri;
//        for (DaoTrip trip:tripArrayList){
//            uri=Uri.parse(trip.getUriPath());
//            uriArraylist.add(uri);
//        }
//
//        int size= uriArraylist.size();
//        if (uriArraylist !=null && size>0){
//            completeAsyncTaskCount=0;
//            FishBun.with(activity)
//                    .setImageAdapter(new GlideAdapter())
//                    .setIsUseDetailView(false)
//                    .setOriginImages(uriArraylist)
//                    .setMaxCount(1)
//                    .setMinCount(1)
//                    .setPickerSpanCount(4)
//                    .setActionBarColor(Color.parseColor("#00A1C8"), Color.parseColor("#0492b5"), false)
//                    .setActionBarTitleColor(Color.parseColor("#ffffff"))
//                    .setAlbumSpanCount(2, 4)
//                    .setButtonInAlbumActivity(false)
//                    .setCamera(false)
//                    .setReachLimitAutomaticClose(false)
//                    .setHomeAsUpIndicatorDrawable(ContextCompat.getDrawable(this, R.drawable.ic_custom_back_white))
//                    .setAllViewTitle("발자취 사진 선택")
//                    .setActionBarTitle("Image Library")
//                    .textOnImagesSelectionLimitReached("더이상 선택할 수 없습니다")
//                    .textOnNothingSelected("아무것도 선택하지 않았습니다.")
//                    .setSelectCircleStrokeColor(Color.BLACK)
//                    .isStartInAllView(true)
//                    .startAlbum();
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
        /*if (!isReturn){
            this.finish();
        }*/
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageData) {
        super.onActivityResult(requestCode, resultCode, imageData);
        Log.i(TAG,"onActivityResult requestCode:"+requestCode);
        Log.i(TAG,"onActivityResult resultCode:"+resultCode);
//        switch (requestCode) {
//            case Define.ALBUM_REQUEST_CODE:
//                if (resultCode == RESULT_OK) {
//                    isReturn=true;
//                    path = imageData.getParcelableArrayListExtra(Define.INTENT_PATH);
//                    selectedPositions = imageData.getIntegerArrayListExtra(Define.INTENT_SELECTED_POSITIONS);
//
//
//                    if (path!=null){
//                        CropImage.activity(path.get(0))
//                                .setGuidelines(CropImageView.Guidelines.ON)
//                                .setInitialCropWindowPaddingRatio(0)
//                                .setAspectRatio(1,1)
//                                .start(this);
//                    }
//
//
//                /*selectedTripArrayList=new ArrayList<>();
//                DaoTrip trip = null;
//                if (pDialog!=null){
//                    pDialog.show();
//
//                    TextView messageView = pDialog.findViewById(android.R.id.message);
//                    messageView.setGravity(Gravity.CENTER);
//                }
//
//                int index=0;
//                for (Integer integer:selectedPositions){
//                    index++;
//                    int i=integer.intValue();
//                    selectedTripArrayList.add(tripArrayList.get(i));
//                    trip=tripArrayList.get(i);
//                    HttpMultiPart(trip,index);
//                }*/
//                }else if(resultCode == Define.TRANS_IMAGES_RESULT_CODE){
//                    finish();
//                }
//                break;
//            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
//                CropImage.ActivityResult result = CropImage.getActivityResult(imageData);
//                if (resultCode == RESULT_OK) {
//                    Uri resultUri = result.getUri();
//
//                    //Log.i(TAG,"resultUri.getPath():"+resultUri.getPath());
//                    if (resultUri!=null){
//                        DaoTrip trip = null;
//                        if (pDialog!=null){
//                            pDialog.show();
//
//                            TextView messageView = pDialog.findViewById(android.R.id.message);
//                            messageView.setGravity(Gravity.CENTER);
//                        }
//
//                        int i=selectedPositions.get(0).intValue();
//                        trip=tripArrayList.get(i);
//
//                        Log.i(TAG,"trip.getUriPath():"+trip.getUriPath());
//                        trip.setFilepath(resultUri.getPath());
//                        //trip.setUriPath(getUriFromPath(resultUri.getPath()).toString());
//                        HttpMultiPart(trip,1);
//                    }
//
//
//                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                    Exception error = result.getError();
//                    finish();
//                } else if(resultCode == RESULT_CANCELED) {
//                    finish();
//                }
//                break;
//        }
    }

//    private void HttpMultiPart(final DaoTrip trip,final int index){
//
//        new AsyncTask<Void, Void, JSONObject>(){
//            int total;																				// 숫자 형 객체
//            int bytesAvailable;																		// 숫자 형 객체
//            int sendBytes;
//
//            @Override
//            protected void onPreExecute() {
//
//                File file=new File(trip.getFilepath());
//                total=(int)file.length();
//                /*pDialog.setMax(total);
//                pDialog.setProgress(0);
//                pDialog.show();*/
//
//                super.onPreExecute();
//            }
//
//            @Override
//            protected JSONObject doInBackground(Void... voids) {
//                sendBytes = 0;
//
//                String boundary = "^-----^";
//                String LINE_FEED = "\r\n";
//                String charset = "UTF-8";
//                OutputStream outputStream;
//                PrintWriter writer;
//
//                JSONObject result = null;
//                try{
//                    //Uri uri=Uri.parse(trip.getUriPath());
//                    //String sPath=getPathFromUri(uri);
//                    String sPath=trip.getFilepath();
//                    sPath=resizeBitmapImage(sPath);
//
//                    File file=new File(sPath);
//                    total=(int)file.length();
//                    if (file.length()>0){
//                        URL url = new URL("http://211.43.13.195:4001/photoshow");
//                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//
//                        connection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);
//                        connection.setRequestMethod("POST");
//                        connection.setDoInput(true);
//                        connection.setDoOutput(true);
//                        connection.setUseCaches(false);
//                        connection.setConnectTimeout(15000);
//
//                        outputStream = connection.getOutputStream();
//                        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param1\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(uid).append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param1:"+uid);
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param2\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(trip.getLocation_country()).append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param2:"+trip.getLocation_country());
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param3\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(trip.getCategoryNum()).append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param3:"+trip.getCategoryNum());
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param4\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(index+"").append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param4:"+index);
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param5\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(trip.getStartdateAndEndDate()).append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param5:"+trip.getStartdateAndEndDate());
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param6\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(trip.getLatitude()+"").append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param6:"+trip.getLatitude());
//
//                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"param7\"").append(LINE_FEED);
//                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.append(trip.getLongitude()+"").append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"param7:"+trip.getLongitude());
//
//                        /** 파일 데이터를 넣는 부분**/
//                        writer.append("--" + boundary).append(LINE_FEED);
//                        writer.append("Content-Disposition: form-data; name=\"photo\"; filename=\"" + file.getName() + "\"").append(LINE_FEED);
//                        writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(LINE_FEED);
//                        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
//                        writer.append(LINE_FEED);
//                        writer.flush();
//                        Log.i(TAG,"photo:"+file.getAbsolutePath());
//
//                        int maxBufferSize = 4*1024;
//                        int bufferSize = Math.min(total, maxBufferSize);
//                        FileInputStream inputStream = new FileInputStream(file);
//                        byte[] buffer = new byte[bufferSize];
//                        int bytesRead = -1;
//                        Log.i(TAG,"total:"+total);
//                        while ((bytesRead = inputStream.read(buffer)) != -1) {
//                            sendBytes += bytesRead;
//                            //Log.i(TAG,"sendBytes:"+sendBytes);
//                            outputStream.write(buffer, 0, bytesRead);
//                            //publishProgress();
//                        }
//                        outputStream.flush();
//                        inputStream.close();
//                        writer.append(LINE_FEED);
//                        writer.flush();
//
//                        writer.append("--" + boundary + "--").append(LINE_FEED);
//                        writer.close();
//
//                        int responseCode = connection.getResponseCode();
//                        if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
//                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                            String inputLine;
//                            StringBuffer response = new StringBuffer();
//                            while ((inputLine = in.readLine()) != null) {
//                                response.append(inputLine);
//                            }
//                            in.close();
//
//                            try {
//                                result = new JSONObject(response.toString());
//                                Log.i(TAG,"result:"+response.toString());
//                            } catch (JSONException e) {
//                                Log.e(TAG,"error:"+e.getLocalizedMessage());
//                            }
//                        } else {
//                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
//                            String inputLine;
//                            StringBuffer response = new StringBuffer();
//                            while ((inputLine = in.readLine()) != null) {
//                                response.append(inputLine);
//                            }
//                            in.close();
//                            Log.i(TAG,"responseCode not ok result:"+response.toString());
//                            result = new JSONObject(response.toString());
//                        }
//                    }else {
//
//                    }
//                } catch (ConnectException e) {
//                    Log.e(TAG, "ConnectException:"+e.getLocalizedMessage());
//
//
//                } catch (Exception e){
//                    e.printStackTrace();
//                }
//
//                return result;
//            }
//
//            @Override
//            protected void onProgressUpdate(Void... values) {
//                super.onProgressUpdate(values);
//
//                //Dialog.setProgress(sendBytes);
//            }
//
//            @Override
//            protected void onPostExecute(JSONObject jsonObject) {
//                super.onPostExecute(jsonObject);
//                if (jsonObject==null){
//                    if (pDialog!=null && pDialog.isShowing()){
//                        pDialog.dismiss();
//
//                        MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(mContext);
//                        builder.setMessage("사진변환 및 업로드를 실패하였습니다.");
//                        builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                finish();
//                            }
//                        });
//                        builder.show();
//                    }
//
//
//                }else {
//                    completeAsyncTaskCount++;
//                    if (completeAsyncTaskCount==selectedPositions.size()){
//                        if (trip!=null){
//                            new android.os.Handler().postDelayed(
//                                    new Runnable() {
//                                        public void run() {
//                                            Intent intent = new Intent(mContext, WebClient.class);
//                                            intent.putExtra("url", "http://211.43.13.195:4001/photoshow/"+uid+"?category_id=" + trip.getCategoryNum()+"&category_num=1");
//                                            intent.putExtra("title", "여행의 발자취");
//                                            intent.putExtra("category_id", trip.getCategoryNum());
//                                            intent.putExtra("category_num", "1");
//                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP /*| Intent.FLAG_ACTIVITY_NO_ANIMATION*/);
//                                            startActivity(intent);
//                                        }
//                                    }, 2000);
//
//                        }
//
//                        new android.os.Handler().postDelayed(
//                                new Runnable() {
//                                    public void run() {
//                                        if (pDialog!=null && pDialog.isShowing()){
//                                            pDialog.dismiss();
//                                        }
//
//                                        finish();
//                                    }
//                                }, 2000);
//                    }
//                }
//
//            }
//
//        }.execute();
//    }

//    public String getPathFromUri(Uri uri){
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null );
//        cursor.moveToNext();
//        String path = cursor.getString( cursor.getColumnIndex( "_data" ) );
//        //cursor.close();
//        return path;
//
//    }

    private String resizeBitmapImage(String path)
    {
        ExifInterface oldExif = null;
        try {
            oldExif = new ExifInterface(path);
        } catch (IOException e) {
            Log.e(TAG,"error:"+e.getMessage());
        }

        int degree=0;
        String exifOrientation = oldExif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int nExifOrientation = oldExif.getAttributeInt(ExifInterface.TAG_ORIENTATION,-1);
        Log.i(TAG,"nExifOrientation:"+nExifOrientation);
        boolean isPortrait=false;
        if (nExifOrientation==ExifInterface.ORIENTATION_ROTATE_90){
            isPortrait=true;
            degree=90;
        }else if (nExifOrientation==ExifInterface.ORIENTATION_ROTATE_270){
            isPortrait=true;
            degree=270;
        }else if (nExifOrientation==ExifInterface.ORIENTATION_ROTATE_180){
            degree=180;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path,options);

        int width = options.outWidth;
        int height = options.outHeight;
        int newWidth = width;
        int newHeight = height;
        float rate = 0.0f;

        /*if(width > height)
        {
            if(maxResolution < width)
            {
                rate = maxResolution / (float) width;
                newHeight = (int) (height * rate);
                newWidth = maxResolution;
            }
        }
        else
        {
            if(maxResolution < height)
            {
                rate = maxResolution / (float) height;
                newWidth = (int) (width * rate);
                newHeight = maxResolution;
            }
        }*/

        if(!isPortrait)  //가로사진일
        {
            int maxResolution=1080;

            if(width >= height)
            {
                if(maxResolution < height)
                {
                    rate = maxResolution / (float) height;
                    newWidth = (int) (width * rate);
                    newHeight = maxResolution;
                }
            }else{

                if(maxResolution < width)
                {
                    rate = maxResolution / (float) width;
                    newHeight = (int) (height * rate);
                    newWidth = maxResolution;
                }
            }
        }
        else    //세로사진일시
        {
            int maxResolution=1080;
            if(width >= height)
            {
                if(maxResolution < height)
                {
                    rate = maxResolution / (float) height;
                    newWidth = (int) (width * rate);
                    newHeight = maxResolution;
                }
            }else{

                if(maxResolution < width)
                {
                    rate = maxResolution / (float) width;
                    newHeight = (int) (height * rate);
                    newWidth = maxResolution;
                }
            }

        }

        options.inSampleSize = calculateInSampleSize(options, newWidth, newHeight);
        options.inJustDecodeBounds = false;

        Bitmap source = BitmapFactory.decodeFile(path,options);

                Bitmap resizedBitmap=Bitmap.createScaledBitmap(source, newWidth, newHeight, true);
        resizedBitmap=RotateBitmap(resizedBitmap,degree);

        String newFilePath=saveBitmapToJpeg(mContext,resizedBitmap,""+System.currentTimeMillis());
        /*if (exifOrientation != null){
            ExifInterface newExif;
            try {
                newExif = new ExifInterface(newFilePath);
                newExif.setAttribute(ExifInterface.TAG_ORIENTATION, exifOrientation);
                newExif.saveAttributes();
            } catch (IOException e) {
                Log.e(TAG,"error:"+e.getMessage());
            }
        }*/
        return newFilePath;
    }

    private static String saveBitmapToJpeg(Context context,Bitmap bitmap, String name){

        File storage = context.getCacheDir(); // 이 부분이 임시파일 저장 경로

        String fileName = name + ".jpg";  // 파일이름은 마음대로!

        File tempFile = new File(storage,fileName);

        try{
            tempFile.createNewFile();  // 파일을 생성해주고

            FileOutputStream out = new FileOutputStream(tempFile);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , out);  // 넘거 받은 bitmap을 jpeg(손실압축)으로 저장해줌

            out.close(); // 마무리로 닫아줍니다.

        } catch (FileNotFoundException e) {
            Log.e(TAG,"error:"+e.getMessage());
        } catch (IOException e) {
            Log.e(TAG,"error:"+e.getMessage());
        }

        return tempFile.getAbsolutePath();   // 임시파일 저장경로를 리턴해주면 끝!
    }

//    public Uri getUriFromPath(String path){
//        Uri fileUri = Uri.parse( path );
//        String filePath = fileUri.getPath();
//        Cursor cursor = getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + filePath + "'", null, null );
//        cursor.moveToNext();
//        int id = cursor.getInt( cursor.getColumnIndex( "_id" ) );
//        Uri uri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id );
//        return uri;
//    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
