package com.dial070.ui;

import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dial070.maaltalk.R;

public class ImageViewerAdapter extends PagerAdapter {

	LayoutInflater inflater;
	private ImageView mImg;

	public ImageViewerAdapter(LayoutInflater inflater) {
		// TODO Auto-generated constructor stub
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ImageViewer.getCount();
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TODO Auto-generated method stub
		View view = null;

		// 새로운 View 객체를 Layoutinflater를 이용해서 생성
		// 만들어질 View의 설계는 res폴더>>layout폴더>>viewpater_childview.xml 레이아웃 파일 사용
		view = inflater.inflate(R.layout.view_pager, null);
		mImg = (ImageView) view.findViewById(R.id.img_viewpager_childimage);

		mImg.setImageBitmap(ImageViewer.getBit(position));
		//ImageViewer.setLayout();
		mImg.setOnClickListener(mPagerListener);

		// ViewPager에 만들어 낸 View 추가
		container.addView(view);

		// Image가 세팅된 View를 리턴
		return view;
	}

	// 화면에 보이지 않은 View는파쾨를 해서 메모리를 관리함.
	// 첫번째 파라미터 : ViewPager
	// 두번째 파라미터 : 파괴될 View의 인덱스(가장 처음부터 0,1,2,3...)
	// 세번째 파라미터 : 파괴될 객체(더 이상 보이지 않은 View 객체)
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub

		// ViewPager에서 보이지 않는 View는 제거
		// 세번째 파라미터가 View 객체 이지만 데이터 타입이 Object여서 형변환 실시
		container.removeView((View) object);

	}

	@Override
	public boolean isViewFromObject(View v, Object obj) {
		// TODO Auto-generated method stub
		return v == obj;
	}
	
	private View.OnClickListener mPagerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        	ImageViewer.setVisible();
        }
    };
    
    @Override public void startUpdate(View arg0) {}
    @Override public void finishUpdate(View arg0) {}
    
    @Override 
    public void restoreState(Parcelable arg0, ClassLoader arg1) {}
    
    @Override 
    public Parcelable saveState() { 
    	return null; 
    }

}
