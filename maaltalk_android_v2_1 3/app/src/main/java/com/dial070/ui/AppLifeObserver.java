package com.dial070.ui;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.dial070.global.ServicePrefs;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.service.SipService;
import com.dial070.status.CallStatus;
import com.dial070.utils.DebugLog;
import com.dial070.utils.ServiceUtils;

public class AppLifeObserver implements LifecycleObserver {

    private final Context context;

    public AppLifeObserver(@NonNull Context context) {
        this.context = context;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onForeground() {
        DebugLog.d("AppLife onForeground() must start SipService called.");
        if(!ServiceUtils.isRunningService(context, SipService.class)) {
            DebugLog.d("AppLife onForeground() SipService is not running...");
            try {
                if(ServicePrefs.mUserID!=null && ServicePrefs.mUserID.length()>0) {
                    DebugLog.d("AppLife onForeground() userId is exist...");
                    context.startService(new Intent(context, SipService.class));
                } else {
                    DebugLog.d("AppLife onForeground() userId is not exist...");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            DebugLog.d("AppLife onForeground() SipService is running...");
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onBackground() {
        DebugLog.d("AppLife onBackground() must stop SipService called.");
        if(ServiceUtils.isRunningService(context, SipService.class)) {
            DebugLog.d("AppLife onBackground() SipService is running...");
            DebugLog.d("AppLife onBackground() current callStatus : "+ CallStatus.INSTANCE.getCurrentStatus());

            if(CallStatus.INSTANCE.getCurrentStatus()!= SipCallSession.InvState.EARLY &&
                    CallStatus.INSTANCE.getCurrentStatus()!=SipCallSession.InvState.INCOMING &&
                    CallStatus.INSTANCE.getCurrentStatus()!=SipCallSession.InvState.CONNECTING &&
                    CallStatus.INSTANCE.getCurrentStatus()!=SipCallSession.InvState.CONFIRMED) {
                // 연결중이 아닌 경우
                DebugLog.d("AppLife onBackground() callStatus is clear.");

                try {
                    context.stopService(new Intent(context, SipService.class));
                    SipService.RTT = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                DebugLog.d("AppLife onBackground() callStatus is not clear.");
            }
        } else {
            DebugLog.d("AppLife onBackground() SipService is not running...");
            try {
                context.stopService(new Intent(context, SipService.class));
                SipService.RTT = 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
