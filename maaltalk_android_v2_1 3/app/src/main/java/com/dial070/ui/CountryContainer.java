package com.dial070.ui;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.HangulUtils;
import com.dial070.utils.EnglishUtils;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;

import android.content.Context;

public class CountryContainer {

	private static final String THIS_FILE="Country Container";
	
//	private Context mContext;	
	
	private static String mSerial;
	
    private static final String mCanadaCodeList[] = {"+1902","+1705","+1867","+1506","+1709","+1519","+1204","+1514","+1604","+1250","+1306","+1807","+1819",
		 "+1604","+1780","+1613","+1403","+1418","+1613","+1416","+1905"};    
	private static final String CANADA_ENG_NAME = "Canada";
	
	
    public static boolean mLoadCountry = false;   
    public static boolean mLoadAllCountry = false;   
    public static boolean mLoadFullCountry = false;
    public static String mUrlCountry = null;   
    public static String mCountrySerial = null;

    
	private static Map<String, ArrayList<CountryItem>> mCountryItemList;
	private static Map<String, ArrayList<CountryItem>> mCountryAllItemList;
	private static Map<String, ArrayList<CountryItem>> mCountryFullItemList;
	private static Map<String,String> mFreeCallCountry; 
	
	public static boolean isCompleteLoad()
	{
		if(!(mLoadCountry && mLoadAllCountry)) return false;
		
		return true;
	}
	
	public static void clearCountryList()
	{
		if(mCountryItemList != null && mCountryItemList.size() > 0)
		{
			mCountryItemList.clear();
			mLoadCountry = false;
		}
		if(mCountryAllItemList != null && mCountryAllItemList.size() > 0)
		{
			mCountryAllItemList.clear();
			mLoadAllCountry = false;
		}		
		if(mCountryFullItemList != null && mCountryFullItemList.size() > 0)
		{
			mCountryFullItemList.clear();
			mLoadFullCountry = false;
		}			
	}

	//Freecall country
	public static boolean isFreecallCountry(String aCode)
	{
		if(aCode.startsWith("+")) aCode = aCode.substring(1,aCode.length());

		if(mFreeCallCountry.containsKey(aCode)) return true;
		else return false;
	}	
	
	public static void setCountryUrl(String aUrl)
	{
		CountryContainer.mUrlCountry = aUrl;
	}
	

	
	//Logest Match
	public static CountryItem getCountryItem(String aNumber, String aLocale)
	{
		CountryItem item = null;
		CountryItem match_item = null;
				
		if(!mLoadAllCountry) return item;
		
		int max_len = 0;
		
		if(!aNumber.startsWith("+")) aNumber = "+" + aNumber;
		
		boolean isCanadaCode = false;
		
		//미국인지 캐나다인지 모름
		for(int i=0;i<mCanadaCodeList.length;i++)
		{
			String canadaCode = mCanadaCodeList[i];
			if(aNumber.startsWith(canadaCode))
			{
				isCanadaCode = true;
				break;
			}
		}
		
		
		ArrayList<CountryItem> data = null;
		
		//무료국가에서 먼저 검색 
		for(int i=0; i < mCountryItemList.size(); i++)
		{
			
			if(aLocale.equalsIgnoreCase("한국어"))
			{
				if(mCountryItemList.size() > HangulUtils.INITIAL_HANGUL_SECTION.length + 1) return null;
				
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);			
			}
			else
			{
				if(mCountryItemList.size() > EnglishUtils.INITIAL_ENGLISH_SECTION.length + 1) return null;

				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);						
			}
			
			if( data !=null && data.size() > 0)
			{
				for(int j=0; j < data.size() ; j++)
				{
					item = data.get(j);
					
					if(aNumber.startsWith(item.Code))
					{
						if(max_len < item.Code.length())
						{
							//캐나인지 미국인지 판단. 
							if(item.Code.equalsIgnoreCase("+1"))
							{
								if(isCanadaCode)
								{
									if(item.NameEng.equalsIgnoreCase(CANADA_ENG_NAME))
									{
										max_len = item.Code.length();
										match_item = item;										
									}
									else
									{
										//미국의 경우 Skip
										continue;
									}
								}
								else
								{
									if(item.NameEng.equalsIgnoreCase(CANADA_ENG_NAME))
									{
										//캐나다의 경우 Skip
										continue;
								
									}
									else
									{
										max_len = item.Code.length();
										match_item = item;				
									}									
								}
							}							
							else
							{
								max_len = item.Code.length();
								match_item = item;
							}
						}
					}
				}
			}
		}				
		
		
		if (match_item == null )
		{
			//무료국가에 없으면 유료국가에서 검색 
			for(int i=0; i < mCountryAllItemList.size(); i++)
			{
				
				if(aLocale.equalsIgnoreCase("한국어"))
				{
					if(i == mCountryAllItemList.size()-1)
						data = mCountryAllItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
					else	
						data = mCountryAllItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);			
				}
				else
				{
					if(i == mCountryAllItemList.size()-1)
						data = mCountryAllItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
					else	
						data = mCountryAllItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);						
				}
				
				if( data !=null && data.size() > 0)
				{
					for(int j=0; j < data.size() ; j++)
					{
						item = data.get(j);
						
						if(aNumber.startsWith(item.Code))
						{
							if(max_len < item.Code.length())
							{
								//캐나인지 미국인지 판단. 
								if(item.Code.equalsIgnoreCase("+1"))
								{
									if(isCanadaCode)
									{
										if(item.NameEng.equalsIgnoreCase(CANADA_ENG_NAME))
										{
											max_len = item.Code.length();
											match_item = item;										
										}
										else
										{
											//미국의 경우 Skip
											continue;
										}
									}
									else
									{
										if(item.NameEng.equalsIgnoreCase(CANADA_ENG_NAME))
										{
											//캐나다의 경우 Skip
											continue;
									
										}
										else
										{
											max_len = item.Code.length();
											match_item = item;				
										}									
									}
								}							
								else
								{
									max_len = item.Code.length();
									match_item = item;
								}
							}
						}
					}
				}
			}
		}

		
				
		return match_item;
	}
	
	public static String getSerial()
	{
		return mSerial;
	}
	
	public static void setSerial(String aSerial)
	{
		CountryContainer.mSerial = aSerial;
	}
	
    private final static Comparator<CountryItem> sDisplayNameComparator =
        new Comparator<CountryItem>() {
        private final Collator   collator = Collator.getInstance();

        public int compare(CountryItem map1, CountryItem map2) {
            return collator.compare(map1.getName(), map2.getName());
        }
    };	
	
	//전체국가 
	public static Map<String, ArrayList<CountryItem>> getAllCountry(Context aContext, String aLocale, boolean save_list)
	{
		if(!mLoadAllCountry) 
		{
			if(!loadCountry(aContext,aLocale,save_list)) return null;
		}
		
		if(mLoadAllCountry) return mCountryAllItemList;
		
		mCountryAllItemList = new HashMap<String, ArrayList<CountryItem>>();
		
		if(aLocale.equalsIgnoreCase("한국어"))
		{
			for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
			{
				mCountryAllItemList.put(HangulUtils.INITIAL_HANGUL_SECTION[i],null);
			}		
			mCountryAllItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,null);	
		}
		else
		{
			for(int i=0; i < EnglishUtils.INITIAL_ENGLISH_SECTION.length ; i++)
			{
				mCountryAllItemList.put(EnglishUtils.INITIAL_ENGLISH_SECTION[i],null);
			}		
			mCountryAllItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,null);				
		}
		
        InputStream ins = aContext.getResources().openRawResource(R.raw.country_all);
        try
        {
	        int size = ins.available();
	
		    // Read the entire resource into a local byte buffer.
		    byte[] buffer = new byte[size];
		    while (ins.read(buffer) != -1);
		    ins.close();

		    String jsontext = new String(buffer);		     
	     
			JSONObject jObject;
			try
			{
			    jObject = new JSONObject(jsontext);
	            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
	            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
	            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
	            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
	            
	            if (responseObject.getInt("RES_CODE") == 0)
	            {
	                JSONObject countryObject = jObject.getJSONObject("COUNTRY_INFO");
	                
	        		boolean isExtraSection = false;
	            	String sectionStr = null;
	                
	                Log.d("COUNTRY", "SERIAL="+ countryObject.getString("SERIAL"));
	                JSONArray countryItemArray = countryObject.getJSONArray("ITEMS");
	                for (int i = 0; i < countryItemArray.length() ; i++) {
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());
	                	
	                	
						ArrayList<CountryItem> country = null;
	                	
	            		if(aLocale.equalsIgnoreCase("한국어"))
	            		{
	            			//section을 찾는다.
							String firstChosung = HangulUtils.getHangulChosung(countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
							
							if(firstChosung.length() == 0)
							{
								isExtraSection = true;
								sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
//								char c = firstChosung.charAt(0);
								
								for (int j = 0; j < HangulUtils.INITIAL_HANGUL_SECTION.length; j++)
								{
									if (firstChosung.equalsIgnoreCase(HangulUtils.INITIAL_HANGUL_SECTION[j])) {
										isExtraSection = false;
										sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 

							
							if(isExtraSection)
							{
								country = mCountryAllItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryAllItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryAllItemList.containsKey(sectionStr))
								{
									country = mCountryAllItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryAllItemList.put(sectionStr,country);
									}
								}
							}
	            		}
	            		else
	            		{
	            			//section을 찾는다.
							String firstChar = EnglishUtils.getEnglishChar(countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
							
							if(firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_EXTRA_SECTION))
							{
								isExtraSection = true;
								sectionStr = EnglishUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
								for (int j = 0; j < EnglishUtils.INITIAL_ENGLISH_SECTION.length; j++)
								{
									if (firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_ENGLISH_SECTION[j])) {
										isExtraSection = false;
										sectionStr = EnglishUtils.INITIAL_ENGLISH_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 
							if(isExtraSection)
							{
								country = mCountryAllItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryAllItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryAllItemList.containsKey(sectionStr))
								{
									country = mCountryAllItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryAllItemList.put(sectionStr,country);
									}
								}
							}	            			
	            		}
						
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());

						if(country != null)
						{
							if(!mFreeCallCountry.containsKey(countryItemArray.getJSONObject(i).getString("CODE").toString()))
							{
								country.add(new CountryItem(
										countryItemArray.getJSONObject(i).getString("NAME_KOR").toString(),
										countryItemArray.getJSONObject(i).getString("NAME_ENG").toString(),
										"+" + countryItemArray.getJSONObject(i).getString("CODE").toString(),
										countryItemArray.getJSONObject(i).getString("GMT").toString(),
										countryItemArray.getJSONObject(i).getString("FLAG").toString(),
										0,/*Integer.parseInt(countryItemArray.getJSONObject(i).getString("FREE_MOBILE").toString())*/
										0,/*Integer.parseInt(countryItemArray.getJSONObject(i).getString("FREE_WIRED").toString())*/
										aLocale
										));
							}
						}	
	                	
	                }		           	
	            }
	            else
	            {
	            	Log.d("COUNTRY", "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
	            	return null;
	            }
			}
			catch (Exception e)
			{
				Log.e("COUNTRY", "JSON ERROR=" + e.getMessage());
				return null;
			}		
		     
        } catch (IOException e)
        {
        	Log.d("COUNTRY", "ERROR ="+ e.getMessage());	
        	return null;
        }		
        
		 for(Object key : mCountryAllItemList.keySet())
		 {
			 ArrayList<CountryItem> object = mCountryAllItemList.get(key);
			 if(object != null && object.size() > 1)
			 {
				 try
				 {
				 	Collections.sort(object, sDisplayNameComparator);
				 }
				 catch (Exception e)
				 {
					 //
				 }
			 }
		 }
        
		
        mLoadAllCountry = true; 
		return mCountryAllItemList;
	}
	
	
	//전체국가 
	public static Map<String, ArrayList<CountryItem>> getFullCountry(Context aContext, String aLocale, boolean save_list)
	{
		if(!mLoadFullCountry) 
		{
			if(!loadCountry(aContext,aLocale,save_list)) return null;
		}
		
		if(mLoadFullCountry) return mCountryFullItemList;
		
		mCountryFullItemList = new HashMap<String, ArrayList<CountryItem>>();
		
		if(aLocale.equalsIgnoreCase("한국어"))
		{
			for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
			{
				mCountryFullItemList.put(HangulUtils.INITIAL_HANGUL_SECTION[i],null);
			}		
			mCountryFullItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,null);	
		}
		else
		{
			for(int i=0; i < EnglishUtils.INITIAL_ENGLISH_SECTION.length ; i++)
			{
				mCountryFullItemList.put(EnglishUtils.INITIAL_ENGLISH_SECTION[i],null);
			}		
			mCountryFullItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,null);				
		}
		
        InputStream ins = aContext.getResources().openRawResource(R.raw.country_all);
        try
        {
	        int size = ins.available();
	
		    // Read the entire resource into a local byte buffer.
		    byte[] buffer = new byte[size];
		    while (ins.read(buffer) != -1);
		    ins.close();

		    String jsontext = new String(buffer);		     
	     
			JSONObject jObject;
			try
			{
			    jObject = new JSONObject(jsontext);
	            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
	            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
	            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
	            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
	            
	            if (responseObject.getInt("RES_CODE") == 0)
	            {
	                JSONObject countryObject = jObject.getJSONObject("COUNTRY_INFO");
	                
	        		boolean isExtraSection = false;
	            	String sectionStr = null;
	                
	                Log.d("COUNTRY", "SERIAL="+ countryObject.getString("SERIAL"));
	                JSONArray countryItemArray = countryObject.getJSONArray("ITEMS");
	                for (int i = 0; i < countryItemArray.length() ; i++) {
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());
	                	
	                	
						ArrayList<CountryItem> country = null;
	                	
	            		if(aLocale.equalsIgnoreCase("한국어"))
	            		{
	            			//section을 찾는다.
							String firstChosung = HangulUtils.getHangulChosung(countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
							
							if(firstChosung.length() == 0)
							{
								isExtraSection = true;
								sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
//								char c = firstChosung.charAt(0);
								
								for (int j = 0; j < HangulUtils.INITIAL_HANGUL_SECTION.length; j++)
								{
									if (firstChosung.equalsIgnoreCase(HangulUtils.INITIAL_HANGUL_SECTION[j])) {
										isExtraSection = false;
										sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 

							
							if(isExtraSection)
							{
								country = mCountryFullItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryFullItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryFullItemList.containsKey(sectionStr))
								{
									country = mCountryFullItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryFullItemList.put(sectionStr,country);
									}
								}
							}
	            		}
	            		else
	            		{
	            			//section을 찾는다.
							String firstChar = EnglishUtils.getEnglishChar(countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
							
							if(firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_EXTRA_SECTION))
							{
								isExtraSection = true;
								sectionStr = EnglishUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
								for (int j = 0; j < EnglishUtils.INITIAL_ENGLISH_SECTION.length; j++)
								{
									if (firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_ENGLISH_SECTION[j])) {
										isExtraSection = false;
										sectionStr = EnglishUtils.INITIAL_ENGLISH_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 
							if(isExtraSection)
							{
								country = mCountryFullItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryFullItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryFullItemList.containsKey(sectionStr))
								{
									country = mCountryFullItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryFullItemList.put(sectionStr,country);
									}
								}
							}	            			
	            		}
						
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());

						if(country != null)
						{
							country.add(new CountryItem(
									countryItemArray.getJSONObject(i).getString("NAME_KOR").toString(),
									countryItemArray.getJSONObject(i).getString("NAME_ENG").toString(),
									"+" + countryItemArray.getJSONObject(i).getString("CODE").toString(),
									countryItemArray.getJSONObject(i).getString("GMT").toString(),
									countryItemArray.getJSONObject(i).getString("FLAG").toString(),
									0,/*Integer.parseInt(countryItemArray.getJSONObject(i).getString("FREE_MOBILE").toString())*/
									0,/*Integer.parseInt(countryItemArray.getJSONObject(i).getString("FREE_WIRED").toString())*/
									aLocale
									));
						}	
	                	
	                }		           	
	            }
	            else
	            {
	            	Log.d("COUNTRY", "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
	            	return null;
	            }
			}
			catch (Exception e)
			{
				Log.e("COUNTRY", "JSON ERROR=" + e.getMessage());
				return null;
			}		
		     
        } catch (IOException e)
        {
        	Log.d("COUNTRY", "ERROR ="+ e.getMessage());	
        	return null;
        }		
        
		 for(Object key : mCountryFullItemList.keySet())
		 {
			 ArrayList<CountryItem> object = mCountryFullItemList.get(key);
			 if(object != null && object.size() > 1)
			 {
				 try
				 {
				 	Collections.sort(object, sDisplayNameComparator);
				 }
				 catch (Exception e)
				 {
					 //
				 }				 
			 }
		 }
        
		
        mLoadFullCountry = true; 
		return mCountryFullItemList;
	}	

	//무료국가 
	public static Map<String, ArrayList<CountryItem>> getCountry(Context aContext, String aLocale)
	{
		if(mLoadCountry) return mCountryItemList;
		if (loadCountry(aContext,aLocale, false)) return mCountryItemList; 
		else return null;
	}	
	
	
	private static String postCountrylInfo(Context aContext)
	{
		if(mUrlCountry == null) return null;
		
		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(aContext);
			
			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 20000);
			HttpConnectionParams.setSoTimeout(params, 20000);
			
			HttpPost httppost = new HttpPost(mUrlCountry);
			
			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String line = null;
			StringBuilder sb = new StringBuilder();			
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}
	
	//무료국가 
	private final static boolean loadCountry(Context aContext, String locale, boolean save_list)
	{
		if(mLoadCountry) return true;
		
		mCountrySerial = null;
		
		if(mCountryItemList == null)
		{
			mCountryItemList = new HashMap<String, ArrayList<CountryItem>>();
		}
		if(mFreeCallCountry == null)
		{
			mFreeCallCountry = new HashMap<String,String>();
			mFreeCallCountry.clear();
		}
		
		if(locale.equalsIgnoreCase("한국어"))
		{
			for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
			{
				mCountryItemList.put(HangulUtils.INITIAL_HANGUL_SECTION[i],null);
			}		
			mCountryItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,null);	
		}
		else
		{
			for(int i=0; i < EnglishUtils.INITIAL_ENGLISH_SECTION.length ; i++)
			{
				mCountryItemList.put(EnglishUtils.INITIAL_ENGLISH_SECTION[i],null);
			}		
			mCountryItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,null);				
		}		
		
		String rs = null;
		
		// 1. 업데이트가 필요하면 다운로드 받아서 저장.
		if(save_list)
		{
			Log.d(THIS_FILE, "REQUEST COUNTRY INFO : START");
			rs = postCountrylInfo(aContext);
			if(rs != null && rs.length() > 0)
			{
				try
				{
					FileOutputStream fos = aContext.getApplicationContext().openFileOutput("country_info.json",Context.MODE_PRIVATE);
					fos.write(rs.getBytes());
					fos.close();
				} catch (Exception e) {;}
			}
			Log.d(THIS_FILE, "REQUEST COUNTRY INFO : END");
			Log.d(THIS_FILE, "RESULT : " + rs);
		}
		
		// 2. 저장된 파일을 읽기 시도함.
		if (rs == null || rs.length() == 0)
		{
			//return 0;
        	try
        	{
        		FileInputStream fis = aContext.getApplicationContext().openFileInput("country_info.json");
        		byte[] data = new byte[fis.available()];
        		while(fis.read(data) != -1);
        		fis.close();
        		
        		rs = new String(data);
        		
        	} catch(Exception e) {e.printStackTrace();}
		}

		// 3. 아무것도 읽지 못했다면 할수 없이 기존에 내장된 무료국가 정보를 사용함.
		if (rs == null || rs.length() == 0)
		{
			InputStream ins = aContext.getResources().openRawResource(R.raw.country_all);		
	        try
	        {
		        int size = ins.available();
		
			    // Read the entire resource into a local byte buffer.
			    byte[] buffer = new byte[size];
			    while (ins.read(buffer) != -1);
			    ins.close();
	
			    rs = new String(buffer);		     
	        }			
			catch (Exception e)
			{
				Log.e(THIS_FILE, "JSON", e);
				return false;
			}			
		}
			

//		JSONObject jObject;
//		try
//		{	
//		    jObject = new JSONObject(rs);
//            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
//            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
//            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
//            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
//            
//            if (responseObject.getInt("RES_CODE") == 0)
//            {
//            
//            }
//			
//			
//		}
//		catch (Exception e)
//		{
//			Log.e(THIS_FILE, "JSON", e);
//			return false;
//		}		
//		
		
		
//        InputStream ins = aContext.getResources().openRawResource(R.raw.country);
//        try
//        {
//	        int size = ins.available();
//	
//		    // Read the entire resource into a local byte buffer.
//		    byte[] buffer = new byte[size];
//		    while (ins.read(buffer) != -1);
//		    ins.close();
//
//		    String jsontext = new String(buffer);		     
//	     
			JSONObject jObject;
			try
			{
			    jObject = new JSONObject(rs);
	            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
	            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
	            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
	            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
	            
	            if (responseObject.getInt("RES_CODE") == 0)
	            {
	                JSONObject countryObject = jObject.getJSONObject("COUNTRY_INFO");
	                
	        		boolean isExtraSection = false;
	            	String sectionStr = null;
	                
	                Log.d("COUNTRY", "SERIAL="+ countryObject.getString("SERIAL"));
	                
	                // SET CURRENT SERIAL
	                mCountrySerial = countryObject.getString("SERIAL");
	                
	                JSONArray countryItemArray = countryObject.getJSONArray("ITEMS");
	                for (int i = 0; i < countryItemArray.length() ; i++) {
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "GMT="+ countryItemArray.getJSONObject(i).getString("GMT").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());
	                	
						ArrayList<CountryItem> country = null;
						
	            		if(locale.equalsIgnoreCase("한국어"))
	            		{
							//section을 찾는다.
							String firstChosung = HangulUtils.getHangulChosung(countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
							
							if(firstChosung.length() == 0)
							{
								isExtraSection = true;
								sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
//								char c = firstChosung.charAt(0);
								
								for (int j = 0; j < HangulUtils.INITIAL_HANGUL_SECTION.length; j++)
								{
									if (firstChosung.equalsIgnoreCase(HangulUtils.INITIAL_HANGUL_SECTION[j])) {
										isExtraSection = false;
										sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 

							
							if(isExtraSection)
							{
								country = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryItemList.containsKey(sectionStr))
								{
									country = mCountryItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryItemList.put(sectionStr,country);
									}
								}
							}
	            		}
	            		else
	            		{
	            			
	            			//section을 찾는다.
							String firstChar = EnglishUtils.getEnglishChar(countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
							
							if(firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_EXTRA_SECTION))
							{
								isExtraSection = true;
								sectionStr = EnglishUtils.INITIAL_EXTRA_SECTION;
							}
							else
							{
								for (int j = 0; j < EnglishUtils.INITIAL_ENGLISH_SECTION.length; j++)
								{
									if (firstChar.equalsIgnoreCase(EnglishUtils.INITIAL_ENGLISH_SECTION[j])) {
										isExtraSection = false;
										sectionStr = EnglishUtils.INITIAL_ENGLISH_SECTION[j];
										break;
									}
								}							
							}	
							
							
							//Country에 추가 
							if(isExtraSection)
							{
								country = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
								
								if( country == null)
								{
									country = new ArrayList<CountryItem>();
									
									mCountryItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION,country);
								}							
							}
							else
							{
								if(mCountryItemList.containsKey(sectionStr))
								{
									country = mCountryItemList.get(sectionStr);
									
									if( country == null)
									{
										country = new ArrayList<CountryItem>();
										
										mCountryItemList.put(sectionStr,country);
									}
								}
							}	 	            			
	            			
	            		}

						
	                	Log.d("COUNTRY", "NAME_KOR="+ countryItemArray.getJSONObject(i).getString("NAME_KOR").toString());
	                	Log.d("COUNTRY", "NAME_ENG="+ countryItemArray.getJSONObject(i).getString("NAME_ENG").toString());
	                	Log.d("COUNTRY", "CODE="+ countryItemArray.getJSONObject(i).getString("CODE").toString());
	                	Log.d("COUNTRY", "FLAG="+ countryItemArray.getJSONObject(i).getString("FLAG").toString());

						if(country != null)
						{
							country.add(new CountryItem(
									countryItemArray.getJSONObject(i).getString("NAME_KOR").toString(),
									countryItemArray.getJSONObject(i).getString("NAME_ENG").toString(),
									"+" + countryItemArray.getJSONObject(i).getString("CODE").toString(),
									countryItemArray.getJSONObject(i).getString("GMT").toString(),
									countryItemArray.getJSONObject(i).getString("FLAG").toString(),
									countryItemArray.getJSONObject(i).getInt("FREE_MOBILE"),
									countryItemArray.getJSONObject(i).getInt("FREE_WIRED"),
									locale
									));
							if(! mFreeCallCountry.containsKey(countryItemArray.getJSONObject(i).getString("CODE").toString()))
							{
								mFreeCallCountry.put(countryItemArray.getJSONObject(i).getString("CODE").toString(), countryItemArray.getJSONObject(i).getString("FLAG").toString());
							}
						}	
	                	
	                }		           	
	            }
	            else
	            {
	            	Log.d("COUNTRY", "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
	            	return false;
	            }
			}
			catch (Exception e)
			{
				Log.e("COUNTRY", "JSON ERROR=" + e.getMessage());
				return false;
			}		
		     
//        } catch (IOException e)
//        {
//        	Log.d("COUNTRY", "ERROR ="+ e.getMessage());	
//        	return false;
//        }		
        
		 for(Object key : mCountryItemList.keySet())
		 {
			 ArrayList<CountryItem> object = mCountryItemList.get(key);
			 if(object != null && object.size() > 1)
			 {
				 try
				 {
				 	Collections.sort(object, sDisplayNameComparator);
				 }
				 catch (Exception e)
				 {
					 //
				 }
			 }
		 }
        
		
        mLoadCountry = true; 
		return true;
	}		
	
}
