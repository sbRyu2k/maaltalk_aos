package com.dial070.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.telephony.SmsMessage;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.MimeTypeMap;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomButtonsController;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.dial070.service.Fcm;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

import static androidx.core.content.FileProvider.*;
//import android.content.Context;
//import android.graphics.Color;
//import java.lang.reflect.InvocationTargetException;

@SuppressLint("SetJavaScriptEnabled")
public class WebClient extends Activity {
    private static final String THIS_FILE = "WEB CLIENT";
    //BJH File Uplaod
    private final static int FILECHOOSER_RESULTCODE = 1;
    private static final int REQUEST_CODE_PERMISSIONS = 1;
    public static WebClient currentContext = null;
    public static boolean bMaaltalkRunned = false;
    public static int currentState = 0; // 0:pause, 1:resume
    final Activity activity = this;
    private String mTitle;
    private TextView mWebTitle;
    private boolean mPopup = false;
    // private ImageButton mBtnNaviBack,mBtnNaviPrev;
    private ImageButton mBtnNaviClose, mBtnNaviReload;
    // private Context mContext;
    private WebView mWeb;
    private AppPrefs mPrefs;
    private ValueCallback<Uri> mUploadMessage;
    private ValueCallback<Uri[]> mUploadMessages; // BJH 2017.03.07 파일 업로드
    private Context mContext;
    private String mWebUrl;
    private String category_id;
    private ImageDownloadTask imageDownloadTask;
    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private String savedFilePath;
    private long mDownloadedFileID;

    private BroadcastReceiver mSMSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                String sb = "";
                Bundle bundle = intent.getExtras();

                if (bundle != null) {
                    Object[] pdusObj = (Object[]) bundle.get("pdus");

                    SmsMessage[] messages = new SmsMessage[pdusObj.length];
                    for (int i = 0; i < pdusObj.length; i++) {
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    }

                    for (SmsMessage currentMessage : messages) {
                        String msg = currentMessage.getDisplayMessageBody();
                        if (msg.contains("Your Maaltalk verification code is")) {
                            String adjustMessage = msg.replace("[Web발신]", "");
                            int begin = adjustMessage.indexOf('<');
                            int end = adjustMessage.indexOf('>');
                            String codeMessage = adjustMessage.substring(begin + 1, end);
                            Log.d(THIS_FILE, "CERT NUMBER : " + codeMessage);
                            mWeb.loadUrl("javascript:setCertNum('" + codeMessage + "')");
                        }
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        Log.d("WebClient", "onCreate() called.. in WebClient");

        currentContext = this;
        bMaaltalkRunned = true;
        mContext = this;//BJH File Uplaod
        mPrefs = new AppPrefs(this);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.web_client_activity);

        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        //DownLoad MANAGER
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        mWebTitle = (TextView) findViewById(R.id.web_title);

        // mBtnNaviBack = (ImageButton) findViewById(R.id.btn_web_navi_back);
        // mBtnNaviPrev = (ImageButton) findViewById(R.id.btn_web_navi_prev);
        mBtnNaviClose = (ImageButton) findViewById(R.id.btn_web_navi_close);
        mBtnNaviReload = (ImageButton) findViewById(R.id.btn_web_navi_reload);

        mWeb = (WebView) findViewById(R.id.web_view);
        mWeb.clearHistory();
        mWeb.clearCache(true);

        WebSettings set = mWeb.getSettings();
        set.setSavePassword(false);
        set.setJavaScriptEnabled(true);
        set.setDomStorageEnabled(true);
        set.setAllowFileAccess(false);
        set.setSupportZoom(true);
        set.setBuiltInZoomControls(true);
        set.setAppCacheEnabled(true);
        set.setJavaScriptCanOpenWindowsAutomatically(true);
        set.setLoadWithOverviewMode(true);
        set.setUseWideViewPort(true);

        //mWeb.clearCache(false); //BJH 2016.07.15

        // BJH WebView 속도 향상
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        set.setRenderPriority(WebSettings.RenderPriority.HIGH);
        set.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        if (Build.VERSION.SDK_INT >= 19) {
            mWeb.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mWeb.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        mWeb.setVerticalScrollbarOverlay(true);
        // mWeb.setBackgroundColor(Color.TRANSPARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // BJH 2017.02.22 안드로이드(Android 5.0) Lollipop Webview issue
            set.setMixedContentMode(WebSettings
                    .MIXED_CONTENT_ALWAYS_ALLOW);

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mWeb, true);
        }

        try {
            set.setEnableSmoothTransition(true);
            set.setAllowContentAccess(false);
            set.setPluginState(WebSettings.PluginState.ON);
        } catch (NullPointerException localNullPointerException) {

        }

        if (Build.VERSION.SDK_INT >= 11) {
            new Runnable() {
                public void run() {
                    // mWeb.getSettings().setDisplayZoomControls(false);
                    try {
                        Method setDisplayZoomControls = WebSettings.class
                                .getMethod("setDisplayZoomControls",
                                        new Class[]{boolean.class});
                        if (setDisplayZoomControls != null) {
                            setDisplayZoomControls.invoke(mWeb.getSettings(),
                                    false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.run();
        } else {
            ZoomButtonsController zoom_controll;
            try {
                zoom_controll = (ZoomButtonsController) mWeb.getClass()
                        .getMethod("getZoomButtonsController")
                        .invoke(mWeb);
                zoom_controll.getContainer().setVisibility(View.GONE);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        mWeb.setWebChromeClient(new WebChromeClient() {
            //BJH FileChooser
            private Uri imageUri;



            public void onProgressChanged(WebView view, int progress) {
                activity.setTitle(getResources().getString(R.string.loading));
                activity.setProgress(progress * 100);

                if (progress == 100) {
                    activity.setTitle(R.string.app_name);
                    // activity.setTitle(mContext.getResources().getString(R.string.app_name)
                    // + " > " + mTitle);
                }
            }

            protected void openFileChooser(ValueCallback uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                final List<Intent> cameraIntents = new ArrayList<Intent>();
                final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                final PackageManager packageManager = getPackageManager();
                final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
                for (ResolveInfo res : listCam) {
                    final String packageName = res.activityInfo.packageName;
                    final Intent catpure = new Intent(captureIntent);
                    catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    catpure.setPackage(packageName);
                    catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    cameraIntents.add(catpure);

                }
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
            }


            // For Lollipop 5.0 Devices BJH 2017.03.07
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (mUploadMessages != null) {
                    mUploadMessages.onReceiveValue(null);
                    mUploadMessages = null;
                }

                mUploadMessages = filePathCallback;

                final List<Intent> cameraIntents = new ArrayList<Intent>();
                final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                final PackageManager packageManager = getPackageManager();
                final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
                for (ResolveInfo res : listCam) {
                    final String packageName = res.activityInfo.packageName;
                    final Intent catpure = new Intent(captureIntent);
                    catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    catpure.setPackage(packageName);
                    catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    cameraIntents.add(catpure);

                }
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);

                return true;
            }

            //For Android 4.1
            protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                final List<Intent> cameraIntents = new ArrayList<Intent>();
                final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                final PackageManager packageManager = getPackageManager();
                final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
                for (ResolveInfo res : listCam) {
                    final String packageName = res.activityInfo.packageName;
                    final Intent catpure = new Intent(captureIntent);
                    catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    catpure.setPackage(packageName);
                    catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    cameraIntents.add(catpure);

                }
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
            }

            protected void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                final List<Intent> cameraIntents = new ArrayList<Intent>();
                final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                final PackageManager packageManager = getPackageManager();
                final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
                for (ResolveInfo res : listCam) {
                    final String packageName = res.activityInfo.packageName;
                    final Intent catpure = new Intent(captureIntent);
                    catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    catpure.setPackage(packageName);
                    catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    cameraIntents.add(catpure);

                }
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType("image/*");
                Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
                startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
            }
        });
        mWeb.setWebViewClient(new WebViewClient() {
            private boolean mFinish = false; // BJH 2017.03.28 두번 로딩 방지

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {//BJH 2016.07.11 인증서 보안 문제
                final AlertDialog.Builder builder = new AlertDialog.Builder(WebClient.this);
                String message = getResources().getString(R.string.ssl_untrusted);
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = getResources().getString(R.string.ssl_untrusted);
                        break;
                    case SslError.SSL_EXPIRED:
                        message = getResources().getString(R.string.ssl_expired);
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = getResources().getString(R.string.ssl_untrusted);
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = getResources().getString(R.string.ssl_untrusted);
                        break;
                    default:
                        message = getResources().getString(R.string.ssl_untrusted);
                        break;
                }
                builder.setTitle(getResources().getString(R.string.ssl_error));
                builder.setMessage(message);

                builder.setPositiveButton(getResources().getString(R.string.ssl_continue), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });

                final AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // Handle the error
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //BJH 광고 URL에 곰신포토카드를 구입하러 갔을 때 새창으로 띄우기
                if(url.contains("goo.gl/QlV7eJ")) {
                    try {
                        Intent intent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(url));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } catch (Exception e) {
                    }
                } else if(url.contains("mvoip.php")) {//BJH 2016.06.09 관리팀 요청 사항
                    try {
                        Uri u = Uri.parse(url);
                        String name = u.getQueryParameter("name");
                        String number = u.getQueryParameter("number");
                        String gender = u.getQueryParameter("gender");
                        Uri uri = Uri.parse(DialMain.MAALTALK_EMAIL);
                        try{
                            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.auth_apply) + " : " + number);
                            intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.auth_user_apply) + name + "\n" + getResources().getString(R.string.auth_user_number) + number +
                                    "\n" + getResources().getString(R.string.auth_user_gender) + gender +
                                    getResources().getString(R.string.auth_apply_desc));
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                    .parse("https://play.google.com/store/apps/details?id=com.google.android.gm"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }
                        view.loadUrl(url);
                        return false;
                    } catch (Exception e) {
                    }
                }else {
                    view.loadUrl(url);
                    return false;
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                Log.d(THIS_FILE, "[WebClient] onPageFinished - url=[" + url
                        + "]");
// by shlee add cookie
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    CookieSyncManager.getInstance().sync();
                } else {
                    CookieManager.getInstance().flush();
                }
                // end
                super.onPageFinished(view, url);
                if ((url.contains("certcid_ok.php") || url.contains("certcid_inter_ok.php") || url.contains("certcid_inter_call.php")) && !mFinish) {
                    mFinish = true;
                    Uri u = Uri.parse(url);
                    if (u != null) {
                        DebugLog.d("test_non_auto_auth step 1");
                        String cid = u.getQueryParameter("cid");
                        String country_code = u
                                .getQueryParameter("country_code");
                        String distributor_id = u
                                .getQueryParameter("distributor_id");
                        String birth_date = u.getQueryParameter("birth_date");
                        if (birth_date == null || birth_date.length() == 0)
                            birth_date = "";
                        String user_name = u.getQueryParameter("user_name");
                        if (user_name == null || user_name.length() == 0)
                            user_name = "";
                        String gender = u.getQueryParameter("gender");
                        if (gender == null || gender.length() == 0)
                            gender = "";
                        String adm_auth = u.getQueryParameter("adm_auth");
                        if (adm_auth == null || adm_auth.length() == 0)
                            adm_auth = "";

                        Intent intent = new Intent();

                        String auth_type = "";
                        String flag = "";
                        if (url.contains("certcid_ok.php")) {
                            DebugLog.d("test_non_auto_auth step 2-1");
                            flag = AuthActivityV2.KOREA_FLAG;
                            if (!country_code.equals("82"))
                                flag = u.getQueryParameter("flag");
                            auth_type = "local";
                        } else if (url.contains("certcid_inter_ok.php")) {
                            DebugLog.d("test_non_auto_auth step 2-2");
                            flag = u.getQueryParameter("flag");
                            auth_type = "inter";
                        } else if (url.contains("certcid_inter_call.php")) {
                            DebugLog.d("test_non_auto_auth step 2-3");
                            flag = u.getQueryParameter("flag");
                            auth_type = "inter_call";
                            String valid_code = u.getQueryParameter("valid_code");
                            intent.putExtra("valid_code", valid_code);
                        }

                        DebugLog.d("test_non_auto_auth step 3");

                        intent.putExtra("cid", cid);
                        intent.putExtra("distributor_id", distributor_id);
                        intent.putExtra("birth_date", birth_date);
                        intent.putExtra("user_name", user_name);
                        intent.putExtra("gender", gender);
                        intent.putExtra("adm_auth", adm_auth); //BJH 2017.01.04 해외인증 관련
                        if (country_code.equals("ios"))
                            country_code = "82";
                        else if (country_code.startsWith("+"))
                            country_code = country_code.substring(1);
                        intent.putExtra("country_code", country_code);
                        intent.putExtra("flag", flag);
                        intent.putExtra("auth_type", auth_type);
                        setResult(RESULT_OK, intent);
                        finish();
                        return;
                    }
                    setResult(RESULT_CANCELED);
                    finish();
                    return;
                } else if (mPopup && url.contains("closed.php")) {
                    finish();
                    return;
                } else if (url.contains(mWebUrl)) {   //발자취용
                    //http://211.43.13.195:4001/photoshow/fixed/091566871040?category_id=091566871040_US_1576821894847&category_num=1
                    //http://211.43.13.195:4001/photoshow/fixed/091504590280?category_id=091504590280_JP_1576800159620&category_num=1

                    /*new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    (new AsyncTask<Void, Void, String>(){
                                        @Override
                                        protected String doInBackground(Void... params) {
                                            String reuslt=null;
                                            try {
                                                // Create a new HttpClient and Post Header
                                                HttpClient httpclient = new HttpsClient(mContext);

                                                HttpParams params2 = httpclient.getParams();
                                                HttpConnectionParams.setConnectionTimeout(params2, 10000);
                                                HttpConnectionParams.setSoTimeout(params2, 10000);

                                                String userID=mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                                                String url="http://211.43.13.195:4001/photoshow/fixed/"+userID+"?category_id="+category_id+"&category_num=1";
                                                Log.i(THIS_FILE,"url:"+url);
                                                HttpGet httppost = new HttpGet(url);

                                                httppost.setHeader("User-Agent",
                                                        ServicePrefs.getUserAgent());
                                                // Execute HTTP GET Request
                                                Log.d(THIS_FILE, "HTTPS GET EXEC");
                                                HttpResponse response = httpclient.execute(httppost);

                                                BufferedReader br = new BufferedReader(
                                                        new InputStreamReader(response.getEntity()
                                                                .getContent()));
                                                String line = null;
                                                StringBuilder sb = new StringBuilder();
                                                while ((line = br.readLine()) != null) {
                                                    sb.append(line);
                                                }
                                                br.close();

                                                Log.d(THIS_FILE, "FEEDBACK RESULT : " + sb.toString());
                                                reuslt=sb.toString();
                                            } catch (ClientProtocolException e) {
                                                // TODO Auto-generated catch block
                                                Log.e(THIS_FILE, "ClientProtocolException", e);
                                            } catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                Log.e(THIS_FILE, "Exception", e);
                                            }

                                            return reuslt;
                                        }

                                        @Override
                                        protected void onPostExecute(String rs) {
                                            if (rs!=null){
                                                JSONObject jObject;
                                                try {
                                                    jObject = new JSONObject(rs);
                                                    JSONObject responseObject = jObject.optJSONObject("response");
                                                    Log.d(THIS_FILE, responseObject.optString("code"));
                                                    if (responseObject.getInt("code") == 200) {
                                                        JSONObject retObject=responseObject.optJSONObject("ret");
                                                        String fixed_photo_url=retObject.optString("fixed_photo_url");
                                                        Log.i(THIS_FILE,"fixed_photo_url:"+fixed_photo_url);
                                                        if (fixed_photo_url.length()>0){
                                                            String[] path=new String[1];
                                                            path[0]="http://211.43.13.195:4001/"+fixed_photo_url;;
                                                            imageDownloadTask=new ImageDownloadTask();
                                                            imageDownloadTask.execute(path);
                                                        }
                                                    } else {
                                                        JSONObject msgObject=responseObject.optJSONObject("msg");
                                                        String title=msgObject.optString("title");
                                                        Log.i(THIS_FILE,"title:"+title);
                                                    }
                                                } catch (Exception e) {
                                                    Log.e(THIS_FILE, "JSON", e);
                                                    Log.d(THIS_FILE, "JSON ERROR");
                                                }
                                            }

                                            super.onPostExecute(rs);
                                        }
                                    }).execute();
                                }
                            }, 3000);*/


                }
            }

        });

        mWeb.requestFocus(View.FOCUS_DOWN);
        mWeb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });

        mWeb.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                //This is the filter
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;


                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (mWeb.canGoBack()) {
                        mWeb.goBack();
                    } else {
                        onBackPressed();
                    }

                    return true;
                }

                return false;
            }
        });

        mBtnNaviClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                DialMain.skipCallview = false;
                finish();
            }
        });

        mBtnNaviReload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mWeb.clearHistory();
                mWeb.clearCache(true);
                mWeb.reload();
            }
        });

        /*
         * mBtnNaviBack.setOnClickListener(new View.OnClickListener() {
         *
         * @Override public void onClick(View arg0) { // TODO Auto-generated
         * method stub if (mWeb.canGoBack()) { mWeb.goBack(); } else {
         * DialMain.skipCallview = false; finish(); } } });
         *
         * mBtnNaviPrev.setOnClickListener(new View.OnClickListener() {
         *
         * @Override public void onClick(View v) { // TODO Auto-generated method
         * stub if(mWeb.canGoForward()) { mWeb.goForward(); } } });
         */

        // For Web page
        String defaultTitle = getResources().getString(R.string.app_name);
        String defaultUrl = "http://m.dial070.co.kr/";
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        category_id = intent.getStringExtra("category_id");
        mWebUrl = url;
        mTitle = intent.getStringExtra("title");
        // String button = intent.getStringExtra("button");

        mPopup = intent.getBooleanExtra("popup", false);

        int notification_id = intent.getIntExtra("id", 0);
        if (notification_id > 0) {
            NotificationManager notificationManager = (NotificationManager) this
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notification_id);
        }

        if (url == null)
            url = defaultUrl;
        if (mTitle == null)
            mTitle = defaultTitle;

        mWebTitle.setText(mTitle);

		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) // BJH 2017.04.07 OS VER 17 미만에서는 보안 이슈가 있기 때문에
			mWeb.addJavascriptInterface(new JsCertInter(), "certcid");*/    //2019-02-08 해외문자인증 편의성 제공 기능 제거

        Log.d(THIS_FILE, "url" + url);
        mWeb.loadUrl(url);
		/*if(url.contains("SMART_ENC_V2/mt_auth_v2.php")) { //BJH 2016.09.20 본인인증 관련 수정 => 2017.06.22
			String userAgent = mWeb.getSettings().getUserAgentString();
			mWeb.getSettings().setUserAgentString(userAgent+" maaltalk");

			Uri uri=Uri.parse(url);
			Log.d(THIS_FILE,"uri.getAuthority:"+uri.getAuthority());
			Log.d(THIS_FILE,"uri.getPath:"+uri.getPath());
			Log.d(THIS_FILE,"uri.getScheme:"+uri.getScheme());

			String url_post = intent.getStringExtra("url_post");
			String locale = intent.getStringExtra("locale");
			String country_code = intent.getStringExtra("country_code");
			String phone_number = intent.getStringExtra("phone_number");

			String param = null;
			try {
				param = "locale=" + URLEncoder.encode(locale, "UTF-8") + "&country_code=" + URLEncoder.encode(country_code, "UTF-8") + "&phone_number=" + URLEncoder.encode(phone_number, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			mWeb.postUrl(url_post, param.getBytes());
			mBtnNaviClose.setVisibility(View.GONE);
		}else{
			mWeb.loadUrl(url);
		}*/

        mWeb.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(final String url, final String userAgent, final String contentDisposition, final String mime, long contentLength) {
                Log.i(THIS_FILE, "onDownloadStart url:" + url);
                Log.i(THIS_FILE, "onDownloadStart mimetype:" + mime);
                Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_SHORT).show();

                Thread thread = new Thread() {
                    public void run() {
                        if (url.startsWith("http://211.43.13.195")) {
                            String mimetype=mime;
                            Uri downloadUri = Uri.parse(url);
                            if (mimetype == null) {
                                MimeTypeMap mtm = MimeTypeMap.getSingleton();

                                String fileName = downloadUri.getLastPathSegment();
                                int pos = 0;
                                if ((pos = contentDisposition.toLowerCase().lastIndexOf("filename=")) >= 0) {
                                    fileName = contentDisposition.substring(pos + 9);
                                    pos = fileName.lastIndexOf(";");
                                    if (pos > 0) {
                                        fileName = fileName.substring(0, pos - 1);
                                    }
                                }

                                // MIME Type을 확장자를 통해 예측한다.
                                String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
                                mimetype = mtm.getMimeTypeFromExtension(fileExtension);
                            }
                            Log.i(THIS_FILE, "onDownloadStart mimetype:" + mimetype);
                            request = new DownloadManager.Request(downloadUri);
                            request.setMimeType(mimetype);
                            //------------------------COOKIE!!------------------------
                            String cookies = CookieManager.getInstance().getCookie(url);
                            request.addRequestHeader("cookie", cookies);
                            //------------------------COOKIE!!------------------------
                            request.addRequestHeader("User-Agent", userAgent);
                            request.setDescription("Downloading file...");
                            request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimetype));
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                   /* File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    savedFilePath=dir.getPath();*/

                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimetype));
                            mDownloadedFileID =downloadManager.enqueue(request);
                        }
                    };
                };
                thread.start();


            }
        });

    }

    public void finishActivity(String p_strFinishMsg) {
        Log.d(THIS_FILE, "[WebClient] finishActivity : " + p_strFinishMsg);

        if (p_strFinishMsg != null) {
            Intent intent = new Intent();
            intent.putExtra("ActivityResult", p_strFinishMsg);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        DialMain.skipCallview = false;
        currentContext = null;
        bMaaltalkRunned = false;

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        currentState = 0;
        unregisterReceiver(mSMSReceiver); // BJH 2017.04.07
        unregisterReceiver(completeReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        currentState = 1;
        registerReceiver(mSMSReceiver, new IntentFilter(
                "android.provider.Telephony.SMS_RECEIVED")); // BJH 2017.04.07
        super.onResume();

        IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(completeReceiver, completeFilter);

        String url = mPrefs.getPreferenceStringValue(AppPrefs.NOTICE_URL);
        if (url != null && url.length() > 0) {
            Log.d(THIS_FILE, "SHOW NOTICE : " + url);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_URL, "");
            String title = mPrefs
                    .getPreferenceStringValue(AppPrefs.NOTICE_TITLE);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_TITLE, "");

            String fb_url = mPrefs
                    .getPreferenceStringValue(AppPrefs.NOTICE_FB_URL);
            mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_FB_URL, "");

            Fcm.cancelNotice(this);

            mWebTitle.setText(title);
            mWeb.loadUrl(url);

            if (fb_url != null && fb_url.length() > 0)
                Fcm.sendFeedback(this, fb_url);

            // RELOAD
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);

        if (intent == null)
            return;

        int notification_id = intent.getIntExtra("id", 0);
        if (notification_id > 0) {
            NotificationManager notificationManager = (NotificationManager) this
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notification_id);
        }

        String url = intent.getStringExtra("url");
        if (url != null) {
            if (!url.contains(mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL)))//BJH 2016.09.20 본인인증 관련 수정 => 2017.06.22
                mWeb.loadUrl(url);
        }

        String title = intent.getStringExtra("title");
        if (title != null) {
            mWebTitle.setText(title);
        }

    }

    /*//BJH 2017.04.09 문자 인증을 편리하게 하기 위해서
	public class JsCertInter {
		public void certInter() {
			Log.d(THIS_FILE, "certInter");
		}

		@JavascriptInterface
		public void certInter(String code) {
			Log.d(THIS_FILE, "certInter : " + code);
			if (!code.equals("+82")) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					int hasPermissionRECEIVESMS = checkSelfPermission(Manifest.permission.RECEIVE_SMS);
					List<String> permissions = new ArrayList<String>();
					if (hasPermissionRECEIVESMS != PackageManager.PERMISSION_GRANTED) {
						permissions.add(Manifest.permission.RECEIVE_SMS);
					}
					if (!permissions.isEmpty()) {
						requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_PERMISSIONS);
					} else {
						jsLoadUrl(true);
					}
				} else {
					jsLoadUrl(true);
				}
			} else {
				jsLoadUrl(false);
			}
		}
	}*/

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (mWeb != null && mWeb.canGoBack()) {
            mWeb.goBack();
            return;
        }
        super.onBackPressed();
    }

    //BJH File Uplaod
    public void onActivityResult(int requestCode, int resultCode, Intent intent)//파일 업로드 결과
    {
        Log.d("test_v2_file_up", "result_code: "+resultCode);

        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // BJH 2017.03.07
                if (mUploadMessages == null) return;
                mUploadMessages.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
                mUploadMessages = null;
            } else {
                if (mUploadMessage == null) return;
                Uri result = intent == null || resultCode != PayClient.RESULT_OK ? null : intent.getData();
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        } else
            Toast.makeText(mContext, "Failed to Upload Image", Toast.LENGTH_LONG).show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if (checkSelfPermission(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                    jsLoadUrl(true);
                } else {
                    jsLoadUrl(true);
                }
                break;
        }
    }

    private void jsLoadUrl(final boolean result) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (result)
                    mWeb.loadUrl("javascript:setCertInter()");
                else
                    mWeb.loadUrl("javascript:setReload()");
            }
        });
    }

    private class ImageDownloadTask extends AsyncTask<String, Integer, String> {        //이미지 다운로드

        private String fileName;
        String savePath = Environment.getExternalStorageDirectory() + File.separator + "maaltalk_travel/";

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            File dir = new File(savePath);
            //상위 디렉토리가 존재하지 않을 경우 생성
            if (!dir.exists()) {
                dir.mkdirs();
            }

            String url = params[0];
            fileName = url.substring(url.lastIndexOf('/') + 1, url.length());
            String localPath = savePath + "/" + fileName;

            try {
                URL imgUrl = new URL(url);
                //서버와 접속하는 클라이언트 객체 생성
                HttpURLConnection conn = (HttpURLConnection) imgUrl.openConnection();
                int response = conn.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK || response == HttpURLConnection.HTTP_CREATED) {
                    File file = new File(localPath);

                    InputStream is = conn.getInputStream();
                    OutputStream outStream = new FileOutputStream(file);

                    byte[] buf = new byte[1024];
                    int len = 0;

                    while ((len = is.read(buf)) > 0) {
                        outStream.write(buf, 0, len);
                    }

                    outStream.close();
                    is.close();
                    conn.disconnect();

                    if (new File(localPath).length() > 0) {
                        result = "success";
                    }
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String obj) {
            if (obj.length() > 0) {
                //저장한 이미지 열기
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                String localPath = savePath + "/" + fileName;
                File file = new File(localPath);

                //type 지정 (이미지)
                i.setDataAndType(Uri.fromFile(file), "image/*");
                mContext.startActivity(i);
            }


            super.onPostExecute(obj);

        }
    }

    private BroadcastReceiver completeReceiver = new BroadcastReceiver() {//다운로드 완료되었을 때

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "다운로드가 완료되었습니다.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

            final long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            if (downloadId == 0) return;

            final Cursor cursor = downloadManager.query(
                    new DownloadManager.Query().setFilterById(downloadId));

            if (cursor.moveToFirst()) {
                String downloadedTo = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                Log.i(THIS_FILE, "The file has been downloaded to: " + downloadedTo);

                if (downloadedTo.substring(0, 7).matches("file://")) {
                    downloadedTo =  downloadedTo.substring(7);
                }

                File file = new File(downloadedTo);
                Uri uri;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                {
                    String strpa = getApplicationContext().getPackageName();
                    uri = getUriForFile(context,strpa , file);
                } else
                {// API 24 미만 일경우..
                    uri = Uri.fromFile(file);
                    //uri = Uri.parse(downloadedTo);
                }

                Intent intent1 = new Intent();
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.setAction(Intent.ACTION_VIEW);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent1.setDataAndType(uri, "image/png");
                intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.startActivity(intent1);
            }



            /*// Prevents the occasional unintentional call. I needed this.
            if (mDownloadedFileID == -1)
                return;
            Intent fileIntent = new Intent(Intent.ACTION_VIEW);

            // Grabs the Uri for the file that was downloaded.
            Uri mostRecentDownload =
                    downloadManager.getUriForDownloadedFile(mDownloadedFileID);
            // DownloadManager stores the Mime Type. Makes it really easy for us.
            String mimeType =
                    downloadManager.getMimeTypeForDownloadedFile(mDownloadedFileID);
            fileIntent.setDataAndType(mostRecentDownload, mimeType);
            fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                mContext.startActivity(fileIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(mContext, "No handler for this type of file.",
                        Toast.LENGTH_LONG).show();
            }
            // Sets up the prevention of an unintentional call. I found it necessary. Maybe not for others.
            mDownloadedFileID = -1;*/


/*Intent intent1 = new Intent();
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent1.setAction(android.content.Intent.ACTION_VIEW);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            String localUrl = savedFilePath;
            String extension = MimeTypeMap.getFileExtensionFromUrl(localUrl);
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

            File file = new File(localUrl);
            Uri uri;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                String strpa = getApplicationContext().getPackageName();
                uri = getUriForFile(context,strpa + ".fileprovider", file);
            } else
            {// API 24 미만 일경우..
                uri = Uri.fromFile(file);
            }

            intent1.setDataAndType(uri, mimeType);
            try {
                startActivity(intent1);
            } catch (Exception e) {
                Toast.makeText(context, "Not found. Cannot open file.", Toast.LENGTH_SHORT).show();
                Log.e(THIS_FILE, "error:" + e.getLocalizedMessage());
            }*/






        }

    };



}
