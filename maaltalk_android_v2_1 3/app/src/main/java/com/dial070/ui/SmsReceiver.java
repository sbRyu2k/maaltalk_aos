package com.dial070.ui;

import java.util.ArrayList;

import com.dial070.maaltalk.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class SmsReceiver extends AppCompatActivity {

	private TextView leftTitle;
	private TextView rightTitle;
	private Context mContext;
	
	private ListView mListView;
	private EditText mReceiverInput;
	private ImageButton mAddReceiver;
	private ImageButton mDelete, mOk;
	private ReceiverPhoneListAdapter mAdapter;
	ArrayList<String> mPhoneList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		//커스텀 타이틀 바 
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);		
		
		setContentView(R.layout.sms_receiver_activity);		
		
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

		leftTitle = (TextView)findViewById(R.id.left_title);
		rightTitle = (TextView)findViewById(R.id.right_title);
		
		leftTitle.setTextColor(Color.WHITE);
		leftTitle.setTypeface(Typeface.DEFAULT_BOLD);
		
		rightTitle.setTextColor(Color.WHITE);
		rightTitle.setTypeface(Typeface.DEFAULT_BOLD);			
		leftTitle.setText("");
		
		
		Intent intent = getIntent();
		mPhoneList = intent.getStringArrayListExtra("phone_list");
		
		if(mPhoneList != null)
			rightTitle.setText(Integer.toString(mPhoneList.size()) + "");		
		
		mListView = (ListView) findViewById(R.id.sms_receiver_phone_list);	

		mReceiverInput = (EditText) findViewById(R.id.receiver_input);
		
		mAddReceiver = (ImageButton) findViewById(R.id.sms_receiver_add);
		mAddReceiver.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String phone_number = mReceiverInput.getText().toString();
				if(phone_number.length() > 0)
				{
					if(mPhoneList.contains(phone_number)) return;
					
					mPhoneList.add(phone_number);
					mAdapter.loadSendList(mPhoneList);
					mAdapter.notifyDataSetChanged();
					
					rightTitle.setText(Integer.toString(mPhoneList.size()) + "");		
					
					mReceiverInput.setText("");
				}
			}
		});
		
		
		mDelete = (ImageButton) findViewById(R.id.btn_sms_receiver_delete);
		mDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
				msgDialog.setTitle(getResources().getString(R.string.delete_all));
				msgDialog.setMessage(getResources().getString(R.string.delete_question));
				msgDialog.setButton(Dialog.BUTTON_POSITIVE,mContext.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						//모든 수신인 삭제 
						mPhoneList.clear();
						
						rightTitle.setText(Integer.toString(mPhoneList.size()) + "");		
						
						mAdapter.loadSendList(mPhoneList);
						mAdapter.notifyDataSetChanged();	
						return;
					}
				});
				msgDialog.setButton(Dialog.BUTTON_NEGATIVE,mContext.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						return;
					}
				});		
				msgDialog.show();					
			}
		});
		
		mOk = (ImageButton) findViewById(R.id.btn_sms_receiver_ok);
		mOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putStringArrayListExtra("phone_list", mPhoneList);
				setResult(RESULT_OK,intent);	
				finish();
			}
		});
		
		
		mAdapter = new ReceiverPhoneListAdapter(mContext,mPhoneList);
		mListView.setAdapter(mAdapter);

		
	}

	
	
	class ReceiverPhoneListAdapter extends BaseAdapter {
		
		Context	mContext;
		ArrayList<String> mPhoneList;
		
		public ReceiverPhoneListAdapter(Context context, ArrayList<String> phoneList)
		{
			mContext = context;
			mPhoneList = phoneList;
		}

		public void loadSendList(ArrayList<String> phoneList)
		{
			mPhoneList = phoneList;
		}
		
		private String read(int offset)
		{
			if (offset < 0 || offset >= mPhoneList.size()) return null;
			
			if (mPhoneList.get(offset) != null)
				return mPhoneList.get(offset);
			
			return null;
		}
		
		private void deleteSendList(int position)
		{
			mPhoneList.remove(position);
	
			this.notifyDataSetChanged();
			
			rightTitle.setText(Integer.toString(mPhoneList.size()) + "");		
		}
		
		@Override
		public int getCount() {
			return mPhoneList.size();
		}

		@Override
		public Object getItem(int position) {
			return read(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			SmsPhoneListView smsPhoneView;
			final String item = read(position);
			if(convertView == null)
			{
				smsPhoneView = new SmsPhoneListView(mContext, read(position));
			}
			else
			{
				smsPhoneView = (SmsPhoneListView) convertView;
				smsPhoneView.setNumber(item);
			}
			
			final int index = position;
			ImageButton btn_phone_delete = (ImageButton) smsPhoneView.findViewById(R.id.sms_phone_delete);
			btn_phone_delete.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//선택 수신자 번호 삭제 
					deleteSendList(index);
				}
			});		
			
			return smsPhoneView;
		}

	}		
}
