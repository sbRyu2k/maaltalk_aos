package com.dial070.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.RecentCallsData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.service.SipNotifications;
import com.dial070.sip.utils.Compatibility;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;

public class Calls extends AppCompatActivity {
//	private static final String THIS_FILE = "DIAL070_CALLS";
	
    private final static int SELECT_CALLS = 1;
    
	private Context mContext;
	private AppPrefs mPrefs;
	private ListView mCallsListView;
	private TextView mCallsEmptyView;
	CallsListAdapter mAdapter;	
	

	private static final int DIAL070_UPDATE_START =  1;
	private static final int DIAL070_UPDATE =  2;
	private static final int DIAL070_UPDATE_END =  3;
	
	//BJH
	private DownloadManager downloadManager;
	private DownloadManager.Request request;

	private InputMethodManager mImm;
	private EditText contacts_search;
	private String oldSearchKey;
	private String searchKey;
	private Cursor mCursor;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case DIAL070_UPDATE_START:
				{
					View emptyView = mCallsListView.getEmptyView();
					if (emptyView != null)
						emptyView.setVisibility(View.GONE);
				}
				break;
				case DIAL070_UPDATE:
				{
					reloadListView();
				}
				break;				
				case DIAL070_UPDATE_END:
				{
					if (mAdapter != null)
					{
						mAdapter.notifyDataSetChanged();
					}					
				}
				break;

			}
		}
	};

	public void setTitle(String title){
		View viewActionBar = getLayoutInflater().inflate(R.layout.title_actionbar_text, null);
		final androidx.appcompat.app.ActionBar abar = getSupportActionBar();
		androidx.appcompat.app.ActionBar.LayoutParams params = new androidx.appcompat.app.ActionBar.LayoutParams(//Center the textview in the ActionBar !
				androidx.appcompat.app.ActionBar.LayoutParams.WRAP_CONTENT,
				androidx.appcompat.app.ActionBar.LayoutParams.MATCH_PARENT,
				Gravity.CENTER);
		TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
		txtTitle.setText(title);
		abar.setCustomView(viewActionBar, params);
		abar.setDisplayShowCustomEnabled(true);
		abar.setDisplayShowTitleEnabled(false);
		abar.setHomeButtonEnabled(true);
		abar.setDisplayHomeAsUpEnabled(true);
		abar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = this;

		setTitle(getString(R.string.recent_call));
		
		setContentView(R.layout.calls_activity);

		mImm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		contacts_search= findViewById(R.id.contacts_search);
		ImageButton ic_more=findViewById(R.id.btnMore);
		ic_more.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showFilterPopup(v);
			}
		});

		oldSearchKey = "";
		searchKey = "";

		try {
			contacts_search.addTextChangedListener(new TextWatcher() {

				//@Override
				public void afterTextChanged(Editable s) {
					// 초성 검색
					try {

						searchKey = contacts_search.getText().toString();

						if(!oldSearchKey.equalsIgnoreCase(searchKey))
						{
							oldSearchKey = searchKey;
							doSearch();
						}


					} catch (Exception e) {
						Log.e("CallsActivity",e.getMessage(),e);
					}

				}

				//@Override
				public void beforeTextChanged(CharSequence s, int start,
											  int count, int after) {
					//do nothing
				}

				//@Override
				public void onTextChanged(CharSequence s, int start,
										  int before, int count) {
					//do nothing
				}

			});
		} catch (Exception e) {
			//
		}
		
		mCallsListView = (ListView) findViewById(R.id.calls_list);
		mCallsEmptyView = (TextView)findViewById(R.id.empty_result);
		
		mPrefs = new AppPrefs(this);
		
		mAdapter = new CallsListAdapter(this);
		//mAdapter.loadRecentCallsData();
		mCallsListView.setAdapter(mAdapter);
		if (Compatibility.getApiLevel() > 10)
			mCallsListView.setSelector(R.drawable.list_selector);
		mCallsListView.setFocusableInTouchMode(true);
	
		mCallsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				CallsListView cv = (CallsListView) arg1;
				if (cv != null) {
					//Popup dialog
					//PopupDialog(cv.getId(), cv.getName(), cv.getNumber(), cv.getMemo(), cv.getRec(), cv.getType());
					dialogSelect(cv.getId(), cv.getName(), cv.getNumber(), cv.getMemo(), cv.getRec(), cv.getType());
				}				
			}
		});
		
		mCallsListView.setEmptyView(mCallsEmptyView);
		View emptyView = mCallsListView.getEmptyView();
		if (emptyView != null)
			emptyView.setVisibility(View.GONE);	
		
		//BJH
		downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);

		SipNotifications notificationManager = new SipNotifications(this); // BJH 2018.07.25 부재중 건의사항 자동 종료 안 되도록
		notificationManager.cancelMissedCalls();
	}

	private void dialogSelect(int id, String name, String number, String memo, String rec, int type){
		boolean existUser = false;
		final String user_phone = number;
		final String user_name = name;
		final int rid = id;
		final String user_rec = rec;
		//기존 연락처 있는지 검사
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				existUser = true;
			}
			c.close();
		}


		ArrayList<String> listMenu = new ArrayList<>();
		listMenu.add(getString(R.string.popup_call_call));
		if(!existUser){
			listMenu.add(getString(R.string.popup_call_contact));
		}

		if(ServicePrefs.mUseMSG){
			listMenu.add(getString(R.string.popup_call_sms));
		}

		if(type == RecentCallsData.VMS_TYPE && user_rec != "" && user_rec.length() > 0) {
			listMenu.add(getString(R.string.popup_call_vms));
			listMenu.add(getString(R.string.popup_call_vms_down));
		}else if (rec.length() > 0){
			listMenu.add(getString(R.string.popup_call_rec));
			listMenu.add(getString(R.string.popup_call_rec_down));
		}

		listMenu.add(getString(R.string.popup_call_delete));

		final String[] menus = listMenu.toArray(new String[listMenu.size()]);

		String title;
		if(name != null && name.length() > 16) title = name.substring(0,16);

		if (name != null && name.length() > 0)
			title=name;
		else
			title=number;

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle(title);
		adb.setItems(menus, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (menus[which].equals(getString(R.string.popup_call_call))){
					if(user_phone != null && SipUri.isPhoneNumber(user_phone))
					{
						makeCall(user_phone);
						dialog.cancel();
					}
				}
				else if (menus[which].equals(getString(R.string.popup_call_contact))){
					if(user_phone != null && SipUri.isPhoneNumber(user_phone))
					{
						contactAdd(user_phone);
						dialog.cancel();
					}
				}else if (menus[which].equals(getString(R.string.popup_call_sms))){
					if(user_phone != null && SipUri.isPhoneNumber(user_phone))
					{
						goSMSWrite(user_phone, user_name);
						dialog.cancel();
					}
				}else if (menus[which].equals(getString(R.string.popup_call_vms))){
					String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
					Listening(url + "/" + user_rec, user_phone);
					dialog.cancel();
				}else if (menus[which].equals(getString(R.string.popup_call_vms_down))){
					String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
					Download(url + "/" + user_rec, user_phone);
					dialog.cancel();
				}else if (menus[which].equals(getString(R.string.popup_call_rec))){
					String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
					Listening(url + "/" + user_rec, user_phone);
					dialog.cancel();
				}else if (menus[which].equals(getString(R.string.popup_call_rec_down))){
					String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
					Download(url + "/" + user_rec, user_phone);
					dialog.cancel();
				}else if (menus[which].equals(getString(R.string.popup_call_delete))){
					if(deleteRecord(rid))
					{
						//v.setEnabled(false);
					}
					dialog.cancel();
				}
			}
		});

		adb.show();
	}

	private void makeCall(String number)
	{
		//전화걸기
		if (number != null && SipUri.isPhoneNumber(number) )
		{
			if(number.startsWith("00982")) {
				number = number.replace("00982", "0");
			}
			Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
			intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
			intent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
			mContext.sendBroadcast(intent);
		}
	}

	private void showFilterPopup(View v) {
		Context wrapper = new ContextThemeWrapper(this, R.style.SmsActionTheme);
		PopupMenu popup = new PopupMenu(wrapper, v);
		Menu menu=popup.getMenu();
		menu.add(Menu.NONE, MENU_OPTION_DELETE, Menu.NONE, R.string.delete_calls);
		// Setup menu item selection
		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				switch (item.getItemId()) {
					case MENU_OPTION_DELETE:
						Intent intent = new Intent(Calls.this, CallsEdit.class);
						startActivity(intent);
						break;
					default:
						break;
				}

				return false;
			}
		});
		// Handle dismissal with: popup.setOnDismissListener(...);
		// Show the menu
		popup.show();
	}

	private boolean doSearch()
	{
		Log.d("CallsActivity","doSearch searchKey:"+searchKey);
		/*if(searchKey.trim().length() == 0)
		{
			isLoaded = false;
			reloadListView();
			return true;
		}

		searchKey = searchKey.toLowerCase();

		mAdapter.searchRecentCallsData(searchKey);
		mAdapter.notifyDataSetChanged();*/

		mAdapter.filter(searchKey);

		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mAdapter.destroyAdapter();
	}
	
	@Override//BJH
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//BJH 
		unregisterReceiver(completeReceiver);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		reloadData();
		//BJH 다운완료
		IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		registerReceiver(completeReceiver, completeFilter); 
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//백버튼 비활성화
		DialMain.mainBackPressed(mContext);
		//super.onBackPressed();
	}
	
	private void reloadData()
	{
		reloadListView();
		loadFromServer();
	}
	
	
	private boolean isLoaded = false;
	private synchronized void reloadListView()
	{
		Log.d("CallsActivity","reloadListView");
		if (mAdapter.isChanged() || !isLoaded)
		{
			isLoaded = true;
			mCursor = mAdapter.loadRecentCallsData();
			mAdapter.notifyDataSetChanged();
		}
	}
	
	
	private Thread reloadThread = null;	
	private void loadFromServer()
	{
		if (reloadThread != null) return;
		reloadThread = new Thread()
		{
			public void run()
			{
				if(mAdapter != null)
				{
					int count = mAdapter.loadFromServer();
					if (count > 0)
						handler.sendEmptyMessage(DIAL070_UPDATE);
					//mAdapter.clear();
					//handler.sendEmptyMessage(DIAL070_RECENT_UPDATE_START);
					//mAdapter.loadRecentCallsData();
					//handler.sendEmptyMessage(DIAL070_RECENT_UPDATE_END);
				}
				reloadThread = null;
			};
		};
		reloadThread.start();	
	}
	
	
	
	
	/*
	private Thread reloadThread = null;	
	private synchronized void reloadListView()
	{
		if (reloadThread != null) return;
		
		if (mAdapter.isChanged() || !isLoaded)
		{
			isLoaded = true;
			reloadThread = new Thread()
			{
				public void run()
				{
					if(mAdapter != null)
					{
							mAdapter.clear();
							handler.sendEmptyMessage(DIAL070_RECENT_UPDATE_START);
							mAdapter.loadRecentCallsData();
							handler.sendEmptyMessage(DIAL070_RECENT_UPDATE_END);
							reloadThread = null;
					}			
				};
			};
			reloadThread.start();
		}
	}
	*/
	
	private void PopupDialog(int id, String name, String number, String memo, String rec, int type)
	{
		boolean existUser = false;
//		long uid = 0;
		//기존 연락처 있는지 검사 
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				existUser = true;
//				uid = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
			}
			c.close();
		}
		
		// custom dialog
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_call_detail);
		//dialog.setCanceledOnTouchOutside(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.call_detail_layout);
		
		layout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
		// set the custom dialog components - text, image and button
		TextView text_name = (TextView) dialog.findViewById(R.id.txt_name);
		if(name != null && name.length() > 16) name = name.substring(0,16);
		
		if (name != null && name.length() > 0)
			text_name.setText(name);
		else
			text_name.setText(number);


		final String user_phone = number;
		final String user_name = name;
		final int rid = id;
//		final String user_memo = memo;
		final String user_rec = rec;


		ImageButton dialogButtonContact = (ImageButton) dialog.findViewById(R.id.btn_popup_call_contact);
		// 주소록에 추가
		dialogButtonContact.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(user_phone != null && SipUri.isPhoneNumber(user_phone))
				{
					//addContact(user_phone);
					contactAdd(user_phone);
					dialog.cancel();
				}
			}
		});
		
		//기존사용자의 경우 위 버튼 숨기기
		if(existUser) dialogButtonContact.setVisibility(View.GONE);
		else dialogButtonContact.setVisibility(View.VISIBLE);
		

		ImageButton dialogButtonMemo = (ImageButton) dialog.findViewById(R.id.btn_popup_call_memo);
		dialogButtonMemo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//메모 하기
				PopupMemo(rid,user_name,user_phone);
				dialog.cancel();
			}
		});
		
		//문자 보내기
		ImageButton dialogButtonSms = (ImageButton) dialog.findViewById(R.id.btn_popup_call_sms);
		dialogButtonSms.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(user_phone != null && SipUri.isPhoneNumber(user_phone))
				{	
					//sendSMS(user_phone); // BJH 2017.01.19
					goSMSWrite(user_phone, user_name);
					dialog.cancel();
				}
			}
		});
		
		
		//녹취 청취
		ImageButton dialogButtonRec = (ImageButton) dialog.findViewById(R.id.btn_popup_call_rec);
		dialogButtonRec.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
				Listening(url + "/" + user_rec, user_phone);
				dialog.cancel();
			}
		});
		
		if(rec.length() == 0) dialogButtonRec.setVisibility(View.GONE);
		else dialogButtonRec.setVisibility(View.VISIBLE);	
	
		//BJH
		ImageButton dialogButtonRecDown = (ImageButton) dialog.findViewById(R.id.btn_popup_call_rec_down);
		dialogButtonRecDown.setOnClickListener(new View.OnClickListener() {
					
			@Override
			public void onClick(View v) 
			{
				String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
				Download(url + "/" + user_rec, user_phone);
				dialog.cancel();
			}
		});
				
		if(rec.length() == 0) dialogButtonRecDown.setVisibility(View.GONE);
		else dialogButtonRecDown.setVisibility(View.VISIBLE);	
		
		//BJH 2016.09.23
		ImageButton dialogButtonVMS = (ImageButton) dialog.findViewById(R.id.btn_popup_call_vms);
		dialogButtonVMS.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
				Listening(url + "/" + user_rec, user_phone);
				dialog.cancel();
			}
		});

		ImageButton dialogButtonVMSDown = (ImageButton) dialog.findViewById(R.id.btn_popup_call_vms_down);
		dialogButtonVMSDown.setOnClickListener(new View.OnClickListener() {
					
			@Override
			public void onClick(View v) 
			{
				String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
				Download(url + "/" + user_rec, user_phone);
				dialog.cancel();
			}
		});
		
		if(type == RecentCallsData.VMS_TYPE && user_rec != "" && user_rec.length() > 0) {
			dialogButtonVMS.setVisibility(View.VISIBLE);	
			dialogButtonVMSDown.setVisibility(View.VISIBLE);
			dialogButtonRec.setVisibility(View.GONE);
			dialogButtonRecDown.setVisibility(View.GONE);
		} else {
			dialogButtonVMS.setVisibility(View.GONE);	
			dialogButtonVMSDown.setVisibility(View.GONE);	
		}	
		
		//삭제
		ImageButton dialogButtonDelete = (ImageButton) dialog.findViewById(R.id.btn_popup_call_delete);
		dialogButtonDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(deleteRecord(rid))
				{
					v.setEnabled(false);
				}
				dialog.cancel();
			}
		});		
		dialog.show();	
	}
	
	private void Listening(String rec_file, String phone_number)
	{
		//String url = "http://trinix.makecover.co.kr/~sgkim/" + rec_file;
		//String url = "http://trinix.makecover.co.kr/~sgkim/SentimentalGreen.mp3";
				
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse(rec_file), "audio/*");
		
		try
		{
			startActivity(intent);
		}
		catch (Exception e)
		{
			//
		}
		
//		Intent intent = new Intent(this, RecordPlayer.class);
//		intent.putExtra("RECORD_URL",rec_file);
//		intent.putExtra("PHONE_NUMBER",phone_number);
//		startActivity(intent);		
	}
	
	//BJH REC DOWNLOAD
	private void Download(String rec_file, String phone_number)
	{
		Uri urlToDownload = Uri.parse(rec_file);
		List<String> pathSegments = urlToDownload.getPathSegments();
	    request = new DownloadManager.Request(urlToDownload);
	    String mFileName = pathSegments.get(pathSegments.size()-1);
	    String[] fileName = mFileName.split("-");
	    mFileName = fileName[0].concat("-".concat(fileName[3]));
	    request.setTitle(mFileName);
	    request.setDescription(getResources().getString(R.string.record_file_download));
	    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
	    	request.setShowRunningNotification(true);  
	    } else {
	        request.setNotificationVisibility(request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
	    }
	    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, mFileName);
//	    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
	    downloadManager.enqueue(request);
	}
	private BroadcastReceiver completeReceiver = new BroadcastReceiver(){//다운로드 완료되었을 때
		   	 
		@Override
	    public void onReceive(Context context, Intent intent) {
			Toast.makeText(context, context.getResources().getString(R.string.record_file_download_ok),Toast.LENGTH_SHORT).show();
	    }
	    	     
	};
	//BJH REC DOWNLOAD

	private void contactAdd(String number)
	{
		DialMain.skipClose = true;
		
		boolean existUser = false;
		long uid = 0;
		//기존 연락처 있는지 검사 
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				existUser = true;
				uid = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
			}
			c.close();
		}
		
		if(!existUser)
		{
			Intent intent = new Intent();
			intent.setAction(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.setData(Uri.fromParts("tel", number, null));
			intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);

			if(number.startsWith("01"))
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
			else
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER);
	
			startActivity(intent);
		}
		else
		{
			Intent intent = new Intent(Intent.ACTION_EDIT);
			//intent.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI + "/" + uid));
			Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, uid);
			intent.setData(contactUri);
			startActivity(intent);
		}
	}	
	
	private void PopupMemo(int id, String name, String number)
	{
		//Get Current Memo
		String memo = getCurrentMemo(id); 
		
		
		Intent intent = new Intent(Calls.this, PopupMemo.class);
		intent.putExtra("TYPE", "CALLS");
		intent.putExtra("NAME", name);
		intent.putExtra("PHONE", number);
		intent.putExtra("MEMO", memo);
		intent.putExtra("ID", id);
		startActivityForResult(intent,SELECT_CALLS);	
	}
	
	private void sendSMS(String number)
	{
		if (number.length() > 0)
		{
			DialMain.skipClose = true;
			//SMS 문자 보내기			
			try {
				
				/*
				 * 
Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
smsIntent.setType("vnd.android-dir/mms-sms");
smsIntent.putExtra("address","phoneNumber");         
smsIntent.putExtra("sms_body","message");
startActivity(smsIntent);
 
				 */
				
		
	            Intent intent = new Intent(Intent.ACTION_SENDTO);
	            Uri uri = Uri.parse("sms:" + number);
	            intent.setData(uri);
	            //intent.putExtra("sms_body","TEST");
	            startActivity(intent);			
			} catch (Exception e) { 
				e.printStackTrace(); 
			}
		}
	}
	
	private String getCurrentMemo(int id)
	{
		String rs = "";
		DBManager database = new DBManager(mContext);
		try 
		{
			database.open();
			Cursor c = database.getRecentCalls(id);
			
			if(c != null)
			{
				if(c.getCount() > 0 && c.moveToFirst())
				{
					rs = c.getString(c.getColumnIndex(RecentCallsData.FIELD_CALL_MEMO));
				}
				c.close();
			}

		} finally {
			database.close();
		}	
		
		return rs;
	}
	
	private boolean deleteRecord(int id)
	{
		DBManager database = new DBManager(mContext);
		try 
		{
			database.open();
			if(database.deleteRecentCalls(id))
			{
				Toast.makeText(mContext, getResources().getString(R.string.delete_ok), Toast.LENGTH_LONG).show();
				reloadListView();
				return true;
			}
			else
			{
				//삭제 실패
				AlertDialog.Builder msgDialog = new AlertDialog.Builder(Calls.this);
				msgDialog.setTitle(getResources().getString(R.string.app_name));
				msgDialog.setMessage(getResources().getString(R.string.error_delete));
				msgDialog.setIcon(R.drawable.ic_launcher);
				msgDialog.setNegativeButton(getResources().getString(R.string.close), null);
				msgDialog.show();
				return false;
			}
		} finally {
			database.close();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode)
		{
			case SELECT_CALLS:
				if (resultCode == RESULT_OK)
				{
					String memo = data.getStringExtra("MEMO");
					if(memo.length() > 0)
					{
						//apply memo icon
						reloadListView();
					}
				}
				break;
			default:
				break;
		}
		return;						
	}
	
	public static final int MENU_OPTION_DELETE = Menu.FIRST + 1;


	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if(menu != null)
		{
			menu.add(Menu.NONE, MENU_OPTION_DELETE, Menu.NONE, R.string.delete_calls);
		}
	    return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if(menu != null)
		{
			menu.clear();
			menu.add(Menu.NONE, MENU_OPTION_DELETE, Menu.NONE, R.string.delete_calls);
		}
		return true;
	}*/


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

	    switch (item.getItemId()) {
        case MENU_OPTION_DELETE:
			Intent intent = new Intent(Calls.this, CallsEdit.class);
			startActivity(intent);
            return true;
        default:
            break;
	    }			
		
		return super.onOptionsItemSelected(item);
	}	
	
	//BJH 2017.01.19 관리팀 요청
	private void goSMSWrite(String number, String name) {
		if(number != null && number.length() > 0) {
			number = number.replace("-", "");
			Intent intent = new Intent(this, SmsWrite.class);
			if(number.startsWith("00182"))
				number = number.replace("00182", "0");
			intent.putExtra("phone", number);
			intent.putExtra("name", name);
			mContext.startActivity(intent);	
		}
	}
	
}
