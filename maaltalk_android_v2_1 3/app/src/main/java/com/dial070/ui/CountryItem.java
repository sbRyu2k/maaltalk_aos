package com.dial070.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class CountryItem {
	public final String FIELD_COUNTRY_CODE = "COUNTRY_CODE";
	public final String FIELD_COUNTRY_NAME = "COUNTRY_NAME";
	public final String FIELD_COUNTRY_FLAG = "COUNTRY_FLAG";
	public final String FIELD_COUNTRY_TIME = "COUNTRY_TIME";	
	public final String FIELD_COUNTRY_GMT = "COUNTRY_GMT";	
	public String NameKor;
	public String NameEng;
	public String Code;
	public String Gmt;
	public String Flag;
	public int FreeMobile;
	public int FreeWired;
	public String strLocale;
	
	CountryItem(String aNameKor, String aNameEng, String aCode, String aGmt, String aFlag, int aFreeMobile, int aFreeWired, String aLocale)
	{
		NameKor = aNameKor;
		NameEng = aNameEng;
		Code = aCode;
		Gmt = aGmt;
		strLocale = aLocale; 
		int extension = 0;
		try
		{
			extension = aFlag.indexOf(".png");
		}
		catch(NullPointerException e)
		{		
		}
		if (extension > 0)	
			Flag = aFlag.substring(0, aFlag.length() - 4);			//.png 제거
		else
			Flag = aFlag;
		FreeMobile = aFreeMobile;
		FreeWired = aFreeWired;
	}
	
	public String getName()
	{
		if(strLocale.equalsIgnoreCase("한국어"))
			return NameKor;
		return NameEng;
	}
		
	public String getGmtTime(String aGmt)
	{
		if ( !(aGmt.length() == 5 && (aGmt.startsWith("+") || aGmt.startsWith("-")))) return "";

		String gmtZone = String.format("GMT%s:%s", aGmt.substring(0, 3), aGmt.substring(3, aGmt.length()));

		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("MMM-dd aa HH:mm", Locale.getDefault());
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone(gmtZone));

	    return dateFormatGmt.format(new Date());	
	}

}
