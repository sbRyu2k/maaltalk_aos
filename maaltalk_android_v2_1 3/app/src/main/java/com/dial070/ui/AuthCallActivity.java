package com.dial070.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AuthCallActivity extends Activity {
    private static final String THIS_FILE = "AuthCallActivity";
    private Context mContext;
    private AppPrefs mPrefs;
    private String m_CountryCode,mAuthCid, mAuthDistributorID, mAuthBirthDate, mAuthUserName, mAuthGender;
    private String mCode;
    private EditText mEdtCode;

    // 중복 클릭 방지 시간 설정
    private long mLastClickTime;
    private long mLastClickTime2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_call);

        mContext = this;

        mPrefs = new AppPrefs(this);

        mEdtCode=findViewById(R.id.edtCode);
        ImageButton btn_web_navi_close=findViewById(R.id.btn_web_navi_close);
        Button btnRetry=findViewById(R.id.btnRetry);
        Button btnNext=findViewById(R.id.btnNext);
        btn_web_navi_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthCallActivity.this.finish();
            }
        });
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 2000){
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                reqNumberAuth();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime2 < 2000){
                    return;
                }

                mLastClickTime2 = SystemClock.elapsedRealtime();

                if (mEdtCode.getText().toString().trim().length()==3){
                    completeAuth();
                }else{
                    showMessage("3자리의 인증코드를 입력해주세요.");
                }

            }
        });

        Intent intent=getIntent();
        m_CountryCode=intent.getStringExtra("country_code");
        mAuthCid=intent.getStringExtra("callerid");
        mAuthDistributorID=intent.getStringExtra("distributor_id");
        mAuthBirthDate=intent.getStringExtra("birth_date");
        mAuthUserName=intent.getStringExtra("user_name");
        mAuthGender=intent.getStringExtra("gender");

        reqNumberAuth();
    }

    private void showMessage(String msg) {
        new MaterialAlertDialogBuilder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    private int reqNumberAuth()
    {
        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL);
        if (url == null || url.length() == 0)
        {
            Log.e(THIS_FILE, "URL_REQ_VALIDCID_INTL ERROR");
            showMessage("URL_REQ_VALIDCID_INTL ERROR");
            return -1;
        }

        // url encoding
        try {
            mAuthCid = URLEncoder.encode(mAuthCid, "utf-8");
            mAuthDistributorID = URLEncoder.encode(mAuthDistributorID, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String qry_url = url + "?country_code="+m_CountryCode+"&callerid="+mAuthCid + "&distributor_id="+mAuthDistributorID +"&birth_date="+mAuthBirthDate+"&gender="+mAuthGender;

        String str = postData(mContext, qry_url);
        Log.d(THIS_FILE,"JSON : "+str);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return -1;
        }

        String dialNumber = null;
        String cid = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject certObject = jObject.getJSONObject("CERT_INFO");
                if(certObject.has("DIAL_NUMBER"))
                    dialNumber = certObject.getString("DIAL_NUMBER");
                if(certObject.has("CID"))
                    cid = certObject.getString("CID");
            }
            else
            {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return -1;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return -1;
        }

        if (cid == null || cid.length() == 0)
        {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_cid));
            return -1;
        }

        // 공백은 제거
        cid = cid.replaceAll(" ", "");


        // SAVE ID
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, cid);
        return 0;
    }

    private void completeAuth(){
        String code = m_CountryCode;

        String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_CODE);
        //String url = "http://211.253.24.69/auth/certcid_global/req_valid_code.php";
        if (url == null || url.length() == 0)
        {
            Log.e(THIS_FILE, "URL_REQ_VALIDCID_CODE ERROR");
            showMessage("URL_REQ_VALIDCID_CODE ERROR");
            return;
        }

        // url encoding
        try {
            mAuthCid = URLEncoder.encode(mAuthCid, "utf-8");
            mAuthDistributorID = URLEncoder.encode(mAuthDistributorID, "utf-8");
            //mAuthUserName = URLEncoder.encode(mAuthUserName, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String qry_url = url + "?valid_code="+mEdtCode.getText().toString().trim()+"&country_code="+code+"&cid="+mAuthCid + "&distributor_id="+mAuthDistributorID +"&birth_date="+mAuthBirthDate+"&user_name="+mAuthUserName+"&gender="+mAuthGender;

        String str = postData(mContext, qry_url);
        Log.d(THIS_FILE,"completeAuth JSON:"+str);
        if (str == null)
        {
            Log.e(THIS_FILE, "NOT RESPONSE");
            showMessage(getResources().getString(R.string.error_server));
            return;
        }

        String id = null;
        String password = null;
        String cid = null;
        String exist = null;
        String device = null;

        try
        {
            JSONObject jObject = new JSONObject(str);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0) {
                JSONObject certObject = jObject.getJSONObject("CREATE_ACCOUNT_INFO");
                if (certObject.has("ID"))
                    id = certObject.getString("ID");
                if (certObject.has("PASSWORD"))
                    password = certObject.getString("PASSWORD");
                if (certObject.has("EXIST"))
                    exist = certObject.getString("EXIST");
                if (certObject.has("CID"))
                    cid = certObject.getString("CID");
                if (certObject.has("DEVICE"))
                    device = certObject.getString("DEVICE");
            } else {
                Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR =" + responseObject.getString("RES_DESC"));
                showMessage(responseObject.getString("RES_DESC"));
                return;
            }

        }
        catch (Exception e)
        {
            Log.e(THIS_FILE, "JSON", e);
            showMessage(getResources().getString(R.string.error_data_recv));
            return;
        }

        if (id == null || id.length() == 0) {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_id));
            return;
        }

        if (password == null || password.length() == 0) {
            // 인증요청 오류
            showMessage(getResources().getString(R.string.auth_number_error_pwd));
            return;
        }

        // SAVE NUMBER
        mPrefs.setPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER, cid);
        mPrefs.setPreferenceStringValue(AppPrefs.AUTH_CID, "");

        mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, id);
        mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, password);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, true);
        mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, true);

        /*Intent intent = new Intent();
        intent.putExtra("id",id);
        intent.putExtra("pwd",password);
        setResult(RESULT_OK, intent);
        finish();*/

        // DO LOGIN
        if (ServicePrefs.login(this, id, password) >= 0) {
            String model = android.os.Build.MODEL;
            if (exist != null && exist.equals("EXIST") && model != null && model.length() > 0 && device != null && device.length() > 0 && !device.contains(model)) {
                logoutAlert();
            } else {
                Intent intent = new Intent(AuthCallActivity.this, DialMain.class);
                startActivity(intent);
                finish();
            }
        } else {
            showMessage(getResources().getString(R.string.error_login));
            return;
        }
    }

    private void logoutAlert() {
        new MaterialAlertDialogBuilder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(getResources().getString(R.string.mt_logout_notice))
                .setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(AuthCallActivity.this, DialMain.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }

    private String postData(Context context, String url) {
        Log.d(THIS_FILE, "REQ : " + url);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 20000);
            HttpConnectionParams.setSoTimeout(params, 20000);

            HttpPost httppost = new HttpPost(url);


            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            String line = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }
}
