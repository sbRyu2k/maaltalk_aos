package com.dial070.ui;

public class AdItem {
	private String AdName;
	private String ImgUrl;
	private String AdUrl;
	public String getAdName() {
		return AdName;
	}
	public void setAdName(String adName) {
		AdName = adName;
	}
	public String getImgUrl() {
		return ImgUrl;
	}
	public void setImgUrl(String imgUrl) {
		ImgUrl = imgUrl;
	}
	public String getAdUrl() {
		return AdUrl;
	}
	public void setAdUrl(String adUrl) {
		AdUrl = adUrl;
	}
}
