package com.dial070.ui;

import java.util.ArrayList;
import java.util.Map;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.EnglishUtils;
import com.dial070.utils.HangulUtils;

import android.content.Context;
import android.content.res.Resources;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;


public class CountryListAdapter extends BaseAdapter {
//	private static final String THIS_FILE="CountryListAdapter";

	private Context mContext;
	private Resources mResources;
	
	private ArrayAdapter<String> mHeaders;

	private final static int TYPE_SECTION_HEADER = 0;
	
	private static Map<String, ArrayList<CountryItem>> mCountryItemList;
	private AppPrefs mPrefs;
	private String mLocale;
	
	public CountryListAdapter(Context context)
	{
		mContext = context;
		mResources = mContext.getResources();	
		mHeaders = new ArrayAdapter<String>(context, R.layout.contacts_list_header);		
		mPrefs = new AppPrefs(mContext);
		mLocale = mPrefs.getPreferenceStringValue(AppPrefs.LOCALE);
		mCountryItemList = null;
		
		if(mLocale.equalsIgnoreCase("한국어"))
		{
			//섹션 초기화 
			for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
			{
				mHeaders.add(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			//기타 섹션 추가 
			mHeaders.add(HangulUtils.INITIAL_EXTRA_SECTION);
		}
		else
		{
			//섹션 초기화 
			for(int i=0; i < EnglishUtils.INITIAL_ENGLISH_SECTION.length ; i++)
			{
				mHeaders.add(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);
			}
			//기타 섹션 추가 
			mHeaders.add(EnglishUtils.INITIAL_EXTRA_SECTION);			
		}
	}
    
	
	public void setData(Map<String, ArrayList<CountryItem>> data)
	{
		if(data == null) return;
		
		mCountryItemList = data;
			
//
//		for(int i=0; i < mCountryItemList.size() ; i++) 
//		{
//			if(i == mCountryItemList.size()-1) mHeaders.add(HangulUtils.INITIAL_EXTRA_SECTION);
//			else mHeaders.add(HangulUtils.INITIAL_HANGUL_SECTION[i]);
//
//		}
	}
	
	public int getSectionPosition(String section)
	{
		int position = 0;
		
		if(mCountryItemList == null) return position;
		
		String sectionStr = null;		
		
		ArrayList<CountryItem> items = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{			
				if(i == mCountryItemList.size()-1) sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
				else sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[i];
			}
			else
			{
				if(i == mCountryItemList.size()-1) sectionStr = EnglishUtils.INITIAL_EXTRA_SECTION;
				else sectionStr = EnglishUtils.INITIAL_ENGLISH_SECTION[i];				
			}
			
			items = mCountryItemList.get(sectionStr);
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(items != null && items.size() > 0)
			{
				if(!sectionStr.equalsIgnoreCase(section))
				{
					position += items.size() + 1;
				}
				else return position;
			}
			
		}		

		return position;			
		
	}	
    
	//@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int count = 0;
		if( mCountryItemList == null) return count;
		
		ArrayList<CountryItem> item = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{				
				if(i == mCountryItemList.size()-1)
					item = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					item = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			else
			{
				if(i == mCountryItemList.size()-1)
					item = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					item = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);				
			}
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(item != null && item.size() > 0)
			{		
				count += item.size() + 1;
			}
		}

		return count;
	}
	
	//@Override
	public Object getItem(int position) {
		
		if( mCountryItemList == null) return null;
		
		int orgPosition = position;
//		int curPosition = 0;
		CountryItem item = null;		
		
		ArrayList<CountryItem> data = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{			
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			else
			{
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);				
			}
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				//curPosition += data.size();
				if(orgPosition <= data.size())
				{
					if(orgPosition == 0)
					{
						return null;
					}
					else
					{
						item = data.get(orgPosition - 1);
					}
					break;
				}
				orgPosition -= data.size() + 1;
			}			
		}		

		return item;	
	}	
	

	//@Override
	public long getItemId(int position) {
		return position;
	}	



	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		CountryListView countryView;
		
		if( mCountryItemList == null) return null;
			
//		String sectionStr = null;

		int orgPosition = position;
//		int curPosition = 0;
		CountryItem item = null;		
		
		ArrayList<CountryItem> data = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{	
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			else
			{
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);			
			}
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				//curPosition += data.size();
				if(orgPosition <= data.size())
				{
					if(orgPosition == 0)
					{
						//Header View
						return mHeaders.getView(i, convertView, parent);
					}
					else
					{
						item = data.get(orgPosition - 1);
						
						if(convertView == null)
						{
							countryView = new CountryListView(mContext, item);
						}
						else
						{
							countryView = (CountryListView) convertView;
						}		
						
						if(item != null)
						{
							countryView.setFlagIcon(mResources.getIdentifier(item.Flag, "drawable", mContext.getPackageName()));
							countryView.setText(0, item.getName());
							countryView.setText(1, item.Code);	
							countryView.setGmtTime(item.Gmt);
							countryView.setFreeCall(item.FreeWired,item.FreeMobile);
						}

						
						countryView.setBackgroundColor(mResources.getColor(R.color.list_even_background));
						
						return countryView;						
						
					}

				}
				orgPosition -= data.size() + 1;
			}			
		}				
		
				
		return null;				
	}


	@Override
	public int getItemViewType(int position) {
		int type = 1;

		int orgPosition = position;
//		int curPosition = 0;
//		CountryItem item = null;		
		
		ArrayList<CountryItem> data = null;
		for(int i=0; i < mCountryItemList.size() ; i++) 
		{
			if(mLocale.equalsIgnoreCase("한국어"))
			{	
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			}
			else
			{
				if(i == mCountryItemList.size()-1)
					data = mCountryItemList.get(EnglishUtils.INITIAL_EXTRA_SECTION);
				else	
					data = mCountryItemList.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);			
			}
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				//curPosition += data.size();
				if(orgPosition <= data.size())
				{
					if(orgPosition == 0)
					{
						return TYPE_SECTION_HEADER;	
					}
					else
					{
						return type;
					}
				}
				orgPosition -= data.size() + 1;
			}			
		}	
		return -1;
	}


	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		//Header and Item
		return 2;
	}


	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}

}
