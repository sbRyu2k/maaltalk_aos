package com.dial070.ui;

import com.dial070.maaltalk.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreListView extends LinearLayout {

//	private ScoreItem mScoreItem;
	private TextView mUserName,mUserPhone,mScore,mCallCount,mZone,mFavAdd,mConAdd,mCallState,mCallDur,mCallStart;
	
	
	public ScoreListView(Context context, ScoreItem item) {
		super(context);
		// TODO Auto-generated constructor stub

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.score_list_item, this, true);
		mUserName = (TextView)findViewById(R.id.txt_user_name);
		mUserPhone = (TextView)findViewById(R.id.txt_phone_number);
		mScore = (TextView)findViewById(R.id.txt_total_score);
		mCallCount = (TextView)findViewById(R.id.txt_count_score);
		mZone = (TextView)findViewById(R.id.txt_zone_score);
		mFavAdd = (TextView)findViewById(R.id.txt_fav_score);
		mConAdd = (TextView)findViewById(R.id.txt_con_score);
		mCallState = (TextView)findViewById(R.id.txt_state_score);
		mCallDur = (TextView)findViewById(R.id.txt_dur_score);
		mCallStart = (TextView)findViewById(R.id.txt_start_score);
//		mScoreItem = item;
	}

	public ScoreListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public void setData(ScoreItem item)
	{
		mUserName.setText(item.getName());
		mUserPhone.setText(item.getNumber());
		mScore.setText(Integer.toString(item.getTotalScore()));
		mCallCount.setText(Integer.toString(item.getCountScore()));
		mZone.setText(Integer.toString(item.getZoneScore()));
		mFavAdd.setText(Integer.toString(item.getFavScore()));
		mConAdd.setText(Integer.toString(item.getConScore()));
		mCallState.setText(Integer.toString(item.getStateScore()));
		mCallDur.setText(Integer.toString(item.getDurScore()));
		mCallStart.setText(Integer.toString(item.getStartScore()));
	}
	
	
}
