package com.dial070.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.dial070.maaltalk.R;
import com.dial070.utils.Log;

public class CallCenterParser {

	private static final String THIS_FILE = "DIAL070_CALL_CENTER_PARSER";
	private CallCenterResult mCr = null;

	ArrayList<CallCenterResult> parseResult;
	String tag;

	CallCenterParser() {
		parseResult = new ArrayList<CallCenterResult>();
	}

	ArrayList<CallCenterResult> getResult() {
		return parseResult;
	}

	int parseXml(Context context) {
		parseResult.clear();

		InputStream ins = context.getResources().openRawResource(
				R.raw.call_center_all);

		if (mCr == null) {
			try {
				int size = ins.available();

				// Read the entire resource into a local byte buffer.
				byte[] buffer = new byte[size];
				while (ins.read(buffer) != -1)
					;
				ins.close();

				String jsontext = new String(buffer);

				JSONObject jObject;
				try {
					jObject = new JSONObject(jsontext);
					JSONObject responseObject = jObject
							.getJSONObject("RESPONSE");
					Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
					Log.d(THIS_FILE, responseObject.getString("RES_TYPE"));
					Log.d(THIS_FILE, responseObject.getString("RES_DESC"));

					if (responseObject.getInt("RES_CODE") == 0) {
						JSONObject countryObject = jObject
								.getJSONObject("CALL_CENTER_INFO");

						Log.d(THIS_FILE,
								"SERIAL=" + countryObject.getString("SERIAL"));
						JSONArray countryItemArray = countryObject
								.getJSONArray("ITEMS");
						for (int i = 0; i < countryItemArray.length(); i++) {
							/*Log.d(THIS_FILE, "TITLE="
									+ countryItemArray.getJSONObject(i)
											.getString("TITLE").toString());
							Log.d(THIS_FILE, "DESC="
									+ countryItemArray.getJSONObject(i)
											.getString("DESC").toString());
							Log.d(THIS_FILE, "NUMBER="
									+ countryItemArray.getJSONObject(i)
											.getString("NUMBER").toString());
							Log.d(THIS_FILE, "ADDRESS="
									+ countryItemArray.getJSONObject(i)
											.getString("ADDRESS").toString());
							Log.d(THIS_FILE, "MAPX="
									+ countryItemArray.getJSONObject(i)
											.getString("MAPX").toString());
							Log.d(THIS_FILE, "MAPY="
									+ countryItemArray.getJSONObject(i)
											.getString("MAPY").toString());*/

							mCr = new CallCenterResult();

							if (mCr != null) {
								if(Locale.getDefault().toString().equals("ko_KR")) {//BJH 다국어 관련 고객센터 번호
									mCr.setTitle(countryItemArray.getJSONObject(i)
											.getString("TITLE_KO").toString());
									mCr.setAddress(countryItemArray
											.getJSONObject(i).getString("ADDRESS_KO")
											.toString());
								} else {
									mCr.setTitle(countryItemArray.getJSONObject(i)
											.getString("TITLE_EN").toString());
									mCr.setAddress(countryItemArray
											.getJSONObject(i).getString("ADDRESS_EN")
											.toString());
								}
								mCr.setDescription(countryItemArray
										.getJSONObject(i).getString("DESC")
										.toString());
								mCr.setCode(countryItemArray
										.getJSONObject(i).getString("CODE")
										.toString());
								mCr.setTelephone(countryItemArray
										.getJSONObject(i).getString("NUMBER")
										.toString());
								mCr.setMapX(countryItemArray.getJSONObject(i)
										.getString("MAPX").toString());
								mCr.setMapY(countryItemArray.getJSONObject(i)
										.getString("MAPY").toString());
								parseResult.add(mCr);
							}

						}
					} else {
						Log.d(THIS_FILE,
								"TYPE=" + responseObject.getString("RES_TYPE")
										+ " ERROR ="
										+ responseObject.getString("RES_DESC"));
					}
				} catch (Exception e) {
					Log.e(THIS_FILE, "JSON ERROR=" + e.getMessage());
				}

			} catch (IOException e) {
				Log.d(THIS_FILE, "ERROR =" + e.getMessage());
			}
		}

		int rs = 0;

		return rs;
	}

}
