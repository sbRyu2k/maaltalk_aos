package com.dial070.ui;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.dial070.maaltalk.R;

public class SmsSearch extends AppCompatActivity {
    private EditText edtMsgSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_search);

        getSupportActionBar().hide();

        ImageButton imgBtnBack=findViewById(R.id.imgBtnBack);
        edtMsgSearch=findViewById(R.id.msg_search);

        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmsSearch.this.finish();
            }
        });

        edtMsgSearch.post(new Runnable() {
            @Override
            public void run() {
                edtMsgSearch.setFocusableInTouchMode(true);
                edtMsgSearch.requestFocus();

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edtMsgSearch,0);

            }
        });
    }
}
