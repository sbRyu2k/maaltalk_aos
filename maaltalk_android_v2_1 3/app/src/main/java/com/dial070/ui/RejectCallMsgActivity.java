package com.dial070.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

public class RejectCallMsgActivity extends Activity {
    private AppPrefs mPrefs;
    private final static String THIS_FILE="RejectCallMsgActivity";
    private ListView listView;
    private ArrayList arrayListMsg;
    private EditText edtMsg;
    private CustomAdapter adapter;
    private InputMethodManager imm;
    private TextView txtLimit;
    private androidx.appcompat.app.AlertDialog sweetAlertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reject_call_msg);

        mPrefs = new AppPrefs(this);

        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        ImageButton btnClose=findViewById(R.id.btnClose);
        txtLimit=findViewById(R.id.txtLimit);
        edtMsg=findViewById(R.id.edtMsg);
        ImageButton btnAdd=findViewById(R.id.btnAdd);
        listView = this.findViewById(R.id.listMsg);
        listView.setEmptyView(findViewById(R.id.emptyView));

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RejectCallMsgActivity.this.finish();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMsg.getText().toString().trim().length()>0){
                    addMsgList(edtMsg.getText().toString());
                }
            }
        });

        setMsgList();
        adapter = new CustomAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(RejectCallMsgActivity.this,RejectCallMsgEditActivity.class);
                intent.putExtra("index",position);
                intent.putExtra("list",arrayListMsg);
                startActivity(intent);
            }
        });

        edtMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String msg = s.toString();
                int msg_len = 0;
                String msg_length = "";
                int remain_byte=0;
                try {
                    msg_len = msg.getBytes("KSC5601").length;
                    if (msg_len > 140){
                        msg_len =140;
                        s.delete(s.length()-2, s.length()-1);
                        if (sweetAlertDialog==null){
                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(RejectCallMsgActivity.this);
                            builder.setTitle(getResources().getString(R.string.app_name));
                            builder.setMessage("최대 글자 수에 도달하였습니다.");
                            builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });
                            sweetAlertDialog=builder.create();
                            sweetAlertDialog.show();
                        }else{
                            if(!sweetAlertDialog.isShowing()){
                                sweetAlertDialog.show();
                            }
                        }
                    }

                    remain_byte = 140 - (msg_len);
                    msg_length = String.format(Locale.getDefault(),"(%d/%d)" , remain_byte, 140);

                    txtLimit.setText(msg_length);
                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        Log.d(THIS_FILE,"onResume");
        setMsgList();
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.d(THIS_FILE,"onStart");
        super.onStart();
    }

    private void setMsgList(){
        /*String json=mPrefs.getPreferenceStringValue(mPrefs.REJECT_CALL_MSG);
        if (json==null || json.trim().length()==0){
            json=mPrefs.getRejectCallMsgOriginArr();
        }
        Log.d(THIS_FILE,"json:"+json);
        JSONObject jObject;
        try {
            jObject = new JSONObject(json);
            JSONArray urlItemArray = jObject.getJSONArray("list");
            arrayListMsg=new ArrayList();
            if (urlItemArray != null) {
                int count=urlItemArray.length();
                for (int i=0; i<count; i++){
                    arrayListMsg.add(urlItemArray.getJSONObject(i).getString("arr"+i).toString());
                }
            }
        } catch (JSONException e) {
            Log.d(THIS_FILE,"JSONException:"+e.getMessage());
        }*/
        String json=mPrefs.getPreferenceStringValue(mPrefs.REJECT_CALL_MSG);
        if (json==null || json.trim().length()==0){
            json=mPrefs.getRejectCallMsgOriginArr();
        }
        Log.d(THIS_FILE,"json:"+json);
        JSONObject jObject;
        try {
            jObject = new JSONObject(json);
            JSONArray urlItemArray = jObject.getJSONArray("list");
            arrayListMsg=new ArrayList();
            if (urlItemArray != null) {
                int count=urlItemArray.length();
                for (int i=0; i<count; i++){
                    arrayListMsg.add(urlItemArray.get(i).toString());
                }
            }
        } catch (JSONException e) {
            Log.d(THIS_FILE,"JSONException:"+e.getMessage());
        }

        //adapter.notifyDataSetChanged();
    }

    private void addMsgList(String msg){
        /*String json=mPrefs.getPreferenceStringValue(mPrefs.REJECT_CALL_MSG);
        if (json==null){
            json=mPrefs.getRejectCallMsgOriginArr();
        }
        Log.d(THIS_FILE,"json:"+json);
        JSONObject jObject = null;
        try {
            jObject = new JSONObject(json);
            JSONArray urlItemArray = jObject.getJSONArray("list");

            if (urlItemArray != null) {
                urlItemArray.put(msg);
            }

            jObject.put("list",urlItemArray);
        } catch (JSONException e) {
            Log.d(THIS_FILE,"JSONException:"+e.getMessage());
        }

        mPrefs.setPreferenceStringValue(mPrefs.REJECT_CALL_MSG,jObject.toString());*/
        if (arrayListMsg!=null){
            if (arrayListMsg.size()<6) {
                arrayListMsg.add(msg);

                adapter.notifyDataSetChanged();

                JSONObject wrapObject = new JSONObject();
                JSONArray jsonArray = new JSONArray(arrayListMsg);
                try {
                    wrapObject.put("list", jsonArray);
                    mPrefs.setPreferenceStringValue(mPrefs.REJECT_CALL_MSG,wrapObject.toString());
                }catch (JSONException e){
                    Log.d(THIS_FILE,"JSONException:"+e.getMessage());
                }

                imm.hideSoftInputFromWindow(edtMsg.getWindowToken(), 0);
                edtMsg.setText("");
            }else{
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(RejectCallMsgActivity.this);
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setMessage("메시지는 6개까지 추가할 수 있습니다.");
                builder.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        }
    }

    private void deleteMsgList(int index){
        /*String json=mPrefs.getPreferenceStringValue(mPrefs.REJECT_CALL_MSG);
        if (json==null){
            json=mPrefs.getRejectCallMsgOriginArr();
        }
        Log.d(THIS_FILE,"json:"+json);
        JSONObject jObject;
        try {
            jObject = new JSONObject(json);
            JSONArray urlItemArray = jObject.getJSONArray("list");

            if (urlItemArray != null) {
                int size=urlItemArray.length();

                JSONObject msgObj = new JSONObject();
                msgObj.put("arr"+size,msg);

                urlItemArray.put(msgObj);

            }
        } catch (JSONException e) {
            Log.d(THIS_FILE,"JSONException:"+e.getMessage());
        }*/

        if (arrayListMsg!=null){
            arrayListMsg.remove(index);
            adapter.notifyDataSetChanged();
        }

        JSONObject wrapObject = new JSONObject();
        JSONArray jsonArray = new JSONArray(arrayListMsg);
        try {
            wrapObject.put("list", jsonArray);
            mPrefs.setPreferenceStringValue(mPrefs.REJECT_CALL_MSG,wrapObject.toString());
        }catch (JSONException e){
            Log.d(THIS_FILE,"JSONException:"+e.getMessage());
        }
    }

    private class CustomAdapter extends BaseAdapter {

        public CustomAdapter(Context context) {

        }

        @Override
        public int getCount() {
            return arrayListMsg.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.listitem_rejectcall_msg, null);
            }

            TextView txtMsg=v.findViewById(R.id.txtMsg);
            ImageButton btnDel=v.findViewById(R.id.btnDel);
            btnDel.setFocusable(false);

            txtMsg.setText(arrayListMsg.get(position).toString());
            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteMsgList(position);
                }
            });

            return v;
        }
    }
}
