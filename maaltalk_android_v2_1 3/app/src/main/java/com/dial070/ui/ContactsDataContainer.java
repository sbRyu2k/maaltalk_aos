package com.dial070.ui;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


import com.dial070.global.ServicePrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.HangulUtils;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;


public class ContactsDataContainer {
//	private static final String THIS_FILE="ContactsDataContainer";

	//private Context mContext;
	//private static Cursor mCursor;

	//private int mHeaderCount;
	//private static int mContactsCount = 0;

	private static Map<String, ArrayList<ContactsData>> mSectionItemList;

    public static boolean mLoadContacts = false;

	public static void clearContactsList()
	{
		if(mSectionItemList != null && mSectionItemList.size() > 0)
		{
			mSectionItemList.clear();
			mLoadContacts = false;
		}
	}

	/*
    public static Map<String, ArrayList<ContactsData>> getContactsData(Context context,String locale)
    {
    	return getFiteringData(context, "",locale);
    }
    */


	/*
    public static int getContactsCount()
    {
    	return mContactsCount;
    }

    public static Map<String,String> getContactsPhoneData(Context context)
    {

    	Map<String,String> phoneList = new HashMap<String,String>();
    	phoneList.clear();

    	String where = "data1 like '+%' or data1 like '00%'";

    	Cursor c = null;
    	try
    	{
    		c = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
    	        new String[] {
				//ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID,
				ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.TYPE,
				ContactsContract.CommonDataKinds.Phone.LABEL
	            }
				, null,null,null);
    	}
    	catch (IllegalArgumentException e)
    	{
    		return null;
    	}

		if (c == null) return phoneList;

		while(c.moveToNext())
		{
			String contactPhoneNumber = null;
			String contactPhoneType = null;
			contactPhoneNumber =  c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			contactPhoneType = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

			//전화번호 중복 제거
			if(!phoneList.containsKey(contactPhoneNumber))
			{
				phoneList.put(contactPhoneNumber, contactPhoneType);
			}

		}
		c.close();

		return phoneList;
    }
    */

    private final static Comparator<ContactsData> sDisplayNameComparator =
        new Comparator<ContactsData>() {
        private final Collator   collator = Collator.getInstance();

        public int compare(ContactsData map1, ContactsData map2) {
            return collator.compare(map1.getDisplayName(), map2.getDisplayName());
        }
    };


    public static Map<String, ArrayList<ContactsData>> getFiteringData(Context context)
    {

		if(mLoadContacts) return mSectionItemList;

    	mSectionItemList = new HashMap<String, ArrayList<ContactsData>>();

		//섹션 초기화
		for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
		{
			mSectionItemList.put(HangulUtils.INITIAL_HANGUL_SECTION[i],null);
		}
		mSectionItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,null);

    	//Uri uri;
    	Cursor cur;

    	String sectionStr = null;

		String contactId = null;
		String contactName = null;
		String contactPhone = "";
		String contactPhoto = null;

		boolean isExtraSection = false;

    	//String where = "(data1 like '+%' or data1 like '00%')";
		/* BJH 2017.03.21 연락처 사진 이미지를 같이 가져오도록 수정 */
    	String[] projection = new String[] {
    			ContactsContract.Data._ID,
    			ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
    			ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI
             };


        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;

		cur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				projection, null, null,sortOrder);

		// 중복 제거를 위해.
    	Map<String,String> phoneList = new HashMap<String,String>();
    	phoneList.clear();

		if (cur != null)
		{
			//int indexId = cur.getColumnIndex(ContactsContract.Data._ID);
			int indexId = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
			int indexNumber = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
			int indexName = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
			int indexPhoto = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);

	 		while(cur.moveToNext())
	    	{
				contactId =  cur.getString(indexId);
				contactPhone =  cur.getString(indexNumber);
				contactName = cur.getString(indexName);
				contactPhoto = cur.getString(indexPhoto);

				if (contactName == null || contactName.length() == 0)
					continue;

				if (contactPhone == null || contactPhone.length() == 0) // BJH 2017.12.12 이슈 수정
					continue;

				// 중복 검사
				if (ServicePrefs.DIAL_SKIP_DUP_CONTACTS)
				{
					String key = contactPhone;
					key = key.replaceAll("-", "");
					if(phoneList.containsKey(key))
					{
						continue;
					}
					phoneList.put(key, contactName);
				}

				ArrayList<ContactsData> contacts = null;
				//Phone 정보 리스트

				ArrayList<PhoneItem> phoneItemList = new ArrayList<PhoneItem>();

					//section을 찾는다.
					String firstChosung = HangulUtils.getHangulChosung(contactName);

					if(firstChosung.length() == 0)
					{
						isExtraSection = true;
						sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
					}
					else
					{
						//char c = firstChosung.charAt(0);
						for (int i = 0; i< HangulUtils.INITIAL_HANGUL_SECTION.length; i++)
						{
							if (firstChosung.equalsIgnoreCase(HangulUtils.INITIAL_HANGUL_SECTION[i])) {
								isExtraSection = false;
								sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[i];
								break;
							}
						}
					}

					//폰 정보
					PhoneItem item = new PhoneItem(ContactHelper.getPhoneTypeName(context,99), contactPhone);
					phoneItemList.add(item);

					//ContactsData에 추가

					if(isExtraSection)
					{
						contacts = mSectionItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);

						if( contacts == null)
						{
							contacts = new ArrayList<ContactsData>();

							mSectionItemList.put(HangulUtils.INITIAL_EXTRA_SECTION,contacts);
						}
					}
					else
					{
						if(mSectionItemList.containsKey(sectionStr))
						{
							contacts = mSectionItemList.get(sectionStr);

							if( contacts == null)
							{
								contacts = new ArrayList<ContactsData>();

								mSectionItemList.put(sectionStr,contacts);
							}
						}
					}


				if(contacts != null)
				{
					contacts.add(new ContactsData( contactName,contactPhone,contactId,contactPhoto,phoneItemList));
				}

	    	} // end of where

	 		// close
 			cur.close();
 			cur = null;
		}

		 for(Object key : mSectionItemList.keySet())
		 {
			 ArrayList<ContactsData> object = mSectionItemList.get(key);
			 if(object != null && object.size() > 1)
			 {
				 try
				 {
				 	Collections.sort(object, sDisplayNameComparator);
				 }
				 catch (Exception e)
				 {
					 //
				 }
			 }
		 }

		//Loading complete!!
		mLoadContacts = true;
		return mSectionItemList;
    }

}


