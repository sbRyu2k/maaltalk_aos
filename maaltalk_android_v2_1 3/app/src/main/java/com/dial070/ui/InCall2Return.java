package com.dial070.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.Vibrator;
import android.provider.ContactsContract;

import com.dial070.utils.DebugLog;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.service.Fcm;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.api.SipUri;
import com.dial070.sip.api.SipUri.ParsedSipContactInfos;
import com.dial070.sip.models.CallerInfo;
import com.dial070.sip.service.MediaManager.MediaState;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.CallsUtils;
import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DialingFeedback;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.dial070.widgets.InCallControls2;
import com.dial070.widgets.InCallControls2.OnDialTriggerListener;
import com.dial070.widgets.InCallControls2.OnTriggerListener;
import com.dial070.widgets.InCallInfo;
import com.dial070.widgets.InCallInfo2;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pjsip.pjsua.pjsuaConstants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

//import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
//import android.widget.EditText;

//public class InCall extends Activity implements OnTriggerListener, OnDialTriggerListener, SensorEventListener
public class InCall2Return extends AppCompatActivity implements OnTriggerListener, SensorEventListener, OnDialTriggerListener {
    private static final int SMS_SELECT_CONTACT = 1;
    private final static int SELECT_CALLS = 2;
    private static final int UPDATE_FROM_CALL = 1;
    private static final int UPDATE_FROM_MEDIA = 2;
    private static final int UPDATE_FROM_TIMER = 3;
    private static final int CMD_CLOSE = 4;
    private static final int CMD_FINISH = 5;
    private static final int SELECT_CALL = 6;
    private static final float PROXIMITY_THRESHOLD = 5.0f;
    public static InCall2Return currentContext = null;
    public static boolean bMaaltalkRunned = false;
    public static int currentState = 0; // 0:pause, 1:resume
    private static String THIS_FILE = "IN CALL2 RETURN";
    PreferencesWrapper prefsWrapper = null;
    App app;
    private SipCallSession[] callsInfo = null;
    private MediaState lastMediaState;
    private Handler mQuitHandler = null;
    private Runnable mQuitRunnable = null;
    private FrameLayout mainFrame;
    private InCallInfo2 inCallInfo;
    private InCallControls2 inCallControls;
    private DialingFeedback dialFeedback;
    //	private boolean mPresenceSend = false;
    private View callInfoPanel;
    private Context mContext = null;
    // Screen wake lock for incoming call
    private WakeLock wakeLock = null;
    // SENSOR
    private SensorManager sensorManager = null;
    private Sensor proximitySensor = null;

    //	private AppPrefs mPrefs;
    //	private boolean isCallConnected = false;
    private boolean proximitySensorTracked = false;
    private PowerManager powerManager = null;
    private WakeLock proximityWakeLock = null;
    private boolean isShowDetails = true;
    private boolean isFirstRun = true;
    private boolean isQuitReq = false;
    //	private AlertDialog.Builder mBuilder;
    //	private AlertDialog mAlert;
    private Dialog mMemoDialog = null;
    private String mCallNumber = null;
    /*
     * Banner 관련 변수 추가 시작
     */
    private AppPrefs mPrefs;
    private ImageLoadingListener animateFirstListener;
    private DisplayImageOptions options;
    private Handler mHandler;
    private Boolean mSensor = true;//BJH Sensor On/Off
    private Boolean mSM = false; // BJH 2016.07.28
    private Boolean mSMStart = false; // BJH 2016.07.28
    // BottomSheetBehavior variable
    private BottomSheetBehavior bottomSheetBehavior;
    // TextView variable
    private TextView bottomSheetHeading;
    private ListView mListMsg;
    private Integer smsMid = 0;
    private ImageView imgArrowUp, imgArrowDown;
    /**
     * Service binding
     */
    private ISipService mSipService = null;
    private BroadcastReceiver dialCommandReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.i(THIS_FILE,"onReceive:"+intent.getAction());
            String command = intent.getStringExtra(DialMain.EXTRA_COMMAND);
            if (command == null) return;
            if (command.equalsIgnoreCase("NOTICE")) {
                String url = intent.getStringExtra(DialMain.EXTRA_NOTICE_URL);
                if (url != null && url.length() > 0) {
                    String title = intent.getStringExtra(DialMain.EXTRA_NOTICE_TITLE);
                    showNotice(url, title);

                    String fb_url = intent.getStringExtra(DialMain.EXTRA_NOTICE_FB_URL);
                    if (fb_url != null && fb_url.length() > 0)
                        Fcm.sendFeedback(context, fb_url);
                }
            }
        }
    };
    // Ui handler
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_FROM_CALL:
                    updateUIFromCall();
                    break;
                case UPDATE_FROM_MEDIA:
                    updateUIFromMedia();
                    break;
                case UPDATE_FROM_TIMER:
                    updateUIFromTimer();
                    break;
                case CMD_CLOSE:
                    callInfoPanel.setVisibility(View.VISIBLE);
                    inCallControls.setVisibility(View.GONE);
                    break;
                case CMD_FINISH:
                    finish();
                    break;
                case SELECT_CALL: {
                    inCallInfo.setTitleForFail();
                    String number = (String) msg.obj;
                    if (number == null || number.length() == 0) return;
                    //BJH 2016.08.26 관리팀 요청
				/*if (number.startsWith("+"))
					selectGCall(number);
				else
					selectGSMCall(number);*/
                    selectGSMCall(number);
                }
                break;

                default:
                    super.handleMessage(msg);
            }
        }
    };
    private ServiceConnection mSipConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            Log.i(THIS_FILE, "onServiceConnected");
            mSipService = ISipService.Stub.asInterface(arg1);
            try {
                if (mCallNumber != null) {
                    doSipCall(mCallNumber);
                } else {
                    // Log.d(THIS_FILE,
                    // "Service started get real call info "+callInfo.getCallId());
                    callsInfo = mSipService.getCalls();
                    if (callsInfo != null) {
                        handler.sendMessage(handler.obtainMessage(UPDATE_FROM_CALL));
                        handler.sendMessage(handler.obtainMessage(UPDATE_FROM_MEDIA));
                    } else {
                        delayedQuit(2000);
                    }
                }
            } catch (RemoteException e) {
                Log.e(THIS_FILE, "Can't get back the call", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.i(THIS_FILE, "onServiceDisconnected");
            mSipService = null;
            callsInfo = null;
        }
    };
    private BroadcastReceiver callStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(THIS_FILE,"onReceive:"+intent.getAction());
            if (action.equals(SipManager.ACTION_SIP_CALL_CHANGED)) {
                if (mSipService != null) {

                    SipCallSession callinfo = intent.getParcelableExtra(SipManager.EXTRA_CALL_INFO);
                    try {
                        callsInfo = mSipService.getCalls();

                        if (callsInfo != null && callsInfo.length > 0 && callinfo.getLastStatus() == 403) {

                        }

                    } catch (RemoteException e) {
                        Log.e(THIS_FILE, "Not able to retrieve calls");
                    }
                }

                handler.sendMessage(handler.obtainMessage(UPDATE_FROM_CALL));
            } else if (action.equals(SipManager.ACTION_SIP_MEDIA_CHANGED)) {
                handler.sendMessage(handler.obtainMessage(UPDATE_FROM_MEDIA));
            } else if (action.equals(SipManager.ACTION_SIP_CALL_BOTTOMSHEET)) {
                setBottomSheetVisibility(intent.getIntExtra("visible", View.INVISIBLE));
            }
        }
    };
    private boolean skipInput = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(THIS_FILE, "Create in call");
        DebugLog.d("onCreate()");

        setContentView(R.layout.in_call_activity_v2);

        currentContext = this;
        bMaaltalkRunned = true;
        mContext = this;

        app = (App) mContext.getApplicationContext();

        /*
         *Banner 관련 추가
         */
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();

        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .build();

        animateFirstListener = new AnimateFirstDisplayListener();

        mPrefs = new AppPrefs(this);
        mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED, false);
        //		mBuilder = new AlertDialog.Builder(this);
        //		mAlert = mBuilder.create();

        mMemoDialog = new Dialog(this);
        mMemoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        prefsWrapper = new PreferencesWrapper(this);

        mainFrame = (FrameLayout) findViewById(R.id.mainFrame);
        inCallInfo = findViewById(R.id.inCallInfo);
        inCallControls = findViewById(R.id.CallControls);
        inCallControls.setOnTriggerListener(this);
        inCallControls.setOnDialTriggerListener(this);

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        bottomSheetHeading = findViewById(R.id.bottomSheetHeading);
        imgArrowUp = findViewById(R.id.imgArrowUp);
        imgArrowDown = findViewById(R.id.imgArrowDown);
        imgArrowDown.setVisibility(View.INVISIBLE);

        findViewById(R.id.bottomSheetLayout).setVisibility(View.GONE);

        mListMsg = findViewById(R.id.listMsg);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, //context(액티비티 인스턴스)
                android.R.layout.simple_list_item_1, // 한 줄에 하나의 텍스트 아이템만 보여주는 레이아웃 파일
                // 한 줄에 보여지는 아이템 갯수나 구성을 변경하려면 여기에 새로만든 레이아웃을 지정하면 됩니다.
                getMsgList()  // 데이터가 저장되어 있는 ArrayList 객체
        );
        mListMsg.setAdapter(adapter);
        mListMsg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String msg = (String) parent.getAdapter().getItem(position);
                Toast.makeText(mContext, "수신거절 메시지를 보냈습니다.", Toast.LENGTH_SHORT).show();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                inCallControls.declineCall();

                String number = getCallNumber();

                if (number.startsWith("00982")) {
                    number = number.replace("00982", "0");
                } else if (number.startsWith("00182")) {
                    number = number.replace("00182", "0");
                }

                final String to = number;

                final ArrayList<Bitmap> fileList = new ArrayList<>();
                sendSmsMessage(to, msg, fileList);
            }
        });

        callInfoPanel = (View) findViewById(R.id.callInfoPanel);
        dialFeedback = new DialingFeedback(this, true);

        // 타이머 변경함.
        mQuitHandler = new Handler();
        mQuitRunnable = new Runnable() {
            @Override
            public void run() {
                if (SipService.currentService != null && SipService.pjService != null) {
                    // 내트워크 이상등으로 호가 정상적으로 종료되지 않는 경우를 위해.
                    SipService.pjService.resetActiveCallSession();
                }
                finish();
            }
        };

        mCallNumber = null;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String number = extras.getString(SipManager.EXTRA_CALL_NUMBER);
            if (number != null && number.length() > 0) {
                mCallNumber = number;
            }
        }

        //BJH Sensor On/Off 확인
        mSensor = mPrefs.getPreferenceBooleanValue(AppPrefs.SENSOR_ENABLE);
        if (mSensor)
            createLockResource();

        bindToService();

        init();

        // Capturing the callbacks for bottom sheet
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetHeading.setText(getString(R.string.reject_call_with_sms));
                } else {
                    bottomSheetHeading.setText(getString(R.string.reject_call_with_sms));
                }

                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        android.util.Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        imgArrowUp.setVisibility(View.VISIBLE);
                        imgArrowDown.setVisibility(View.INVISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        android.util.Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        android.util.Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        imgArrowUp.setVisibility(View.INVISIBLE);
                        imgArrowDown.setVisibility(View.VISIBLE);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        android.util.Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        android.util.Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }


            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });
    }

    private ArrayList getMsgList() {
        String json = mPrefs.getPreferenceStringValue(mPrefs.REJECT_CALL_MSG);
        if (json == null || json.trim().length() == 0) {
            json = mPrefs.getRejectCallMsgOriginArr();
        }
        Log.d(THIS_FILE, "json:" + json);
        JSONObject jObject;
        ArrayList arrayListMsg = new ArrayList();
        try {
            jObject = new JSONObject(json);
            JSONArray urlItemArray = jObject.getJSONArray("list");

            if (urlItemArray != null) {
                int count = urlItemArray.length();
                for (int i = 0; i < count; i++) {
                    arrayListMsg.add(urlItemArray.get(i).toString());
                }
            }
        } catch (JSONException e) {
            Log.d(THIS_FILE, "JSONException:" + e.getMessage());
        }

        return arrayListMsg;
    }

    @Override
    protected void onStart() {
        Log.d(THIS_FILE, "Start in call");
        super.onStart();
        currentState = 1;

        if (proximitySensor != null) {
			/* BJH 2017.03.08
			Error:Error: The WIFI_SERVICE must be looked up on the Application context or memory will leak on devices < Android N. Try changing to .getApplicationContext() [WifiManagerLeak]
			*/
            WifiManager wman = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo winfo = wman.getConnectionInfo();
            if (winfo == null || !prefsWrapper.keepAwakeInCall()) {
                // Try to use powermanager proximity sensor
                if (powerManager != null) {
                    try {
                        boolean supportProximity = false;
                        Field f = PowerManager.class.getDeclaredField("PROXIMITY_SCREEN_OFF_WAKE_LOCK");
                        int proximityScreenOffWakeLock = (Integer) f.get(null);
                        if (Compatibility.isCompatible(17)) {
                            // Changes of the private API on android 4.2
                            Method method = powerManager.getClass().getDeclaredMethod("isWakeLockLevelSupported", int.class);
                            supportProximity = (Boolean) method.invoke(powerManager, proximityScreenOffWakeLock);
                            Log.d(THIS_FILE, "Use 4.2 detection way for proximity sensor detection. Result is " + supportProximity);
                        } else {
                            Method method = powerManager.getClass().getDeclaredMethod("getSupportedWakeLockFlags");
                            int supportedFlags = (Integer) method.invoke(powerManager);
                            Log.d(THIS_FILE, "Proxmity flags supported : " + supportedFlags);
                            supportProximity = ((supportedFlags & proximityScreenOffWakeLock) != 0x0);
                        }

						/*
	                    if (android.os.Build.MODEL.toUpperCase().startsWith("SHV-E2") && !android.os.Build.MODEL.toUpperCase().startsWith("SHV-E250"))
	                    {
	                    	// 갤럭시 S3에서 문제가 있어서 지원하지 않는다.
	                    	supportProximity = false;
	                    }
						 */

                        if (Build.MODEL.toUpperCase().startsWith("SM") && Build.VERSION.SDK_INT >= 21) {
                            // BJH 2016.07.21 삼성 단말기 이슈
                            mSM = true;
                        }

                        if (supportProximity && !mSMStart) { //BJH 2016.07.28 삼성 단말기 최초 onStart 일 때
                            Log.d(THIS_FILE, "We can use native screen locker !!");
                            proximityWakeLock = powerManager.newWakeLock(proximityScreenOffWakeLock, "maaltalk:com.dial070.sip.CallProximity");
                            proximityWakeLock.setReferenceCounted(false);
                        }

                        if (mSM && !mSMStart) //BJH 2016.07.28 삼성 단말기 최초 onStart 일 때
                            mSMStart = true;
                    } catch (Exception e) {
                        Log.d(THIS_FILE, "Impossible to get power manager supported wake lock flags");
                    }
                }
            }
            if (proximityWakeLock == null) {
                // Fall back to manual mode
                isFirstRun = true;
                sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
                proximitySensorTracked = true;
            }
        }

        dialFeedback.resume();
        handler.sendMessage(handler.obtainMessage(UPDATE_FROM_CALL));
    }

    @Override
    public void onAttachedToWindow() {
        this.getWindow().setFlags(
                //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,

                //WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    @Override
    protected void onStop() {
        currentState = 0;

        if (proximityWakeLock != null && proximityWakeLock.isHeld()) {
            if (!mSM)//BJH 2016.07.28 삼성 단말기 onStop이 되는 경우 센서 오작동을 막음
                proximityWakeLock.release();
        }
        if (proximitySensor != null && proximitySensorTracked) {
            proximitySensorTracked = false;
            sensorManager.unregisterListener(this);
        }

        dialFeedback.pause();

        super.onStop();
        Log.d(THIS_FILE, "Stop in call");
        /*
         * 배너관련 추가
         */
        if (mHandler != null) {
            mHandler.removeMessages(0);
            mHandler = null;
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onPause");
        pauseProcess();
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "onResume");
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        Log.d(THIS_FILE, "Destroy in call");

        currentContext = null;
        bMaaltalkRunned = false;

        if (mMemoDialog != null && mMemoDialog.isShowing()) mMemoDialog.dismiss();

        if (mQuitHandler != null && mQuitRunnable != null)
            mQuitHandler.removeCallbacks(mQuitRunnable);

        unbindFromService();

        releaseLockResource();

        super.onDestroy();
    }

    private void pauseProcess() {
        if (mSipService != null) {
            SipCallSession call = getCurrentCallInfo();
            if (call != null && call.getCallId() != SipCallSession.INVALID_CALL_ID) {
                // 전화가 걸려왔을때 전원 버튼을 누르면 끊어준다.\
                int state = call.getCallState();
                if (call.isIncoming() &&
                        (state == SipCallSession.InvState.INCOMING ||
                                state == SipCallSession.InvState.EARLY)) {
                    if (SipService.currentService != null && SipService.pjService != null)
                        SipService.pjService.mediaManager.stopRing();

                    // REJECT
					/*
					final int _call_id = call.getCallId();
					Thread t = new Thread()
					{
						@Override
						public void run()
						{
							try {
								int status = mSipService.hangup(-1, 0);
								//if (status != pjsua.PJ_SUCCESS)
								{
									delayedQuit(2000);
								}
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					};
					if (t != null)
					{
						t.setPriority(Thread.MIN_PRIORITY);
						t.start();
					}
					 */
                }
            }
        }
    }

    private void createLockResource() {
        // lock
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "maaltalk:com.dial070.sip.onIncomingCall");

        // Sensor management
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        Log.d(THIS_FILE, "Proximty sensor : " + proximitySensor);
    }

    private synchronized void releaseLockResource() {
        if (wakeLock != null && wakeLock.isHeld()) {
            Log.d(THIS_FILE, "Releasing wake up lock");
            wakeLock.release();
            wakeLock = null;
        }

        if (proximityWakeLock != null && proximityWakeLock.isHeld()) {
            proximityWakeLock.release();
            proximityWakeLock = null;
        }
    }

    private boolean bindToService() {
        boolean bound = bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(callStateReceiver, new IntentFilter(SipManager.ACTION_SIP_CALL_CHANGED));
        registerReceiver(callStateReceiver, new IntentFilter(SipManager.ACTION_SIP_MEDIA_CHANGED));
        registerReceiver(dialCommandReceiver, new IntentFilter(DialMain.ACTION_DIAL_COMMAND_RETURN));
        registerReceiver(callStateReceiver, new IntentFilter(SipManager.ACTION_SIP_CALL_BOTTOMSHEET));
        return bound;
    }

    private void doSipCall(final String number) {
        Log.d(THIS_FILE, "doSipCall:" + number);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    SipProfileState accountInfo;
                    accountInfo = mSipService.getSipProfileState(0);
                    if (accountInfo != null && accountInfo.isValidForCall()) {
                        int status = mSipService.makeCall(number, 0);
                        if (status != pjsuaConstants.PJ_SUCCESS) {
                            handler.sendMessage(handler.obtainMessage(SELECT_CALL, number));
                        }
                    } else {
                        Log.e(THIS_FILE, "Service can't be called to make the call");
                        handler.sendMessage(handler.obtainMessage(SELECT_CALL, number));
                    }
                } catch (RemoteException e) {
                    Log.e(THIS_FILE, "Service can't be called to make the call");
                    handler.sendMessage(handler.obtainMessage(SELECT_CALL, number));
                }
            }
        };
        if (t != null) {
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
        }
    }

    private void doSipXfer(final int srcCallid, final int destCallid) {
        Log.d(THIS_FILE, "doSipXfer srcCallid:" + srcCallid + ", destCallid:" + destCallid);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    SipProfileState accountInfo;
                    accountInfo = mSipService.getSipProfileState(0);
                    if (accountInfo != null && accountInfo.isValidForCall()) {
                        //int status=mSipService.xfer(srcCallid, "01032572139");
                        int status = mSipService.xferReplace(srcCallid, destCallid, 0);
                        if (status == pjsuaConstants.PJ_SUCCESS) {
                            Log.d(THIS_FILE, "doSipXfer PJ_SUCCESS");
                        } else {
                            Log.d(THIS_FILE, "doSipXfer PJ_SUCCESS is not");
                        }
                    } else {
                        Log.e(THIS_FILE, "Service can't be called to xfer the call");
                    }
                } catch (RemoteException e) {
                    Log.e(THIS_FILE, "Service can't be called to xfer the call");
                }
            }
        };
        if (t != null) {
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
        }
    }

    private void setBottomSheetVisibility(int visible) {
       /* if(visible==View.VISIBLE){
            if(ServicePrefs.mUseMSG){
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                findViewById(R.id.bottomSheetLayout).setVisibility(View.VISIBLE);
            }else{
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                findViewById(R.id.bottomSheetLayout).setVisibility(View.GONE);
            }
        }else {
            //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            findViewById(R.id.bottomSheetLayout).setVisibility(View.GONE);
        }*/
        findViewById(R.id.bottomSheetLayout).setVisibility(View.GONE);
    }

    private void showNotice(String url, String title) {
        DialMain.skipCallview = true;

        Log.d(THIS_FILE, "showNotice : " + url);
        Intent intent = new Intent(mContext, WebClient.class);
        intent.putExtra("url", url);
        intent.putExtra("title", title);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP /*| Intent.FLAG_ACTIVITY_NO_ANIMATION*/);
        startActivity(intent);
    }

    private void unbindFromService() {
        try {
            unbindService(mSipConnection);
        } catch (Exception e) {
        }
        mSipService = null;

        try {
            unregisterReceiver(callStateReceiver);
        } catch (IllegalArgumentException e) {
            // That's the case if not registered (early quit)
        }

        try {
            unregisterReceiver(dialCommandReceiver);
        } catch (Exception e) {
            Log.w(THIS_FILE, "Unable to unregisterReceiver : dialCommandReceiver", e);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        // TODO : update UI
        Log.d(THIS_FILE, "New intent is launched");

        super.onNewIntent(intent);
    }

    //	private void gsmCall(String number) {
    //		Log.i(THIS_FILE, "gsmCall : " + number);
    //		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(String.format("tel:%s", number)));
    //		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    //		startActivity(intent);
    //	}

    private void selectGCall(final String number) {
        Resources res = mContext.getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
        msgDialog.setTitle(R.string.make_gsm_call);
        msgDialog.setMessage(res.getString(R.string.make_gcall_question));
        msgDialog.setButton(Dialog.BUTTON_POSITIVE,res.getString(R.string.yes), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int which) {
                DialMain.goGCall(mContext, number);
                finish();
                return;
            }
        });
        msgDialog.setButton(Dialog.BUTTON_NEGATIVE ,res.getString(R.string.no), new DialogInterface.OnClickListener() {

            //@Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                return;
            }
        });
        msgDialog.show();
    }

    private void selectGSMCall(final String number) {
        Resources res = mContext.getResources();
        AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
        msgDialog.setTitle(R.string.make_gsm_call);
        msgDialog.setMessage(res.getString(R.string.make_gsm_call_question));
        msgDialog.setButton(Dialog.BUTTON_POSITIVE ,res.getString(R.string.confirm), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int which) {
                //gsmCall(number);
                finish();
                return;
            }
        });
		/*
		msgDialog.setButton2(res.getString(R.string.no), new DialogInterface.OnClickListener() {

			//@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				return;



			}
		});*/
        msgDialog.show();
    }

    private SipCallSession getCurrentCallInfo() {
        Log.i(THIS_FILE, "getCurrentCallInfo");
        SipCallSession currentCallInfo = null;
        try {
            callsInfo = mSipService.getCalls();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (callsInfo == null) {
            return null;
        }

        Log.i(THIS_FILE, "callsInfo size:" + callsInfo.length);

        long callConnectStart = 0;
        for (SipCallSession callInfo : callsInfo) {
            Log.i(THIS_FILE, "getCurrentCallInfo getCallId:" + callInfo.getCallId());
            Log.i(THIS_FILE, "getCurrentCallInfo isActive():" + callInfo.isActive());
            Log.i(THIS_FILE, "getCurrentCallInfo getMediaStatus():" + callInfo.getMediaStatus());
            Log.i(THIS_FILE, "getCurrentCallInfo getConnectStart():" + callInfo.getConnectStart());
            //int state = callInfo.getCallState();
            if (callInfo.isActive()) {
                if (callConnectStart < callInfo.getConnectStart()) {
                    currentCallInfo = callInfo;
                    callConnectStart = callInfo.getConnectStart();
                }
            }
            /*switch (state) {
                case SipCallSession.InvState.NULL:
                case SipCallSession.InvState.DISCONNECTED:
                    break;
                default: {
                    if (callInfo.isActive()) {
                        if (callConnectStart < callInfo.getConnectStart()){
                            currentCallInfo = callInfo;
                            callConnectStart = callInfo.getConnectStart();
                        }
                    }

                    break;
                }
            }*/
            /*if (currentCallInfo != null) {
                break;
            }*/
        }
        return currentCallInfo;
    }

    private void updateUIFromCall() {
        if (mSipService == null) {
            return;
        }

        SipCallSession currentCallInfo = getCurrentCallInfo();
        Log.d(THIS_FILE, ">> Call : " + currentCallInfo);

        // Update in call actions
        inCallInfo.setCallState(currentCallInfo);
        inCallControls.setCallState(currentCallInfo);

        if (mCallNumber != null) {
            if (currentCallInfo == null) {
                inCallInfo.setTitleForCall(mCallNumber);
                return;
            }
            mCallNumber = null;
        }


        int state = 0;
        if (currentCallInfo != null) {
            Log.d(THIS_FILE, "Update ui from call " + currentCallInfo.getCallId() + " state " + CallsUtils.getStringCallState(currentCallInfo, this));
            state = currentCallInfo.getCallState();
            Log.d(THIS_FILE, "updateUIFromCall state:" + state);
        }

        // We manage wake lock
        switch (state) {
            case SipCallSession.InvState.INCOMING:
            case SipCallSession.InvState.EARLY:
            case SipCallSession.InvState.CALLING:

                if (wakeLock != null && !wakeLock.isHeld()) {
                    Log.d(THIS_FILE, "Acquire wake up lock");
                    wakeLock.acquire();
                }

                if (proximityWakeLock != null) {
                    if (currentCallInfo.isIncoming()) {
                        if (proximityWakeLock.isHeld()) {
                            proximityWakeLock.release();
                        }
                    } else {
                        if (!proximityWakeLock.isHeld()) {
                            proximityWakeLock.acquire();
                        }
                    }
                }
                //				isCallConnected = false;
                break;
            case SipCallSession.InvState.CONFIRMED:
                if (wakeLock != null && wakeLock.isHeld()) {
                    Log.d(THIS_FILE, "Releasing wake up lock - confirmed");
                    wakeLock.release();
                }

                if (proximityWakeLock != null && !proximityWakeLock.isHeld() && powerManager.isScreenOn())//BJH 센서 오류 해결책 캐시슬라이드 같은 어플이 있는 경우 OnStart, OnStop 이 됨.
                {
                    proximityWakeLock.acquire();
                }
                //	isCallConnected = true;
                //BJH 연결중이 생략될 때가 있음

                if (mSipService != null) {
                    int destCallId = 0;
                    int active_count = 0;
                    try {
                        SipCallSession[] callsInfo = mSipService.getCalls();
                        if (callsInfo != null && callsInfo.length > 0) {
                            for (SipCallSession callInfo : callsInfo) {
                                if (callInfo.isActive()) {
                                    active_count++;
                                    /*if(!callInfo.isIncoming()){
                                        destCallId=callInfo.getCallId();
                                        if (destCallId==currentCallInfo.getCallId()){

                                        }
                                    }*/
                                }
                            }

                            if (App.isReturnClicked && active_count == 2) {
                                Intent finishIntent = new Intent(Dial.BC_FINISH);
                                finishIntent.setPackage(getPackageName());
                                sendBroadcast(finishIntent);
//                                sendBroadcast(new Intent(Dial.BC_FINISH));
                            }
                        }
                    } catch (RemoteException e) {
                        Log.e(THIS_FILE, "Not able to retrieve calls");
                    }
                }


                break;
            case SipCallSession.InvState.NULL:
                Log.i(THIS_FILE, "WTF?");
            case SipCallSession.InvState.DISCONNECTED:
                Log.d(THIS_FILE, "SipCallSession.InvState.DISCONNECTED : " + currentCallInfo);
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                if (audioManager.getRingerMode() != AudioManager.RINGER_MODE_SILENT) {
                    closeCall(); // BJH 2017.06.28
                }

                // Set background to red and delay quit
                //delayedQuit(2000);
                delayedQuit(0);
                //메모 초기화
                AppPrefs prefs = new AppPrefs(mContext);
                prefs.setPreferenceStringValue(AppPrefs.LAST_CALL_MEMO, "");

                if (mSM) {//BJH 2016.07.28 삼성 단말기 통화가 종료되면 초기화 시킴.
                    mSM = false;
                    mSMStart = false;
                }
                /*
                 * BJH 2016.07.27 통화화면이 뜨자마자 발신자가 종료하는 경우 무한 링잉이 울리는 부분 수정, 2016.10.10 java.lang.NullPointerException
                 */
                if (SipService.pjService.mediaManager != null)
                    SipService.pjService.mediaManager.stopRing();

                return;
            case SipCallSession.InvState.CONNECTING:

                break;
        }

        int mediaStatus = SipCallSession.MediaState.NONE;
        if (currentCallInfo != null)
            currentCallInfo.getMediaStatus();
        switch (mediaStatus) {
            case SipCallSession.MediaState.ACTIVE:
                break;
            case SipCallSession.MediaState.REMOTE_HOLD:
            case SipCallSession.MediaState.LOCAL_HOLD:
            case SipCallSession.MediaState.NONE:
                break;
            case SipCallSession.MediaState.ERROR:
            default:
                break;
        }
        Log.d(THIS_FILE, "we leave the update ui function");

    }

    private void updateUIFromMedia() {
        if (SipService.pjService.mediaManager != null && mSipService != null) {
            MediaState mediaState = SipService.pjService.mediaManager.getMediaState();
            Log.d(THIS_FILE, "Media update ....");
            if (!mediaState.equals(lastMediaState)) {
                //				SipCallSession callInfo = getCurrentCallInfo();
                lastMediaState = mediaState;
                inCallControls.setMediaState(lastMediaState);
            }
        }
    }

    private void updateUIFromTimer() {
        if (mSipService == null) return;
        SipCallSession currentCallInfo = getCurrentCallInfo();
        if (currentCallInfo == null) return;
        //		int callId = currentCallInfo.getCallId();
        //
    }

    @Override
    public void onTrigger(int whichAction, final SipCallSession call) {
        if (skipInput) {
            return;
        }

        //BJH 2016.11.10
        final int action = whichAction;

        Log.d(THIS_FILE, "In Call Activity is triggered:" + whichAction);
        Log.d(THIS_FILE, "We have a current call : " + call);
        Log.d(THIS_FILE, "call.getCallId() : " + call.getCallId());

        try {
            switch (whichAction) {
                case TAKE_CALL: {
                    Log.i(THIS_FILE, "TAKE_CALL");
                    if (mSipService != null) {
                        inCallInfo.setAnswer();
                        if (call != null && call.getCallId() != SipCallSession.INVALID_CALL_ID) {
                            final int _call_id = call.getCallId();
                            Thread t = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        /*int status = */
                                        mSipService.answer(_call_id, SipCallSession.StatusCode.OK);
                                        pushResponse("TAKE"); //BJH 2016.11.10
                                    } catch (RemoteException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            };
                            if (t != null) {
                                t.setPriority(Thread.MIN_PRIORITY);
                                t.start();
                            }
                        }
                    }
                    break;
                }
                case DECLINE_CALL:
                case CLEAR_CALL: {
                    Log.i(THIS_FILE, "CLEAR_CALL");
                    if (mSipService != null) {
                        inCallInfo.setHangup();
                        inCallControls.setHangup();

                        if (call != null && call.getCallId() != SipCallSession.INVALID_CALL_ID) {
                            //							final int _call_id = call.getCallId();
                            Thread t = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        int destCallId = 0;
                                        int srcCallId = 0;
                                        long highConnectStart = 0;
                                        long lowConnectStart = 0;
                                        int active_count = 0;
                                        try {
                                            SipCallSession[] callsInfo = mSipService.getCalls();
                                            if (callsInfo != null && callsInfo.length > 0) {
                                                highConnectStart = callsInfo[0].getConnectStart();
                                                lowConnectStart = callsInfo[0].getConnectStart();
                                                destCallId = callsInfo[0].getCallId();

                                                for (SipCallSession callInfo : callsInfo) {
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallId:" + callInfo.getCallId());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallState:" + callInfo.getCallState());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatusText:" + callInfo.getLastStatusText());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatus:" + callInfo.getLastStatus());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getMediaStatus:" + callInfo.getMediaStatus());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getRemoteContact:" + callInfo.getRemoteContact());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress isIncoming:" + callInfo.isIncoming());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress isActive:" + callInfo.isActive());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress isSecure:" + callInfo.isSecure());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getConnectStart:" + callInfo.getConnectStart());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress getAccId:" + callInfo.getAccId());
                                                    Log.i(THIS_FILE, "getActiveCallInProgress callStart:" + callInfo.callStart);

                                                    if (callInfo.isActive()) {
                                                        if (callInfo.getConnectStart() > highConnectStart) {
                                                            highConnectStart = callInfo.getConnectStart();
                                                            if (!callInfo.isIncoming()) {
                                                                destCallId = callInfo.getCallId();
                                                            }
                                                        }

                                                        if (callInfo.getConnectStart() < lowConnectStart) {
                                                            lowConnectStart = callInfo.getConnectStart();
                                                            srcCallId = callInfo.getCallId();
                                                        }
                                                    }
                                                }
                                            }
                                        } catch (RemoteException e) {
                                            Log.e(THIS_FILE, "Not able to retrieve calls");
                                        }

                                        Log.e(THIS_FILE, "current call.getCallId:"+call.getCallId());
                                        Log.e(THIS_FILE, "destCallId:"+destCallId);
                                        mSipService.hangup(destCallId, 0);


                                        //if (status != pjsua.PJ_SUCCESS)
                                        {
                                            delayedQuit(2000);
                                            if (action == DECLINE_CALL) //BJH 2016.11.10
                                                pushResponse("DECLINE");
                                        }
                                    } catch (RemoteException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            };
                            if (t != null) {
                                t.setPriority(Thread.MIN_PRIORITY);
                                t.start();
                            }
                        } else {
                            finish();
                        }
                    }
                    break;
                }
                case MUTE_ON:
                case MUTE_OFF: {
                    if (mSipService != null) {
                        mSipService.setMicrophoneMute((whichAction == MUTE_ON) ? true : false);
                    }
                    break;
                }
                case SPEAKER_ON:
                case SPEAKER_OFF: {
                    if (mSipService != null) {
                        mSipService.setSpeakerphoneOn((whichAction == SPEAKER_ON) ? true : false);
                    }
                    break;
                }
                case DIALPAD_ON:
                case DIALPAD_OFF: {
                    ViewGroup view = (ViewGroup) inCallControls.findViewById(R.id.call_container_controls);
                    view.bringToFront();
                    break;
                }
                case DETAILED_DISPLAY: {
                    //inCallInfo.switchDetailedInfo(isShowDetails);
                    isShowDetails = !isShowDetails;
                    break;
                }
                case TOGGLE_HOLD: {
                    if (mSipService != null && call != null && call.getCallId() != SipCallSession.INVALID_CALL_ID) {
                        if (call.getMediaStatus() == SipCallSession.MediaState.LOCAL_HOLD || call.getMediaStatus() == SipCallSession.MediaState.NONE) {
                            mSipService.reinvite(call.getCallId(), true);
                        } else {
                            mSipService.hold(call.getCallId());
                        }
                    }
                    break;
                }
                case MEDIA_SETTINGS: {
                    break;
                }
                case CONTACTS: {
                    //주소혹에 추가
                    //http://www.techjini.com/blog/insert-and-modify-contact-in-android/ 참고
                    String add_number = getCallNumber();
                    if (add_number != null && SipUri.isPhoneNumber(add_number)) {

                        contactAdd(add_number);

                        //						Intent intent = new Intent(Intent.ACTION_INSERT,      ContactsContract.Contacts.CONTENT_URI);
                        //						intent.putExtra(ContactsContract.Intents.Insert.PHONE,add_number);
                        //						try {
                        //							startActivity(intent);
                        //						}
                        //						catch(ActivityNotFoundException e)
                        //						{
                        //
                        //						}
                    }
                    break;
                }
                case REC: {
                    toggleRec();
                    break;
                }
                case RETURN_ON: {
                    Log.i(THIS_FILE, "RETURN_ON");
                    if (mSipService != null) {
                        try {
                            SipCallSession[] callsInfo = mSipService.getCalls();
                            if (callsInfo != null && callsInfo.length > 0) {
                                for (SipCallSession callInfo : callsInfo) {
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallId:" + callInfo.getCallId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallState:" + callInfo.getCallState());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatusText:" + callInfo.getLastStatusText());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatus:" + callInfo.getLastStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getMediaStatus:" + callInfo.getMediaStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getRemoteContact:" + callInfo.getRemoteContact());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isIncoming:" + callInfo.isIncoming());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isActive:" + callInfo.isActive());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isSecure:" + callInfo.isSecure());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getConnectStart:" + callInfo.getConnectStart());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getAccId:" + callInfo.getAccId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress callStart:" + callInfo.callStart);
                                }
                            }
                        } catch (RemoteException e) {
                            Log.e(THIS_FILE, "Not able to retrieve calls");
                        }
                    }

                    break;
                }
                case RETURN_OFF: {
                    Log.i(THIS_FILE, "RETURN_OFF");
                    /*try {
                        SipCallSession[] callsInfo = mSipService.getCalls();
                        if (callsInfo!=null && callsInfo.length>0){
                            for(SipCallSession callInfo : callsInfo){
                                Log.i(THIS_FILE,"getActiveCallInProgress getCallId:"+callInfo.getCallId());
                                Log.i(THIS_FILE,"getActiveCallInProgress getCallState:"+callInfo.getCallState());
                                Log.i(THIS_FILE,"getActiveCallInProgress getLastStatusText:"+callInfo.getLastStatusText());
                                Log.i(THIS_FILE,"getActiveCallInProgress getLastStatus:"+callInfo.getLastStatus());
                                Log.i(THIS_FILE,"getActiveCallInProgress getMediaStatus:"+callInfo.getMediaStatus());
                                Log.i(THIS_FILE,"getActiveCallInProgress getRemoteContact:"+callInfo.getRemoteContact());
                                Log.i(THIS_FILE,"getActiveCallInProgress isIncoming:"+callInfo.isIncoming());
                                Log.i(THIS_FILE,"getActiveCallInProgress isActive:"+callInfo.isActive());
                                Log.i(THIS_FILE,"getActiveCallInProgress isSecure:"+callInfo.isSecure());
                                Log.i(THIS_FILE,"getActiveCallInProgress getConnectStart:"+callInfo.getConnectStart());
                                Log.i(THIS_FILE,"getActiveCallInProgress getAccId:"+callInfo.getAccId());
                                Log.i(THIS_FILE,"getActiveCallInProgress callStart:"+callInfo.callStart);
                            }
                        }
                    } catch (RemoteException e) {
                        Log.e(THIS_FILE, "Not able to retrieve calls");
                    }*/
                    break;
                }
                case RETURN_SEND: {
                    Log.i(THIS_FILE, "RETURN_SEND");
                    //String number=mPrefs.getPreferenceStringValue(AppPrefs.RETURN_CALL_NUMBER);
                    String number = inCallControls.getReturnCallNumber();
                    if (number.length() > 3) {
                        doSipCall(number);
                        //mSipService.xfer(call.getCallId(), "<sip:07079180020@222.122.38.174>");
                    }
                    break;
                }
                case RETURN_CANCEL: {
                    Log.i(THIS_FILE, "RETURN_CANCEL");
                    if (mSipService != null) {
                        int destCallId = 0;
                        int srcCallId = 0;

                        try {
                            SipCallSession[] callsInfo = mSipService.getCalls();
                            if (callsInfo != null && callsInfo.length > 0) {
                                for (SipCallSession callInfo : callsInfo) {
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallId:" + callInfo.getCallId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallState:" + callInfo.getCallState());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatusText:" + callInfo.getLastStatusText());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatus:" + callInfo.getLastStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getMediaStatus:" + callInfo.getMediaStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getRemoteContact:" + callInfo.getRemoteContact());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isIncoming:" + callInfo.isIncoming());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isActive:" + callInfo.isActive());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isSecure:" + callInfo.isSecure());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getConnectStart:" + callInfo.getConnectStart());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getAccId:" + callInfo.getAccId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress callStart:" + callInfo.callStart);

                                    if (callInfo.isActive()) {
                                        if (callInfo.isIncoming()) {
                                            srcCallId = callInfo.getCallId();
                                        } else {
                                            destCallId = callInfo.getCallId();
                                        }
                                    }
                                }
                            }
                        } catch (RemoteException e) {
                            Log.e(THIS_FILE, "Not able to retrieve calls");
                        }


                        final int nDestCallID = destCallId;
                        if (call != null && call.getCallId() != SipCallSession.INVALID_CALL_ID) {
                            //							final int _call_id = call.getCallId();
                            Thread t = new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        /*int status = */
                                        Log.i(THIS_FILE, "RETURN_CANCEL:" + nDestCallID);
                                        mSipService.hangup(nDestCallID, 0);
                                    } catch (RemoteException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            };
                            if (t != null) {
                                t.setPriority(Thread.MIN_PRIORITY);
                                t.start();
                            }
                        }
                    }
                    break;
                }
                case RETURN_REPLACE: {
                    if (mSipService != null) {
                        int destCallId = 0;
                        int srcCallId = 0;
                        long highConnectStart = 0;
                        long lowConnectStart = 0;
                        try {
                            SipCallSession[] callsInfo = mSipService.getCalls();
                            if (callsInfo != null && callsInfo.length > 0) {
                                highConnectStart = callsInfo[0].getConnectStart();
                                lowConnectStart = callsInfo[0].getConnectStart();

                                for (SipCallSession callInfo : callsInfo) {
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallId:" + callInfo.getCallId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getCallState:" + callInfo.getCallState());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatusText:" + callInfo.getLastStatusText());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getLastStatus:" + callInfo.getLastStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getMediaStatus:" + callInfo.getMediaStatus());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getRemoteContact:" + callInfo.getRemoteContact());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isIncoming:" + callInfo.isIncoming());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isActive:" + callInfo.isActive());
                                    Log.i(THIS_FILE, "getActiveCallInProgress isSecure:" + callInfo.isSecure());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getConnectStart:" + callInfo.getConnectStart());
                                    Log.i(THIS_FILE, "getActiveCallInProgress getAccId:" + callInfo.getAccId());
                                    Log.i(THIS_FILE, "getActiveCallInProgress callStart:" + callInfo.callStart);

                                    if (callInfo.isActive()) {
                                        if (callInfo.getConnectStart() > highConnectStart) {
                                            highConnectStart = callInfo.getConnectStart();
                                            if (!callInfo.isIncoming()) {
                                                destCallId = callInfo.getCallId();
                                            }
                                        }

                                        if (callInfo.getConnectStart() < lowConnectStart) {
                                            lowConnectStart = callInfo.getConnectStart();
                                            srcCallId = callInfo.getCallId();
                                        }
                                    }
                                }
                            }
                        } catch (RemoteException e) {
                            Log.e(THIS_FILE, "Not able to retrieve calls");
                        }

                        doSipXfer(srcCallId, destCallId);
                    }

                    break;
                }
            }
        } catch (RemoteException e) {
            Log.e(THIS_FILE, "Was not able to call service method", e);
        }

    }

    @Override
    public void onTrigger(int keyCode, int dialTone) {
        // TODO Auto-generated method stub
        Log.d(THIS_FILE, "keyCode:" + keyCode + ", dialTone:" + dialTone);
        if (mSipService != null) {
            SipCallSession currentCall = getCurrentCallInfo();
            if (currentCall != null && currentCall.getCallId() != SipCallSession.INVALID_CALL_ID) {
                /*if (keyCode==-1011){
                    String number=mPrefs.getPreferenceStringValue(AppPrefs.RETURN_CALL_NUMBER);
                    Log.d(THIS_FILE,"-1011 currentCall.getCallId():"+currentCall.getCallId());
                    if (number.length()>3){
                        doSipCall(number);
                    }
                    //doSipXfer(currentCall.getCallId(),number);
                }else if (keyCode==-1012){
                    doSipXfer(0, currentCall.getCallId());
                }*/
                if (keyCode == -1013) {
                    goContacts();
                } else {
                    try {
                        if (!mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)) {
                            mSipService.sendDtmf(currentCall.getCallId(), keyCode);
                        }

                        dialFeedback.giveFeedback(dialTone);
                        //					KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);

                        // jwkim delete keypad display
                        // char nbr = event.getNumber();
                        // dialPadTextView.getText().append(nbr);

                    } catch (RemoteException e) {
                        Log.e(THIS_FILE, "Was not able to send dtmf tone", e);
                    }
                }
            }
        }
    }

    //연락처 검색
    private void goContacts() {
        Intent intent = new Intent(InCall2Return.this, Contacts.class);
        intent.putExtra("SelectType", 0);    //0:contacts ,1 :favorites
        startActivityForResult(intent, SMS_SELECT_CONTACT);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (proximitySensorTracked && !isFirstRun) {
            float distance = event.values[0];
            boolean active = (distance >= 0.0 && distance < PROXIMITY_THRESHOLD && distance < event.sensor.getMaximumRange());
            Log.d(THIS_FILE, "Distance is now " + distance);
            boolean isValidCallState = false;
            if (callsInfo != null) {
                for (SipCallSession callInfo : callsInfo) {
                    int state = callInfo.getCallState();
                    isValidCallState |= ((state == SipCallSession.InvState.CONFIRMED) || (state == SipCallSession.InvState.CONNECTING) || (state == SipCallSession.InvState.CALLING) || (state == SipCallSession.InvState.EARLY && !callInfo.isIncoming()));
                    if (isValidCallState) break;
                }
            }

            /*
             * BJH 2016.07.25 센서 모드 작동중 일 때 터치 관련 수정
             */
            if (isValidCallState && active) {
                skipInput = true;
                mainFrame.setVisibility(View.INVISIBLE);
                inCallControls.setVisibility(View.INVISIBLE);

                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            } else {
                skipInput = false;
                mainFrame.setVisibility(View.VISIBLE);
                inCallControls.setVisibility(View.VISIBLE);

                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }

        }
        if (isFirstRun) {
            isFirstRun = false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (skipInput) {
            if (!(keyCode == KeyEvent.KEYCODE_VOLUME_UP || keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
                return super.onKeyDown(keyCode, event);
            }
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK: {
                //if (checkBackPressed()) return true;
                return true;
                //break;
            }
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                //
                // Volume has been adjusted by the user.
                //
                Log.d(THIS_FILE, "onKeyDown: Volume button pressed");
                int action = AudioManager.ADJUST_RAISE;
                if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    action = AudioManager.ADJUST_LOWER;
                }

                // Detect if ringing
                SipCallSession currentCallInfo = getCurrentCallInfo();
                // If not any active call active
                if (currentCallInfo == null && mSipService != null) {
                    break;
                }

                if (mSipService != null) {
                    try {
                        mSipService.adjustVolume(currentCallInfo, action, AudioManager.FLAG_SHOW_UI);
                    } catch (RemoteException e) {
                        Log.e(THIS_FILE, "Can't adjust volume", e);
                    }
                }

                return true;
            case KeyEvent.KEYCODE_CALL:
            case KeyEvent.KEYCODE_ENDCALL:
                return inCallControls.onKeyDown(keyCode, event);
            case KeyEvent.KEYCODE_SEARCH:
                // Prevent search
                return true;
            default:
                // Nothing to do
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_CALL:
            case KeyEvent.KEYCODE_ENDCALL:
            case KeyEvent.KEYCODE_SEARCH:
                return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean checkBackPressed() {
        if (mSipService != null) {
            SipCallSession _call = getCurrentCallInfo();
            if (_call != null) {
                //				final int _call_id = _call.getCallId();
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        try {
                            /*int status = */
                            mSipService.hangup(-1, 0);
                            //if (status != pjsua.PJ_SUCCESS)
                            {
                                delayedQuit(2000);
                            }
                        } catch (RemoteException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                };
                if (t != null) {
                    t.setPriority(Thread.MIN_PRIORITY);
                    t.start();
                }
                return true;
            }
        }
        return false;
    }

    private synchronized void delayedQuit(long delay) {
        Log.d(THIS_FILE, "delayedQuit : " + isQuitReq);

        if (isQuitReq) return;
        isQuitReq = true;

        DialMain.checkStack = true; // 혹시 호가 살아 있으면 statck restart

        // 전화가 끊어진 후 바로 자동 종료하면 말톡이 죽는 것처럼 느껴져서.
        // 전화 끊어진 후 화면이 꺼지면 자동 종료가 되도록 수정했습니다.
        //		if (DialMain.fromPush)
        //		{
        //			DialMain.fromPush = false;
        //			if (!isCallConnected)
        //			{
        //				Log.d(THIS_FILE, "SET AUTO CLOSE !!");
        //				if (DialMain.currentContext != null)
        //				{
        //					DialMain.currentContext.setAutoClose(10000);
        //				}
        //			}
        //		}

        handler.sendMessage(handler.obtainMessage(CMD_CLOSE));
        if (delay > 0) {
            mQuitHandler.postDelayed(mQuitRunnable, 1000);
        } else {
            handler.sendMessage(handler.obtainMessage(CMD_FINISH));
        }
    }

    /**
     *
     */
    private void init() {
        // 초기화
    }

    private String getCallNumber() {

        // 초기화
        String remoteNumber = null;

        if (mSipService == null) {
            return remoteNumber;
        }
        SipCallSession currentCallInfo = getCurrentCallInfo();


        if (currentCallInfo == null) {
            return remoteNumber;
        }

        final String remoteUri = currentCallInfo.getRemoteContact();

        if (remoteUri != null) {

            ParsedSipContactInfos uriInfos = SipUri.parseSipContact(remoteUri);
            String remoteContact = SipUri.getDisplayedSimpleContact(remoteUri);
            remoteNumber = uriInfos.userName;

            CallerInfo callerInfo = CallerInfo.getCallerInfoFromSipUri(mContext, remoteUri);
            if (callerInfo != null) {
                remoteContact = callerInfo.name;
                if (remoteContact == null || remoteContact.length() == 0)
                    remoteContact = callerInfo.phoneNumber;
                if (callerInfo.callType == 2)    //VMS
                {
                    return null;
                }

            }
        }

        return remoteNumber;
    }

    private void contactAdd(String number) {
        boolean existUser = false;
        long uid = 0;
        //기존 연락처 있는지 검사
        Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
        if (c != null) {
            if (c.getCount() > 0 && c.moveToFirst()) {
                existUser = true;
                uid = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
            }
            c.close();
        }

        //연락처 초기화
        ContactsDataContainer.clearContactsList();

        if (!existUser) {
            Intent intent = new Intent();
            intent.setAction(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.fromParts("tel", number, null));
            intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);

            if (number.startsWith("01"))
                intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            else
                intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER);

            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            //intent.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI + "/" + uid));
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, uid);
            intent.setData(contactUri);
            startActivity(intent);
        }
    }

    private void toggleRec() {
        Log.d(THIS_FILE, "toggleRec");
        String keyCode = "*3";
        if (mSipService != null) {
            SipCallSession currentCall = getCurrentCallInfo();
            if (currentCall != null && currentCall.getCallId() != SipCallSession.INVALID_CALL_ID) {
                try {
                    mSipService.sendDtmf2(currentCall.getCallId(), keyCode);
                } catch (RemoteException e) {
                    Log.e(THIS_FILE, "Was not able to send dtmf tone", e);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECT_CALLS:
                if (resultCode == RESULT_OK) {
                    String memo = data.getStringExtra("MEMO");
                    if (memo.length() > 0) {
                        //save memo
                        //reloadListView();
                    }
                }
                break;
            case SMS_SELECT_CONTACT:
                if (resultCode == RESULT_OK) {
                    String number = data.getStringExtra("PhoneNumber");
                    number = number.replace("-", "");
                    if (number.trim().length() > 0) {
                        inCallControls.setNumber(number);
                    }
                }
                break;
            default:
                break;
        }

        return;

    }

    /*
     * 배너 관련 추가
     */
    private String postAdInfo() {
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        String urlString = String.format("%s?name=%s", mPrefs.getPreferenceStringValue(AppPrefs.URL_AD_INFO), uid);

        Log.d(THIS_FILE, "AD INFO URL=" + urlString);

        try {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new HttpsClient(this);

            HttpParams params = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            HttpPost httppost = new HttpPost(urlString);

            httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
            // Execute HTTP Post Request
            Log.d(THIS_FILE, "HTTPS POST EXEC");
            HttpResponse response = httpclient.execute(httppost);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            return sb.toString();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "ClientProtocolException", e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(THIS_FILE, "Exception", e);
        }
        return null;
    }

    //BJH 2016.11.10
    private void pushResponse(String action) {
        long response_time = System.currentTimeMillis();
        DBManager database = new DBManager(mContext);
        try {
            database.open();
            AppPrefs prefs = new AppPrefs(mContext);
            String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
            TelephonyManager telManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            String number;
            if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    number = "Not Found";
                }
                else {
                    number = telManager.getLine1Number();
                }
            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
                    number = "Not Found";
                }
                else {
                    number = telManager.getLine1Number();
                }
            }

            String phone_number = InCallInfo.getPhoneNumber();
            if (phone_number != null) {
                phone_number = phone_number.replace("-", "");
            }
            PushRecordData push_record_data = new PushRecordData(0, 0, response_time, "", "CALL", phone_number, uid, number, action);
            database.insertPushRecord(push_record_data);
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //BJH 2017.06.28
    private void closeCall() {
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(1000);
    }

    private int sendSmsMessage(final String number, final String msg, final ArrayList<Bitmap> list) {
        String name = null;
        long mid = 0;
        final String to = number;

        // INSERT DATABASE
        DBManager database = new DBManager(mContext);
        try {
            database.open();

            name = ContactHelper.getContactsNameByPhoneNumber(mContext, to);
            if (name == null) name = to;
            //insertRecentCalls(database,"me",to,name);
            mid = insertSmsList(database, "me", to, name, msg, list);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        database.close();

        final ArrayList fileList = new ArrayList();

        Thread t = new Thread() {
            public void run() {
                // SEND TO SERVER //BJH 메시지 결과 값 출력
                int i = Sms.sendMessageToServer(mContext, to, msg, fileList);
                if (i < 0) {
                    String str = "";
                    if (i == -2) {
                        str = "did";
                    } else if (i == -3) {
                        str = "balance";
                    } else if (i == -4) {
                        str = "expired";
                    } else if (i == -5) {
                        str = "verified";
                    } else if (i == -6) {
                        str = "status";
                    }
                    Sms.deleteItem(mContext, smsMid);
                    Sms.updateDeleteList(mContext, to);

                    Looper.prepare();

                    Sms.AlertFail(mContext, str);

                    Looper.loop();
                }
            }
        };
        t.start();

        return 0;

    }

    private long insertSmsList(DBManager db, String from, String to, String name, String msg, ArrayList<Bitmap> list) {
        String user_name = ContactHelper.getContactsNameByPhoneNumber(mContext, to);

        int newCount = db.getSmsNewMsgCount(to);

        if (db.existSmsList(to)) {
            db.updateSmsList(to, user_name, msg, SmsListData.SEND_TYPE, newCount);
        } else {
            //내부번호 인지 체크 2:친구목록
            int userType = SmsListData.USER_TYPE_CONTACT;
            /*if(isInternal(to)) userType = SmsListData.USER_TYPE_BUDDY;*/

            SmsListData sms_list = new SmsListData(userType, to, name, SmsListData.SEND_TYPE, msg, user_name, System.currentTimeMillis(), newCount);
            db.insertSmsList(sms_list);
        }
        String callback = ServicePrefs.mUser070;
        //if(mChangedSender) callback = mSender.getText().toString();

        long cdate = System.currentTimeMillis();
        SmsMsgData sms_data = new SmsMsgData(0, from, to, callback, SmsMsgData.SEND_TYPE, 0, "", msg, cdate);
        long mid = db.insertSmsMsg(sms_data);

        smsMid = (int) mid;

        //int count = db.getSmsNewMsgCount(from);
        //db.updateSmsList(to,name, msg, SmsListData.SEND_TYPE, count);

        return mid;
    }

    static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
