package com.dial070.ui;

import android.os.Parcel;
import android.os.Parcelable;

public class StatItem implements Parcelable {
	private int rtt;
	private int loss;
	private int index;

	private String StatDate;

	public StatItem(Parcel in)
	{
		readFromParcel(in);
	}

	public StatItem(int rtt,int loss, int index, String date)
	{
		this.rtt = rtt;
		this.loss = loss;
		this.index = index;
		this.StatDate = date;
	}
	

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(rtt);
		dest.writeInt(loss);
		dest.writeInt(index);
		dest.writeString(StatDate);
	}

	private void readFromParcel(Parcel in){
		rtt = in.readInt();
		loss = in.readInt();
		index = in.readInt();
		StatDate = in.readString();
	}	
	


	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public StatItem createFromParcel(Parcel in) {
			return new StatItem(in);
		}

		public StatItem[] newArray(int size) {
			return new StatItem[size];
		}
	};
	   
	public int getRTT()
	{
		return rtt;
	}
	
	public int getLOSS()
	{
		return loss;
	}	
	
	public int getIndex()
	{
		return index;
	}
	
	public String getStatDate()
	{
		return StatDate;
	}
}
