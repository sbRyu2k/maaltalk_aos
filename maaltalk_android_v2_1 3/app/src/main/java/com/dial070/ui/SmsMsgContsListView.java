package com.dial070.ui;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.ClipboardManager;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.db.DBManager;
import com.dial070.db.RecentCallsData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipUri;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.ImageLoader;
import com.dial070.utils.Log;

public class SmsMsgContsListView extends LinearLayout {
//	private static final String THIS_FILE="SmsMsgContsListView";
	private Context mContext;
	private DBManager mDatabase; // form adapter
	private SmsMsgContsListAdapter mAdapter;
	private ContextWrapper mContextWrapper;

	private LinearLayout mLayoutFrom;
	private LinearLayout mLayoutMe;
	private LinearLayout mLayoutHeader;
//	private LinearLayout mLayoutMsgItem;

	private TextView mSmsMsgHeader;
	private TextView mSmsMsgTimeFrom;
	private TextView mSmsMsgFrom;
	private TextView mSmsMsgTimeTo;
	private TextView mSmsMsgTo;
//	private ImageView mNewMsg;
	private CheckBox mChkMsgTo;
	private CheckBox mChkMsgFrom;

	private ImageView mSmsFromImage1;
	private ImageView mSmsFromImage2;
	private ImageView mSmsFromImage3;
	
	
	private ImageView mSmsToImage1;
	private ImageView mSmsToImage2;
	private ImageView mSmsToImage3;
	

	private SmsMsgContsItem mSmsMsgContsItem;
	
	//BJH
	private String mNumber;
	private AppPrefs mPrefs;

	public SmsMsgContsListView(Context context, SmsMsgContsListAdapter adapter, DBManager database, SmsMsgContsItem item) {
		super(context);
		//BJH
		if(item.getSmsMsgType() == 1)
			mNumber = item.getSmsMsgFrom();
		else
			mNumber = item.getSmsMsgTo();
		
		mContext = context;
		mAdapter = adapter;
		mContextWrapper= new ContextWrapper(context);
		mDatabase = database;
		mPrefs = new AppPrefs(context);//BJH 2016.09.22

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.sms_msg_conts_list_item, this, true);

		mLayoutFrom = (LinearLayout) findViewById(R.id.lay_sms_msg_conts_from);
		mLayoutMe = (LinearLayout) findViewById(R.id.lay_sms_msg_conts_to);
		mLayoutHeader = (LinearLayout) findViewById(R.id.lay_sms_conts_header);
//		mLayoutMsgItem = (LinearLayout) findViewById(R.id.lay_sms_msg_item);

		mSmsMsgHeader = (TextView) findViewById(R.id.sms_msg_conts_header);

		mSmsMsgFrom = (TextView) findViewById(R.id.sms_msg_from);
		Linkify.addLinks(mSmsMsgFrom, Linkify.WEB_URLS);//BJH autoLink
		mSmsMsgFrom.setText(item.getSmsMsg());

		mSmsFromImage1 = (ImageView) findViewById(R.id.sms_from_image_1);
		mSmsFromImage2 = (ImageView) findViewById(R.id.sms_from_image_2);
		mSmsFromImage3 = (ImageView) findViewById(R.id.sms_from_image_3);

		mSmsMsgTimeFrom = (TextView) findViewById(R.id.sms_msg_last_time_from);
		mSmsMsgTimeFrom.setText(item.getSmsMsgTime());

		mChkMsgFrom = (CheckBox) findViewById(R.id.chk_sms_msg_from);
		mChkMsgTo = (CheckBox) findViewById(R.id.chk_sms_msg_to);

		mSmsMsgTo = (TextView) findViewById(R.id.sms_msg_to);
		Linkify.addLinks(mSmsMsgTo, Linkify.WEB_URLS);//BJH autoLink
		mSmsMsgTo.setText(item.getSmsMsg());
		
		mSmsToImage1 = (ImageView) findViewById(R.id.sms_to_image_1);
		mSmsToImage2 = (ImageView) findViewById(R.id.sms_to_image_2);
		mSmsToImage3 = (ImageView) findViewById(R.id.sms_to_image_3);
		

		mSmsMsgTimeTo = (TextView) findViewById(R.id.sms_msg_last_time_to);
		mSmsMsgTimeTo.setText(item.getSmsMsgTime());
		
		View.OnLongClickListener LongClipClickListener = new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) 
			{
				int mid = mSmsMsgContsItem.getSmsMsgId();
				//BJH 2016.10.11 사장님 요청 사항
				TextView tv = (TextView)v;
		        final String clipText = tv.getText().toString();
				//PopupDialog(mid,getResources().getString(R.string.menu));
				//PopupWorkDialog(mid, getResources().getString(R.string.select_work), clipText);
				dialogSelectWorkDialog(mid, getResources().getString(R.string.select_work), clipText);
				return true;
			}};
			
		//BJH 2016.10.13 음성사서함 버그 수정
		View.OnClickListener onClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				String vms = mSmsMsgContsItem.getVMS();
				if(vms == null)
					return;
				
				//PopupVMSDialog(vms);
				dialogSelectVMS(vms);
			}};
		mSmsMsgFrom.setClickable(true);
		mSmsMsgFrom.setOnClickListener(onClickListener);	
		
		//BJH 2016.10.13 이미지
		View.OnLongClickListener LongImage1ClickListener = new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) 
			{
				int mid = mSmsMsgContsItem.getSmsMsgId();
				String url = mSmsMsgContsItem.getSmsFile1();
				PopupImageDialog(mid, getResources().getString(R.string.select_work), url);
				return true;
			}};
		View.OnLongClickListener LongImage2ClickListener = new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) 
			{
				int mid = mSmsMsgContsItem.getSmsMsgId();
				String url = mSmsMsgContsItem.getSmsFile2();
				PopupImageDialog(mid, getResources().getString(R.string.select_work), url);
				return true;
			}};
		View.OnLongClickListener LongImage3ClickListener = new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) 
			{
				int mid = mSmsMsgContsItem.getSmsMsgId();
				String url = mSmsMsgContsItem.getSmsFile3();
				PopupImageDialog(mid, getResources().getString(R.string.select_work), url);
				return true;
			}};
			
			
		
		
		mSmsMsgFrom.setLongClickable(true);
		mSmsMsgFrom.setOnLongClickListener(LongClipClickListener);
		mSmsFromImage1.setLongClickable(true);
		mSmsFromImage1.setOnLongClickListener(LongImage1ClickListener);
		mSmsFromImage2.setLongClickable(true);
		mSmsFromImage2.setOnLongClickListener(LongImage2ClickListener);
		mSmsFromImage3.setLongClickable(true);
		mSmsFromImage3.setOnLongClickListener(LongImage3ClickListener);
		
		mSmsMsgTo.setLongClickable(true);
		mSmsMsgTo.setOnLongClickListener(LongClipClickListener);
		mSmsToImage1.setLongClickable(true);
		mSmsToImage1.setOnLongClickListener(LongImage1ClickListener);
		mSmsToImage2.setLongClickable(true);
		mSmsToImage2.setOnLongClickListener(LongImage2ClickListener);
		mSmsToImage3.setLongClickable(true);
		mSmsToImage3.setOnLongClickListener(LongImage3ClickListener);
	
		this.mSmsMsgContsItem = item;
	}
	
	public void clearResource()
	{
		clearImageView(mSmsFromImage1);
		clearImageView(mSmsFromImage2);
		clearImageView(mSmsFromImage3);
		
		clearImageView(mSmsToImage1);
		clearImageView(mSmsToImage2);
		clearImageView(mSmsToImage3);
		
	}

	public SmsMsgContsItem getItem() {
		return mSmsMsgContsItem;
	}

	private String dateFormatLocale(long time) {
		String rs = "";
		if (time <= 0)
			return rs;
		Date temp = new Date(time);

		int year = temp.getYear() + 1900;
		int month = temp.getMonth() + 1;
		int day = temp.getDate();
		int e = temp.getDay();

		String strDay = "";

		switch (e) {
		case 0:
			strDay = getResources().getString(R.string.sunday);
			break;
		case 1:
			strDay = getResources().getString(R.string.monday);
			break;
		case 2:
			strDay = getResources().getString(R.string.thesday);
			break;
		case 3:
			strDay = getResources().getString(R.string.wednesday);
			break;
		case 4:
			strDay = getResources().getString(R.string.thursday);
			break;
		case 5:
			strDay = getResources().getString(R.string.friday);
			break;
		case 6:
			strDay = getResources().getString(R.string.saturday);
			break;
		default:
			break;
		}

		rs = String.format(Locale.getDefault(), "------ %4d%s %2d%s %2d%s %s ------", 
				year, getResources().getString(R.string.year), 
				month, getResources().getString(R.string.month),
				day, getResources().getString(R.string.day),
				strDay);

		return rs;
	}

	private String dateFormatTime(long time) {
		String rs = "";
		if (time <= 0)
			return rs;
		Date temp = new Date(time);
		SimpleDateFormat formatter = new SimpleDateFormat("a h:mm",Locale.getDefault());
		rs = formatter.format(temp);
		return rs;
	}

	public void setCheckBox(String from, boolean checked) {
		if (!from.equalsIgnoreCase("me")) {
			mChkMsgFrom.setChecked(checked);
		} else {
			mChkMsgTo.setChecked(checked);
		}
	}

	public void setSmsMsgContsData(SmsMsgContsItem item, boolean mode, boolean checked) {
		if (item == null)
			return;

		this.mSmsMsgContsItem = item;

		if (mode) {
			mChkMsgFrom.setVisibility(View.VISIBLE);
			mChkMsgTo.setVisibility(View.VISIBLE);
		} else {
			mChkMsgFrom.setVisibility(View.GONE);
			mChkMsgTo.setVisibility(View.GONE);
		}

		long time = 0;
		try {
			time = Long.parseLong(item.getSmsMsgTime().trim());
		} catch (NumberFormatException e) {
		}

		String smsMsgDate = dateFormatTime(time);
		String smsMsg = item.getSmsMsg();
		//BJH 2016.10.13
		String vms = item.getVMS();
		
		int mid = item.getSmsMsgId();
		String file1 = item.getSmsFile1();
		String file2 = item.getSmsFile2();
		String file3 = item.getSmsFile3();
		
		int fcount = 0;
		if (file1 != null && file1.length() > 0) fcount++;
		if (file2 != null && file2.length() > 0) fcount++;
		if (file3 != null && file3.length() > 0) fcount++;
		
		if (item.getShowDay())
		{
			mLayoutHeader.setVisibility(View.VISIBLE);
			mSmsMsgHeader.setText(dateFormatLocale(time));			
		}
		else
		{
			mLayoutHeader.setVisibility(View.GONE);
			mSmsMsgHeader.setText("");			
		}
		
		

//		if (item.getSmsMsgType() == SmsMsgData.HEADER_TYPE) 
//		{
//			mLayoutMe.setVisibility(View.GONE);
//			mLayoutFrom.setVisibility(View.GONE);
//			mLayoutHeader.setVisibility(View.VISIBLE);
//			// mLayoutChatItem.setLayoutParams(new
//			// LayoutParams(LayoutParams.FILL_PARENT, 20));
//			mSmsMsgHeader.setText(dateFormatLocale(time));
//		} 
//		else
		{
			if (!item.getSmsMsgFrom().equalsIgnoreCase("me")) {
//				mLayoutHeader.setVisibility(View.GONE);
				mLayoutMe.setVisibility(View.GONE);
				mLayoutFrom.setVisibility(View.VISIBLE);
				
				mSmsMsgFrom.setText(smsMsg);
				mSmsMsgTimeFrom.setText(smsMsgDate);
				
				if (smsMsg != null && smsMsg.length() > 0)
					mSmsMsgFrom.setVisibility(View.VISIBLE);
				else if (fcount == 0)
					mSmsMsgFrom.setVisibility(View.VISIBLE);
				else
					mSmsMsgFrom.setVisibility(View.GONE);
				
				/*
				 * BJH 2016.10.13 마시멜로우 버전부터 TextView가 GONE이면 이미지도 보이지 않는다.
				 */
				if(fcount > 0 && smsMsg.length() == 0) {
					smsMsg = mContext.getString(R.string.file_subject);
					mSmsMsgFrom.setText(mContext.getString(R.string.file_subject));
					mSmsMsgFrom.setVisibility(View.VISIBLE);
				}
				// 2016.10.13 음성사서함 버그 수정
				if(vms != null && vms.length() > 0) {	
					int flag_id = getResources().getIdentifier("vms", "drawable",
							mContext.getPackageName());
					if (flag_id != 0) {
						Drawable icon = getResources().getDrawable(flag_id);
						icon.setBounds(0, 0, icon.getMinimumWidth(),
								icon.getMinimumHeight());// BJH OUT OF MEMORY
						mSmsMsgFrom.setCompoundDrawablesWithIntrinsicBounds(icon, null,
								null, null);
					}
				} else {
					mSmsMsgFrom.setCompoundDrawablesWithIntrinsicBounds(null, null,
							null, null);
				}

				if (mode) {
					mChkMsgFrom.setChecked(checked);
				}
				
				// SET MEDIA 
				setImageViewUrl(item, mSmsFromImage1, file1, mid, 1);
				setImageViewUrl(item, mSmsFromImage2, file2, mid, 2);
				setImageViewUrl(item, mSmsFromImage3, file3, mid, 3);
				
			} else {
				// 내가 보낸 메세지
//				mLayoutHeader.setVisibility(View.GONE);
				mLayoutFrom.setVisibility(View.GONE);
				mLayoutMe.setVisibility(View.VISIBLE);

				mSmsMsgTo.setText(smsMsg);
				mSmsMsgTimeTo.setText(smsMsgDate);
				
				if (smsMsg != null && smsMsg.length() > 0)
					mSmsMsgTo.setVisibility(View.VISIBLE);
				else if (fcount == 0)
					mSmsMsgTo.setVisibility(View.VISIBLE);
				else
					mSmsMsgTo.setVisibility(View.GONE);	
				
				/*
				 * BJH 2016.10.13 마시멜로우 버전부터 TextView가 GONE이면 이미지도 보이지 않는다.
				 */
				if(fcount > 0 && smsMsg.length() == 0) {
					smsMsg = mContext.getString(R.string.file_subject);
					mSmsMsgTo.setText(smsMsg);
					mSmsMsgTo.setVisibility(View.VISIBLE);
				}

				if (mode) {
					mChkMsgTo.setChecked(checked);
				}
				
				// SET MEDIA 
				setImageViewUrl(item, mSmsToImage1, file1, mid, 1);
				setImageViewUrl(item, mSmsToImage2, file2, mid, 2);
				setImageViewUrl(item, mSmsToImage3, file3, mid, 3);
				
			}
		}

	}
	
	private String getImageFileName(final long mid, final int seq)
	{
		String homePath = mContextWrapper.getFilesDir().getPath();
		homePath = homePath + "/chat/media/";
		/*
		File file = new File(homePath);
	    if ( !file.exists() )
	    {
	        // 디렉토리가 존재하지 않으면 디렉토리 생성
	        file.mkdirs();
	    }*/
		String aFile = homePath + "media_" + String.valueOf(mid) + "_" + String.valueOf(seq) + ".jpg";
		return aFile;		
	}
	
	private void clearImageView(ImageView aImageView)
	{
		aImageView.setVisibility(View.GONE);
		BitmapDrawable draw = (BitmapDrawable)aImageView.getDrawable();
		if (draw != null)
		{
			Bitmap bmp = draw.getBitmap();
			if (bmp != null)
			{
				bmp.recycle();
			}
		}
		aImageView.setImageDrawable(null);
	}
	
	
	private void setImageViewUrl(final SmsMsgContsItem item, final ImageView aImageView, final String aUrl, final long mid, final int seq)
	{
		clearImageView(aImageView);
		
		if (aUrl != null && aUrl.length() > 0) 
		{
			// CHECK DOWNLOAD FILE
			final String aFile = getImageFileName(mid, seq);
			File f = new File(aFile);
			if (f.exists())
			{
				// LOAD FROM LOCAL FILE
				setImageViewFile(aImageView, aFile);
			}			
			else if (aUrl.startsWith("http"))
			{
				// DOWNLOAD FROM HTTP SERVER			
				ImageLoader imageLoader = new ImageLoader();
				imageLoader.loadImage(aImageView, aUrl, aFile, new ImageLoader.OnLoadingCompleteListener() {
		            @Override
		            public void onComplete(ImageView imageView, String save) {
		            	// save & display
		            	if (save == null || save.length() == 0)
		            	{
		            		// 다운로드 실패.
		                	File f = new File(aFile);
			            	if (f.exists())
			            	{
			            		f.delete();
			            	}
			            	return;
		            	}
		        		
		        		if (seq == 1)
		        			item.setFile1(aFile);
		        		else if (seq == 2)
		        			item.setFile2(aFile);
		        		else if (seq == 3)
		        			item.setFile3(aFile);
		            	
		            	setImageViewFile(aImageView, save);
		            }
		            @Override
		            public void onCancel(ImageView imageView) {
		            	File f = new File(aFile);
		            	if (f.exists())
		            	{
		            		f.delete();
		            	}
		            }
		        });
			}
		}		
	}
	
	private void setImageViewFile(final ImageView aImageView, final String aFile)
	{
		if (aFile == null || aFile.length() == 0)
		{
			clearImageView(aImageView);
			return;			
		}
		
		
		// LOAD FROM FILE
		try
		{
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(aFile, o);

			final int REQUIRED_WIDTH = 512;
			final int REQUIRED_HIGHT = 512;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
				scale *= 2;			
			
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inSampleSize = scale;
			
			boolean retry = false;
			Bitmap bmp = null;
			try
			{
				bmp = BitmapFactory.decodeFile(aFile, options);
			}
			catch (OutOfMemoryError e) 
			{
				retry = true;
				e.printStackTrace();
			}
			
			if (retry)
			{
				scale *= 2;
				options.inSampleSize = scale;
				bmp = BitmapFactory.decodeFile(aFile, options);
			}
			
			if (bmp != null)
			{
				aImageView.setImageBitmap(bmp);
				
				if (options.outWidth <= 176)
				{
					aImageView.setScaleType(ScaleType.FIT_CENTER);
					LayoutParams params = (LayoutParams) aImageView.getLayoutParams();
					params.width = options.outWidth * 2;
					params.height = options.outHeight * 2;
					aImageView.setLayoutParams(params);
				}
				
				aImageView.setVisibility(View.VISIBLE);
				// MWS 이미지버튼 포커스를 false
				aImageView.setFocusable(false);
				aImageView.setFocusableInTouchMode(false);
			}
			else
			{
				//aImageView.setImageResource(R.drawable.ic_menu_change_sender);
				//aImageView.setVisibility(View.VISIBLE);
				
				// 손상되어 있을 수 있으므로 삭제함.
	          	File f = new File(aFile);
            	if (f.exists())
            	{
            		f.delete();
            	}
			}
		} 
		catch (OutOfMemoryError e) 
		{
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		aImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(mContext, ImageViewer.class);
				intent.putExtra("file", aFile);
				intent.putExtra("number", mNumber);
				intent.putExtra("title", getResources().getString(R.string.msg_image));
				mContext.startActivity(intent);					
				
				/*
				File file = new File(aFile);
				Uri uri = Uri.fromFile(new File(aFile));
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(uri,"image/*");
				try
				{
					mContext.startActivity(intent);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				*/				
			}
		});		
		
	}
	
	private void PopupDialog(final int id, String name) {
		// custom dialog
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_msg_list);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.msg_detail_layout);

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});

		// set the custom dialog components - text, image and button
		TextView text_name = (TextView) dialog.findViewById(R.id.txt_name);
		
		if (name != null && name.length() > 16)
			name = name.substring(0, 16);

		if (name != null && name.length() > 0)
			text_name.setText(name);

		// 삭제
		ImageButton dialogButtonDelete = (ImageButton) dialog.findViewById(R.id.btn_popup_msg_delete);
		dialogButtonDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mAdapter != null)
				{
					mAdapter.delete(id);
					//BJH 문자 메시지 갱신
					Sms.updateDeleteList(mContext, mNumber);
					mAdapter.notifyDataSetChanged();
				}
				dialog.cancel();
			}
		});
		dialog.show();
	}

	//BJH 2016.09.26 음성 메시지 청취
	private void PopupVMSDialog(final String aUrl)
	{
		if (aUrl == null || aUrl.length() == 0)
			return;			
		
		final String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);
		
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_vms_list);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.msg_vms_layout);

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});

		// set the custom dialog components - text, image and button
		TextView text_name = (TextView) dialog.findViewById(R.id.txt_name);
		text_name.setText(mContext.getString(R.string.pns_vms_title));

		// 청취
		ImageButton dialogButtonListen = (ImageButton) dialog.findViewById(R.id.btn_popup_vms_listen);
		dialogButtonListen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String listenUrl = url + "/" + aUrl;
				Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(listenUrl), "audio/*");
				
				try
				{
					mContext.startActivity(intent);
				}
				catch (Exception e)
				{
					//
				}
				dialog.cancel();
			}
		});
		
		// 다운
		ImageButton dialogButtonDown = (ImageButton) dialog.findViewById(R.id.btn_popup_vms_down);
		dialogButtonDown.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String downUrl = url + "/" + aUrl;
				SmsMsgContsActivity.VMSDownload(mContext, downUrl);
				dialog.cancel();
			}
		});
		dialog.show();
	}
	
	//BJH 2016.10.11 사장님 요청 사항
	private void PopupWorkDialog(final int id, String name, final String clipText) {
		// custom dialog
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_msg_work);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.msg_detail_layout);

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});

		// set the custom dialog components - text, image and button
		TextView text_name = (TextView) dialog.findViewById(R.id.txt_name);
		
		if (name != null && name.length() > 16)
			name = name.substring(0, 16);

		if (name != null && name.length() > 0)
			text_name.setText(name);
		
		// 복사
		Button dialogButtonCopy = (Button) dialog.findViewById(R.id.btn_popup_msg_copy);
		dialogButtonCopy.setVisibility(View.VISIBLE);
		dialogButtonCopy.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClipboardManager clip = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                clip.setText(clipText);
				dialog.cancel();
			}
		});

		// 삭제
		Button dialogButtonDelete = (Button) dialog.findViewById(R.id.btn_popup_msg_delete);
		dialogButtonDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mAdapter != null)
				{
					mAdapter.delete(id);
					Sms.updateDeleteList(mContext,mNumber);
					mAdapter.notifyDataSetChanged();
				}
				dialog.cancel();
			}
		});
		dialog.show();
	}

	private void PopupImageDialog(final int id, String name, final String url) {
		// custom dialog
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_msg_work);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.msg_detail_layout);

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});

		// set the custom dialog components - text, image and button
		TextView text_name = (TextView) dialog.findViewById(R.id.txt_name);
		
		if (name != null && name.length() > 16)
			name = name.substring(0, 16);

		if (name != null && name.length() > 0)
			text_name.setText(name);

		// 삭제
		Button dialogButtonDelete = (Button) dialog.findViewById(R.id.btn_popup_msg_delete);
		dialogButtonDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mAdapter != null)
				{
					mAdapter.delete(id);
					Sms.updateDeleteList(mContext, mNumber);
					mAdapter.notifyDataSetChanged();
				}
				dialog.cancel();
			}
		});
		dialog.show();
	}

	private void dialogSelectVMS(final String aUrl){
		if (aUrl == null || aUrl.length() == 0)
			return;

		final String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REC_DIR);

		ArrayList<String> listMenu = new ArrayList<>();
		listMenu.add(mContext.getString(R.string.msg_vms_listen));
		listMenu.add(mContext.getString(R.string.msg_vms_download));

		final String[] menus = listMenu.toArray(new String[listMenu.size()]);

		String title=mContext.getString(R.string.pns_vms_title);

		AlertDialog.Builder adb = new AlertDialog.Builder(mContext, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
		adb.setTitle(title);
		adb.setItems(menus, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (menus[which].equals(mContext.getString(R.string.msg_vms_listen))){
					String listenUrl = url + "/" + aUrl;
					Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.parse(listenUrl), "audio/*");

					try
					{
						mContext.startActivity(intent);
					}
					catch (Exception e)
					{
						//
					}
					dialog.cancel();
				}
				else if (menus[which].equals(mContext.getString(R.string.msg_vms_download))){
					String downUrl = url + "/" + aUrl;
					SmsMsgContsActivity.VMSDownload(mContext, downUrl);
					dialog.cancel();
				}
			}
		});

		adb.show();
	}

	private void dialogSelectWorkDialog(final int id, String name, final String clipText) {
		ArrayList<String> listMenu = new ArrayList<>();
		listMenu.add(mContext.getString(R.string.copy));
		listMenu.add(mContext.getString(R.string.opt_menu_delete));

		final String[] menus = listMenu.toArray(new String[listMenu.size()]);

		String title=mContext.getString(R.string.select_work);

		AlertDialog.Builder adb = new AlertDialog.Builder(mContext, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
		adb.setTitle(title);
		adb.setItems(menus, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (menus[which].equals(mContext.getString(R.string.copy))){
					ClipboardManager clip = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
					clip.setText(clipText);
					dialog.cancel();
				}
				else if (menus[which].equals(mContext.getString(R.string.opt_menu_delete))){
					if (mAdapter != null)
					{
						mAdapter.delete(id);
						Sms.updateDeleteList(mContext,mNumber);
						mAdapter.notifyDataSetChanged();
					}
					dialog.cancel();
				}
			}
		});

		adb.show();
	}
	
}
