package com.dial070.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dial070.dao.DaoCategory;
import com.dial070.dao.DaoTrip;
import com.dial070.dao.Data;
import com.dial070.dao.UploadCategoryAnswersResponse;
import com.dial070.db.DBManager;
import com.dial070.db.TravelCategoryData;
import com.dial070.db.TravelStepData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.utils.RecyclerAdapter;
import com.dial070.utils.RetrofitExService;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class CategoryActivity extends Fragment implements SwipeRefreshLayout.OnRefreshListener, RecyclerAdapter.OnLoadMoreListener {
    public final static String BROADCAST_MESSAGE = "RefreshTabFragmentTravel";
    public final static String BROADCAST_LONG_MESSAGE = "PostDataTabFragmentTravel";
    public static int AsyncTaskMaxCount =11;
    private BroadcastReceiver mReceiver = null;
    final int REQUEST_CODE_PERMISSIONS = 1;
    final int REQUEST_CODE_PERMISSIONS_MORE = 2;
    private final String TAG = "CategoryActivity";
    private ArrayList<DaoCategory> mCategoryArrayList;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    private ProgressDialog progressDialog;
    private GetPhotoTask getPhotoTask;
    private GetCategotyTask getCategotyTask;
    private GetPhotoTaskMore getPhotoTaskMore;
    private Context mContext;
    private AppPrefs mPrefs;
    SwipeRefreshLayout swipeRefresh;
    private Date startDate;

    private String userID;
    DBManager database;

    boolean isLoadDB=false;
    public static int completeAsyncTaskCount=-1;
    private static boolean isRefresh=false;
    private TextView txtRequestPermission;
    private boolean isComplete=false;
    private TextView txtNodata;
    private AlertDialog msgDialogComplete;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        database = new DBManager(mContext);
        mPrefs = new AppPrefs(mContext);
        userID = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

        DebugLog.d("test_travel onCreate()<--");

        mCategoryArrayList = new ArrayList<>();

        postClickLogData("Travelphoto");

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tabView = null;
        if (this.getTag().equals("travel")) {
            tabView = setTabViewTravel(inflater);

            checkPermission();
        }
        return tabView;
    }

    @Override
    public void onResume() {
        super.onResume();

        registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver();
    }

    @Override
    public void onDestroy() {
        if (getPhotoTask != null && !getPhotoTask.isCancelled()) {
            getPhotoTask.cancel(true);
        }

        if (getPhotoTaskMore != null && !getPhotoTaskMore.isCancelled()) {
            getPhotoTaskMore.cancel(true);
        }

        if (getCategotyTask != null && !getCategotyTask.isCancelled()) {
            getCategotyTask.cancel(true);
        }

        if (database!=null&&database.isOpen()){
            database.close();
        }

        if (msgDialogComplete!=null && msgDialogComplete.isShowing()){
            msgDialogComplete.dismiss();
        }

        super.onDestroy();
    }

    private void postClickLogData(String type){
        HashMap<String, Object> input = new HashMap<>();
        input.put("param1", userID);
        input.put("param2", type);
        input.put("param3", "Android");

        DebugLog.d("test_travel postClickLogData--> param1: "+userID);
        DebugLog.d("test_travel postClickLogData--> param2: "+type);
        DebugLog.d("test_travel postClickLogData--> param3: Android");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitExService.URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitExService retrofitExService = retrofit.create(RetrofitExService.class);
        retrofitExService.postClickLogData(input).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.d(TAG, "onResponse:"+response.toString());
                try {
                    DebugLog.d("test_travel postClickLogData onResponse: "+response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (response.isSuccessful()) {
                    Log.d(TAG, "response code : "+response.code());
                    /*Log.d(TAG, "response.body() : "+response.body());
                    try {
                        Log.d(TAG, "response.body().string(): "+response.body().string() );
                    } catch (IOException e) {
                        Log.d(TAG, "error: "+e.getLocalizedMessage());
                    }*/

                            /*try {
                                Log.d(TAG, "response.body().getResponse().getCode() : "+response.body().getResponse().getCode());
                            }catch (Exception e){
                                Log.e(TAG,"error:"+e.getMessage());
                            }*/
                }else {
                    Log.d(TAG, "response code : "+response.code());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure");
            }
        });
    }


    private void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPermissionRead = mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            List<String> permissions = new ArrayList<String>();
            if (hasPermissionRead != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_PERMISSIONS);
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                }
            } else {
                if (mPrefs.getPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE).length()==0){
                    AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                    msgDialog.setTitle("알림");
                    msgDialog.setCancelable(false);
                    msgDialog.setMessage("사진정보에 접근하는 것을 동의하시겠습니까?");
                    msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                            new DialogInterface.OnClickListener() {
                                // @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    txtRequestPermission.setVisibility(View.GONE);
                                    swipeRefresh.setVisibility(View.VISIBLE);
                                    mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"1");
                                    loadCategory();
                                }
                            });
                    msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"");
                                    txtRequestPermission.setVisibility(View.VISIBLE);
                                    swipeRefresh.setVisibility(View.GONE);
                                }
                            });
                    msgDialog.show();
                }else {
                    loadCategory();
                }

            }
        } else {
            if (mPrefs.getPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE).length()==0){
                AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                msgDialog.setTitle("알림");
                msgDialog.setCancelable(false);
                msgDialog.setMessage("사진정보에 접근하는 것을 동의하시겠습니까?");
                msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                        new DialogInterface.OnClickListener() {
                            // @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                txtRequestPermission.setVisibility(View.GONE);
                                swipeRefresh.setVisibility(View.VISIBLE);
                                mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"1");
                                loadCategory();
                            }
                        });
                msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"");
                                txtRequestPermission.setVisibility(View.VISIBLE);
                                swipeRefresh.setVisibility(View.GONE);
                            }
                        });
                msgDialog.show();
            }else {
                loadCategory();
            }
        }
    }

    private void loadCategory(){
        Log.i(TAG,"loadCategory");
        DebugLog.d("test_travel loadCategory()<--");
        isRefresh=false;
        if (!database.isOpen()){
            database.open();
        }
        database.createTravelTable();

        Cursor cursor = database.getTravelCategoryData();
        DaoCategory category;
        mCategoryArrayList=new ArrayList<>();
        //Log.i(TAG,"cursor count:"+cursor.getCount());
        loading();
        if (cursor!=null && cursor.getCount() != 0) {
            DebugLog.d("test_travel loadCategory cursor is not null && count>0");
        //if (false) {
            isLoadDB=true;
            cursor.moveToFirst();
            do {
                String path=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_PATH));
                String country_name=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_COUNTRY_NAME));
                String country_code=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_COUNTRY_CODE));
                String start_date=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_STARTDATE));
                String end_date=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_ENDDATE));
                String category_num=cursor.getString(cursor.getColumnIndex(TravelCategoryData.FIELD_CATEGORY_NUM));
                int photoCount=cursor.getInt(cursor.getColumnIndex(TravelCategoryData.FIELD_PHOTO_COUNT));

                category=new DaoCategory();
                category.setFilepath1(path);
                category.setTitle(country_name);
                category.setCountryCode(country_code);
                category.setStartDate(start_date);
                category.setEndDate(end_date);
                category.setCategoryNum(category_num);
                category.setCount(photoCount);
                mCategoryArrayList.add(category);
            } while (cursor.moveToNext());
            cursor.close();
            if (mCategoryArrayList!=null && mCategoryArrayList.size()>0){
                loadData();

                String uploadedTime=mPrefs.getPreferenceStringValue(AppPrefs.CATEGORY_UPLOADED_DATETIME);
                Log.i(TAG,"uploadedTime:"+uploadedTime);
                if (uploadedTime.length()==0){
                    new UploadCategoryInfoTask().execute();
                }
            }

            loadingEnd();
        } else {
            DebugLog.d("test_travel loadCategory cursor is null or count==0");
            if (isComplete){
                txtNodata.setVisibility(View.VISIBLE);
                swipeRefresh.setVisibility(View.GONE);
                loadingEnd();
                return;
            }else {
                txtNodata.setVisibility(View.GONE);
                swipeRefresh.setVisibility(View.VISIBLE);
            }
            completeAsyncTaskCount=0;
            adapter.clearItems();
            /*getPhotoTask = new GetPhotoTask();
            getPhotoTask.execute();*/

            Date endDate = null;
            Date startDate = null;
            AsyncTaskMaxCount=11;
//            AsyncTaskMaxCount=1;
            Log.i(TAG,"AsyncTaskMaxCount:"+AsyncTaskMaxCount);

            for (int i = 0; i< AsyncTaskMaxCount; i++){
                if (endDate==null){
                    endDate = new Date();
                }else {
                    endDate = startDate;
                }

                startDate = getPreviousYear(endDate);

                DebugLog.d("test_travel in for loop startDate: "+startDate.toString());
                DebugLog.d("test_travel in for loop endDate  : "+endDate.toString());

                Long[] time=new Long[2];
                time[0]=startDate.getTime();
                time[1]=endDate.getTime();

                /*try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }*/
                getCategotyTask=new GetCategotyTask();
                getCategotyTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,time);
            }


        }
    }

    private void refresh(){
        isComplete=false;
        if (completeAsyncTaskCount==-1 || completeAsyncTaskCount==AsyncTaskMaxCount){
            if (adapter.getAllItems()!=null && adapter.getAllItems().size()>0){
                if (!database.isOpen()){
                    database.open();
                }
                database.createTravelTable();

                loading();

                String origin_date=mPrefs.getPreferenceStringValue(AppPrefs.CATEGORY_LOADED_DATE);
                Log.i(TAG,"origin_date:"+origin_date);

                if (origin_date.length()>0){
                    completeAsyncTaskCount=0;

                    Date endDate = null;
                    Date startDate = null;
                    AsyncTaskMaxCount=1;
                    isRefresh=true;
                    for (int i = 0; i< AsyncTaskMaxCount; i++){
                        if (endDate==null){
                            endDate = new Date();
                        }else {
                            endDate = startDate;
                        }

                        SimpleDateFormat sFormatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                        sFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                        try {
                            startDate = sFormatter.parse(origin_date);
                        } catch (ParseException e) {
                            Log.e(TAG,"error:"+e.getLocalizedMessage());
                            startDate = getPreviousYear(endDate);
                        }
                        //startDate = getPreviousYear(endDate);

                        Long[] time=new Long[2];
                        time[0]=startDate.getTime();
                        time[1]=endDate.getTime();
                        getCategotyTask=new GetCategotyTask();
                        getCategotyTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,time);
                    }
                }


                /*Cursor cursor = database.getTravelCategoryRecentLastCategoryNumData();
                if (cursor!=null && cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    String category_num=cursor.getString(0);
                    Log.i(TAG,"category_num:"+category_num);
                    Cursor cursorStep = database.getTravelStepRecentLastData(category_num);
                    if (cursorStep!=null && cursorStep.getCount() != 0) {
                        isRefresh=true;
                        cursorStep.moveToFirst();
                        String origin_date=cursorStep.getString(cursorStep.getColumnIndex(TravelStepData.FIELD_DATE));
                        Log.i(TAG,"origin_date:"+origin_date);

                        completeAsyncTaskCount=0;

                        Date endDate = null;
                        Date startDate = null;
                        AsyncTaskMaxCount=1;
                        for (int i = 0; i< AsyncTaskMaxCount; i++){
                            if (endDate==null){
                                endDate = new Date();
                            }else {
                                endDate = startDate;
                            }

                            SimpleDateFormat sFormatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                            sFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                            try {
                                startDate = sFormatter.parse(origin_date);
                            } catch (ParseException e) {
                                Log.e(TAG,"error:"+e.getLocalizedMessage());
                                startDate = getPreviousYear(endDate);
                            }
                            //startDate = getPreviousYear(endDate);

                            Long[] time=new Long[2];
                            time[0]=startDate.getTime();
                            time[1]=endDate.getTime();
                            getCategotyTask=new GetCategotyTask();
                            getCategotyTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,time);
                        }
                    }
                }


                 */
            }else{
                checkPermission();
            }

        }else {
            Toast.makeText(mContext,"이미지를 불러오는 중에는 해당 기능을 이용할 수 없습니다.",Toast.LENGTH_SHORT).show();
        }

    }

    private View setTabViewTravel(LayoutInflater inflater) {
        View tabView = inflater.inflate(R.layout.activity_trip, null);

        ImageButton btnEvent = tabView.findViewById(R.id.imgBtnEvent);
        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/74ho6KJFmp0"));
                startActivity(intent);
            }
        });

        Button btnRegister = tabView.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                Intent intent = new Intent(mContext, WebClient.class);
                intent.putExtra("url", "http://211.43.13.195:4001/user/?name=" + uid);
                intent.putExtra("title", "여행의 발자취");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP /*| Intent.FLAG_ACTIVITY_NO_ANIMATION*/);
                startActivity(intent);
            }
        });

        txtRequestPermission=tabView.findViewById(R.id.txtRequestPermission);
        txtNodata=tabView.findViewById(R.id.txtNodata);
        swipeRefresh = tabView.findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this);
        recyclerView = tabView.findViewById(R.id.listTrip);
        RecyclerDecoration spaceDecoration = new RecyclerDecoration(20);
        recyclerView.addItemDecoration(spaceDecoration);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(mContext, this);
        recyclerView.setAdapter(adapter);

        adapter.setLinearLayoutManager(layoutManager);
        adapter.setRecyclerView(recyclerView);

        txtNodata.setVisibility(View.GONE);
        if (mPrefs.getPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE).length()==0){
            txtRequestPermission.setVisibility(View.VISIBLE);
            swipeRefresh.setVisibility(View.GONE);
        }else {
            txtRequestPermission.setVisibility(View.GONE);
            swipeRefresh.setVisibility(View.VISIBLE);
        }


        txtRequestPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPrefs.getPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE).length()==0){
                    AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                    msgDialog.setTitle("알림");
                    msgDialog.setCancelable(false);
                    msgDialog.setMessage("사진정보에 접근하는 것을 동의하시겠습니까?");
                    msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                            new DialogInterface.OnClickListener() {
                                // @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    txtRequestPermission.setVisibility(View.GONE);
                                    swipeRefresh.setVisibility(View.VISIBLE);
                                    mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"1");
                                    loadCategory();
                                }
                            });
                    msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mPrefs.setPreferenceStringValue(AppPrefs.PHOTO_ACCESS_AGREE,"");
                                    txtRequestPermission.setVisibility(View.VISIBLE);
                                    swipeRefresh.setVisibility(View.GONE);
                                }
                            });
                    msgDialog.show();
                }else {
                    loadCategory();
                }
            }
        });

        return tabView;
    }

    /*@Override
    protected void onStart() {
        if (recyclerView!=null && adapter!=null){
            if (recyclerView.getVisibility()!=View.VISIBLE){
                adapter.notifyDataSetChanged();
                recyclerView.setVisibility(View.VISIBLE);
            }
        }

        super.onStart();
    }

    @Override
    protected void onStop() {
        if (recyclerView!=null){
            recyclerView.setVisibility(View.INVISIBLE);
        }

        super.onStop();
    }*/

    private ArrayList<DaoCategory> getCategory(String sStartDate, String sEndDate) {
        DebugLog.d("test_travel getCategory()<--");
        ArrayList<String> result = new ArrayList<>();
//        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

//        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};
        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.DATE_TAKEN
        };

        String sortOrder = MediaStore.Files.FileColumns.DATE_TAKEN+" DESC";

        int j = 1;
        int count = 0;
        ArrayList<DaoTrip> photoArrayList;
        Date endDate = new Date();
        Date startDate = new Date();

        do {
            if (j == 1) {
                endDate = new Date();
            } else {
                endDate = startDate;
            }

            long startTime=System.currentTimeMillis();
            Log.i(TAG,"##### "+j+" start #####");
            Log.i(TAG,"cursor start time:"+startTime);

            startDate = getPreviousYear(endDate);

            Log.i(TAG, "startDate:" + startDate.toString());

//            Cursor cursor = mContext.getContentResolver().query(uri, projection, MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?", new String[]{"" + startDate.getTime(), "" + endDate.getTime()}, MediaStore.Images.ImageColumns.DATE_TAKEN + " desc");

            Cursor cursor = mContext.getContentResolver()
                    .query(
                            uri,
                            projection,
                            MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?",
                            new String[]{"" + startDate, "" + endDate},
                            sortOrder
                    );

//            Cursor cursor = mContext.getContentResolver()
//                    .query(
//                            uri,
//                            projection,
//                            null,
//                            null,
//                            sortOrder
//                    );

//            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
//            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID);
            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int lastIndex;
            while (cursor.moveToNext()) {
                String absolutePathOfImage = cursor.getString(columnIndex);
                String nameOfFile = cursor.getString(columnDisplayname);
                String dateTake = cursor.getString(columnDateTaken);

                if (!TextUtils.isEmpty(absolutePathOfImage)) {
                    result.add(absolutePathOfImage);
                }

//                lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
//                lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;
//
//                if (!TextUtils.isEmpty(absolutePathOfImage)) {
//                    result.add(absolutePathOfImage);
//                }
            }

            Log.i(TAG,"cursor count:"+cursor.getCount());
            long endTime=System.currentTimeMillis();
            Log.i(TAG,"cursor end time:"+endTime);
            Log.i(TAG,"cursor loading duration:"+(endTime-startTime)/1000);
            Log.i(TAG,"##### "+j+" end #####");

            cursor.close();

            photoArrayList = new ArrayList<>();
            for (String string : result) {
                //Log.i(TAG,"fileName:"+string);

                if (string != null && string.trim().length() > 0) {
                    DaoTrip daoTravelPhoto = getPhotoMetaData(string);
                    if (daoTravelPhoto!=null){
                        Log.i(TAG, "daoTravelPhoto getCountryCode:" + daoTravelPhoto.getCountryCode());
                        photoArrayList.add(daoTravelPhoto);
                    }
                }
            }
            count = photoArrayList.size();
            Log.i(TAG, "photoArrayList size:" + count);
            j++;
            if (j == AsyncTaskMaxCount) {
                break;
            }
            this.startDate=startDate;
        } while (count == 0);


        /*if (photoArrayList.size() > 0) {
            Collections.sort(photoArrayList, new Comparator<DaoTrip>() {
                @Override
                public int compare(DaoTrip p1, DaoTrip p2) {
                    return p2.getDatetime().compareTo(p1.getDatetime());
                }
            });
        }*/


        ArrayList<DaoCategory> categoryArrayList = new ArrayList<>();
        String prevCountryState = "";
        DaoCategory daoCategory;
        int size = photoArrayList.size();
        String categoryNum="";
        TravelStepData data;
        for (int i = 0; i < size; i++) {
            DaoTrip daoTrip = photoArrayList.get(i);
            daoCategory = new DaoCategory();
            if (daoTrip.getLocation_country_state().equals(prevCountryState)) {
                DaoCategory prevDaoCategory = categoryArrayList.get(categoryArrayList.size() - 1);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                daoCategory.setTitle(prevCountryState);
                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                daoCategory.setEndDate(daoTrip.getDate());
                categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);

                /*Log.i(TAG,"category prevCountryState:"+prevCountryState);
                Log.i(TAG,"category getTitle:"+daoCategory.getTitle());
                Log.i(TAG,"category getFilepath1:"+daoCategory.getFilepath1());
                Log.i(TAG,"category getEndDate:"+daoCategory.getEndDate());*/
            } else {
                categoryNum=userID+"_"+daoTrip.getCountryCode()+"_"+daoTrip.getDate();

                prevCountryState = daoTrip.getLocation_country_state();
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(daoTrip.getCountryCode());
                daoCategory.setFilepath1(daoTrip.getFilepath());
                daoCategory.setStartDate(daoTrip.getDate());
                daoCategory.setEndDate(daoTrip.getDate());
                daoCategory.setCategoryNum(categoryNum);
                categoryArrayList.add(daoCategory);
            }

            try {
                data=new TravelStepData();
                data.setPath(daoTrip.getFilepath());
//                data.setUri_path(getStringUriFromPath(daoTrip.getFilepath()));
                data.setUri_path(daoTrip.getFilepath());
                data.setAddress(daoTrip.getAddress());
                data.setCountry_name(daoTrip.getLocation_country());
                data.setCountry_code(daoTrip.getCountryCode());
                data.setLatitude(daoTrip.getLatitude());
                data.setLongitude(daoTrip.getLongitude());
                data.setOrigin_date(daoTrip.getDatetime());
                data.setSimple_date(daoTrip.getDate());
                data.setCategory_num(categoryNum);
                Log.i(TAG,"daoTrip country:"+daoTrip.getLocation_country());
                Log.i(TAG,"daoTrip path:"+daoTrip.getFilepath());
                Log.i(TAG,"daoTrip Category_num:"+categoryNum);
                database.insertTravelStep(data);
            }
            catch(SQLException e)
            {
                Log.e(TAG,"error:"+e.getLocalizedMessage());
            }

        }

        /*size = categoryArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoCategory category = categoryArrayList.get(i);
            String imgArray[] = category.getFilepath1().split("######");
            if (imgArray.length > 3) {
                category.setRandomNumber(randomNumber(imgArray.length));
                categoryArrayList.set(i, category);
            }
        }*/

        /*for (DaoCategory category:categoryArrayList){
            Log.i(TAG,"category title:"+category.getTitle());
            Log.i(TAG,"category filepath:"+category.getFilepath1());
            Log.i(TAG,"category start:"+category.getStartDate());
            Log.i(TAG,"category end:"+category.getEndDate());
        }*/

        /*Collection<DaoTrip> dataPointsCalledJohn =
                Collections2.filter(photoArrayList, countryStateEqualsTo("대한민국 대전광역시"));

        ArrayList<DaoTrip> arrayList=new ArrayList<>(dataPointsCalledJohn);
        for (DaoTrip daoTrip:arrayList){
            Log.i(TAG,"daoTrip:"+daoTrip.getAddress());
        }*/

        return categoryArrayList;
    }

    private ArrayList<DaoCategory> getCategoryMore() {
        ArrayList<String> result = new ArrayList<>();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};
        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.DATE_TAKEN
        };

        String sortOrder = MediaStore.Files.FileColumns.DATE_TAKEN+" DESC";

        int j = 1;
        int count = 0;
        ArrayList<DaoTrip> photoArrayList=new ArrayList<>();
        Date endDate = this.startDate;
        Date startDate2 = this.startDate;
        do {
            if (j == 1) {
                endDate = this.startDate;
            } else {
                endDate = startDate2;
            }

            startDate2 = getPreviousYear(endDate);

            Log.i(TAG, "startDate2:" + startDate2.toString());

            if (startDate2.getTime()<1260434943742.0f){
                break;
            }

            long startTime=System.currentTimeMillis();
            Log.i(TAG,"##### "+j+" more start #####");
            Log.i(TAG,"cursor start time:"+startTime);

//            Cursor cursor = mContext.getContentResolver().query(uri, projection, MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?", new String[]{"" + startDate2.getTime(), "" + endDate.getTime()}, MediaStore.Images.ImageColumns.DATE_TAKEN + " desc");

            Cursor cursor = mContext.getContentResolver()
                    .query(
                            uri,
                            projection,
                            MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?",
                            new String[]{"" + startDate, "" + endDate},
                            sortOrder
                    );

//            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
//            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns._ID);
            int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
            int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

            int lastIndex;
            while (cursor.moveToNext()) {
                String absolutePathOfImage = cursor.getString(columnIndex);
                String nameOfFile = cursor.getString(columnDisplayname);
                String dateTake = cursor.getString(columnDateTaken);

//                lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
//                lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;

                if (!TextUtils.isEmpty(absolutePathOfImage)) {
                    result.add(absolutePathOfImage);
                }
            }

            Log.i(TAG,"cursor count:"+cursor.getCount());
            long endTime=System.currentTimeMillis();
            Log.i(TAG,"cursor end time:"+endTime);
            Log.i(TAG,"cursor loading duration:"+(endTime-startTime)/1000);
            Log.i(TAG,"##### "+j+" more end #####");

            cursor.close();

            photoArrayList = new ArrayList<>();
            for (String string : result) {
                //Log.i(TAG,"fileName:"+string);
                if (string != null && string.trim().length() > 0) {
                    DaoTrip daoTravelPhoto = getPhotoMetaData(string);
                    if (daoTravelPhoto != null) {
                        //Log.i(TAG,"daoTravelPhoto getLocation_country_state:"+daoTravelPhoto.getLocation_country_state());
                        //Log.i(TAG,"daoTravelPhoto getFilepath1:"+daoTravelPhoto.getFilepath1());
                        if (daoTravelPhoto!=null){
                            Log.i(TAG, "daoTravelPhoto getCountryCode:" + daoTravelPhoto.getCountryCode());
                            photoArrayList.add(daoTravelPhoto);
                        }
                    }
                }
            }
            count = photoArrayList.size();
            Log.i(TAG, "photoArrayList size:" + count);
            j++;
            if (j == AsyncTaskMaxCount) {
                break;
            }
            this.startDate=startDate2;
        } while (count == 0);


        /*if (photoArrayList.size() > 0) {
            Collections.sort(photoArrayList, new Comparator<DaoTrip>() {
                @Override
                public int compare(DaoTrip p1, DaoTrip p2) {
                    return p2.getDatetime().compareTo(p1.getDatetime());
                }
            });
        }*/


        ArrayList<DaoCategory> categoryArrayList = new ArrayList<>();
        String prevCountryState = "";
        DaoCategory daoCategory;
        String categoryNum="";
        TravelStepData data;
        int size = photoArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoTrip daoTrip = photoArrayList.get(i);
            daoCategory = new DaoCategory();
            if (daoTrip.getLocation_country_state().equals(prevCountryState)) {
                DaoCategory prevDaoCategory = categoryArrayList.get(categoryArrayList.size() - 1);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                daoCategory.setEndDate(daoTrip.getDate());
                categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);

                /*Log.i(TAG,"category prevCountryState:"+prevCountryState);
                Log.i(TAG,"category getTitle:"+daoCategory.getTitle());
                Log.i(TAG,"category getFilepath1:"+daoCategory.getFilepath1());
                Log.i(TAG,"category getEndDate:"+daoCategory.getEndDate());*/
            } else {
                categoryNum=userID+"_"+daoTrip.getCountryCode()+"_"+daoTrip.getDate();

                prevCountryState = daoTrip.getLocation_country_state();
                daoCategory.setTitle(prevCountryState);
                daoCategory.setCountryCode(daoTrip.getCountryCode());
                daoCategory.setFilepath1(daoTrip.getFilepath());
                daoCategory.setStartDate(daoTrip.getDate());
                daoCategory.setEndDate(daoTrip.getDate());
                daoCategory.setCategoryNum(categoryNum);
                categoryArrayList.add(daoCategory);
            }

            try {
                data=new TravelStepData();
                data.setPath(daoTrip.getFilepath());
//                data.setUri_path(getStringUriFromPath(daoTrip.getFilepath()));
                data.setUri_path(daoTrip.getFilepath());
                data.setAddress(daoTrip.getAddress());
                data.setCountry_name(daoTrip.getLocation_country());
                data.setCountry_code(daoTrip.getCountryCode());
                data.setLatitude(daoTrip.getLatitude());
                data.setLongitude(daoTrip.getLongitude());
                data.setOrigin_date(daoTrip.getDatetime());
                data.setSimple_date(daoTrip.getDate());
                data.setCategory_num(categoryNum);
                Log.i(TAG,"daoTrip country:"+daoTrip.getLocation_country());
                Log.i(TAG,"daoTrip path:"+daoTrip.getFilepath());
                Log.i(TAG,"daoTrip uripath:"+getStringUriFromPath(daoTrip.getFilepath()));
                database.insertTravelStep(data);
            }
            catch(SQLException e)
            {
                Log.e(TAG,"error:"+e.getLocalizedMessage());
            }
        }


        /*size = categoryArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoCategory category = categoryArrayList.get(i);
            String imgArray[] = category.getFilepath1().split("######");
            if (imgArray.length > 3) {
                category.setRandomNumber(randomNumber(imgArray.length));
                categoryArrayList.set(i, category);
            }
        }*/

        /*for (DaoCategory category:categoryArrayList){
            Log.i(TAG,"category title:"+category.getTitle());
            Log.i(TAG,"category filepath:"+category.getFilepath1());
            Log.i(TAG,"category start:"+category.getStartDate());
            Log.i(TAG,"category end:"+category.getEndDate());
        }*/

        /*Collection<DaoTrip> dataPointsCalledJohn =
                Collections2.filter(photoArrayList, countryStateEqualsTo("대한민국 대전광역시"));

        ArrayList<DaoTrip> arrayList=new ArrayList<>(dataPointsCalledJohn);
        for (DaoTrip daoTrip:arrayList){
            Log.i(TAG,"daoTrip:"+daoTrip.getAddress());
        }*/


        return categoryArrayList;
    }

    private ArrayList<DaoCategory> getCategoryNew(long startDate, long endDate) {
        DebugLog.d("test_travel getCategoryNew()<-------------------------------------------------");

        ArrayList<String> result = new ArrayList<>();
//        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN};
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.DATE_TAKEN
        };

        String sortOrder = MediaStore.Files.FileColumns.DATE_TAKEN+" DESC";

        int count = 0;
        ArrayList<DaoTrip> photoArrayList;
        Log.i(TAG, "startDate:" + new Date(startDate));
        Log.i(TAG, "endDate:" + new Date(endDate));

//        Cursor cursor = mContext.getContentResolver()
//                .query(
//                        uri,
//                        projection,
//                        MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?",
//                        new String[]{"" + startDate, "" + endDate},
//                        MediaStore.Images.ImageColumns.DATE_TAKEN + " asc"
//                );

        Cursor cursor = mContext.getContentResolver()
                .query(
                        uri,
                        projection,
                        MediaStore.Images.ImageColumns.DATE_TAKEN + ">? and " + MediaStore.Images.ImageColumns.DATE_TAKEN + "<=?",
                        new String[]{"" + startDate, "" + endDate},
                        sortOrder
                );

//        Cursor cursor = mContext.getContentResolver()
//                .query(
//                        uri,
//                        null,
//                        null,
//                        null,
//                        null
//                );

        DebugLog.d("test_travel getCategoryNew cursor size: "+cursor.getCount());

//        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
//        int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATE_TAKEN);

        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns._ID);
        int columnDisplayname = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATE_TAKEN);
        int columnDateTaken = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME);

        int columnTest = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);

        int lastIndex;
        while (cursor.moveToNext()) {
            String absolutePathOfImage = cursor.getString(columnIndex);
            String nameOfFile = cursor.getString(columnDisplayname);
            String dateTake = cursor.getString(columnDateTaken);

            String testPath = cursor.getString(columnTest);

            DebugLog.d("test_travel imagePath  : "+absolutePathOfImage);
            DebugLog.d("test_travel displayName: "+nameOfFile);
            DebugLog.d("test_travel storagePath: "+testPath);

            if (!TextUtils.isEmpty(absolutePathOfImage)) {
                result.add(absolutePathOfImage);
            }

//            lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
//            lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;
//
//            if (!TextUtils.isEmpty(absolutePathOfImage)) {
//                result.add(absolutePathOfImage);
//            }
        }
        cursor.close();

        DebugLog.d("test_travel =================================");
        DebugLog.d("test_travel result size: "+result.size());

        for(String tempUri: result) {
            DebugLog.d("test_travel tempUri: "+tempUri);
        }

        //photoArrayList = new ArrayList<>();

        ArrayList<DaoCategory> categoryArrayList = new ArrayList<>();
        String prevCountryState = "";
        DaoCategory daoCategory;
        String categoryNum="";
        TravelStepData data;

        int listCount=0;
        int sResultSize=result.size();
        boolean isKorea=false;
        boolean prevIsKorea = false;
        for (int i=0;i<sResultSize;i++) {
            String string=result.get(i);
            //Log.i(TAG,"fileName:"+string);
            if (getCategotyTask.isCancelled()){
                DebugLog.d("test_travel ---- STOP TASK CANCELED ----");
                break;

            }else {
                DebugLog.d("test_travel ---- ALIVE ----");
                DaoTrip daoTrip = getPhotoMetaData(string);
                if (daoTrip!=null){
                    Log.i(TAG, "daoTravelPhoto getCountryCode:" + daoTrip.getCountryCode());
                    Log.i(TAG, "daoTravelPhoto getFilepath:" + daoTrip.getFilepath());

                    DebugLog.d("test_travel daoTravelPhoto getCountryCode: "+daoTrip.getCountryCode());
                    DebugLog.d("test_travel daoTravelPhoto getFilePath   : "+daoTrip.getFilepath());

                    //photoArrayList.add(daoTrip);
                    isKorea=daoTrip.isKorea();
                    if (isKorea){
                        DebugLog.d("test_travel COND.0 true ==== ");
                        prevIsKorea=true;
                    }else {
                        DebugLog.d("test_travel COND.0 false ==== ");
                        if (isRefresh && i==0){
                            DaoCategory prevCategory=adapter.getAllItems().get(0);
                            if (daoTrip.getLocation_country_state().equals(prevCategory.getTitle())){
                                prevCountryState = prevCategory.getTitle();
                            }
                        }
                        daoCategory = new DaoCategory();
                        if (daoTrip.getLocation_country_state().equals(prevCountryState) && !prevIsKorea) {
                            DebugLog.d("test_travel COND.1 ==== ");

                            DaoCategory prevDaoCategory;
                            if (isRefresh && i==0){
                                prevDaoCategory = adapter.getAllItems().get(0);
                                categoryNum=prevDaoCategory.getCategoryNum();
                                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                                daoCategory.setTitle(prevCountryState);
                                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                                daoCategory.setEndDate(daoTrip.getDate());
                                daoCategory.setCount(prevDaoCategory.getCount()+1);
                                //categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);
                                categoryArrayList.add(daoCategory);
                            }else {
                                prevDaoCategory = categoryArrayList.get(categoryArrayList.size() - 1);
                                daoCategory.setCountryCode(prevDaoCategory.getCountryCode());
                                daoCategory.setCategoryNum(prevDaoCategory.getCategoryNum());
                                daoCategory.setTitle(prevCountryState);
                                daoCategory.setFilepath1(prevDaoCategory.getFilepath1() + "######" + daoTrip.getFilepath());
                                daoCategory.setStartDate(prevDaoCategory.getStartDate());
                                daoCategory.setEndDate(daoTrip.getDate());
                                daoCategory.setCount(prevDaoCategory.getCount()+1);
                                categoryArrayList.set(categoryArrayList.size() - 1, daoCategory);
                            }
                        } else {
                            DebugLog.d("test_travel COND.2 ==== ");

                            prevIsKorea=false;

                            categoryNum=userID+"_"+daoTrip.getCountryCode()+"_"+daoTrip.getCategoryDate();

                            prevCountryState = daoTrip.getLocation_country_state();
                            daoCategory.setTitle(prevCountryState);
                            daoCategory.setCountryCode(daoTrip.getCountryCode());
                            daoCategory.setFilepath1(daoTrip.getFilepath());
                            daoCategory.setStartDate(daoTrip.getDate());
                            daoCategory.setEndDate(daoTrip.getDate());
                            daoCategory.setCategoryNum(categoryNum);
                            daoCategory.setCount(1);
                            categoryArrayList.add(daoCategory);
                        }

                        try {
                            DebugLog.d("test_travel COND.3 create Step Data ==== ");

                            data=new TravelStepData();
                            DebugLog.d("test_travel COND.3-1 create Step Data ==== ");
                            data.setPath(daoTrip.getFilepath());
                            DebugLog.d("test_travel COND.3-2 create Step Data ==== ");
//                            data.setUri_path(getStringUriFromPath(daoTrip.getFilepath()));
                            data.setUri_path(daoTrip.getFilepath());
                            DebugLog.d("test_travel COND.3-3 create Step Data ==== ");
                            data.setAddress(daoTrip.getAddress());
                            DebugLog.d("test_travel COND.3-4 create Step Data ==== ");
                            data.setCountry_name(daoTrip.getLocation_country());
                            DebugLog.d("test_travel COND.3-5 create Step Data ==== ");
                            data.setCountry_code(daoTrip.getCountryCode());
                            DebugLog.d("test_travel COND.3-6 create Step Data ==== ");
                            data.setLatitude(daoTrip.getLatitude());
                            DebugLog.d("test_travel COND.3-7 create Step Data ==== ");
                            data.setLongitude(daoTrip.getLongitude());
                            DebugLog.d("test_travel COND.3-8 create Step Data ==== ");
                            data.setOrigin_date(daoTrip.getDatetime());
                            DebugLog.d("test_travel COND.3-9 create Step Data ==== ");
                            data.setSimple_date(daoTrip.getDate());
                            DebugLog.d("test_travel COND.3-10 create Step Data ==== ");
                            data.setCategory_num(categoryNum);

                            DebugLog.d("test_travel daoTrip country: "+daoTrip.getLocation_country());
                            DebugLog.d("test_travel daoTrip path   : "+daoTrip.getFilepath());
                            DebugLog.d("test_travel daoTrip catNum : "+categoryNum);

                            if (getCategotyTask.isCancelled()){
                                break;
                            }
                            long resultRetrun=database.insertTravelStep(data);

                            DebugLog.d("test_travel insertTravelStep result: "+resultRetrun);

                            if (resultRetrun!=-1){
                                adapter.addItemOnly(daoCategory);

                                if (daoCategory.getCount()>=3){
                                    if ((daoCategory.getCount()==3 || daoCategory.getCount()%10==0) && !getCategotyTask.isCancelled()){
                                        getActivity().runOnUiThread(getCategotyTask.changeUI);
                                    }
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            Log.e(TAG,"error:"+e.getLocalizedMessage());
                        }
                    }


                } else {
                    DebugLog.d("test_travel ---- STOP DAO TRIP IS NULL ----");
                }
            }
        }

        int count2=categoryArrayList.size();
        DebugLog.d("test_travel category size: "+count2);

        // test
//        if (count2>0 && !getCategotyTask.isCancelled()){
//            for (int i = 0; i < count2; i++) {
//                if (getCategotyTask.isCancelled())
//                    break;
//                DebugLog.d("test_travel category idx: "+i);
//
//                DaoCategory category=categoryArrayList.get(i);
//                if (database!=null && database.isOpen()){
//                    Cursor cursor1= database.getTravelStepRecentLastData(category.getCategoryNum());
//                    if (cursor1==null || cursor1.getCount()==0){
//                        categoryArrayList.remove(i);
//                    }
//                    cursor1.close();
//                }
//            }
//        }

        /*count = photoArrayList.size();
        Log.i(TAG, "photoArrayList size:" + count);*/


        /*if (photoArrayList.size() > 0) {
            Collections.sort(photoArrayList, new Comparator<DaoTrip>() {
                @Override
                public int compare(DaoTrip p1, DaoTrip p2) {
                    return p2.getDatetime().compareTo(p1.getDatetime());
                }
            });
        }*/

        /*size = categoryArrayList.size();
        for (int i = 0; i < size; i++) {
            DaoCategory category = categoryArrayList.get(i);
            String imgArray[] = category.getFilepath1().split("######");
            if (imgArray.length > 3) {
                category.setRandomNumber(randomNumber(imgArray.length));
                categoryArrayList.set(i, category);
            }
        }*/

        /*for (DaoCategory category:categoryArrayList){
            Log.i(TAG,"category title:"+category.getTitle());
            Log.i(TAG,"category filepath:"+category.getFilepath1());
            Log.i(TAG,"category start:"+category.getStartDate());
            Log.i(TAG,"category end:"+category.getEndDate());
        }*/

        /*Collection<DaoTrip> dataPointsCalledJohn =
                Collections2.filter(photoArrayList, countryStateEqualsTo("대한민국 대전광역시"));

        ArrayList<DaoTrip> arrayList=new ArrayList<>(dataPointsCalledJohn);
        for (DaoTrip daoTrip:arrayList){
            Log.i(TAG,"daoTrip:"+daoTrip.getAddress());
        }*/

        if (getCategotyTask.isCancelled()) {
            categoryArrayList=null;
        }

        DebugLog.d("test_travel getCategoryNew()<-------------------------------------------------");

        return categoryArrayList;
    }

    private DaoTrip getPhotoMetaData(String file) {
        /*long startTime=System.currentTimeMillis();
        Log.i(TAG,"##### getPhotoMetaData start #####");
        Log.i(TAG,"getPhotoMetaData start time:"+startTime);*/
        DebugLog.d("test_travel travel_meta getPhotoMetaData()<-- path: "+file);

        DaoTrip daoTravelPhoto = null;
        String tempFileUri = file;
        Uri photoUri = null;
        try {
            ExifInterface exifInterface = null;

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q) {
                photoUri = Uri.withAppendedPath(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        file
                );
                photoUri = MediaStore.setRequireOriginal(photoUri);
                InputStream is = getContext().getContentResolver().openInputStream(photoUri);

                DebugLog.d("test_travel travel_meta VERSION OVER Q photoUri: "+photoUri.toString());
                tempFileUri = photoUri.toString();

                if(is!=null) {
                    exifInterface = new ExifInterface(is);
                    is.close();
                }

            } else {
                exifInterface = new ExifInterface(file);
            }

//            ExifInterface exifInterface = new ExifInterface(file);

            if (exifInterface != null) {
                DebugLog.d("test_travel travel_meta ---- ALIVE EXIF IS EXIST  ----");

                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
                String datetime = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);

                DebugLog.d("test_travel travel_meta --> uri           : "+tempFileUri);
                DebugLog.d("test_travel travel_meta --> latitude      : "+latValue);
                DebugLog.d("test_travel travel_meta --> latitude Ref  : "+latRef);
                DebugLog.d("test_travel travel_meta --> longitude     : "+lngValue);
                DebugLog.d("test_travel travel_meta --> longitude Ref : "+lngRef);
                DebugLog.d("test_travel travel_meta --> dateTime      : "+datetime);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null && datetime != null) {
                    DebugLog.d("test_travel travel_meta ---- ALIVE CONDITION TRUE ---- ");

                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);

                    SimpleDateFormat sFormatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                    sFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                    SimpleDateFormat convertFormat = new SimpleDateFormat("yyyy.MM.dd");
                    convertFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                    SimpleDateFormat categoryFormat = new SimpleDateFormat("yyyyMMdd");
                    categoryFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

                    //Log.i(TAG, "latitude:" + latitude);
                    //Log.i(TAG, "longitude:" + longitude);
                    if ((latitude < 33.11111111 || latitude > 43.00972222) || (longitude < 124.19583333 || longitude > 131.87222222)) {

                        DaoTrip daoTravelPhotoAddressInfo = convertToAddress(latitude, longitude);
                        if (daoTravelPhotoAddressInfo != null && daoTravelPhotoAddressInfo.getLocation_country() !=null) {
                            if (daoTravelPhotoAddressInfo.getAddress() != null) {
                                daoTravelPhoto = new DaoTrip();
                                if(photoUri!=null) {
                                    daoTravelPhoto.setFilepath(photoUri.toString());
                                } else {
                                    daoTravelPhoto.setFilepath(file);
                                }
                                daoTravelPhoto.setDatetime(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                                daoTravelPhoto.setLatitude(daoTravelPhotoAddressInfo.getLatitude());
                                daoTravelPhoto.setLongitude(daoTravelPhotoAddressInfo.getLongitude());
                                daoTravelPhoto.setAddress(daoTravelPhotoAddressInfo.getAddress());
                                daoTravelPhoto.setLocation_country(daoTravelPhotoAddressInfo.getLocation_country());
                                daoTravelPhoto.setCountryCode(daoTravelPhotoAddressInfo.getCountryCode());
                                String state = daoTravelPhotoAddressInfo.getLocation_state();
                                daoTravelPhoto.setLocation_country_state(daoTravelPhotoAddressInfo.getLocation_country());
                                daoTravelPhoto.setLocation_state(state);
                                daoTravelPhoto.setKorea(false);

                                Date d = null;
                                try {
                                    d = sFormatter.parse(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                                    String dateFormat = convertFormat.format(d);
                                    daoTravelPhoto.setDate(dateFormat);
                                    dateFormat = categoryFormat.format(d);
                                    daoTravelPhoto.setCategoryDate(dateFormat);
                                } catch (ParseException e) {
                                    Log.e(TAG, "error:" + e.getLocalizedMessage());
                                    DebugLog.e("test_travel travel_meta in condition error: "+e.getMessage());
                                    daoTravelPhoto.setDate("");
                                }

                                /*long endTime=System.currentTimeMillis();
                                Log.i(TAG,"getPhotoMetaData end time:"+endTime);
                                Log.i(TAG,"getPhotoMetaData duration:"+(endTime-startTime)/1000);
                                Log.i(TAG,"##### getPhotoMetaData end #####");*/

                                return daoTravelPhoto;
                            }
                        }
                    } else {
                        DebugLog.d("test_travel travel_meta ---- STOP CONDITION KOREA FALSE ---- ");

                        daoTravelPhoto = new DaoTrip();
                        daoTravelPhoto.setKorea(true);
                        if(photoUri!=null) {
                            daoTravelPhoto.setFilepath(photoUri.toString());
                        } else {
                            daoTravelPhoto.setFilepath(file);
                        }
//                        daoTravelPhoto.setFilepath(file);
                        daoTravelPhoto.setCountryCode("KR");
                    }


                } else {
                    DebugLog.d("test_travel travel_meta ---- ALIVE CONDITION FALSE ---- ");
                }
            } else {
                DebugLog.d("test_travel travel_meta ---- STOP EXIF IS NOT EXIST ----");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            DebugLog.e("test_travel travel_meta out condition error: "+e.getMessage());
            Log.e(TAG,"error:"+e.getMessage());
        }

        return daoTravelPhoto;
    }

    private DaoTrip getPhotoMetaDataBackup(String file) {
        DaoTrip daoTravelPhoto = null;
        try {
            ExifInterface exifInterface = new ExifInterface(file);

            if (exifInterface != null) {
                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                String datetime = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null && datetime != null) {
                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);

                    DaoTrip daoTravelPhotoAddressInfo = convertToAddress(latitude, longitude);
                    if (daoTravelPhotoAddressInfo != null) {
                        if (daoTravelPhotoAddressInfo.getAddress() != null) {
                            daoTravelPhoto = new DaoTrip();
                            daoTravelPhoto.setFilepath(file);
                            daoTravelPhoto.setDatetime(exifInterface.getAttribute(ExifInterface.TAG_DATETIME));
                            daoTravelPhoto.setLatitude(daoTravelPhotoAddressInfo.getLatitude());
                            daoTravelPhoto.setLongitude(daoTravelPhotoAddressInfo.getLongitude());
                            daoTravelPhoto.setAddress(daoTravelPhotoAddressInfo.getAddress());
                            daoTravelPhoto.setLocation_country(daoTravelPhotoAddressInfo.getLocation_country());
                            daoTravelPhoto.setLocation_state(daoTravelPhotoAddressInfo.getLocation_state());

                            return daoTravelPhoto;
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            //Log.e(TAG,"error:"+e.getMessage());
        }

        return daoTravelPhoto;
    }

    /*private double[] getLocation(String file) {
        try {
            androidx.exifinterface.media.ExifInterface exifInterface = new androidx.exifinterface.media.ExifInterface(file);
            if (exifInterface != null) {
                String latValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                String latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                String lngValue = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                String lngRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);

                if (latValue != null && latRef != null && lngValue != null && lngRef != null) {
                    double latitude = convertRationalLatLonToDouble(latValue, latRef);
                    double longitude = convertRationalLatLonToDouble(lngValue, lngRef);
                    return new double[]{latitude, longitude};
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            Log.e(TAG, "error:" + e.getMessage());
        }
        return null;
    }*/

    private static double convertRationalLatLonToDouble(String rationalString, String ref) {
        try {
            String[] parts = rationalString.split(",", -1);

            String[] pair;
            pair = parts[0].split("/", -1);
            double degrees = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            pair = parts[1].split("/", -1);
            double minutes = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            pair = parts[2].split("/", -1);
            double seconds = Double.parseDouble(pair[0].trim())
                    / Double.parseDouble(pair[1].trim());

            double result = degrees + (minutes / 60.0) + (seconds / 3600.0);
            if ((ref.equals("S") || ref.equals("W"))) {
                return -result;
            } else if (ref.equals("N") || ref.equals("E")) {
                return result;
            } else {
                // Not valid
                throw new IllegalArgumentException();
            }
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            // Not valid
            throw new IllegalArgumentException();
        }
    }

    private DaoTrip convertToAddress(double latitude, double longitude) {
        DaoTrip daoTravelPhoto = null;
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(mContext, Locale.KOREA);

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            Log.e(TAG, "error:" + e.getMessage());
        }

        Address address = addresses.get(0);
        ArrayList<String> addressFragments = new ArrayList<String>();
        String sAddress = "";
        if (addresses != null || addresses.size() > 0) {
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
                Log.i(TAG, "address:" + address.getAddressLine(i));
            }
            sAddress = address.getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            //String city = address.getLocality();
            String state = address.getAdminArea();
            String country = address.getCountryName();
            String countryCode = address.getCountryCode();

            /*Log.i(TAG,"country:"+country);
            Log.i(TAG,"state:"+state);
            Log.i(TAG,"city:"+city);
            Log.i(TAG,"countryCode:"+countryCode);
            Log.i(TAG,"adminArea:"+adminArea);
            Log.i(TAG,"subAdminArea:"+subAdminArea);
            Log.i(TAG,"subThoroughfare:"+subThoroughfare);
            Log.i(TAG,"featureName:"+featureName);
            Log.i(TAG,"subLocality:"+subLocality);
            Log.i(TAG,"thoroughfare:"+thoroughfare);
            Log.i(TAG,"premises:"+premises);*/

            daoTravelPhoto = new DaoTrip();
            daoTravelPhoto.setAddress(sAddress);
            daoTravelPhoto.setLocation_country(country);
            daoTravelPhoto.setLocation_state(state);
            daoTravelPhoto.setLatitude(latitude);
            daoTravelPhoto.setLongitude(longitude);
            daoTravelPhoto.setCountryCode(countryCode);
        }

        return daoTravelPhoto;
    }

    public void loading() {
        //로딩
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        progressDialog = new ProgressDialog(mContext);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("잠시만 기다려 주세요");
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.setCancelable(true);
                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                if (getCategotyTask != null && !getCategotyTask.isCancelled()) {
                                    getCategotyTask.cancel(true);
                                }
                            }
                        });
                        if (getActivity()!=null && !getActivity().isFinishing()){
                            progressDialog.show();
                        }

                    }
                }, 0);
    }

    public void loadingEnd() {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog!=null && progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                }, 0);
    }

    @Override
    public void onRefresh() {
        Log.i(TAG,"completeAsyncTaskCount:"+completeAsyncTaskCount);
        if (completeAsyncTaskCount==-1 || completeAsyncTaskCount==AsyncTaskMaxCount){
            /*database.dropTravelData();

            isLoadDB=false;

            adapter.setInitPreviousTotalItemCount();
            adapter.setMoreLoading(false);

            checkPermission();*/
            refresh();
        }else {
            Toast.makeText(mContext,"이미지를 불러오는 중에는 새로고침을 할 수 없습니다.",Toast.LENGTH_SHORT).show();
        }


        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onLoadMore() {
        /*Log.i(TAG,"onLoadMore");
        if (!isLoadDB){
            adapter.setProgressMore(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ///////이부분에을 자신의 프로젝트에 맞게 설정하면 됨
                    //다음 페이지? 내용을 불러오는 부분
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        int hasPermissionRead = mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                        List<String> permissions = new ArrayList<String>();
                        if (hasPermissionRead != PackageManager.PERMISSION_GRANTED) {
                            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }

                        if (!permissions.isEmpty()) {
                            requestPermissions(permissions.toArray(new String[permissions.size()]), REQUEST_CODE_PERMISSIONS);
                            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

                            }
                        } else {
                            getPhotoTaskMore = new GetPhotoTaskMore();
                            getPhotoTaskMore.execute();
                        }
                    } else {
                        getPhotoTaskMore = new GetPhotoTaskMore();
                        getPhotoTaskMore.execute();
                    }
                }
            }, 500);
        }*/

    }

    private void loadData() {
        if (mCategoryArrayList!=null){
            adapter.addAll(mCategoryArrayList);
        }

    }

    class GetPhotoTask extends AsyncTask<Void, Integer, ArrayList<DaoCategory>> {
        @Override
        protected void onPreExecute() {
            //loading();

            super.onPreExecute();
        }

        @Override
        protected ArrayList<DaoCategory> doInBackground(Void... voids) {
            String sStartDate = "2019-12-01 00:00:00";
            String sEndDate = "2019-12-31 23:59:59";
            /**
             * 설정된 날짜와 상관없이 현재는 최근 1년 사진만 가져오게 변경
             *
             */
            ArrayList<DaoCategory> photoArrayList = getCategory(sStartDate, sEndDate);
            if (photoArrayList!=null && photoArrayList.size()>0){
                try {
                    TravelCategoryData data=new TravelCategoryData();
                    for (DaoCategory category:photoArrayList){
                        data.setPath(category.getFilepath1());
                        data.setCountry_name(category.getTitle());
                        data.setStart_date(category.getStartDate());
                        data.setEnd_date(category.getEndDate());
                        data.setCategory_num(category.getCategoryNum());
                        data.setPath(category.getFilepath1());
                        String imgArray[]=category.getFilepath1().split("######");
                        data.setPhoto_count(imgArray.length);
                        database.insertTravelCategory(data);
                    }
                }
                catch(SQLException e)
                {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }
            }


            return photoArrayList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        protected void onPostExecute(ArrayList<DaoCategory> result) {
            //loadingEnd();
            mCategoryArrayList = result;
            loadData();
            //adapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }

        //Task가 취소되었을때 호출
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    class GetPhotoTaskMore extends AsyncTask<Void, Integer, ArrayList<DaoCategory>> {
        @Override
        protected void onPreExecute() {
            //loading();
            super.onPreExecute();
        }

        @Override
        protected ArrayList<DaoCategory> doInBackground(Void... voids) {
            Log.i(TAG,"GetPhotoTaskMore doInBackground");
            String sStartDate = "2019-12-01 00:00:00";
            String sEndDate = "2019-12-31 23:59:59";
            /**
             * 설정된 날짜와 상관없이 현재는 최근 1년 사진만 가져오게 변경
             *
             */
            ArrayList<DaoCategory> photoArrayList = getCategoryMore();
            if (photoArrayList!=null && photoArrayList.size()>0){
                try {
                    TravelCategoryData data=new TravelCategoryData();
                    for (DaoCategory category:photoArrayList){
                        data.setPath(category.getFilepath1());
                        data.setCountry_name(category.getTitle());
                        data.setStart_date(category.getStartDate());
                        data.setEnd_date(category.getEndDate());
                        data.setCategory_num(category.getCategoryNum());
                        data.setPath(category.getFilepath1());
                        String imgArray[]=category.getFilepath1().split("######");
                        data.setPhoto_count(imgArray.length);
                        database.insertTravelCategory(data);
                    }
                }
                catch(SQLException e)
                {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }
            }

            return photoArrayList;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        protected void onPostExecute(ArrayList<DaoCategory> result) {
            Log.i(TAG,"GetPhotoTaskMore onPostExecute:"+result.size());
            if (result!=null && result.size()>0){
                //loadingEnd();
                mCategoryArrayList.clear();
                adapter.setProgressMore(false);
                adapter.setMoreLoading(false);
                mCategoryArrayList = result;
                //adapter.notifyDataSetChanged();

                adapter.addItemMore(mCategoryArrayList);


                int totalHeight = recyclerView.getHeight();

                View v=LayoutInflater.from(mContext).inflate(R.layout.trip_item,null,false);
                v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                int height = v.getMeasuredHeight();
                Log.i(TAG,"height:"+height);
                Log.i(TAG,"totalHeight:"+totalHeight);

                if (totalHeight>0 && height>0 && adapter.getItemCount()>0){
                    if (totalHeight>height*adapter.getItemCount()){
                        adapter.setMoreLoading(true);
                        onLoadMore();
                        //adapter.setProgressMore(true);
                        //adapter.setProgressMore(false);
                    }
                }
            }else {
                adapter.setProgressMore(false);
                adapter.setMoreLoading(false);
            }


            super.onPostExecute(result);
        }

        //Task가 취소되었을때 호출
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    /**
     * i년 전의 날을 구한다.
     */
    public Date getPreviousYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, -1);

        return cal.getTime();
    }

    private List<Integer> randomNumber(int size) {
        int a[] = new int[3];
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i <= 2; i++) {
            a[i] = r.nextInt(size);

            for (int j = 0; j < i; j++) {
                if (a[i] == a[j]) {
                    i--;
                }
            }
        }

        for (int value : a) {
            list.add(value);
        }

        //Collections.sort(list);
        return list;

    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    getPhotoTask = new GetPhotoTask();
                    getPhotoTask.execute();
                } else {
                    Toast.makeText(mContext, "사진을 불러 올 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;

            case REQUEST_CODE_PERMISSIONS_MORE:
                if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    getPhotoTaskMore = new GetPhotoTaskMore();
                    getPhotoTaskMore.execute();
                } else {
                    Toast.makeText(mContext, "사진을 불러 올 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public String getStringUriFromPath(String path){
        Uri fileUri = Uri.parse( path );
        String filePath = fileUri.getPath();
        Cursor cursor = mContext.getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + filePath + "'", null, null );
        cursor.moveToNext();
        int id = cursor.getInt( cursor.getColumnIndex( "_id" ) );
        Uri uri = ContentUris.withAppendedId( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id );
        cursor.close();
        return uri.toString();
    }

    public class GetCategotyTask extends AsyncTask<Long, Integer, ArrayList<DaoCategory>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<DaoCategory> doInBackground(Long... datetime) {
            DebugLog.d("test_travel GetCategoryTask doInBackground()<--");

            long startDate = datetime[0];
            long endDate = datetime[1];
            ArrayList<DaoCategory> photoArrayList = getCategoryNew(startDate, endDate);
            if (photoArrayList!=null && photoArrayList.size()>0){
                try {

                    TravelCategoryData data=new TravelCategoryData();

                    for (DaoCategory category:photoArrayList){
                        data.setPath(category.getFilepath1());
                        data.setCountry_name(category.getTitle());
                        data.setStart_date(category.getStartDate());
                        data.setEnd_date(category.getEndDate());
                        data.setCategory_num(category.getCategoryNum());
                        data.setPath(category.getFilepath1());
                        data.setPhoto_count(category.getCount());
                        //String imgArray[]=category.getFilepath1().split("######");
                        //data.setPhoto_count(imgArray.length);
                        database.insertTravelCategory(data);
                    }

                }
                catch(SQLException e)
                {
                    Log.e(TAG,"error:"+e.getLocalizedMessage());
                }
            }

            return photoArrayList;
        }

        private Runnable changeUI = new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        };

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(ArrayList<DaoCategory> daoCategories) {
            super.onPostExecute(daoCategories);

            loadingEnd();

            completeAsyncTaskCount++;
            if (completeAsyncTaskCount== AsyncTaskMaxCount){
                isComplete=true;
                //adapter.addAllComplete();
                loadCategory();

                SimpleDateFormat sFormatter2 = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                sFormatter2.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
                String loadedDate=sFormatter2.format(new Date(System.currentTimeMillis()));
                //Log.i(TAG,"gpsDatetime2:"+loadedDate);
                mPrefs.setPreferenceStringValue(AppPrefs.CATEGORY_LOADED_DATE,loadedDate);

                if (!isCancelled()&&getActivity()!=null && !getActivity().isFinishing()){
                    msgDialogComplete = new AlertDialog.Builder(mContext).create();
                    msgDialogComplete.setTitle("알림");
                    msgDialogComplete.setMessage("불러오기가 완료되었습니다.");
                    msgDialogComplete.setButton(Dialog.BUTTON_POSITIVE,"닫기",
                            new DialogInterface.OnClickListener() {
                                // @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    msgDialogComplete.show();

                    new UploadCategoryInfoTask().execute();
                }

            }else {
                if (daoCategories!=null && daoCategories.size()>0) {
                    //mCategoryArrayList = daoCategories;
                    //adapter.addItemMoreNew(mCategoryArrayList);
                    adapter.notifyDataSetChanged();
                    //loadData();
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    /** 동적으로(코드상으로) 브로드 캐스트를 등록한다. **/
    private void registerReceiver(){
        /** 1. intent filter를 만든다
         *  2. intent filter에 action을 추가한다.
         *  3. BroadCastReceiver를 익명클래스로 구현한다.
         *  4. intent filter와 BroadCastReceiver를 등록한다.
         * */
        if(mReceiver != null) return;

        final IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(BROADCAST_MESSAGE);
        theFilter.addAction(BROADCAST_LONG_MESSAGE);

        this.mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(BROADCAST_MESSAGE)){
                    isComplete=false;
                    if (completeAsyncTaskCount==-1 || completeAsyncTaskCount==AsyncTaskMaxCount){
                        AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                        msgDialog.setTitle("알림");
                        msgDialog.setMessage("처음부터 사진을 다시 불러오시겠습니까?");
                        msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                                new DialogInterface.OnClickListener() {
                                    // @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        database.dropTravelData();
                                        mPrefs.setPreferenceStringValue(AppPrefs.CATEGORY_LOADED_DATE,"");
                                        isLoadDB=false;

                                        adapter.setInitPreviousTotalItemCount();
                                        adapter.setMoreLoading(false);

                                        checkPermission();
                                    }
                                });
                        msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        msgDialog.show();

                    }else {
                        Toast.makeText(mContext,"이미지를 불러오는 중에는 해당 기능을 이용할 수 없습니다.",Toast.LENGTH_SHORT).show();
                    }
                }else if (intent.getAction().equals(BROADCAST_LONG_MESSAGE)){
                    AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
                    msgDialog.setTitle("알림");
                    msgDialog.setMessage("여행의 발자취 오류 리포팅을 전송하시겠습니까?");
                    msgDialog.setButton(Dialog.BUTTON_POSITIVE,"예",
                            new DialogInterface.OnClickListener() {
                                // @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    HttpMultiPart();
                                }
                            });
                    msgDialog.setButton(Dialog.BUTTON_NEGATIVE, "아니오",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    msgDialog.show();
                }
            }
        };

        getActivity().registerReceiver(mReceiver, theFilter);

    }

    /** 동적으로(코드상으로) 브로드 캐스트를 종료한다. **/
    private void unregisterReceiver() {
        if(mReceiver != null){
            getActivity().unregisterReceiver(mReceiver);
            mReceiver = null;
        }

    }



    public class UploadCategoryInfoTask extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.i(TAG,"doInBackground");
            JSONObject obj = new JSONObject();
            try {
                JSONArray jArray = new JSONArray();//배열이 필요할때

                ArrayList<DaoCategory> arrayList= adapter.getAllItems();
                for (DaoCategory category: arrayList) {
                    String title=category.getTitle();
                    final String date;
                    if (category.getStartDate().equals(category.getEndDate())){
                        date=category.getStartDate();
                    }else {
                        date=category.getStartDate()+"~"+category.getEndDate();
                    }

                    JSONObject sObject = new JSONObject();//배열 내에 들어갈 json
                    sObject.put("country", title);
                    sObject.put("visit_dt", date);
                    jArray.put(sObject);
                }
                obj.put("param1", jArray);//배열을 넣음
                Log.i(TAG,"category convert json:"+obj.toString());


                /*URL url = new URL("http://211.43.13.195:4001/photoshow/"+userID);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestProperty("content-type", "application/json");
                connection.setRequestMethod("PUT");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setConnectTimeout(15000);

                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                wr.write(obj.toString());
                wr.flush();
                wr.close();

                StringBuffer response;
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                } else {
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                    String inputLine;
                    response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                }
                Log.i(TAG,"result:"+response.toString());*/


            } catch (Exception e) {
                Log.e(TAG,"error:"+e.getLocalizedMessage());
                obj=null;
            }
            return obj;
        }

        @Override
        protected void onPostExecute(JSONObject obj) {
            Log.i(TAG,"onPostExecute");
            Log.i(TAG,"category convert json:"+obj.toString());

            if (obj!=null){
                if (getCategotyTask!=null && getCategotyTask.isCancelled()){
                    return;
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(RetrofitExService.URL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RetrofitExService retrofitExService = retrofit.create(RetrofitExService.class);

                //String json="{'param1':[{'country':'알제리','visit_dt':'2019.12.24~2019.12.25'},{'country':'캐나다','visit_dt':'2019.12.24'},{'country':'볼리비아','visit_dt':'2019.12.23'},{'country':'중국','visit_dt':'2019.12.06~2019.12.24'},{'country':'중국','visit_dt':'2019.12.03'},{'country':'중국','visit_dt':'2019.12.02'},{'country':'미국','visit_dt':'2019.12.02'},{'country':'미국','visit_dt':'2019.10.02~2019.10.12'},{'country':'일본','visit_dt':'2017.12.02'},{'country':'중국','visit_dt':'2016.12.03'},{'country':'러시아','visit_dt':'2015.04.03'}]}";
                retrofitExService.putData(userID,obj.toString()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        Log.d(TAG, "onResponse:"+response.toString());
                        if (response.isSuccessful()) {
                            Log.d(TAG, "response code : "+response.code());

                            postClickLogData("Accessphoto");
                            /*try {
                                Log.d(TAG, "response.body().string() : "+response.body().string());
                                //Log.d(TAG, "response.body().getResponse().getCode() : "+response.body().getResponse().getCode());
                            }catch (Exception e){
                                Log.e(TAG,"error:"+e.getMessage());
                            }*/
                            if (response.code()==200){
                                mPrefs.setPreferenceStringValue(AppPrefs.CATEGORY_UPLOADED_DATETIME,System.currentTimeMillis()+"");
                            }
                        }else {
                            Log.d(TAG, "response code : "+response.code());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Log.d(TAG, "onFailure");
                    }
                });
            }


            super.onPostExecute(obj);
        }
    }

    private void postCSV(final Context context) {
        (new AsyncTask<Void, Void, String>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected String doInBackground(Void... params) {
                File csv=new File(Setting.MAALTALK_DATABASE_ROOT);
                if (csv!=null && csv.length()>0){
                    AppPrefs mPrefs = new AppPrefs(context);
                    String post_url = "http://211.253.24.69/auth/push_record/send_mtt_record.php";
                    String user_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpContext localContext = new BasicHttpContext();


                    HttpPost httpPost = new HttpPost(post_url);

                    try {
                        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        Log.i(TAG,"csv:"+csv.getAbsolutePath());

                        nameValuePairs.add(new BasicNameValuePair("name", user_id));
                        nameValuePairs.add(new BasicNameValuePair("qna", "test"));
                        nameValuePairs.add(new BasicNameValuePair("os_type", "ANDROID"));
                        nameValuePairs.add(new BasicNameValuePair("os_ver", Build.VERSION.SDK));
                        nameValuePairs.add(new BasicNameValuePair("app_ver", ServicePrefs.getLoginVersion()));
                        nameValuePairs.add(new BasicNameValuePair("device_model", ServicePrefs.getModel()));

                        entity.addPart("userfile", new FileBody(csv));

                        for(int index=0; index < nameValuePairs.size(); index++) {
                            entity.addPart(nameValuePairs.get(index).getName(), new StringBody(nameValuePairs.get(index).getValue(), Charset.forName("UTF-8")));
                        }

                        httpPost.setEntity(entity);

                        HttpResponse response = httpClient.execute(httpPost, localContext);
                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                response.getEntity().getContent()));

                        String line = null;
                        StringBuilder sb = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        Log.d(TAG, "HTTPS POST EXEC : " + sb.toString());

                        // CHECK STATUS CODE
                        if (response.getStatusLine().getStatusCode() != 200) {
                            Log.e(TAG, "HTTPS POST STATUS : "
                                    + response.getStatusLine().toString());
                            return null;
                        }

                        return sb.toString();
                    } catch (IOException e) {
                        Log.e(TAG, "error: " + e.getMessage(), e);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String rs) {
                if (rs!=null){
                    JSONObject jObject;
                    try {
                        jObject = new JSONObject(rs);
                        JSONObject responseObject = jObject.getJSONObject("RESPONSE");
                        if (responseObject.getInt("RES_CODE") == 0) {
                            Toast.makeText(getActivity(), "전송 완료", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "전송 실패", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "JSON ERROR: "+ e.getLocalizedMessage());
                    }

                }

                super.onPostExecute(rs);
            }
        }).execute();
    }


    private void HttpMultiPart(){

        new AsyncTask<Void, Void, JSONObject>(){
            int total;																				// 숫자 형 객체
            int bytesAvailable;																		// 숫자 형 객체
            int sendBytes;

            @Override
            protected void onPreExecute() {

                File file=new File(Setting.MAALTALK_DATABASE_ROOT);
                total=(int)file.length();

                super.onPreExecute();
            }

            @Override
            protected JSONObject doInBackground(Void... voids) {
                sendBytes = 0;

                String boundary = "^-----^";
                String LINE_FEED = "\r\n";
                String charset = "UTF-8";
                OutputStream outputStream;
                PrintWriter writer;

                JSONObject result = null;
                try{
                    //Uri uri=Uri.parse(trip.getUriPath());
                    //String sPath=getPathFromUri(uri);
                    File file=new File(Setting.MAALTALK_DATABASE_ROOT);
                    total=(int)file.length();
                    if (file.length()>0){
                        URL url = new URL("http://211.253.24.69/auth/push_record/send_mtt_record.php");
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                        connection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);
                        connection.setRequestMethod("POST");
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        connection.setUseCaches(false);
                        connection.setConnectTimeout(15000);

                        outputStream = connection.getOutputStream();
                        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);


                        /**
                         *
                         *                         nameValuePairs.add(new BasicNameValuePair("name", user_id));
                         *                         nameValuePairs.add(new BasicNameValuePair("qna", "test"));
                         *                         nameValuePairs.add(new BasicNameValuePair("os_type", "ANDROID"));
                         *                         nameValuePairs.add(new BasicNameValuePair("os_ver", Build.VERSION.SDK));
                         *                         nameValuePairs.add(new BasicNameValuePair("app_ver", ServicePrefs.getLoginVersion()));
                         *                         nameValuePairs.add(new BasicNameValuePair("device_model", ServicePrefs.getModel()));
                         *
                         */

                        String user_id = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/

                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"name\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append(user_id).append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"name:"+user_id);

                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"qna\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append("test").append(LINE_FEED);
                        writer.flush();

                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"os_type\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append("ANDROID").append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"os_type:"+"ANDROID");

                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"os_ver\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append(Build.VERSION.SDK).append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"os_ver:"+Build.VERSION.SDK);

                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"app_ver\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append(ServicePrefs.getLoginVersion()).append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"app_ver:"+ServicePrefs.getLoginVersion());

                        /** Body에 데이터를 넣어줘야 할경우 없으면 Pass **/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"device_model\"").append(LINE_FEED);
                        writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.append(ServicePrefs.getModel()).append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"device_model:"+ServicePrefs.getModel());

                        /** 파일 데이터를 넣는 부분**/
                        writer.append("--" + boundary).append(LINE_FEED);
                        writer.append("Content-Disposition: form-data; name=\"userfile\"; filename=\"" + file.getName() + "\"").append(LINE_FEED);
                        writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(LINE_FEED);
                        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                        writer.append(LINE_FEED);
                        writer.flush();
                        Log.i(TAG,"photo:"+file.getAbsolutePath());

                        int maxBufferSize = 4*1024;
                        int bufferSize = Math.min(total, maxBufferSize);
                        FileInputStream inputStream = new FileInputStream(file);
                        byte[] buffer = new byte[bufferSize];
                        int bytesRead = -1;
                        Log.i(TAG,"total:"+total);
                        while ((bytesRead = inputStream.read(buffer)) != -1) {
                            sendBytes += bytesRead;
                            //Log.i(TAG,"sendBytes:"+sendBytes);
                            outputStream.write(buffer, 0, bytesRead);
                            //publishProgress();
                        }
                        outputStream.flush();
                        inputStream.close();
                        writer.append(LINE_FEED);
                        writer.flush();

                        writer.append("--" + boundary + "--").append(LINE_FEED);
                        writer.close();

                        int responseCode = connection.getResponseCode();
                        if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();
                            while ((inputLine = in.readLine()) != null) {
                                response.append(inputLine);
                            }
                            in.close();

                            try {
                                result = new JSONObject(response.toString());
                                Log.i(TAG,"result:"+response.toString());
                            } catch (JSONException e) {
                                Log.e(TAG,"error:"+e.getLocalizedMessage());
                            }
                        } else {
                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                            String inputLine;
                            StringBuffer response = new StringBuffer();
                            while ((inputLine = in.readLine()) != null) {
                                response.append(inputLine);
                            }
                            in.close();
                            Log.i(TAG,"responseCode not ok result:"+response.toString());
                            result = new JSONObject(response.toString());
                        }
                    }else {

                    }
                } catch (ConnectException e) {
                    Log.e(TAG, "ConnectException:"+e.getLocalizedMessage());


                } catch (Exception e){
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);

                //Dialog.setProgress(sendBytes);
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                try {
                    JSONObject responseObject = jsonObject.getJSONObject("RESPONSE");
                    if (responseObject.getInt("RES_CODE") == 0) {
                        if (getActivity()!=null && !getActivity().isFinishing() ){
                            Toast.makeText(getActivity(), "전송 완료", Toast.LENGTH_SHORT).show();
                        }

                        return;
                    }
                } catch (Exception e) {
                    if (getActivity()!=null && !getActivity().isFinishing()){
                        Toast.makeText(getActivity(), "전송 실패", Toast.LENGTH_SHORT).show();
                    }

                    Log.e(TAG, "JSON ERROR: "+ e.getLocalizedMessage());
                }
            }

        }.execute();
    }

}
