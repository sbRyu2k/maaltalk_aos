package com.dial070.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua.pjsua;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.database.Cursor;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DialerKeyListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.db.ScoreData;
import com.dial070.global.GlobalConst;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.api.SipProfileState;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.Compatibility;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.DialingFeedback;
import com.dial070.utils.EnglishUtils;
import com.dial070.utils.HangulUtils;
import com.dial070.utils.Log;
import com.dial070.utils.PhoneNumberTextWatcher;
import com.dial070.utils.ScoreUtils;
import com.dial070.widgets.DialPad;
import com.dial070.widgets.DialPad.OnDialKeyListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class Dial extends Activity implements OnLongClickListener,
		OnDialKeyListener, TextWatcher {
	private static final String THIS_FILE = "DIAL070_DIAL";
	public final static String BC_DIAL = "DialBC_DIAL";
	public final static String BC_CALL= "DialBC_CALL";
	public final static String BC_SEARCH= "DialBC_SEARCH";
	public final static String BC_UPDATE_REG_STATE= "DialBC_UPDATE_REG_STATE";
	public final static String BC_DIALOG= "DialBC_DIALOG";
	public final static String BC_FINISH= "DialBC_FINISH";

	// 즐겨찾기
	public static final int FAVORITE_VIEW_MODE = 0;
	public static final int FAVORITE_EDIT_MODE = 1;

	// private static final int MAKE_CALL = 0;
	private final static int SELECT_CONTACT = 1;
	private final static int SELECT_FAVORITE = 2;
	private final static int SELECT_SEARCH_RESULT = 3;
	private final static int START_GPS = 4;
	private final static int SELECT_SEARCH = 5;
	// BJH
	private static final int SELECT_COUNTRY = 7;

	private final static int SEARCH_TYPE_PHONE = 1;
	private final static int SEARCH_TYPE_NAME = 2;

	// ADD FAVORITE RETURN CODE
	private final static int RESULT_ADD_FAIL = -1;
	private final static int RESULT_ADD_USER = 0;
	private final static int RESULT_EXIST_USER = 1;

	private Context mContext;
	private AppPrefs mPrefs;
	private GridView mFavoritesListView;
	private FavoritesListAdapter mAdapter;
	private FavoriteData mFavoritesItem;
	// private int mFavoriteIndex;
	private int mEditMode;
	// private boolean mFirstLoad;
	// private static boolean requestGPS = true;
	// private boolean mIsDialView = false;

	// private boolean mLoadSearch = false;
	private boolean mSkipSearch = false;

	// FOR LOCATION : GPS
	// private static final int MIN_TIME = 60 * 1000; // 60초
	// private static final int MIN_DISTANCE = 200; // 200M

	// private LocationManager mLocMan = null;
	// private String mProvider = null;
	// private double mMyLongitude;
	// private double mMyLatitude;

	private String mSearchFirstContact = "";

	// KeyPad
	private ImageView mHintCallNumber;
	private EditText mEditCallNumber;
	private LinearLayout mKeypadView,mLinearSearch;
	private FrameLayout mLayoutCallNumber;
	private ImageButton dial0Button;
	private ImageButton dialStarButton;// BJH 2016.12.02
	private ImageButton mCallNumberDelete, mShortDialpad, mShortFavorite;
	private DialPad mDialPad;
	private DialingFeedback dialFeedback;

	private TextView mTextResultName, mTextResultPhone, mTextResultCount;
	private ImageButton mResultMore;
	// BJH
	private Button m_ButtonCountry;
	private boolean mLoadFullCountry = false;
	private static Map<String, ArrayList<CountryItem>> mCountryFullItemList;

	private String oldNumber, newNumber;
	private boolean loadMatchContact;
	private boolean mKeypadMode = false;
	private boolean mResetNumber = false;

	private ArrayList<ContactsData> mContactList;

	private BroadcastReceiver mKeypadViewReceiver;
	// private BroadcastReceiver mFavoriteViewReceiver;

	private static final int DIAL070_CONTACT_SEARCH_START = 1;
	private static final int DIAL070_CONTACT_SEARCH_OK = 2;
	private static final int DIAL070_CONTACT_SEARCH_ERROR = 3;
	private static final int DIAL070_CONTACT_SEARCH_CLOSE = 4;
	private static final int DIAL070_CONTACT_ADD_FAVORITE = 5;
	private static final int DIAL070_CONTACT_ADD_FAIL = 6;
	private static final int DIAL070_CONTACT_EXIST_USER = 7;

	private static final int DIAL070_FAVORITE_UPDATEED = 9;

	// BJH
	private String m_CountryCode = null;
	private String m_CountryFlag = null;
	private String m_NumberCode = "";
	private String mLocale = "";
	//2016.06.07
	private String mLocaleStr = "ko_KR";
	// App app;
	//BJH 2016.08.17
	private ArrayList<CountryItem> mCountry = null;
	//BJH 2016.10.17
	private boolean mUserMsg = false;

	private long mLastClickTime;

	private BroadcastReceiver mUrlSchemeCallReceiver = null;

	private BroadcastReceiver mBrUpdateRegState;
	private ImageView imgRtt, btnRefresh;
	private TextView txtRtt,txtNumber,txtNumRemainDay;
	private BroadcastReceiver mBrDialog;
	private BroadcastReceiver mBrFinish;

	private CircleProgressBar mCircleProgressBar;

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case DIAL070_CONTACT_SEARCH_START: {

				}
				break;
				case DIAL070_CONTACT_SEARCH_OK: {
					applySearchResult();
				}
				break;
				case DIAL070_CONTACT_SEARCH_ERROR: {
					//
					loadMatchContact = false;
				}
				break;
				case DIAL070_CONTACT_SEARCH_CLOSE: {
					// dialog cloase
				}
				break;
				case DIAL070_CONTACT_ADD_FAVORITE: {
					reloadFavorites(true);
				}
				break;
				case DIAL070_CONTACT_EXIST_USER: {
					if (Dial.this != null && !Dial.this.isFinishing()) {
						Alert(Dial.this, getResources().getString(
								R.string.error_already_registered));
					}
				}
				break;
				case DIAL070_CONTACT_ADD_FAIL: {
					if (Dial.this != null && !Dial.this.isFinishing()) {
						Alert(Dial.this, getResources().getString(
								R.string.error_add_favorite));
					}
				}
				break;
				case DIAL070_FAVORITE_UPDATEED: {
					if (mAdapter != null)
						mAdapter.notifyDataSetChanged();
				}
				break;
			}
		}
	};

	private Thread reloadThread = null;

	private synchronized void reloadFavorites(final boolean doUpdateScore) {
		if (reloadThread != null)
			return;

		reloadThread = new Thread() {
			public void run() {
				if (doUpdateScore)
					UpdateScore();

				if (mAdapter != null) {
					mAdapter.buildFavoritesData();
					// mAdapter.notifyDataSetChanged();
					if (handler != null)
						handler.sendEmptyMessage(DIAL070_FAVORITE_UPDATEED);
				}
				reloadThread = null;
			};
		};
		reloadThread.start();
	}

	private void reloadWait() {
		if (reloadThread != null) {
			try {
				reloadThread.join(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dial_activity);

		Log.d(THIS_FILE,"onCreate");

		mContext = this;

		App.isReturnClicked=false;

		mPrefs = new AppPrefs(this);

		// mFavoriteIndex = -1;
		mFavoritesItem = null;

		oldNumber = "";
		newNumber = "";
		mSearchFirstContact = "";
		loadMatchContact = false;
		// mFirstLoad = true;
		mContactList = new ArrayList<ContactsData>();

		mEditMode = FAVORITE_VIEW_MODE;

		mDialPad = (DialPad) findViewById(R.id.dialPad);

		mDialPad.setOnDialKeyListener(this);

		dialFeedback = new DialingFeedback(this, false);

		imgRtt=findViewById(R.id.imgRtt);
		btnRefresh=findViewById(R.id.btnRefresh);
		txtRtt=findViewById(R.id.txtRtt);
		txtNumber=findViewById(R.id.txtNumber);
		txtNumRemainDay=findViewById(R.id.txtNumRemainDay);

		mLayoutCallNumber = (FrameLayout) findViewById(R.id.layout_call_number);
		mHintCallNumber = (ImageView) findViewById(R.id.hint_call_number);
		mEditCallNumber = (EditText) findViewById(R.id.edit_call_number);
		mKeypadView = (LinearLayout) findViewById(R.id.keypad_view);
		mLinearSearch = findViewById(R.id.linearSearch);
		mCallNumberDelete = (ImageButton) findViewById(R.id.btn_number_delete);
		mShortDialpad = (ImageButton) findViewById(R.id.btn_short_dialpad);
		mShortFavorite = (ImageButton) findViewById(R.id.btn_short_favorite);

		dial0Button = (ImageButton) findViewById(R.id.dial_0);
		dialStarButton = (ImageButton) findViewById(R.id.dial_star);

		mTextResultName = (TextView) findViewById(R.id.txt_search_result_name);
		mTextResultPhone = (TextView) findViewById(R.id.txt_search_result_number);
		mTextResultCount = (TextView) findViewById(R.id.txt_search_result_count);
		mResultMore = (ImageButton) findViewById(R.id.btn_listup);

		mFavoritesListView = (GridView) findViewById(R.id.favorites_list_view);

		mCircleProgressBar = new CircleProgressBar(this);
		mCircleProgressBar.setCanceledOnTouchOutside(false);

		mCallNumberDelete.setVisibility(View.GONE);
		mShortDialpad.setVisibility(View.GONE);
		// BJH
		// mShortFavorite.setVisibility(View.VISIBLE);
		mShortFavorite.setVisibility(View.GONE);

		txtNumber.setText(ServicePrefs.mUser070);
		if (ServicePrefs.mUser070!=null && ServicePrefs.mUser070.trim().length()>0 ){
			String DID_EXPIRED=ServicePrefs.DID_EXPIRED;
			try {
				if (DID_EXPIRED!=null && DID_EXPIRED.trim().length()>0 && Integer.parseInt(DID_EXPIRED)>0) {
					txtNumRemainDay.setVisibility(View.VISIBLE);
					txtNumRemainDay.setText("D-" + DID_EXPIRED);
				}else {
					txtNumRemainDay.setVisibility(View.INVISIBLE);
					txtNumber.setVisibility(View.INVISIBLE);
				}
			}catch (NumberFormatException e){
				txtNumRemainDay.setVisibility(View.INVISIBLE);
				txtNumber.setVisibility(View.INVISIBLE);
			}
		}

		/**
		 * TEST
		 */

		btnRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DebugLog.d("test_v2 ff_antena imgRtt onClicked.");
				Toast.makeText(Dial.this, "안테나 신호 체크중 입니다.", Toast.LENGTH_SHORT).show();
//				workStart();
				updateRegState();
				long currentTime = System.currentTimeMillis();
				FirebaseCrashlytics.getInstance().setCustomKey("current_time_mills", currentTime);
				FirebaseCrashlytics.getInstance().recordException(new Exception("RTT REFRESH INVOKED"));

				showProgress();
				workStart();
			}
		});

		// For GPS
		// mMyLongitude = 0.0;
		// mMyLatitude = 0.0;

		/*
		 * mLocMan = (LocationManager)
		 * mContext.getSystemService(Context.LOCATION_SERVICE); if (mLocMan !=
		 * null) { mProvider = mLocMan.getBestProvider(new Criteria(), true); }
		 *
		 * // GPS를 사용하는 경우 GPS모듈 실행 if (ServicePrefs.checkGps(mContext))
		 * changeGpsListener(); else changeBestListener(); // 최초 설치시
		 * changeBestListener();
		 *
		 * mMyLatitude =
		 * Util.parseDouble(mPrefs.getPreferenceStringValue(AppPrefs
		 * .LAST_LOC_LATITUDE)); mMyLongitude =
		 * Util.parseDouble(mPrefs.getPreferenceStringValue
		 * (AppPrefs.LAST_LOC_LONGITUDE));
		 *
		 * if (mMyLatitude == 0.0 && mMyLongitude == 0.0) { // 최초설치시 사용자의 마지막
		 * 위치정보는 없으므로 changeBestListener(); }
		 */

		mLayoutCallNumber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setKeypadVisible();
			}
		});

		mLayoutCallNumber
				.setOnLongClickListener(new View.OnLongClickListener() {

					@Override
					public boolean onLongClick(View v) {
						try {
							String str = null;

							ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
							str = clipboard.getText().toString();

							/*
							 * android.content.ClipData.Item item =
							 * clipboard.getPrimaryClip().getItemAt(0); if
							 * (item.getText() != null) str =
							 * item.getText().toString();
							 */

							if (str != null && str.length() > 0) {
								str = PhoneNumberUtils
										.convertKeypadLettersToDigits(str);
								str = PhoneNumberUtils.stripSeparators(str);
								str = str.replaceAll("\\-", "");
								if (str.length() > 0) {
									mEditCallNumber.getText().clear();
									mEditCallNumber.setText("");
									mEditCallNumber.append(str);
								}
							}
						} catch (Exception e) {
						}
						return false;
					}
				});

		mShortDialpad.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				setKeypadVisible();
			}
		});

		mShortFavorite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				setFavoriteVisible();
			}
		});

		mCallNumberDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// BJH
				String number = mEditCallNumber.getText().toString();
				if (number != null && number.length() == 1) {
					/*
					 * if(number.equals("+")) { String code =
					 * mPrefs.getPreferenceStringValue
					 * (AppPrefs.DIAL_COUNTRY_CODE); if(code.equals("+82"))
					 * m_NumberCode = ""; else m_NumberCode = code;
					 * m_ButtonCountry.setText(code);
					 * m_ButtonCountry.setVisibility(View.VISIBLE); }
					 */
					m_CountryCode = null;
					m_CountryFlag = null;
					UpdateCode();
				}
				keyPressed(KeyEvent.KEYCODE_DEL);
			}
		});

		mEditCallNumber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				setKeypadVisible();
			}
		});

		mTextResultPhone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchResultToCallNumber();
			}
		});

		mEditCallNumber.setBackgroundColor(0);
		mEditCallNumber.setKeyListener(DialerKeyListener.getInstance());
		PhoneNumberTextWatcher digitFormater = new PhoneNumberTextWatcher();
		mEditCallNumber.addTextChangedListener(digitFormater);
		mEditCallNumber.addTextChangedListener(this);
		mEditCallNumber.setInputType(android.text.InputType.TYPE_NULL);
		mEditCallNumber.setCursorVisible(false);
		afterTextChanged(mEditCallNumber.getText());
		dial0Button.setOnLongClickListener(this);
		dialStarButton.setOnLongClickListener(this); //BJH 2016.12.02
		mCallNumberDelete.setOnLongClickListener(this);

		//mHintCallNumber.setVisibility(View.VISIBLE); // BJH 2016.12.29

		mAdapter = new FavoritesListAdapter(this);
		mFavoritesListView.setAdapter(mAdapter);

		// mFavoritesListView.setOn

		mFavoritesListView
				.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
												   View arg1, int arg2, long arg3) {

						if (mKeypadView.getVisibility() == View.VISIBLE)
							return false;

						// TODO Auto-generated method stub
						// mFavoriteIndex = arg2;

						FavoritesListView view = (FavoritesListView) arg1;

						mFavoritesItem = view.getData();

						if (mFavoritesItem == null) {
							// ForTest
							Log.e(THIS_FILE, "FAVORITE ITEM IS NULL!!");
							return false;
						} else {
							if (mFavoritesItem.getDisplayName()
									.equalsIgnoreCase("new_add")) {
								return false;
							} else {
								// PopupDialog(mFavoritesItem.getId(),
								// mFavoritesItem.getDisplayName(),
								// mFavoritesItem.getPhoneNumber(),
								// mFavoritesItem.getMemo());
								PopupDialog(mFavoritesItem.getId(),
										mFavoritesItem.getDisplayName(),
										mFavoritesItem.getPhoneNumber());
							}
						}
						return true;
					}
				});

		mFavoritesListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
											int arg2, long arg3) {

						if (mKeypadView.getVisibility() == View.VISIBLE)
							return;

						// TODO Auto-generated method stub
						// mFavoriteIndex = arg2;

						// Log.e(THIS_FILE,"SELETECTED FAVORITE INDEX=" + arg2);

						// mFavoritesItem = (FavoriteData)
						// mAdapter.getItem(arg2);

						FavoritesListView view = (FavoritesListView) arg1;

						mFavoritesItem = view.getData();

						if (mFavoritesItem == null) {
							// ForTest
							// Log.e(THIS_FILE,"FAVORITE ITEM IS NULL!!");
							return;
						} else {

							if (mEditMode == FAVORITE_VIEW_MODE
									&& mFavoritesItem.getDisplayName()
									.equalsIgnoreCase("new_add")) {
								Intent intent = new Intent(Dial.this,
										Contacts.class);
								intent.putExtra("SelectType", 1); // 0:contacts
								// ,1
								// :favorites
								// intent.putExtra("Position",mFavoriteIndex);
								startActivityForResult(intent, SELECT_FAVORITE);
							} else {
								if (mEditMode == FAVORITE_EDIT_MODE) {
									if (mFavoritesItem.getDisplayName()
											.equalsIgnoreCase("new_add"))
										return;

									Resources res = mContext.getResources();
									AlertDialog msgDialog = new AlertDialog.Builder(
											mContext).create();
									msgDialog
											.setTitle(R.string.delete_favorites);
									msgDialog.setMessage(mFavoritesItem
											.getPhoneNumber()
											+ " "
											+ res.getString(R.string.delete_favorites_question));
									msgDialog.setButton(Dialog.BUTTON_POSITIVE,
											res.getString(R.string.yes),
											new DialogInterface.OnClickListener() {
												// @Override
												public void onClick(
														DialogInterface dialog,
														int which) {

													deleteFavorite(mFavoritesItem);
													return;
												}
											});
									msgDialog.setButton(Dialog.BUTTON_NEGATIVE,
											res.getString(R.string.no),
											new DialogInterface.OnClickListener() {

												// @Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													return;
												}
											});
									msgDialog.show();

								} else {
									// 전화걸기
									String number = mFavoritesItem
											.getPhoneNumber();
									/*
									 * BJH 2016.07.26
									 */
									if (number.length() > 0) {
										/*if(!(number.startsWith("+") || number.startsWith("001")))
											number = "+" + number;*/ //BJH 2016.12.29 불필요
										sipCall(number);
									}
								}
							}
						}
					}

				});

		mResultMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// 검색결과 더 보기
				// Activity
				mResultMore.setEnabled(false);

				Intent intent = new Intent(Dial.this, PopupResultMore.class);
				intent.putParcelableArrayListExtra("RESULT", mContactList);
				startActivityForResult(intent, SELECT_SEARCH_RESULT);

			}
		});

		setKeypadVisible();

		// BJH
		// UpdateCode();
		// app=(App) mContext.getApplicationContext();
		//2016.06.09 수정
		mLocaleStr = Locale.getDefault().toString();
		if (mLocaleStr.equals("ko_KR")) // BJH 다국어 한국이 아니면 영어
			mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "한국어");
		else
			mPrefs.setPreferenceStringValue(AppPrefs.LOCALE, "영어");
		mLocale = mPrefs.getPreferenceStringValue(AppPrefs.LOCALE);
		m_ButtonCountry = (Button) findViewById(R.id.buttonCountry);
		m_ButtonCountry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ShowCountry();
			}
		});
		try {
			mEditCallNumber.addTextChangedListener(new TextWatcher() {

				@Override
				public void afterTextChanged(Editable editable) {
					String msg = editable.toString();
					Log.d(THIS_FILE,"afterTextChanged:"+msg);
					String country_code = "";
					if (msg.length() > 0) {
						if (msg.substring(0, 1).equals("+")) {
							if (msg.length() > 1) {
								String code = msg.substring(1);
								if (code.length() == 1)
									getSearchItem(country_code, code, 1);
								else if (code.length() == 2)
									getSearchItem(country_code, code, 2);
								else if (code.length() == 3)
									getSearchItem(country_code, code, 3);
								else if (code.length() == 4)
									getSearchItem(country_code, code, 4);
							}
						} else {
							// m_ButtonCountry.setVisibility(View.VISIBLE);
							country_code = m_ButtonCountry.getText().toString();
							if (country_code != null
									&& country_code.length() > 0) {
								country_code = country_code.substring(1);
								if (msg.length() > 0) {
									if (msg.length() == 1)
										getSearchItem(country_code, msg, 1);
									else if (msg.length() == 2)
										getSearchItem(country_code, msg, 2);
									else if (msg.length() == 3)
										getSearchItem(country_code, msg, 3);
									else if (msg.length() == 4)
										getSearchItem(country_code, msg, 4);
								}
							}
						}
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
											  int count, int after) {
					// do nothing
				}

				@Override
				public void onTextChanged(CharSequence s, int start,
										  int before, int count) {
					// do nothing
				}
			});
		} catch (Exception e) {
			//
		}

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String callType = extras.getString(DialMain.EXTRA_SCHEME_CALL_TYPE);
			String schemeCallNumber = extras.getString(DialMain.EXTRA_SCHEME_CALL_NUMBER);

			if(callType!=null&&schemeCallNumber!=null&& callType.trim().length()>0 && schemeCallNumber.trim().length()>0){
				setCallNumber(schemeCallNumber);
				if (callType.equals("call")){
					mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, schemeCallNumber);
				}
			}

			App.isReturnClicked=extras.getBoolean("ReturnClicked",false);
			if (App.isReturnClicked){
				int RTT = SipService.RTT;
				int LOSS = SipService.LOSS;

				int antena = R.drawable.antena_0;
				if (RTT < 100)
					antena = R.drawable.antena_3;
				else if (RTT < 200)
					antena = R.drawable.antena_2;
				else
					antena = R.drawable.antena_1;

				imgRtt.setImageResource(antena);

				if (LOSS != 0)
					txtRtt.setText(" " + RTT + "ms (" + LOSS + ")");
				else
					txtRtt.setText(" " + RTT + "ms");
			}

		}
	}

	private void setCallNumber(String schemeCallNumber){
        mEditCallNumber.setText(schemeCallNumber);
        mEditCallNumber.requestFocus();
        mEditCallNumber.setSelection(mEditCallNumber.getText().length());
    }

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		reloadWait();

		if (mAdapter != null) {
			mAdapter.destroyAdapter();
			mAdapter = null;
		}
		if (mUrlSchemeCallReceiver != null)  // shlee add check null
			unregisterReceiver(mUrlSchemeCallReceiver);

		unregisterFinishReceiver();
		unregisterDialogReceiver();
	}

	@Override
	protected void onResume() {
		registerUrlSchemeCallReceiver();
		registerUpdateRegStateReceiver();
		registerDialogReceiver();
		registerFinishReceiver();

		Intent regStateChangedIntent = new Intent(SipManager.ACTION_SIP_REGISTRATION_CHANGED);
		regStateChangedIntent.setPackage(getPackageName());
		sendBroadcast(regStateChangedIntent);

		Thread t = new Thread() {
			public void run() {
				dialFeedback.resume();
			};
		};
		if (t != null)
			t.start();

		// registerFavoriteViewReceiver();
		registerKeypadViewReceiver();

		// 설치후 최초 한번만
		/*
		 * if (requestGPS) { Boolean skip =
		 * mPrefs.getPreferenceBooleanValue(AppPrefs.SKIP_GPS); if (skip) {
		 * requestGPS = false; } else {
		 * mPrefs.setPreferenceBooleanValue(AppPrefs.SKIP_GPS, true); } }
		 *
		 * if (!ServicePrefs.checkGps(mContext) && requestGPS) { AlertDialog
		 * msgDialog = new AlertDialog.Builder(mContext).create();
		 * msgDialog.setTitle("GPS");
		 * msgDialog.setMessage(getResources().getString(R.string.gps_quest));
		 * msgDialog.setButton(mContext.getResources().getString(R.string.yes),
		 * new DialogInterface.OnClickListener() {
		 *
		 * @Override public void onClick(DialogInterface dialog, int which) {
		 * startGps(); return; } });
		 * msgDialog.setButton2(mContext.getResources().getString(R.string.no),
		 * new DialogInterface.OnClickListener() {
		 *
		 * @Override public void onClick(DialogInterface dialog, int which) { if
		 * (mLocMan != null && mProvider != null) {
		 * mLocMan.requestLocationUpdates(mProvider, MIN_TIME, MIN_DISTANCE,
		 * mLocationListener); } return; } }); msgDialog.show(); requestGPS =
		 * false; }
		 */

		if (mResetNumber) {
			mResetNumber = false;
			mEditCallNumber.setText("");
		}

		super.onResume();
		DebugLog.d("AppLife onResume in Dial");
		bindToService();

		// if(!app.mContact)
		postCountryCode();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(mKeypadViewReceiver);
		unregisterUpdateRegStateReceiver();
		unbindFromService();
		dialFeedback.pause();
		workStop();
		// stopGps();
		super.onPause();
	}

	@Override
	public void afterTextChanged(Editable s) {
		mHintCallNumber.setVisibility(View.INVISIBLE);
		newNumber = mEditCallNumber.getText().toString();
		Log.d(THIS_FILE,"newNumber:"+newNumber);
		if (newNumber.length() == 0) {
			oldNumber = "";
			clearSearchContact();
			return;
		}
		newNumber = newNumber.replaceAll("-", "");
		if (!oldNumber.equals(newNumber)) {
			oldNumber = newNumber;
			// Search Contact
			// Log.e(THIS_FILE, "PHONE NUMBER IS " + newNumber);

			if (mSkipSearch)
				return;

			if (newNumber.length() > 3)
				doSearchContact(SEARCH_TYPE_PHONE, mEditCallNumber.getText()
						.toString());
			else
				doSearchContact(SEARCH_TYPE_NAME, newNumber);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
								  int after) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		afterTextChanged(mEditCallNumber.getText());
	}

	private void keyPressed(int keyCode) {
		KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
		switch (keyCode) {
			case GlobalConst.USER_KEY_CODE_CONTACT: {
				// 연락처 검색
				goContacts();
				break;
			}
			case GlobalConst.USER_KEY_CODE_SIP: {
				// 통화 SIP
				DebugLog.d("test_v2 key_press code_sip");
				mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
				App.isReturnContactClicked=false;
				goSipCall();
				break;
			}
			case GlobalConst.USER_KEY_CODE_GCALL: {
				// goGCall();
				// showAntena();
				//goSearch();
				goCoupon();
				break;
			}
			case GlobalConst.USER_KEY_CODE_CLOSE: {
				App.isReturnClicked=false;
				finish();
				mPrefs.setPreferenceBooleanValue(AppPrefs.RETURN_ENABLED,false);
				App.isReturnContactClicked=false;
				break;
			}
			default:
				mEditCallNumber.onKeyDown(keyCode, event);
				break;

		}
	}

	@Override
	public void onTrigger(int keyCode, int dialTone) {
		// TODO Auto-generated method stub
		if (dialTone != GlobalConst.TONE_NONE)
			dialFeedback.giveFeedback(dialTone);
		Log.i("Dialer.java-onTrigger", "DialKeyEvent");
		keyPressed(keyCode);
	}

	@Override
	public boolean onLongClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.dial_0: {
				// BJH
				mEditCallNumber.getText().clear();
				dialFeedback.hapticFeedback();
				keyPressed(KeyEvent.KEYCODE_PLUS);
				return true;
			}
			case R.id.dial_star: {
				// BJH 2016.12.02
				/*String flag = mPrefs.getPreferenceStringValue(AppPrefs.PUSH_RECORD_FLAG);
				if(flag != null && flag.length() > 0) {
					return false;
				} else {
					Setting.setPushLayout();
					Alert(mContext, getResources().getString(R.string.db_data_setting));
					mPrefs.setPreferenceStringValue(AppPrefs.PUSH_RECORD_FLAG, "Y");
					return true;
				}*/
			}
			case R.id.btn_number_delete: {
				mEditCallNumber.getText().clear();
				mCallNumberDelete.setPressed(false);
				clearSearchContact();
				// BJH
				m_CountryCode = null;
				m_CountryFlag = null;
				UpdateCode();
				return true;
			}
		}
		return false;
	}

	private void setKeypadVisible() {
		mCallNumberDelete.setVisibility(View.VISIBLE);
		mShortDialpad.setVisibility(View.GONE);
		// BJH
		// mShortFavorite.setVisibility(View.VISIBLE);

		//mHintCallNumber.setVisibility(View.INVISIBLE); // BJH 2016.12.29
		mEditCallNumber.setVisibility(View.VISIBLE);
		mEditCallNumber.requestFocus();
		mKeypadView.setVisibility(View.VISIBLE);
		mKeypadMode = true;
	}

	private void setFavoriteVisible() {
		mEditCallNumber.setText("");
		mCallNumberDelete.setVisibility(View.GONE);

		mShortDialpad.setVisibility(View.VISIBLE);
		mShortFavorite.setVisibility(View.GONE);

		mEditCallNumber.setCursorVisible(false);
		//mHintCallNumber.setVisibility(View.VISIBLE); // BJH 2016.12.29
		mFavoritesListView.setVisibility(View.VISIBLE);
		mKeypadView.setVisibility(View.INVISIBLE);
		mKeypadMode = false;
	}

	/*
	 * private int getDistance(Double longitudeA, Double latitudeA, Double
	 * longitudeB, Double latitudeB) { float distance = 0.0f;
	 *
	 * Location locationA = new Location("point A");
	 *
	 * locationA.setLatitude(latitudeA); locationA.setLongitude(longitudeA);
	 *
	 * Location locationB = new Location("point B");
	 *
	 * locationB.setLatitude(latitudeB); locationB.setLongitude(longitudeB);
	 *
	 * distance = locationA.distanceTo(locationB);
	 *
	 * return (int)distance; }
	 *
	 * LocationListener mLocationListener = new LocationListener() {
	 *
	 * @Override public void onLocationChanged(Location arg0) {
	 * Log.d(THIS_FILE,"onLocationChanged");
	 *
	 * Double myLatitude =
	 * Util.parseDouble(mPrefs.getPreferenceStringValue(AppPrefs
	 * .LAST_LOC_LATITUDE)); Double myLongitude =
	 * Util.parseDouble(mPrefs.getPreferenceStringValue
	 * (AppPrefs.LAST_LOC_LATITUDE)); if (mFirstLoad) { mFirstLoad = false;
	 * mPrefs.setPreferenceStringValue(AppPrefs.LAST_LOC_LONGITUDE,
	 * String.format("%.6f", arg0.getLongitude()));
	 * mPrefs.setPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE,
	 * String.format("%.6f", arg0.getLatitude())); }
	 *
	 * int distance =
	 * getDistance(arg0.getLongitude(),arg0.getLatitude(),myLongitude
	 * ,myLatitude);
	 *
	 * if(distance < 500) { return; }
	 *
	 * // TODO Auto-generated method stub mMyLongitude = arg0.getLongitude(); //
	 * 경도 mMyLatitude = arg0.getLatitude(); // 위도
	 *
	 * mPrefs.setPreferenceStringValue(AppPrefs.LAST_LOC_LONGITUDE,
	 * String.format("%.6f", mMyLongitude));
	 * mPrefs.setPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE,
	 * String.format("%.6f", mMyLatitude));
	 *
	 * updateLocationScore(); }
	 *
	 * @Override public void onProviderDisabled(String arg0) { // TODO
	 * Auto-generated method stub // 현재상태 : 서비스 사용 불가
	 * Log.d(THIS_FILE,"onProviderDisabled"); }
	 *
	 * @Override public void onProviderEnabled(String arg0) { // TODO
	 * Auto-generated method stub // 현재상태 : 서비스 사용 가능
	 * Log.d(THIS_FILE,"onProviderEnabled"); }
	 *
	 * @Override public void onStatusChanged(String arg0, int arg1, Bundle arg2)
	 * { // TODO Auto-generated method stub Log.d(THIS_FILE,"onStatusChanged");
	 *
	 * }
	 *
	 * };
	 *
	 * private void changeBestListener() { if (mLocMan != null) {
	 * mLocMan.removeUpdates(mLocationListener); mProvider =
	 * mLocMan.getBestProvider(new Criteria(), true); if (mProvider != null)
	 * mLocMan.requestLocationUpdates(mProvider, MIN_TIME, MIN_DISTANCE,
	 * mLocationListener); } }
	 *
	 * private void changeGpsListener() { String gpsSettings =
	 * android.provider.Settings.Secure.getString(getContentResolver(),
	 * android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	 *
	 * if (gpsSettings.indexOf("gps", 0) < 0) return;
	 *
	 * if (mLocMan != null) { mLocMan.removeUpdates(mLocationListener);
	 * mLocMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME,
	 * MIN_DISTANCE, mLocationListener); } }
	 */

	// private boolean checkGps()
	// {
	// String gpsSettings =
	// android.provider.Settings.Secure.getString(getContentResolver(),
	// android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	// if (gpsSettings.indexOf("gps", 0) < 0) return false;
	// return true;
	// }

	/*
	 * private void startGps() { DialMain.skipClose = true;
	 *
	 * Intent intent = new
	 * Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	 * intent.addCategory(Intent.CATEGORY_DEFAULT);
	 * startActivityForResult(intent, START_GPS); }
	 *
	 * private void stopGps() { if(ServicePrefs.checkGps(mContext)) { final
	 * Intent poke = new Intent(); poke.setClassName("com.android.settings",
	 * "com.android.settings.widget.SettingsAppWidgetProvider");
	 * poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	 * poke.setData(Uri.parse("3")); this.mContext.sendBroadcast(poke); } }
	 *
	 * //점수 재계산
	 *
	 * private void updateLocationScore() { DBManager database = new
	 * DBManager(mContext); try { database.open();
	 *
	 * //reset location; database.updateResetScoreLocation();
	 *
	 * Double myLatitude =
	 * Util.parseDouble(mPrefs.getPreferenceStringValue(AppPrefs
	 * .LAST_LOC_LATITUDE)); Double myLongitude =
	 * Util.parseDouble(mPrefs.getPreferenceStringValue
	 * (AppPrefs.LAST_LOC_LATITUDE));
	 *
	 * //String callNumber; Double curLongitude,curLatitude; int zone_id; int
	 * distance = 0;
	 *
	 * Cursor c = database.getZones();
	 *
	 * if(c != null) { if (c.moveToFirst()) { do { zone_id =
	 * c.getInt(c.getColumnIndex(ZoneData.FIELD_ZONE_ID)); curLatitude =
	 * c.getDouble(c.getColumnIndex(ZoneData.FIELD_LATITUDE)); curLongitude =
	 * c.getDouble(c.getColumnIndex(ZoneData.FIELD_LONGITUDE)); //callNumber =
	 * c.getString(c.getColumnIndex(ZoneData.FIELD_NUMBER)); distance =
	 * getDistance(curLongitude,curLatitude,myLongitude,myLatitude);
	 *
	 * //거리 업데이트 database.updateZoneDistance(zone_id, distance); }
	 * while(c.moveToNext()); } c.close(); } database.updateScoreZone();
	 * database.updateScore();
	 *
	 * } finally { database.close(); } }
	 */

	private void UpdateScore() {
		DBManager database = new DBManager(mContext);
		try {
			database.open();

			// reset location;
			database.updateScoreCTime();
			database.updateScore();

		} finally {
			database.close();
		}
	}

	// 연락처 검색
	private void goContacts() {
		// app.mContact = true;
		if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
			return;
		}

		mLastClickTime = SystemClock.elapsedRealtime();

		Intent intent = new Intent(Dial.this, Contacts.class);
		intent.putExtra("SelectType", 0); // 0:contacts ,1 :favorites
		intent.putExtra("Type","POPUP");
		startActivityForResult(intent, SELECT_CONTACT);
	}

	// 검색
	private void goSearch() {
		Intent intent = new Intent(Dial.this, Search.class);
		startActivityForResult(intent, SELECT_SEARCH);
	}

	// 쿠폰
	private void goCoupon() {
		/*String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_COUPON_INFO);
		Intent intent = new Intent(Dial.this, WebClient.class);
		intent.putExtra("url", url);
		intent.putExtra("title",
				getResources().getString(R.string.title_coupon_and_search));
		startActivity(intent);*/
		String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_COUPON_INFO);
		if (url == null || url.length() == 0)
			return;
		Intent intent = new Intent(mContext, WebClient.class);
		intent.putExtra("url", url);
		intent.putExtra("title",
				getResources().getString(R.string.title_coupon));
		startActivity(intent);


		/*Intent intent = new Intent(Dial.this, TabTravelCouponActivity.class);
		intent.putExtra("url", url);
		intent.putExtra("title",
				getResources().getString(R.string.title_coupon_and_search));
		startActivityForResult(intent, SELECT_SEARCH);*/
	}



	// SIP CALL
	private void goSipCall() {
		String number = mEditCallNumber.getText().toString();
		if (number.length() > 0) {
			mResetNumber = true;
			String call_number = number.replace("-", "");
			if(!(call_number.startsWith("+") || call_number.startsWith("001") || call_number.startsWith("002") || call_number.startsWith("00700") || call_number.startsWith("009"))) { // BJH 2017.01.17 사장님 요청 사항
				if(call_number.substring(0, 1).equals("1") && call_number.length() == 11) { // BJH 2017.01.17 사장님 요청 사항 => 2017.02.10 사장님 요청 변경 11자리로
					//call_number = "+" + call_number; // BJH 2017.07.05 중국 번호 이슈 사항
					if(m_NumberCode.equals("+1")) // BJH 2017.07.05 사장님 요청
						call_number = "+" + call_number;
					else
						call_number = m_NumberCode.concat(call_number);
				} else {
					if (!m_NumberCode.equals("+82") && m_NumberCode.length() > 0) {
						/*if (call_number.substring(0, 1).equals("0")) {
							if(!call_number.startsWith("010")) //BJH 2016.09.19 사장님 요청
								call_number = m_NumberCode.concat(call_number); // BJH 2016.12.22 짧은 번호 인 경우 이슈
						}
						else
							call_number = m_NumberCode.concat(call_number);*/
						if(!call_number.startsWith("010")) // BJH 2017.01.17 사장님 요청 사항
							call_number = m_NumberCode.concat(call_number);
					} else {
						if(m_NumberCode.concat(call_number).startsWith("+8210"))
							call_number = m_NumberCode.concat(call_number);
					}
				}
			}
			if (App.isReturnClicked){
				doSipCall(call_number);
			}else {
				sipCall(call_number);
			}

		} else {
			// 마지막으로 통화한 번호로 전화걸기
			if (mPrefs.getPreferenceStringValue(AppPrefs.LAST_CALL_NUMBER)
					.length() > 0) {
				mEditCallNumber.setText("");
				// BJH
				getSearchItem(mPrefs
						.getPreferenceStringValue(AppPrefs.LAST_CALL_NUMBER));
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.d(THIS_FILE,"onNewIntent");
	}

	private void goScoreList() {
		Intent intent = new Intent(Dial.this, ScoreList.class);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(THIS_FILE,"onActivityResult:"+requestCode);
		switch (requestCode) {
			case SELECT_CONTACT:
				if (resultCode == RESULT_OK) {
					// mIsDialView = true;
					mEditCallNumber.setText("");
					// mEditCallNumber.append(data.getStringExtra("PhoneNumber"));
					//BJH 2016.06.07 연락처에서 가져온 번호가 국가코드가 없고 다국어 지원 상태에 따라 국가코드 변경 (사장님 요청 사항)
					String number = data.getStringExtra("PhoneNumber");
					//String buttonStr = m_ButtonCountry.getText().toString();
					//if(!number.startsWith("+")) {
					//BJH 2016.06.29 불편사항 수정
					//number = mPrefs.getPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE) + number;
					//BJH 2016.08.16 사장님 요청사항 +가 없는 경우 무조건 +82로 걸리도록
					//number = AuthActivity.KOREA_CODE + number;
					/*if(mLocaleStr.equals("ko_KR") && !buttonStr.equals(AuthActivity.KOREA_CODE))
						number = AuthActivity.KOREA_CODE + number;
					else if(mLocaleStr.equals("ja") && !buttonStr.equals(AuthActivity.JAPAN_CODE))
						number = AuthActivity.JAPAN_CODE + number;
					else if(mLocaleStr.equals("zh_CN") && !buttonStr.equals(AuthActivity.CHINA_CODE))
						number = AuthActivity.CHINA_CODE + number;
					else if(mLocaleStr.equals("en_US") && !buttonStr.equals(AuthActivity.USA_CODE))
						number = AuthActivity.USA_CODE + number;*/

					//}
				/*if(!(number.startsWith("+") || number.startsWith("001"))) // BJH 2016.12.01 사장님 요청으로 롤백
					number = m_NumberCode.concat(number);*/ //BJH 2017.01.17 아이폰과 동일하게
					getSearchItem(number);
					// mIsDialView = false;
				}
				break;
			case SELECT_FAVORITE:
				if (resultCode == RESULT_OK) {
					int rs = addFavorites(
							data.getStringExtra(FavoritesData.FIELD_DISPLAYNAME),
							data.getStringExtra(FavoritesData.FIELD_PHONENUMBER));
					if (rs == RESULT_ADD_FAIL) {
						handler.sendEmptyMessage(DIAL070_CONTACT_ADD_FAIL);
					} else if (rs == RESULT_ADD_USER) {
						handler.sendEmptyMessage(DIAL070_CONTACT_ADD_FAVORITE);
					} else if (rs == RESULT_EXIST_USER) {
						handler.sendEmptyMessage(DIAL070_CONTACT_EXIST_USER);
					}
				}
				break;
			case SELECT_SEARCH_RESULT: {
				mResultMore.setEnabled(true);
				if (resultCode == RESULT_OK) {
					mSkipSearch = true;

					mEditCallNumber.setText("");
					//mEditCallNumber.append(data.getStringExtra("PhoneNumber"));
					//BJH 2016.08.16 사장님 요청 사항
					String number = data.getStringExtra("PhoneNumber");
				/*if(!number.startsWith("+"))
					number = AuthActivity.KOREA_CODE + number;*/
				/*if(!(number.startsWith("+") || number.startsWith("001"))) // BJH 2016.12.01 사장님 요청으로 롤백
					number = m_NumberCode.concat(number);*/ //BJH 2017.01.17 아이폰과 동일하게
					getSearchItem(number);

					mTextResultName.setText(data.getStringExtra("DisplayName"));
					mTextResultPhone.setText(data.getStringExtra("PhoneNumber"));

					mSkipSearch = false;

					// 선택된 전화번호로 검색
					doSearchContact(SEARCH_TYPE_PHONE, mEditCallNumber.getText()
							.toString());
				}
			}
			break;
			case START_GPS:
				if (resultCode == RESULT_CANCELED) {
					// changeGpsListener();
				}
				break;
			case SELECT_SEARCH:
				if (resultCode == RESULT_OK) {
					// mIsDialView = true;
					mEditCallNumber.setText("");
					// mEditCallNumber.append(data.getStringExtra("PhoneNumber"));
				/*if (data.getStringExtra("PhoneNumber").startsWith("+")) // BJH 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
					getSearchItem(data.getStringExtra("PhoneNumber"));
				else
					getSearchItem("+82" + data.getStringExtra("PhoneNumber"));*/
					String type = data.getStringExtra("Type"); // BJH 2016.12.01 사장님 요청 사항
					String number = data.getStringExtra("PhoneNumber");
				/*if(type != null && type.length() > 0) { //Naver 검색
					if (number.startsWith("+")) // BJH 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
						getSearchItem(number);
					else
						getSearchItem("+82" + number);
				} else {
					if(!(number.startsWith("+") || number.startsWith("001")))
						getSearchItem(m_NumberCode.concat(number));
					else
						getSearchItem(number);
				}*/
				/* BJH 2017.02.06 */
					if(type != null && type.length() > 0) { //Naver 검색
						if (type.equals("naver") && !number.startsWith("+")) // 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
							codeInit("+82", "kr");
						else if(type.equals("call_center") && number.startsWith("+82")) { // 콜센터인 경우 +82로 국가번호로 하고 거는 번호는 +82를 지운다.
							codeInit("+82", "kr");
							number = number.replace("+82", "");
						}
					}
					getSearchItem(number);
					// mIsDialView = false;
				}
				break;
			case SELECT_COUNTRY:
				if (resultCode == RESULT_OK) {
					Log.d(THIS_FILE, "RESULT : " + data.toString());
					m_CountryCode = data.getStringExtra("COUNTRY_CODE");
					m_CountryFlag = data.getStringExtra("COUNTRY_FLAG");
					if (m_CountryCode != null && m_CountryCode.length() > 0)
						mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE,
								m_CountryCode);
					if (m_CountryFlag != null && m_CountryFlag.length() > 0)
						mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG,
								m_CountryFlag);
					UpdateCode();
				}
				break;
			default:
				break;
		}
		return;
	}

	public static final int MENU_OPTION_ADD_CONTACT = Menu.FIRST + 1;
	public static final int MENU_OPTION_SEND_SMS = Menu.FIRST + 2;
	public static final int MENU_OPTION_GO_FAVORITE = Menu.FIRST + 3;
	public static final int MENU_OPTION_CLOSE = Menu.FIRST + 4;
	public static final int MENU_OPTION_EDIT_FAVORITE = Menu.FIRST + 5;
	public static final int MENU_OPTION_DONE_FAVORITE = Menu.FIRST + 6;
	public static final int MENU_OPTION_SCORE_LIST = Menu.FIRST + 7;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (menu != null) {
			if (mKeypadMode) {
				menu.add(Menu.NONE, MENU_OPTION_ADD_CONTACT, Menu.NONE,
						R.string.add_contact);
				menu.add(Menu.NONE, MENU_OPTION_SEND_SMS, Menu.NONE,
						R.string.send_sms);
				menu.add(Menu.NONE, MENU_OPTION_GO_FAVORITE, Menu.NONE,
						R.string.go_favorite);
				menu.add(Menu.NONE, MENU_OPTION_CLOSE, Menu.NONE,
						R.string.close_dial070);
			} else {
				menu.add(Menu.NONE, MENU_OPTION_EDIT_FAVORITE, Menu.NONE,
						R.string.edit_favorite);
				menu.add(Menu.NONE, MENU_OPTION_DONE_FAVORITE, Menu.NONE,
						R.string.done_favorite);
				if (ServicePrefs.DIAL_SHOW_SCORE)
					menu.add(Menu.NONE, MENU_OPTION_SCORE_LIST, Menu.NONE,
							R.string.score_list);
			}
		}
		// Log.e(THIS_FILE,"OPTION = onCreateOptionsMenu");
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (menu != null) {
			menu.clear();
			if (mKeypadMode) {
				menu.add(Menu.NONE, MENU_OPTION_ADD_CONTACT, Menu.NONE,
						R.string.add_contact);
				menu.add(Menu.NONE, MENU_OPTION_SEND_SMS, Menu.NONE,
						R.string.send_sms);
				menu.add(Menu.NONE, MENU_OPTION_GO_FAVORITE, Menu.NONE,
						R.string.go_favorite);
				menu.add(Menu.NONE, MENU_OPTION_CLOSE, Menu.NONE,
						R.string.close_dial070);
			} else {
				menu.add(Menu.NONE, MENU_OPTION_EDIT_FAVORITE, Menu.NONE,
						R.string.edit_favorite);
				menu.add(Menu.NONE, MENU_OPTION_DONE_FAVORITE, Menu.NONE,
						R.string.done_favorite);
				if (ServicePrefs.DIAL_SHOW_SCORE)
					menu.add(Menu.NONE, MENU_OPTION_SCORE_LIST, Menu.NONE,
							R.string.score_list);
			}
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		// Log.e(THIS_FILE,"OPTION = onOptionsItemSelected");
		// if(mKeypadView.getVisibility() == View.VISIBLE)
		if (mKeypadMode) {
			switch (item.getItemId()) {
				case MENU_OPTION_ADD_CONTACT:
					addContact();
					return true;
				case MENU_OPTION_SEND_SMS:
					//sendSMS();
					goSMSWrite();
					return true;
				case MENU_OPTION_GO_FAVORITE:
					goFavorite();
					return true;
				case MENU_OPTION_CLOSE:
					closeDial070();
					return true;
				default:
					break;
			}
		} else {
			switch (item.getItemId()) {
				case MENU_OPTION_EDIT_FAVORITE:
					setFavoriteMode(FAVORITE_EDIT_MODE);
					return true;
				case MENU_OPTION_DONE_FAVORITE:
					setFavoriteMode(FAVORITE_VIEW_MODE);
					return true;
				case MENU_OPTION_SCORE_LIST:
					goScoreList();
					return true;
				default:
					break;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	private BroadcastReceiver regStateReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			// updateOnlineStatus();
		}
	};

	private BroadcastReceiver natDetectReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			/*
			 * if (mProgressDialog != null && mProgressDialog.isShowing()) {
			 * mProgressDialog.dismiss(); mProgressDialog = null; }
			 *
			 * if (!mDetectNatTypeReq) return; mDetectNatTypeReq = false;
			 *
			 * int type = intent.getIntExtra(SipManager.EXTRA_NAT_TYPE, 0);
			 * showNetworkType(type);
			 */
		}
	};

	private boolean checkOnline() {
		DebugLog.d("checkOnline()");

		if (mSipService == null) {
			FirebaseCrashlytics.getInstance().setCustomKey("check_online_sip_service", false);
			return false;
		}
		FirebaseCrashlytics.getInstance().setCustomKey("check_online_sip_service", true);
		SipProfileState accountInfo = null;
		try {
			accountInfo = mSipService.getSipProfileState(0);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FirebaseCrashlytics.getInstance().recordException(new Exception(e.getMessage()));
			return false;
		}
		if(accountInfo==null) {
			FirebaseCrashlytics.getInstance().setCustomKey("check_online_account_info", false);

		} else {
			FirebaseCrashlytics.getInstance().setCustomKey("check_online_account_info", true);
			FirebaseCrashlytics.getInstance().setCustomKey("check_online_account_info_is_valid_for_call", accountInfo.isValidForCall());
		}

		return (accountInfo != null && accountInfo.isValidForCall());
	}

	private void updateRegState() {
		DebugLog.d("updateRegState()");
		DebugLog.d("AppLife updateRegState called.");

		Intent intentDial=new Intent(Dial.BC_UPDATE_REG_STATE);
		if (checkOnline()) {
            /*int RTT = SipService.RTT;
            int LOSS = SipService.LOSS;
            intentDial.putExtra("RTT",RTT);
            intentDial.putExtra("LOSS",LOSS);*/
			intentDial.putExtra("ONLINE_STATE",true);
		}else{
			intentDial.putExtra("ONLINE_STATE",false);
		}
		intentDial.setPackage(getPackageName());
		sendBroadcast(new Intent(intentDial));
	}

	private Activity mContextToBindTo = this;
	 private ISipService mSipService = null;
	private ServiceConnection mSipConnection = null;

	private boolean bindToService() {
		if (mSipConnection == null) {
			mSipConnection = new ServiceConnection() {
				public void onServiceConnected(ComponentName name,
											   IBinder binder) {
					DebugLog.d("AppLife bindToService onServiceConnected...");

					 mSipService = ISipService.Stub.asInterface(binder);
					 // add
					boolean stateOnline = checkOnline();

					DebugLog.d("AppLife bindToService onServiceConnected stateOnline: "+stateOnline);
					DebugLog.d("AppLife bindToService onServiceConnected rtt: "+SipService.RTT);

					FirebaseCrashlytics.getInstance().setCustomKey("state_online_in_dial_bind_service", stateOnline);
					FirebaseCrashlytics.getInstance().setCustomKey("rtt_in_dial_bind_service", SipService.RTT);

					int count =0;

					Thread thread = new Thread(() -> {
						for(int i=0; i<5; i++) {
							try {
								if(!checkOnline()) {
									Thread.sleep(1000);
									updateRegState();
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
					}
					});
					thread.start();

//					while(!checkOnline()) {
//							try {
//
//								Thread.sleep(1000);
//								updateRegState();
//
//							} catch (InterruptedException e) {
//								e.printStackTrace();
//							}
//						}

//					if (stateOnline){
//						int RTT = SipService.RTT;
//						int LOSS = SipService.LOSS;
//
//						int antena = R.drawable.antena_0;
//						if (RTT < 100)
//							antena = R.drawable.antena_3;
//						else if (RTT < 200)
//							antena = R.drawable.antena_2;
//						else
//							antena = R.drawable.antena_1;
//
//						imgRtt.setImageResource(antena);
//
//						if (LOSS != 0)
//							txtRtt.setText(" " + RTT + "ms (" + LOSS + ")");
//						else
//							txtRtt.setText(" " + RTT + "ms");
//					}else{
//						imgRtt.setImageResource(R.drawable.antena_0);
//						txtRtt.setText("");
//					}
				}

				public void onServiceDisconnected(ComponentName name) {
					// mSipService = null;
					DebugLog.d("AppLife bindToService onServiceDisconnected...");
				}
			};
		}

		mContextToBindTo.bindService(new Intent(mContextToBindTo,
				SipService.class), mSipConnection, BIND_AUTO_CREATE);
		registerReceiver(regStateReceiver, new IntentFilter(
				SipManager.ACTION_SIP_REGISTRATION_CHANGED));
		registerReceiver(natDetectReceiver, new IntentFilter(
				SipManager.ACTION_SIP_NAT_DETECT_CHANGED));

		return true;
	}

	private void unbindFromService() {
		if (mSipConnection != null) {
			mContextToBindTo.unbindService(mSipConnection);
		}

		try {
			unregisterReceiver(regStateReceiver);
		} catch (Exception e) {
			Log.w(THIS_FILE, "Unable to unregisterReceiver", e);
		}

		try {
			unregisterReceiver(natDetectReceiver);
		} catch (Exception e) {
			Log.w(THIS_FILE, "Unable to natDetectReceiver", e);
		}
	}

	// 전화걸기
	private boolean sipCall(String number) {
		// check number
		DebugLog.d("test_v2 sipCall in Dial called. number: "+number);

		if (number == null) {
			return false;
		}
		//BJH 2016.06.28 초기화
		//if (Build.VERSION.SDK_INT < 21)
		//	mEditCallNumber.setText("");
		//BJH 2016.06.29 국가번호 유지를 위해서

		if(number.startsWith("00982")) {
			number = number.replace("00982", "0");
		}

		mPrefs.setPreferenceStringValue(AppPrefs.LAST_CALL_NUMBER,
				number);

		Intent intent;
		if (App.isReturnClicked){
			intent = new Intent(DialMain.ACTION_DIAL_COMMAND_RETURN);
		}else {
			intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
		}

		intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
		intent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
		intent.setPackage(getPackageName());
		sendBroadcast(intent);

		return true;
	}

	private void doSipCall(final String number) {
		DialMain.skipClose = true;

		Intent intent;
		if (App.isReturnClicked){
			intent = new Intent(SipManager.ACTION_SIP_CALL_RETURN_UI);
		}else {
			intent = new Intent(SipManager.ACTION_SIP_CALL_UI);
		}
		intent.putExtra(SipManager.EXTRA_CALL_NUMBER, number);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP /*
		 * | Intent.
		 * FLAG_ACTIVITY_NO_ANIMATION
		 */);
		intent.setPackage(getPackageName());
		startActivity(intent);
	}

	/*
	 * 상단 잔액 백그라운드 #5acdfc 키패드 검색 백그라운드 #31ace3 검색어 번호 활성화 #75f7ff 키패드 하단 백그라운드
	 * #f7f7f8 전화번호 입력 텍스트 #4f4f4f 전화번호 입력 백그라운드 #ffffff 검색 결과 텍스트 이름 #013e68
	 * 즐겨찾기 백그라운드 #e6eef5
	 */

	private String getCurrentMemo(int id) {
		String rs = "";
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			Cursor c = database.getFavorite(id);

			if (c != null) {
				if (c.getCount() > 0 && c.moveToFirst()) {
					rs = c.getString(c
							.getColumnIndex(FavoritesData.FIELD_FIELD2));
				}
				c.close();
			}

		} finally {
			database.close();
		}

		return rs;
	}

	private void PopupDialog(int id, String name, String number) {
		// Get Memo
		String memo = getCurrentMemo(id);

		Intent intent = new Intent(Dial.this, PopupFavorite.class);
		intent.putExtra("NAME", name);
		intent.putExtra("PHONE", number);
		intent.putExtra("ID", id);
		intent.putExtra("MEMO", memo);
		startActivity(intent);
	}

	private void applySearchResult() {
		if (mContactList != null) {
			if (mContactList.size() > 0) {
				mLinearSearch.setVisibility(View.VISIBLE);
				mTextResultName.setVisibility(View.VISIBLE);
				mTextResultPhone.setVisibility(View.VISIBLE);
				mTextResultCount.setVisibility(View.VISIBLE);
				mResultMore.setVisibility(View.VISIBLE);
				mResultMore.setEnabled(true);
				ContactsData item = mContactList.get(0);
				mTextResultName.setText(item.getDisplayName());
				if (mSearchFirstContact != null
						&& mSearchFirstContact.length() > 0)
					mTextResultPhone
							.setText(Html.fromHtml(mSearchFirstContact)); // Html.fromHtml(item.getPhoneNumber()));
				else
					mTextResultPhone.setText(Html.fromHtml(item
							.getPhoneNumber()));
				mTextResultCount.setText(Integer.toString(mContactList.size()));
			} else {
				mLinearSearch.setVisibility(View.INVISIBLE);
				mTextResultName.setVisibility(View.INVISIBLE);
				mTextResultPhone.setVisibility(View.INVISIBLE);
				mTextResultCount.setVisibility(View.INVISIBLE);
				mResultMore.setVisibility(View.INVISIBLE);
			}
		} else {
			mLinearSearch.setVisibility(View.INVISIBLE);
			mTextResultName.setVisibility(View.INVISIBLE);
			mTextResultPhone.setVisibility(View.INVISIBLE);
			mTextResultCount.setVisibility(View.INVISIBLE);
			mResultMore.setVisibility(View.INVISIBLE);
		}

		loadMatchContact = false;
	}

	private void searchResultToCallNumber() {
		if (mContactList != null && mContactList.size() > 0) {
			ContactsData item = mContactList.get(0);
			String number = Html.fromHtml(item.getPhoneNumber()).toString();
			mEditCallNumber.setText("");
			//mEditCallNumber.append(number);
			/*// BJH 2016.08.16 사장님 요청 사항
			if(!(number.startsWith("+") || number.startsWith("001")))
				number = m_NumberCode.concat(number); //BJH 2016.12.01 사장님 요청으로 롤백 => 앞에 국가번호를 붙일필요가 없음*/
			getSearchItem(number);
		}
	}

	// 검색키에 해당하는 리스트 아이템을 개수를 구함
	private ArrayList<ContactsData> getSearchItemByPhone(String searchKey) {
		if (mContactList == null)
			return null;

		// 초기화
		mContactList.clear();

		ContactsData item;

		if (searchKey.length() == 0)
			return mContactList;

		Cursor c = null;
		try {

			String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
					+ " COLLATE LOCALIZED ASC";
			String selection = null;
			if (Compatibility.getApiLevel() > 10)
				selection = ContactsContract.Contacts.HAS_PHONE_NUMBER
						+ " > 0 ";

			c = mContext
					.getContentResolver()
					.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							new String[] {
									ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
									ContactsContract.CommonDataKinds.Phone.NUMBER },
							selection, null, sortOrder);
		} catch (IllegalArgumentException e) {
			return null;
		}

		if (c == null)
			return mContactList;
		boolean mFirst = true;
		mSearchFirstContact = "";

		Map<String, String> phoneList = new HashMap<String, String>();
		phoneList.clear();

		while (c.moveToNext()) {
			String contactPhoneNumber = null;
			String contactName = null;
			String comparePhoneNumber = null;
			contactName = c
					.getString(c
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			contactPhoneNumber = c
					.getString(c
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			comparePhoneNumber = contactPhoneNumber.replaceAll("-", "");

			// if(comparePhoneNumber.startsWith(searchKey))
			if (comparePhoneNumber.contains(searchKey)) {

				// 중복 검사
				if (ServicePrefs.DIAL_SKIP_DUP_CONTACTS) {
					String key = comparePhoneNumber;
					if (phoneList.containsKey(key)) {
						continue;
					}
					phoneList.put(key, "1");
				}

				if (mFirst) {
					mSearchFirstContact = comparePhoneNumber.replace(searchKey,
							"<font color='#00A1C8'><b>" + searchKey
									+ "</b></font>");
					mFirst = false;
				}
				comparePhoneNumber = comparePhoneNumber
						.replace(searchKey, "<font color='#00A1C8'><b>"
								+ searchKey + "</b></font>"); // 75f7ff
				// Log.e(THIS_FILE,"NAME=" + contactName + "  PHONE NUMBER=" +
				// comparePhoneNumber);

				item = new ContactsData(contactName, comparePhoneNumber, "",
						"", null);
				mContactList.add(item);
			}
		}
		c.close();

		return mContactList;
	}

	// 초성검색
	private ArrayList<ContactsData> getSearchItemByName(String searchKey) {
		if (mContactList == null)
			return null;

		// 초기화
		mContactList.clear();

		// 한글 초성 검색만 지원하는 경우 '9' 제외
		if (searchKey.contains("+") || searchKey.contains("1")
				|| searchKey.contains("2") || searchKey.contains("3")
				|| searchKey.contains("*") || searchKey.contains("#"))
			return mContactList;

		ArrayList<String> searchInitail = getSearchInitail(searchKey);

		ContactsData item;
		String find_name;

		if (searchInitail.size() == 0)
			return mContactList;

		Cursor c = null;
		try {

			String sortOrder = ContactsContract.Contacts.DISPLAY_NAME
					+ " COLLATE LOCALIZED ASC";
			String selection = null;
			if (Compatibility.getApiLevel() > 10)
				selection = ContactsContract.Contacts.HAS_PHONE_NUMBER
						+ " > 0 ";

			c = mContext
					.getContentResolver()
					.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							new String[] {
									ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
									ContactsContract.CommonDataKinds.Phone.NUMBER },
							selection, null, sortOrder);
		} catch (IllegalArgumentException e) {
			return null;
		}

		if (c == null)
			return mContactList;

		Map<String, String> phoneList = new HashMap<String, String>();
		phoneList.clear();

		while (c.moveToNext()) {
			String contactPhoneNumber = null;
			String contactName = null;

			contactName = c
					.getString(c
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			contactPhoneNumber = c
					.getString(c
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			// 중복 검사
			if (ServicePrefs.DIAL_SKIP_DUP_CONTACTS) {
				String key = contactPhoneNumber;
				key = key.replaceAll("-", "");
				if (phoneList.containsKey(key)) {
					continue;
				}
				phoneList.put(key, "1");
			}

			for (int i = 0; i < searchInitail.size(); i++) {
				find_name = HangulUtils.getHangulInitialSound(contactName,
						searchInitail.get(i));
				if (find_name != null && find_name.length() > 0 && find_name.indexOf(searchInitail.get(i)) >= 0) { // BJH 2017.10.12  SM-G950U1(Galaxy S8) 초성검색 오류
					contactPhoneNumber = contactPhoneNumber.replaceAll("-", "");

					item = new ContactsData(contactName, contactPhoneNumber,
							"", "", null);
					mContactList.add(item);
				}
			}
		}
		c.close();

		return mContactList;
	}

	private ArrayList<String> getSearchInitail(String keyword) {
		Log.d(THIS_FILE,"getSearchInitail:"+keyword);
		// 2 - 'ㄱ ㅋㄲ'
		// 3 - 'ㄴ ㄹ'
		// 4 - 'ㄷ ㅌㄸ'
		// 5 - 'ㅁ ㅅㅆ'
		// 6 - 'ㅂ ㅍㅃ'
		// 7 - 'ㅇ ㅎ'
		// 8 - 'ㅈ ㅊㅉ'
		// 9 - 'WXYZ'
		String[] s = { "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "", "", "", "", "", "", "", "", "", "", "", "" };
		String[] init = { "", "", "", "", "", "", "", "", ""};

		ArrayList<String> result = new ArrayList<String>();
		int index = 0;
		int count = 0;
		// count = (int)Math.pow(3, keyword.length());
		String hangul_chosung;
		for (int i = 0; i < keyword.length(); i++) {
			try {
				hangul_chosung = HangulUtils.INITIAL_HANGUL_KEYPAD[Integer
						.parseInt(Character.toString(keyword.charAt(i)))];
			} catch (Exception e) {
				continue;
			}

			init[(i * 3)] = Character.toString(hangul_chosung.charAt(0));
			init[(i * 3) + 1] = Character.toString(hangul_chosung.charAt(1));
			if (hangul_chosung.length() >= 3)
				init[(i * 3) + 2] = Character
						.toString(hangul_chosung.charAt(2));
			else
				init[(i * 3) + 2] = "";
		}

		int i, j, k;
		i = j = k = 0;

		for (i = 0; i < 3; i++) {
			if (init[i].length() == 0)
				continue;
			s[index] = init[i];
			if (keyword.length() > 1) {
				for (j = 0; j < 3; j++) {
					if (init[3 + j].length() == 0)
						continue;
					s[index] = init[i] + init[3 + j];
					if (keyword.length() > 2) {
						for (k = 0; k < 3; k++) {
							if (init[6 + k].length() == 0)
								continue;
							s[index] = init[i] + init[3 + j] + init[6 + k];
							index++;
						}
					} else {
						index++;
					}
				}
			} else {
				index++;
			}
		}
		count = index;

		for (i = 0; i < count; i++) {
			result.add(s[i]);
			// Log.e(THIS_FILE,"LIST="+s[i]);
		}
		return result;
	}

	private void clearSearchContact() {
		mContactList.clear();
		mTextResultName.setVisibility(View.INVISIBLE);
		mTextResultPhone.setVisibility(View.INVISIBLE);
		mTextResultCount.setVisibility(View.INVISIBLE);
		mResultMore.setVisibility(View.INVISIBLE);
		mLinearSearch.setVisibility(View.INVISIBLE);
	}

	private boolean pendSearch = false;
	private int pendSearchType = 0;
	private String pendSearchKey = null;

	private void doSearchContact(int type, String number) {
		if (number == null)
			return;

		final String searchKey = number.replaceAll("-", "");
		final int searchType = type;

		// 주소록 검색
		if (loadMatchContact) {
			// 검색 끝난후 다시 검색 요청함.
			pendSearchType = type;
			pendSearchKey = searchKey;
			pendSearch = true;
			return;
		}
		loadMatchContact = true;

		// 네이버 검색
		Thread t = new Thread() {
			public void run() {
				handler.sendEmptyMessage(DIAL070_CONTACT_SEARCH_START);
				String _searchKey = searchKey;
				int _searchType = searchType;
				for (;;) {
					mSearchFirstContact = "";
					if (_searchType == SEARCH_TYPE_PHONE)
						mContactList = getSearchItemByPhone(_searchKey);
					else
						mContactList = getSearchItemByName(_searchKey);

					if (pendSearch) {
						pendSearch = false;
						_searchType = pendSearchType;
						_searchKey = pendSearchKey;
						continue; // retry
					}
					break;
				}

				if (mContactList == null) {
					handler.sendEmptyMessage(DIAL070_CONTACT_SEARCH_ERROR);
				} else {
					handler.sendEmptyMessage(DIAL070_CONTACT_SEARCH_OK);
				}

			};
		};
		if (t != null) {
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}

	@Override
	public void onBackPressed() {
		/*if (mKeypadView.getVisibility() != View.VISIBLE) {
			if (mEditMode == FAVORITE_EDIT_MODE) {
				setFavoriteMode(FAVORITE_VIEW_MODE);
				return;
			}
			setKeypadVisible();
			return;
		}

		DialMain.AppCloseRequest(mContext);*/
		App.isReturnClicked=false;
		finish();
		super.onBackPressed();
	}

	private void registerKeypadViewReceiver() {
		mKeypadViewReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				setKeypadVisible();
			}
		};
		registerReceiver(mKeypadViewReceiver, new IntentFilter(
				DialMain.ACTION_KEYPAD_VIEW));
	}

	/*
	 * private void registerFavoriteViewReceiver() { mFavoriteViewReceiver = new
	 * BroadcastReceiver() {
	 *
	 * @Override public void onReceive(Context context, Intent intent) {
	 * setFavoriteVisible(); } }; registerReceiver(mFavoriteViewReceiver , new
	 * IntentFilter(DialMain.ACTION_FAVORITE_VIEW)); }
	 */

	// 즐겨찾기 편집 모드 / 뷰 모드 설정
	private void setFavoriteMode(int mode) {
		mEditMode = mode;
		mAdapter.setAdpaterMode(mode);
		mAdapter.notifyDataSetChanged();
	}

	// 즐겨찾기 삭제
	private boolean deleteFavorite(FavoriteData item) {
		if (!mAdapter.deleteFavorites(item))
			return false;
		reloadFavorites(true);
		return true;
	}

	private int addFavorites(String name, String phone) {
		int rs = RESULT_ADD_FAIL;
		DBManager database = new DBManager(mContext);

		try {
			if (phone == null || phone.length() == 0) {
				return rs;
			}

			database.open();

			//
			// 필터링 call number
			String call_number = phone;
			call_number = call_number.replaceAll("\\+82", "0");
			// BJH 국가번호 발신 때문에
			// call_number = call_number.replaceAll("\\+", "");
			call_number = call_number.replaceAll("\\*", "");
			call_number = call_number.replaceAll("\\#", "");
			call_number = call_number.replaceAll("\\-", "");

			String display_name = phone;

			if (phone.contains("<") || phone.contains("@")) {
				display_name = "";
				Pattern sipUriSpliter = Pattern
						.compile("^(?:\")?([^<\"]*)(?:\")?[ ]*(?:<)?sip(?:s)?:([^@]*)@[^>]*(?:>)?");
				Matcher m = sipUriSpliter.matcher(phone);
				if (m.matches()) {
					if (!TextUtils.isEmpty(m.group(2))) {
						call_number = m.group(2);
					} else if (!TextUtils.isEmpty(m.group(1))) {
						call_number = m.group(1);
					}
				}
			} else {
				// NORMAL NUMBER
				display_name = ContactHelper.getContactsNameByPhoneNumber(this,
						call_number);
				if (display_name == null)
					display_name = call_number;
			}

			boolean existFavorite = false;
			int density = 1;
			Cursor c = database.getFavoriteByPhone(call_number);
			if (c != null) {
				if (c.getCount() > 0) {
					existFavorite = true;
					if (c.moveToFirst()) {
						density = c.getInt(c
								.getColumnIndex(FavoritesData.FIELD_DENSITY));
						density = density + 1;
					}
				}
				c.close();
			}

			if (existFavorite) {
				rs = RESULT_EXIST_USER;
			} else {
				// 초기값 1
				FavoritesData favorite = new FavoritesData(display_name,
						call_number, "", "", call_number, "MANUAL", "", 1);
				try {
					database.insertFavorites(favorite);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// Score
				int score = 0;
				int call_count = 0;
				int call_count_score = 0;
				long call_start = 0;
				int call_state = 0;
				int call_start_score = 0;
				int call_dur = 0;
				int call_dur_score = ScoreUtils.getScoreDuration(call_dur);
				int call_state_score = 0;
				int con_add = 1;
				int con_score = 6;
				int fav_add = 1;
				int fav_score = 10;

				if (!database.existScoreByPhone(call_number)) {
					score = call_count_score + call_start_score
							+ call_dur_score + call_state_score + con_score
							+ fav_score;

					// 신규 추가
					ContentValues cv = new ContentValues();
					cv.put(ScoreData.FIELD_NUMBER, call_number);
					cv.put(ScoreData.FIELD_SCORE, score);
					cv.put(ScoreData.FIELD_ZONE_ID, 0);
					cv.put(ScoreData.FIELD_ZONE_SCORE, 0);
					cv.put(ScoreData.FIELD_CALL_COUNT, call_count);
					cv.put(ScoreData.FIELD_CALL_COUNT_SCORE, call_count_score);
					cv.put(ScoreData.FIELD_CALL_START, call_start);
					cv.put(ScoreData.FIELD_CALL_START_SCORE, call_start_score);
					cv.put(ScoreData.FIELD_CALL_DUR, 1);
					cv.put(ScoreData.FIELD_CALL_DUR_SCORE, call_dur_score);
					cv.put(ScoreData.FIELD_CALL_STATE, call_state);
					cv.put(ScoreData.FIELD_CALL_STATE_SCORE, call_state_score);
					cv.put(ScoreData.FIELD_CON_ADD, con_add);
					cv.put(ScoreData.FIELD_CON_ADD_SCORE, con_score);
					cv.put(ScoreData.FIELD_FAV_ADD, fav_add);
					cv.put(ScoreData.FIELD_FAV_ADD_SCORE, fav_score);
					cv.put(ScoreData.FIELD_CAL_ADD, 0);
					cv.put(ScoreData.FIELD_CAL_ADD_SCORE, 0);
					cv.put(ScoreData.FIELD_F1, 0);
					cv.put(ScoreData.FIELD_F1_SCORE, 0);
					database.insertScores(cv);
				}

				rs = RESULT_ADD_USER;
			}
		} finally {
			database.close();
		}

		return rs;
	}

	private void addContact() {
		String number = mEditCallNumber.getText().toString();
		if (number.length() > 0) {
			contactAdd(number);
		}
	}

	private void sendSMS() {
		String number = mEditCallNumber.getText().toString();
		if (number.length() > 0) {
			DialMain.skipClose = true;
			// SMS 문자 보내기
			try {
				Intent intent = new Intent(Intent.ACTION_SENDTO);
				Uri uri = Uri.parse("sms:" + number);
				intent.setData(uri);
				intent.setPackage(getPackageName());
				startActivity(intent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void goFavorite() {
		setFavoriteVisible();
	}

	private void contactAdd(String number) {
		DialMain.skipClose = true;

		boolean existUser = false;
		long uid = 0;
		// 기존 연락처 있는지 검사
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
		if (c != null) {
			if (c.getCount() > 0 && c.moveToFirst()) {
				existUser = true;
				uid = c.getLong(c
						.getColumnIndex(ContactsContract.PhoneLookup._ID));
			}
			c.close();
		}

		if (!existUser) {
			Intent intent = new Intent();
			intent.setAction(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.setData(Uri.fromParts("tel", number, null));
			intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);

			if (number.startsWith("01"))
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE,
						ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
			else
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE,
						ContactsContract.CommonDataKinds.Phone.TYPE_OTHER);

			startActivity(intent);
		} else {
			Intent intent = new Intent(Intent.ACTION_EDIT);
			// intent.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI
			// + "/" + uid));
			Uri contactUri = ContentUris.withAppendedId(
					ContactsContract.Contacts.CONTENT_URI, uid);
			intent.setData(contactUri);
			startActivity(intent);
		}
	}

	private void closeDial070() {
		Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
		intent.putExtra(DialMain.EXTRA_COMMAND, "QUIT");
		sendBroadcast(intent);
	}

	private void showAntena() {
		Intent intent = new Intent(mContext, NetworkStat.class);
		mContext.startActivity(intent);
	}

	// BJH
	private static void Alert(Context context, String msg) {
		new MaterialAlertDialogBuilder(context)
				.setTitle(context.getResources().getString(R.string.app_name))
				.setMessage(msg)
				.setPositiveButton(context.getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				})
				.show();
	}

	// BJH
	private void ShowCountry() {
		Intent intent = new Intent(Dial.this, CountryList.class);
		startActivityForResult(intent, SELECT_COUNTRY);
	}

	private synchronized void UpdateCode() {

		// SET BUTTON
		if (m_CountryCode == null)
			m_CountryCode = mPrefs
					.getPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE);
		if (m_CountryFlag == null)
			m_CountryFlag = mPrefs
					.getPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG);

		//Log.d(THIS_FILE,"m_CountryCode:"+m_CountryCode+",m_CountryFlag:"+m_CountryFlag);

		if (m_CountryFlag!=null && mCountry!=null && m_CountryFlag.contains("flag_")){
			for (int i = 0; i < mCountry.size(); i++) {

				if (mCountry.get(i).Code!=null && mCountry.get(i).Code.equals(m_CountryCode)){
					mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG,mCountry.get(i).Flag);
					m_CountryFlag=mCountry.get(i).Flag;

					break;
				}
			}
		}
		m_ButtonCountry.setText(m_CountryCode);

		m_NumberCode = m_CountryCode;

		/*
		int flag_id = getResources().getIdentifier(m_CountryFlag, "drawable",
				mContext.getPackageName());
		if (flag_id != 0) {
			Drawable icon = getResources().getDrawable(flag_id);
			icon.setBounds(0, 0, icon.getMinimumWidth(),
					icon.getMinimumHeight());// BJH OUT OF MEMORY
			m_ButtonCountry.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
		}
		*/
	}

	private synchronized void postCountryCode() {
		if (mLoadFullCountry)
			return;
		mCountryFullItemList = new HashMap<String, ArrayList<CountryItem>>();

		if (mLocale.equalsIgnoreCase("한국어")) {
			for (int i = 0; i < HangulUtils.INITIAL_HANGUL_SECTION.length; i++) {
				mCountryFullItemList.put(HangulUtils.INITIAL_HANGUL_SECTION[i],
						null);
			}
			mCountryFullItemList.put(HangulUtils.INITIAL_EXTRA_SECTION, null);
		} else {
			for (int i = 0; i < EnglishUtils.INITIAL_ENGLISH_SECTION.length; i++) {
				mCountryFullItemList.put(
						EnglishUtils.INITIAL_ENGLISH_SECTION[i], null);
			}
			mCountryFullItemList.put(EnglishUtils.INITIAL_EXTRA_SECTION, null);
		}

		InputStream ins = mContext.getResources().openRawResource(
				R.raw.country_all);

		try {
			int size = ins.available();

			// Read the entire resource into a local byte buffer.
			byte[] buffer = new byte[size];
			while (ins.read(buffer) != -1)
				;
			ins.close();

			String jsontext = new String(buffer);

			JSONObject jObject;
			try {
				jObject = new JSONObject(jsontext);
				JSONObject responseObject = jObject.getJSONObject("RESPONSE");
				Log.d("RES_CODE", responseObject.getString("RES_CODE"));
				Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
				Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

				if (responseObject.getInt("RES_CODE") == 0) {
					JSONObject countryObject = jObject
							.getJSONObject("COUNTRY_INFO");

					boolean isExtraSection = false;
					String sectionStr = null;

					Log.d("COUNTRY",
							"SERIAL=" + countryObject.getString("SERIAL"));
					JSONArray countryItemArray = countryObject
							.getJSONArray("ITEMS");
					//Log.d(THIS_FILE,"countryItemArray.length() : "+countryItemArray.length());
					for (int i = 0; i < countryItemArray.length(); i++) {
						/*Log.d("COUNTRY",
								"NAME_KOR="
										+ countryItemArray.getJSONObject(i)
										.getString("NAME_KOR")
										.toString());
						Log.d("COUNTRY",
								"NAME_ENG="
										+ countryItemArray.getJSONObject(i)
										.getString("NAME_ENG")
										.toString());
						Log.d("COUNTRY",
								"CODE="
										+ countryItemArray.getJSONObject(i)
										.getString("CODE").toString());
						Log.d("COUNTRY",
								"FLAG="
										+ countryItemArray.getJSONObject(i)
										.getString("FLAG").toString());*/

						//ArrayList<CountryItem> country = null;
						if(mCountry == null) { //2016.08.17 한번만 하기 위해서
							if (mLocale.equalsIgnoreCase("한국어")) {
								// section을 찾는다.
								String firstChosung = HangulUtils
										.getHangulChosung(countryItemArray
												.getJSONObject(i)
												.getString("NAME_KOR").toString());

								if (firstChosung.length() == 0) {
									isExtraSection = true;
									sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
								} else {
									// char c = firstChosung.charAt(0);

									for (int j = 0; j < HangulUtils.INITIAL_HANGUL_SECTION.length; j++) {
										if (firstChosung
												.equalsIgnoreCase(HangulUtils.INITIAL_HANGUL_SECTION[j])) {
											isExtraSection = false;
											sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[j];
											break;
										}
									}
								}

								// Country에 추가

								if (isExtraSection) {
									mCountry = mCountryFullItemList
											.get(HangulUtils.INITIAL_EXTRA_SECTION);

									if (mCountry == null) {
										mCountry = new ArrayList<CountryItem>();

										mCountryFullItemList.put(
												HangulUtils.INITIAL_EXTRA_SECTION,
												mCountry);
									}
								} else {
									if (mCountryFullItemList
											.containsKey(sectionStr)) {
										mCountry = mCountryFullItemList
												.get(sectionStr);

										if (mCountry == null) {
											mCountry = new ArrayList<CountryItem>();

											mCountryFullItemList.put(sectionStr,
													mCountry);
										}
									}
								}
							} else {
								// section을 찾는다.
								String firstChar = EnglishUtils
										.getEnglishChar(countryItemArray
												.getJSONObject(i)
												.getString("NAME_ENG").toString());

								if (firstChar
										.equalsIgnoreCase(EnglishUtils.INITIAL_EXTRA_SECTION)) {
									isExtraSection = true;
									sectionStr = EnglishUtils.INITIAL_EXTRA_SECTION;
								} else {
									for (int j = 0; j < EnglishUtils.INITIAL_ENGLISH_SECTION.length; j++) {
										if (firstChar
												.equalsIgnoreCase(EnglishUtils.INITIAL_ENGLISH_SECTION[j])) {
											isExtraSection = false;
											sectionStr = EnglishUtils.INITIAL_ENGLISH_SECTION[j];
											break;
										}
									}
								}

								// Country에 추가
								if (isExtraSection) {
									mCountry = mCountryFullItemList
											.get(EnglishUtils.INITIAL_EXTRA_SECTION);

									if (mCountry == null) {
										mCountry = new ArrayList<CountryItem>();

										mCountryFullItemList.put(
												EnglishUtils.INITIAL_EXTRA_SECTION,
												mCountry);
									}
								} else {
									if (mCountryFullItemList
											.containsKey(sectionStr)) {
										mCountry = mCountryFullItemList
												.get(sectionStr);

										if (mCountry == null) {
											mCountry = new ArrayList<CountryItem>();

											mCountryFullItemList.put(sectionStr,
													mCountry);
										}
									}
								}
							}
						}

						Log.d("COUNTRY",
								"NAME_KOR="
										+ countryItemArray.getJSONObject(i)
										.getString("NAME_KOR")
										.toString());
						Log.d("COUNTRY",
								"NAME_ENG="
										+ countryItemArray.getJSONObject(i)
										.getString("NAME_ENG")
										.toString());
						Log.d("COUNTRY",
								"CODE="
										+ countryItemArray.getJSONObject(i)
										.getString("CODE").toString());
						Log.d("COUNTRY",
								"FLAG="
										+ countryItemArray.getJSONObject(i)
										.getString("FLAG").toString());

						if (mCountry != null) {
							mCountry.add(new CountryItem(countryItemArray
									.getJSONObject(i).getString("NAME_KOR")
									.toString(), countryItemArray
									.getJSONObject(i).getString("NAME_ENG")
									.toString(), "+"
									+ countryItemArray.getJSONObject(i)
									.getString("CODE").toString(),
									countryItemArray.getJSONObject(i)
											.getString("GMT").toString(),
									countryItemArray.getJSONObject(i)
											.getString("FLAG").toString(), 0,
									0,
									mLocale));
							mLoadFullCountry = true;
						}

					}
				} else {
					Log.d("COUNTRY",
							"TYPE=" + responseObject.getString("RES_TYPE")
									+ " ERROR ="
									+ responseObject.getString("RES_DESC"));
				}
			} catch (Exception e) {
				Log.e("COUNTRY", "JSON ERROR=" + e.getMessage());
			}

		} catch (IOException e) {
			Log.d("COUNTRY", "ERROR =" + e.getMessage());
		}

		UpdateCode();
	}

	private void getSearchItem(String country_code, String searchKey, int length) {
		Log.d(THIS_FILE,"getSearchItem:"+country_code+", searchKey:"+searchKey+",length:"+length);
		// 초기화
		CountryItem data = null;
		//ArrayList<CountryItem> item = null;
		for (int i = 0; i < mCountryFullItemList.size(); i++) {
			if(mCountry == null ) { //2016.08.17 한번만 하기 위해서
				if (mLocale.equalsIgnoreCase("한국어")) {
					if (i == mCountryFullItemList.size() - 1)
						mCountry = mCountryFullItemList
								.get(HangulUtils.INITIAL_EXTRA_SECTION);
					else
						mCountry = mCountryFullItemList
								.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
				} else {
					if (i == mCountryFullItemList.size() - 1)
						mCountry = mCountryFullItemList
								.get(EnglishUtils.INITIAL_EXTRA_SECTION);
					else
						mCountry = mCountryFullItemList
								.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);
				}
			}
			// 섹션의 아이템이 없는경우 표시하지 않음
			if (mCountry != null && mCountry.size() > 0) {
				for (int j = 0; j < mCountry.size(); j++) {
					data = mCountry.get(j);
					if (country_code != null && country_code.length() > 0) {// BJH 크리스마섬 국가코드 에러 수정
						if (data.Code.equals("+" + country_code + searchKey)) {
							// m_CountryFlag = data.Flag;
							codeInit(data.Code, data.Flag);
							//mEditCallNumber.setText(""); // BJH 2017.01.17 아이폰과 동일하도록
						}
					} else {
						if (data.Code.equals("+" + searchKey)) {
							// m_CountryFlag = data.Flag;
							codeInit(data.Code, data.Flag);
							//mEditCallNumber.setText(""); // BJH 2017.01.17 아이폰과 동일하도록
						}
					}
					/*
					 * if(data.Code.startsWith("+" + searchKey)) { code = "+" +
					 * searchKey; }
					 */
				}
			}

		}
	}

	private void getSearchItem(String searchKey) {
		// 초기화
		if (!searchKey.startsWith("+")) {// BJH 번호가 지워지지 않는 오류 수정
			mEditCallNumber.append(searchKey);
		} else {
			CountryItem data = null;
			//ArrayList<CountryItem> item = null;
			//BJH 2016.06.19 수정
			searchKey = searchKey.replace("-", "");
			String key_value = searchKey;
			String code_value = "";
			String flag_value = "";
			for (int i = 0; i < mCountryFullItemList.size(); i++) {
				if(mCountry == null ) { //2016.08.17 한번만 하기 위해서
					if (mLocale.equalsIgnoreCase("한국어")) {
						if (i == mCountryFullItemList.size() - 1)
							mCountry = mCountryFullItemList
									.get(HangulUtils.INITIAL_EXTRA_SECTION);
						else
							mCountry = mCountryFullItemList
									.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
					} else {
						if (i == mCountryFullItemList.size() - 1)
							mCountry = mCountryFullItemList
									.get(EnglishUtils.INITIAL_EXTRA_SECTION);
						else
							mCountry = mCountryFullItemList
									.get(EnglishUtils.INITIAL_ENGLISH_SECTION[i]);
					}
				}
				// 섹션의 아이템이 없는경우 표시하지 않음
				if (mCountry != null && mCountry.size() > 0) {
					for (int j = 0; j < mCountry.size(); j++) {
						data = mCountry.get(j);
						if (data.Code.length() == 2 && searchKey.length() >= 2) {
							if (data.Code.equals(searchKey.substring(0, 2))) {
								//searchKey = searchKey.substring(2);
								// m_CountryFlag = data.Flag;
								//codeInit(data.Code, data.Flag);
								if(code_value.length() < 2) {
									//key_value = searchKey.substring(2); // BJH 2017.01.17 아이폰과 동일하도록
									code_value = data.Code;
									flag_value = data.Flag;
								}
							}
						} else if (data.Code.length() == 3 && searchKey.length() >= 3) {
							if (data.Code.equals(searchKey.substring(0, 3))) {
								//searchKey = searchKey.substring(3);
								// m_CountryFlag = data.Flag;
								//codeInit(data.Code, data.Flag);
								if(code_value.length() < 3) {
									//key_value = searchKey.substring(3); // BJH 2017.01.17 아이폰과 동일하도록
									code_value = data.Code;
									flag_value = data.Flag;
								}
							}
						} else if (data.Code.length() == 4 && searchKey.length() >= 4) {
							if (data.Code.equals(searchKey.substring(0, 4))) {
								//searchKey = searchKey.substring(4);
								// m_CountryFlag = data.Flag;
								//codeInit(data.Code, data.Flag);
								if(code_value.length() < 4) {
									//key_value = searchKey.substring(4); // BJH 2017.01.17 아이폰과 동일하도록
									code_value = data.Code;
									flag_value = data.Flag;
								}
							}
						} else if (data.Code.length() == 5 && searchKey.length() >= 5) {
							if (data.Code.equals(searchKey.substring(0, 5))) {
								//searchKey = searchKey.substring(5);
								// m_CountryFlag = data.Flag;
								//codeInit(data.Code, data.Flag);
								if(code_value.length() < 5) {
									//key_value = searchKey.substring(5); // BJH 2017.01.17 아이폰과 동일하도록
									code_value = data.Code;
									flag_value = data.Flag;
								}
							}
						} else if (data.Code.length() == 6 && searchKey.length() >= 6) {// BJH 크리스마스섬
							if (data.Code.equals(searchKey.substring(0, 6))) {
								//searchKey = searchKey.substring(6);
								// m_CountryFlag = data.Flag;
								//codeInit(data.Code, data.Flag);
								if(code_value.length() < 6) {
									//key_value = searchKey.substring(6); // BJH 2017.01.17 아이폰과 동일하도록
									code_value = data.Code;
									flag_value = data.Flag;
								}
							}
						}
					}
				}

			}

			if(code_value != null && code_value.length() > 0 && flag_value != null && flag_value.length() > 0)
				codeInit(code_value, flag_value);
			mEditCallNumber.append(key_value);
		}
	}

	private void codeInit(String code_number, String code_flag) {
		if (code_number.equals("+1"))
			code_flag = "us";
		else if (code_number.equals("+7"))
			code_flag = "ru";

		m_NumberCode = code_number;

		m_ButtonCountry.setText(code_number);
		// app.mContact = true;
		m_CountryCode = code_number;
		m_CountryFlag = code_flag;
		//BJH 2016.08.17 팀장님 요구 사항
		mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_CODE,
				code_number);
		mPrefs.setPreferenceStringValue(AppPrefs.DIAL_COUNTRY_FLAG,
				code_flag);

		// UpdateCode();
		/*int flag_id = getResources().getIdentifier(code_flag, "drawable",
				mContext.getPackageName());
		if (flag_id != 0) {
			Drawable icon = getResources().getDrawable(flag_id);
			Log.d(THIS_FILE,"icon.getMinimumWidth2():"+icon.getMinimumWidth());
			icon.setBounds(0, 0, icon.getMinimumWidth(),
					icon.getMinimumHeight());// BJH OUT OF MEMORY
			m_ButtonCountry.setCompoundDrawablesWithIntrinsicBounds(icon, null,null, null);
		}
		*/
	}

	//BJH 2017.01.19 관리팀 요청
	private void goSMSWrite(/*String number, String name*/) {
		String number = mEditCallNumber.getText().toString();
		if (number != null && number.length() > 0) {
			number = number.replace("-", "");
			Intent intent = new Intent(this, SmsWrite.class);
			if(number.startsWith("00182"))
				number = number.replace("00182", "0");
			intent.putExtra("phone", number);
			//intent.putExtra("name", name);
			mContext.startActivity(intent);
		}
	}

	/** 동적으로(코드상으로) 브로드 캐스트를 등록한다. **/
	private void registerUrlSchemeCallReceiver(){
		/** 1. intent filter를 만든다
		 *  2. intent filter에 action을 추가한다.
		 *  3. BroadCastReceiver를 익명클래스로 구현한다.
		 *  4. intent filter와 BroadCastReceiver를 등록한다.
		 * */
		if(mUrlSchemeCallReceiver != null) return;

		final IntentFilter theFilter = new IntentFilter();
		theFilter.addAction(BC_DIAL);
		theFilter.addAction(BC_CALL);
		theFilter.addAction(BC_SEARCH);

		this.mUrlSchemeCallReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(BC_DIAL)){
					//Toast.makeText(context, "recevied Data : "+receviedData, Toast.LENGTH_SHORT).show();
					String number = intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER);
					setCallNumber(number);
				}else if (intent.getAction().equals(BC_CALL)){
					String number = intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER);
					setCallNumber(number);
					mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, number);
				}else if (intent.getAction().equals(BC_SEARCH)){
					// mIsDialView = true;
					mEditCallNumber.setText("");
					// mEditCallNumber.append(data.getStringExtra("PhoneNumber"));
				/*if (data.getStringExtra("PhoneNumber").startsWith("+")) // BJH 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
					getSearchItem(data.getStringExtra("PhoneNumber"));
				else
					getSearchItem("+82" + data.getStringExtra("PhoneNumber"));*/
					String type = intent.getStringExtra("Type"); // BJH 2016.12.01 사장님 요청 사항
					String number = intent.getStringExtra("PhoneNumber");
				/*if(type != null && type.length() > 0) { //Naver 검색
					if (number.startsWith("+")) // BJH 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
						getSearchItem(number);
					else
						getSearchItem("+82" + number);
				} else {
					if(!(number.startsWith("+") || number.startsWith("001")))
						getSearchItem(m_NumberCode.concat(number));
					else
						getSearchItem(number);
				}*/
					/* BJH 2017.02.06 */
					if(type != null && type.length() > 0) { //Naver 검색
						if (type.equals("naver") && !number.startsWith("+")) // 검색결과가 네이버 검색 일 경우 한국 번호이므로 +82를 추가한다.
							codeInit("+82", "kr");
						else if(type.equals("call_center") && number.startsWith("+82")) { // 콜센터인 경우 +82로 국가번호로 하고 거는 번호는 +82를 지운다.
							codeInit("+82", "kr");
							number = number.replace("+82", "");
						}
					}
					getSearchItem(number);
					// mIsDialView = false;
				}
			}
		};

		registerReceiver(mUrlSchemeCallReceiver, theFilter);

	}

	/** 동적으로(코드상으로) 브로드 캐스트를 종료한다. **/
	private void unregisterReceiver() {
		if(mUrlSchemeCallReceiver != null){
			unregisterReceiver(mUrlSchemeCallReceiver);
			mUrlSchemeCallReceiver = null;
		}

	}

	private void registerUpdateRegStateReceiver(){
		if(mBrUpdateRegState != null) return;

		this.mBrUpdateRegState=new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				DebugLog.d("test_rtt in Dial screen updateRegStateReceiver onReceived...");
				boolean testState = intent.getBooleanExtra("ONLINE_STATE",false);
				DebugLog.d("test_rtt in Dial screen testState: "+testState);
				DebugLog.d("test_rtt in Dial screen static value: "+SipService.RTT);

				FirebaseCrashlytics.getInstance().setCustomKey("state_online_in_dial_reg_br", testState);
				FirebaseCrashlytics.getInstance().setCustomKey("rtt_in_dial_reg_br", SipService.RTT);

				Log.i(THIS_FILE,"onReceive:"+intent.getAction());
				if(intent.getAction().equals(BC_UPDATE_REG_STATE)){
					boolean stateOnline=intent.getBooleanExtra("ONLINE_STATE",false);
					if (stateOnline){
						/*int RTT = intent.getIntExtra("RTT",0);
						int LOSS = intent.getIntExtra("LOSS",0);*/

						int RTT = SipService.RTT;
						int LOSS = SipService.LOSS;

						int antena = R.drawable.antena_0;
						if (RTT < 100)
							antena = R.drawable.antena_3;
						else if (RTT < 200)
							antena = R.drawable.antena_2;
						else
							antena = R.drawable.antena_1;

						imgRtt.setImageResource(antena);

						if (LOSS != 0)
							txtRtt.setText(" " + RTT + "ms (" + LOSS + ")");
						else
							txtRtt.setText(" " + RTT + "ms");
					}else{
						imgRtt.setImageResource(R.drawable.antena_0);
						txtRtt.setText("");
					}

				}
			}
		};

		IntentFilter theFilter = new IntentFilter();
		theFilter.addAction(BC_UPDATE_REG_STATE);
		registerReceiver(this.mBrUpdateRegState,theFilter);
	}

	private void registerDialogReceiver(){
		if(mBrDialog != null) return;

		this.mBrDialog=new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.i(THIS_FILE,"onReceive:"+intent.getAction());
				if (!mPrefs.getPreferenceBooleanValue(AppPrefs.RETURN_ENABLED)){
					if(intent.getAction().equals(BC_DIALOG)){
						String type=intent.getStringExtra("type");
						String number=intent.getStringExtra("number");
						int status=intent.getIntExtra("status",0);
						if (type!=null){
							if (type.equals("type1")){
								selectGSMCall(number);
							}else if (type.equals("type2")){
								selectGSMCall2(number);
							}else if (type.equals("error")){
								alertErrorMsg(status);
							}
						}
					}
				}

			}
		};

		IntentFilter theFilter = new IntentFilter();
		theFilter.addAction(BC_DIALOG);
		registerReceiver(this.mBrDialog,theFilter);
	}

	private void registerFinishReceiver(){
		if(mBrFinish != null) return;

		this.mBrFinish=new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.i(THIS_FILE,"onReceive:"+intent.getAction());
				if(intent.getAction().equals(BC_FINISH)){
					//App.isReturnClicked=false;
					//finish();
				}
			}
		};

		IntentFilter theFilter = new IntentFilter();
		theFilter.addAction(BC_FINISH);
		registerReceiver(this.mBrFinish,theFilter);
	}

	private void unregisterUpdateRegStateReceiver() {
		if(mBrUpdateRegState != null){
			unregisterReceiver(mBrUpdateRegState);
			mBrUpdateRegState = null;
		}
	}

	private void unregisterDialogReceiver() {
		if(mBrDialog != null){
			unregisterReceiver(mBrDialog);
			mBrDialog = null;
		}
	}

	private void unregisterFinishReceiver() {
		if(mBrFinish != null){
			unregisterReceiver(mBrFinish);
			mBrFinish = null;
		}
	}

	private void selectGSMCall(final String number) {
		Log.i(THIS_FILE,"selectGSMCall");
		if (number==null){
			return;
		}

		new MaterialAlertDialogBuilder(this)
				.setTitle(getResources().getString(R.string.make_gsm_call))
				.setMessage(getResources().getString(R.string.make_gsm_call_question))
				.setPositiveButton(getResources().getString(R.string.confirm), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				})
				.show();
	}

	private void selectGSMCall2(final String number) {
		if (number==null){
			return;
		}

		new MaterialAlertDialogBuilder(this)
				.setTitle(getResources().getString(R.string.make_gsm_call))
				.setMessage(getResources().getString(R.string.make_gsm_call_question2))
				.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						doSipCall(number);
					}
				})
				.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				})
				.show();
	}

	private void alertErrorMsg(int status) {
		Log.d(THIS_FILE,"alertErrorMsg:"+status);
		String msg = status + ": ";
		if (status == 403)
			msg = msg + this.getResources()
					.getString(R.string.status_code_603);
		else if (status == 404)
			msg = msg + this.getResources()
					.getString(R.string.status_code_404);
		else if (status == 486)
			msg = msg + this.getResources()
					.getString(R.string.status_code_486);
		else if (status == 603)
			msg = msg + this.getResources()
					.getString(R.string.status_code_603);
		else
			msg = msg + this.getResources()
					.getString(R.string.status_code_etc);

		msg = msg + this.getResources().getString(
				R.string.status_code_svc_center);

        /*new SweetAlertDialog(currentContext, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(currentContext.getResources().getString(R.string.guide))
                .setContentText(msg)
                .setConfirmText(currentContext.getResources().getString(R.string.confirm))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        // 에러코드별 확인사항
                    }
                })
                .show();*/

		AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);
		builder.setTitle(this.getResources().getString(R.string.guide));
		builder.setMessage(msg);
		builder.setCancelable(true);
		builder.setPositiveButton(this.getResources().getString(R.string.confirm),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				});

		builder.show();
	}
	private Boolean isWorking = false;
	private Thread workThread = null;

	private void workStart() {
//		if(isWorking) {
//			return;
//		}
		isWorking = true;

		workThread = new Thread(() -> {
			workProcess();
			workThread = null;
			isWorking = false;
		});
		workThread.start();
	}

	private void workStop() {
		if(!isWorking) {
			return;
		}
		isWorking = false;
		workWait();
	}

	private void workWait() {
		if(workThread!=null) {
			try {
				workThread.join(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void workProcess() {
		DebugLog.d("test_v2 ff_antena work process called.");
		if(SipService.currentService == null) {
			DebugLog.d("test_v2 ff_antena currentService is null.");
			return;
		}
		if(SipService.pjService == null) {
			DebugLog.d("test_v2 ff_antena pjService is null.");
			return;
		}

		ArrayList<StatItem> mStatList = new ArrayList<>();

		int result = pjsua.mobile_rtt_handler_send(20, 500);
		DebugLog.d("test_v2 ff_antena pjsua.rtt_handler_send result: "+result);

		int zeroCount = 0;

		int old_block = 0;
		ArrayList<Integer> avgList = new ArrayList<>();
		while(isWorking) {
			// check
			int count = pjsua.mobile_rtt_handler_get_recv_count();

			if(count==0) {
				zeroCount = zeroCount+1;
			}

			if(count>=500 || zeroCount==10) {
				isWorking = false;
			}

			DebugLog.d("test_v2 ff_antena work process called. count: "+count);

			int block = count / 50;
			if (block <= old_block)
			{
				sleep(100);
				continue;
			}
			old_block = block;

//			mStatList.clear();

			for (int i=0; i<block; i++) {
				int idx = i+1;

				int bc = idx * 50;
				int rtt_sum = 0;
				int loss_sum = 0;
				int rtt_count = 0;
				for (int x=(bc-50); x<bc; x++)
				{
					int rtt = pjsua.mobile_rtt_handler_get_rtt(x + 1);
					int flag = pjsua.mobile_rtt_handler_get_flag(x + 1);
					if (flag == 0) loss_sum++;
					rtt_sum = rtt_sum + rtt;
					rtt_count++;
				}
				rtt_sum = rtt_sum / rtt_count;
				DebugLog.d("test_v2 ff_antena"+" index: "+idx+"rtt_sum: "+rtt_sum+" loss_sum: "+loss_sum);

				StatItem item = new StatItem(rtt_sum, loss_sum, idx, ""+idx);
				mStatList.add(item);
			}

			// calc avg value
			int totalRtt = 0;
			int totalCount = mStatList.size();

			for(int i=0; i<mStatList.size(); i++) {
				totalRtt = totalRtt+mStatList.get(i).getRTT();
			}

			int avgValue = (totalRtt/totalCount)/1000;
			DebugLog.d("test_v2 ff_antena avgValue: "+avgValue);
			avgList.add(avgValue);

			// update
			sleep(500);
		}
		// calc avg
		int totalAvgRtt = 0;
		int totalAvgCount = avgList.size();

		for(int i=0; i<totalAvgCount; i++) {
			totalAvgRtt = totalAvgRtt+avgList.get(i);
		}

		int totalAvgValue = (totalAvgRtt/totalAvgCount);
		DebugLog.d("test_v2 ff_antena totalAvgValue: "+totalAvgValue);
		SipService.RTT =  totalAvgValue;

		runOnUiThread(() -> {
			int RTT = SipService.RTT;
			int LOSS = SipService.LOSS;

			int antena = R.drawable.antena_0;
			if (RTT < 100) {
				antena = R.drawable.antena_3;
			} else if (RTT < 200) {
				antena = R.drawable.antena_2;
			} else if(RTT == 0) {
				antena = R.drawable.antena_0;
			} else {
				antena = R.drawable.antena_1;
			}
			txtRtt.setText(String.format(" %d ms", RTT));
			imgRtt.setImageResource(antena);

			Toast.makeText(Dial.this, String.format("%d ms 가 적용되었습니다.", RTT), Toast.LENGTH_SHORT).show();

			hideProgress();
		});
	}

	private void sleep(int delay)
	{
		try
		{
			Thread.sleep(delay);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showProgress() {
		if(mCircleProgressBar!=null && !mCircleProgressBar.isShowing()) {
			mCircleProgressBar.show();
		}
	}

	private void hideProgress() {
		if(mCircleProgressBar!=null && mCircleProgressBar.isShowing()) {
			mCircleProgressBar.dismiss();
		}
	}

}
