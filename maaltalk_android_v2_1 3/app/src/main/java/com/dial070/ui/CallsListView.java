package com.dial070.ui;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.db.RecentCallsData;
import com.dial070.maaltalk.R;
import com.dial070.utils.PhoneNumberTextWatcher;

public class CallsListView extends LinearLayout {

	private int	mId;
	private String mNumber;
	private String mName;
	private String mMemo;
	private String mRec;
	private int mType; // BJH 2016.09.23
	private ImageView mTypeView;
	private ImageView mRecView;
	private ImageView mVMSView; // BJH 2016.09.23
	private TextView mNameView;
	private TextView mPhoneNumber;
	private TextView mDateView;
	private TextView mDurationView;

	private Context mContext;
	
	public CallsListView(Context context)
	{
		super(context);
		
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//inflater.inflate(R.layout.calls_item, this, true);
		inflater.inflate(R.layout.listitem_recent, this, true);

		// internal.
		mId = 0;
		mNumber = "";
		mName = "";
		mMemo = "";
		mRec = "";
		mType = 0; 

		mNameView = (TextView) findViewById(R.id.txtName);
		mPhoneNumber = (TextView) findViewById(R.id.txtNumber);
		mDateView = (TextView) findViewById(R.id.txtCallDate);
		mTypeView = (ImageView) findViewById(R.id.imgCallType);
		mDurationView = (TextView) findViewById(R.id.txtDuration);
		mRecView = (ImageView) findViewById(R.id.imgRec);
		mVMSView = (ImageView) findViewById(R.id.imgVms);
		
		PhoneNumberTextWatcher digitFormater = new PhoneNumberTextWatcher();
		mPhoneNumber.addTextChangedListener(digitFormater);
		
	}

	private static Drawable imageIn = null;
	private static Drawable imageOut = null;
	private static Drawable imageVMS = null; // BJH 2016.09.23
	private static Drawable imageMissed = null;
	public void setData(int id, String name, int type, long date, long duration, int call_type, String phone, int hidden, String memo, String rec)
	{
		String PhoneNumber = phone;
		
		mId = id;
		mNumber = PhoneNumber;
		
		mName = name;
        if(mName == null || mName.length() == 0)
        {
        	mNameView.setVisibility(View.GONE);
			mPhoneNumber.setTextSize(15);
        	mPhoneNumber.setTypeface(mPhoneNumber.getTypeface(), Typeface.BOLD);
        }
        else 
        {
        	mNameView.setVisibility(View.VISIBLE);
        	mNameView.setText(mName);
			mPhoneNumber.setTextSize(12);
			mPhoneNumber.setTypeface(mPhoneNumber.getTypeface(), Typeface.NORMAL);
        }		
        
		mMemo = memo;
		
		mRec = rec;
		
		mType = type;
		
		// Set the date/time field by mixing relative and absolute times.
		//int flags = DateUtils.FORMAT_ABBREV_RELATIVE;

        CharSequence dateClause = DateUtils.formatDateRange(mContext, date, date, DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME  | DateUtils.FORMAT_24HOUR);
		mDateView.setText(dateClause);
		
        // Set the duration
		mDurationView.setVisibility(View.VISIBLE);
		mDurationView.setText(formatDuration(duration));
		mDurationView.setTextColor(Color.argb(255, 2, 161, 200));
        
        if(mRec != null && mRec.length() > 0)
        {
        	mRecView.setVisibility(View.VISIBLE);
        }
        else
        {
        	mRecView.setVisibility(View.INVISIBLE);
        }        
        
        //BJH 2016.09.23
        mVMSView.setVisibility(View.INVISIBLE);
        
        // 0:normal, 1:internal, 2:vms, 3:qa
        /*
        switch (call_type) {
        case 1:
        	PhoneNumber = "";
        	break;
        case 2:	
        	PhoneNumber = "";
        	break;
        }
        */
     
        if(hidden == 1 && call_type != 1)
        	PhoneNumber = "";

        if(PhoneNumber == null || PhoneNumber.length() == 0)
        {
        	mPhoneNumber.setVisibility(View.GONE);
        }
        else 
        {
        	mPhoneNumber.setVisibility(View.VISIBLE);
        	mPhoneNumber.setText(PhoneNumber);
        	mPhoneNumber.setTextColor(Color.BLACK);
        }
        
		// Set the icon
		switch (type) {
		case RecentCallsData.INCOMING_TYPE:
		{
			if (imageIn == null)
				imageIn = getResources().getDrawable(R.drawable.ic_recent_receive);
			mTypeView.setImageDrawable(imageIn);
		}
			break;

		case RecentCallsData.OUTGOING_TYPE:
		{
			if (imageOut == null)
				imageOut = getResources().getDrawable(R.drawable.ic_recent_send);
			mTypeView.setImageDrawable(imageOut);
		}
			break;
			
		case RecentCallsData.VMS_TYPE: //BJH 2016.09.23
		{
			if (imageVMS == null)
				imageVMS = getResources().getDrawable(R.drawable.ic_recent_miss);
				//imageVMS = getResources().getDrawable(R.drawable.vms);
			mTypeView.setImageDrawable(imageVMS);
			//mPhoneNumber.setTextColor(Color.argb(255, 245, 107, 93));
			mDurationView.setText(getResources().getString(R.string.missed_call));
			mDurationView.setTextColor(Color.argb(255, 233, 0, 0));
			if(mRec != "")
				mVMSView.setVisibility(View.VISIBLE);
			mRecView.setVisibility(View.INVISIBLE);
		}
			break;

		case RecentCallsData.MISSED_TYPE:
		{
			if (imageMissed == null)
				imageMissed = getResources().getDrawable(R.drawable.ic_recent_miss);
			mTypeView.setImageDrawable(imageMissed);			
			//mPhoneNumber.setTextColor(Color.argb(255, 245, 107, 93));
			mDurationView.setText(getResources().getString(R.string.missed_call));
			mDurationView.setTextColor(Color.argb(233, 233, 0, 0));
		}
			break;
		}		

	}
	
    private String formatDuration(long elapsedSeconds) {
        long minutes = 0;
        long seconds = 0;

        if (elapsedSeconds >= 60) {
            minutes = elapsedSeconds / 60;
            elapsedSeconds -= minutes * 60;
        }
        seconds = elapsedSeconds;

        return mContext.getString(R.string.calls_duration_format, minutes, seconds);
    }


	public int getId() {
		return mId;
	}


	public void setId(int id) {
		this.mId = id;
	}


	public String getNumber() {
		return mNumber;
	}


	public void setNumber(String number) {
		this.mNumber = number;
	}		
	
	public String getName()
	{
		return mName;
	}
	
	public String getMemo()
	{
		return mMemo;
	}
	
	public String getRec()
	{
		return mRec;
	}	
	
	public int getType() // BJH 2016.09.23
	{
		return mType;
	}	
}
