package com.dial070.ui;

import android.os.Parcel;
import android.os.Parcelable;

public class ContactsPhoneList implements Parcelable {

	
    String uid;
    String phone_type;
    String phone_number;
 
    public String getUid() {
        return this.uid;
    }
     
    public String getPhoneType() {
        return this.phone_type;
    }
   
    public String getPhoneNumber() {
        return this.phone_number;
    }
         
     
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
     
    public void writeToParcel(Parcel dest, int flags) {
        // 목적지(dest)로 가는 parcel 객체에 생성자에서 사용했던 순서대로 값을 넣어준다.
        dest.writeString(this.uid);
        dest.writeString(this.phone_type);
        dest.writeString(this.phone_number);
    }
     
    public static final Parcelable.Creator<ContactsPhoneList> CREATOR = new Parcelable.Creator<ContactsPhoneList>() {
        public ContactsPhoneList createFromParcel(Parcel in) {
            String arg1 = in.readString();
            String arg2 = in.readString();          
            String arg3 = in.readString();          
            return new ContactsPhoneList(arg1, arg2, arg3);
        }
         
        public ContactsPhoneList[] newArray(int size) {
            return new ContactsPhoneList[size];
        }
    };
     
 
    ContactsPhoneList(String id, String type, String num){
        this.uid = id;
        this.phone_type = type;
        this.phone_number = num;
    } 	
	
}
