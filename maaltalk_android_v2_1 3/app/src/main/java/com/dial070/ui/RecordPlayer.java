package com.dial070.ui;


import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dial070.maaltalk.R;

public class RecordPlayer extends Activity  implements OnClickListener, OnTouchListener, OnCompletionListener, OnBufferingUpdateListener {

	private String mRecUrl;
	private String mNumber;
	private MediaPlayer mPlayer; // MediaPlayer 객체입니다.
	private SeekBar seekBarProgress;
	private Button mButtonPlayPause;

	private TextView mTextPhoneNumber;
	private int mediaFileLengthInMilliseconds;
	
	private final Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.record_player_activity);
		
		mRecUrl = null;
		
		Intent intent = getIntent();
		mRecUrl = intent.getStringExtra("RECORD_URL");
		mNumber = intent.getStringExtra("PHONE_NUMBER");
		
		mTextPhoneNumber = (TextView) findViewById(R.id.phone_number);
		mTextPhoneNumber.setText(mNumber);
		
		
		//For Test
		//mRecUrl = "http://trinix.makecover.co.kr/~sgkim/SentimentalGreen.mp3";
		seekBarProgress = (SeekBar)findViewById(R.id.play_seekbar);
		seekBarProgress.setMax(99); // It means 100% .0-99
		seekBarProgress.setOnTouchListener(this);		
		
		mButtonPlayPause = (Button)findViewById(R.id.btn_play_pause);
		mButtonPlayPause.setOnClickListener(this);
		
		mPlayer = new MediaPlayer();
		mPlayer.setOnBufferingUpdateListener(this);
		mPlayer.setOnCompletionListener(this);
		

	}
	
	
	private void primarySeekBarProgressUpdater() {
		try
		{
			if(seekBarProgress == null) return;
			seekBarProgress.setProgress((int)(((float)mPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
		}catch(Exception e){
		}		
		
		if(mPlayer == null) return;

		if (mPlayer.isPlaying()) {
			Runnable notification = new Runnable() {
	
				public void run() {
					primarySeekBarProgressUpdater();
				}
			};
			handler.postDelayed(notification,1000);
		}
	}	

	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if(mPlayer != null)
			if(mPlayer.isPlaying()) mPlayer.stop();
			mPlayer.release(); 
			mPlayer = null;		
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//자동시작
		PlayAndPause();
	}


	//@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub
		seekBarProgress.setSecondaryProgress(percent);
	}


	//@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		//buttonPlayPause.setImageResource(R.drawable.button_play);
		mButtonPlayPause.setText(R.string.record_play);
	}


	//@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.play_seekbar){
	
			/** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
			if(mPlayer.isPlaying()){
				SeekBar sb = (SeekBar)v;
				int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
				mPlayer.seekTo(playPositionInMillisecconds);
			}
		}
		return false;
	}

	private void PlayAndPause()
	{
		/** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
		try {
			mPlayer.setDataSource(mRecUrl); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
			mPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.

		} catch (Exception e) {
			e.printStackTrace();
		}
		mediaFileLengthInMilliseconds = mPlayer.getDuration(); // gets the song length in milliseconds from URL
		if(!mPlayer.isPlaying()){
			mPlayer.start();
			//버튼 Pause
			mButtonPlayPause.setText(R.string.record_pause);
		}else {
			mPlayer.pause();
			//버튼 재생 
			mButtonPlayPause.setText(R.string.record_play);
		}
		primarySeekBarProgressUpdater();		
	}
	
	//@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btn_play_pause){
			PlayAndPause();

		}		
	}

			 
}
