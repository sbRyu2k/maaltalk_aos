package com.dial070.ui;

public class FavoriteData {
	private int mId;
	private String mDisplayName;
	private String mPhoneNumber;
	private String mContactId;
	private String mPhotoId;
	private boolean mSelected = false;	
	private String mRegisterType;
	private String mMemo;
	
	public FavoriteData(int id, String user_name, String phone_number, String user_id, String photo_id, String register_type, String memo)
	{
		mId = id;
		mDisplayName = user_name;
		mPhoneNumber = phone_number;
		mContactId = user_id;
		mPhotoId = photo_id;		
		mRegisterType = register_type;
		mSelected = false;
		mMemo = memo;
	}
	
	public int getId()
	{
		return mId;
	}
	
	public String getMemo()
	{
		return mMemo;
	}
	
	public String getRegisterType()
	{
		return mRegisterType;
	}

	public String getDisplayName()
	{
		return mDisplayName;
	}
	
	public String getPhoneNumber()
	{
		return mPhoneNumber;
	}
		
	public String getContactId()
	{
		return mContactId;
	}

	public String getPhotoId()
	{
		return mPhotoId;
	}	
	
	public void setDisplayName(String name)
	{
		mDisplayName = name;
	}
	
	public void setPhoneNumber(String phone)
	{
		mPhoneNumber = phone;
	}
		
	public void setId(int id)
	{
		mId = id;
	}
	
	public void setContactId(String id)
	{
		mContactId = id;
	}

	public void setRegisterType(String type)
	{
		mRegisterType = type;
	}
	
	public void setPhotoId(String photo_id)
	{
		mPhotoId = photo_id;
	}	
	
	public void setMemo(String memo)
	{
		mMemo = memo;
	}		
	
	public void setSelected(boolean selected)
	{
		mSelected = selected;
	}
	
	public boolean getSelected()
	{
		return mSelected;
	}
}
