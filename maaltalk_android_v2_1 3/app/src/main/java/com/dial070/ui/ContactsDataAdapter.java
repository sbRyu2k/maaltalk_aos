package com.dial070.ui;

import java.util.ArrayList;
import java.util.Map;

import com.dial070.maaltalk.R;
//import com.dial070.utils.AppPrefs;
import com.dial070.utils.HangulUtils;
import com.dial070.ui.ContactsData;
import com.dial070.ui.ContactsListView;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;



public class ContactsDataAdapter extends BaseAdapter {
// 	private static final String THIS_FILE="ContactsDataAdapter";
	
	private Context mContext;
	private Resources mResources;

	private ArrayAdapter<String> mHeaders;

	private final static int TYPE_SECTION_HEADER = 0;
	
	private Map<String, ArrayList<ContactsData>> mSectionItemList;
//	private AppPrefs mPrefs;

	public ContactsDataAdapter(Context context)
	{
		mContext = context;
		mResources = mContext.getResources();	
		mHeaders = new ArrayAdapter<String>(context, R.layout.contacts_list_header);		
//		mPrefs = new AppPrefs(mContext);
		mSectionItemList = null;

		//섹션 초기화 
		for(int i=0; i < HangulUtils.INITIAL_HANGUL_SECTION.length ; i++)
		{
			mHeaders.add(HangulUtils.INITIAL_HANGUL_SECTION[i]);
		}
		//기타 섹션 추가 
		mHeaders.add(HangulUtils.INITIAL_EXTRA_SECTION);

	}
	
	public void setData(Map<String, ArrayList<ContactsData>> data)
	{
		if(data == null) return;
		
		mSectionItemList = data;
	}

	public int getSectionPosition(String section)
	{
		int position = 0;
		
		if(mSectionItemList == null) return position;
		
		String sectionStr = null;		
		
		ArrayList<ContactsData> items = null;
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{

			if(i == mSectionItemList.size()-1) sectionStr = HangulUtils.INITIAL_EXTRA_SECTION;
			else sectionStr = HangulUtils.INITIAL_HANGUL_SECTION[i];

			items = mSectionItemList.get(sectionStr);
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(items != null && items.size() > 0)
			{
				if(!sectionStr.equalsIgnoreCase(section))
				{
					position += items.size() + 1;
				}
				else return position;
			}
		}		

		return position;			
	}		
	

	//@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int count = 0;
		if( mSectionItemList == null) return count;
		
		ArrayList<ContactsData> item = null;
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{
			
			if(i == mSectionItemList.size()-1)
				item = mSectionItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
			else	
				item = mSectionItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);

			//섹션의 아이템이 없는경우 표시하지 않음 
			if(item != null && item.size() > 0)
			{		
				count += item.size() + 1;
			}
		}

		return count;		
	}

	//@Override
	public Object getItem(int position) {
		
		if( mSectionItemList == null) return null;
		
		int orgPosition = position;
//		int curPosition = 0;
		ContactsData item = null;		
		
		ArrayList<ContactsData> data = null;
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{
			if(i == mSectionItemList.size()-1)
				data = mSectionItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
			else	
				data = mSectionItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);

			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				//curPosition += data.size();
				if(orgPosition <= data.size())
				{
					if(orgPosition == 0)
					{
						return null;
					}
					else
					{
						item = data.get(orgPosition - 1);
					}
					break;
				}
				orgPosition -= data.size() + 1;
			}			
		}		

		return item;		
	}

	//@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ContactsListView contactsView;
		
		if( mSectionItemList == null) return null;
		
		int orgPosition = position;
//		int curPosition = 0;
		
		ContactsData item = null;	
//		String sectionStr = null;
		
		ArrayList<ContactsData> data = null;
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{
			if(i == mSectionItemList.size()-1)
				data = mSectionItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
			else	
				data = mSectionItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);	
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				if(orgPosition <= data.size())
				{

					if(orgPosition == 0)
					{
						//Header View
						return mHeaders.getView(i, convertView, parent);
					}
					else
					{
						item = data.get(orgPosition - 1);
						
						if(convertView == null)
						{
							contactsView = new ContactsListView(mContext, item);
						}
						else
						{
							contactsView = (ContactsListView) convertView;
						}
						
						if(item != null)
						{
							contactsView.setData(item.getDisplayName(), item.getPhoneNumber(), item.getContactId(), item.getPhotoId());
							contactsView.setItem(item);
						}
						
						contactsView.setBackgroundColor(mResources.getColor(R.color.list_even_background));
						
						return contactsView;
					}
				}
				orgPosition -= data.size() + 1;
			}
			
		}
				
		return null;				
	}
	
	@Override
	public int getItemViewType(int position) {
		int type = 1;

		int orgPosition = position;
//		int curPosition = 0;
//		ContactsData item = null;		
		
		ArrayList<ContactsData> data = null;
		for(int i=0; i < mSectionItemList.size() ; i++) 
		{
			if(i == mSectionItemList.size()-1)
				data = mSectionItemList.get(HangulUtils.INITIAL_EXTRA_SECTION);
			else	
				data = mSectionItemList.get(HangulUtils.INITIAL_HANGUL_SECTION[i]);
			
			//섹션의 아이템이 없는경우 표시하지 않음 
			if(data != null && data.size() > 0)
			{
				//curPosition += data.size();
				if(orgPosition <= data.size())
				{
					if(orgPosition == 0)
					{
						return TYPE_SECTION_HEADER;	
					}
					else
					{
						return type;
					}
				}
				orgPosition -= data.size() + 1;
			}			
		}	
		return -1;
	}


	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		//Header and Item
		return 2;
	}


	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}	
}







 