package com.dial070.ui;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import kr.co.kcp.util.PackageState;

import org.apache.http.util.EncodingUtils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.App;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

@SuppressLint("SetJavaScriptEnabled")


public class PayClient extends AppCompatActivity {
	private static final String THIS_FILE = "PAY CLIENT";
	
	private String mTitle;
	
	private TextView mWebTitle;
	//private ImageButton mBtnNaviBack,mBtnNaviPrev; 
	private ImageButton mBtnNaviClose,mBtnNaviReload;
	
	private Context mContext;
	private WebView mWeb;
	private AppPrefs mPrefs;
	private PreferencesWrapper mSysPrefs;
	private String mWebURL = null;
	final Activity activity = this;
	
	
	public static PayClient currentContext = null;
	public static int currentState = 0; // 0:pause, 1:resume
	
	
	private final Handler handler = new Handler();
    public static final String   ACTIVITY_RESULT         = "ActivityResult";
    public static final int      PROGRESS_STAT_NOT_START = 1;
    public static final int      PROGRESS_STAT_IN        = 2;
    public static final int      PROGRESS_DONE           = 3;
    public              int      m_nStat                 = PROGRESS_STAT_NOT_START;
    
    private boolean mPayResult = false;
    
    //BJH
    private final static int FILECHOOSER_RESULTCODE = 1; 
    private ValueCallback<Uri> mUploadMessage;
	private ValueCallback<Uri[]> mUploadMessages; // BJH 2017.03.07 파일 업로드
    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private boolean mPopup = true;// 사용자별 설명서 팝업창 이동
	private Bundle mbundle;
    
    private static void Alert(Context context, String msg)
    {
		AlertDialog.Builder msgDialog = new AlertDialog.Builder(context);
		msgDialog.setTitle(context.getResources().getString(R.string.app_name));
		msgDialog.setMessage(msg);
		msgDialog.setIcon(R.drawable.ic_launcher);
		msgDialog.setNegativeButton(context.getResources().getString(R.string.close), null);
		msgDialog.show();
    }
	
	
	@SuppressLint("JavascriptInterface")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mbundle  = savedInstanceState;

		DebugLog.d("test_my_info url: PayClient onCreate()<--");


		currentContext = this;
		mContext = this;
		mPrefs = new AppPrefs(this);//BJH
		mSysPrefs = new PreferencesWrapper(this);//BJH
		mPayResult = false;
		
		//getWindow().requestFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.web_client_activity);
		
		mWebTitle = (TextView) findViewById(R.id.web_title);
		mBtnNaviClose = (ImageButton) findViewById(R.id.btn_web_navi_close);
		mBtnNaviReload = (ImageButton) findViewById(R.id.btn_web_navi_reload);
		mBtnNaviReload.setVisibility(View.GONE);
		
		mWeb = (WebView) findViewById(R.id.web_view);

		WebSettings set = mWeb.getSettings();
		set.setJavaScriptEnabled(true);
		set.setSupportMultipleWindows(true);
		set.setJavaScriptCanOpenWindowsAutomatically(true);
		set.setAllowFileAccess(true);//BJH
		set.setSavePassword(false);
		set.setAppCacheEnabled(true);

		//BJH WebView 속도 향상
		getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		set.setRenderPriority(WebSettings.RenderPriority.HIGH);
		set.setCacheMode(WebSettings.LOAD_NO_CACHE);
		if (Build.VERSION.SDK_INT >= 19) {
			mWeb.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
		else {
			mWeb.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}

		//DownLoad MANAGER
		downloadManager = (DownloadManager)getSystemService(Context.DOWNLOAD_SERVICE);

		mWeb.addJavascriptInterface(new KCPPayBridge()        , "KCPPayApp"     );
        mWeb.addJavascriptInterface(new KCPPayPinInfoBridge() , "KCPPayPinInfo" ); // 페이핀 기능 추가
        mWeb.addJavascriptInterface(new KCPPayPinReturn()     , "KCPPayPinRet"  ); // 페이핀 기능 추가

        mWeb.setWebViewClient  ( new mWebViewClient()  );
        mWeb.setWebChromeClient( new mWebChromeClient() {

        	// ktk  shlee 수정 guide
			 @Override
			 public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
				 // Dialog Create Code
				 WebView newWebView = new WebView(activity);
				 WebSettings webSettings = newWebView.getSettings();
				 webSettings.setJavaScriptEnabled(true);

				 final Dialog dialog = new Dialog(activity);
				 dialog.setContentView(newWebView);

				 ViewGroup.LayoutParams params = dialog.getWindow().getAttributes();
				 params.width = ViewGroup.LayoutParams.MATCH_PARENT;
				 params.height = ViewGroup.LayoutParams.MATCH_PARENT;
				 dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
				 dialog.show();
				 newWebView.setWebChromeClient(new WebChromeClient() {
					 @Override
					 public void onCloseWindow(WebView window) {
						 dialog.dismiss();
					 }
				 });

				 // WebView Popup에서 내용이 안보이고 빈 화면만 보여 아래 코드 추가
				 newWebView.setWebViewClient(new WebViewClient() {
					 @Override
					 public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
						 return false;
					 }
				 });

				 ((WebView.WebViewTransport)resultMsg.obj).setWebView(newWebView);
				 resultMsg.sendToTarget();

				 return true;
				// return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
			 }
			 // end ktk

    	});




		//mWeb.clearCache(true); //BJH 2016.07.15

		mWeb.setVerticalScrollbarOverlay(true);
		//mWeb.setBackgroundColor(Color.TRANSPARENT);


		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // BJH 2017.02.22 안드로이드(Android 5.0) Lollipop Webview issue
			mWeb.getSettings().setMixedContentMode(WebSettings
					.MIXED_CONTENT_ALWAYS_ALLOW);

			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.setAcceptCookie(true);
			cookieManager.setAcceptThirdPartyCookies(mWeb, true);
		}
		
		mWeb.requestFocus(View.FOCUS_DOWN);
		mWeb.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_UP:
					if (!v.hasFocus()) {
						v.requestFocus();
					}
					break;
				}
				return false;
			}
		});
		
		mBtnNaviClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (mPayResult)
				{
					finishActivity(getResources().getString(R.string.pay_ok));
					return;
				}
				finish();
			}
		});
		
		mBtnNaviReload.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mWeb.reload();
			}
		});		
		
		/*
		mBtnNaviBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (mWeb.canGoBack())
				{
					mWeb.goBack();
				}
				else
				{
					DialMain.skipCallview = false;
					finish();
				}				
			}
		});
		
		mBtnNaviPrev.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mWeb.canGoForward())
				{
					mWeb.goForward();
				}
			}
		});
		*/
		
		//For Web page 
		String defaultTitle = getResources().getString(R.string.app_name);
		String defaultUrl = "http://m.dial070.co.kr/";
		Intent intent = getIntent();
		String url = intent.getStringExtra("url");		
		mTitle = intent.getStringExtra("title");
		
		mPopup = intent.getBooleanExtra("popup", false); // BJH 말톡 사용자별 팝업창
		
		int notification_id = intent.getIntExtra("id", 0);
		if (notification_id > 0)
		{
			NotificationManager notificationManager = (NotificationManager) this.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
			notificationManager.cancel(notification_id);
		}
		
		if(url == null) url = defaultUrl;
		if(mTitle == null) mTitle = defaultTitle;
		mWebTitle.setText(mTitle);
		
		mWebURL = url;
		Reload();
		
	}
	
	private void Reload()
	{
		if (mWebURL == null || mWebURL.length() == 0) return;
		
		String postData = "AppUrl=dialpay://card_pay";
		//mWeb.loadUrl(mWebURL);
		mWeb.postUrl( mWebURL, EncodingUtils.getBytes(postData, "BASE64") );
	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		currentContext = null;
		/*if (mWeb != null) //BJH 2016.10.11 로딩이 완료되기전에 웹뷰를 닫으면 어플이 죽음..
		{
			mWeb.destroy();
		}*/
		super.onDestroy();
	}

	@Override
	public void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mWeb != null) {
			mWeb.destroy();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		currentState = 0;
		super.onPause();
		//BJH 
		unregisterReceiver(completeReceiver);
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		currentState = 1;
		super.onResume();
		//BJH 다운완료
		IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
	    registerReceiver(completeReceiver, completeFilter); 
	}


	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		
		if (intent == null) return;
		
		int notification_id = intent.getIntExtra("id", 0);
		if (notification_id > 0)
		{
			NotificationManager notificationManager = (NotificationManager) this.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
			notificationManager.cancel(notification_id);
		}
		
		String url = intent.getStringExtra("url");
		if (url != null)
		{
			mWeb.loadUrl(url);
		}
	}
	
	/// KCP
	
	private boolean url_scheme_intent( WebView view, String url )
    {

		Log.d( THIS_FILE, "[PayDemoActivity] called__test - url=[" + url + "]" );
    	// shlee add for google policy intent url

		// convert Intent scheme URL to Intent object
		Intent intent = null;
		try {
			intent = Intent.parseUri(url,Intent.URI_INTENT_SCHEME);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		// forbid launching activities without BROWSABLE category
		intent.addCategory("android.intent.category.BROWSABLE");
		// forbid explicit call
		intent.setComponent(null);
		// forbid Intent with selector Intent
		intent.setSelector(null);



        //chrome 버젼 방식 : 2014.01 추가
        if ( url.startsWith( "intent" ) )
        {

        	Bundle bundle;

            //ILK 용
            if( url.contains( "com.lotte.lottesmartpay" ) )
            {
                try{
					// start the activity by the Intent
					view.getContext().startActivity(intent, mbundle);
                  //  startActivity( Intent.parseUri(url, Intent.URI_INTENT_SCHEME) );
                }/* catch ( URISyntaxException        e ) {
                    Log.d( THIS_FILE, "[PayDemoActivity] URISyntaxException=[" + e.getMessage() + "]" );
                    return false;
                } */
                catch ( ActivityNotFoundException e ) {
                    Log.d( THIS_FILE, "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
                    return false;
                }
            }
            //ILK 용
            else if ( url.contains( "com.ahnlab.v3mobileplus" ) )
            {
                try {
					// start the activity by the Intent
					view.getContext().startActivity(intent, mbundle);
                    //view.getContext().startActivity(Intent.parseUri(url, 0));
                } catch ( ActivityNotFoundException e ) {
                    Log.d( THIS_FILE, "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
                    return false;
                }
            }
            //폴라리스 용
            else
            {
              //  Intent intent = null;
                
                try {
					// start the activity by the Intent
					view.getContext().startActivity(intent, mbundle);
                   // intent = Intent.parseUri( url, Intent.URI_INTENT_SCHEME );
                } catch ( ActivityNotFoundException e ) {
					Log.d( THIS_FILE, "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
					return false;
				}
                
                // 앱설치 체크를 합니다.
                if ( getPackageManager().resolveActivity( intent, 0 ) == null )
                {
                    String packagename = intent.getPackage();
                    
                    if ( packagename != null )
                    {
						// start the activity by the Intent
						view.getContext().startActivity(intent, mbundle);
                     //   startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packagename ) ) );
                        
                        return true;
                    }
                }
                
                intent = new Intent( Intent.ACTION_VIEW, Uri.parse( intent.getDataString() ) );
                
                try{

					// start the activity by the Intent
					view.getContext().startActivity(intent, mbundle);
                //    startActivity( intent );
                }catch( ActivityNotFoundException e ) {
                    Log.d( THIS_FILE, "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
                    return false;
                }
            }
        }
        // 기존 방식
        else
        {
            /*
            if ( url.startsWith( "ispmobile" ) )
            {
                if( !new PackageState( this ).getPackageDownloadInstallState( "kvp.jjy.MispAndroid" ) )
                {
                    startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse( "market://details?id=kvp.jjy.MispAndroid320" ) ) );
                    
                    return true;
                }
            }
            else if ( url.startsWith( "paypin" ) )
            {
                if( !new PackageState( this ).getPackageDownloadInstallState( "com.skp.android.paypin" ) )
                {
                    if( !url_scheme_intent( "tstore://PRODUCT_VIEW/0000284061/0" ) )
                    {
                        url_scheme_intent( "market://details?id=com.skp.android.paypin&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5za3AuYW5kcm9pZC5wYXlwaW4iXQ.k" );
                    }
                    
                    return true;
                }
            }
            */
            /*  shlee remove
            // 삼성과 같은 경우 어플이 없을 경우 마켓으로 이동 할수 있도록 넣은 샘플 입니다.
            // 실제 구현시 업체 구현 여부에 따라 삭제 처리 하시는것이 좋습니다.
            if ( url.startsWith( "mpocket.online.ansimclick" ) )
            {
                if( !new PackageState( this ).getPackageDownloadInstallState( "kr.co.samsungcard.mpocket" ) )
                {
                    Toast.makeText(this, "어플을 설치 후 다시 시도해 주세요.", Toast.LENGTH_LONG).show();
                    
                    startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse( "market://details?id=kr.co.samsungcard.mpocket" ) ) );
                    
                    return true;
                }
            }
            
            try
            {
                startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( url ) ) );
            }
            catch(Exception e)
            {
                // 어플이 설치 안되어 있을경우 오류 발생. 해당 부분은 업체에 맞게 구현
                Toast.makeText(this, "해당 어플을 설치해 주세요.", Toast.LENGTH_LONG).show();
            }

             */
        }

        return true;
    }
	 
    private class mWebViewClient extends WebViewClient
    {
    	@Override
		public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) { //BJH 2016.07.11 인증서 보안 문제
			final AlertDialog.Builder builder = new AlertDialog.Builder(PayClient.this);
			String message = getResources().getString(R.string.ssl_untrusted);
			switch (error.getPrimaryError())
			{
			    case SslError.SSL_UNTRUSTED:
			    	message = getResources().getString(R.string.ssl_untrusted);
			    	break;
			    case SslError.SSL_EXPIRED:
			        message = getResources().getString(R.string.ssl_expired); 
			        break;
			    case SslError.SSL_IDMISMATCH:
			        message = getResources().getString(R.string.ssl_untrusted);
			        break;
			    case SslError.SSL_NOTYETVALID:
			        message = getResources().getString(R.string.ssl_untrusted);
			        break;
			    default:
			    	message = getResources().getString(R.string.ssl_untrusted);
			    	break;
			}
			builder.setTitle(getResources().getString(R.string.ssl_error));
			builder.setMessage(message);
			 
			builder.setPositiveButton(getResources().getString(R.string.ssl_continue), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					handler.proceed();
				}
			});
			  
			builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					handler.cancel();
				}
			});
			  
			final  AlertDialog dialog = builder.create();
			dialog.show();
		}
    	
        @Override
        public boolean shouldOverrideUrlLoading( WebView view, String url )
        {
            Log.d( THIS_FILE, "[PayClient] called__shouldOverrideUrlLoading - url=[" + url + "]" );

            if (url != null && !url.equals("about:blank"))
            {
                if( url.startsWith("http://") || url.startsWith("https://"))
                {
                    if (url.contains("http://market.android.com")            ||
                        url.contains("http://m.ahnlab.com/kr/site/download") ||
                        url.endsWith(".apk")                                   )
                    {
                        return url_scheme_intent( view, url );
                    }
                    //BJH DownloadManager
                    else if(url.contains(".mp3")){
                    	Uri urlToDownload = Uri.parse(url);
                    	List<String> pathSegments = urlToDownload.getPathSegments();
                    	request = new DownloadManager.Request(urlToDownload);
                        String mFileName = pathSegments.get(pathSegments.size()-1);
                        String[] fileName = mFileName.split("-");
                        mFileName = fileName[0].concat("-".concat(fileName[3]));
                        request.setTitle(mFileName);
                        request.setDescription(getResources().getString(R.string.record_file_download));
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        	request.setShowRunningNotification(true);  
                        } else {
                        	request.setNotificationVisibility(request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        }
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, mFileName);
//                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
                        downloadManager.enqueue(request);
                    }
                    //BJH 말톡 사용자별 설명서에서 홈페이지 클릭을 눌렀을 때 새창으로 이동
                    else if (url.contains("www.maaltalk.com")) { // BJH 2016.11.23
                    	if(url.contains("callfw.php")) {
    						Uri u = Uri.parse(url);
    						if (u != null) {
    							String callfw = u.getQueryParameter("callfw");
								mNumber = "tel:" + callfw;
								makeCall();
    						}
                    	} else {
                    		try {
                    			Intent intent = new Intent(
                    					Intent.ACTION_VIEW,
                    					Uri.parse(url));
                    			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    			startActivity(intent);
                    		} catch (Exception e) {
                    		}
                    	}
					}
                    else if (url.contains("usim_call.php")) { // BJH 2017.02.01
                    	url = url.replace("#", "%23"); // #으로 getQueryParameter하면 #이 없으므로
                    	Uri u = Uri.parse(url);
                    	if (u != null) {
							String call = u.getQueryParameter("call");
							call = call.replace("#", "%23"); // ACTION_CALL 할 때 #으로 들어가면 #이 빠지므로
							/*Intent intent;
							if (Build.VERSION.SDK_INT >= 26)
								intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + call));
							else
								intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + call));
							startActivity(intent);*/


							Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + call));
							startActivity(intent);
						}
					}
                    //BJH 말톡 서비스 해제
                    //else if (url.contains("maaltalk_svc_out.php")) {
                    	//serviceOut();
					//}
                    else
                    {
                        view.loadUrl( url );
                        return false;
                    }
                }
                else if(url.startsWith("mailto:"))
                {
                    return false;
                }
                else if(url.startsWith("tel:"))
                {
                    return false;
                }
                else
                {
                    return url_scheme_intent( view, url );
                }
            }

            return true;
        }

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			Log.d( THIS_FILE, "[PayClient] onPageFinished - url=[" + url + "]" );
			super.onPageFinished(view, url);
			
        	if (url.endsWith("ok.php"))
        	{
        		finishActivity(getResources().getString(R.string.pay_ok));
        	}
        	//BJH
        	else if (url.endsWith("auto_close.php"))
        	{
        		finishActivity(getResources().getString(R.string.request_ok));
        	}
        	//BJH 말톡 사용자별 팝업창 닫기
		    else if (mPopup && url.endsWith("closed.php")) { 
		    	finishActivity("popup");
		    } 
		    else if (url.endsWith("closed.php")) { //BJH 2016.10.02
		    	finish();
		    } 
		    else if (url.endsWith("maaltalk_server.php")) { // 말톡 사용자별 설명서에서
		    	finishActivity("maaltalk_server"); // 서버 이동 할 경우
		    }
		    else if (url.contains("maaltalk_svc_out.php")) {// 말톡 서비스 해지
		    	finishActivity(getResources().getString(R.string.svc_out));
			}
        	else if (url.endsWith("fail.php"))
        	{
        		Alert(mContext, getResources().getString(R.string.pay_error));
        		Reload();
        	}	    			
        	else if (url.endsWith("cancel.php"))
        	{
        		Reload();
        	}
        	else if (url.endsWith("usim_close.php")) //BJH 2017.02.01
        	{
        		finishActivity(getResources().getString(R.string.apn_lookup_fail));
        	}
		}
    }	
    //BJH 2016.06.22
    private class mWebChromeClient extends WebChromeClient
    {
    	public void onProgressChanged(WebView view, int progress)
        {
            activity.setTitle(getResources().getString(R.string.loading));
            activity.setProgress(progress * 100);

            if(progress == 100)
            {
            	activity.setTitle(R.string.app_name);
            	//activity.setTitle(mContext.getResources().getString(R.string.app_name) + " > " + mTitle);
            }
        }

		//BJH FileChooser
		private Uri imageUri;
		protected void openFileChooser(ValueCallback uploadMsg, String acceptType)
		{
			mUploadMessage = uploadMsg;
			final List<Intent> cameraIntents = new ArrayList<Intent>();
			final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			final PackageManager packageManager = getPackageManager();
			final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
			for(ResolveInfo res : listCam) {
				final String packageName = res.activityInfo.packageName;
				final Intent catpure = new Intent(captureIntent);
				catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
				catpure.setPackage(packageName);
				catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				cameraIntents.add(catpure);

			}
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");
			Intent chooserIntent = Intent.createChooser(i,"Image Chooser");
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
			startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
		}


		// For Lollipop 5.0 Devices BJH 2017.03.07
		@TargetApi(Build.VERSION_CODES.LOLLIPOP)
		public boolean onShowFileChooser(WebView mWebView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams)
		{
			if (mUploadMessages != null) {
				mUploadMessages.onReceiveValue(null);
				mUploadMessages = null;
			}

			mUploadMessages = filePathCallback;

			final List<Intent> cameraIntents = new ArrayList<Intent>();
			final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			final PackageManager packageManager = getPackageManager();
			final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
			for(ResolveInfo res : listCam) {
				final String packageName = res.activityInfo.packageName;
				final Intent catpure = new Intent(captureIntent);
				catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
				catpure.setPackage(packageName);
				catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				cameraIntents.add(catpure);

			}
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");
			Intent chooserIntent = Intent.createChooser(i,"Image Chooser");
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
			startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);

			return true;
		}

		//For Android 4.1
		protected void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture)
		{
			mUploadMessage = uploadMsg;
			final List<Intent> cameraIntents = new ArrayList<Intent>();
			final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			final PackageManager packageManager = getPackageManager();
			final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
			for(ResolveInfo res : listCam) {
				final String packageName = res.activityInfo.packageName;
				final Intent catpure = new Intent(captureIntent);
				catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
				catpure.setPackage(packageName);
				catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				cameraIntents.add(catpure);

			}
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");
			Intent chooserIntent = Intent.createChooser(i,"Image Chooser");
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
			startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
		}

		protected void openFileChooser(ValueCallback<Uri> uploadMsg)
		{
			mUploadMessage = uploadMsg;
			final List<Intent> cameraIntents = new ArrayList<Intent>();
			final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			final PackageManager packageManager = getPackageManager();
			final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
			for(ResolveInfo res : listCam) {
				final String packageName = res.activityInfo.packageName;
				final Intent catpure = new Intent(captureIntent);
				catpure.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
				catpure.setPackage(packageName);
				catpure.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				cameraIntents.add(catpure);

			}
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");
			Intent chooserIntent = Intent.createChooser(i,"Image Chooser");
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
			startActivityForResult(Intent.createChooser(chooserIntent, "File Browser"), FILECHOOSER_RESULTCODE);
		}
    }	

    private class KCPPayPinReturn
    {
        public String getConfirm()
        {
        	App myApp = (App)getApplication();
            
            if( myApp.b_type )
            {
                myApp.b_type = false;
                
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }
    
    private class KCPPayPinInfoBridge
    {
        public void getPaypinInfo(final String url)
        {
            handler.post( new Runnable() {
                public void run()
                {
                    Log.d( THIS_FILE, "[PayClient] KCPPayPinInfoBridge=[getPaypinInfo]" );

                    PackageState ps = new PackageState( PayClient.this );

                    if(!ps.getPackageAllInstallState( "com.skp.android.paypin" ))
                    {
                        paypinConfim();
                    }
                    else
                    {
                        url_scheme_intent( null, url );
                    }
                }
            });
        }

        private void paypinConfim()
        {
            AlertDialog.Builder  dlgBuilder = new AlertDialog.Builder( PayClient.this );
            AlertDialog          alertDlg;

            dlgBuilder.setTitle(getResources().getString(R.string.confirm));
            dlgBuilder.setMessage(getResources().getString(R.string.pay_paypin));
            dlgBuilder.setCancelable( false );
            dlgBuilder.setPositiveButton(getResources().getString(R.string.install),
                                          new DialogInterface.OnClickListener()
                                              {
                                                  @Override
                                                  public void onClick(DialogInterface dialog, int which)
                                                  {
                                                      dialog.dismiss();
                                                      
                                                      if( 
                                                          //url_scheme_intent( "https://play.google.com/store/apps/details?id=com.skp.android.paypin&feature=nav_result#?t=W10." );
                                                          //url_scheme_intent( "market://details?id=com.skp.android.paypin&feature=nav_result#?t=W10." );
                                                          !url_scheme_intent( null, "tstore://PRODUCT_VIEW/0000284061/0" )
                                                      )
                                                      {
                                                          url_scheme_intent( null, "market://details?id=com.skp.android.paypin&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5za3AuYW5kcm9pZC5wYXlwaW4iXQ.k" );
                                                      }
                                                  }
                                              }
                                          );
            dlgBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                                          new DialogInterface.OnClickListener()
                                              {
                                                  @Override
                                                  public void onClick(DialogInterface dialog, int which)
                                                  {
                                                      dialog.dismiss();
                                                      
                                                      Toast.makeText(PayClient.this, getResources().getString(R.string.setting_pay_cancel) , Toast.LENGTH_SHORT).show();
                                                  }
                                              }
                                          );

            alertDlg = dlgBuilder.create();
            alertDlg.show();    
        }
    }
    
    private class KCPPayBridge
    {
        public void launchMISP( final String arg )
        {
            handler.post( new Runnable() {
                public void run()
                {
                    String  strUrl;
                    String  argUrl;

                    PackageState ps = new PackageState( PayClient.this );

                    argUrl = arg;

                    if(!arg.equals("Install"))
                    {
                        if(!ps.getPackageDownloadInstallState( "kvp.jjy.MispAndroid" ))
                        {
                            argUrl = "Install";
                        }
                    }

                    strUrl = ( argUrl.equals( "Install" ) == true )
                                ? "market://details?id=kvp.jjy.MispAndroid320" //"http://mobile.vpay.co.kr/jsp/MISP/andown.jsp"
                                : argUrl;

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse( strUrl ) );

                    m_nStat = PROGRESS_STAT_IN;

                    startActivity( intent );
                }
            });
        }
    }
    

    @Override
    protected void onRestart() {
		super.onRestart();
	//	super.onResume();

		Log.d(THIS_FILE, "[PayDemoActivity] called__onResume + INPROGRESS=[" + m_nStat + "]");

		App myApp = (App) getApplication();

		// 하나 SK 모듈로 결제 이후 해당 카드 정보를 가지고 오기위해 사용
		if (myApp.m_uriResult != null) {
			if ((myApp.m_uriResult.getQueryParameter("isp_res_cd") == null ? "" :
					myApp.m_uriResult.getQueryParameter("isp_res_cd")).equals("0000")) {
				Log.d(THIS_FILE,
						"[PayDemoActivity] ISP Result = 0000");

				mWeb.loadUrl("http://testpay.kcp.co.kr/lds/smart_phone_linux_jsp/sample/card/samrt_res.jsp?result=OK&a=" + myApp.m_uriResult.getQueryParameter("a"));
			} else {
				Log.d(THIS_FILE, "[PayDemoActivity] ISP Result = cancel");
			}
		}

		if (m_nStat == PROGRESS_STAT_IN) {
			checkFrom();
		}

		myApp.m_uriResult = null;
	}

    private void checkFrom()
    {
        try
        {
            App myApp = (App)getApplication();

            if ( myApp.m_uriResult != null )
            {
                m_nStat = PROGRESS_DONE;

                String strResultInfo = myApp.m_uriResult.getQueryParameter( "approval_key" );

                if ( strResultInfo == null || strResultInfo.length() <= 4 )  finishActivity(getResources().getString(R.string.pay_error_isp));

                String  strResCD = strResultInfo.substring( strResultInfo.length() - 4 );

                Log.d(THIS_FILE, "[PayDemoActivity] result=[" + strResultInfo + "]+" + "res_cd=[" + strResCD + "]" );

                if ( strResCD.equals( "0000" ) == true )
                {
                    String strApprovalKey = "";

                    strApprovalKey = strResultInfo.substring( 0, strResultInfo.length() - 4  );

                    Log.d(THIS_FILE, "[PayDemoActivity] approval_key=[" + strApprovalKey + "]" );

                    if (ServicePrefs.DIAL_PAY_TEST)
                    {
                    	mWeb.loadUrl( "https://devpggw.kcp.co.kr:8100/app.do?ActionResult=app&AppUrl=dialpay&approval_key=" + strApprovalKey );
                    }
                    else
                    {
                    	mWeb.loadUrl( "https://smpay.kcp.co.kr/app.do?ActionResult=app&AppUrl=dialpay&approval_key=" + strApprovalKey );
                    }
                    // OK
                    mPayResult = true;
                    
                }
                else if ( strResCD.equals( "3001" ) == true )
                {
                    finishActivity(getResources().getString(R.string.pay_error_isp_user));
                }
                else
                {
                    finishActivity(getResources().getString(R.string.pay_error_isp_etc));
                }
            }
        }
        catch ( Exception e )
        {
        }
        finally
        {
        }
    }
    @Override
    protected Dialog onCreateDialog( int id )
    {
        Log.d(THIS_FILE, "[PayDemoActivity] called__onCreateDialog - id=[" + id + "]" );

        super.onCreateDialog( id );

        AlertDialog.Builder dlgBuilder = new AlertDialog.Builder( this );
        AlertDialog alertDlg;

        dlgBuilder.setTitle(getResources().getString(R.string.cancel));
        dlgBuilder.setMessage(getResources().getString(R.string.pay_work_cancel));
        dlgBuilder.setCancelable( false );
        dlgBuilder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                finishActivity(getResources().getString(R.string.pay_error_user_cancel));
            }
        });

        dlgBuilder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        alertDlg = dlgBuilder.create();

        return  alertDlg;
    }
    
    public void finishActivity( String p_strFinishMsg )
    {
    	Log.d(THIS_FILE, "[PayDemoActivity] finishActivity : " + p_strFinishMsg );

        if ( p_strFinishMsg != null )
        {
        	Intent intent = new Intent();
            intent.putExtra( ACTIVITY_RESULT, p_strFinishMsg );
            setResult( RESULT_OK, intent );
        }
        else
        {
            setResult( RESULT_CANCELED );
        }

        finish();
    }    
	
    //BJH
    public void onActivityResult(int requestCode, int resultCode, Intent intent)//파일 업로드 결과
	{
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == FILECHOOSER_RESULTCODE) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // BJH 2017.03.07
				if (mUploadMessages == null) return;
				mUploadMessages.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
				mUploadMessages = null;
			} else {
				if (mUploadMessage == null) return;
				Uri result = intent == null || resultCode != PayClient.RESULT_OK ? null : intent.getData();
				mUploadMessage.onReceiveValue(result);
				mUploadMessage = null;
			}
		} else
			Toast.makeText(mContext, "Failed to Upload Image", Toast.LENGTH_LONG).show();
	}
    private BroadcastReceiver completeReceiver = new BroadcastReceiver(){//다운로드 완료되었을 때
   	 
    	@Override
    	public void onReceive(Context context, Intent intent) {
    	    Toast.makeText(context, "다운로드가 완료되었습니다.",Toast.LENGTH_SHORT).show();
    	}
    	     
    };
    
    /*public void serviceOut() {//BJH 서비스 해제
		//BJH Webview Session Clear
		CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		cookieManager.removeSessionCookie(); 
		cookieManager.removeAllCookie();
		cookieSyncManager.sync();
		//DB Data Clear
		DBManager db = new DBManager(mContext);
		db.open();
		db.deleteAllRecentCalls();
		db.deleteAllSmsList();
		db.deleteAllSmsMsg();
		db.deleteAllFavorites(); 
		db.close();
		//Stop Service
		stopService(new Intent(mContext, MessageService.class));
		stopService(new Intent(mContext, SipService.class));
		stopService(new Intent(mContext, StartForegroundService.class));
		
		if (ServicePrefs.mLogin) {
			ServicePrefs.logout();
		}
		// FOR SAFE
		SipService.currentService = null;
		//Preference Clear
		mPrefs.resetAllDefaultValues();
		mSysPrefs.resetAllDefaultValues();
		//Go Main
		//android.os.Process.killProcess(android.os.Process.myPid());
		Intent intent = new Intent(mContext, DialMain.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
		//Intent intent = new Intent(mContext, CloseActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				//| Intent.FLAG_ACTIVITY_CLEAR_TASK
				//| Intent.FLAG_ACTIVITY_NEW_TASK);
		//startActivity(intent);
		//finish();
	}*/

	private void makeCall() {
		//Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(mNumber));
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(mNumber));
		startActivity(intent);
	}

	private static final int REQUEST_CODE_PERMISSIONS = 1;
	private String mNumber;

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_CODE_PERMISSIONS: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					makeCall();
				} else {
					MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(this);
					builder.setTitle(getResources().getString(R.string.permission_setting));
					builder.setMessage(getResources().getString(R.string.permission_desc));
					builder.setCancelable(false);
					builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
							Uri uri = Uri.fromParts("package", getPackageName(), null);
							intent.setData(uri);
							startActivity(intent);
							finish();
						}
					});
					builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							finish();
						}
					});
					builder.show();
				}
			}
		}
	}

	private static boolean hasPermissions(Context context, String... permissions) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}

}
