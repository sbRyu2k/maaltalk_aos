package com.dial070.ui;


import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dial070.DialMain;
import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;

public class PopupFavorite extends Activity {

//    private final static int SELECT_CALLS = 1;
    private final static int SELECT_FAVORITE = 2;
    private final static int ADD_CONTACT = 3;
    
    private AppPrefs mPrefs;
	private Context mContext;
	private String mName, mNumber,mMemo;
	private int mRid;
	
	private LinearLayout mLayout;
	private ImageView mUserPhoto;
	private TextView mTextName, mTextPhone, mTextMemo;
	private ImageButton mButtonAdd;
	private ImageButton mButtonCall, mButtonSms, mButtonMemo;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.popup_favorite_detail);		
		
		mContext = this;
		mPrefs = new AppPrefs(this);
		
		Intent intent = getIntent();
		mName = intent.getStringExtra("NAME");
		mNumber = intent.getStringExtra("PHONE");
		mRid = intent.getIntExtra("ID", 0);
		mMemo = intent.getStringExtra("MEMO");
		
		mLayout = (LinearLayout) findViewById(R.id.favorites_detail_layout);
		mUserPhoto = (ImageView) findViewById(R.id.img_user_photo);
		mTextName = (TextView) findViewById(R.id.txt_user_name);
		mTextPhone = (TextView) findViewById(R.id.txt_user_phone);
		mTextMemo = (TextView) findViewById(R.id.txt_user_memo);
		mButtonAdd = (ImageButton) findViewById(R.id.btn_add_contact);
		
		//초기화
		if (mNumber == null) mName = "";
		if (mNumber == null) mNumber = "";	
		
		if(mMemo.length() == 0 || mMemo == null)
		{
			mTextMemo.setText("");
			mTextMemo.setHint(getResources().getString(R.string.memo_hint));
		}
		else
		{
			mTextMemo.setText(mMemo);
		}

		mButtonAdd.setVisibility(View.GONE);
		
		mTextName.setText(mName);
		if((mNumber.length() > 0) && mName.equals(mNumber))
		{
			mTextPhone.setVisibility(View.VISIBLE);
			mTextPhone.setText("");
			
			// Check Contact
			String cname = ContactHelper.getContactsNameByPhoneNumber(mContext,mNumber);
			if(cname == null)
				mButtonAdd.setVisibility(View.VISIBLE);
		}
		else
		{
			mTextPhone.setText(mNumber);
		}
		
		
		
		if(mNumber.length() > 0)
		{
        	Uri u = ContactHelper.getPhotoUriByPhoneNumber(mContext, mNumber);
			if (u != null) {
				mUserPhoto.setImageURI(u);
			} 
		}
		
		mButtonCall = (ImageButton) findViewById(R.id.btn_popup_call);
		mButtonSms = (ImageButton) findViewById(R.id.btn_popup_sms);
		mButtonMemo = (ImageButton) findViewById(R.id.btn_popup_memo);
		
		mButtonCall.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sipCall(mNumber);
				finish();
			}
		});
		
		mButtonSms.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendSMS(mNumber);
				finish();
			}
		});
		
		
		mButtonMemo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//메모 작성
				if(mRid > 0)
					writeMemo(mRid,mName,mNumber);
			}
		});
		
		mLayout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		mButtonAdd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//
				if(mNumber.length() > 0) contactAdd(mNumber);
			}
		});
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		mLayout.setVisibility(View.INVISIBLE);
		super.onPause();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		mLayout.setVisibility(View.VISIBLE);
		super.onResume();
	}

	private void contactAdd(String number)
	{
		DialMain.skipClose = true;
		boolean existUser = false;
		long uid = 0;
		//기존 연락처 있는지 검사 
		Cursor c = ContactHelper.getContactsByPhoneNumber(mContext, number);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				existUser = true;
				uid = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
			}
			c.close();
		}
		
		if(!existUser)
		{
			Intent intent = new Intent();
			intent.setAction(ContactsContract.Intents.SHOW_OR_CREATE_CONTACT);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			intent.setData(Uri.fromParts("tel", number, null));
			intent.putExtra(ContactsContract.Intents.EXTRA_FORCE_CREATE, true);

			if(number.startsWith("01"))
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
			else
				intent.putExtra(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER);
	
			startActivityForResult(intent,ADD_CONTACT);
		}
		else
		{
			Intent intent = new Intent(Intent.ACTION_EDIT);
			//intent.setData(Uri.parse(ContactsContract.Contacts.CONTENT_LOOKUP_URI + "/" + uid));
			Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, uid);
			intent.setData(contactUri);
			startActivityForResult(intent,ADD_CONTACT);
		}
		
	}		
	
	private boolean sipCall(String number)
	{
		// check number
		if (number == null) return false; 
		if(number.length() == 0) return false;
	
		/*
		Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
		intent.putExtra(DialMain.EXTRA_COMMAND, "CALL");
		intent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
		sendBroadcast(intent);
		*/
		
		mPrefs.setPreferenceStringValue(AppPrefs.AUTO_DIAL, number);
		return true;
	}		
	
	private void sendSMS(String number)
	{
		//SMS 문자 보내기
		if (number == null) return;
		
		DialMain.skipClose = true;

		try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            Uri uri = Uri.parse("sms:" + number);
            intent.setData(uri);
            startActivity(intent);			
		} catch (Exception e) { 
			e.printStackTrace(); 
		}
	}	
	
	private String getCurrentMemo(int id)
	{
		String rs = "";
		DBManager database = new DBManager(mContext);
		try 
		{
			database.open();
			Cursor c = database.getFavorite(id);
			
			if(c != null)
			{
				if(c.getCount() > 0 && c.moveToFirst())
				{
					rs = c.getString(c.getColumnIndex(FavoritesData.FIELD_FIELD2));
				}
				c.close();
			}

		} finally {
			database.close();
		}	
		
		return rs;
	}		
	
	private void updateFavoriteName()
	{
		String newName = ContactHelper.getContactsNameByPhoneNumber(mContext,mNumber);
		if(newName == null) return;
		
		//Log.e("POPUP_FAVORITE","NEW NAME=" + newName);
		
		//update UI
		mTextName.setText(newName);
		mTextPhone.setText(mNumber);
		mButtonAdd.setVisibility(View.GONE);
		
		DBManager database = new DBManager(mContext);
		try 
		{
			database.open();
			database.updateFavoritesName(mNumber,mName);
		} finally {
			database.close();
		}			
	}
	
	private void writeMemo(int id, String name, String number)
	{
		
		String memo = getCurrentMemo(id);
		
		Intent intent = new Intent(PopupFavorite.this, PopupMemo.class);
		intent.putExtra("TYPE", "FAVORITES");
		intent.putExtra("NAME", name);
		intent.putExtra("PHONE", number);
		intent.putExtra("ID", id);
		intent.putExtra("MEMO", memo);
		startActivityForResult(intent,SELECT_FAVORITE);			
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	    
		switch (requestCode)
		{
			case SELECT_FAVORITE:
				if (resultCode == RESULT_OK)
				{
					mTextMemo.setText(data.getStringExtra("MEMO"));
				}
				break;
			case ADD_CONTACT:
				//if(resultCode == RESULT_OK)
				{
					//update favorite 
					updateFavoriteName();
				}
				break;
			default:
				break;
		}
		return;			
		
		
		
	}
	
	
	
}
