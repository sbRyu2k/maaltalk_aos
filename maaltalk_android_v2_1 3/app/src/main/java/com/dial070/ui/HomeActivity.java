package com.dial070.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.service.SipService;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.utils.OnSingleClickListener;

public class HomeActivity extends AppCompatActivity {
    public final static String BC_HOME_UPDATE_BALANCE= "HOME_UPDATE_BALANCE";
    private BroadcastReceiver mBrUpdateBalance;
    private AppPrefs mPrefs;
    private Context mContext;
    private static final int SELECT_PAYMENT = 1;
    private boolean mUserMsg = false;
    private static final int SELECT_USER_MSG = 6;
    private TextView txtMyNumber, txtLabelExpirationDate2, txtExpirationDate, txtMyPlan, txtLabelPlanExpirationDate2, txtPlanExpirationDate,
            txtLabelMyBalance, txtMyBalance, txtMyDistributor, txtLabelExpirationDate3, txtLabelPlanExpirationDate3, txtMyMaxBalance, txtVersion;
    private ProgressBar progressBalance;
    private final static String TAG="HomeActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        DebugLog.d("onCreate()");

        try {
            getSupportActionBar().hide();
        }catch(NullPointerException e) {  // shlee add exception  because getsupportActionbar  return null under os 9
            Log.e("HomeActivity","nullpointer");
            e.printStackTrace();
        }

        mContext = this;

        mPrefs = new AppPrefs(this);

        txtMyNumber = findViewById(R.id.txtMyNumber);
        txtLabelExpirationDate2 = findViewById(R.id.txtLabelExpirationDate2);
        txtExpirationDate = findViewById(R.id.txtExpirationDate);
        txtMyPlan = findViewById(R.id.txtMyPlan);
        txtLabelPlanExpirationDate2 = findViewById(R.id.txtLabelPlanExpirationDate2);
        txtPlanExpirationDate = findViewById(R.id.txtPlanExpirationDate);
        txtLabelMyBalance = findViewById(R.id.txtLabelMyBalance);
        txtMyBalance = findViewById(R.id.txtMyBalance);
        txtMyDistributor = findViewById(R.id.txtMyDistributor);
        txtLabelExpirationDate3 = findViewById(R.id.txtLabelExpirationDate3);
        txtLabelPlanExpirationDate3 = findViewById(R.id.txtLabelPlanExpirationDate3);
        txtMyMaxBalance = findViewById(R.id.txtMyMaxBalance);
        progressBalance = findViewById(R.id.progressBalance);
        txtVersion = findViewById(R.id.txtVersion);

        progressBalance.setVisibility(View.VISIBLE);

        Button btnPay=findViewById(R.id.btnPay);
        Button btnCoupon=findViewById(R.id.btnCoupon);
        Button btnUsim=findViewById(R.id.btnUsim);
        btnPay.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DebugLog.d("clicked --> btnPay");
                String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE);

                DebugLog.d("test_v2 clicked btnPay url: "+url);

                if (ServicePrefs.mPayType != null
                        && ServicePrefs.mPayType.equalsIgnoreCase("1"))
                    url = mPrefs.getPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA);

                String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
                if (ServicePrefs.DIAL_PAY_TEST)
                    url = "http://110.45.220.168/maaltalk/order_mobile_app_v2.php";

                if (url == null || url.length() == 0)
                    return;
                if (uid == null || uid.length() == 0)
                    return;

                String urlString = String.format("%s?name=%s&refresh=%s", url, uid,System.currentTimeMillis()+"");
                DebugLog.d("test_v2 clicked btnPay urlFull: "+urlString);


                Intent intent = new Intent(mContext, PayClient.class);
                intent.putExtra("url", urlString);
                intent.putExtra("title",
                        getResources().getString(R.string.title_my_point));
                startActivityForResult(intent, SELECT_PAYMENT);
            }
        });
        btnCoupon.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DebugLog.d("clicked --> btnCoupon");
                Toast.makeText(HomeActivity.this ,"해당 기능은 현재 정비중입니다.", Toast.LENGTH_SHORT).show();

//                String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_COUPON_INFO);
//                if (url == null || url.length() == 0)
//                    return;
//                //Intent intent = new Intent(mContext, WebClient.class);
//                Intent intent = new Intent(mContext, TabTravelCouponActivity.class);
//                intent.putExtra("url", url);
//                //intent.putExtra("title",getResources().getString(R.string.title_coupon));
//                startActivity(intent);
            }
        });
        btnUsim.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DebugLog.d("clicked --> btnUsim");
                String url = mPrefs
                        .getPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI);

                if (url == null || url.length() == 0)
                    return;

                Intent intent = new Intent(mContext, PayClient.class);
                intent.putExtra("url", url);
                intent.putExtra("title",
                        getResources().getString(R.string.title_usim_pocket));
                startActivity(intent);
            }
        });

        txtVersion.setText(ServicePrefs.getVersion());

        txtMyNumber.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_DID));
        txtLabelExpirationDate2.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_DID_DATE_EXPIRED));
        txtExpirationDate.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_DID_EXPIRED));
        txtMyPlan.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_PLAN_NAME));
        txtLabelPlanExpirationDate2.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_PLAN_DATE_EXPIRED));
        txtPlanExpirationDate.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_PLAN_EXPIRED));
        txtLabelMyBalance.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_LABEL_BALANCE));
        txtMyBalance.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_BALANCE));
        txtMyMaxBalance.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_BALANCE_MAX));
        txtMyDistributor.setText(mPrefs.getPreferenceStringValue(AppPrefs.HOME_DISTRIBUTOR_ID));
        progressBalance.setMax(mPrefs.getPreferenceIntegerValue(AppPrefs.HOME_PROGRESS_MAX));
        progressBalance.setProgress(mPrefs.getPreferenceIntegerValue(AppPrefs.HOME_PROGRESS));

        String PRODUCT_ID=mPrefs.getPreferenceStringValue(AppPrefs.HOME_PRODUCT_ID);
        if (PRODUCT_ID.length()==4){ //0000, 1130,1310,1520,1030,1012, 4013,4016,4863,4866,4813,4816,4843,4003,4006,4026
            txtExpirationDate.setVisibility(View.VISIBLE);
            txtLabelExpirationDate3.setVisibility(View.VISIBLE);

            String DID_DATE_EXPIRED=mPrefs.getPreferenceStringValue(AppPrefs.HOME_DID_DATE_EXPIRED);
            if (DID_DATE_EXPIRED.equals("- - -") || DID_DATE_EXPIRED.equals(getString(R.string.balance_expiration_070num))) {
                txtExpirationDate.setVisibility(View.INVISIBLE);
                txtLabelExpirationDate3.setVisibility(View.INVISIBLE);
            }

            String PLAN_DATE_EXPIRED=mPrefs.getPreferenceStringValue(AppPrefs.HOME_PLAN_DATE_EXPIRED);
            if (PLAN_DATE_EXPIRED.equals("- - -") || PLAN_DATE_EXPIRED.equals(getString(R.string.balance_expiration_plan))){
                txtPlanExpirationDate.setVisibility(View.INVISIBLE);
                txtLabelPlanExpirationDate3.setVisibility(View.INVISIBLE);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        DebugLog.d("onResume()");
        registerUpdateBalanceReceiver();

        userMsg();

        if (txtMyNumber.getText().toString().trim().length()==0){
            Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
            intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
            sendBroadcast(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        DebugLog.d("onPause()");
        unregisterUpdateBalanceReceiver();
    }

    @Override
    public void onBackPressed() {
        //DialMain.AppCloseRequest(mContext);
        getParent().onBackPressed();
    }

    private void setHomeData(Intent intent){
        Log.i(TAG,"setHomeData");
        DebugLog.d("intent --> "+intent.getClass().getSimpleName());

        String RES_CODE= intent.getStringExtra("RES_CODE");
        String DID= intent.getStringExtra("DID");
        String DID_DATE_EXPIRED= intent.getStringExtra("DID_DATE_EXPIRED");
        String DID_EXPIRED= intent.getStringExtra("DID_EXPIRED");
        String PLAN_NAME= intent.getStringExtra("PLAN_NAME");
        String PLAN_DATE_EXPIRED= intent.getStringExtra("PLAN_DATE_EXPIRED");
        String PLAN_EXPIRED= intent.getStringExtra("PLAN_EXPIRED");
        String BALANCE_MONEY= intent.getStringExtra("BALANCE_MONEY");
        String BALANCE_TIME= intent.getStringExtra("BALANCE_TIME");
        String BALANCE_TIME_MAX= intent.getStringExtra("BALANCE_TIME_MAX");
        String PRODUCT_ID= intent.getStringExtra("PRODUCT_ID");
        String DISTRIBUTOR_ID= intent.getStringExtra("DISTRIBUTOR_ID");
        String resultBalance="";

        if (RES_CODE.equals("0")){
            if (PRODUCT_ID.length()==4){ //0000, 1130,1310,1520,1030,1012, 4013,4016,4863,4866,4813,4816,4843,4003,4006,4026
                if (PRODUCT_ID.equals("1000") || PRODUCT_ID.equals("2081") || PRODUCT_ID.equals("4100")){   //잔여기간
                    txtLabelMyBalance.setText(getString(R.string.balance_time));

                    if (PRODUCT_ID.equals("2081")){
                        resultBalance="";
                    }else {
                        resultBalance=getString(R.string.balance_unlimited);
                        txtMyMaxBalance.setText("MAX");
                        progressBalance.setMax(10);
                        progressBalance.setProgress(10);
                    }
                }else if (PRODUCT_ID.equals("0000") || PRODUCT_ID.startsWith("1") || PRODUCT_ID.startsWith("4")){   //잔여시간
                    txtLabelMyBalance.setText(getString(R.string.balance_time));

                    try {
                        if (BALANCE_TIME!=null && BALANCE_TIME.length()>0 && Integer.parseInt(BALANCE_TIME)<1){
                            resultBalance=getString(R.string.balance_required_charge);
                        }else if (BALANCE_TIME!=null && BALANCE_TIME.length()>0 && Integer.parseInt(BALANCE_TIME)>=1){
                            resultBalance=BALANCE_TIME+getString(R.string.balance_minute);
                        }else {
                            resultBalance=getString(R.string.balance_required_charge);
                        }

                        if (BALANCE_TIME_MAX!=null && BALANCE_TIME_MAX.length()>0 && Integer.parseInt(BALANCE_TIME_MAX)>=10){
                            //txtMyMaxBalance.setVisibility(View.VISIBLE);
                            txtMyMaxBalance.setText(BALANCE_TIME_MAX+getString(R.string.balance_minute));
                            //progressBalance.setVisibility(View.VISIBLE);
                            progressBalance.setMax(Integer.parseInt(BALANCE_TIME_MAX));
                            progressBalance.setProgress(Integer.parseInt(BALANCE_TIME));
                        }else {
                            //txtMyMaxBalance.setVisibility(View.GONE);
                            //progressBalance.setVisibility(View.GONE);
                            txtMyMaxBalance.setText("MAX");
                            progressBalance.setMax(10);
                            progressBalance.setProgress(10);
                        }
                    }catch (NumberFormatException e){
                        resultBalance=getString(R.string.balance_required_charge);
                        //txtMyMaxBalance.setVisibility(View.GONE);
                        //progressBalance.setVisibility(View.GONE);
                        txtMyMaxBalance.setText("MAX");
                        progressBalance.setMax(10);
                        progressBalance.setProgress(0);
                    }

                }else if (PRODUCT_ID.equals("9000") || PRODUCT_ID.startsWith("3")){ //잔액
                    txtLabelMyBalance.setText(getString(R.string.balance_money));

                    //txtMyMaxBalance.setVisibility(View.INVISIBLE);
                    //progressBalance.setVisibility(View.GONE);
                    txtMyMaxBalance.setText("MAX");
                    progressBalance.setMax(10);
                    progressBalance.setProgress(10);
                    try {
                        if (BALANCE_MONEY!=null & BALANCE_MONEY.length()>0 && Integer.parseInt(BALANCE_MONEY)<100){
                            resultBalance=getString(R.string.balance_required_charge);
                            progressBalance.setProgress(0);
                        }else if (BALANCE_MONEY!=null & BALANCE_MONEY.length()>0 && Integer.parseInt(BALANCE_MONEY)>=100){
                            resultBalance=BALANCE_MONEY+getString(R.string.balance_won);
                        }else {
                            resultBalance=getString(R.string.balance_required_charge);
                            progressBalance.setProgress(0);
                        }
                    }catch (NumberFormatException e){
                        resultBalance=getString(R.string.balance_required_charge);
                        progressBalance.setProgress(0);
                    }
                }else{
                    resultBalance="?";
                }

                txtExpirationDate.setVisibility(View.VISIBLE);
                txtLabelExpirationDate3.setVisibility(View.VISIBLE);

                if (DID.length()==0){
                    DID=getString(R.string.balance_no_070num);
                    DID_DATE_EXPIRED="- - -";
                    txtExpirationDate.setVisibility(View.INVISIBLE);
                    txtLabelExpirationDate3.setVisibility(View.INVISIBLE);
                }else {
                    DID= PhoneNumberUtils.formatNumber(DID);
                    try {
                        if (DID_EXPIRED!=null && DID_EXPIRED.length()>0 && Integer.parseInt(DID_EXPIRED)<=0){
                            DID_DATE_EXPIRED=getString(R.string.balance_expiration_070num);
                            txtExpirationDate.setVisibility(View.INVISIBLE);
                            txtLabelExpirationDate3.setVisibility(View.INVISIBLE);
                        }else if (DID_EXPIRED.length()>0 && Integer.parseInt(DID_EXPIRED)>0){
                            DID_DATE_EXPIRED=DID_DATE_EXPIRED+",";
                        }
                    }catch (NumberFormatException e){
                        DID_DATE_EXPIRED=getString(R.string.balance_expiration_070num);
                        txtExpirationDate.setVisibility(View.INVISIBLE);
                        txtLabelExpirationDate3.setVisibility(View.INVISIBLE);
                    }

                }

                try {
                    if (PRODUCT_ID.equals("9000") || PRODUCT_ID.startsWith("3")){
                        txtPlanExpirationDate.setVisibility(View.INVISIBLE);
                        txtLabelPlanExpirationDate3.setVisibility(View.INVISIBLE);
                        PLAN_DATE_EXPIRED="- - -";
                    }else if (PRODUCT_ID.equals("0000")){
                        txtPlanExpirationDate.setVisibility(View.INVISIBLE);
                        txtLabelPlanExpirationDate3.setVisibility(View.INVISIBLE);
                        PLAN_DATE_EXPIRED="- - -";
                    }else {
                        if (PLAN_EXPIRED!=null & PLAN_EXPIRED.length()>0 && Integer.parseInt(PLAN_EXPIRED)<=0){
                            PLAN_DATE_EXPIRED=getString(R.string.balance_expiration_plan);
                            resultBalance=getString(R.string.balance_required_charge);
                            txtPlanExpirationDate.setVisibility(View.INVISIBLE);
                            txtLabelPlanExpirationDate3.setVisibility(View.INVISIBLE);
                            progressBalance.setMax(10);
                            progressBalance.setProgress(0);
                        }else if (PLAN_EXPIRED.length()>0 && Integer.parseInt(PLAN_EXPIRED)>0){
                            PLAN_DATE_EXPIRED=PLAN_DATE_EXPIRED+",";
                            txtPlanExpirationDate.setVisibility(View.VISIBLE);
                            txtLabelPlanExpirationDate3.setVisibility(View.VISIBLE);
                        }
                    }

                }catch (NumberFormatException e){
                    PLAN_DATE_EXPIRED=getString(R.string.balance_expiration_plan);
                    resultBalance=getString(R.string.balance_required_charge);
                    txtPlanExpirationDate.setVisibility(View.INVISIBLE);
                    txtLabelPlanExpirationDate3.setVisibility(View.INVISIBLE);
                }

                txtMyNumber.setText(DID);
                txtLabelExpirationDate2.setText(DID_DATE_EXPIRED);
                txtExpirationDate.setText(DID_EXPIRED+getString(R.string.balance_day));
                txtMyPlan.setText(PLAN_NAME);
                txtLabelPlanExpirationDate2.setText(PLAN_DATE_EXPIRED);
                txtPlanExpirationDate.setText(PLAN_EXPIRED+getString(R.string.balance_day));
                txtMyBalance.setText(resultBalance);
                txtMyDistributor.setText(DISTRIBUTOR_ID);
            }
        }


        if (txtMyMaxBalance.getText().toString().equals("MAX")){
            txtMyMaxBalance.setVisibility(View.INVISIBLE);
        }else {
            txtMyMaxBalance.setVisibility(View.VISIBLE);
        }

        Log.i(TAG,"progressBalance.getProgress():"+progressBalance.getProgress());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_DID,txtMyNumber.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_DID_DATE_EXPIRED,txtLabelExpirationDate2.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_DID_EXPIRED,txtExpirationDate.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_PLAN_NAME,txtMyPlan.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_PLAN_DATE_EXPIRED,txtLabelPlanExpirationDate2.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_PLAN_EXPIRED,txtPlanExpirationDate.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_LABEL_BALANCE,txtLabelMyBalance.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_BALANCE,txtMyBalance.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_BALANCE_MAX,txtMyMaxBalance.getText().toString());
        mPrefs.setPreferenceStringValue(AppPrefs.HOME_DISTRIBUTOR_ID,txtMyDistributor.getText().toString());

        mPrefs.setPreferenceIntegerValue(AppPrefs.HOME_PROGRESS,progressBalance.getProgress());
        mPrefs.setPreferenceIntegerValue(AppPrefs.HOME_PROGRESS_MAX,progressBalance.getMax());

        mPrefs.setPreferenceStringValue(AppPrefs.HOME_PRODUCT_ID,PRODUCT_ID);
    }

    private void registerUpdateBalanceReceiver(){
        if(mBrUpdateBalance != null) return;

        this.mBrUpdateBalance=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(BC_HOME_UPDATE_BALANCE)){
                    setHomeData(intent);
                }
            }
        };

        IntentFilter theFilter = new IntentFilter();
        theFilter.addAction(BC_HOME_UPDATE_BALANCE);
        registerReceiver(this.mBrUpdateBalance,theFilter);
    }

    private void unregisterUpdateBalanceReceiver() {
        if(mBrUpdateBalance != null){
            unregisterReceiver(mBrUpdateBalance);
            mBrUpdateBalance = null;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String msg = ""; // BJH 2016.10.11
        int rs = 0; // BJH 2016.10.11
        switch (requestCode) {
            //BJH 2016.06.21 결제 후 서버 변경
            case SELECT_PAYMENT:
                if (resultCode == RESULT_OK) {
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    if (msg.equals(getResources().getString(R.string.pay_ok))) {
                        Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                        intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                        sendBroadcast(intent);
                        rs = ServicePrefs.login_check(this);// BJH
                        if (rs >= 0)
                            SipService.currentService.changeSipStack();
                    }
                } else {
                    msg = getResources().getString(R.string.setting_pay_cancel);
                }
                Alert(this, msg);
                break;
            // BJH 사용자별 설명서에서 결제가 되었을 경우와 서버 변경을 선택했을 경우
            case SELECT_USER_MSG:
                msg = "";
                if (resultCode == RESULT_OK) {
                    msg = data.getExtras().getString(PayClient.ACTIVITY_RESULT);
                    //BJH 2016.06.21 말톡 상품 구매 후 서버 재 할당
                    if (!msg.equals("popup") && !msg.equals("maaltalk_server")) { // 사용자별 설명서 팝업창 닫기가 아닐 경우
                        if (msg.equals(getResources().getString(R.string.pay_ok))) {
                            Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
                            intent.putExtra(DialMain.EXTRA_COMMAND, "CHECK_BALANCE");
                            sendBroadcast(intent);
                            rs = ServicePrefs.login_check(this);// BJH
                            if (rs >= 0)
                                SipService.currentService.changeSipStack();
                        }
                        Alert(this, msg);
                    }
                    if (msg.equals("maaltalk_server")) { // 사용자별 설명서에서 서버 이동을 눌렀을 경우
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                HomeActivity.this);
                        LayoutInflater inflater = HomeActivity.this.getLayoutInflater();
                        View view_svr = inflater.inflate(R.layout.svr_dialog, null);
                        builder.setView(view_svr);
                        builder.setPositiveButton(
                                getResources().getString(R.string.change),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {
                                        // TODO Auto-generated method stub
                                        ServicePrefs.DNSLookUp(mContext);
                                    }
                                });
                        builder.setNegativeButton(
                                getResources().getString(R.string.cancel),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // TODO Auto-generated method stub
                                        dialog.cancel();
                                    }
                                });
                        builder.setTitle(mContext.getResources().getString(
                                R.string.title_my_svr));
                        builder.show();
                    }
                }
                //DialMain.setServiceState(); //BJH 2017.01.03 인증 완료 후 유심 설정을 하기 위해서
                break;
            default:
                break;
        }
        return;
    }

    private static void Alert(Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                context);
        builder.setMessage(msg);
        builder.setPositiveButton(
                context.getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0,
                                        int arg1) {

                    }
                });
        builder.setTitle(context.getResources().getString(
                R.string.app_name));
        builder.show();
    }

    private void userMsg() { // BJH 2016.10.17
        if(mUserMsg)
            return;

        mUserMsg = true;
        String uid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
        if (uid != null && uid.length() > 0) {
            // BJH
            String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_USER_MSG);
            if (url == null || url.length() == 0)
                return;

            // CHECK SERIAL
            String serial = mPrefs
                    .getPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL);
            String old_serial = mPrefs
                    .getPreferenceStringValue(AppPrefs.URL_USER_MSG_OLD_SERIAL);
            String ex_old_serial = mPrefs
                    .getPreferenceStringValue(AppPrefs.URL_POPUP_OLD_SERIAL);
            String distributor_id = mPrefs
                    .getPreferenceStringValue(AppPrefs.DISTRIBUTOR_ID);

            if (old_serial == null)
                old_serial = "";
            if (ex_old_serial == null)
                ex_old_serial = "";

            if (ex_old_serial.length() == 0 && serial != null
                    && !serial.equalsIgnoreCase(old_serial)) {
                //if(distributor_id != null && distributor_id.length() > 0 && distributor_id.startsWith("usim"))
                //DialMain.saveUSIMJSON(mContext, mPrefs); // BJH 2017.01.03 서버에서 usim.json을  최초 설치 저장

                DialMain.skipClose = true;

                mPrefs.setPreferenceStringValue(
                        AppPrefs.URL_USER_MSG_OLD_SERIAL, serial);

                String urlString = String.format("%s?name=%s", url, uid);
                Intent intent = new Intent(HomeActivity.this, PayClient.class); // 사용자별 설명서에서 구매할 수 있도록 PayClient 변경
                intent.putExtra("url", urlString);
                // intent.putExtra("title", "말톡");
                intent.putExtra("title",
                        getResources().getString(R.string.title_user_msg));
                intent.putExtra("popup", true);
                // startActivity(intent);
                startActivityForResult(intent, SELECT_USER_MSG);
            }
        }
    }
}
