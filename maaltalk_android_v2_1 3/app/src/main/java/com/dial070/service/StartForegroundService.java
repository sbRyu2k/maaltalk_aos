package com.dial070.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;

import com.dial070.DialMain;
import com.dial070.maaltalk.R;
import com.dial070.ui.MarshmallowResponse;

public class StartForegroundService extends Service{
	private static final Class<?>[] mSetForegroundSignature = new Class[] {
			boolean.class};
	private static final Class<?>[] mStartForegroundSignature = new Class[] {
			int.class, Notification.class};
	private static final Class<?>[] mStopForegroundSignature = new Class[] {
			boolean.class};

	private NotificationManager mNM;
	private Method mSetForeground;
	private Method mStartForeground;
	private Method mStopForeground;
	private Object[] mSetForegroundArgs = new Object[1];
	private Object[] mStartForegroundArgs = new Object[2];
	private Object[] mStopForegroundArgs = new Object[1];

	private static final String OLD_CHANNEL_ID = "channel_startforeground";
	private static final String NEW_CHANNEL_ID = "channel_startforeground_maaltalk";
	private static final int mID = 1;

	//private final static int STARTFOREGROUND_RESULTCODE = 1;
	private static final String THIS_FILE = "START FOREGROUND";

	void invokeMethod(Method method, Object[] args) {
		try {
			method.invoke(this, args);
		} catch (InvocationTargetException e) {
			// Should not happen.
			Log.w(THIS_FILE, "Unable to invoke method", e);
		} catch (IllegalAccessException e) {
			// Should not happen.
			Log.w(THIS_FILE, "Unable to invoke method", e);
		}
	}

	/**
	 * This is a wrapper around the new startForeground method, using the older
	 * APIs if it is not available.
	 */
	void startForegroundCompat(int id, Notification notification) {
		// If we have the new startForeground API, then use it.
		if (mStartForeground != null) {
			mStartForegroundArgs[0] = Integer.valueOf(id);
			mStartForegroundArgs[1] = notification;
			invokeMethod(mStartForeground, mStartForegroundArgs);
			return;
		}

		// Fall back on the old API.
		mSetForegroundArgs[0] = Boolean.TRUE;
		invokeMethod(mSetForeground, mSetForegroundArgs);
		mNM.notify(id, notification);
	}

	/**
	 * This is a wrapper around the new stopForeground method, using the older
	 * APIs if it is not available.
	 */
	void stopForegroundCompat(int id) {
		// If we have the new stopForeground API, then use it.
		if (mStopForeground != null) {
			mStopForegroundArgs[0] = Boolean.TRUE;
			invokeMethod(mStopForeground, mStopForegroundArgs);
			return;
		}

		// Fall back on the old API.  Note to cancel BEFORE changing the
		// foreground state, since we could be killed at that point.
		mNM.cancel(id);
		mSetForegroundArgs[0] = Boolean.FALSE;
		invokeMethod(mSetForeground, mSetForegroundArgs);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		try {
			mStartForeground = getClass().getMethod("startForeground",
					mStartForegroundSignature);
			mStopForeground = getClass().getMethod("stopForeground",
					mStopForegroundSignature);
			return;
		} catch (NoSuchMethodException e) {
			// Running on an older platform.
			mStartForeground = mStopForeground = null;
		}
		try {
			mSetForeground = getClass().getMethod("setForeground",
					mSetForegroundSignature);
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException(
					"OS doesn't have Service.startForeground OR Service.setForeground!");
		}

		//startForegroundCompat(1, new Notification());
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.i(THIS_FILE, "onStartCommand");

		//startForeground(STARTFOREGROUND_RESULTCODE, new Notification());

		/*NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.noti)
						.setContentTitle(title)
						.setContentText(msg);

		intent = new Intent(StartForegroundService.this,DialMain.class);//BJH Splash -> Main
		intent.putExtra(DialMain.START_FOREGROUND, startId);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		//stackBuilder.addParentStack(DialMain.class);
		stackBuilder.addNextIntent(intent);

		PendingIntent resultPandingIntent =
				stackBuilder.getPendingIntent(
						0,
						PendingIntent.FLAG_UPDATE_CURRENT
				);

		mBuilder.setContentIntent(resultPandingIntent);*/

		if (Build.VERSION.SDK_INT >= 26) {

			if(mNM.getNotificationChannel(OLD_CHANNEL_ID) != null){
				mNM.deleteNotificationChannel(OLD_CHANNEL_ID);
			}

			int importance = NotificationManager.IMPORTANCE_MIN;
			NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, this.getString(R.string.title_foreground), importance);
			//channel.setShowBadge(false);
			mNM.createNotificationChannel(channel);
		}

		String msg = getResources().getString(R.string.noti_msg_foreground);
		String title = getResources().getString(R.string.app_name);

		Notification notification = null;
		intent = new Intent(StartForegroundService.this,DialMain.class);//BJH Splash -> Main
		intent.putExtra(DialMain.START_FOREGROUND, 0);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		//stackBuilder.addParentStack(DialMain.class);
		stackBuilder.addNextIntent(intent);

		PendingIntent resultPandingIntent =
				stackBuilder.getPendingIntent(
						0,
						PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE
				);

		notification = MarshmallowResponse.createSFSNotification(this, resultPandingIntent, NEW_CHANNEL_ID, title, msg, R.drawable.noti);
		notification.number=0;

		this.startForeground(mID, notification);

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		//stopForegroundCompat(mID);
		stopForeground(true);
		super.onDestroy();
	}
}
