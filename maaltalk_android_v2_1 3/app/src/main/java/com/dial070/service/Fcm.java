package com.dial070.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.bridge.DialBridgeNew;
import com.dial070.bridge.utils.BridgeMediaManager;
import com.dial070.bridge.utils.BridgeRinger;
import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.SipNotifications;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.Compatibility;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.ui.InCall;
import com.dial070.ui.MarshmallowResponse;
import com.dial070.ui.SmsMsgContsActivity;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.InstanceIdResult;

//import org.apache.http.entity.StringEntity;
//import android.os.Handler;
//import android.os.Message;

public class Fcm {
	private static final String THIS_FILE = "Dial070-FCM";
	public static NotificationManager notificationManager = null;
	public static String mRegistrationID = null;
	public static boolean mRegistrationOK = false; // 토큰 받아오기 절차가 완료 되었는가.

	public static final int FCM_NOTIF_ID = 1001;
	public static final int FCM_NOTIF_NOTICE = FCM_NOTIF_ID + 1;
	public static final int FCM_NOTIF_CALL = FCM_NOTIF_ID + 2;
	public static final int FCM_NOTIF_VMS = FCM_NOTIF_ID + 3;
	public static final int FCM_NOTIF_CB = FCM_NOTIF_ID + 4;
	public static final int FCM_NOTIF_MSG = FCM_NOTIF_ID + 5;
	public static final int FCM_NOTIF_MTO = FCM_NOTIF_ID + 6;
	public static final int FCM_NOTIF_BRIDGE = FCM_NOTIF_ID + 7;

	public static String mode="";

	private static String BRIDGE_TS="";
	private static String bridge_cid="";

    //public static Ringtone defaultRingtone;
	public static BridgeRinger ringer;
	private static Handler autoRingStopHandler = null;
	private static Runnable autoRingStopRunnable = null;


	public static BridgeMediaManager mediaManager;

	public static int BRIDGE_FINAL_STATUS = 0;	//0이면 bye
	// private static final int DIAL070_FCM_FAIL = 1;
	// private static Context context;

	/*
	 * private static Handler handler = new Handler() { public void
	 * handleMessage(Message msg) { switch (msg.what) { case DIAL070_FCM_FAIL: {
	 * if (Fcm.context != null) { String e =
	 * getResources().getString(R.string.Fcm.reg_error); String s = e + "\n(" +
	 * Fcm.GetPlayServicesVersion(Fcm.context) + ")";
	 *
	 * Toast toast = Toast.makeText(Fcm.context, s, Toast.LENGTH_LONG);
	 * toast.show();
	 *
	 * // AlertDialog.Builder msgDialog = new //
	 * AlertDialog.Builder(Fcm.context); //
	 * msgDialog.setTitle(getResources().getString(R.string.app_name));
	 * msgDialog.setMessage(s); // msgDialog.setIcon(R.drawable.ic_launcher); //
	 * msgDialog.setNegativeButton(getResources().getString(R.string.close),
	 * null); // msgDialog.show();
	 *
	 * } } break; } } };
	 */

	public static void init() {
		// for memory load.
		DebugLog.d("PushyMQTT init()");
		PushyMQTT.init();
	}

	public static void Startup(final Context context) {
		// Intent intent = new Intent(context, BootReceiver.class);
		Intent intent = new Intent(BootReceiver.ACTION_START);
		context.sendBroadcast(intent);
	}

	public static int GetPlayServicesVersion(final Context context) {
		// check version
		int v = 0;
		try {
			v = context.getPackageManager().getPackageInfo(
					"com.google.android.gms", 0).versionCode;
			Log.d(THIS_FILE, "GooglePlayServices Version : " + v);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return v;
	}

	public static boolean Register(final Context context) {
//		Log.d(THIS_FILE,"Fcm Register");
		DebugLog.d("FCM Register --> "+context.getClass().getSimpleName());
		// Fcm.context = context;
		//Startup(context);

		if (!mRegistrationOK || !PushyMQTT.mRegistrationOK) {
			//registerInBackground(context);
//			Log.d(THIS_FILE,"Fcm !mRegistrationOK || !PushyMQTT.mRegistrationOK is true");
			DebugLog.d("FCM !mRegistrationOK || !PushyMQTT.mRegistrationOK is true");
			FirebaseInstanceId.getInstance().getInstanceId()
					.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
						@Override
						public void onComplete(@NonNull Task<InstanceIdResult> task) {
							AppPrefs mPrefs = new AppPrefs(context);
							if (!task.isSuccessful()) {
//								Log.v(THIS_FILE, "getInstanceId failed", task.getException());
								DebugLog.d("FCM getInstanceId failed --> "+task.getException().getMessage());
								mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, "");
								return;
							}else{
								// Get new Instance ID token
								mRegistrationID = task.getResult().getToken();
//								Log.v(THIS_FILE, "mRegistrationID:"+mRegistrationID);
								DebugLog.d("FCM registrationId --> "+mRegistrationID);
								mRegistrationOK = true;
//								Log.d(THIS_FILE,"Fcm mRegistrationOK change true");
								DebugLog.d("FCM registrationOK change true");


								if (mRegistrationID != null && mRegistrationID.length() > 0) {
//									Log.d(THIS_FILE, "REG-ID : " + mRegistrationID);
									DebugLog.d("FCM REG-ID --> "+mRegistrationID);
									mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, mRegistrationID);

									/*String userid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
									if (ServicePrefs.mLogin && userid != null && userid.length() > 0)
									{
										String id = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);
										mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, mRegistrationID);
										//Fcm.updateToken(context,"FCM");
									}*/
								} else {
//									Log.e(THIS_FILE, "REG-ID : ERROR");
									DebugLog.d("FCM REG-ID --> ERROR");
									mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, "");
								}
							}
						}
					});
		}else{
//			Log.d(THIS_FILE,"Fcm !mRegistrationOK || !PushyMQTT.mRegistrationOK is false");
			DebugLog.d("FCM !mRegistrationOK || !PushyMQTT.mRegistrationOK is false");
		}

		return true;
	}

	public static void Reset(final Context context) {
		DebugLog.d("Reset caller --> "+context.getClass().getSimpleName());

		mRegistrationID = null;
		mRegistrationOK = false;

		AppPrefs mPrefs = new AppPrefs(context);
		mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, "");
		mPrefs.setPreferenceStringValue(AppPrefs.GCM_VER, "");
	}

	private static String FCM_SENDER_ID  = "480394280680";

	/*private static void registerInBackground(final Context context) {

		Thread t = new Thread() {
			public void run() {

				// REG GCM
				try {
					doRegister(context);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// REG PUSHY // BJH 2017.03.02
				*//*try {
					PushyMQTT.Register(context);
				} catch (Exception e) {
					e.printStackTrace();
				}*//*

				// UPDATE
				try {
					if(PushyMQTT.mRegistrationID == null)
						Thread.sleep(4000); // BJH 2017.03.02 Pushy Gradle 변경 후 Token 값 가져오는 시간이 단말기에 따라 다르기 때문에
					updateToken(context);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
		if (t != null) {
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}

	private static boolean doRegister(final Context context) {
		return doRegister2(context);
	}

	private static boolean doRegister2(final Context context) {

		String refreshedToken = FirebaseInstanceId.getInstance().getToken();
		Log.d("MyFirebaseMsgService", "Refreshed token: " + refreshedToken);

		AppPrefs mPrefs = new AppPrefs(context);

		if (refreshedToken != null) {
			mRegistrationID = refreshedToken;
			mRegistrationOK = true;
			Log.d(THIS_FILE, "REG-ID CACHE : " + mRegistrationID);
			return true;
		}

		return false;
	}*/

	private static boolean send_token = false; // 토큰 전송 절차가 완료됨.
	private static boolean sending_token = false; // 토큰 전송하는중..
	private static boolean send_pending = false; // 로그인이 아직 안되어 있는 경우.

	public static boolean needSendToken() {
		// Fcm.발급절차가 완료되었는지 검사.
//		Log.d(THIS_FILE,"needSendToken 1");
		DebugLog.d("Check.1 FCM 발급절차가 완료되었는지 검사");
		if (!Fcm.mRegistrationOK) {
			return false;
		}
//		Log.d(THIS_FILE,"needSendToken 2");
		DebugLog.d("Check.2 PUSHY 발급절차가 완료되었는지 검사");
		// PUSHY 발급절차가 완료되었는지 검사.
		if (!PushyMQTT.mRegistrationOK) {
			return false;
		}
//		Log.d(THIS_FILE,"needSendToken 3");
		DebugLog.d("Check.3 전송중일경우 처리(보내지 않음)");
		// 전송중이면 보내지 않는다.
		if (sending_token)
			return false;
//		Log.d(THIS_FILE,"needSendToken 4");
		DebugLog.d("Check.4 서버로 전송하려하지만 아직 로그인이 안된 경우 처리");
		// TOKEN을 서버로 전송할려고 했는데 아직 로그인이 안된 경우.
		if (send_pending)
			return true;
//		Log.d(THIS_FILE,"needSendToken 5");/
		DebugLog.d("Check.5 전송이 아직 안된 경우 처리");
		// 전송이 아직 완료가 안된 경우?
		if (send_token)
			return true;
		return false;
	}

	private static final String OLD_CHANNEL_ID = "channel_01";
	public static final String NEW_CHANNEL_ID = "maaltalk_channel_01";
	public static final String CALL_CHANNEL_ID = "maaltalk_channel_call";
	public static final String BRIDGE_CHANNEL_ID = "maaltalk_channel_bridge";

	public static boolean updateToken(final Context context,final String mode) {
//		Log.d(THIS_FILE,"updateToken "+ mode);
		DebugLog.d("updateToken mode --> "+mode+" | context --> "+context.getClass().getSimpleName());
		if (!ServicePrefs.mLogin) {
			send_pending = true;
//			Log.d(THIS_FILE," ServicePrefs.mLogin is false");
			DebugLog.d("ServicePrefs.mLogin is false");
			return false;
		}
		send_pending = false;

//		Log.d(THIS_FILE,"updateToken pass");
		DebugLog.d("updateToken pass");

		// if (mRegistrationID == null || mRegistrationID.length() == 0) return
		// false;

		AppPrefs mPrefs = new AppPrefs(context);
//		Log.v(THIS_FILE, "SEND FCM   TOKEN : " + mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID));
//		Log.v(THIS_FILE, "SEND PUSHY TOKEN : " + PushyMQTT.mRegistrationID);

		DebugLog.d("SEND FCM   TOKEN --> "+mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID));
		DebugLog.d("SEND PUSHY TOKEN --> "+PushyMQTT.mRegistrationID);

		sending_token = true;
		String rs = postFCM(context);
		sending_token = false;

		if (rs == null)
			return false;

//		Log.v(THIS_FILE, "SEND TOKEN  RESULT : " + rs);
		DebugLog.d("SEND TOKEN RESULT --> "+rs);

		if (rs.length() > 0) {
			// CHECK RESULT
			if (rs.contains("\"RES_CODE\":\"0\""))
				send_token = true;
		}

		return true;
	}

	public static void handleReceive(Context context, Intent intent) {
//		Log.d(THIS_FILE, "handleReceive");
		DebugLog.d("caller context --> "+context.getClass().getSimpleName());
		DebugLog.d("action --> "+intent.getAction());

		Bundle extra = intent.getExtras();
		if (extra == null)
			return;
		Log.d(THIS_FILE, extra + " ");
		String msg = extra.getString("msg");
		if (msg == null)
			return;

		DebugLog.d("message --> "+msg);

		if (Build.VERSION.SDK_INT >= 26) {
			notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			if(notificationManager.getNotificationChannel(OLD_CHANNEL_ID) != null){
				notificationManager.deleteNotificationChannel(OLD_CHANNEL_ID);
			}

			NotificationChannel notificationChannel = notificationManager.getNotificationChannel(NEW_CHANNEL_ID);
			if (notificationChannel == null) {
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}

		}else{
			notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		}

		String push_type = extra.getString("from");

//		Log.d(THIS_FILE, "Fcm MSG: " + msg);

		DebugLog.d("FCM MSG --> "+msg);

		JSONObject js = null;
		String Alert = null;

		JSONObject jsAPS = null;

		try {
			js = new JSONObject(msg);

			jsAPS = js.getJSONObject("aps");
			if (jsAPS == null) {
				return;
			}

			Alert = jsAPS.getString("alert");
			if (Alert != null) {
				// Toast toast = Toast.makeText(context,
				// context.getString(R.string.app_name) + "\n\n" + Alert,
				// Toast.LENGTH_SHORT);
				// toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 150);
				// toast.show();
			} else
				return;
		} catch (Exception e) {
			return;
		}

		// /////////////////////////////////////////////////////////////////
		// MESSAGE (SMS, LMS)
		try {
			JSONObject jsMsg = js.getJSONObject("msg");
			if (jsMsg != null) {
				DebugLog.d("MESSAGE(SMS, LMS) --> processMsg");
				processMsg(context, jsAPS, jsMsg ,push_type);
				return;
			}
		} catch (Exception e) {
			//
		}

		// /////////////////////////////////////////////////////////////////
		// CALL
		try {
			JSONObject jsCall = js.getJSONObject("call");
			if (jsCall != null) {
				DebugLog.d("MESSAGE CALL --> processCall() | processCall_FCM()");

				TelephonyManager telephonyManager = (TelephonyManager) context
						.getSystemService(Context.TELEPHONY_SERVICE);
				int state = telephonyManager.getCallState();
				if (state != TelephonyManager.CALL_STATE_IDLE) {
					DebugLog.d("MESSAGE CALL --> processCall_FCM");
					processCall_FCM(context, jsAPS, jsCall);
				} else {
					DebugLog.d("MESSAGE CALL --> processCall");
					processCall(context, jsAPS, jsCall, push_type); //BJH 2016.11.11
				}
				return;
			}
		} catch (Exception e) {
			//
		}

		// BRIDGE
		try {
			JSONObject jsCall = js.getJSONObject("bridge");
			if (jsCall != null) {
				DebugLog.d("MESSAGE BRIDGE --> processBridge()");

				processBridge(context, jsAPS, jsCall, push_type); // BJH 2016.11.11
			}
		} catch (Exception e) {
			//
		}

		// /////////////////////////////////////////////////////////////////
		// NOTICE
		try {

			JSONObject jsNotice = js.getJSONObject("notice");
			if (jsNotice != null) {
				DebugLog.d("MESSAGE NOTICE --> processNotice()");

				processNotice(context, jsAPS, jsNotice, push_type);
				return;
			}
		} catch (Exception e) {
			//
		}

		// /////////////////////////////////////////////////////////////////
		// VMS
		try {
			JSONObject jsVMS = js.getJSONObject("vms");
			if (jsVMS != null) {
				DebugLog.d("MESSAGE VMS --> processVMSV2()");

				//processVMS(context, jsAPS, jsVMS);
				processVMSV2(context, jsAPS, jsVMS);
				return;
			}
		} catch (Exception e) {
			//
		}

		// BJH 2016.09.22 MTO
		try {
			JSONObject jsMTO = js.getJSONObject("mto");
			if (jsMTO != null) {
				DebugLog.d("MESSAGE MTO --> processMTO()");

				processMTO(context, jsAPS, jsMTO, push_type); // BJH 2016.11.11
				return;
			}
		} catch (Exception e) {
			//
		}

		// CALLBACK
		try {
			JSONObject jsCallBack = js.getJSONObject("cb");
			if (jsCallBack != null) {
				DebugLog.d("MESSAGE CB(callback) --> processCallback()");

				processCallBack(context, jsAPS, jsCallBack);
				return;
			}
		} catch (Exception e) {
			//
		}

		//BJH 2016.06.29 Maaltalk 관리자페이지 푸시를 받고 SipService가 종료되지 않는 경우에만 레지스터하기
		try {
			JSONObject jsMaster = js.getJSONObject("master");
			if (jsMaster != null && SipService.currentService != null) {
				DebugLog.d("MESSAGE master --> changeSipStack()");

				int rs = ServicePrefs.login_check(context);// BJH
				if (rs >= 0)
					SipService.currentService.changeSipStack();
				return;
			}
		} catch (Exception e) {
			//
		}

	}

	private static void processNotice(final Context context, JSONObject aps,
									  JSONObject notice ,String push_type) {
//		Log.d(THIS_FILE, "NOTICE");
		DebugLog.d("NOTICE --> "+context.getClass().getSimpleName()+" | notice --> "+notice.toString()+" | pushType --> "+push_type);

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}



		String cid = null;
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // BJH 2016.11.11
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		long receive_time = System.currentTimeMillis();
		try {
			cid = notice.getString("name");
			try {
				DBManager database = new DBManager(context);
				String number;
				try {
					database.open();
					AppPrefs prefs = new AppPrefs(context);
					String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
					TelephonyManager telManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE);
					//shlee request

					if(Build.VERSION.SDK_INT<Build.VERSION_CODES.P) {
						if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
							number= "not Found";
						}
						else {
							number = telManager.getLine1Number();
						}

					} else {
						if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
							number= "not Found";
						}
						else {
							number = telManager.getLine1Number();
						}
					}

					PushRecordData push_record_data = new PushRecordData(0, receive_time, 0, push_type, "NOTICE", cid, uid, number, "");
					long result=database.insertPushRecord(push_record_data);
//					Log.d(THIS_FILE,"MSG insertPushRecord result:"+result);
					DebugLog.d("MSG insertPushRecord Result --> "+result);
					database.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}



		String name = context.getString(R.string.pns_notice_title);
		String url = "http://" + ServicePrefs.getDomain() + "/";
		String img_url = "";
		String fb_url = "";
		try {
			if (notice.has("name"))
				name = notice.getString("name");
			if (notice.has("url"))
				url = notice.getString("url");
			if (notice.has("img"))
				img_url = notice.getString("img");
			if (notice.has("fb"))
				fb_url = notice.getString("fb");
		} catch (Exception e) {
			//
		}

		sendFeedback(context, fb_url);

		// App이 실행되어 있으면 바로 전달.
		//if (DialMain.currentContext != null && DialMain.currentState != 0) {
		/*if (DialMain.bMaaltalkRunned && DialMain.currentState != 0) {
			Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
			intent.putExtra(DialMain.EXTRA_COMMAND, "NOTICE");
			intent.putExtra(DialMain.EXTRA_NOTICE_URL, url);
			intent.putExtra(DialMain.EXTRA_NOTICE_IMG_URL, img_url);
			intent.putExtra(DialMain.EXTRA_NOTICE_FB_URL, fb_url);
			intent.putExtra(DialMain.EXTRA_NOTICE_TITLE, name);
			context.sendBroadcast(intent);
			sendFeedback(context, fb_url);
			return;
		} else if (InCall.bMaaltalkRunned  && InCall.currentState != 0) {
			Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
			intent.putExtra(DialMain.EXTRA_COMMAND, "NOTICE");
			intent.putExtra(DialMain.EXTRA_NOTICE_URL, url);
			intent.putExtra(DialMain.EXTRA_NOTICE_IMG_URL, img_url);
			intent.putExtra(DialMain.EXTRA_NOTICE_FB_URL, fb_url);
			intent.putExtra(DialMain.EXTRA_NOTICE_TITLE, name);
			context.sendBroadcast(intent);
			sendFeedback(context, fb_url);
			return;
		} else if (WebClient.bMaaltalkRunned
				&& WebClient.currentState != 0) {
			Log.d(THIS_FILE, "showNotice : " + url);
			Intent intent = new Intent(context, WebClient.class);
			intent.putExtra("url", url);
			intent.putExtra("img_url", img_url);
			intent.putExtra("fb_url", fb_url);
			intent.putExtra("title", name);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_SINGLE_TOP *//*
		 * | Intent.
		 * FLAG_ACTIVITY_NO_ANIMATION
		 *//*);
			context.startActivity(intent);
			sendFeedback(context, fb_url);
			return;
		}*/

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		// Intent notificationIntent = new Intent(context, WebClient.class);
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
		notificationIntent.putExtra("id", FCM_NOTIF_NOTICE);

		notificationIntent.putExtra(DialMain.EXTRA_NOTICE_URL, url);
		notificationIntent.putExtra(DialMain.EXTRA_NOTICE_IMG_URL, img_url);
		notificationIntent.putExtra(DialMain.EXTRA_NOTICE_FB_URL, fb_url);
		notificationIntent.putExtra(DialMain.EXTRA_NOTICE_TITLE, name);

		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent content = PendingIntent.getActivity(context,
				(int) (Math.random() * 100), notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		Notification notification = null;

		// CHECK IMAGE
		Bitmap remote_picture = null;
		if (img_url.length() > 0) {
			try {
				remote_picture = BitmapFactory
						.decodeStream((InputStream) new URL(img_url)
								.getContent());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (remote_picture != null && Compatibility.isCompatible(14)) {
			NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
			notiStyle.setBigContentTitle(name);
			notiStyle.setSummaryText(alert);

			if (remote_picture != null)
				notiStyle.bigPicture(remote_picture);

			NotificationCompat.Builder b;
			if (Build.VERSION.SDK_INT >= 26) {
				b = new NotificationCompat.Builder(
						context, NEW_CHANNEL_ID).setSmallIcon(R.drawable.noti_notice)
						.setAutoCancel(true).setContentIntent(content)
						.setContentTitle(name).setContentText(alert)
						.setTicker(alert).setStyle(notiStyle);
			}else{
				b = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.noti_notice)
						.setAutoCancel(true).setContentIntent(content)
						.setContentTitle(name).setContentText(alert)
						.setTicker(alert).setStyle(notiStyle);
			}

			// 4.X 는 접혀 있을 수 있어서..
			if (!Compatibility.isCompatible(21))
				b.setPriority(NotificationCompat.PRIORITY_MAX);

			notification = b.build();
		} else {
			notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, name, alert, R.drawable.noti_notice);
		}

		DebugLog.d("Ringtone/vibrate inFCM Notification Process");

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notification.sound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		notification.vibrate = new long[] { 1000, 1000, 500, 500, 200, 200,
				200, 200, 200, 200 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_NOTICE); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
		notificationManager.notify(FCM_NOTIF_NOTICE, notification);

		// 노티 영역이 아니라 백그라운드에 있거나 앱을 실행했다면?

		AppPrefs mPrefs = new AppPrefs(context);
		mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_URL, url);
		mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_FB_URL, fb_url);
		mPrefs.setPreferenceStringValue(AppPrefs.NOTICE_TITLE, name);

		// SET BADGE NUMBER;
		// ServicePrefs.setBadgeNumber(ServicePrefs.BADGE_HOME, "N");
		// ServicePrefs.fireBadgeEvent(context);
	}

	public static void cancelNotice(final Context context) {
		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		notificationManager.cancel(FCM_NOTIF_NOTICE); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
	}

	public static void cancelMTO(final Context context) { // BJH 2017.05.15
		DebugLog.d("canceMTO caller --> "+context.getClass().getSimpleName());
		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		notificationManager.cancel(FCM_NOTIF_MTO); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
	}

	private static void processMsg(final Context context, JSONObject aps,
								   JSONObject notice, String push_type) {
//		Log.d(THIS_FILE, "MSG");

		DebugLog.d("MSG --> "+notice.toString()+" | caller --> "+context.getClass().getSimpleName()+" | pushType --> "+push_type);

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}



		String cid = null;
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // BJH 2016.11.11
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		long receive_time = System.currentTimeMillis();
		try {
			cid = notice.getString("name");
			try {
				DBManager database = new DBManager(context);
				String number;
				try {
					database.open();
					AppPrefs prefs = new AppPrefs(context);
					String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
					TelephonyManager telManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE);
					//shlee request
					if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
						if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
							number= "not Found";
						}
						else {
							number = telManager.getLine1Number();
						}
					} else {
						if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
							number= "not Found";
						}
						else {
							number = telManager.getLine1Number();
						}
					}

					PushRecordData push_record_data = new PushRecordData(0, receive_time, 0, push_type, "MSG", cid, uid, number, "");
					long result=database.insertPushRecord(push_record_data);
//					Log.d(THIS_FILE,"MSG insertPushRecord result:"+result);
					DebugLog.d("MSG insertPushRecord result --> "+result);
					database.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}






		String name = context.getString(R.string.pns_msg_title);
		// String url = "http://" + ServicePrefs.getDomain() + "/";
		String img_url = "";
		String fb_url = "";
		try {
			if (notice.has("name"))
				name = notice.getString("name");
			// if (notice.has("url"))
			// url = notice.getString("url");
			if (notice.has("img"))
				img_url = notice.getString("img");
			if (notice.has("fb"))
				fb_url = notice.getString("fb");
		} catch (Exception e) {
			//
		}

		// App이 실행되어 있으면 바로 전달.
		/*
		 * if (DialMain.currentContext != null && DialMain.currentState != 0) {
		 * Intent intent = new Intent(DialMain.ACTION_DIAL_COMMAND);
		 * intent.putExtra(DialMain.EXTRA_COMMAND, "MESSAGE");
		 * intent.putExtra(DialMain.EXTRA_MESSAGE_TITLE, alert);
		 * intent.putExtra(DialMain.EXTRA_MESSAGE_NUMBER, name);
		 *
		 * context.sendBroadcast(intent); sendFeedback(context, fb_url); return;
		 * }
		 */

		// MESSAGE RELOAD REQUEST
		/*Intent intent = new Intent(context, MessageService.class);
		intent.setAction(MessageService.ACTION_START);
		{
			if (Build.VERSION.SDK_INT < 26) { // BJH 2018.03.13 Oreo 부턴 백그라운드 서비스 이슈가 있음
				context.startService(intent);
			} else {
				context.startForegroundService(intent);
			}
		}*/

		sendFeedback(context, fb_url);

		if (SmsMsgContsActivity.currentContext != null) {
			String number = SmsMsgContsActivity.currentContext.getNumber();
			if (number != null && number.length() > 0
					&& number.equalsIgnoreCase(name)) {
				// try {
				// Uri notification =
				// RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				// Ringtone r =
				// RingtoneManager.getRingtone(context.getApplicationContext(),
				// notification);
				// r.play();
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				return;
			}
		}

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		// Intent notificationIntent = new Intent(context, WebClient.class);
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, false);
		notificationIntent.putExtra("id", FCM_NOTIF_MSG);

		notificationIntent.putExtra(DialMain.EXTRA_MESSAGE_TITLE, alert);
		notificationIntent.putExtra(DialMain.EXTRA_MESSAGE_NUMBER, name);

		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent content = PendingIntent.getActivity(context,
				(int) (Math.random() * 100), notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		Notification notification = null;

		// CHECK IMAGE
		Bitmap remote_picture = null;
		if (img_url.length() > 0) {
			try {
				remote_picture = BitmapFactory
						.decodeStream((InputStream) new URL(img_url)
								.getContent());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//BJH 2016.09.22
		String title = "";
		if (name != null && name.length() > 0) {
			title = ContactHelper.getContactsNameByPhoneNumber(context, name);
			Log.d(THIS_FILE,"title:"+title);
			if (title == null)
				title = name;
		}

		if (remote_picture != null && Compatibility.isCompatible(14)) {
			NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
			notiStyle.setBigContentTitle(name);
			notiStyle.setSummaryText(alert);

			if (remote_picture != null)
				notiStyle.bigPicture(remote_picture);

			NotificationCompat.Builder b;
			if (Build.VERSION.SDK_INT >= 26) {
				b = new NotificationCompat.Builder(
						context, NEW_CHANNEL_ID).setSmallIcon(R.drawable.noti_notice)
						.setAutoCancel(true).setContentIntent(content)
						.setContentTitle(title).setContentText(alert)
						.setTicker(alert).setStyle(notiStyle);
			}else{
				b = new NotificationCompat.Builder(
						context).setSmallIcon(R.drawable.noti_notice)
						.setAutoCancel(true).setContentIntent(content)
						.setContentTitle(title).setContentText(alert)
						.setTicker(alert).setStyle(notiStyle);
			}

			// 4.X 는 접혀 있을 수 있어서..
			if (!Compatibility.isCompatible(21))
				b.setPriority(NotificationCompat.PRIORITY_MAX);

			notification = b.build();
		} else {
			notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, title, alert, R.drawable.noti_notice);
		}

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notification.sound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		// notification.vibrate = new long[] { 1000, 1000, 500, 500, 200, 200,
		// 200, 200, 200, 200 };
		notification.vibrate = new long[] { 1000, 1000, 500 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_MSG); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
		notificationManager.notify(FCM_NOTIF_MSG, notification);

		// 노티 영역이 아니라 백그라운드에 있거나 앱을 실행했다면?
		/*
		 * AppPrefs mPrefs = new AppPrefs(context);
		 * mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_NUMBER, name);
		 * mPrefs.setPreferenceStringValue(AppPrefs.MESSAGE_TITLE, alert);
		 */

		// SET BADGE NUMBER;
		// ServicePrefs.setBadgeNumber(ServicePrefs.BADGE_HOME, "N");
		// ServicePrefs.fireBadgeEvent(context);
	}

	public static void cancelMsg(final Context context) {
		DebugLog.d("cancelMsg caller --> "+context.getClass().getSimpleName());

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		notificationManager.cancel(FCM_NOTIF_MSG); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
	}

	private static void processCall_FCM(final Context context, JSONObject aps,
										JSONObject call) {
//		Log.d(THIS_FILE, "processCall_FCM");

		DebugLog.d("processCall_FCM caller --> "+context.getClass().getSimpleName()+" | call msg --> "+call.toString());

		String title = context.getString(R.string.pns_call_title);
		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		String cid = null;
		try {
			cid = call.getString("cid");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String name = null;
		if (cid != null && cid.length() > 0) {
			name = ContactHelper.getContactsNameByPhoneNumber(context, cid);
			if (name != null && name.length() > 0) {
				title = title + " : " + name;
				alert = name + " (" + cid + ")";
			}
		}

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		Notification notification = null;

		notification.when = System.currentTimeMillis();

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
		if (cid != null && cid.length() > 0) {
			notificationIntent.putExtra(DialMain.EXTRA_GCM_CID, cid);
		}
		notificationIntent.putExtra("id", FCM_NOTIF_CALL);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent content = PendingIntent.getActivity(context,
				(int) (Math.random() * 100), notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, title, alert, R.drawable.noti_inbound);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_CALL);
		notificationManager.notify(FCM_NOTIF_CALL, notification);

		Toast toast = Toast.makeText(context, alert, Toast.LENGTH_SHORT);
		// toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 150);
		toast.show();

		/*
		 * AlertDialog msgDialog = new AlertDialog.Builder(context).create();
		 * msgDialog.setTitle(context.getString(R.string.pns_call_title));
		 * msgDialog.setMessage(alert); msgDialog.setIcon(R.drawable.noti_call);
		 * msgDialog.setButton(context.getResources().getString(R.string.deny),
		 * new DialogInterface.OnClickListener() {
		 *
		 * @Override public void onClick(DialogInterface dialog, int which) { //
		 * NOTI도 삭제. notificationManager.cancel(FCM_NOTIF_CALL); } });
		 * msgDialog.setButton2(context.getResources().getString(R.string.accept
		 * ), new DialogInterface.OnClickListener() {
		 *
		 * @Override public void onClick(DialogInterface dialog, int which) { //
		 * 실행. notificationManager.cancel(FCM_NOTIF_CALL);
		 *
		 * Intent intent = new Intent(context, DialMain.class);
		 * context.startActivity(intent);
		 *
		 * } }); msgDialog.show();
		 */
	}

	/**
	 * TEST
	 *
	 */
	public static void stopMediaService() {
		DebugLog.d("yield media service from bridgemediamanager to mediamanager");
		if(mediaManager==null) {
			return;
		}
		mediaManager.stopAnnoucing();
		mediaManager.stopService();

	}

	private static void processCall(final Context context, JSONObject aps,
									JSONObject call, String push_type) {

		Log.d(THIS_FILE, "processCall " + SipService.currentService + "");
		DebugLog.d("processCall --> "+SipService.currentService);
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("aps --> "+aps);
		DebugLog.d("call --> "+call);
		DebugLog.d("push_type --> "+push_type);

//		Log.d(THIS_FILE, "processCall " + SipService.currentService + "");


		String title = context.getString(R.string.pns_call_title);
		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		DebugLog.d("==== START CALL STATE SAVE PROCESS...");

		String cid = null;
		String status = "";
		String pushValue = "";
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // BJH 2016.11.11
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		long push_time = System.currentTimeMillis();
		long receive_time = System.currentTimeMillis();
		try {
			if (call.has("status")) {
				status = call.optString("status");
				DebugLog.d("status --> "+status);
			}

			if (status.equals("BYE")){
				pushValue="CALL_BYE";
			}else {
				pushValue="CALL";
				App.isCallTakeClicked=false;
				//shlee check if the call is running return
				if (SipService.currentService != null ) {
//					Log.e(THIS_FILE, "SERVICE RUNNING !!");
					DebugLog.d("SERVICE RUNNING !!");
					return;
				}

				if (InCall.currentContext != null && InCall.currentState != 0) {
//					Log.e(THIS_FILE, "SKIP FCM CALL : BUSY ");
					DebugLog.d("SKIP FCM CALL: BUSY");
					return;
				}
			}

			cid = call.getString("cid");
			if (call.has("current_time")) {
				String push_date = call.getString("current_time");
				if (push_date != null && push_date.length() > 0)
				{
					try {
						Date d = mFormatter.parse(push_date);
						push_time = d.getTime();
						DBManager database = new DBManager(context);
						String number;
						try {
							database.open();
							AppPrefs prefs = new AppPrefs(context);
							String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
							TelephonyManager telManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE);
							//shlee request
							if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}
							} else {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}
							}

							PushRecordData push_record_data = new PushRecordData(push_time, receive_time, 0, push_type, pushValue, cid, uid, number, "");
							database.insertPushRecord(push_record_data);
							database.close();
						}
						catch(SQLException e)
						{
							e.printStackTrace();
						}
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String name = null;
		if (cid != null && cid.length() > 0) {
			name = ContactHelper.getContactsNameByPhoneNumber(context, cid);
			if (name != null && name.length() > 0) {
				title = title + " : " + name;
				alert = name + " (" + cid + ")";
			}
		}

		DebugLog.d("==== END CALL STATE SAVE PROCESS...");


		/**
		 *  안드로이드10 벨소리 관련 2020.05.29 맹완석
		 */

		DebugLog.d("==== START NOTIFICATION PROCESS...");

		if (Build.VERSION.SDK_INT >= 29) {
			if (status.equals("BYE") && !App.isCallTakeClicked) {    //부재중알림 isCallTakeClicked --> 전화받기 버튼 클릭 여부인듯
				Log.i(THIS_FILE, "CALL status BYE");
				DebugLog.d("CALL status BYE --> "+"안드로이드 10 벨소리 관련 처리");
				cancelAutoRingStop();
				showNotificationForMissedCall(context, alert);
				stopRing();
//				return; //shlee add
//				Intent stopIntent = new Intent(context, RingtonePlayingService.class);
				//context.stopService(stopIntent);

			} else if (status.equals("BYE")) {
//				Log.i(THIS_FILE, "BRIDGE status BYE");
				DebugLog.d("BRIDGE status BYE --> ");
				cancelAutoRingStop();
				stopRing();
//				return; // shlee add
				//Intent stopIntent = new Intent(context, RingtonePlayingService.class);
				//context.stopService(stopIntent);

			}
		}

		if (SipService.currentService != null && App.isDialMainForeground) {
			Log.e(THIS_FILE, "SERVICE RUNNING !!");
			DebugLog.d("SERVICE RUNNING !!");
			return;
		}

//		if (InCall.currentContext != null && InCall.currentState != 0) {
//			Log.e(THIS_FILE, "SKIP FCM CALL : BUSY ");
//			DebugLog.d("SKIP FCM CALL --> BUSY");
//			return;
//		}

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(CALL_CHANNEL_ID, context.getString(R.string.app_name), importance);
				channel.setSound(null,null);
				notificationManager.createNotificationChannel(channel);
			} else{
				notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		} else {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
				NotificationChannel notificationChannel = notificationManager.getNotificationChannel(CALL_CHANNEL_ID);
				if (notificationChannel == null) {
					int importance = NotificationManager.IMPORTANCE_HIGH;
					notificationChannel = new NotificationChannel(CALL_CHANNEL_ID, context.getString(R.string.pns_call_title), importance);
					notificationChannel.setSound(null,null);
					notificationManager.createNotificationChannel(notificationChannel);
				}
			}
		}

		Notification notification = null;

        if (Build.VERSION.SDK_INT >= 29) {
			/**
			 *  안드로이드10 관련 2020.05.29 맹완석
			 */

			ServicePrefs.mPush = true;
			Intent fullScreenIntent = new Intent(SipManager.ACTION_SIP_DIALER);
			fullScreenIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			fullScreenIntent.putExtra("NO_DELAY", true);								// NO_DELAY 의미
			fullScreenIntent.putExtra("ANDROID10_CLICK", true);						// ANDROID10_CLICK 의미
			fullScreenIntent.putExtra(DialMain.EXTRA_PUSH, true);
			PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,
					fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

			/**
			 * MEDIA SERVICE AREA
			 */

//			if (ringer == null) {
//				ringer = new BridgeRinger(context);
//			}
//
//			if(mediaManager == null) {
//				mediaManager = new BridgeMediaManager(context);
//				mediaManager.startService();
//			}
//
//			if (!ringer.isRinging()) {
//				PreferencesWrapper prefsWrapper = new PreferencesWrapper(context);
//
//				DebugLog.d("ringer.ring() called in FCM 1396");
//
//				/**
//				 * TEST
//				 */
////				ringer.ring(prefsWrapper.getRingtone());
//				if(!mediaManager.isRinging()) {
//					mediaManager.startRing();
//				}
//
////				ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
////				toneGenerator.startTone(ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK, 1000*60);
//
//				autoRingStopRunnable = new Runnable() {
//					@Override
//					public void run() {
//						Log.i(THIS_FILE, "autoRingStopHandler");
//						stopRing();
//					}
//				};
//				autoRingStopHandler = new Handler(Looper.getMainLooper());
//				autoRingStopHandler.postDelayed(autoRingStopRunnable, 60000);
//			}

			notification = MarshmallowResponse.createNotification(context, fullScreenPendingIntent, CALL_CHANNEL_ID, title, alert, R.drawable.noti_inbound);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.when = System.currentTimeMillis();

			// 통지 출력
			//notificationManager.cancel(FCM_NOTIF_CALL);
//			notificationManager.notify(FCM_NOTIF_CALL, notification);

			/**
			 * TEST
			 */
//			if(mediaManager!=null) {
//				mediaManager.stopService();
//				mediaManager=null;
//			}
//
//			mediaManager = new BridgeMediaManager(context);
//			mediaManager.startService();
//			if(mediaManager!=null) {
//				DebugLog.d("BR FCM --> mediaManager is not null --> startRing");
//				mediaManager.startRing();
//			}

			/*if (mediaManager!=null){
				mediaManager.stopService();
				mediaManager=null;
			}

			mediaManager = new BridgeMediaManager(context);
			mediaManager.startService();
			if (mediaManager != null) {
				mediaManager.startRing();
			}*/

        } else {
            Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
            notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
            if (cid != null && cid.length() > 0) {
                notificationIntent.putExtra(DialMain.EXTRA_GCM_CID, cid);
            }
            notificationIntent.putExtra("id", FCM_NOTIF_CALL);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent content = PendingIntent.getActivity(context, (int) (Math.random() * 100), notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, title, alert, R.drawable.noti_inbound);

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.when = System.currentTimeMillis();

            // 통지 출력
            //notificationManager.cancel(FCM_NOTIF_CALL);
            //notificationManager.notify(FCM_NOTIF_CALL, notification);
            //notificationManager.cancel(FCM_NOTIF_CALL);

//            if (call != null) {
//                ServicePrefs.mPush=true;
//                Intent IntroIntent = new Intent(context, DialMain.class);
//                IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                IntroIntent.putExtra("NO_DELAY", true);
//                IntroIntent.putExtra(DialMain.EXTRA_PUSH, true);
//                context.startActivity(IntroIntent);
//                return;
//            }
        }
	}

	/**
	 *  안드로이드10 벨소리 관련 2020.05.29 맹완석
	 */
	public static void cancelAutoRingStop() {
		DebugLog.d("cancelAutoRingStop()");
		if (autoRingStopHandler != null) {
			DebugLog.d("cancelAutoRingStop !!");
			if (autoRingStopRunnable != null) {
				autoRingStopHandler.removeCallbacks(autoRingStopRunnable);
				autoRingStopRunnable = null;
			}
			autoRingStopHandler = null;
		}
	}

	/**
	 *  안드로이드10 벨소리 관련 2020.05.29 맹완석
	 */
	public static void stopRing() {
//		Log.i(THIS_FILE, "stopRing");
		DebugLog.d("stopRing()");
//		if (ringer != null ) { // && ringer.isRinging()) {  shlee remove check this logic
//			DebugLog.d("stopRing() is called...");
//			ringer.stopRing();
//			ringer = null;
//		} else {
//			DebugLog.d("stopRing() is not called...");
//		}

		Intent intent = new Intent("in_call_notification_answer");
		App.getGlobalApplicationContext().sendBroadcast(intent);

//		if(mediaManager!=null) {
////			mediaManager.stopRing();
//			if(mediaManager.getAudioManager().getMode()!=0) {
//				mediaManager.getAudioManager().setMode(AudioManager.MODE_NORMAL);
//			}
//
//			mediaManager.resetSettings();
//			mediaManager.stopAnnoucing();
//		} else {
//
//		}

		if (notificationManager != null) {
			notificationManager.cancel(Fcm.FCM_NOTIF_CALL);
		}

		if (notificationManager != null) {
			notificationManager.cancel(Fcm.FCM_NOTIF_BRIDGE);
		}

		/*new Thread(new Runnable() {
			@Override
			public void run() {
				if (mediaManager != null) {
					mediaManager.stopAnnoucing();
					mediaManager.resetSettings();
					mediaManager.stopService();
					mediaManager = null;
				}
			}
		}).start();*/

	}


	private static void showNotificationForMissedCall(Context context, String displayName) {
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("displayName --> "+displayName);

		if(notificationManager == null) {
			notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			if (Build.VERSION.SDK_INT >= 26) {
				int importance = NotificationManager.IMPORTANCE_DEFAULT;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}
		}

		int icon = R.drawable.noti_missed;
		CharSequence tickerText =  "말톡 "+context.getText(R.string.missed_call);

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_CALLLOG);
		Bundle bundle = new Bundle();
		bundle.putString("action", SipManager.ACTION_SIP_CALLLOG);
		notificationIntent.putExtras(bundle);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		Notification missedCallNotification = MarshmallowResponse.createNotification(context, contentIntent, NEW_CHANNEL_ID, tickerText.toString(), displayName, icon);

		notificationManager.notify(SipNotifications.CALLLOG_NOTIF_ID, missedCallNotification);
	}

	//BJH 2016.08.31
	private static void processBridge(final Context context, JSONObject aps,
									  JSONObject bridge, String push_type) {
//		Log.d(THIS_FILE, "BRIDGE");
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("bridge --> "+bridge.toString());
		DebugLog.d("pushType --> "+push_type);

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		String cid = null;
		String dial_number = null;
		String push_date = null;
		String status = "";
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
		long now = System.currentTimeMillis();
		long date = System.currentTimeMillis();
		long receive_time = System.currentTimeMillis();
		long expired_time = 300000L; // BJH 2017.01.25 사장님 요청 사항 //BJH 2017.07.26 브릿지 단말기 시간 오류 관련
		long close_time = 300000L; //BJH 2017.07.26 브릿지 단말기 시간 오류 관련
		long compare_date = now;
		Log.d(THIS_FILE, "now: " + mFormatter.format(now));
		try {
			if (bridge.has("cid")) {
				cid = bridge.getString("cid");
			}
			if (bridge.has("dial_number")) {
				dial_number = bridge.getString("dial_number");
			}
			if (bridge.has("status"))
				status = bridge.getString("status");

			if (status!=null && status.equals("BYE")){
				BRIDGE_FINAL_STATUS=0;
			}else {
				BRIDGE_FINAL_STATUS=1;
				App.isCallTakeClicked=false;
			}

			if (bridge.has("current_time")) {
				push_date = bridge.getString("current_time"); // BJH 2016.11.11
				if (push_date != null && push_date.length() > 0)
				{
					try {
						Date d = mFormatter.parse(push_date);
						date = d.getTime();
						compare_date = now - date;
						expired_time = expired_time - compare_date;
						DBManager database = new DBManager(context);
						String number;
						try {
							database.open();
							AppPrefs prefs = new AppPrefs(context);
							String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
							TelephonyManager telManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE);
							//shlee request
							if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}
							} else {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}
							}

							PushRecordData push_record_data = new PushRecordData(date, receive_time, 0, push_type, "BRIDGE", cid, uid, number, "");
							database.insertPushRecord(push_record_data);
							database.close();
						}
						catch(SQLException e)
						{
							e.printStackTrace();
						}
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				//AppPrefs prefs = new AppPrefs(context);
				//String bridge_ts = prefs.getPreferenceStringValue(AppPrefs.BRIDGE_TS);

//				Log.d(THIS_FILE, "BRIDGE_TS : " + BRIDGE_TS);
//				Log.d(THIS_FILE, "BRIDGE_TS push_date : " + push_date);

				DebugLog.d("BRIDGE_TS --> "+BRIDGE_TS);
				DebugLog.d("BRIDGE_TS push date --> "+push_date);

				if (BRIDGE_TS != null && BRIDGE_TS.length() > 0 && !status.equals("BYE")) {

					long diffSec=-1000000;
					long diff=-1;
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					try {
						Date FirstDate = format.parse(BRIDGE_TS);
						Date SecondDate = format.parse(push_date);

						diff =  SecondDate.getTime()-FirstDate.getTime();
						diffSec = Math.abs(diff/1000);
					} catch (Exception e){
						Log.e(THIS_FILE,e.getLocalizedMessage());
					}

					if (diffSec!=-1000000 && diff!=-1){
						if(diffSec >= 0 && diffSec <2) {
							if (bridge_cid.equals(cid)) {
//								Log.d(THIS_FILE, "BRIDGE OVERLAP:"+diffSec);
								DebugLog.d("BRIDGE OVERLAP --> "+diffSec);
								return;
							}
						}
					}/*else if(BRIDGE_TS.compareTo(push_date) >= 0) {
						Log.d(THIS_FILE, "BRIDGE OVERLAP");
						return;
					}*/
				}
				//prefs.setPreferenceStringValue(AppPrefs.BRIDGE_TS, push_date);
                BRIDGE_TS=push_date;
				bridge_cid = cid;
			}
		} catch (Exception e) {
			//
		}

//		Log.d(THIS_FILE, "cid: " + cid);
//		Log.d(THIS_FILE, "dial_number: " + dial_number);
//		Log.d(THIS_FILE, "compare_date: " + compare_date);

		DebugLog.d("cid --> "+cid);
		DebugLog.d("dialNumber --> "+dial_number);
		DebugLog.d("compareDate --> "+compare_date);


		String display = "";
		String contact_name = "";
		if (cid != null && cid.length() > 0) {
			display = ContactHelper.getContactsNameByPhoneNumber(context, cid);
			if (display == null) {
				display = cid;
				contact_name = "";
			} else
				contact_name = display;
		}


		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(BRIDGE_CHANNEL_ID, context.getString(R.string.app_name), importance);
				channel.setSound(null,null);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}else {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
				NotificationChannel notificationChannel = notificationManager.getNotificationChannel(BRIDGE_CHANNEL_ID);
				if (notificationChannel == null) {
					int importance = NotificationManager.IMPORTANCE_HIGH;
					notificationChannel = new NotificationChannel(BRIDGE_CHANNEL_ID, context.getString(R.string.app_name), importance);
					notificationChannel.setSound(null,null);
					notificationManager.createNotificationChannel(notificationChannel);
				}
			}
		}

		/**
		 *  안드로이드10 관련 2020.05.29 맹완석
		 */
		if (Build.VERSION.SDK_INT >= 29) {
			if (status.equals("BYE") && !App.isCallTakeClicked) {
				Log.i(THIS_FILE, "BRIDGE status BYE");
				cancelAutoRingStop();
				showNotificationForMissedCall(context, display);
				stopRing();
			} else if (status.equals("BYE")) {
				Log.i(THIS_FILE, "BRIDGE status BYE");
				cancelAutoRingStop();
				stopRing();
			} else {
				String title = "말톡브릿지" + " : " + display;
				Notification notification = null;
				//Intent fullScreenIntent = new Intent(DialBridgeNew.ACTION_BRIDGE_UI);
				Intent fullScreenIntent = new Intent(DialBridgeNew.ACTION_BRIDGE_UI);
				fullScreenIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				fullScreenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				fullScreenIntent.putExtra("cid", cid);
				fullScreenIntent.putExtra("dial_number", dial_number);
				fullScreenIntent.putExtra("expired_time", expired_time);
				PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,
						fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

				if (ringer == null) {
					ringer = new BridgeRinger(context);
				}
				if (!ringer.isRinging()) {
					PreferencesWrapper prefsWrapper = new PreferencesWrapper(context);

					DebugLog.d("ringer.ring() called in FCM 1758");

					/**
					 * TEST
					 */
//					ringer.ring(prefsWrapper.getRingtone());

					autoRingStopRunnable = new Runnable() {
						@Override
						public void run() {
//							Log.i(THIS_FILE, "autoRingStopHandler");
							DebugLog.d("autoRingStopHandler");
							stopRing();
						}
					};
					autoRingStopHandler = new Handler(Looper.getMainLooper());
					autoRingStopHandler.postDelayed(autoRingStopRunnable, 60000);
				}

				notification = MarshmallowResponse.createNotification(context, fullScreenPendingIntent, BRIDGE_CHANNEL_ID, title, alert, R.drawable.noti_inbound);

				notification.flags |= Notification.FLAG_AUTO_CANCEL;
				notification.when = System.currentTimeMillis();

				// 통지 출력
				//notificationManager.cancel(FCM_NOTIF_BRIDGE);
//				notificationManager.notify(FCM_NOTIF_BRIDGE, notification);
				return;
			}
		}


		String display_name = null;
		DialBridgeNew DB = (DialBridgeNew) DialBridgeNew.mBridge;
//		Log.d(THIS_FILE, "DialBridge: " + DB);
		DebugLog.d("DialBridge --> "+DB.getClass().getSimpleName());

		if(compare_date < close_time ) {
			if(DB == null) {
				if(!status.equals("BYE")) {
					//if(!alert.equals("bye")) {
					if (cid != null && cid.length() > 0) {
//						Log.d(THIS_FILE, "GO BRIDGE");
						DebugLog.d("GO BRIDGE");

						Intent BridgeIntent = new Intent(DialBridgeNew.ACTION_BRIDGE_UI);
						/*if(SipService.currentService != null) { //BJH 2016.09.13 이때는 사용자가 말톡을 킴 상태이므로
							BridgeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						} else {
							BridgeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_CLEAR_TASK
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						}*/
						BridgeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						BridgeIntent.putExtra("cid", cid);
						BridgeIntent.putExtra("dial_number", dial_number);
						BridgeIntent.putExtra("expired_time", expired_time);
						context.startActivity(BridgeIntent);


						/*Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + cid));
						context.startActivity(intent2);*/

						/*Intent intent = new Intent(context, BridgePopupService.class);
						intent.putExtra("NAME", display);
						intent.putExtra("PHONE", "");
						intent.putExtra("DESC", context.getString(R.string.mto_desc));
						intent.putExtra("ID", FCM_NOTIF_BRIDGE);
						intent.putExtra("TIME", now);
						intent.putExtra("CID",cid);
						intent.putExtra("dial_number", dial_number);
						context.startService(intent);*/

						/*KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
						if( myKM.inKeyguardRestrictedInputMode()) {
							Intent BridgeIntent = new Intent(DialBridge.ACTION_BRIDGE_UI);
							BridgeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
									| Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_NEW_TASK | *//*Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |*//* Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
							BridgeIntent.putExtra("cid", cid);
							BridgeIntent.putExtra("dial_number", dial_number);
							BridgeIntent.putExtra("expired_time", expired_time);
							BridgeIntent.putExtra("TIME", now);
							BridgeIntent.putExtra("ID", FCM_NOTIF_BRIDGE);
							context.startActivity(BridgeIntent);
						}else{
							Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + cid));
							context.startActivity(intent2);

							Intent intent = new Intent(context, BridgePopupService.class);
							intent.putExtra("NAME", display);
							intent.putExtra("PHONE", "");
							intent.putExtra("DESC", "말톡 브릿지로 전화가 오고 있습니다.");
							intent.putExtra("ID", FCM_NOTIF_BRIDGE);
							intent.putExtra("TIME", now);
							intent.putExtra("CID",cid);
							intent.putExtra("dial_number", dial_number);
							context.startService(intent);
						}*/
						return;
					}
					//}else if(alert.equals("bye")) {
				}/*else if(status.equals("BYE")) {
					Log.d(THIS_FILE, "BYE BRIDGE");
					display_name = DB.getDisplayName();
					if (display_name == null)
						display_name = cid;

					DB.showNotificationForMissedCall(context, display_name);
					DB.finish();
				}*/
			} else {
				//if(alert.equals("bye")) {
				if(status.equals("BYE")) {
//					Log.d(THIS_FILE, "BYE BRIDGE 2");
					DebugLog.d("BYE BRIDGE 2");

					display_name = DB.getDisplayName();
					if (display_name == null)
						display_name = cid;

					DB.showNotificationForMissedCall(context, display_name);
					DB.finish();
					/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
						DB.finishAndRemoveTask();
					}else {
						DB.finish();
					}*/
				}
			}
		} else {
//			Log.d(THIS_FILE, "compare_date >= close_time");
			DebugLog.d("compareDate >= closeTime");
			/*if(DB == null && !status.equals("BYE")) {
				display_name = ContactHelper.getContactsNameByPhoneNumber(context, cid);
				if (display_name == null)
					display_name = cid;

				if(notificationManager == null)
					notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

				int icon = R.drawable.noti_missed;
				CharSequence tickerText =  context.getText(R.string.missed_call);

				Notification missedCallNotification = null;

				Intent notificationIntent2 = new Intent(SipManager.ACTION_SIP_CALLLOG);
				notificationIntent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent2, PendingIntent.FLAG_CANCEL_CURRENT);

				missedCallNotification = MarshmallowResponse.createNotification(context, contentIntent, NEW_CHANNEL_ID, tickerText.toString(), display_name, icon);
				notificationManager.notify(SipNotifications.CALLLOG_NOTIF_ID, missedCallNotification);
			}
			else if(status.equals("BYE")) {
				Log.d(THIS_FILE, "BYE BRIDGE 3");
				display_name = DB.getDisplayName();
				if (display_name == null)
					display_name = cid;

				DB.showNotificationForMissedCall(context, display_name);
				DB.finish();
			}*/
		}
	}

	private static void processVMS(final Context context, JSONObject aps,
								   JSONObject vms) {
		Log.d(THIS_FILE, "VMS");

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		Notification notification = null;

		String number = null;
		try {
			number = vms.getString("number");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
		notificationIntent.putExtra("id", FCM_NOTIF_VMS);
		if (number != null && number.length() > 0)
			notificationIntent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent content = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, context.getString(R.string.pns_vms_title).toString(), alert, R.drawable.noti_vms);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		String sound = null;
		try {
			String temp = aps.getString("sound");
			if (temp != null) {
				String[] soundname = temp.split("\\.");
				sound = soundname[0];
			}
		} catch (Exception e) {

		}

		if (sound != null)
			notification.sound = Uri.parse("android.resource://"
					+ context.getPackageName() + "/raw/" + sound);

		notification.vibrate = new long[] { 1000, 1000, 500, 500, 200, 200,
				200, 200, 200, 200 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_VMS); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
		notificationManager.notify(FCM_NOTIF_VMS, notification);
	}
	//BJH 2016.09.22
	private static DBManager mDatabase;
	private static void processVMSV2(final Context context, JSONObject aps,
									 JSONObject vms) {
		Log.d(THIS_FILE, "VMSV2");

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		AppPrefs prefs = new AppPrefs(context);

		String name = "";
		String cdate = "";
		String subject = context.getString(R.string.pns_vms_title);
		String text = "";
		String file_name = "";

		try {
			if (vms.has("cid"))
				name = vms.getString("cid");
			if (vms.has("current_time")) {
				cdate = vms.getString("current_time");
			}
			if (vms.has("file_name")) {
				file_name = vms.getString("file_name");
				file_name = file_name.substring(0,8) + "/" + file_name;
				if(!file_name.endsWith(".mp3"))
					file_name = file_name + ".mp3";
				text = subject + "|" + file_name;
			}

			String vms_ts = prefs.getPreferenceStringValue(AppPrefs.VMS_TS);
			String vms_file_name = prefs.getPreferenceStringValue(AppPrefs.VMS_FILE_NAME);
			if (vms_ts != null && vms_ts.length() > 0 && vms_file_name != null && vms_file_name.length() > 0) {
				if(vms_ts.compareTo(cdate) >= 0 || file_name.equals(vms_file_name) ) {
					Log.d(THIS_FILE, "VMS OVERLAP");
					return;
				}
			}
			prefs.setPreferenceStringValue(AppPrefs.VMS_TS, cdate);
			prefs.setPreferenceStringValue(AppPrefs.VMS_FILE_NAME, file_name);

			mDatabase = new DBManager(context);
			if(!mDatabase.isOpen()) mDatabase.open();

			SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
			long msgDate = System.currentTimeMillis();
			if (cdate != null && cdate.length() > 0)
			{
				try
				{
					Date d = mFormatter.parse(cdate);
					msgDate = d.getTime();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			try
			{
				String str_ts= "000000000000000"; // 15 : TS + SEQ
				String last_ts = prefs.getPreferenceStringValue(AppPrefs.MSG_TS);
				if (last_ts != null && last_ts.length() > 0)
					str_ts = last_ts;
				String ts = str_ts.substring(13);
				int seq = Integer.parseInt(ts);
				// MSG
				SmsMsgData msg_data = new SmsMsgData(seq, name, "me", name, SmsListData.RECEIVE_TYPE, SmsMsgData.MSG_NEW, subject, text, msgDate);
				msg_data.setMediaValues("", "", "");
				mDatabase.insertSmsMsg(msg_data);
				updateMsgList(context, name);
				if (mDatabase != null)
				{
					if (mDatabase.isOpen())
						mDatabase.close();
					mDatabase = null;
				}
				Intent intent = new Intent(MessageService.ACTION_NEW_MESSAGE);
				context.sendBroadcast(intent);
			} catch (Exception e)
			{
				Log.e(THIS_FILE, "DB", e);
			}
		} catch (Exception e) {
			//
		}

		String display = "";
		if (name != null && name.length() > 0) {
			display = ContactHelper.getContactsNameByPhoneNumber(context, name);
			if (display == null)
				display = name;
		}
		// MESSAGE RELOAD REQUEST
		{
			if (Build.VERSION.SDK_INT < 26) { // BJH 2018.03.13 Oreo 부턴 백그라운드 서비스 이슈가 있음
				Intent intent = new Intent(context, MessageService.class);
				intent.setAction(MessageService.ACTION_START);
				context.startService(intent);
			}
		}

		if (SmsMsgContsActivity.currentContext != null) {
			String number = SmsMsgContsActivity.currentContext.getNumber();
			if (number != null && number.length() > 0
					&& number.equalsIgnoreCase(name)) {
				return;
			}
		}

		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, false);
		notificationIntent.putExtra("id", FCM_NOTIF_MSG);

		notificationIntent.putExtra(DialMain.EXTRA_MESSAGE_TITLE, alert);
		notificationIntent.putExtra(DialMain.EXTRA_MESSAGE_NUMBER, name);

		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent content = PendingIntent.getActivity(context,
				(int) (Math.random() * 100), notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		Notification notification = null;

		notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, subject + " : " + display, alert, R.drawable.noti_vms);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notification.sound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		notification.vibrate = new long[] { 1000, 1000, 500, 500, 200, 200,
				200, 200, 200, 200 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_VMS); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
		notificationManager.notify(FCM_NOTIF_VMS, notification);
	}

	//BJH 2016.09.22
	private static void processMTO(final Context context, JSONObject aps,
								   JSONObject mto, String push_type) {
//		Log.d(THIS_FILE, "MTO");

		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("mto --> "+mto.toString());
		DebugLog.d("pushType --> "+push_type);

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		String title =  context.getString(R.string.mto);
		String name = "";
		String cdate = "";
		long msgDate = System.currentTimeMillis();
		long receive_time = System.currentTimeMillis();
		try {
			if (mto.has("cid"))
				name = mto.getString("cid");
			if (mto.has("current_time")) {
				cdate = mto.getString("current_time");
				SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // BJH 2016.11.11
				mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
				if (cdate != null && cdate.length() > 0)
				{
					try
					{
						Date d = mFormatter.parse(cdate);
						msgDate = d.getTime();
						DBManager database = new DBManager(context);
						String number;
						try {
							database.open();
							AppPrefs prefs = new AppPrefs(context);
							String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
							TelephonyManager telManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE);
							//shlee request
							if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}

							} else {
								if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
									number= "not Found";
								}
								else {
									number = telManager.getLine1Number();
								}
							}

							PushRecordData push_record_data = new PushRecordData(msgDate, receive_time, 0, push_type, "MTO", name, uid, number, "");
							database.insertPushRecord(push_record_data);
							database.close();
						}
						catch(SQLException e)
						{
							e.printStackTrace();
						}
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				AppPrefs prefs = new AppPrefs(context);
				String mto_ts = prefs.getPreferenceStringValue(AppPrefs.MTO_TS);
				if (mto_ts != null && mto_ts.length() > 0) {
					if(mto_ts.compareTo(cdate) >= 0) {
//						Log.d(THIS_FILE, "MTO OVERLAP");
						DebugLog.d("MTO OVERLAP");
						return;
					}
				}
				prefs.setPreferenceStringValue(AppPrefs.MTO_TS,cdate);
			}
		} catch (Exception e) {
			//
		}

		long expired_time = 150000L; // BJH 2017.05.15
		long compare_time = System.currentTimeMillis() - msgDate;
		if(compare_time > expired_time) // 푸시가 온지 한참 지난후이기 때문에
			return;

		String display = "";
		String contact_name = "";
		if (name != null && name.length() > 0) {
			display = ContactHelper.getContactsNameByPhoneNumber(context, name);
			if (display == null) {
				display = name;
				contact_name = "";
			} else
				contact_name = display;
		}


		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		Notification notification = null;

		notification = null;

		/* Intent notificationIntent = new Intent(context, PopupService.class); //BJH 2016.10.12 수정
		notificationIntent.putExtra("NAME", contact_name);
		notificationIntent.putExtra("PHONE", name);
		notificationIntent.putExtra("ID", FCM_NOTIF_MTO);
		notificationIntent.putExtra("DESC", context.getString(R.string.mto_desc)); */
		// BJH 2017.05.15 마시멜로우 그리기 허용 이슈 때문에 주석 처리
		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
		notificationIntent.putExtra("id", FCM_NOTIF_MTO);
		if (name != null && name.length() > 0) {
			notificationIntent.putExtra(DialMain.EXTRA_MTO, name);
		}
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent content = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, title + " : " + display, alert, R.drawable.noti_inbound);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		notification.sound = null;

		notification.vibrate = new long[] { 0, 1000, 250, 1000, 250, 1000, 250, 1000, 250, 1000, 250, 1000, 250, 1000, 500, 1000 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_MTO); // FLAG_CANCEL_CURRENT가
		// 안먹어서직접 제거.
		notificationManager.notify(FCM_NOTIF_MTO, notification);

		/*Intent intent = new Intent(context, PopupService.class);
		if(SipService.currentService != null) { //BJH 2016.09.23 이때는 사용자가 말톡을 킴 상태이므로
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		} else {
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		intent.putExtra("NAME", contact_name);
		intent.putExtra("PHONE", name);
		intent.putExtra("DESC", context.getString(R.string.mto_desc));
		intent.putExtra("ID", FCM_NOTIF_MTO);
		context.startActivity(intent);*/
		//context.startService(notificationIntent);

        /*Intent intent = new Intent(context, PopupService.class);
        intent.putExtra("NAME", contact_name);
        intent.putExtra("PHONE", name);
        intent.putExtra("DESC", context.getString(R.string.mto_desc));
        intent.putExtra("ID", FCM_NOTIF_MTO);
        context.startService(intent);*/

		return;
	}

	private static void processCallBack(final Context context, JSONObject aps,
										JSONObject cb) {
//		Log.d(THIS_FILE, "CALLBACK");

		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("callback --> "+cb.toString());

		String alert = null;
		try {
			alert = aps.getString("alert");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		if (notificationManager == null) {
			if (Build.VERSION.SDK_INT >= 26) {
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				int importance = NotificationManager.IMPORTANCE_HIGH;
				NotificationChannel channel = new NotificationChannel(NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}else{
				notificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
			}
		}

		Notification notification = null;

		String number = null;
		try {
			number = cb.getString("cbnum");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_DIALER);
		notificationIntent.putExtra(DialMain.EXTRA_PUSH, true);
		notificationIntent.putExtra("id", FCM_NOTIF_CB);
		if (number != null && number.length() > 0)
			notificationIntent.putExtra(DialMain.EXTRA_CALL_NUMBER, number);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent content = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		notification = MarshmallowResponse.createNotification(context, content, NEW_CHANNEL_ID, context.getString(R.string.pns_vms_title).toString(), alert, R.drawable.noti_vms);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		String sound = null;
		try {
			String temp = aps.getString("sound");
			if (temp != null) {
				String[] soundname = temp.split("\\.");
				sound = soundname[0];
			}
		} catch (Exception e) {

		}

		if (sound != null)
			notification.sound = Uri.parse("android.resource://"
					+ context.getPackageName() + "/raw/" + sound);

		notification.vibrate = new long[] { 1000, 1000, 500, 500, 200, 200,
				200, 200, 200, 200 };
		notification.when = System.currentTimeMillis();

		// 통지 출력
		notificationManager.cancel(FCM_NOTIF_CB); // FLAG_CANCEL_CURRENT가 안먹어서직접
		// 제거.
		notificationManager.notify(FCM_NOTIF_CB, notification);

	}

	private static String postFCM(Context context) {
		DebugLog.d("caller --> "+context.getClass().getSimpleName());

		String username = ServicePrefs.mUserID;
		String password = ServicePrefs.mUserPWD;

		AppPrefs mPrefs = new AppPrefs(context);

		if (username == null || username.length() == 0) {
			// load
			username = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
			password = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);
		}

		if (username == null || username.length() == 0) {
//			Log.e(THIS_FILE, "REQ_CLIENT_INFO : ERROR AUTH INFO");
			DebugLog.d("REQ_CLIENT_INFO --> ERROR AUTH INFO");
			return "";
		}

		String deviceModel = Build.MODEL;
		if (deviceModel == null) {
			deviceModel = "none";
		}

		PreferencesWrapper prefsWrapper = new PreferencesWrapper(context);
		String codecname=prefsWrapper.getMediaCodec();
		if (codecname == null){
			codecname="silk";
		}

		String osVersion = Build.VERSION.RELEASE;
		if (osVersion == null) {
			osVersion = "none";
		}

		// FCM TOKEN
		//String token = mRegistrationID;
		String token = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);
		// if (token==null || token.length() == 0) {
		// token="null";
		// }
//		Log.i(THIS_FILE, "FCM_TOKEN : " + token);
		DebugLog.d("FCM_TOKEN --> "+token);

		// PUSHY TOKEN
		String pushyToken = PushyMQTT.mRegistrationID;
		// if (pushyToken==null || pushyToken.length() == 0) {
		// pushyToken="null";
		// }
//		Log.i(THIS_FILE, "PUSHY TOKEN : " + pushyToken);
		DebugLog.d("PUSHY TOKEN --> "+pushyToken);

		String url = mPrefs
				.getPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO);
		//url = "https://14.63.160.14/maaltalk/test/req_client_info.php";
//		Log.d(THIS_FILE, "REQ_CLIENT_INFO : " + url);
		DebugLog.d("REQ_CLIENT_INFO --> "+url);

		if (url == null || url.length() == 0)
			return "";

		try {
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(context);

			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 20000);
			HttpConnectionParams.setSoTimeout(params, 20000);

			HttpPost httppost = new HttpPost(url);

			DebugLog.d("REQ_CLIENT_INFO param1: "+username);
			DebugLog.d("REQ_CLIENT_INFO param2: "+password);
			DebugLog.d("REQ_CLIENT_INFO param3: ANDROID");
			DebugLog.d("REQ_CLIENT_INFO param4: "+Build.VERSION.SDK);
			DebugLog.d("REQ_CLIENT_INFO param5: "+token);
			DebugLog.d("REQ_CLIENT_INFO param6: 0");
			DebugLog.d("REQ_CLIENT_INFO param7: "+ServicePrefs.getLoginVersion());
			DebugLog.d("REQ_CLIENT_INFO param8: "+deviceModel
					+ "(" + osVersion +","+codecname+ ")");
			DebugLog.d("REQ_CLIENT_INFO param9: ");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("param1", username));
			nameValuePairs.add(new BasicNameValuePair("param2", password));
			nameValuePairs.add(new BasicNameValuePair("param3", "ANDROID"));
			nameValuePairs.add(new BasicNameValuePair("param4",
					Build.VERSION.SDK));
			nameValuePairs.add(new BasicNameValuePair("param5", token));
			nameValuePairs.add(new BasicNameValuePair("param6", "0"));
			Log.d(THIS_FILE,"getLoginVersion:"+ServicePrefs
					.getLoginVersion());
			nameValuePairs.add(new BasicNameValuePair("param7", ServicePrefs
					.getLoginVersion()));
			nameValuePairs.add(new BasicNameValuePair("param8", deviceModel
					+ "(" + osVersion +","+codecname+ ")"));
			nameValuePairs.add(new BasicNameValuePair("param9", PushyMQTT.mRegistrationID));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			// Execute HTTP Post Request
			// Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);

			BufferedReader br = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();

//			Log.d(THIS_FILE, "HTTPS POST EXEC : " + sb.toString());
			DebugLog.d("HTTP POST EXEC --> "+sb.toString());

			// CHECK STATUS CODE
			if (response.getStatusLine().getStatusCode() != 200) {
//				Log.e(THIS_FILE, "HTTPS POST STATUS : "
//						+ response.getStatusLine().toString());

				DebugLog.d("HTTPS POST STATUS --> "+response.getStatusLine().toString());
				return null;
			}

			return sb.toString();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
//			Log.e(THIS_FILE, "ClientProtocolException", e);
			DebugLog.d("ClientProtocolException --> "+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			Log.e(THIS_FILE, "Exception", e);
			DebugLog.d("Exception --> "+e.getMessage());
			e.printStackTrace();
		}
		return null;

	}

	public static void sendFeedback(Context context, String url) {
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("url --> "+url);

		if (url == null)
			return;
		if (url.length() == 0)
			return;

		AppPrefs mPrefs = new AppPrefs(context);
		String number = mPrefs
				.getPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER);
		if (number == null)
			number = "";

		String username = ServicePrefs.mUserID;
		if (username == null)
			username = "";

		// url + cid
		url = url + "&did=2&cid=" + number + "&uid=" + username;

		Log.d(THIS_FILE, "FEEDBACK GET : " + url);

		sendUrl(context, url);
	}

	private static void sendUrl(final Context context, final String url) {
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("url --> "+url);

		Thread t = new Thread() {
			public void run() {
				try {
					// Create a new HttpClient and Post Header
					HttpClient httpclient = new HttpsClient(context);

					HttpParams params = httpclient.getParams();
					HttpConnectionParams.setConnectionTimeout(params, 20000);
					HttpConnectionParams.setSoTimeout(params, 20000);

					HttpGet httppost = new HttpGet(url);

					httppost.setHeader("User-Agent",
							ServicePrefs.getUserAgent());
					// Execute HTTP GET Request
//					Log.d(THIS_FILE, "HTTPS GET EXEC");
					DebugLog.d("HTTPS GET EXEC");

					HttpResponse response = httpclient.execute(httppost);

					BufferedReader br = new BufferedReader(
							new InputStreamReader(response.getEntity()
									.getContent()));
					String line = null;
					StringBuilder sb = new StringBuilder();
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();

//					Log.d(THIS_FILE, "FEEDBACK RESULT : " + sb.toString());
					DebugLog.d("FEEDBACK RESULT --> "+sb.toString());

				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
//					Log.e(THIS_FILE, "ClientProtocolException", e);
					DebugLog.d("ClientProtocolException --> "+e.getMessage());
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
//					Log.e(THIS_FILE, "Exception", e);
					DebugLog.d("Exception --> "+e.getMessage());
					e.printStackTrace();
				}
			}
		};

		if (t != null) {
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}

	//BJH 2016.09.22
	private static void updateMsgList(Context context, String callback) {
		DebugLog.d("caller --> "+context.getClass().getSimpleName());
		DebugLog.d("callback --> "+callback);

		Cursor c = mDatabase.getLastSmsMsg(callback);
		if(c == null)
		{
			return;
		}

		if(!c.moveToFirst())
		{
			c.close();
			return;
		}

		String display_name = ContactHelper.getContactsNameByPhoneNumber(context, callback);;
		if (display_name == null) display_name = "";

		String text = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
		long msgDate = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));

		c.close();


		int newCount = mDatabase.getSmsNewMsgCount(callback);

		// LIST
		if(mDatabase.existSmsList(callback))
		{
			mDatabase.updateSmsList(callback, display_name, text, SmsListData.RECEIVE_TYPE, newCount);
		}
		else
		{
			//내부번호 인지 체크 2:친구목록
			int userType = SmsListData.USER_TYPE_CONTACT;
			//if(isInternal(callback)) userType = SmsListData.USER_TYPE_BUDDY;

			SmsListData sms_list = new SmsListData(userType,callback, "me", SmsListData.RECEIVE_TYPE, text, display_name, msgDate, newCount);
			mDatabase.insertSmsList(sms_list);
		}
	}

	/* Mac Address */
	public static String getMACAddress() {
		String interfaceName = "wlan0";
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac==null) return "";
				StringBuilder buf = new StringBuilder();
				for (int idx=0; idx<mac.length; idx++)
					buf.append(String.format("%02X:", mac[idx]));
				if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
				return buf.toString();
			}
		} catch (Exception ex) { } // for now eat exceptions


		return "";
	}

	private static void startMessageService(Context context) {
		Log.d(THIS_FILE, "MESSAGE SERVICE START !!");
		Intent intent = new Intent(context, MessageService.class);
		intent.setAction(MessageService.ACTION_START);
		context.startService(intent);
	}

	private static void stopMessageService(Context context) {
		Log.d(THIS_FILE, "MESSAGE SERVICE STOP !!");
		context.stopService(new Intent(context, MessageService.class));
	}

	public static void PushTokenRegister(final Context context) { // Pushy MQTT Pushy 토큰이 늦게 받아서 토큰 응답이 온 후 토큰 업데이트
		DebugLog.d("caller --> "+context.getClass().getSimpleName());

		Thread t = new Thread() {
			public void run() {
				try{
					updateToken(context,"pushy");
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
		if (t != null) {
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}

}
