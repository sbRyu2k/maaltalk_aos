package com.dial070.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.telephony.TelephonyManager;

import com.dial070.utils.Log;

public class InstallReferrerReceiver extends BroadcastReceiver {
	private static final String THIS_FILE = "INSTALL REFERRER";
	private final static String ACTION_INSTALL_REFERRER = "com.android.vending.INSTALL_REFERRER";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(ACTION_INSTALL_REFERRER)) {
			String referrer = intent.getStringExtra("referrer");
			if(referrer != null)
				Log.d(THIS_FILE, referrer);
		}
	}

}
