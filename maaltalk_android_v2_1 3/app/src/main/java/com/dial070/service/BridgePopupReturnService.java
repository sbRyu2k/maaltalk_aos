package com.dial070.service;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.SQLException;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.bridge.utils.BridgeMediaManager;
import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

/*public class BridgePopupReturnService extends Service {
	private static final String THIS_FILE = "BridgePopupService";
	public BridgeMediaManager mediaManager = null;
	private Handler setMediaHandler = null;
	private Runnable setMediaRunnable = null;
	private Notification inCallNotification = null;
	private Notification missedCallNotification = null;
	private NotificationManager notificationManager = null;
	private static final int BRIDGE_NOTIF_ID = 3001;
	private static final int CALL_NOTIF_ID = BRIDGE_NOTIF_ID + 1;
	private NotificationManager mNotificationManager = null;

	private View mView; // 항상 보이게 할 뷰
	private WindowManager mManager;
	private WindowManager.LayoutParams mParams;
	private int mId;
	private Handler autoCloseHandler = null;
	private Runnable autoCloseRunnable = null;

	private float START_X, START_Y; // 움직이기 위해 터치한 시작 점
	private int PREV_X, PREV_Y; // 움직이기 이전에 뷰가 위치한 점
	private int MAX_X = -1, MAX_Y = -1; // 뷰의 위치 최대 값
	private Context mContext;

	private TelephonyManager mTelephonyManager = null;
	private Long mExpired_time = 60000L;
	private Long mMedia_time = 1000L;
	private static final int CALL_INCOMING = 1;
	private static final int CALL_CONNECTING = 2;
	private static final int CALL_DISCONNECTED = 3;
	private boolean mIncoming = false;
	private PowerManager.WakeLock wakeLock = null;
	private boolean mWakeLock = false;
	private PowerManager powerManager = null;

	private String mNumber,mDisplay;

	private int count=0;

	// 전화번호
	private static String phoneNumber = "";

	// incoming 수신 플래그
	private static boolean incomingFlag = false;

	private final IBinder mBinder = new BridgeServiceBinder();

	public class BridgeServiceBinder extends Binder {
		public BridgePopupReturnService getService() {
			return BridgePopupReturnService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(THIS_FILE,"onBind");
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(THIS_FILE,"onUnbind");
		stopService();
		return super.onUnbind(intent);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(THIS_FILE,"onStartCommand");

		return START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this; //2016.11.10
		Log.d(THIS_FILE,"onCreate");
		KeyguardManager km = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
		KeyguardManager.KeyguardLock keyLock = km.newKeyguardLock(Context.KEYGUARD_SERVICE);
		keyLock.disableKeyguard();

		LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.floating_bridge_return, null);
		mView.setOnTouchListener(mViewTouchListener);

		mTelephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);
		mTelephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			mParams = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
							| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
					PixelFormat.TRANSLUCENT);
		} else {
			mParams = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
							| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
					PixelFormat.TRANSLUCENT);
		}
		mParams.gravity = Gravity.TOP | Gravity.LEFT;

		*//*mParams = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
				PixelFormat.TRANSLUCENT);

		// params.gravity = Gravity.RIGHT | Gravity.TOP;
		mParams.gravity = Gravity.TOP | Gravity.LEFT;*//*

		mManager = (WindowManager) getSystemService(WINDOW_SERVICE);

		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		mManager.addView(mView, mParams);
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);


		ImageView imgClose=mView.findViewById(R.id.imgClose);
		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//stopService();
				hideView();
			}
		});

		Button btnMaaltalk = mView.findViewById(R.id.btnMaaltalk);
		btnMaaltalk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(mContext, DialMain.class);
				intent.putExtra("type","BridgeReturn");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);

				hideView();
			}
		});

		//setAutoClose(60000);
	}

	private void listen_none() {
		if(mTelephonyManager != null) {
			mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
			mTelephonyManager = null;
		}
	}
	
	private void stopService() {
		Log.i(THIS_FILE,"stopService");
		stopService(new Intent(BridgePopupReturnService.this, BridgePopupReturnService.class));
	}
	
	private View.OnTouchListener mViewTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: // 사용자 터치 다운이면
				// Log2.i("ACTION_DOWN");
				if (MAX_X == -1)
					setMaxPosition();
				START_X = event.getRawX(); // 터치 시작 점
				START_Y = event.getRawY(); // 터치 시작 점
				PREV_X = mParams.x; // 뷰의 시작 점
				PREV_Y = mParams.y; // 뷰의 시작 점
				break;
			case MotionEvent.ACTION_MOVE:
				// Log2.i("ACTION_MOVE");
				int x = (int) (event.getRawX() - START_X); // 이동한 거리
				int y = (int) (event.getRawY() - START_Y); // 이동한 거리

				// 터치해서 이동한 만큼 이동 시킨다
				mParams.x = PREV_X + x;
				mParams.y = PREV_Y + y;

				// optimizePosition(); //뷰의 위치 최적화
				mManager.updateViewLayout(mView, mParams); // 뷰 업데이트
				break;
			case MotionEvent.ACTION_UP:
				float diffX = event.getRawX() - START_X;
				float diffY = event.getRawY() - START_Y;
				if (Math.abs(diffX) < 30.0 && Math.abs(diffY) < 30.0) {
					//
				}
				break;
			}

			return false;
		}
	};

	private void setMaxPosition() {
		DisplayMetrics matrix = new DisplayMetrics();
		mManager.getDefaultDisplay().getMetrics(matrix); // 화면 정보를 가져와서

		MAX_X = matrix.widthPixels - mView.getWidth(); // x 최대값 설정
		MAX_Y = matrix.heightPixels - mView.getHeight(); // y 최대값 설정
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(THIS_FILE,"onDestroy");
		if (mView != null) // 서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
		{
			((WindowManager) getSystemService(WINDOW_SERVICE))
			.removeView(mView);
			mView = null;
		}

		App.callBridgeFlag=false;
	}

	//BJH 2016.11.10
	private void pushResponse(String action) {
		long response_time = System.currentTimeMillis();
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			AppPrefs prefs = new AppPrefs(mContext);
			String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
			TelephonyManager telManager = (TelephonyManager) mContext.getSystemService( Context.TELEPHONY_SERVICE);
			String number = telManager.getLine1Number();
			PushRecordData push_record_data = new PushRecordData(0, 0, response_time, "", "BRIDGE", mNumber, uid, number, action);
			database.insertPushRecord(push_record_data);
			database.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

	private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		public void onCallStateChanged(int state, String incomingNumber) {
			Log.d(THIS_FILE,"onCallStateChanged state:"+state+", incomingNumber:"+incomingNumber);
			switch (state) {
				*//*case TelephonyManager.CALL_STATE_RINGING: // 폰이 울린다.
				case TelephonyManager.CALL_STATE_OFFHOOK: // 폰이 현재 통화 중.
					if(!mWakeLock) { // wakeLock.acquire()이면 브릿지가 활성화된 상태이므로 브릿지가 끊기면 안되기 때문
						//cancelMediaStart();
						//missed();
					}
					break;*//*
				case TelephonyManager.CALL_STATE_RINGING:
					// TODO 전화를 왔을때이다. (벨이 울릴때)
					incomingFlag = true;
					App.callBridgeFlag=false;
					break;

				case TelephonyManager.CALL_STATE_OFFHOOK:
					if(incomingFlag){
						// TODO 전화가 왔을때이다. (통화 시작)
					} else {
						// TODO 안드로이드 8.0 으로 테스트했을때는 ACTION_NEW_OUTGOING_CALL 거치지 않고, 이쪽으로 바로 온다.
						App.callBridgeFlag=true;
					}

					if (App.callBridgeFlag){
						//cancelAutoClose();
						//stopService();
					}

					break;

				case TelephonyManager.CALL_STATE_IDLE:
					if(incomingFlag){
						// TODO 전화가 왔을때이다. (전화를 끊었을 경우)
					} else {
						// TODO 전화를 걸었을 떄이다. (전화를 끊었을 경우)
						Log.i(THIS_FILE,"CALL_STATE_IDLE");
						if (App.callBridgeFlag){
							Log.i(THIS_FILE,"callBridgeFlag is true");
							App.stopBridgeService();
							listen_none();
						}else {
							Log.i(THIS_FILE,"callBridgeFlag is false");
						}
					}

					App.callBridgeFlag=false;

					break;
				default:
					break;
			}
		}
	};

	private void hideView(){
		Log.i(THIS_FILE,"hideView");
		mView.setVisibility(View.GONE);
	}

	public void showView(){
		Log.i(THIS_FILE,"showView");
		mView.setVisibility(View.VISIBLE);
	}
}*/
