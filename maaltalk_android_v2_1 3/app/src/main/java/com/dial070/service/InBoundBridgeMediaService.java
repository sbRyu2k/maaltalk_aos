package com.dial070.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.dial070.maaltalk.R;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.service.MediaManager;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.status.CallStatus;
import com.dial070.utils.DebugLog;
import com.dial070.utils.NotiUtils;
import com.dial070.utils.ServiceUtils;
import com.dial070.view.notification.BridgeCallNotification;

public class InBoundBridgeMediaService extends SipService {

    public static final String ACTION_IN_BRIDGE_CALL_BYE = "in_bridge_call_received_bye";
    public static final String ACTION_IN_BRIDGE_CALL_DECLINE = "in_call_bridge_notification_decline";
    public static final String ACTION_IN_BRIDGE_CALL_TAKE = "in_call_bridge_notification_take";

    private NotificationActionReceiver mNotificationReceiver;

    private ISipService mSipService = null;
    private ServiceConnection mSipConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            DebugLog.d("onServiceConnected");
            mSipService = ISipService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            DebugLog.d("onServiceDisconnected");
            mSipService = null;
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        DebugLog.d("IBBMS onStartCommand called");

//        if(mSipConnection==null) {
//            DebugLog.d("IBBMS onCreateCommand mSipConnection is null");
//            startService(new Intent(this, SipService.class));
//            bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);
//        } else {
//            DebugLog.d("IBBMS onCreateCommand mSipConnection is not null");
//        }

        if(!ServiceUtils.isRunningService(this, SipService.class)) {
            startService(new Intent(this, SipService.class));
        }
        bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);

        String cid = "";
        if(intent!=null && intent.hasExtra("cid")) {
            cid = intent.getStringExtra("cid");
        }

        initResources();
        startService();
        MediaManager mediaManager = MediaManager.getInstance(this);
        mediaManager.startService();
//        showStartUpNotification("33300000004");
        showStartUpNotification(cid);

//        startRing();
        return START_STICKY;
    }

    private void initResources() {
        DebugLog.d("IBBMS initResources called");
        if(mNotificationReceiver==null) {
            mNotificationReceiver = new NotificationActionReceiver();
        }
    }

    private void startService() {
        DebugLog.d("IBBMS startService called");
        if(mNotificationReceiver!=null) {
            IntentFilter notificationFilter = new IntentFilter();
            notificationFilter.addAction(ACTION_IN_BRIDGE_CALL_BYE);
            notificationFilter.addAction(ACTION_IN_BRIDGE_CALL_DECLINE);
            notificationFilter.addAction(ACTION_IN_BRIDGE_CALL_TAKE);
            registerReceiver(mNotificationReceiver, notificationFilter);
        }
    }

    private void stopService() {
        DebugLog.d("IBBMS stopService called");
        if(mNotificationReceiver!=null) {
            unregisterReceiver(mNotificationReceiver);
        }
    }

    private void showStartUpNotification(String cid) {
        DebugLog.d("IBBMS showStartUpNotification called");
        CallStatus.INSTANCE.updatePushCallNumber(cid);

        Notification notification = null;

        String title = "말톡 브릿지 전화수신요청";
        String desc = "";

        NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
        NotiUtils.createChannel(this);
        PendingIntent fullScreenPendingIntent = NotiUtils.createBridgeFullScreenCallPendingIntent(this, cid, cid, 30000);
        Notification incallNotification = null;

        if(Build.VERSION.SDK_INT >= 29) {
//            incallNotification = NotiUtils.createCallNotification(
//                    this,
//                    fullScreenPendingIntent,
//                    NotiUtils.Channel.BRIDGE_CHANNEL_ID,
//                    title,
//                    cid,
//                    R.drawable.noti_inbound,
//                    true,
//                    true
//            );
            incallNotification = new BridgeCallNotification().createNotification(
                    this,
                    fullScreenPendingIntent,
                    NotiUtils.Channel.BRIDGE_CHANNEL_ID,
                    title,
                    cid,
                    R.drawable.noti_inbound
            );


        } else {
//            incallNotification = NotiUtils.createCallNotification(
//                    this,
//                    fullScreenPendingIntent,
//                    NotiUtils.Channel.BRIDGE_CHANNEL_ID,
//                    title,
//                    cid,
//                    R.drawable.noti_inbound,
//                    true,
//                    true
//            );
            incallNotification = new BridgeCallNotification().createNotification(
                    this,
                    fullScreenPendingIntent,
                    NotiUtils.Channel.BRIDGE_CHANNEL_ID,
                    title,
                    cid,
                    R.drawable.noti_inbound
            );
        }

        startForeground(12345, incallNotification);
    }

    private void startRing() {
        PreferencesWrapper preferencesWrapper = new PreferencesWrapper(this);
        if(SipService.pjService.mediaManager==null) {
            DebugLog.d("IBBMS SipService.pjService.mediaManager is null");
            MediaManager mediaManager = MediaManager.getInstance(this);
//            mediaManager.startService();
            mediaManager.startRing("", "inboundbridgeService_startRing()");

        } else {
            DebugLog.d("IBBMS SipService.pjService.media'Manager is not null");
            SipService.pjService.mediaManager.startRing("", "inboundBridgeService_startRing() else");
        }
    }

    private void stopRing() {
        if(SipService.pjService.mediaManager==null) {
            DebugLog.d("IBBMS SipService.pjService.mediaManager is null");
            MediaManager mediaManager = MediaManager.getInstance(this);
            mediaManager.stopRing();
//            mediaManager.stopService();
        } else {
            DebugLog.d("IBBMS SipService.pjService.mediaManager is not null");
            SipService.pjService.mediaManager.stopRing();
        }
    }

    @Override
    public void onDestroy() {
        DebugLog.d("IBBMS onDestroy called");
        stopService();
        super.onDestroy();
    }

    private class NotificationActionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(ACTION_IN_BRIDGE_CALL_BYE)) {
                DebugLog.d("IBBMS NotificationReceiver received ACTION_IN_BRIDGE_CALL_BYE");
                stopRing();
                if(SipService.pjService.mediaManager!=null) {
                    SipService.pjService.mediaManager.stopService();
                }
            } else if(intent.getAction().equals(ACTION_IN_BRIDGE_CALL_DECLINE)) {
                DebugLog.d("IBBMS NotificationReceiver received ACTION_IN_BRIDGE_CALL_DECLINE");
                stopRing();
                if(SipService.pjService.mediaManager!=null) {
                    SipService.pjService.mediaManager.stopAnnouncing();
                    SipService.pjService.mediaManager.stopService();
                }
                stopSelf();
            } else if(intent.getAction().equals(ACTION_IN_BRIDGE_CALL_TAKE)) {
                stopRing();
                stopSelf();
            }
        }
    }
}
