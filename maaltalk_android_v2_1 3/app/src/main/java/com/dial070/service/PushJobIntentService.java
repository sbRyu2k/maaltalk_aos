package com.dial070.service;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

/**
 * Created by DialCommunications on 2018-03-13.
 */


/*BJH 2018-07-24 FCM, Pushy JobIntentService 통합*/

public class PushJobIntentService extends JobIntentService {

    private static final String THIS_FILE = "Dial070-JOB-SERVICE";
    /**
     * Unique job ID for this service.
     */
    static final int JOB_ID = 1000;

    /**
     * Convenience method for enqueuing work in to this service.
     */
    static void enqueueWork(Context context, Intent work) {
        DebugLog.d("JS request Enqueueing --> "+context.getClass().getName());
//        wakeup(context);
//        Fcm.handleReceive(context, work);
        enqueueWork(context, PushJobIntentService.class, JOB_ID, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DebugLog.d("JS Job execution start");
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        DebugLog.d("onStartCommend --> "+intent.getAction());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleWork(Intent intent) {
        // We have received work to do.  The system or framework is already
        // holding a wake lock for us at this point, so we can just go.
//        Log.i(THIS_FILE, "Executing work: " + intent);
        DebugLog.d("JS Executing work: "+intent);

        //Bundle extras = intent.getExtras();

        if (intent == null) {
//            Log.i(THIS_FILE, " extras is null");
            DebugLog.d("JS Job has no data --> extras is null");
        }else{
//            Log.i(THIS_FILE, " PUSH Received: " + intent.getStringExtra("msg"));
            DebugLog.d("JS Push Received: "+intent.getStringExtra("msg"));
            wakeup(this);
            Fcm.handleReceive(this, intent);
        }

        /*for (int i = 0; i < 5; i++) {
            Log.i(THIS_FILE, "Running service " + (i + 1)
                    + "/5 @ " + SystemClock.elapsedRealtime());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
        Log.i(THIS_FILE, "Completed service @ " + SystemClock.elapsedRealtime());*/
    }

    @Override
    public void onDestroy() {
        DebugLog.d("JS Job execution destroy");
        super.onDestroy();
    }

    private static PowerManager.WakeLock sCpuWakeLock = null;
    private static int id = 0;
    static void acquireCpuWakeLock(Context context) {
        DebugLog.d("caller --> "+context.getClass().getSimpleName());

        //Log.e("PushWakeLock", "Acquiring cpu wake lock");
        //Log.e("PushWakeLock", "wake sCpuWakeLock = " + sCpuWakeLock);

		/*
		if (sCpuWakeLock != null) {
			return;
		}
		*/
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        sCpuWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "maaltalk:com.dial070.service.DialJobIntentService." + ++id);

        sCpuWakeLock.acquire();
    }

	/*
	static void releaseCpuLock() {
		Log.e("PushWakeLock", "Releasing cpu wake lock");
		Log.e("PushWakeLock", "relase sCpuWakeLock = " + sCpuWakeLock);

		if (sCpuWakeLock != null) {
			sCpuWakeLock.release();
			sCpuWakeLock = null;
		}
	}
	*/

    static void wakeup(Context context) {
        DebugLog.d("caller --> "+context.getClass().getSimpleName());
        //if (sCpuWakeLock != null) return;
        acquireCpuWakeLock(context);
        final PowerManager.WakeLock _lock = sCpuWakeLock;
        Thread t = new Thread()
        {
            public void run()
            {
                if (sCpuWakeLock == null) return;
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                _lock.release();
                //Log.e("PushWakeLock", "Releasing cpu wake lock");
            };
        };
        if (t != null)
            t.start();
    }
}
