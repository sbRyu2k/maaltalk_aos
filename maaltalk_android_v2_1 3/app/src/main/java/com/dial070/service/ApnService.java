package com.dial070.service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

public class ApnService extends Service implements View.OnClickListener {
	private static final String THIS_FILE = "APN SERVICE";
	private View mView; // 항상 보이게 할 뷰
	private WindowManager mManager;
	private WindowManager.LayoutParams mParams;
	private ClipboardManager mClipboardManager;
	private Context mContext;
	private int currentViewMode = 0;
	private String mOper = "";
	private String mCMStr;
	private TableLayout mTabFirst, mTabDesc, mTabName, mTabApn, mTabUser,
			mTabPwd, mTabCert, mTabEnd/* , mTabUSA */;
	private TableLayout mTabMMSC, mTabMMSProxy, mTabMMSPort, mTabMCC, mTabMNC,
			mTabType; // BJH 2016.12.07
	private EditText mEditName, mEditApn, mEditUser, mEditPwd, mEditMMSC,
			mEditMMSProxy, mEditMMSPort, mEditMCC, mEditMNC, mEditType; // BJH
																		// 2016.12.07
	private TextView mTextApn, mTextCert;
	private Button mBtnName, mBtnApn, mBtnUser, mBtnPwd, mBefore, mNext,
			mComplete, mCancel, mBtnMMSC, mBtnMMSProxy, mBtnMMSPort, mBtnMCC,
			mBtnMNC, mBtnType/* , mBtnTmobile, mBtnSimple */; // BJH
	// 2016.12.07
	private int mUpDown = 1; // BJH 2016.12.29 up =1, down = 0
	private AppPrefs mPrefs;

	private float START_X, START_Y; // 움직이기 위해 터치한 시작 점
	private int PREV_X, PREV_Y; // 움직이기 이전에 뷰가 위치한 점
	private int MAX_X = -1, MAX_Y = -1; // 뷰의 위치 최대 값

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this;
		mPrefs = new AppPrefs(this);
		LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.apn_floating_v2, null);
		mView.setOnTouchListener(mViewTouchListener);
		mView.setClickable(true);

		mTabFirst = (TableLayout) mView.findViewById(R.id.tableLayoutFirst);
		mTabDesc = (TableLayout) mView.findViewById(R.id.tableLayoutDesc);
		mTabName = (TableLayout) mView.findViewById(R.id.tableLayoutName);
		mTabApn = (TableLayout) mView.findViewById(R.id.tableLayoutApn);
		mTabUser = (TableLayout) mView.findViewById(R.id.tableLayoutUser);
		mTabPwd = (TableLayout) mView.findViewById(R.id.tableLayoutPwd);
		mTabMMSC = (TableLayout) mView.findViewById(R.id.tableLayoutMMSC); // BJH 2016.12.07
		mTabMMSProxy = (TableLayout) mView.findViewById(R.id.tableLayoutMMSProxy);
		mTabMMSPort = (TableLayout) mView.findViewById(R.id.tableLayoutMMSPort);
		mTabMCC = (TableLayout) mView.findViewById(R.id.tableLayoutMCC);
		mTabMNC = (TableLayout) mView.findViewById(R.id.tableLayoutMNC);
		mTabCert = (TableLayout) mView.findViewById(R.id.tableLayoutCert);
		mTabType = (TableLayout) mView.findViewById(R.id.tableLayoutType);
		mTabEnd = (TableLayout) mView.findViewById(R.id.tableLayoutEnd);
		mEditName = (EditText) mView.findViewById(R.id.editTextName);
		mEditApn = (EditText) mView.findViewById(R.id.editTextApn);
		mEditUser = (EditText) mView.findViewById(R.id.editTextUser);
		mEditPwd = (EditText) mView.findViewById(R.id.editTextPwd);
		mEditMMSC = (EditText) mView.findViewById(R.id.editTextMMSC);
		mEditMMSProxy = (EditText) mView.findViewById(R.id.editTextMMSProxy);
		mEditMMSPort = (EditText) mView.findViewById(R.id.editTextMMSPort);
		mEditMCC = (EditText) mView.findViewById(R.id.editTextMCC);
		mEditMNC = (EditText) mView.findViewById(R.id.editTextMNC);
		mEditType = (EditText) mView.findViewById(R.id.editTextType);
		mBtnName = (Button) mView.findViewById(R.id.btnNamecopy);
		mBtnApn = (Button) mView.findViewById(R.id.btnApncopy);
		mBtnUser = (Button) mView.findViewById(R.id.btnUsercopy);
		mBtnPwd = (Button) mView.findViewById(R.id.btnPwdcopy);
		mBtnMMSC = (Button) mView.findViewById(R.id.btnMMSCcopy);
		mBtnMMSProxy = (Button) mView.findViewById(R.id.btnMMSProxycopy);
		mBtnMMSPort = (Button) mView.findViewById(R.id.btnMMSPortcopy);
		mBtnMCC = (Button) mView.findViewById(R.id.btnMCCcopy);
		mBtnMNC = (Button) mView.findViewById(R.id.btnMNCcopy);
		mBtnType = (Button) mView.findViewById(R.id.btnTypecopy);
		mBefore = (Button) mView.findViewById(R.id.btnBefore);
		mNext = (Button) mView.findViewById(R.id.btnNext);
		mComplete = (Button) mView.findViewById(R.id.btnComplete);
		mCancel = (Button) mView.findViewById(R.id.btnCancel);
		mTextApn = (TextView) mView.findViewById(R.id.textViewApn);
		mTextCert = (TextView) mView.findViewById(R.id.textViewCert); // BJH 2016.12.28

		mClipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		mClipboardManager.addPrimaryClipChangedListener(mPrimaryChangeListener);

		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		int x = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				300, dm);
		int y = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				100, dm);

		int flag;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			flag=WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
		}else{
			flag=WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
		}
		mParams = new WindowManager.LayoutParams(x,
				WindowManager.LayoutParams.WRAP_CONTENT,
				flag, // 항상 최 상위에 있게.
																// status bar 밑에
																// 있음. 터치 이벤트 받을
																// 수 있음.
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD, // 이
																			// 속성을
																			// 안주면
																			// 터치
																			// &
																			// 키
																			// 이벤트도
																			// 먹게
																			// 된다.
				// 포커스를 안줘서 자기 영역 밖터치는 인식 안하고 키이벤트를 사용하지 않게 설정
				PixelFormat.TRANSLUCENT);

		// params.gravity = Gravity.RIGHT | Gravity.TOP;
		mParams.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;
		mParams.y = y;

		mManager = (WindowManager) getSystemService(WINDOW_SERVICE);

		mManager.addView(mView, mParams);

		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		mOper = telephonyManager.getSimOperatorName().toLowerCase();// 심 회사 체크
		if(mOper == null)
			mOper = "";
		String operCode = telephonyManager.getSimOperator();
		Log.d(THIS_FILE, "SimOperatorName : " + mOper);

		if (mOper != null && mOper.length() > 0) {
			if (mOper.startsWith("lycamobile")) // Lyca
				mOper = "lyca";
			else if (mOper.startsWith("vinaphone")) // 베트남
				mOper = "vinaphone";
			else if (mOper.startsWith("ntt")) // 일본 2GB, 일본무제한
				mOper = "ntt";
			else if (mOper.startsWith("unicom") || mOper.contains("unicom")) // 중국
				mOper = "3gnet";
			else if (mOper.startsWith("ais")) // 태국
				mOper = "ais";
			else if (mOper.startsWith("abc")) // 홍콩
				mOper = "abc";
			else if (mOper.startsWith("3")) // 홍콩
				mOper = "europe";
		} else {
			if (operCode != null && operCode.length() > 0) {
				if (operCode.equals("310410")) // H2O
					mOper = "h2o";
				else if (operCode.equals("45407")) // 마카오
					mOper = "3gnet";
				else if (operCode.equals("310260")) // 심플모바일, T-Mobile
					mOper = "t-mobile";
				else if (operCode.equals("23420")) // 유럽
					mOper = "europe";
			}
		}

		if (mOper.startsWith("vinaphone")) {
			mTextApn.setText(this.getResources().getString(
					R.string.apn_vinaphone));
			mEditName.setText("vinaphone");
			mEditApn.setText("m3-world");
			mEditUser.setText("mms");
			mEditPwd.setText("mms");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("");
		} else if (mOper.startsWith("ais")) {
			mTextApn.setText(this.getResources().getString(R.string.apn_ais));
			mEditName.setText("AIS internet");
			mEditApn.setText("internet");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("520");
			mEditMNC.setText("03");
			mTextCert.setText(getResources().getString(R.string.new_apn_cert));
			mEditType.setText("default");
		} else if (mOper.startsWith("ntt")) {
			mTextApn.setText(this.getResources().getString(R.string.apn_ntt));
			mEditName.setText("NTT Docomo");
			mEditApn.setText("umobile.jp");
			mEditUser.setText("umobile@umobile.jp");
			mEditPwd.setText("umobile");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText(getResources().getString(R.string.apn_cert));
			mEditType.setText("");
		} else if (mOper.startsWith("3gnet")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_short));
			mEditName.setText("3gnet");
			mEditApn.setText("3gnet");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("");
		} else if (mOper.startsWith("abc")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_short));
			mEditName.setText("CMHK DATA");
			mEditApn.setText("cmhk");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("");
		} else if (mOper.startsWith("lyca")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_lyca));
			mEditName.setText("lyca mobile");
			mEditApn.setText("data.lycamobile.com");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("http://lyca.mmsmvno.com/mms/wapenc");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("default,mms,supl");
		} else if (mOper.startsWith("t-mobile")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_short));
			mEditName.setText("T-Mobile");
			mEditApn.setText("epc.tmobile.com");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("");
		} else if (mOper.startsWith("europe")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_short));
			mEditName.setText("3");
			mEditApn.setText("three.co.uk");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("");
			mEditMMSProxy.setText("");
			mEditMMSPort.setText("");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("");
		} else if (mOper.startsWith("h2o")) { // BJH 2016.12.07
			mTextApn.setText(this.getResources().getString(R.string.apn_h2o));
			mEditName.setText("h2o wireless");
			mEditApn.setText("prodata");
			mEditUser.setText("");
			mEditPwd.setText("");
			mEditMMSC.setText("http://mmsc.mobile.att.net");
			mEditMMSProxy.setText("proxy.mobile.att.net");
			mEditMMSPort.setText("80");
			mEditMCC.setText("");
			mEditMNC.setText("");
			mTextCert.setText("");
			mEditType.setText("default,mms,supl");
		} else {
			if(mOper.equals("sktelecom") || mOper.equals("kt") || mOper.equals("lg u+") || mOper.equals("olleh")) {
				mTextApn.setText(this.getResources()
						.getString(R.string.apn_default));
				mEditName.setText("");
				mEditApn.setText("");
				mEditUser.setText("");
				mEditPwd.setText("");
				mEditMMSC.setText("");
				mEditMMSProxy.setText("");
				mEditMMSPort.setText("");
				mEditMCC.setText("");
				mEditMNC.setText("");
				mTextCert.setText("");
				mEditType.setText("");
			} else {
				String desc = mPrefs.getPreferenceStringValue(AppPrefs.MTU_DESC);
				if(desc != null && desc.length() > 0) {
					mTextApn.setText(desc);
					mEditName.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_USIM_NAME));
					mEditApn.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_APN));
					mEditUser.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_USER_NAME));
					mEditPwd.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_PWD));
					mEditMMSC.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_MMSC));
					mEditMMSProxy.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_MMS_PROXY));
					mEditMMSPort.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_MMS_PORT));
					mEditMCC.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_MCC));
					mEditMNC.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_MNC));
					mTextCert.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_CERT));
					mEditType.setText(mPrefs.getPreferenceStringValue(AppPrefs.MTU_TYPE));
				} else {
					mTextApn.setText(this.getResources()
							.getString(R.string.apn_default));
					mEditName.setText("");
					mEditApn.setText("");
					mEditUser.setText("");
					mEditPwd.setText("");
					mEditMMSC.setText("");
					mEditMMSProxy.setText("");
					mEditMMSPort.setText("");
					mEditMCC.setText("");
					mEditMNC.setText("");
					mTextCert.setText("");
					mEditType.setText("");
				}
			}
		}
		SetViewMode(currentViewMode);

		mBtnName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditName.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnApn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditApn.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnUser.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditUser.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnPwd.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditPwd.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnMMSC.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditMMSC.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnMMSProxy.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditMMSProxy.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnMMSPort.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditMMSPort.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnMCC.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditMCC.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnMNC.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditMNC.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mBtnType.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCMStr = mEditType.getText().toString();
				mClipboardManager.setText(mCMStr);
			}

		});

		mNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mUpDown = 1;
				SetViewMode(currentViewMode + 1);
			}

		});

		mBefore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mUpDown = 0;
				SetViewMode(currentViewMode - 1);
			}

		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				stopService(new Intent(ApnService.this, ApnService.class));
			}

		});

		mComplete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				stopService(new Intent(ApnService.this, ApnService.class));
			}

		});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mView != null) // 서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
		{
			mClipboardManager
					.removePrimaryClipChangedListener(mPrimaryChangeListener);
			((WindowManager) getSystemService(WINDOW_SERVICE))
					.removeView(mView);
			mView = null;
		}
	}

	private void SetViewMode(int mode) {
		String name = mEditName.getText().toString();
		String apn = mEditApn.getText().toString();
		String user = mEditUser.getText().toString();
		String pwd = mEditPwd.getText().toString();
		String mmsc = mEditMMSC.getText().toString();
		String mms_proxy = mEditMMSProxy.getText().toString();
		String mms_port = mEditMMSPort.getText().toString();
		String mcc = mEditMCC.getText().toString();
		String mnc = mEditMNC.getText().toString();
		String cert = mTextCert.getText().toString();
		String type = mEditType.getText().toString();
		boolean change = false;
		switch (mode) {
		case 0:
			// FIRST
			mTabFirst.setVisibility(View.VISIBLE);
			mTabDesc.setVisibility(View.GONE);
			mTabName.setVisibility(View.GONE);
			mTabApn.setVisibility(View.GONE);
			mTabUser.setVisibility(View.GONE);
			mTabPwd.setVisibility(View.GONE);
			mTabMMSC.setVisibility(View.GONE);
			mTabMMSProxy.setVisibility(View.GONE);
			mTabMMSPort.setVisibility(View.GONE);
			mTabMCC.setVisibility(View.GONE);
			mTabMNC.setVisibility(View.GONE);
			mTabCert.setVisibility(View.GONE);
			mTabType.setVisibility(View.GONE);
			mTabEnd.setVisibility(View.GONE);
			mBefore.setVisibility(View.GONE);
			mNext.setVisibility(View.VISIBLE);
			mCancel.setVisibility(View.VISIBLE);
			mComplete.setVisibility(View.GONE);
			break;
		case 1:
			// DESC
			mTabFirst.setVisibility(View.GONE);
			mTabDesc.setVisibility(View.VISIBLE);
			mTabName.setVisibility(View.GONE);
			mTabApn.setVisibility(View.GONE);
			mTabUser.setVisibility(View.GONE);
			mTabPwd.setVisibility(View.GONE);
			mTabMMSC.setVisibility(View.GONE);
			mTabMMSProxy.setVisibility(View.GONE);
			mTabMMSPort.setVisibility(View.GONE);
			mTabMCC.setVisibility(View.GONE);
			mTabMNC.setVisibility(View.GONE);
			mTabCert.setVisibility(View.GONE);
			mTabType.setVisibility(View.GONE);
			mTabEnd.setVisibility(View.GONE);
			mBefore.setVisibility(View.VISIBLE);
			mNext.setVisibility(View.VISIBLE);
			mCancel.setVisibility(View.VISIBLE);
			mComplete.setVisibility(View.GONE);
			break;
		case 2:
			if (name != null && name.length() > 0) {
				// NAME
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.VISIBLE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change= true;
			break;
		case 3:
			if (apn != null && apn.length() > 0) {
				// APN
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.VISIBLE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 4:
			if (user != null && user.length() > 0) {
				// USER NAME
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.VISIBLE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 5:
			if (pwd != null && pwd.length() > 0) {
				// PWD
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.VISIBLE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 6:
			if (mmsc != null && mmsc.length() > 0) {
				// MMSC
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.VISIBLE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else 
				change = true;
			break;
		case 7:
			if (mms_proxy != null && mms_proxy.length() > 0) {
				// MMS PROXY
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.VISIBLE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 8:
			if (mms_port != null && mms_port.length() > 0) {
				// MMS PORT
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.VISIBLE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 9:
			if (mcc != null && mcc.length() > 0) {
				// MCC
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.VISIBLE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 10:
			if (mnc != null && mnc.length() > 0) {
				// MNC
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.VISIBLE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 11:
			if (cert != null && cert.length() > 0) {
				// CERT
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.VISIBLE);
				mTabType.setVisibility(View.GONE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 12:
			if (type != null && type.length() > 0) {
				// TYPE
				mTabFirst.setVisibility(View.GONE);
				mTabDesc.setVisibility(View.GONE);
				mTabName.setVisibility(View.GONE);
				mTabApn.setVisibility(View.GONE);
				mTabUser.setVisibility(View.GONE);
				mTabPwd.setVisibility(View.GONE);
				mTabMMSC.setVisibility(View.GONE);
				mTabMMSProxy.setVisibility(View.GONE);
				mTabMMSPort.setVisibility(View.GONE);
				mTabMCC.setVisibility(View.GONE);
				mTabMNC.setVisibility(View.GONE);
				mTabCert.setVisibility(View.GONE);
				mTabType.setVisibility(View.VISIBLE);
				mTabEnd.setVisibility(View.GONE);
				mBefore.setVisibility(View.VISIBLE);
				mNext.setVisibility(View.VISIBLE);
				mCancel.setVisibility(View.VISIBLE);
				mComplete.setVisibility(View.GONE);
			} else
				change = true;
			break;
		case 13:
			// END
			mTabFirst.setVisibility(View.GONE);
			mTabDesc.setVisibility(View.GONE);
			mTabName.setVisibility(View.GONE);
			mTabApn.setVisibility(View.GONE);
			mTabUser.setVisibility(View.GONE);
			mTabPwd.setVisibility(View.GONE);
			mTabMMSC.setVisibility(View.GONE);
			mTabMMSProxy.setVisibility(View.GONE);
			mTabMMSPort.setVisibility(View.GONE);
			mTabMCC.setVisibility(View.GONE);
			mTabMNC.setVisibility(View.GONE);
			mTabCert.setVisibility(View.GONE);
			mTabType.setVisibility(View.GONE);
			mTabEnd.setVisibility(View.VISIBLE);
			mBefore.setVisibility(View.GONE);
			mNext.setVisibility(View.GONE);
			mCancel.setVisibility(View.GONE);
			mComplete.setVisibility(View.VISIBLE);
			break;
		}
		if(!change) {
			currentViewMode = mode; // save
		}
		else {
			if(mUpDown == 1)
				SetViewMode(mode + 1);
			else
				SetViewMode(mode - 1);
		}

	}

	private ClipboardManager.OnPrimaryClipChangedListener mPrimaryChangeListener = new ClipboardManager.OnPrimaryClipChangedListener() {
		public void onPrimaryClipChanged() {

			// this will be called whenever you copy something to the clipboard
			Toast.makeText(mContext,
					mContext.getResources().getString(R.string.apn_copy_desc),
					Toast.LENGTH_SHORT).show();
		}
	};

	private View.OnTouchListener mViewTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: // 사용자 터치 다운이면
				// Log2.i("ACTION_DOWN");
				if (MAX_X == -1)
					setMaxPosition();
				START_X = event.getRawX(); // 터치 시작 점
				START_Y = event.getRawY(); // 터치 시작 점
				PREV_X = mParams.x; // 뷰의 시작 점
				PREV_Y = mParams.y; // 뷰의 시작 점
				break;
			case MotionEvent.ACTION_MOVE:
				// Log2.i("ACTION_MOVE");
				int x = (int) (event.getRawX() - START_X); // 이동한 거리
				int y = (int) (event.getRawY() - START_Y); // 이동한 거리

				// 터치해서 이동한 만큼 이동 시킨다
				mParams.x = PREV_X + x;
				mParams.y = PREV_Y + y;

				// optimizePosition(); //뷰의 위치 최적화
				mManager.updateViewLayout(mView, mParams); // 뷰 업데이트
				break;
			case MotionEvent.ACTION_UP:
				float diffX = event.getRawX() - START_X;
				float diffY = event.getRawY() - START_Y;
				if (Math.abs(diffX) < 30.0 && Math.abs(diffY) < 30.0) {
					//
				}
				break;
			}

			return false;
		}
	};

	private void setMaxPosition() {
		DisplayMetrics matrix = new DisplayMetrics();
		mManager.getDefaultDisplay().getMetrics(matrix); // 화면 정보를 가져와서

		MAX_X = matrix.widthPixels - mView.getWidth(); // x 최대값 설정
		MAX_Y = matrix.heightPixels - mView.getHeight(); // y 최대값 설정
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

}
