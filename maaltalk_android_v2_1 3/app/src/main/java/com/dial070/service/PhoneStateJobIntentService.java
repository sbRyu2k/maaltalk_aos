package com.dial070.service;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.JobIntentService;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.dial070.db.CTimeData;
import com.dial070.db.DBManager;
import com.dial070.db.FavoritesData;
import com.dial070.db.RecentCallsData;
import com.dial070.db.ScoreData;
import com.dial070.db.ZoneData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.Log;
import com.dial070.utils.ScoreUtils;
import com.dial070.utils.Util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneStateJobIntentService extends JobIntentService {
	public static final String ACTION_START = "com.dial070.service.PhoneStateJobIntentService.ACTION_START";
	public static final String ACTION_INCOMMING = "com.dial070.service.PhoneStateJobIntentService.INCOMMING_CALL";
	public static final String ACTION_OUTGOING = "com.dial070.service.PhoneStateJobIntentService.OUTGOING_CALL";

	private static final String THIS_FILE = "PhoneStateJobIntentService";
//	private int CallState = TelephonyManager.CALL_STATE_IDLE;

	private String PhoneNumber = "";
	//private String CallStartTime ="";
	//private String CallEndTime ="";
	private long startTime,endTime;
	private boolean IsConnected = false;

	private int mCallType;	//1:Incomming, 2: Outgoing, 3:Missed

	private DBManager mDatabase;
	private AppPrefs mPrefs;
	private PreferencesWrapper mSysPrefs;
	private Context mContext;

	//private Format mFormatter;

	static final int JOB_ID = 3000;

	/**
	 * Convenience method for enqueuing work in to this service.
	 */
	static void enqueueWork(Context context, Intent work) {
		enqueueWork(context, PhoneStateJobIntentService.class, JOB_ID, work);
	}

	@Override
	protected void onHandleWork(Intent intent) {
		Log.d(THIS_FILE, "PhoneStateJobIntentService onHandleWork");

		if(intent == null)
		{
			Log.e(THIS_FILE,"onProcess: Intent is null!");
			stopSelf();
			return;

		}

		Log.d(THIS_FILE, "onProcess : " + Build.DEVICE);
		onProcess(intent);
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		Log.d(THIS_FILE, "PhoneStateJobIntentService onCreate");

		mDatabase = new DBManager(this);

		mContext = this;

		mPrefs = new AppPrefs(this);
		mSysPrefs = new PreferencesWrapper(this);

		if(!mDatabase.isOpen()) mDatabase.open();
		//mFormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "PhoneStateJobIntentService onDestroy");

	    try {
	         if( mDatabase.isOpen() )
	        	 mDatabase.close();
	      } catch(Exception ex) {
	    	  ex.printStackTrace();
	      }

		Log.d(THIS_FILE, "PhoneStateJobIntentService onDestroy : Database Close!!");
		super.onDestroy();

	}

	private void onProcess(Intent intent) {
		
		String action = intent.getAction();
		Bundle bundle = intent.getExtras();
		if (action == null)
			return;

		if(bundle == null)
		{
			Log.e(THIS_FILE,"onProcess: Bundle is null!");
			return;
		}
		
		if (action.equals("android.intent.action.PHONE_STATE")) {

			String state = bundle.getString(TelephonyManager.EXTRA_STATE);

			if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {

				OnIdle();

			} else if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
				String number = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				OnIncomingCall(number);

			} else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {

				OnOffHook();

			} else {

				Log.d(THIS_FILE, " EXTRA_STATE : " + state);

			}

		} else if (action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {

			OnOutgoingCall(bundle.getString(Intent.EXTRA_PHONE_NUMBER));

		} else {

			Log.d(THIS_FILE, " CALL STATE : " + action);

		}
	}

	private void OnIncomingCall(String number) {
		Log.d(THIS_FILE, "INCOMMING CALL : " + number);
		
		Intent intent = new Intent(ACTION_INCOMMING);
		sendBroadcast(intent);			
		
		if (number == null) 
		{
			number = getResources().getString(R.string.call_cid_not_found);
		}		
		//필터링 call number
		number = number.replaceAll("\\+", "");
		number = number.replaceAll("\\*", "");
		number = number.replaceAll("\\#", "");
		number = number.replaceAll("\\-", "");

//		this.CallState = TelephonyManager.CALL_STATE_RINGING;
		this.PhoneNumber = number;
		this.mCallType = RecentCallsData.INCOMING_TYPE;
		this.IsConnected = false;
	}

	private void OnOutgoingCall(String number) {
		Log.d(THIS_FILE, "OUTGOING CALL : " + number);
		
		Intent intent = new Intent(ACTION_OUTGOING);
		sendBroadcast(intent);		
		
		if (number == null) 
		{
			number = getResources().getString(R.string.call_cid_not_found);
		}
		
		//필터링 call number
		number = number.replaceAll("\\+82", "0");
		number = number.replaceAll("\\+", "");
		number = number.replaceAll("\\*", "");
		number = number.replaceAll("\\#", "");
		number = number.replaceAll("\\-", "");
		

//		this.CallState = TelephonyManager.CALL_STATE_RINGING;
		this.PhoneNumber = number;
		this.mCallType = RecentCallsData.OUTGOING_TYPE;
		this.IsConnected = false;
	}

	private void OnOffHook() {
		Log.d(THIS_FILE, "OFFHOOK");
//		this.CallState = TelephonyManager.CALL_STATE_OFFHOOK;
		this.IsConnected = true;
		startTime = System.currentTimeMillis();
	}

	private void OnIdle() {
		Log.d(THIS_FILE, "IDLE");
		
//		this.CallState = TelephonyManager.CALL_STATE_IDLE;
		endTime = System.currentTimeMillis();

		Boolean authOk = mPrefs.getPreferenceBooleanValue(AppPrefs.AUTH_OK);
		if(authOk)
		{
			MergeFavorite();
			if (ServicePrefs.DIAL_RECENT_MERGE)
				InsertRecentCalls();
		}
		stopSelf();
	}

	private void MergeFavorite()
	{
		if (mDatabase == null) return;

		if(PhoneNumber == null || PhoneNumber.length() == 0) return;
		
		//
		//필터링 call number
		String call_number = PhoneNumber;
		call_number = call_number.replaceAll("\\+82", "0");
		call_number = call_number.replaceAll("\\+", "");
		call_number = call_number.replaceAll("\\*", "");
		call_number = call_number.replaceAll("\\#", "");
		call_number = call_number.replaceAll("\\-", "");		

		String display_name = call_number;
		
		if (PhoneNumber.contains("<") || PhoneNumber.contains("@"))
		{
			display_name = "";
			Pattern sipUriSpliter = Pattern.compile("^(?:\")?([^<\"]*)(?:\")?[ ]*(?:<)?sip(?:s)?:([^@]*)@[^>]*(?:>)?");
			Matcher m = sipUriSpliter.matcher(PhoneNumber);
			if (m.matches()) {
				if (!TextUtils.isEmpty(m.group(2))) {
					call_number = m.group(2);
				} else if (!TextUtils.isEmpty(m.group(1))) {
					call_number = m.group(1);
				}
			}
		}
		else
		{
			// NORMAL NUMBER
			display_name = ContactHelper.getContactsNameByPhoneNumber(this, call_number);
			if (display_name == null) display_name = call_number;
		}
		
		boolean existFavorite = false;
		int density = 1;
		Cursor c = mDatabase.getFavoriteByPhone(call_number);
		if(c != null)
		{
			if(c.getCount() > 0)
			{
				existFavorite = true;
				if(c.moveToFirst())
				{
					density = c.getInt(c.getColumnIndex(FavoritesData.FIELD_DENSITY));
					density = density + 1;
				}
			}
			c.close();
		}
		
		if(existFavorite)
		{
			try
			{
				mDatabase.updateFavoriteDensity(call_number,density);
			} catch (Exception e) {
				e.printStackTrace();
			}				
		}
		else
		{
			
			boolean use_favorite = mSysPrefs.getPreferenceBooleanValue(PreferencesWrapper.USE_FAVORITE);
			if (use_favorite)
			{
				FavoritesData favorite = new FavoritesData(display_name, call_number, "", "", call_number, "AUTO", "",1);
				try
				{
					mDatabase.insertFavorites(favorite);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		
		
		//FavoritesData favorite = new FavoritesData(displayname, phone, id, indentifier, phone_number, "", "", "",density);

		//if (mDatabase.insertFavorites(favorite) < 0) return false;		
	}
	
	private void InsertRecentCalls()
	{

//		int call_type = 0; //normal
//		int hidden = 0;		//show
		int duration = 0;
//		int identifier = 0;
		
		if(PhoneNumber == null || PhoneNumber.length() == 0) return;
		
		//
		//필터링 call number
		String call_number = PhoneNumber;
		call_number = call_number.replaceAll("\\+82", "0");
		call_number = call_number.replaceAll("\\+", "");
		call_number = call_number.replaceAll("\\*", "");
		call_number = call_number.replaceAll("\\#", "");
		call_number = call_number.replaceAll("\\-", "");		
		
		String display_name = call_number;

		
		if (PhoneNumber.contains("<") || PhoneNumber.contains("@"))
		{
			display_name = "";
			Pattern sipUriSpliter = Pattern.compile("^(?:\")?([^<\"]*)(?:\")?[ ]*(?:<)?sip(?:s)?:([^@]*)@[^>]*(?:>)?");
			Matcher m = sipUriSpliter.matcher(PhoneNumber);
			if (m.matches()) {
				if (!TextUtils.isEmpty(m.group(2))) {
					call_number = m.group(2);
				} else if (!TextUtils.isEmpty(m.group(1))) {
					call_number = m.group(1);
				}
			}
//			hidden = 1;
		}
		else
		{
			// NORMAL NUMBER
			display_name = ContactHelper.getContactsNameByPhoneNumber(this, call_number);
			if (display_name == null) display_name = "";
		}

//		String call_memo = "";
		
		if((endTime - startTime) > 0)
			duration = (int)((endTime - startTime) / 1000);		
		
		if (this.mCallType == RecentCallsData.INCOMING_TYPE && !this.IsConnected)
			this.mCallType = RecentCallsData.MISSED_TYPE;
		
		if (mDatabase == null) return;
		
		AppPrefs prefs = new AppPrefs(mContext);
		
		/*
		RecentCallsData cdr = new RecentCallsData(display_name, call_number, identifier,this.mCallType, duration, call_type, hidden,call_memo,"");
		try
		{
			mDatabase.insertRecentCalls(cdr);
			
			//Save call number
			prefs.setPreferenceStringValue(AppPrefs.LAST_CALL_NUMBER, call_number);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		
		//Score 
		int score = 0;
		int call_count = 1;
		int call_count_score = 1;
//		long call_start = 1;
		int call_state = 1;
		int call_start_score = 4;
		int call_dur = duration;
		int call_dur_score = ScoreUtils.getScoreDuration(call_dur);
		int call_state_score = 0;
		int con_add = 0;
		int con_score = 0;
		int fav_add = 0;
		int fav_score = 0;
		int loc_score = 0;
		
		
		boolean existFavorite = false;
		if(mDatabase.existFavoriteByPhone(call_number))
		{
			fav_add = 1;
			fav_score = 10;
			existFavorite = true;
		}		
		

		// 즐겨찾기에 있는 경우만 연산.
		if (existFavorite)
		{
	
			//GPS 사용시에만 계산함
			if (ServicePrefs.checkGps(mContext))
			{
				Double myLatitude = Util.parseDouble(prefs.getPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE));
				Double myLongitude = Util.parseDouble(prefs.getPreferenceStringValue(AppPrefs.LAST_LOC_LATITUDE));
				Double lastLatitude,lastLongitude;
				String callNumber = "";
				//위치정보 계산
				boolean Found = false;
				//Cursor cur_loc = mDatabase.getZoneByPhone(call_number);
				Cursor cur_loc = mDatabase.getZones();
				if(cur_loc != null)
				{
					//현재 위치와 등록된 사용자와의 거리를 계산
					if (cur_loc.moveToFirst())
					{
						do
						{					
							lastLatitude = cur_loc.getDouble(cur_loc.getColumnIndex(ZoneData.FIELD_LATITUDE));
							lastLongitude = cur_loc.getDouble(cur_loc.getColumnIndex(ZoneData.FIELD_LONGITUDE));
							callNumber = cur_loc.getString(cur_loc.getColumnIndex(ZoneData.FIELD_NUMBER));
							//distance 계산
							Location locationA = new Location("point A");
		
							locationA.setLatitude(lastLatitude);
							locationA.setLongitude(lastLongitude);
		
							Location locationB = new Location("point B");
		
							locationB.setLatitude(myLatitude);
							locationB.setLongitude(myLongitude);
		
							Log.d(THIS_FILE,"Number="+callNumber+",lastLatitude=" + lastLatitude + ",lastLongitude=" + lastLongitude);
							
							float distance = locationA.distanceTo(locationB);
							Log.d(THIS_FILE,"DISTANECE="+distance);
							if(distance <= 500.0f)
							{
								//Found
								Found = true;
								loc_score = 10;
								break;
							}										
						}
						while(cur_loc.moveToNext());
					}
					
					cur_loc.close();
					
					if(!Found)
					{
						//INSERT ZONE
						ZoneData zone = new ZoneData(call_number, myLatitude, myLongitude,0);
						mDatabase.insertZones(zone);
						loc_score = 10;
					}
					
				}
				else
				{
					ZoneData zone = new ZoneData(call_number, myLatitude, myLongitude,0);
					mDatabase.insertZones(zone);
					loc_score = 10;
				}
				
				mDatabase.updateScoreLocation(call_number,loc_score);
			}
			
			// 전화 시간대.
			if (startTime != 0) 
			{
				Date startDate = new Date(startTime);
				
				//위치정보 계산
				boolean Found = mDatabase.existCTimeByPhone(call_number, startDate);

				if(!Found)
				{
					CTimeData ctime = new CTimeData(call_number, startDate);
					mDatabase.insertCTime(ctime);
				}
				
				call_start_score = 10;						
				mDatabase.updateScoreCTime(call_number, call_start_score);							
			}			
			
			
			// 주소 검색
			if(display_name.length() > 0)
			{
				con_add = 1;
				con_score = 6;
			}
				
			if(this.mCallType == RecentCallsData.MISSED_TYPE) call_state_score = 10;
			else call_state_score = 6;
			
	
			if(!mDatabase.existScoreByPhone(call_number))
			{
				score = loc_score + call_count_score + call_start_score + call_dur_score + call_state_score + con_score + fav_score;
						
				//신규 추가
				ContentValues cv = new ContentValues();
				cv.put(ScoreData.FIELD_NUMBER, call_number);
				cv.put(ScoreData.FIELD_SCORE, score);
				cv.put(ScoreData.FIELD_ZONE_ID, 0);
				cv.put(ScoreData.FIELD_ZONE_SCORE, loc_score);
				cv.put(ScoreData.FIELD_CALL_COUNT, call_count);
				cv.put(ScoreData.FIELD_CALL_COUNT_SCORE, call_count_score);
				cv.put(ScoreData.FIELD_CALL_START, endTime);
				cv.put(ScoreData.FIELD_CALL_START_SCORE, call_start_score);
				cv.put(ScoreData.FIELD_CALL_DUR, 1);
				cv.put(ScoreData.FIELD_CALL_DUR_SCORE, call_dur_score);
				cv.put(ScoreData.FIELD_CALL_STATE, call_state);
				cv.put(ScoreData.FIELD_CALL_STATE_SCORE, call_state_score);
				cv.put(ScoreData.FIELD_CON_ADD, con_add);
				cv.put(ScoreData.FIELD_CON_ADD_SCORE, con_score);
				cv.put(ScoreData.FIELD_FAV_ADD, fav_add);
				cv.put(ScoreData.FIELD_FAV_ADD_SCORE, fav_score);
				cv.put(ScoreData.FIELD_CAL_ADD, 0);
				cv.put(ScoreData.FIELD_CAL_ADD_SCORE, 0);
				cv.put(ScoreData.FIELD_F1, 0);
				cv.put(ScoreData.FIELD_F1_SCORE, 0);
				mDatabase.insertScores(cv);
			}
			else
			{
	
				Cursor c = mDatabase.getScoreByPhone(call_number);
				
				if(c != null)
				{
					if(c.getCount() > 0 && c.moveToFirst())
					{
						call_count = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_COUNT));
						call_dur = c.getInt(c.getColumnIndex(ScoreData.FIELD_CALL_DUR));
//						call_start = c.getLong(c.getColumnIndex(ScoreData.FIELD_CALL_START));
					}
					c.close();
				}
				
				//update
				call_count = call_count + 1;
				call_count_score = ScoreUtils.getScoreCallCount(call_count);
				
				call_dur = call_dur + duration;
				call_dur_score = ScoreUtils.getScoreDuration(call_dur);
				
				score = loc_score + call_count_score + call_start_score + call_dur_score + call_state_score + con_score + fav_score;
				
				mDatabase.updateScoreCall(call_number, loc_score, call_count, endTime, call_state, call_dur, con_add, call_count_score, call_start_score, call_dur_score, call_state_score, con_score, score);
			}
		}
	}
	
	
	

}
