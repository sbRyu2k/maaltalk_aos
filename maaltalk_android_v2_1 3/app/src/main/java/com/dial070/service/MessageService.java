package com.dial070.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Base64;

import com.dial070.db.DBManager;
import com.dial070.db.SmsListData;
import com.dial070.db.SmsMsgData;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.ContactHelper;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;

public class MessageService extends Service {
	static String THIS_FILE = "DIAL070_MESSAGESERVICE";
	
	public static final String ACTION_START = "com.dial070.service.MessageService.START";
	public static final String ACTION_NEW_MESSAGE = "com.dial070.service.MessageService.NEW_MESSAGE"; // FOR NOTIFY
	
	private Context mContext;
	private AppPrefs mPrefs;
	private DBManager mDatabase;
	private boolean isMsgProcess=false;
	private boolean isPushMsgProcess=false;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		Log.d(THIS_FILE, "onCreate : begin");
		
		mDatabase = new DBManager(this);
		mContext = this;
		mPrefs = new AppPrefs(this);
	
		if(!mDatabase.isOpen()) mDatabase.open();

		Log.d(THIS_FILE, "onCreate : end");
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onDestroy");
		if (mDatabase != null) 
		{
			if (mDatabase.isOpen())
				mDatabase.close();
			mDatabase = null;
		}
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onBind");
		return null;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onUnbind");
		return super.onUnbind(intent);
	}	


//	@Override
//	public void onStart(Intent intent, int startId) {
//		// TODO Auto-generated method stub
//		Log.d(THIS_FILE, "onStart");
//		super.onStart(intent, startId);
//	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d(THIS_FILE, "onStartCommand");
		onProcess(intent);
		super.onStartCommand(intent, flags, startId);
		return START_NOT_STICKY; // START_STICKY
	}
	
	private void onProcess(Intent intent) {
		Log.d(THIS_FILE, "intent.getAction() : " + intent.getAction());
		if (intent == null) 
			return;
		
		String action = intent.getAction();
		
		if (action == null)
			return;

//		Bundle bundle = intent.getExtras();
//		if(bundle == null)
//		{
//			Log.e(THIS_FILE,"onProcess: Bundle is null!");
//			return;
//		}
		
		if (action.equals(ACTION_START)) {
			reload();
		}
	}
	
	
	
	private Thread reloadThread = null;
	private boolean reloadPending = false; // 동작중 reload 요청이 온 경우를 위해.
	private Thread reloadPushThread = null; // BJH 2016.5.26
	private boolean reloadPushPending = false; // 동작중 reload 요청이 온 경우를 위해.
	private void reload()
	{
		Log.d(THIS_FILE, "reload");
		if (reloadThread != null) 
		{
			reloadPending = true;
			return;			
		}
		
		reloadThread = new Thread()
		{
			public void run()
			{
				Log.d(THIS_FILE, "MESSAGE THREAD START !!");
				
				//reloadPending = true;
				int count = 1;
				while(--count >= 0)
				{
					//reloadPending = false;

					// 	GET DATA
					int rs = loadFromServer();
					if (rs > 0)
					{
						// NOTIFY TO UI
						Intent intent = new Intent(ACTION_NEW_MESSAGE);
						sendBroadcast(intent);
					}
					
					/*try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if (reloadPending)
					{
						reloadPending = false;
						count = 1;
					}	*/
								
				}				
				// END
				reloadThread = null;
				Log.d(THIS_FILE, "MESSAGE THREAD END");
			};
		};
		reloadThread.start();	
		
		//BJH 2016.06.07 수정
		if (reloadPushThread != null) 
		{
			reloadPushPending = true;
			return;			
		}
		
		reloadPushThread = new Thread() //BJH 푸시메시지
		{
			public void run()
			{
				Log.d(THIS_FILE, "PUSH MESSAGE THREAD START !!");
				
				//reloadPushPending = true;
				int count = 1;
				while(--count >= 0)
				{
					//reloadPushPending = false;
					
					int rs_push = loadPushFromServer();
					if (rs_push > 0)
					{
						// NOTIFY TO UI
						Intent intent = new Intent(ACTION_NEW_MESSAGE);
						sendBroadcast(intent);
					}
					
					/*try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if (reloadPushPending)
					{
						reloadPushPending = false;
						count = 1;
					}	*/
								
				}				
				// END
				reloadPushThread = null;
				Log.d(THIS_FILE, "PUSH MESSAGE THREAD END");
			};
		};
		reloadPushThread.start();	
		
	}	
	
	
	// LOAD FROM SERVER
	public int loadFromServer()
	{
		String uid = ServicePrefs.mUser070;
		if (uid == null || uid.length() == 0) 
		{
			Log.d(THIS_FILE, "NEED 070 NUMBER !!");
			return 0;
		}
		
		boolean useMsg = ServicePrefs.mUseMSG;
		if (!useMsg) 
		{
			Log.d(THIS_FILE, "MSG_TYPE != Y");
			return 0;
		}
		
		// MAKE TOKEN
		String str_date = "000000000000000"; // 15 : TS + SEQ
		
		String last_ts = mPrefs.getPreferenceStringValue(AppPrefs.MSG_TS);
		if (last_ts != null && last_ts.length() > 0)
			str_date = last_ts;
		
		String key = uid + str_date;
		//BJH 2016.06.14 문자 보안을 위해서
		key = ServicePrefs.mUserID + key;
		
		Log.d(THIS_FILE, "MSG KEY : " + key);
		
		String token = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP); 
		return requestData(uid, token);
	}
	
	private int toInt(String s)
	{
		int v = 0;
		try
		{
			v =  Integer.parseInt(s);
		}
		catch (Exception e)
		{
		}    
		return v;
	}
	
	private String getLastMsgTS()
	{
		String ts = "000000000000000"; // 15 : TS + SEQ

		
		Cursor c = mDatabase.getLastSmsRecvMsg();
		if(c == null)
		{
			return ts;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			return ts;
		}
		
		int mseq = 0;
		long mtime = 0;
	
		mseq = c.getInt(c.getColumnIndex(SmsMsgData.FIELD_MSG_SEQ));
		mtime = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));

		c.close();
		
		Date d = new Date(mtime);
		
		SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
		String last_ts = _ormatter.format(d);

		// TS + SEQ
		ts = last_ts + String.valueOf(mseq);
		
		return ts;
	}
	
	private void updateMsgList(String callback)
	{
		Cursor c = mDatabase.getLastSmsMsg(callback);
		if(c == null)
		{
			return;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			return;
		}
		
    	String display_name = ContactHelper.getContactsNameByPhoneNumber(mContext, callback);;
    	if (display_name == null) display_name = "";

		String text = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
		long msgDate = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));
		
		c.close();
		
		
		int newCount = mDatabase.getSmsNewMsgCount(callback);
		
		// LIST             			
		if(mDatabase.existSmsList(callback))
		{
			mDatabase.updateSmsList(callback, display_name, text, SmsListData.RECEIVE_TYPE, newCount);
		}
		else
		{
			//내부번호 인지 체크 2:친구목록 
			int userType = SmsListData.USER_TYPE_CONTACT;
			//if(isInternal(callback)) userType = SmsListData.USER_TYPE_BUDDY;
			
			SmsListData sms_list = new SmsListData(userType,callback, "me", SmsListData.RECEIVE_TYPE, text, display_name, msgDate, newCount);
			mDatabase.insertSmsList(sms_list);
		}       		
	}
	
	private int requestData(String uid, String token)
	{
		// SEND QUERY
		Log.d(THIS_FILE, "REQUEST MESSAGE DATA : START");
		
		String rs = postData(token);
		
		Log.d(THIS_FILE, "REQUEST MESSAGE DATA : END");
		Log.d(THIS_FILE, "RESULT : " + rs);		
		if (rs == null || rs.length() == 0) return 0;
		
		//SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
		// BJH 2016.08.26 정확한 문자기록 갱신을 위해서
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
		int count = 0;
		ArrayList<String>senderList = new ArrayList<String>();
		
		
		JSONObject jObject;
		try
		{
		    jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0"))
            {
                JSONObject infoObject = jObject.getJSONObject("MESSAGE_INFO");
                JSONArray itemArray = infoObject.getJSONArray("ITEMS");
                for (int i = 0; i < itemArray.length(); i++) {
                	
                	String mseq = itemArray.getJSONObject(i).getString("MSEQ").toString();
//                	String mtype = itemArray.getJSONObject(i).getString("TYPE").toString();
                	String callback = itemArray.getJSONObject(i).getString("CALLBACK").toString();
                	
                	String cdate = itemArray.getJSONObject(i).getString("INSERT_TIME").toString();
                	String subject = itemArray.getJSONObject(i).getString("SUBJECT").toString();
                	String text = itemArray.getJSONObject(i).getString("TEXT").toString();
                	
//                	String fcount = itemArray.getJSONObject(i).getString("FILECNT").toString();
                	String file1 = itemArray.getJSONObject(i).getString("FILELOC1").toString();
                	String file2 = itemArray.getJSONObject(i).getString("FILELOC2").toString();
                	String file3 = itemArray.getJSONObject(i).getString("FILELOC3").toString();
                	
                	
                	int seq = toInt(mseq);
                	
        			long msgDate = System.currentTimeMillis();
        			if (cdate != null && cdate.length() > 0)
        			{
                 		try
                		{
            				Date d = mFormatter.parse(cdate);
            				msgDate = d.getTime();
            				if(i == 0) { // BJH 2016.08.26 정확한 문자 갱신을 위해서
            					//SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
            					SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss");
            					_ormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
                    			String last_ts = _ormatter.format(d) + mseq;
                				mPrefs.setPreferenceStringValue(AppPrefs.MSG_TS, last_ts);
            				}
                		} catch (Exception e) 
                		{
                			e.printStackTrace();
                		}        				
        			}   
        			
        			// INSERT
             		try
            		{
             			// MSG
            			SmsMsgData msg_data = new SmsMsgData(seq, callback, "me", callback, SmsListData.RECEIVE_TYPE, SmsMsgData.MSG_NEW, subject, text, msgDate);
            			msg_data.setMediaValues(file1, file2, file3);
            			mDatabase.insertSmsMsg(msg_data);
            			count++;
            			
            			if (senderList.indexOf(callback) < 0)
            				senderList.add(callback);
            			
             			
            		} catch (Exception e) 
            		{
            			Log.e(THIS_FILE, "DB", e);
            		}        			
                }	
                
                if(itemArray.length() > 0)
                {
                	Log.d(THIS_FILE, "DATA SIZE ="+ Integer.toString(itemArray.length()));

                }
            }
            else
            {
            	Log.d(THIS_FILE, responseObject.getString("RES_TYPE") + "ERROR ="+ responseObject.getString("RES_DESC"));
            }
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return 0;
		}		
		
		
		if (count > 0)
		{
			// GET LAST TIMESTAMP
    		/*try
    		{
    			String last_ts = getLastMsgTS();
    			Log.d(THIS_FILE, "MSG DATE : " + last_ts);			
    			mPrefs.setPreferenceStringValue(AppPrefs.MSG_TS, last_ts);
    		} 
    		catch (Exception e) 
    		{
    			Log.e(THIS_FILE, "DB", e);
    		}*/
    		
    		
    		// UPDFATE MESSAGE LIST
    		try
    		{
	    		for(String callback : senderList)
	    		{
	    			updateMsgList(callback);
	    		}
    		} 
    		catch (Exception e) 
    		{
    			Log.e(THIS_FILE, "DB", e);
    		}
	    		
		}

		return count;
	}
	
	private String postData(String token)
	{
		String url_cdr_info = mPrefs.getPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO);
		String recents_url = url_cdr_info + "?token="+token;
		Log.i(THIS_FILE,"msg_url:"+recents_url);
		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(mContext);
			HttpPost httppost = new HttpPost(recents_url);
			
			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String line = null;
			StringBuilder sb = new StringBuilder();			
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}	
	//BJH 마지막 푸시 시간 오류 수정
	public int loadPushFromServer()
	{
		String uid = ServicePrefs.mUserID;
		if(uid == null || uid.length() == 0) 
		{
			return 0;
		}
			
		// MAKE TOKEN
		String str_date = "00000000000000"; // 14 : TS
			
		String last_ts = mPrefs.getPreferenceStringValue(AppPrefs.PUSH_TS);
		if (last_ts != null && last_ts.length() > 0)
			str_date = last_ts;
			
		String key = uid + str_date;
			
		Log.d(THIS_FILE, "PUSH KEY : " + key);
			
		String token = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP); 
		return requestPushData(uid, token);
	}	
	
	private int requestPushData(String uid, String token)
	{
		// SEND QUERY
		Log.d(THIS_FILE, "REQUEST MESSAGE DATA : START");
		
		String rs = postPushData(token);
		
		Log.d(THIS_FILE, "REQUEST MESSAGE DATA : END");
		Log.d(THIS_FILE, "RESULT : " + rs);		
		if (rs == null || rs.length() == 0) return 0;
		
		//SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
		// BJH 2016.08.26 정확한 문자기록 갱신을 위해서
		SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
		int count = 0;
		ArrayList<String>senderList = new ArrayList<String>();
		
		
		JSONObject jObject;
		try
		{
		    jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
            if (responseObject.getString("RES_CODE").equalsIgnoreCase("0"))
            {
                JSONObject infoObject = jObject.getJSONObject("PUSH_MSG_INFO");
                JSONArray itemArray = infoObject.getJSONArray("ITEMS");
                for (int i = 0; i < itemArray.length(); i++) {
                	
                	String mseq = itemArray.getJSONObject(i).getString("JID").toString();
                	//String callback = itemArray.getJSONObject(i).getString("CALLBACK").toString();
                	String callback = "PUSH";
                	String cdate = itemArray.getJSONObject(i).getString("PUSH_DATE").toString();
                	//String subject = itemArray.getJSONObject(i).getString("SUBJECT").toString();
                	String subject = mContext.getResources().getString(R.string.push_msg);
                	String contents = itemArray.getJSONObject(i).getString("CONTENTS").toString();
                	String param1 = itemArray.getJSONObject(i).getString("PARAM1").toString();
                	String text = contents + "\n" + param1;
                	
                	String file1 = itemArray.getJSONObject(i).getString("PARAM2").toString();
                	String file2 = "";
                	String file3 = "";
                	
                	int seq = toInt(mseq);
                	
        			long PushDate = System.currentTimeMillis();
        			if (cdate != null && cdate.length() > 0)
        			{
                 		try
                		{
            				Date d = mFormatter.parse(cdate);
            				PushDate = d.getTime();
            				if(i == 0) { // BJH 2016.08.26 정확한 문자 갱신을 위해서
            					//SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
            					SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss");
            					_ormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));//BJH 2016.08.26
                    			String last_ts = _ormatter.format(d) + mseq;
                				mPrefs.setPreferenceStringValue(AppPrefs.PUSH_TS, last_ts);
            				}
                		} catch (Exception e) 
                		{
                			e.printStackTrace();
                		}        				
        			}   
        			
        			// INSERT
             		try
            		{
             			// PUSH
            			SmsMsgData msg_data = new SmsMsgData(seq, callback, "me", callback, SmsListData.RECEIVE_TYPE, SmsMsgData.MSG_NEW, subject, text, PushDate);
            			msg_data.setMediaValues(file1, file2, file3);
            			mDatabase.insertSmsMsg(msg_data);
            			count++;
            			
            			if (senderList.indexOf(callback) < 0)
            				senderList.add(callback);
            			
             			
            		} catch (Exception e) 
            		{
            			Log.e(THIS_FILE, "DB", e);
            		}        			
                }	
                
                if(itemArray.length() > 0)
                {
                	Log.d(THIS_FILE, "DATA SIZE ="+ Integer.toString(itemArray.length()));

                }
            }
            else
            {
            	Log.d(THIS_FILE, responseObject.getString("RES_TYPE") + "ERROR ="+ responseObject.getString("RES_DESC"));
            }
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return 0;
		}		
		
		
		if (count > 0)
		{	
			// GET LAST TIMESTAMP
    		/*try
    		{
    			String last_ts = getLastPush();
    			Log.d(THIS_FILE, "PUSH DATE : " + last_ts);			
    			mPrefs.setPreferenceStringValue(AppPrefs.PUSH_TS, last_ts);
    		} 
    		catch (Exception e) 
    		{
    			Log.e(THIS_FILE, "DB", e);
    		}*/
    		
    		// UPDFATE MESSAGE LIST
    		try
    		{
	    		for(String callback : senderList)
	    		{
	    			updatePushMsgList(callback);
	    		}
    		} 
    		catch (Exception e) 
    		{
    			Log.e(THIS_FILE, "DB", e);
    		}
	    		
		}

		return count;
	}
	
	private String postPushData(String token)
	{
		String url_push_info = mPrefs.getPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO);
		String recents_url = url_push_info + "?token="+token;
		Log.i(THIS_FILE,"push_msg_url:"+recents_url);
		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(mContext);
			HttpPost httppost = new HttpPost(recents_url);
			
			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String line = null;
			StringBuilder sb = new StringBuilder();			
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}	
	
	private void updatePushMsgList(String callback)
	{
		Cursor c = mDatabase.getLastSmsMsg(callback);
		if(c == null)
		{
			return;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			return;
		}
		
    	String display_name = ContactHelper.getContactsNameByPhoneNumber(mContext, callback);;
    	if (display_name == null) display_name = "";

		String text = c.getString(c.getColumnIndex(SmsMsgData.FIELD_MSG));
		long msgDate = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));
		
		c.close();
		
		
		int newCount = mDatabase.getSmsNewMsgCount(callback);
		
		// LIST             			
		if(mDatabase.existSmsList(callback))
		{
			mDatabase.updateSmsList(callback, display_name, text, SmsListData.RECEIVE_TYPE, newCount);
		}
		else
		{
			//내부번호 인지 체크 2:친구목록 
			int userType = SmsListData.USER_TYPE_PUSH;
			//if(isInternal(callback)) userType = SmsListData.USER_TYPE_BUDDY;
			
			SmsListData sms_list = new SmsListData(userType,callback, "me", SmsListData.RECEIVE_TYPE, text, display_name, msgDate, newCount);
			mDatabase.insertSmsList(sms_list);
		}       		
	}
	
	private String getLastPush()//BJH LAST PUSH DATE
	{
		String ts = "000000000000000"; // 14 : TS

		
		Cursor c = mDatabase.getLastPushRecvMsg();
		if(c == null)
		{
			return ts;
		}
		
		if(!c.moveToFirst())
		{
			c.close();
			return ts;
		}
		
		long mtime = 0;
	
		mtime = c.getLong(c.getColumnIndex(SmsMsgData.FIELD_DATE));

		c.close();
		
		Date d = new Date(mtime);
		
		SimpleDateFormat _ormatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.getDefault());
		String last_ts = _ormatter.format(d);
		
		return last_ts;
	}
	
}
