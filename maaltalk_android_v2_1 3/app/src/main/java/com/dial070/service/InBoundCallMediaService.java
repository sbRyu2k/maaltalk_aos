package com.dial070.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;
import android.telecom.Call;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dial070.App;
import com.dial070.DialMain;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.ISipService;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.models.CallerInfo;
import com.dial070.sip.pjsip.PjSipService;
import com.dial070.sip.service.SipService;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.sip.utils.bluetooth.BluetoothWrapper;
import com.dial070.status.CallStatus;
import com.dial070.status.WaitingStatus;
import com.dial070.ui.CallStat;
import com.dial070.ui.InCall2;
import com.dial070.utils.DebugLog;
import com.dial070.utils.NotiUtils;
import com.dial070.utils.ServiceUtils;
import com.dial070.view.entitiy.CallActionData;
import com.dial070.view.entitiy.CallData;
import com.dial070.view.notification.CallingNotification;
import com.dial070.view.notification.IncomingCallInvisibleNotification;
import com.dial070.view.notification.IncomingCallVisibleNotification;
import com.dial070.view.notification.ProcessCallNotification;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class InBoundCallMediaService extends Service implements View.OnKeyListener {

    public @interface Operations {
        String ACTION_OPERATION_CODE = "operation_code";
        String ACTION_OPERATION_SNOOZE_START = "snooze_start";
    }

    private PowerManager.WakeLock mWakeLock = null;

    private PhoneStateChangeReceiver mNativePhoneReceiver = null;
    private NotificationActionReceiver mNotificationActReceiver = null;

    private AudioManager mAudioManager;
    private PreferencesWrapper mPrefsWrapper;


    private ISipService mSipService = null;
    private ServiceConnection mSipConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mSipService = ISipService.Stub.asInterface(service);
            try {
                DebugLog.d("SipConnection onServiceConnected componentName --> "+name);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            DebugLog.d("SipConnection onServiceDisconnected componentName --> "+name);
            mSipService = null;
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        DebugLog.d("onBind called");
        return null;
    }

    @Override
    public void onCreate() {
        DebugLog.d("onCreate called");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            DebugLog.d("IBCS onStartCommand current call state --> "+CallStatus.INSTANCE.getCurrentStatus());
            String cid = "";

            if(intent==null) {
                return START_NOT_STICKY;
            }

            if(intent!=null && intent.hasExtra("cid")) {
                cid = intent.getStringExtra("cid");
            }

            FirebaseCrashlytics.getInstance().log("InBoundCallMediaService onStartCommand called. cid: "+cid);

            int operationCode = 0;
            if(intent!=null && intent.hasExtra(Operations.ACTION_OPERATION_CODE)) {
                operationCode = intent.getIntExtra(Operations.ACTION_OPERATION_CODE, 0);
            }

            boolean isSnoozeStart = false;
            if(intent!=null && intent.hasExtra(Operations.ACTION_OPERATION_SNOOZE_START)) {
                isSnoozeStart = intent.getBooleanExtra(Operations.ACTION_OPERATION_SNOOZE_START, false);
            }

            DebugLog.d("IBCS onStartCommand operationCode --> "+operationCode);
            DebugLog.d("IBCS onStartCommand snooze condition current --> "+isSnoozeStart);
            DebugLog.d("IBCS onSTartCommand App.isDailMainForeground --> "+App.isDialMainForeground);

            if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY) {
                DebugLog.d("IBCS onStartCommand Already incoming call exist --> escape condition");
                return START_NOT_STICKY;
//                return START_STICKY;
            }

            if(App.isDialMainForeground) {
                DebugLog.d("IBCS onStartCommand Service Already running --> escape condition");
                if(isSnoozeStart==false) {
                    return START_NOT_STICKY;
//                    return START_STICKY;
                }
            }

            if(operationCode==0) {
                PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
                mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getPackageName());
                mWakeLock.acquire(10000);

                showStartUpNotification(intent, cid);
                initMediaServiceResources();
                startServices();

                if(isSnoozeStart) {
                    visibleNotification(intent, cid);
                } else {
                    if(CallStatus.INSTANCE.getCurrentStatus()!=SipCallSession.InvState.EARLY) {

                    }
                }
                CallStatus.INSTANCE.updateCallAction(new CallActionData(1));
                CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.EARLY, 0, cid));

            } else {
                DebugLog.d("IBCS onStartCommand operationCode 11 stop ring from fcm.stopRing");
            }

            /**
             * mod
             */
            if(!ServiceUtils.isRunningService(this, SipService.class)) {
                startService(new Intent(this, SipService.class));

            }
//            startService(new Intent(this, SipService.class));
            bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);

        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }
//        if(!ServiceUtils.isRunningService(this, SipService.class)) {
//            startService(new Intent(this, SipService.class));
//        }
//        bindService(new Intent(this, SipService.class), mSipConnection, Context.BIND_AUTO_CREATE);

        return START_NOT_STICKY;
//        return START_STICKY;
    }

    private void showStartUpNotification(Intent intent, String cid) {
        try {
            boolean isSnoozeStart = false;
            if(intent!=null && intent.hasExtra(Operations.ACTION_OPERATION_SNOOZE_START)) {
                isSnoozeStart = intent.getBooleanExtra(Operations.ACTION_OPERATION_SNOOZE_START, false);
            }

            DebugLog.d("IBCS showStartUpNotification isSnoozeStart --> "+isSnoozeStart);
            CallStatus.INSTANCE.updatePushCallNumber(cid);

            Notification notification = null;

            String title = "전화수신요청";
            String desc = "";

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            PendingIntent fullScreenPendingIntent = NotiUtils.createFullScreenCallPendingIntent(this, cid);
            Notification incallNotification = null;

            if(Build.VERSION.SDK_INT >= 29) {
                incallNotification = NotiUtils.createCallNotification(
                        this,
                        fullScreenPendingIntent,
                        NotiUtils.Channel.CALL_CHANNEL_ID,
                        title,
                        cid,
                        R.drawable.noti_inbound,
                        true,
                        true
                );

            } else {
                incallNotification = NotiUtils.createCallNotification(
                        this,
                        null,
                        NotiUtils.Channel.CALL_CHANNEL_ID,
                        title,
                        cid,
                        R.drawable.noti_inbound,
                        false,
                        true
                );
            }

            startForeground(12345, incallNotification);
//            notificationManager.notify(12345, incallNotification);

            if(isSnoozeStart) {
                visibleNotification(intent, desc);
            } else {
                if(Build.VERSION.SDK_INT >= 29) {
                    underQOverrideNotification(intent, desc);
                } else {
                    underQOverrideNotification(intent, desc);
                    ServicePrefs.mPush=true;
                    Intent IntroIntent = new Intent(this, DialMain.class);
                    IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    IntroIntent.putExtra("NO_DELAY", true);
                    IntroIntent.putExtra(DialMain.EXTRA_PUSH, true);
//                    this.startActivity(IntroIntent);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
//            stopSelf();
        }
    }

    private void showStartUpWithInviteNotification() {
        try {
            Notification notification = null;

            String title = "전화수신요청";
            String desc = "";
            String cid = CallStatus.INSTANCE.getCurrentCallNumber();

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            PendingIntent fullScreenPendingIntent = NotiUtils.createFullScreenCallPendingIntent(this, cid);
            Notification incallNotification = null;

            if(Build.VERSION.SDK_INT >= 29) {
                incallNotification = NotiUtils.createCallNotification(
                        this,
                        fullScreenPendingIntent,
                        NotiUtils.Channel.CALL_CHANNEL_ID,
                        title,
                        cid,
                        R.drawable.noti_inbound,
                        true,
                        true
                );

            } else {
                incallNotification = NotiUtils.createCallNotification(
                        this,
                        null,
                        NotiUtils.Channel.CALL_CHANNEL_ID,
                        title,
                        cid,
                        R.drawable.noti_inbound,
                        false,
                        true
                );

//                startForeground(12345, incallNotification);

//                IncomingCallVisibleNotification incomingCallVisibleNotification = new IncomingCallVisibleNotification();
//                incallNotification = incomingCallVisibleNotification.createNotification(this, null, NotiUtils.Channel.INFO_CHANNEL_ID, title, desc, R.drawable.noti_inbound);
//                NotiUtils.createChannel(this);
//                incallNotification.flags = Notification.FLAG_ONLY_ALERT_ONCE;

            }

//            startForeground(12345, incallNotification);
            notificationManager.notify(12345, incallNotification);

//            notificationManager.notify(12345, incallNotification);
//            startForeground(12345, incallNotification);

        } catch (Exception e) {
            e.printStackTrace();
//            stopSelf();
        }
    }

    /**
     * 통화중으로 변경된 상태 노티피케이션
     * @param intent
     * @param cid
     */
    private void updateStartUpNotification(Intent intent, String cid) {     // 통화중
        try {
            Notification notification = null;
//        intent = new Intent(this, DialMain.class);//BJH Splash -> Main
//        intent.putExtra(DialMain.START_FOREGROUND, 0);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        //stackBuilder.addParentStack(DialMain.class);
//        stackBuilder.addNextIntent(intent);

            String title = "통화중";
            String desc = cid;

            if(cid.equals("")) {
                desc = CallStatus.INSTANCE.getCurrentCallNumber();
            }

//        PendingIntent resultPendingIntent =
//                stackBuilder.getPendingIntent(
//                        0,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                );
//        notification = MarshmallowResponse.createSFSNotification(this, null, NotiUtils.Channel.CALL_CHANNEL_ID, title, desc, R.drawable.noti_inbound);

            CallingNotification callingNotification = new CallingNotification();
            notification = callingNotification.createNotification(
                    this,
                    null,
                    NotiUtils.Channel.INFO_CHANNEL_ID,
                    title,
                    desc,
                    R.drawable.noti_inbound
            );

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            notification.flags = Notification.FLAG_ONLY_ALERT_ONCE;
            notificationManager.notify(12345, notification);

        } catch (Exception e) {
            e.printStackTrace();
//            stopSelf();
        }
    }

    private void underQOverrideNotification(Intent intent, String cid) {
        try {
            DebugLog.d("underQOverride Notification called...");

            Notification notification = null;
            String title = "전화 수신 요청을 처리중.";
            String desc = "연결이 완료되면 전화 수신화면으로 이동합니다.";

//        if(intent!=null && intent.hasExtra("cid")) {
//            desc = intent.getStringExtra("cid");
//        }
//
//        if(desc.equals("")) {
//            desc = CallStatus.INSTANCE.getCurrentCallNumber();
//        }

            ProcessCallNotification processCallNotification = new ProcessCallNotification();
            notification = processCallNotification.createNotification(this, null, NotiUtils.Channel.INFO_CHANNEL_ID, title, desc, R.drawable.noti_inbound);

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            notification.flags = Notification.FLAG_ONLY_ALERT_ONCE;

            notificationManager.notify(12345, notification);

//            IncomingCallVisibleNotification incomingCallVisibleNotification = new IncomingCallVisibleNotification();
//            notification = incomingCallVisibleNotification.createNotification(this, null, NotiUtils.Channel.INFO_CHANNEL_ID, title, desc, R.drawable.noti_inbound);
//
//            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
//            NotiUtils.createChannel(this);
//            notification.flags = Notification.FLAG_ONLY_ALERT_ONCE;
//
//            notificationManager.notify(12345, notification);

        } catch (Exception e) {
            e.printStackTrace();
//            stopSelf();
        }
    }

    /**
     * 앱이 구동중(visible) 상태에서 전화 수신 요청이 왔을 경우 보여주는 노티피케이션
     * 앱이 구동중(invisible-->visible) 상태 변화 시 보여주는 노티피케이션
     * @param intent
     * @param cid
     */
    private void visibleNotification(Intent intent, String cid) {
        try {
            DebugLog.d("visibleNotification called...");
            Notification notification = null;
            String title = "전화수신요청";
            String desc = "";

            if(intent!=null && intent.hasExtra("cid")) {
                desc = intent.getStringExtra("cid");
            }

            if(desc.equals("")) {
                desc = CallStatus.INSTANCE.getCurrentCallNumber();
            }

            IncomingCallVisibleNotification incomingCallVisibleNotification = new IncomingCallVisibleNotification();
            notification = incomingCallVisibleNotification.createNotification(this, null, NotiUtils.Channel.INFO_CHANNEL_ID, title, desc, R.drawable.noti_inbound);

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            notification.flags = Notification.FLAG_ONLY_ALERT_ONCE;

            notificationManager.notify(12345, notification);
        } catch (Exception e) {
            e.printStackTrace();
//            stopSelf();
        }
    }

    /**
     * 앱이 구종중(visible-->invisible) 상태변화 시 보여주는 노티피케이션
     * @param intent
     */
    private void invisibleNotification(Intent intent) {
        try {
            String title = "전화수신요청";
            String desc = "";

            if(intent!=null && intent.hasExtra("cid")) {
                desc = intent.getStringExtra("cid");
            }

            if(desc.equals("")) {
                desc = CallStatus.INSTANCE.getCurrentCallNumber();
            }

            PendingIntent fullScreenPendingIntent = NotiUtils.createFullScreenCallPendingIntent(this, CallStatus.INSTANCE.getCurrentCallNumber());
            IncomingCallInvisibleNotification incomingCallInVisibleNotification = new IncomingCallInvisibleNotification();
            Notification inCallNotification = incomingCallInVisibleNotification.createNotification(
                    this,
                    fullScreenPendingIntent,
                    NotiUtils.Channel.CALL_CHANNEL_ID,
                    title,
                    desc,
                    R.drawable.noti_inbound);

            NotificationManager notificationManager = NotiUtils.getNotificationManager(this);
            NotiUtils.createChannel(this);
            notificationManager.notify(12345, inCallNotification);
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }
    }

    private void initMediaServiceResources() {
        mPrefsWrapper = new PreferencesWrapper(getApplicationContext());

        mNativePhoneReceiver = new PhoneStateChangeReceiver();
        mNotificationActReceiver = new NotificationActionReceiver();
    }

    private void startServices() {
        registerReceiver(mNativePhoneReceiver, new IntentFilter("android.intent.action.PHONE_STATE"));
        IntentFilter filter = new IntentFilter("in_call_notification_answer");
        filter.addAction("in_call_notification_decline");
        filter.addAction("in_call_notification_answer_decline");
        filter.addAction("in_call_notification_confirmed");
        filter.addAction("in_call_notification_application_active");
        filter.addAction("in_call_notification_exit_visible");
        /**
         * TEST
         */
        filter.addAction("treat_incoming_call");
        filter.addAction("invalid_in_call_state_remove");
        registerReceiver(mNotificationActReceiver, filter);
    }

    private void stopService() {
        if(mNativePhoneReceiver != null) {
            unregisterReceiver(mNativePhoneReceiver);
        }

        if(mNotificationActReceiver != null) {
            unregisterReceiver(mNotificationActReceiver);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        DebugLog.d("onTaskRemoved called --> ");
        stopService();
        stopForeground(true);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        DebugLog.d("onDestroy called");
        try{
            if(SipService.pjService!=null || SipService.pjService.mediaManager!=null) {
                if(SipService.pjService.mediaManager.isRinging()){
                    SipService.pjService.mediaManager.stopRing();
                }
            }

            stopService();
            CallStatus.INSTANCE.update(new CallData(SipCallSession.InvState.NULL, 0, ""));
            CallStatus.INSTANCE.updatePushCallNumber("");
            CallStatus.INSTANCE.updateCallAction(new CallActionData(0));
            WaitingStatus.callWaitQueue.clear();

            unbindService(mSipConnection);
            mSipService = null;

            stopForeground(true);
            if(mWakeLock!=null) {
                mWakeLock.release();
                mWakeLock = null;
            }

        } catch (Exception e) {

        }

        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        DebugLog.d("onLowMemory called");
        super.onLowMemory();
    }

    private class NotificationActionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String cid = "";

            if(intent.hasExtra("cid")) {
                cid = intent.getStringExtra("cid");
            }

            DebugLog.d("notification receiver cid --> "+cid);

            if(action.equals("in_call_notification_decline")) {
                DebugLog.d("notification action received --> in_call_notification_decline");
                DebugLog.d("notification action received callId --> "+CallStatus.INSTANCE.getCurrentCallId());
//                stopAnnouncing();

                try {
//                    mSipService.hangup(-1, 0);
                    Thread hangupThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mSipService.hangup(-1, 0);
                                stopService(new Intent(InBoundCallMediaService.this, SipService.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    hangupThread.start();

//                    if(SipService.pjService.mediaManager!=null) {
//                        SipService.pjService.mediaManager.stopRing();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    stopSelf();
                }
                stopSelf();

            } else if( action.equals("in_call_notification_answer_decline")) {
                try {
                    DebugLog.d("notification action received --> in_call_notification_answer_decline");

                    mSipService.answer(CallStatus.INSTANCE.getCurrentCallId(), SipCallSession.StatusCode.DECLINE);
                } catch (Exception e) {
                    e.printStackTrace();
                    stopSelf();
                }
                stopSelf();

            } else if(action.equals("in_call_notification_answer")) {
                DebugLog.d("in_call_notification_answer");

            } else if(action.equals("in_call_notification_confirmed")) {
                DebugLog.d("in_call_notification_confirmed");
                updateStartUpNotification(intent, cid);

            } else if(action.equals("in_call_notification_application_active")) {
                DebugLog.d("in_call_notification_application_active");
                visibleNotification(intent, cid);

            } else if(action.equals("in_call_notification_exit_visible")) {
                DebugLog.d("in_call_notification_exit_visible");
                invisibleNotification(intent);

            } else if(action.equals("in_call_notification_received_bye")) {
                DebugLog.d("in_call_notification_received_bye");
//                stopRing();
//                stopAnnouncing();
            } else if(action.equals("treat_incoming_call")) {
                DebugLog.d("treat_incoming_call");
                showStartUpWithInviteNotification();

            } else if(action.equals("invalid_in_call_state_remove")) {
                DebugLog.d("invalid_in_call_state_remove");
                stopForeground(true);
            }
        }
    }

    private class PhoneStateChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(TELEPHONY_SERVICE);
            NativePhoneStateListener phoneStateListener = new NativePhoneStateListener();
            telephonyManager.listen(phoneStateListener, phoneStateListener.LISTEN_CALL_STATE);
        }
    }

    private class NativePhoneStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
//            DebugLog.d("native   state --> "+state+" , incomingNumber : "+phoneNumber);
//            DebugLog.d("internet state --> "+ CallStatus.INSTANCE.getCurrentStatus());

            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    if(App.isDialMainForeground) {
                        return;
                    }
                    if(CallStatus.INSTANCE.getCurrentStatus()== SipCallSession.InvState.EARLY ||
                            CallStatus.INSTANCE.getCurrentStatus() == SipCallSession.InvState.INCOMING) {
                        try{
                            mSipService.hangup(-1, 0);
                            stopService(new Intent(InBoundCallMediaService.this, SipService.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        stopSelf();

                    }
                    break;
            }
            CallStatus.INSTANCE.setNativeStatus(state);
        }
    }

    private SipCallSession getCurrentCallInfo() {
        try {
            DebugLog.d("getCurrentCallInfo()");

            SipCallSession currentCallInfo = null;
            SipCallSession[] callsInfo = null;
            try {
                callsInfo = mSipService.getCalls();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            if (callsInfo == null) {
                return null;
            }

            DebugLog.d("callsInfo size --> "+callsInfo.length);

            long callConnectStart = 0;
            for (SipCallSession callInfo : callsInfo) {
                if (callInfo.isActive()) {
                    if (callConnectStart < callInfo.getConnectStart()) {
                        currentCallInfo = callInfo;
                        callConnectStart = callInfo.getConnectStart();
                    }
                }
            }
            return currentCallInfo;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:

                int action = AudioManager.ADJUST_RAISE;
                if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    action = AudioManager.ADJUST_LOWER;
                }
                SipCallSession currentCallInfo = getCurrentCallInfo();
                if(mSipService!=null) {
                    try {
                        mSipService.adjustVolume(currentCallInfo, action, AudioManager.FLAG_SHOW_UI);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } else {
                }
                return true;
        }
        return false;
    }
}
