package com.dial070.service;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.dial070.bridge.utils.BridgeMediaManager;
import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipManager;
import com.dial070.sip.service.SipNotifications;
import com.dial070.ui.MarshmallowResponse;
import com.dial070.ui.TransparentActivity;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class BridgePopupService extends Service {
	private static final String THIS_FILE = "BridgePopupService";
	public BridgeMediaManager mediaManager = null;
	private Handler setMediaHandler = null;
	private Runnable setMediaRunnable = null;
	private Notification inCallNotification = null;
	private Notification missedCallNotification = null;
	private NotificationManager notificationManager = null;
	private static final int BRIDGE_NOTIF_ID = 3001;
	private static final int CALL_NOTIF_ID = BRIDGE_NOTIF_ID + 1;
	private NotificationManager mNotificationManager = null;

	private View mView; // 항상 보이게 할 뷰
	private WindowManager mManager;
	private WindowManager.LayoutParams mParams;
	private int mId;
	private TextView txtTime, txtDesc;
	private ImageButton mButtonClose;
	private Handler autoCloseHandler = null;
	private Runnable autoCloseRunnable = null;

	private float START_X, START_Y; // 움직이기 위해 터치한 시작 점
	private int PREV_X, PREV_Y; // 움직이기 이전에 뷰가 위치한 점
	private int MAX_X = -1, MAX_Y = -1; // 뷰의 위치 최대 값
	private Context mContext;

	private TelephonyManager mTelephonyManager = null;
	private Long mExpired_time = 60000L;
	private Long mMedia_time = 1000L;
	private static final int CALL_INCOMING = 1;
	private static final int CALL_CONNECTING = 2;
	private static final int CALL_DISCONNECTED = 3;
	private boolean mIncoming = false;
	private PowerManager.WakeLock wakeLock = null;
	private boolean mWakeLock = false;
	private PowerManager powerManager = null;

	private String mNumber,mDisplay;

	private int count=0;

	// 전화번호
	private static String phoneNumber = "";

	// incoming 수신 플래그
	private static boolean incomingFlag = false;
	private static boolean callBridgeFlag = false;

	private void setAutoClose(long delayMS) {
		cancelAutoClose();
		autoCloseRunnable = new Runnable() {
			@Override
			public void run() {
				mNotificationManager.cancel(mId);
				mNotificationManager = null;

				showNotificationForMissedCall(mContext,mDisplay);

				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				startActivity(intent);
				stopService();
				//txtDesc.setText("말톡브릿지 부재중 전화가 있습니다.");
			}
		};
		autoCloseHandler = new Handler();
		autoCloseHandler.postDelayed(autoCloseRunnable, delayMS);
	}

	private void cancelAutoClose() {
		if (autoCloseHandler != null) {
			if (autoCloseRunnable != null) {
				autoCloseHandler.removeCallbacks(autoCloseRunnable);
				autoCloseRunnable = null;
			}
			autoCloseHandler = null;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(THIS_FILE,"onStartCommand");
		mId = intent.getIntExtra("ID", 0);
		final long now=intent.getLongExtra("TIME",0);
		mNumber = intent.getStringExtra("CID");
		mDisplay = intent.getStringExtra("NAME");
		final String dial_number = intent.getStringExtra("dial_number");

		Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + dial_number));
		mContext.startActivity(intent2);

        SimpleDateFormat mFormatter = new SimpleDateFormat("aa hh:mm");
        mFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        String time=mFormatter.format(now);
        txtTime.setText(time);

		pushResponse("START");

		/*Handler delayHandler = new Handler();
		delayHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent1 = new Intent(mContext, TransparentActivity.class);
				intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
						| Intent.FLAG_ACTIVITY_CLEAR_TASK
						| Intent.FLAG_ACTIVITY_NEW_TASK);
				intent1.putExtra("TIME", now);
				intent1.putExtra("dial_number", dial_number);
				intent1.putExtra("NAME", mDisplay);
				mContext.startActivity(intent1);
			}
		}, 1000);*/

		return START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this; //2016.11.10

		KeyguardManager km = (KeyguardManager) mContext.getSystemService(Context.KEYGUARD_SERVICE);
		KeyguardManager.KeyguardLock keyLock = km.newKeyguardLock(Context.KEYGUARD_SERVICE);
		keyLock.disableKeyguard();

		LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.popup_bridge_floating, null);
		mView.setOnTouchListener(mViewTouchListener);

		mTelephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);
		mTelephonyManager.listen(mPhoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			mParams = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
							| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
					PixelFormat.TRANSLUCENT);
		} else {
			mParams = new WindowManager.LayoutParams(
					WindowManager.LayoutParams.MATCH_PARENT,
					WindowManager.LayoutParams.WRAP_CONTENT,
					WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
							| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
					PixelFormat.TRANSLUCENT);
		}
		mParams.gravity = Gravity.TOP | Gravity.LEFT;

		/*mParams = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
				PixelFormat.TRANSLUCENT);

		// params.gravity = Gravity.RIGHT | Gravity.TOP;
		mParams.gravity = Gravity.TOP | Gravity.LEFT;*/

		mManager = (WindowManager) getSystemService(WINDOW_SERVICE);

		mNotificationManager = (NotificationManager) this
				.getSystemService(android.content.Context.NOTIFICATION_SERVICE);

		mManager.addView(mView, mParams);
		mNotificationManager = (NotificationManager) this
				.getSystemService(android.content.Context.NOTIFICATION_SERVICE);


		ImageView imgClose=mView.findViewById(R.id.imgClose);
		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancelAutoClose();
				stopService();
			}
		});

        txtTime= (TextView) mView.findViewById(R.id.txtTime);
        txtDesc= (TextView) mView.findViewById(R.id.txtDesc);
		
		mButtonClose = (ImageButton) mView.findViewById(R.id.mto_close);

		mButtonClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mNotificationManager.cancel(mId);
				mNotificationManager = null;

				cancelAutoClose();
				stopService();				
			}
		});

		//setMediaStart(mContext, mMedia_time);
		setAutoClose(60000);
	}
	
	private void stopService() {
		stopService(new Intent(BridgePopupService.this, BridgePopupService.class));
	}

	public void showNotificationForMissedCall(Context context, String displayName) {
		if(notificationManager == null) {
			notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
			if (Build.VERSION.SDK_INT >= 26) {
				int importance = NotificationManager.IMPORTANCE_DEFAULT;
				NotificationChannel channel = new NotificationChannel(Fcm.NEW_CHANNEL_ID, getString(R.string.app_name), importance);
				notificationManager.createNotificationChannel(channel);
			}
		}

		int icon = R.drawable.noti_missed;
		CharSequence tickerText =  context.getText(R.string.missed_call);
		long when = System.currentTimeMillis();

		Intent notificationIntent = new Intent(SipManager.ACTION_SIP_CALLLOG);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		missedCallNotification = MarshmallowResponse.createNotification(context, contentIntent, Fcm.NEW_CHANNEL_ID, tickerText.toString(), displayName, icon);

		notificationManager.notify(SipNotifications.CALLLOG_NOTIF_ID, missedCallNotification);
	}
	
	private View.OnTouchListener mViewTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: // 사용자 터치 다운이면
				// Log2.i("ACTION_DOWN");
				if (MAX_X == -1)
					setMaxPosition();
				START_X = event.getRawX(); // 터치 시작 점
				START_Y = event.getRawY(); // 터치 시작 점
				PREV_X = mParams.x; // 뷰의 시작 점
				PREV_Y = mParams.y; // 뷰의 시작 점
				break;
			case MotionEvent.ACTION_MOVE:
				// Log2.i("ACTION_MOVE");
				int x = (int) (event.getRawX() - START_X); // 이동한 거리
				int y = (int) (event.getRawY() - START_Y); // 이동한 거리

				// 터치해서 이동한 만큼 이동 시킨다
				mParams.x = PREV_X + x;
				mParams.y = PREV_Y + y;

				// optimizePosition(); //뷰의 위치 최적화
				mManager.updateViewLayout(mView, mParams); // 뷰 업데이트
				break;
			case MotionEvent.ACTION_UP:
				float diffX = event.getRawX() - START_X;
				float diffY = event.getRawY() - START_Y;
				if (Math.abs(diffX) < 30.0 && Math.abs(diffY) < 30.0) {
					//
				}
				break;
			}

			return false;
		}
	};

	private void setMaxPosition() {
		DisplayMetrics matrix = new DisplayMetrics();
		mManager.getDefaultDisplay().getMetrics(matrix); // 화면 정보를 가져와서

		MAX_X = matrix.widthPixels - mView.getWidth(); // x 최대값 설정
		MAX_Y = matrix.heightPixels - mView.getHeight(); // y 최대값 설정
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mView != null) // 서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
		{
			((WindowManager) getSystemService(WINDOW_SERVICE))
			.removeView(mView);
			mView = null;
		}
	}
	
	//BJH 2016.11.10
	private void pushResponse(String action) {
		long response_time = System.currentTimeMillis();
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			AppPrefs prefs = new AppPrefs(mContext);
			String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
			TelephonyManager telManager = (TelephonyManager) mContext.getSystemService( Context.TELEPHONY_SERVICE);
			String number;

			if(Build.VERSION.SDK_INT<Build.VERSION_CODES.P) {
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
					number = "Not Found";
				}
				else {
					number = telManager.getLine1Number();
				}

			} else {
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
					number = "Not Found";
				}
				else {
					number = telManager.getLine1Number();
				}
			}

			PushRecordData push_record_data = new PushRecordData(0, 0, response_time, "", "BRIDGE", mNumber, uid, number, action);
			database.insertPushRecord(push_record_data);
			database.close();
		} 
		catch(SQLException e)
		{
			e.printStackTrace();
		}	 
	}

	private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
		public void onCallStateChanged(int state, String incomingNumber) {
			Log.d(THIS_FILE,"onCallStateChanged state:"+state+", incomingNumber:"+incomingNumber);
			switch (state) {
				/*case TelephonyManager.CALL_STATE_RINGING: // 폰이 울린다.
				case TelephonyManager.CALL_STATE_OFFHOOK: // 폰이 현재 통화 중.
					if(!mWakeLock) { // wakeLock.acquire()이면 브릿지가 활성화된 상태이므로 브릿지가 끊기면 안되기 때문
						//cancelMediaStart();
						//missed();
					}
					break;*/
				case TelephonyManager.CALL_STATE_RINGING:
					// TODO 전화를 왔을때이다. (벨이 울릴때)
					incomingFlag = true;
					callBridgeFlag=false;
					break;

				case TelephonyManager.CALL_STATE_OFFHOOK:
					if(incomingFlag){
						// TODO 전화가 왔을때이다. (통화 시작)
					} else {
						// TODO 안드로이드 8.0 으로 테스트했을때는 ACTION_NEW_OUTGOING_CALL 거치지 않고, 이쪽으로 바로 온다.

					}

					if (incomingFlag) {
						// TODO 전화가 왔고, 통화를 시작했을때 그에 맞는 프로세스를 실행한다.
					} else {
						// TODO 전화를 했을때 그에 맞는 프로세스를 실행한다.
					}

					if (callBridgeFlag){
						cancelAutoClose();
						stopService();
					}
					callBridgeFlag=false;
					break;

				case TelephonyManager.CALL_STATE_IDLE:
					if(incomingFlag){
						// TODO 전화가 왔을때이다. (전화를 끊었을 경우)
					} else {
						// TODO 전화를 걸었을 떄이다. (전화를 끊었을 경우)
					}

					callBridgeFlag=true;

					break;
				default:
					break;
			}
		}
	};

	private void setMediaStart(final Context context, long delayMS) {
		Log.d(THIS_FILE, "setMediaStart (" + delayMS + ")");
		setMediaRunnable = new Runnable() {
			@Override
			public void run() {
				if (wakeLock != null && !wakeLock.isHeld()) {
					Log.d(THIS_FILE, "Acquire wake up lock");
					wakeLock.acquire();
					mWakeLock = true;
				}
				if (mediaManager == null) {
					mediaManager = new BridgeMediaManager(context);
					mediaManager.startService();
					/*if (mediaManager != null) {
						mediaManager.stopAnnoucing();
						mediaManager.resetSettings();
					}*/
					if (mediaManager != null) {
						mediaManager.startRing();
					}
				}
			}
		};
		setMediaHandler = new Handler();
		setMediaHandler.postDelayed(setMediaRunnable, delayMS);
	}

	private void cancelMediaStart() {
		if (setMediaHandler != null) {
			Log.d(THIS_FILE, "cancelMediaStart !!");
			if (setMediaRunnable != null) {
				setMediaHandler.removeCallbacks(setMediaRunnable);
				setMediaRunnable = null;
				if (mediaManager != null) {
					stopRing();
				}
			}
			setMediaHandler = null;
		}
	}

	private void stopRing() {
		if (mediaManager != null) {
			mediaManager.stopAnnoucing();
			mediaManager.resetSettings();
			mediaManager.stopService();
			mediaManager = null;
		}
	}
}
