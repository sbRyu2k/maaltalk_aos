package com.dial070.service;

import android.Manifest;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.dial070.db.DBManager;
import com.dial070.db.PushRecordData;
import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;

public class PopupService extends Service {
	private static final String THIS_FILE = "POPUP SERVICE";
	private View mView; // 항상 보이게 할 뷰
	private WindowManager mManager;
	private WindowManager.LayoutParams mParams;
	private int mId;
	private String mName, mNumber, mDesc;	
	private TextView mTextName, mTextPhone, mTextDesc;
	private ImageButton mButtonClose;
	private Handler autoCloseHandler = null;
	private Runnable autoCloseRunnable = null;
	private NotificationManager mNotificationManager = null;
	private float START_X, START_Y; // 움직이기 위해 터치한 시작 점
	private int PREV_X, PREV_Y; // 움직이기 이전에 뷰가 위치한 점
	private int MAX_X = -1, MAX_Y = -1; // 뷰의 위치 최대 값
	private Context mContext;
	
	private void setAutoClose(long delayMS) {
		cancelAutoClose();
		autoCloseRunnable = new Runnable() {
			@Override
			public void run() {
				mNotificationManager.cancel(mId);
				mNotificationManager = null;
				stopService();
			}
		};
		autoCloseHandler = new Handler();
		autoCloseHandler.postDelayed(autoCloseRunnable, delayMS);
	}
	
	private void cancelAutoClose() {
		if (autoCloseHandler != null) {
			if (autoCloseRunnable != null) {
				autoCloseHandler.removeCallbacks(autoCloseRunnable);
				autoCloseRunnable = null;
			}
			autoCloseHandler = null;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mName = intent.getStringExtra("NAME");
		mNumber = intent.getStringExtra("PHONE");	
		mDesc = intent.getStringExtra("DESC");
		mId = intent.getIntExtra("ID", 0);

		//초기화
		if (mNumber == null) mName = "";
		if (mNumber == null) mNumber = "";	
		
		mTextName.setText(mName);
		mTextPhone.setText(mNumber);
		mTextDesc.setText(mDesc);
		
		pushResponse("START");

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this; //2016.11.10
		LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.popup_floating, null);
		mView.setOnTouchListener(mViewTouchListener);

		mParams = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
				PixelFormat.TRANSLUCENT);

		// params.gravity = Gravity.RIGHT | Gravity.TOP;
		mParams.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;

		mManager = (WindowManager) getSystemService(WINDOW_SERVICE);

		mManager.addView(mView, mParams);
		mNotificationManager = (NotificationManager) this
				.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
		
		mTextName = (TextView) mView.findViewById(R.id.mto_name);
		mTextPhone = (TextView) mView.findViewById(R.id.mto_phone);
		mTextDesc = (TextView) mView.findViewById(R.id.mto_desc);
		
		mButtonClose = (ImageButton) mView.findViewById(R.id.mto_close);

		mButtonClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cancelAutoClose();
				mNotificationManager.cancel(mId);
				mNotificationManager = null;
				stopService();				
			}
		});
		setAutoClose(10000);
	}
	
	private void stopService() {
		stopService(new Intent(PopupService.this, PopupService.class));
	}
	
	private View.OnTouchListener mViewTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: // 사용자 터치 다운이면
				// Log2.i("ACTION_DOWN");
				if (MAX_X == -1)
					setMaxPosition();
				START_X = event.getRawX(); // 터치 시작 점
				START_Y = event.getRawY(); // 터치 시작 점
				PREV_X = mParams.x; // 뷰의 시작 점
				PREV_Y = mParams.y; // 뷰의 시작 점
				break;
			case MotionEvent.ACTION_MOVE:
				// Log2.i("ACTION_MOVE");
				int x = (int) (event.getRawX() - START_X); // 이동한 거리
				int y = (int) (event.getRawY() - START_Y); // 이동한 거리

				// 터치해서 이동한 만큼 이동 시킨다
				mParams.x = PREV_X + x;
				mParams.y = PREV_Y + y;

				// optimizePosition(); //뷰의 위치 최적화
				mManager.updateViewLayout(mView, mParams); // 뷰 업데이트
				break;
			case MotionEvent.ACTION_UP:
				float diffX = event.getRawX() - START_X;
				float diffY = event.getRawY() - START_Y;
				if (Math.abs(diffX) < 30.0 && Math.abs(diffY) < 30.0) {
					//
				}
				break;
			}

			return false;
		}
	};

	private void setMaxPosition() {
		DisplayMetrics matrix = new DisplayMetrics();
		mManager.getDefaultDisplay().getMetrics(matrix); // 화면 정보를 가져와서

		MAX_X = matrix.widthPixels - mView.getWidth(); // x 최대값 설정
		MAX_Y = matrix.heightPixels - mView.getHeight(); // y 최대값 설정
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mView != null) // 서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
		{
			((WindowManager) getSystemService(WINDOW_SERVICE))
			.removeView(mView);
			mView = null;
		}
	}
	
	//BJH 2016.11.10
	private void pushResponse(String action) {
		DebugLog.d("msg action --> "+action);

		long response_time = System.currentTimeMillis();
		DBManager database = new DBManager(mContext);
		try {
			database.open();
			AppPrefs prefs = new AppPrefs(mContext);
			String uid = prefs.getPreferenceStringValue(AppPrefs.USER_ID);
			TelephonyManager telManager = (TelephonyManager) mContext.getSystemService( Context.TELEPHONY_SERVICE);
			String number;

			if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
					number = "Not Found";
				}
				else {
					number = telManager.getLine1Number();
				}

			} else {
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED) {
					number = "Not Found";
				}
				else {
					number = telManager.getLine1Number();
				}
			}

			PushRecordData push_record_data = new PushRecordData(0, 0, response_time, "", "MTO", mNumber, uid, number, action);
			database.insertPushRecord(push_record_data);
			database.close();
		} 
		catch(SQLException e)
		{
			e.printStackTrace();
		}	 
	}	

}
