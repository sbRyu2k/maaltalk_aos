package com.dial070.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.telephony.TelephonyManager;

import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

public class SimStateReceive extends BroadcastReceiver {
	private static final String THIS_FILE = "SIM STATE";
	private final static String ACTION_SIM_STATE_CHANGED = "android.intent.action.SIM_STATE_CHANGED";
	private final static int SIM_VALID = 0;
	private final static int SIM_INVALID = 1;
	private static int simState = SIM_INVALID;
	private static float mRx;
	private static float mTx;

	public static int getSimState() {
		return simState;
	}

	public static float getRx() {
		return mRx;
	}

	public static float getTx() {
		return mTx;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(ACTION_SIM_STATE_CHANGED)) {
			TelephonyManager tm = (TelephonyManager) context
					.getSystemService(Service.TELEPHONY_SERVICE);
			int state = tm.getSimState();

			DebugLog.d("SIM state change received --> "+state);

			Log.d(THIS_FILE, "USIM STATE : " + state);
			switch (state) {
			case TelephonyManager.SIM_STATE_READY:
				simState = SIM_VALID;
				// BJH 갤럭시6은 마이크로 유심이라 재시작 하지 않아도 유심 교체가 가능함..
				mRx = TrafficStats.getMobileRxBytes();
				mTx = TrafficStats.getMobileTxBytes();
				break;
			case TelephonyManager.SIM_STATE_UNKNOWN:
			case TelephonyManager.SIM_STATE_ABSENT:
			case TelephonyManager.SIM_STATE_PIN_REQUIRED:
			case TelephonyManager.SIM_STATE_PUK_REQUIRED:
			case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
			default:
				simState = SIM_INVALID;
				break;
			}
		}
	}

}
