package com.dial070.service;


import me.pushy.sdk.Pushy;
import me.pushy.sdk.util.PushyPersistence;
import me.pushy.sdk.util.exceptions.PushyException;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;

import java.net.URL;

public class PushyMQTT {
	private static final String THIS_FILE = "Dial070-PUSHY";
	public static String mRegistrationID = null;
	public static boolean mRegistrationOK = false; // 토큰 받아오기 절차가 완료 되었는가.
	private static Context mContext;

	public static void init()
	{
		// for memory load.
	}

	public static boolean Register(final Context context) {
//		Log.d(THIS_FILE,"Pushy Register");
		DebugLog.d("Pushy Register --> "+context.getClass().getSimpleName());

		/* BJH */
		if (mRegistrationID == null) {
			DebugLog.d("Pushy Register --> mRegistrationID is null.");
			mContext = context;
			new RegisterForPushNotificationsAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			DebugLog.d("Pushy Register --> mRegistrationID is not null.");
//			Log.d(THIS_FILE, "REG-ID CACHE : " + mRegistrationID);
			DebugLog.d("REG-ID CACHE --> "+mRegistrationID);
			mRegistrationOK = true;
		}
		return true;
	}


	public static void handleReceive(Context context, Intent intent)
	{
		Log.i(THIS_FILE, "handleReceive");
		Fcm.handleReceive(context, intent);
	}

	private static class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception>
	{
		protected Exception doInBackground(Void... params) {
			try {
				// Assign a unique token to this device
				String deviceToken = Pushy.register(mContext);
				mRegistrationID = deviceToken;
				// Log it for debugging purposes
				Log.d(THIS_FILE, "Pushy device token: " + deviceToken);

				// Send the token to your backend server via an HTTP GET request
				///new URL("https://{YOUR_API_HOSTNAME}/register/device?token=" + deviceToken).openConnection();

				//Fcm.PushTokenRegister(mContext);
			}
			catch (Exception exc) {
				// Return exc to onPostExecute
				return exc;
			}

			mRegistrationOK = true;

			return null;
		}

		@Override
		protected void onPostExecute(Exception exc) {
			// Failed?
			if (exc != null) {
				// Show error as toast message
				//Toast.makeText(mContext, exc.toString(), Toast.LENGTH_LONG).show();
				Fcm.PushTokenRegister(mContext);
				return;
			}

			// Succeeded, do something to alert the user
		}
	}
}
