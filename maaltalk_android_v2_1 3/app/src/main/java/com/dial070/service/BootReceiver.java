package com.dial070.service;

import com.dial070.utils.Log;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.*;

public class BootReceiver extends BroadcastReceiver
{
	private static final String THIS_FILE = "Dial070-BOOT";
	public static final String ACTION_START = "com.dial070.service.START";
	public static final String ACTION_TIMER = "com.dial070.service.TIMER";

	private final long START_INTERVAL 	= (1000 * 30 * 1); 	// 설정 후 첫 알람 주기 30초
	private final long TIMER_INTERVAL 	= (1000 * 60 * 5); 	// 알람 반복 주기 5분

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Log.e(THIS_FILE, "onReceive : " + intent.toString());

		String action = intent.getAction();
		if (action != null)
		{
			if (action.equals(Intent.ACTION_BOOT_COMPLETED))
			{
//				Intent i = new Intent();
//				i.setAction("com.dial070.PhoneStateService");
//				context.startService(i);
//				setAlarmTimer(context);
			}
			else if (action.equals(BootReceiver.ACTION_START))
			{
//				setAlarmTimer(context);
			}
			else if (action.equals(BootReceiver.ACTION_TIMER))
			{
				sendMessage(context);
			}
		}
	}

	public void setAlarmTimer(Context context)
	{
		removeAlarmTimer(context);

		long newTime = System.currentTimeMillis() + START_INTERVAL;
		Log.e(THIS_FILE, "setAlarmTimer : " + String.valueOf(START_INTERVAL) + "ms / " + String.valueOf(TIMER_INTERVAL) + "ms");

		AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

		Intent intent = new Intent(BootReceiver.ACTION_TIMER);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0 | PendingIntent.FLAG_IMMUTABLE);
		am.setRepeating(AlarmManager.RTC_WAKEUP, newTime, TIMER_INTERVAL, pi);
	}


	private void removeAlarmTimer(Context context)
	{
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

		Intent intent = new Intent(BootReceiver.ACTION_TIMER);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0 | PendingIntent.FLAG_IMMUTABLE);
		am.cancel(pi);
	}

	private void sendMessage(Context context)
	{
		// Fcm.Heart Beat
		{
			Intent i = new Intent();
			i.setAction("com.google.android.intent.action.MCS_HEARTBEAT");
			try {
				Log.i(THIS_FILE, "sendBroadcast : " + i.getAction());
				context.sendBroadcast(i);
			} catch (Exception e) { e.printStackTrace(); }

		}

		// Fcm.Heart Beat
		{
			Intent i = new Intent();
			i.setAction("com.google.android.gms.Fcm.ACTION_HEARTBEAT_NOW");
			try {
				Log.i(THIS_FILE, "sendBroadcast : " + i.getAction());
				context.sendBroadcast(i);
			} catch (Exception e) { e.printStackTrace(); }

		}

		// Fcm.Service Start
		{
			Intent i = new Intent();
			i.setAction("com.google.android.gms.INITIALIZE");
			try {
				Log.i(THIS_FILE, "sendBroadcast : " + i.getAction());
				context.sendBroadcast(i);
			} catch (Exception e) { e.printStackTrace(); }

		}
	}

}