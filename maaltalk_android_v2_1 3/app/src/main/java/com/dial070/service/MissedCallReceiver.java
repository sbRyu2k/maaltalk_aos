package com.dial070.service;

import java.lang.reflect.Method;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.dial070.maaltalk.R;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.Log;

public class MissedCallReceiver extends BroadcastReceiver {
	public static final int MCR_NOTIF_ID = 4001;
	private static String mLastState = null;
	private static final String THIS_FILE = "MISSED CALL RECEIVER";
	private static NotificationManager notificationManager = null;
	private AppPrefs mPrefs = null;

	@Override
	public void onReceive(final Context context, Intent intent) {
		/**
		 * http://mmarvick.github.io/blog/blog/lollipop-multiple-
		 * broadcastreceiver-call-state/ 2번 호출되는 문제 해결
		 */
		return;
		
		/*String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
		if (mLastState != null && state.equals(mLastState))
			return;
		else
			mLastState = state;
		
		String incomingNumber = intent
				.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

		final String phone_number = PhoneNumberUtils.formatNumber(
				incomingNumber).replace("-", "");
		
		if(mPrefs == null)
			mPrefs = new AppPrefs(context);

		if (TelephonyManager.EXTRA_STATE_RINGING.equals(state)) {
			if(phone_number != null && !phone_number.equals(mPrefs.getPreferenceStringValue(AppPrefs.CONF_NUM_070)))
				return;
			
			String alert = context.getString(R.string.missed_desc);
			Toast.makeText(context, alert, Toast.LENGTH_LONG).show();

		} else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state)) {
			if(phone_number != null && !phone_number.equals(mPrefs.getPreferenceStringValue(AppPrefs.CONF_NUM_070)))
				return;
			
			Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mPrefs.getPreferenceStringValue(AppPrefs.CONF_NUM_070)));
			callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(callIntent);
		}*/

	}
	
	public boolean killCall(Context context) {
        try {
            // Get the boring old TelephonyManager
            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);

            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =  
                    Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);

        } catch (Exception ex) { // Many things can go wrong with reflection calls
            Log.d(THIS_FILE,"PhoneStateReceiver **" + ex.toString());
            return false;
        }
        return true;
    }

}
