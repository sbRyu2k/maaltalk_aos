package com.dial070.service;

import android.content.ComponentName;
import android.content.Intent;

import com.dial070.App;
import com.dial070.data.remote.entity.fcm.FcmBridge;
import com.dial070.data.remote.entity.fcm.FcmCall;
import com.dial070.data.remote.entity.fcm.FcmMTO;
import com.dial070.data.remote.entity.fcm.FcmMessage;
import com.dial070.data.remote.entity.fcm.FcmResponse;
import com.dial070.sip.api.SipCallSession;
import com.dial070.sip.service.SipService;
import com.dial070.status.CallStatus;
import com.dial070.status.WaitingStatus;
import com.dial070.utils.DebugLog;
import com.dial070.utils.Log;
import com.dial070.utils.ServiceUtils;
import com.dial070.utils.WakeUpUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;
import com.dial070.service.Fcm;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with Fcm. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        DebugLog.d("FCM From: "+remoteMessage.getFrom());
        DebugLog.d("FCM currentTime: "+System.currentTimeMillis());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            DebugLog.d("FCM  payload exist.");
            DebugLog.d("FCM payload : "+remoteMessage.getData());
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//            DebugLog.d("FCM Message data payload: "+remoteMessage.getData());

            ComponentName comp = new ComponentName(this.getPackageName(), PushJobIntentService.class.getName());
            Intent i = new Intent(this, PushJobIntentService.class);
            i.putExtra("msg", remoteMessage.getData().get("msg"));
            i.putExtra("from", "FCM");

            PushJobIntentService.enqueueWork(this, (i.setComponent(comp)));

            FcmResponse fcmResponse = FcmResponse.parseFcmData(remoteMessage.getData().get("msg"));
            DebugLog.d("fcmResponse message Type --> "+fcmResponse.getMessageType());

            if(fcmResponse.getMessageType()==FcmResponse.TYPE_CALL) {
                if(fcmResponse.getFcmType()==null) {
                    DebugLog.d("fcmResponse FcmType is null");
                    return;
                }
                if(fcmResponse.getFcmType() instanceof FcmCall) {
                    DebugLog.d("fcmResponse FcmType --> FcmCall");
                    FcmCall call = (FcmCall) fcmResponse.getFcmType();
                    DebugLog.d("fcmResponse Fcm CID --> "+call.getCid());

                    if(call.getStatus()==null || call.getStatus().equals("")) {
                        if(App.isDialMainForeground) {
//                            DebugLog.d("fcm Response is visible Mode --> ");
//                            DebugLog.d("fcm Response App.isDialMainForeground --> "+App.isDialMainForeground);
//
//                            if(!ServiceUtils.isRunningService(App.getGlobalApplicationContext(), SipService.class)) {
//                                DebugLog.d("fcm Response SipService is running...");
//                            } else {
//                                DebugLog.d("fcm Response SipService isn't running...");
//                                startService(new Intent(this, SipService.class));
//                            }
//
//                            if(SipService.currentService == null) {
//                                DebugLog.d("fcm Response SipService.currentService --> null");
//                            } else {
//                                DebugLog.d("fcm Response SipService.currentService --> not null");
//                            }

                        } else {
                            // in invisible
                            if(CallStatus.INSTANCE.getCurrentStatus()!= SipCallSession.InvState.INCOMING &&
                                    CallStatus.INSTANCE.getCurrentStatus()!=SipCallSession.InvState.EARLY) {

                                DebugLog.d("fcm Response is invisible mode --> ");
                                Intent intent = new Intent(this, InBoundCallMediaService.class);
                                if(call.getCid()==null) {
                                    intent.putExtra("cid", "");
                                } else {
                                    intent.putExtra("cid", call.getCid());
                                }
                                startService(intent);

                            } else {
                                // 이미 수신중인 전화가 있는 경우... waiting Queue에 저장... 하 이거 어떻게 해야 하냐...
                                DebugLog.d("fcm Response is invisible mode && in previous call exist");
                                Intent intent = new Intent(this, InBoundCallMediaService.class);
                                if(call.getCid()==null) {
                                    intent.putExtra("cid", "");
                                } else {
                                    intent.putExtra("cid", call.getCid());
                                }
//                                startService(intent);
                                WaitingStatus.callWaitQueue.add(call.getCid());
                            }
                        }
                        return;
                    }

                    if(call.getStatus().equals("BYE")) {
                        Intent intent = new Intent(this, InBoundCallMediaService.class);
                        stopService(intent);
                        return;
                    }
                }
            }

            else if(fcmResponse.getMessageType()==FcmResponse.TYPE_BRIDGE) {
                if(fcmResponse.getFcmType()==null) {
                    DebugLog.d("fcmResponse FcmType is null");
                    return;
                }

                if(fcmResponse.getFcmType() instanceof FcmBridge) {
                    DebugLog.d("fcmResponse FcmType --> FcmBridge");
                    FcmBridge bridge = (FcmBridge) fcmResponse.getFcmType();
                    if(bridge.getStatus()==null || bridge.getStatus().equals("")) {
                        Intent intent = new Intent(this, InBoundBridgeMediaService.class);
                        if(bridge.getCid()==null) {
                            intent.putExtra("cid", "");
                        } else {
                            intent.putExtra("cid", bridge.getCid());
                        }
//                        startService(intent);

                        if(CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.INCOMING ||
                                CallStatus.INSTANCE.getCurrentStatus()==SipCallSession.InvState.EARLY) {

                        } else if(CallStatus.INSTANCE.getNativeStatus()!=0) {

                        } else {
                            startService(intent);
                        }

//                        startService(intent);
                        return;
                    }

                    if(bridge.getStatus().equals("BYE")) {
                        DebugLog.d("fcmResponse getStatus BYE in Bridge");
                        Intent intent = new Intent(this, InBoundBridgeMediaService.class);
                        Intent stopRingIntent = new Intent("in_bridge_call_received_bye");
                        sendBroadcast(stopRingIntent);
                        stopService(intent);
                        return;
                    }
                }
            }

            else if(fcmResponse.getMessageType()==FcmResponse.TYPE_MSG) {
                if(fcmResponse.getFcmType()==null) {
                    DebugLog.d("fcmResponse FcmType is null");
                    return;
                }
                if(fcmResponse.getFcmType() instanceof FcmMessage) {
                    DebugLog.d("fcmResponse FcmType --> FcmMessage");
                    FcmMessage message = (FcmMessage) fcmResponse.getFcmType();
                    DebugLog.d("message --> "+fcmResponse.getAps().getAlert());
                }
            }

            else if(fcmResponse.getMessageType()==FcmResponse.TYPE_MTO) {
                if(fcmResponse.getFcmType()==null) {
                    return;
                }
                if(fcmResponse.getFcmType() instanceof FcmMTO) {
                    Intent mtoIntent = new Intent("mto_msg_received");
                    sendBroadcast(mtoIntent);
                }
            }
        } else {
            DebugLog.d("FCM  payload not exist.");
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            DebugLog.d("FCM Message Notification Body(From FCM): "+remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        DebugLog.d("FCM Refreshed token: "+token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Fcm.Reset(this);
        Fcm.Register(this);
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }
}
