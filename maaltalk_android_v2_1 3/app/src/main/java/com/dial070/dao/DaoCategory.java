package com.dial070.dao;

import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

public class DaoCategory implements Serializable {
    private String title;
    private String countryCode;
    private String filepath1;
    private String filepath2;
    private String filepath3;
    private String startDate;
    private String endDate;
    private String categoryNum;
    private List<Integer> randomNumber;
    private int count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilepath1() {
        return filepath1;
    }

    public void setFilepath1(String filepath1) {
        this.filepath1 = filepath1;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFilepath2() {
        return filepath2;
    }

    public void setFilepath2(String filepath2) {
        this.filepath2 = filepath2;
    }

    public String getFilepath3() {
        return filepath3;
    }

    public void setFilepath3(String filepath3) {
        this.filepath3 = filepath3;
    }

    public List<Integer> getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(List<Integer> randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(String categoryNum) {
        this.categoryNum = categoryNum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /**
     * country와 state가 같으면 같은 것으로 봄.
     * @param object
     * @return
     */
    @Override
    public boolean equals(@Nullable Object object) {
        boolean isSame = false;

        if (object != null && object instanceof DaoCategory)
        {
            isSame = this.categoryNum == ((DaoCategory) object).categoryNum;
        }

        return isSame;
    }
}
