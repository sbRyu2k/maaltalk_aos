package com.dial070.dao;

import java.io.Serializable;

public class DaoTrip implements Serializable {
    private String filepath;
    private String datetime;
    private String address;
    private String location_country;
    private String location_state;
    private String location_country_state;
    private double latitude;
    private double longitude;
    private int count=0;
    private boolean isMain;
    private String startDate;
    private String endDate;
    private String date;
    private String countryCode;
    private String uriPath;
    private String categoryNum;
    private String startdateAndEndDate;
    private String categoryDate;
    private boolean isKorea;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation_country() {
        return location_country;
    }

    public void setLocation_country(String location_country) {
        this.location_country = location_country;
    }

    public String getLocation_state() {
        return location_state;
    }

    public void setLocation_state(String location_state) {
        this.location_state = location_state;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isMain() {
        return isMain;
    }

    public void setMain(boolean main) {
        isMain = main;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLocation_country_state() {
        return location_country_state;
    }

    public void setLocation_country_state(String location_country_state) {
        this.location_country_state = location_country_state;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getUriPath() {
        return uriPath;
    }

    public void setUriPath(String uriPath) {
        this.uriPath = uriPath;
    }

    public String getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(String categoryNum) {
        this.categoryNum = categoryNum;
    }

    public String getStartdateAndEndDate() {
        return startdateAndEndDate;
    }

    public void setStartdateAndEndDate(String startdateAndEndDate) {
        this.startdateAndEndDate = startdateAndEndDate;
    }

    public String getCategoryDate() {
        return categoryDate;
    }

    public void setCategoryDate(String categoryDate) {
        this.categoryDate = categoryDate;
    }

    public boolean isKorea() {
        return isKorea;
    }

    public void setKorea(boolean is) {
        isKorea = is;
    }

    /**
     * country와 state가 같으면 같은 것으로 봄.
     * @param object
     * @return
     */
    /*@Override
    public boolean equals(@Nullable Object object) {
        boolean isSame = false;

        if (object != null && object instanceof DaoTrip)
        {
            isSame = this.location_country_state == ((DaoTrip) object).location_country_state;
        }

        return isSame;
    }*/


}
