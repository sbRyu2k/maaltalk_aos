package com.dial070.dao;

public class UploadCategoryData {
    private String title;
    private String date;
    private String categoryNum;

    public UploadCategoryData(String title, String date, String categoryNum) {
        this.title = title;
        this.date = date;
        this.categoryNum = categoryNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(String categoryNum) {
        this.categoryNum = categoryNum;
    }
}
