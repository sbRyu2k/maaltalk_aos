package com.dial070.utils;

import android.content.Context;
import android.os.PowerManager;

public class WakeUpUtils {

    public static PowerManager.WakeLock sCpuWakeLock = null;
    private static int id = 1;

    public static void wakeUp(Context context) {
        DebugLog.d("caller --> "+context.getClass().getSimpleName());

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        sCpuWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "maaltalk:com.dial070.service.DialJobIntentService." + ++id);

        sCpuWakeLock.acquire(10000);
    }
}
