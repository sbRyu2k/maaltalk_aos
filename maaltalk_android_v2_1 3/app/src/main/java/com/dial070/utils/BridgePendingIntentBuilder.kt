package com.dial070.utils

import android.app.PendingIntent
import android.content.Context
import android.content.Intent

class BridgePendingIntentBuilder private constructor(builder: FullScreenIntentBuilder, context: Context){
    private val intentAction: String
    private val isSingleTopNewTask: Boolean
    private val isNewTask: Boolean
    private val isSingleTask: Boolean
    private val isTopTask: Boolean
    // variable
    private val callNumber: String?
    private val cid: String?
    private val expiredTime: Long?

    private val noDelay: Boolean
    private val extraPush: Boolean
    val fullScreenPendingIntent: PendingIntent

    class FullScreenIntentBuilder(// required parameters
            val intentAction: String) {
        val isSingleTopTask = false
        var isNewTask = false
        var isSingleTask = false
        var isTopTask = false
        var callNumber: String? = null
        var cid: String? = null
        var expiredTime: Long? = null

        // optional parameters
        var noDelay = true
        var extraPush = true
        var pendingIntent: PendingIntent? = null
        fun noDely(noDelay: Boolean): FullScreenIntentBuilder {
            this.noDelay = noDelay
            return this
        }

        fun isSingleTask(isSingleTask: Boolean): FullScreenIntentBuilder {
            this.isSingleTask = isSingleTask
            return this
        }

        fun isTopTask(isTopTask: Boolean): FullScreenIntentBuilder {
            this.isTopTask = isTopTask
            return this
        }

        fun isNewTask(isNewTask: Boolean): FullScreenIntentBuilder {
            this.isNewTask = isNewTask
            return this
        }

        fun extraPush(extraPush: Boolean): FullScreenIntentBuilder {
            this.extraPush = extraPush
            return this
        }

        fun callNumber(callNumber: String?): FullScreenIntentBuilder {
            this.callNumber = callNumber
            return this
        }

        fun cid(cid: String?): FullScreenIntentBuilder {
            this.cid = cid
            return this
        }

        fun expiredTime(expiredTime: Long?): FullScreenIntentBuilder {
            this.expiredTime = expiredTime
            return this
        }

        fun build(context: Context): BridgePendingIntentBuilder {
            val builder = BridgePendingIntentBuilder(this, context)
            pendingIntent = builder.fullScreenPendingIntent
            return builder
        }

        fun getFullScreenPendingIntent(): PendingIntent? {
            return pendingIntent
        }
    }

    init {
        noDelay = builder.noDelay
        extraPush = builder.extraPush
        intentAction = builder.intentAction
        isSingleTopNewTask = builder.isSingleTopTask
        isSingleTask = builder.isSingleTask
        isTopTask = builder.isTopTask
        isNewTask = builder.isNewTask
        callNumber = builder.callNumber
        cid = builder.cid
        expiredTime = builder.expiredTime

        val intent = Intent(builder.intentAction)
        if (builder.isSingleTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
        if (builder.isTopTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        if (builder.isNewTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        if (builder.callNumber != null && builder.callNumber != "") {
            intent.putExtra("dial_number", builder.callNumber)
        }
        if(builder.cid != null && builder.cid != "") {
            intent.putExtra("cid", builder.cid)
        }
        if(builder.expiredTime != null) {
            intent.putExtra("expired_time", builder.expiredTime)
        }
        intent.putExtra("NO_DELAY", builder.noDelay)
        intent.putExtra("push", builder.extraPush)
        fullScreenPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
    }
}

