package com.dial070.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.dial070.bridge.DialBridgeNew;
import com.dial070.maaltalk.R;
import com.dial070.sip.api.SipManager;

import java.lang.reflect.Method;

public class NotiUtils {

    public @interface Channel {
        String OLD_CHANNEL_ID = "channel_01";
        String NEW_CHANNEL_ID = "maaltalk_channel_01";
        String CALL_CHANNEL_ID = "maaltalk_channel_call";
        String BRIDGE_CHANNEL_ID = "maaltalk_channel_bridge";
        String INFO_CHANNEL_ID = "maaltalk_channel_info";
    }

    public static NotificationManager getNotificationManager(Context context) {
        return (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static void createChannel(Context context) {
        if(Build.VERSION.SDK_INT<26) {
            return;
        }
        NotificationManager notificationManager = getNotificationManager(context);

        if(notificationManager.getNotificationChannel(Channel.OLD_CHANNEL_ID)!=null) {
            notificationManager.deleteNotificationChannel(Channel.OLD_CHANNEL_ID);
        }

        if(notificationManager.getNotificationChannel(Channel.NEW_CHANNEL_ID)==null) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel maalTalkDefaultChannel = new NotificationChannel(Channel.NEW_CHANNEL_ID, context.getString(R.string.app_name), importance);
            notificationManager.createNotificationChannel(maalTalkDefaultChannel);
        }

        if(notificationManager.getNotificationChannel(Channel.CALL_CHANNEL_ID)==null) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel callChannel = new NotificationChannel(Channel.CALL_CHANNEL_ID, context.getString(R.string.pns_call_title), importance);
            callChannel.setSound(null, null);
            notificationManager.createNotificationChannel(callChannel);
        }

        if(notificationManager.getNotificationChannel(Channel.BRIDGE_CHANNEL_ID)==null) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel bridgeChannel = new NotificationChannel(Channel.BRIDGE_CHANNEL_ID, context.getString(R.string.app_name), importance);
            bridgeChannel.setSound(null, null);
            notificationManager.createNotificationChannel(bridgeChannel);
        }

        if(notificationManager.getNotificationChannel(Channel.INFO_CHANNEL_ID)==null) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel infoChannel = new NotificationChannel(Channel.INFO_CHANNEL_ID, "통화정보", importance);
            infoChannel.setSound(null, null);
            notificationManager.createNotificationChannel(infoChannel);
        }
    }

    public static PendingIntent createFullScreenCallPendingIntent(Context context, String callNumber) {
//        CallPendingIntentBuilder.FullScreenIntentBuilder builder = null;
//        if(Build.VERSION.SDK_INT>=29) {
//            new CallPendingIntentBuilder
//                    .FullScreenIntentBuilder(SipManager.ACTION_SIP_DIALER);
//        } else {
//            new CallPendingIntentBuilder
//                    .FullScreenIntentBuilder(SipManager.ACTION_SIP_DIALER);
//        }

        CallPendingIntentBuilder.FullScreenIntentBuilder builder = null;

        if(Build.VERSION.SDK_INT>=29) {
            builder = new CallPendingIntentBuilder
                    .FullScreenIntentBuilder(SipManager.ACTION_SIP_DIALER);
        } else {
            builder = new CallPendingIntentBuilder
                    .FullScreenIntentBuilder(SipManager.ACTION_SIP_CALL_UI);
        }

//        CallPendingIntentBuilder.FullScreenIntentBuilder builder = new CallPendingIntentBuilder
//                .FullScreenIntentBuilder(SipManager.ACTION_SIP_DIALER);

        builder.extraPush(true)
                .noDely(true)
                .isNewTask(true)
                .isSingleTask(true)
                .isTopTask(false)
                .callNumber(callNumber)
                .build(context);

        PendingIntent fullScreenPendingIntent = builder.getFullScreenPendingIntent();
        return fullScreenPendingIntent;
    }

    public static PendingIntent createBridgeFullScreenCallPendingIntent(Context context, String callNumber, String cid, long expiredTime) {
        BridgePendingIntentBuilder.FullScreenIntentBuilder builder = new BridgePendingIntentBuilder
                .FullScreenIntentBuilder(DialBridgeNew.ACTION_BRIDGE_UI);

        builder.extraPush(true)
                .noDely(true)
                .isNewTask(true)
                .isSingleTask(true)
                .isTopTask(false)
                .callNumber(callNumber)
                .cid(cid)
                .expiredTime(expiredTime)
                .build(context);

        PendingIntent fullScreenPendingIntent = builder.getFullScreenPendingIntent();
        return fullScreenPendingIntent;
    }

    public static Notification createCallNotification(Context context, PendingIntent pendingIntent,
                                                      String channelId, String title, String text, int iconId,
                                                      boolean onGoing, boolean isCallMessage) {

        Notification notification = null;

        if(Build.VERSION.SDK_INT>=29) {
            notification = buildAndroidQNotificationWithBuilder(context, pendingIntent, channelId, title, text, iconId, onGoing, isCallMessage);

        } else if(Build.VERSION.SDK_INT>=26) {
            notification = buildOreoNotificationWithBuilder(context, pendingIntent, channelId, title, text, iconId, onGoing, isCallMessage);

        } else {
            notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId, onGoing, isCallMessage);
//            if(isNotificationBuilderSupported()) {
//                notification = buildNotificationWithBuilder(context, pendingIntent, title, text, iconId, onGoing, isCallMessage);
//            } else {
//                notification = buildNotificationPreHoneycomb(context, pendingIntent, title, text, iconId, onGoing);
//            }
        }

        return notification;

    }

    private static Notification buildNotificationWithBuilder(Context context, PendingIntent pendingIntent,
                                                             String title, String text, int iconId, boolean onGoing, boolean isCallMessage) {

//        RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_headup_incall);
        RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_headup_incall_under_q);
        Intent declineIntent = new Intent("in_call_notification_decline");
        PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
        notificationView.setTextViewText(R.id.title, title);
        notificationView.setTextViewText(R.id.desc, text);

        android.app.Notification.Builder builder = new android.app.Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setSmallIcon(iconId)
                .setOngoing(false);

        if(isCallMessage) {
//            builder.setCategory(Notification.CATEGORY_CALL);
            builder.setContent(notificationView);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return builder.build();
        } else {
            return builder.getNotification();
        }
    }

    private static Notification buildAndroidQNotificationWithBuilder(Context context, PendingIntent pendingIntent, String channelId,
                                                                     String title, String text, int iconId, boolean onGoing, boolean isCallMessage) {
        if (Build.VERSION.SDK_INT >= 29) {

            if(isCallMessage) {
                RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_headup_incall);
                Intent declineIntent = new Intent("in_call_notification_decline");
                PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
                notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
                notificationView.setTextViewText(R.id.title, title);
                notificationView.setTextViewText(R.id.desc, text);

                Notification notification = new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(iconId)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setFullScreenIntent(pendingIntent,true)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setOngoing(onGoing)
                        .setCategory(Notification.CATEGORY_CALL)
                        .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                        .setCustomContentView(notificationView)
                        .build();

                return notification;

            } else {
                return buildOreoNotificationWithBuilder(context, pendingIntent, channelId, title, text, iconId, onGoing, false);
            }

        } else
            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId, onGoing, isCallMessage);
    }

    private static Notification buildOreoNotificationWithBuilder(Context context, PendingIntent pendingIntent,
                                                                 String channel_id, String title, String text, int iconId, boolean onGoing, boolean isCallMessage) {
        if (Build.VERSION.SDK_INT >= 26) {
            if(isCallMessage) {
//                RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_headup_incall);
                RemoteViews notificationView = new RemoteViews(context.getPackageName(), R.layout.view_headup_incall_under_q);
                Intent declineIntent = new Intent("in_call_notification_decline");
                PendingIntent declinePendingIntent = PendingIntent.getBroadcast(context, 0, declineIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
                notificationView.setOnClickPendingIntent(R.id.container_decline, declinePendingIntent);
                notificationView.setTextViewText(R.id.title, title);
                notificationView.setTextViewText(R.id.desc, text);

                Notification notification = new NotificationCompat.Builder(context, channel_id)
                        .setSmallIcon(iconId)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setFullScreenIntent(pendingIntent, true)
                        .setAutoCancel(true)
                        .setTimeoutAfter(100)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setOngoing(onGoing)
                        .setCategory(Notification.CATEGORY_CALL)
                        .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                        .setCustomContentView(notificationView)
                        .build();

//                .setContentIntent(pendingIntent)

                return notification;

            } else {
                Notification notification = new NotificationCompat.Builder(context, channel_id)
                        .setSmallIcon(iconId)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        // .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .build();

                return notification;
            }

        } else
//            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId, onGoing);
            return buildNotificationWithBuilder(context, pendingIntent, title, text, iconId, onGoing, isCallMessage);
    }

    @SuppressWarnings("deprecation")
    private static Notification buildNotificationPreHoneycomb(Context context, PendingIntent pendingIntent,
                                                              String title, String text, int iconId, boolean onGoing) {

        Notification notification = new Notification(iconId, "", System.currentTimeMillis());
        try {
            // try to call "setLatestEventInfo" if available
            Method m = notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class);
            m.invoke(notification, context, title, text, pendingIntent);
        } catch (Exception e) {
            // do nothing
        }
        return notification;
    }

//    public static boolean isNotificationBuilderSupported() {
//        try {
//            return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) && Class.forName("android.app.Notification.Builder") != null;
//        } catch (ClassNotFoundException e) {
//            return false;
//        }
//    }
}
