package com.dial070.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.Color;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;

public class BitmapHelper {
		public static final float DEFAULT_BADGE_SIZE = 12.0f;
	
        public static Bitmap getRoundedCornerBitmap(Context context, Bitmap bitmap , int roundLevel) {

        	if(context == null || bitmap == null) return null;
        	
            Bitmap roundedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);

            Canvas canvas = new Canvas(roundedBitmap);
            
            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = convertDipsToPixels(context, roundLevel);

            paint.setAntiAlias(true);

            canvas.drawARGB(0, 0, 0, 0);

            paint.setColor(color);

            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

            canvas.drawBitmap(bitmap, rect, rect, paint);

            return roundedBitmap;
        }

        public static int convertDipsToPixels(Context context, int dips) {
            final float scale = context.getResources().getDisplayMetrics().density;
            return (int) (dips * scale + 0.5f);
        }
        
        public static Bitmap getBadgeBitmap(Context context, Bitmap bitmap , Bitmap badge, String title) {

        	if(context == null || bitmap == null || badge == null) return null;
        	
        	float scale = context.getResources().getDisplayMetrics().density;
        	int density_dpi = context.getResources().getDisplayMetrics().densityDpi;

        	Bitmap out = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        	out.setDensity(density_dpi);
        	
            Canvas canvas = new Canvas(out);
            canvas.setDensity(density_dpi);
            
            
            final Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize(DEFAULT_BADGE_SIZE * scale);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setTextAlign(Align.CENTER);
            paint.setAntiAlias(true);
            
            bitmap.setDensity(density_dpi);
            badge.setDensity(density_dpi);            
            canvas.drawBitmap(bitmap, 0, 0, paint);
			canvas.drawBitmap(badge, 0, 0, paint);
			
			float badge_title_x = (float)(badge.getScaledWidth(canvas)) / 2.0f;
			float badge_title_y = (float)(badge.getScaledHeight(canvas)) / 2.0f + 3.0f;			
			canvas.drawText(title, badge_title_x, badge_title_y, paint);
	
            return out;
        }
	}
