package com.dial070.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class ImageLoader {
	private static final String THIS_FILE = "ImageLoader";

	public static interface OnLoadingCompleteListener {
		public void onComplete(ImageView imageView, String path);

		public void onCancel(ImageView imageView);
	}

	ArrayList<UrlImageLoadingTask> loadingList = new ArrayList<UrlImageLoadingTask>();

	/**
	 * Loads a image from url and calls onComplete() when finished<br>
	 * 
	 * @Note you should manually set the loaded image to ImageView in the
	 *       onComplete()
	 * @param imageView
	 * @param url
	 * @param onComplete
	 */
	public void loadImage(ImageView imageView, String url, String save, OnLoadingCompleteListener onComplete) {
		try {
			URL url2 = new URL(url);
			if (imageView != null) {
				for (int i = 0; i < loadingList.size(); i++) {
					UrlImageLoadingTask tmptask = loadingList.get(i);
					if (tmptask.updateView != null && tmptask.updateView.equals(imageView)) {
						tmptask.cancel(true);
						break;
					}
				}
			}
			UrlImageLoadingTask loadtask = new UrlImageLoadingTask(imageView, onComplete, url, save);
			loadtask.execute(url2);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads a image from url and calls onComplete() when finished
	 * 
	 * @param url
	 * @param onComplete
	 */
	public void loadImage(String url, OnLoadingCompleteListener onComplete) {
		loadImage(null, url, null, onComplete);
	}

	/**
	 * Loads a image from url and sets the loaded image to ImageView
	 * 
	 * @param imageView
	 * @param url
	 */
	public void loadImage(ImageView imageView, String url) {
		loadImage(imageView, url, null, null);
	}

	/**
	 * Cancel loading of a ImageView
	 */
	public void cancel(ImageView imageView) {
		for (int i = 0; i < loadingList.size(); i++) {
			UrlImageLoadingTask tmptask = loadingList.get(i);
			if (tmptask.updateView.equals(imageView)) {
				loadingList.remove(i);
				tmptask.cancel(true);
				break;
			}
		}
	}

	/**
	 * Cancel loading of a Url
	 */
	public void cancel(String url) {
		for (int i = 0; i < loadingList.size(); i++) {
			UrlImageLoadingTask tmptask = loadingList.get(i);
			if (tmptask.url.equals(url)) {
				loadingList.remove(i);
				tmptask.cancel(true);
				break;
			}
		}
	}

	/**
	 * Cancel all loading tasks
	 */
	public void cancelAll() {
		while (loadingList.size() > 0) {
			UrlImageLoadingTask tmptask = loadingList.get(0);
			loadingList.remove(tmptask);
			tmptask.cancel(true);
		}
	}

	private class UrlImageLoadingTask extends AsyncTask<URL, Void, String> {
		public ImageView updateView = null;
		public String url;
		public String savePath;
		private boolean isCancelled = false;
		private InputStream urlInputStream;
		private OnLoadingCompleteListener onComplete = null;

		private UrlImageLoadingTask(ImageView updateView, OnLoadingCompleteListener onComplete, String url, String save) {
			this.updateView = updateView;
			this.onComplete = onComplete;
			this.url = url;
			this.savePath = save;
		}

		@Override
		protected String doInBackground(URL... params) {
			try {
				URL url = params[0];
				HttpResponse res = httpGet(updateView.getContext(), url.toString());
				this.urlInputStream = res.getEntity().getContent();
				
				if (this.savePath != null && this.savePath.length() > 0)
				{
					checkFilePath(this.savePath);
					FileOutputStream output = new FileOutputStream(this.savePath, false);
					int read = 0;
					byte[] bytes = new byte[1024];
					while ((read = this.urlInputStream.read(bytes)) != -1) {
					    output.write(bytes, 0, read);
					}
					output.close();
					//return BitmapFactory.decodeFile(this.savePath); // app에서 관리
				}
				else
				{
					//return BitmapFactory.decodeStream(urlInputStream); // app에서 관리
				}
				return this.savePath;
				
			} catch (Exception e) {
				Log.w(THIS_FILE, "failed to load image from " + url, e);
				return null;
			} finally {
				if (this.urlInputStream != null) {
					try {
						this.urlInputStream.close();
					} catch (IOException e) {
						; // swallow
					} finally {
						this.urlInputStream = null;
					}
				}
			}
		}
		
		private void checkFilePath(String path)
		{
			try {
				File file = new File(path);

				File dir = new File(file.getParent());
				if (!dir.exists())
					dir.mkdirs();
				
				if (file.exists())
					file.delete();
				
				file.createNewFile();
				//file.setReadable(true, false);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		private HttpResponse httpGet(Context context,  String url)
		{
			HttpResponse res = null;
			try
			{
				HttpClient httpclient = new HttpsClient(context);
				//HttpParams params = httpclient.getParams();
				//HttpConnectionParams.setConnectionTimeout(params, 120000);
				//HttpConnectionParams.setSoTimeout(params, 120000);

				// Execute HTTP Post Request
				HttpGet httppost = new HttpGet(url);
				Log.d(THIS_FILE, "HTTPS GET EXEC");
				res = httpclient.execute(httppost);
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				Log.e(THIS_FILE, "Exception", e);
			}
			return res;
		}
		

		@Override
		protected void onPostExecute(String result) {
			if (!this.isCancelled) {
				// hope that call is thread-safe
				if (onComplete != null) {
					onComplete.onComplete(updateView, result);
				}
			}
			loadingList.remove(this);
		}

		/*
		 * just remember that we were cancelled, no synchronization necessary
		 */
		@Override
		protected void onCancelled() {
			this.isCancelled = true;
			try {
				if (this.urlInputStream != null) {
					try {
						this.urlInputStream.close();
					} catch (IOException e) {
						;// swallow
					} finally {
						this.urlInputStream = null;
					}
				}
			} finally {
				super.onCancelled();
				if (onComplete != null)
					onComplete.onCancel(updateView);
				loadingList.remove(this);
			}
		}
	}

}