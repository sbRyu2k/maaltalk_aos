package com.dial070.utils;

import com.dial070.dao.Data;
import com.dial070.dao.UploadCategoryAnswersResponse;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RetrofitExService {

    String URL = "http://211.43.13.195:4001";
    String URL_TEST = "http://jsonplaceholder.typicode.com";


    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("/photoshow/{user}")
    Call<ResponseBody> putData(@Path("user") String userID, @Body String param);

    @FormUrlEncoded
    @POST("/clicklog")
    Call<ResponseBody> postClickLogData(@FieldMap HashMap<String, Object> param);

    @FormUrlEncoded
    @POST("/posts")
    Call<Data> postData(@FieldMap HashMap<String, Object> param);

}
