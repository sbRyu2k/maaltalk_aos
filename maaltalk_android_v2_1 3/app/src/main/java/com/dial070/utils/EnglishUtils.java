package com.dial070.utils;

public class EnglishUtils {
	public static final String[] INITIAL_ENGLISH_SECTION = { "A", "B", "C", "D",
		"E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
		"O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y","Z"};	
	public static final String INITIAL_EXTRA_SECTION = "#";		

	public static String getEnglishChar(String value)
	{
		
		if(value == null || value.length() == 0) return INITIAL_EXTRA_SECTION;
		boolean found = false;
		String character = Character.toString(value.toUpperCase().charAt(0));	
		for(int i=0; i < INITIAL_ENGLISH_SECTION.length;i++)
		{
			if(INITIAL_ENGLISH_SECTION[i].equalsIgnoreCase(character)) 
			{
				found = true;
				break;
			}
		}
		if(!found) return INITIAL_EXTRA_SECTION; 
		return character;
	}
	
	public static String getEnglishInitial(String value, String key)
	{
		return "";
	}
}
