package com.dial070.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dial070.dao.DaoCategory;
import com.dial070.dao.DaoTrip;
import com.dial070.db.DBManager;
import com.dial070.db.TravelStepData;
import com.dial070.maaltalk.R;
import com.dial070.ui.CategorySelectPhotoActivity;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private static final String TAG="CategoryActivity RecyclerAdapter";
    private static int viewHeight=0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;

    private OnLoadMoreListener onLoadMoreListener;
    private LinearLayoutManager mLinearLayoutManager;

    private boolean isMoreLoading = false;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem;

    private ArrayList<DaoCategory> itemList;

    ImageLoadingListener animateFirstListener;
    private ImageLoaderConfiguration config;
    DisplayImageOptions options;

    public RecyclerAdapter(Context context ,OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener=onLoadMoreListener;
        this.context=context;
        this.itemList =new ArrayList<>();

        File cacheDir = StorageUtils.getCacheDirectory(context);

        config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800) // // 사이즈 설정
                .threadPriority(Thread.NORM_PRIORITY - 1)
                .denyCacheImageMultipleSizesInMemory()
                .taskExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                .taskExecutorForCachedImages(AsyncTask.THREAD_POOL_EXECUTOR)
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .diskCache(new UnlimitedDiskCache(cacheDir))
                //.writeDebugLogs() // Remove for release app
                .build();

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .build();

        animateFirstListener = new AnimateFirstDisplayListener();

        ImageLoader.getInstance().init(config);
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager){
        this.mLinearLayoutManager=linearLayoutManager;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setRecyclerView(RecyclerView mView){
        mView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                /*visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition();
                Log.d(TAG, "total:"+totalItemCount + "");
                Log.d(TAG, "visible:"+visibleItemCount + "");

                Log.d(TAG, "first:"+firstVisibleItem + "");
                Log.d(TAG, "last:"+lastVisibleItem + "");


                if (totalItemCount == previousTotalItemCount) {
                    isMoreLoading = true;
                }

                if (!isMoreLoading && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isMoreLoading = true;

                    previousTotalItemCount = totalItemCount;
                }*/
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= null;
        //View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.trip_item,null);
        if (viewType == VIEW_ITEM) {
            v= LayoutInflater.from(parent.getContext()).inflate(R.layout.trip_item,parent,false);

            /*v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int width = v.getMeasuredWidth();
            int height = v.getMeasuredHeight();
            Log.i(TAG,"height:"+height);*/
            return new ViewHolder(v);
        } else {
            v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new ProgressViewHolder(v);
        }
    }

    public void addAll(ArrayList<DaoCategory> lst){
        itemList.clear();
        itemList.addAll(lst);
        notifyDataSetChanged();
    }

    public void addItemMore(ArrayList<DaoCategory> lst){
        Log.i(TAG,"addItemMore:");
        itemList.addAll(lst);
        notifyItemRangeChanged(0,itemList.size());
    }

    public void clearItems(){
        itemList.clear();
        notifyDataSetChanged();
    }

    public void addItemOnly(DaoCategory category){
        //Log.i(TAG,"addIteOnly:");
        boolean isNew=true;
        for (int i=0;i<itemList.size();i++){
            if (itemList.get(i).equals(category)){
                isNew=false;
                itemList.set(i,category);
                break;
            }
        }
        if (isNew){
            itemList.add(category);
        }
    }

    public void addAllComplete(){
        Collections.sort(itemList, new Comparator<DaoCategory>() {
            @Override
            public int compare(DaoCategory p1, DaoCategory p2) {
                return p2.getStartDate().compareTo(p1.getStartDate());
            }
        });

        notifyDataSetChanged();
    }

    public ArrayList<DaoCategory> getAllItems(){
        return itemList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //Log.i(TAG,"onBindViewHolder:"+position);
        if (holder instanceof RecyclerView.ViewHolder) {
            final DaoCategory item=itemList.get(position);
            if (item!=null){
                final String date;
                if (item.getStartDate().equals(item.getEndDate())){
                    date=item.getStartDate();
                }else {
                    date=item.getStartDate()+"~"+item.getEndDate();
                }
                ((ViewHolder)holder).title.setText(item.getTitle());
                ((ViewHolder)holder).txtSubtitle.setText(date);
                ((ViewHolder)holder).cardview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, item.getTitle(), Toast.LENGTH_SHORT).show();

                        showGallery(date,item.getCategoryNum());
                    }
                });
                ((ViewHolder)holder).txtExtraNumber.setVisibility(View.GONE);

                String imgArray[]=item.getFilepath1().split("######");

                DebugLog.d("test_travel travel_item photo path: "+item.getFilepath1());
                DebugLog.d("test_travel travel_item photo arr size: "+imgArray.length);

                if (imgArray!=null){
                    if (imgArray.length==1){
                        ((ViewHolder)holder).img1.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img2.setVisibility(View.GONE);
                        ((ViewHolder)holder).img3.setVisibility(View.GONE);
                        ((ViewHolder)holder).viewLine.setVisibility(View.GONE);

//                        String decodedImgUri = Uri.fromFile(new File(imgArray[0])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);

                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img1, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[0])
                                .centerCrop()
                                .into(((ViewHolder)holder).img1);
                    }else if (imgArray.length==2){
                        ((ViewHolder)holder).img1.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img2.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img3.setVisibility(View.GONE);
                        ((ViewHolder)holder).viewLine.setVisibility(View.GONE);

//                        String decodedImgUri = Uri.fromFile(new File(imgArray[0])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img1, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[0])
                                .centerCrop()
                                .into(((ViewHolder)holder).img1);

//                        decodedImgUri = Uri.fromFile(new File(imgArray[1])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img2, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[1])
                                .centerCrop()
                                .into(((ViewHolder)holder).img2);
                    }else if (imgArray.length==3){
                        ((ViewHolder)holder).img1.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img2.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img3.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).viewLine.setVisibility(View.VISIBLE);

//                        String decodedImgUri = Uri.fromFile(new File(imgArray[0])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img1, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[0])
                                .centerCrop()
                                .into(((ViewHolder)holder).img1);

//                        decodedImgUri = Uri.fromFile(new File(imgArray[1])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img2, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[1])
                                .centerCrop()
                                .into(((ViewHolder)holder).img2);

//                        decodedImgUri = Uri.fromFile(new File(imgArray[2])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img3, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[2])
                                .centerCrop()
                                .into(((ViewHolder)holder).img3);
                    }else {
                        ((ViewHolder)holder).img1.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img2.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).img3.setVisibility(View.VISIBLE);
                        ((ViewHolder)holder).viewLine.setVisibility(View.VISIBLE);

                        //List<Integer> index= item.getRandomNumber();

//                        String decodedImgUri = Uri.fromFile(new File(imgArray[0])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img1, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[0])
                                .centerCrop()
                                .into(((ViewHolder)holder).img1);

//                        decodedImgUri = Uri.fromFile(new File(imgArray[1])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img2, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[1])
                                .centerCrop()
                                .into(((ViewHolder)holder).img2);

//                        decodedImgUri = Uri.fromFile(new File(imgArray[2])).toString();
//                        decodedImgUri=Uri.decode(decodedImgUri);
                        //ImageLoader.getInstance().displayImage(decodedImgUri, ((ViewHolder)holder).img3, options, animateFirstListener);
                        Glide.with(((ViewHolder)holder).itemView.getContext())
                                .load(imgArray[2])
                                .centerCrop()
                                .into(((ViewHolder)holder).img3);

                        ((ViewHolder)holder).txtExtraNumber.setVisibility(View.VISIBLE);
                        //int extraNum=imgArray.length-3;
                        ((ViewHolder)holder).txtExtraNumber.setText("+"+(item.getCount()-3));
                    }
                }
            }
        }
    }

    private void showGallery(String date,String categoryNum){
        DebugLog.d("test_travel showGallery showGallery()<----------------------------------------------------");
        DebugLog.d("test_travel showGallery categoryNum: "+categoryNum);
        DBManager database = new DBManager(context);
        database.open();

        Cursor cursor = database.getTravelStepData(categoryNum);
        DaoTrip trip=null;
        ArrayList<DaoTrip> arrayList=new ArrayList<>();
        //Log.i(TAG,"cursor count:"+cursor.getCount());
        if (cursor!=null && cursor.getCount() != 0) {
            DebugLog.d("test_travel showGallery cursor size>0");
            cursor.moveToFirst();
            do {
                String path=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_PATH));
                String country_name=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_COUNTRY_NAME));
                String country_code=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_COUNTRY_CODE));
                String uriPath=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_URI_PATH));
                String datetime=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_DATE));
                String address=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_ADDRESS));
                double latitude=cursor.getDouble(cursor.getColumnIndex(TravelStepData.FIELD_LATITUDE));
                double longitude=cursor.getDouble(cursor.getColumnIndex(TravelStepData.FIELD_LONGITUDE));
                //String categoryNum=cursor.getString(cursor.getColumnIndex(TravelStepData.FIELD_CATEGORY_NUM));

                DebugLog.d("test_travel showGallery ==============================================");
                DebugLog.d("test_travel showGallery path       : "+path);
                DebugLog.d("test_travel showGallery countryName: "+country_name);
                DebugLog.d("test travel showGallery countryCode: "+country_code);
                DebugLog.d("test_travel showGallery uriPath    : "+uriPath);
                DebugLog.d("test_travel showGallery dateTime   : "+datetime);
                DebugLog.d("test_travel showGallery address    : "+address);
                DebugLog.d("test_travel showGallery latitude   : "+latitude);
                DebugLog.d("test_travel showGallery longitude  : "+longitude);

                trip=new DaoTrip();
                trip.setFilepath(path);
                trip.setUriPath(uriPath);
                trip.setCountryCode(country_code);
                trip.setLocation_country(country_name);
                trip.setDatetime(datetime);
                trip.setAddress(address);
                trip.setLatitude(latitude);
                trip.setLongitude(longitude);
                trip.setCategoryNum(categoryNum);
                trip.setStartdateAndEndDate(date);
                arrayList.add(trip);
            } while (cursor.moveToNext());

            database.close();
        }

//        Intent intent=new Intent(context, CategorySelectPhotoActivity.class);
//        intent.putExtra("Trip",arrayList);
//        context.startActivity(intent);
    }

    public void setMoreLoading(boolean isMoreLoading) {
        Log.i(TAG,"setMoreLoading:"+isMoreLoading);
        this.isMoreLoading=isMoreLoading;
    }

    public void setInitPreviousTotalItemCount() {
        Log.i(TAG,"setInitPreviousTotalItemCount");
        previousTotalItemCount=0;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setProgressMore(final boolean isProgress) {
        Log.i(TAG,"setProgressMore:"+isProgress);
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    itemList.add(null);
                    notifyItemInserted(itemList.size() - 1);
                }
            });
        } else {
            itemList.remove(itemList.size() - 1);
            notifyItemRemoved(itemList.size());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img1;
        ImageView img2;
        ImageView img3;
        TextView title;
        TextView txtSubtitle;
        TextView txtExtraNumber;
        CardView cardview;
        View viewLine;

        public ViewHolder(View itemView) {
            super(itemView);
            img1=(ImageView)itemView.findViewById(R.id.img1);
            img2=(ImageView)itemView.findViewById(R.id.img2);
            img3=(ImageView)itemView.findViewById(R.id.img3);
            title=(TextView)itemView.findViewById(R.id.txtTitle);
            txtSubtitle=(TextView)itemView.findViewById(R.id.txtSubtitle);
            txtExtraNumber=(TextView)itemView.findViewById(R.id.txtExtraNumber);
            cardview=itemView.findViewById(R.id.card_view);
            viewLine=itemView.findViewById(R.id.viewLine);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pBar;
        public ProgressViewHolder(View v) {
            super(v);
            pBar = (ProgressBar) v.findViewById(R.id.pBar);
        }
    }

    class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}