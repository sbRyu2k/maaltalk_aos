package com.dial070.utils;

public class Util {

	public Util() {
		// TODO Auto-generated constructor stub
	}

	public static double parseDouble(String str) 
	{
		if (str == null || str.length() == 0) return 0.0;
		str = str.replace(",",".");
		
		double d = 0.0;
		try 
		{
			d = Double.parseDouble(str);
		} 
		catch (NumberFormatException nfe) 
		{
			return 0.0;
		}
		return d;
	}

}
