package com.dial070.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

//import android.text.format.DateFormat;

public class ScoreUtils {

	
	public static int getScoreDuration(int dur)
	{
		int rs = 4;
		if(dur >= 600) rs = 10;
		else if(dur >= 300) rs = 8;
		else if(dur >= 180) rs = 6;
		return rs;
	}
	
	/*
	public static int getScoreCallStart(long start, long endTime)
	{
		int rs = 4;
		if(start == 0) return rs;
		int diff = (int)((endTime-start) / 1000);
		if(diff > 7200) rs = 6;
		else if(diff > 3600) rs = 8;
		else if(diff >= 0) rs = 10;
		
		return rs;
	}
	*/
	
	public static int getScoreCallCount(int count)
	{
		int rs = 0;
		if(count >= 100) rs = 10;
		else if(count >= 50) rs = 8;
		else if(count >= 30) rs = 6;
		else if(count >= 10) rs = 4;
		else if(count >= 5) rs = 3;
		else if(count >= 3) rs = 2;
		else if(count >= 1) rs = 1;
		return rs;
	}	
	
	public static int getTimeUnit(Date d)
	{
		try
		{
			SimpleDateFormat dateHour = new SimpleDateFormat("H",Locale.US);
			SimpleDateFormat dateMin = new SimpleDateFormat("mm",Locale.US);
			String strHour = (String)dateHour.format(d);
			String strMin = (String)dateMin.format(d);
			
			int hour = Integer.parseInt(strHour);		
			int min = Integer.parseInt(strMin);
			return hour*60 + min;
		}
		catch(Exception e)
		{		
		}
		return 0;
	}	
	
}
