package com.dial070.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;

import com.dial070.maaltalk.R;


public class ContactHelper {
	
	private static HashMap<String,String> mNameList = new HashMap<String,String>();
	public static String getContactsNameByPhoneNumber(Context context, String number)
	{
		if (number == null || number.length() == 0) return null;
		
		if (mImageList == null) mNameList = new HashMap<String,String>();
		
		synchronized (mNameList)
		{
			String name = mNameList.get(number);
			if (name != null)
			{
				if (name.equalsIgnoreCase("_null"))
					return null;
				else
					return name;
			}
			
			name = getContactsNameByPhoneNumberDB(context, number);
			if (name != null)
			{
				mNameList.put(number, name);
				return name;
				
			}
			mNameList.put(number, "_null");
		}
		return null;
	}	

	public static String getContactsNameByPhoneNumberDB(Context context, String number)
	{
		if (number == null || number.length() == 0) return null;
		Log.d("ContactHelper","number:"+number);
		Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
		Cursor c = context.getContentResolver().query(uri, new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
		if (c == null) return null;
		
		String name = null;
		while (c.moveToNext())
		{
			name = c.getString(c.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
			Log.d("ContactHelper","name:"+name);
			break;
		}
		c.close();
		return name;
		
	}	
	
	
	
	public static Cursor getContactsByPhoneNumber(Context ctx, String phone_number)
	{
		Cursor c;
		try {
			Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone_number));
			c = ctx.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup._ID,ContactsContract.PhoneLookup.DISPLAY_NAME,ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI},null,null,null);

			if(c == null) return null;
		}catch (Exception e){
			return null;
		}

		return c;		
	}
	
	public static long getContactsIDByPhoneNumber(Context context, String number)
	{
		if (number == null || number.length() == 0) return 0;

		long uid = 0;
		Cursor c = ContactHelper.getContactsByPhoneNumber(context, number);
		if(c != null)
		{
			if(c.getCount() > 0 && c.moveToFirst())
			{
				uid = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
			}
			c.close();
		}
		return uid;
	}		
	
//	private Cursor getContactsCursor(Context ctx)
//	{
//		String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
//		String selection = null;
//		if (Compatibility.getApiLevel() > 10)
//			selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0 ";
//
//		Cursor cursor = ctx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, selection, null, sortOrder);	
//		
//		if(cursor == null) return null;
//
//		return cursor;
//	}
	
	
	public static String getPhoneTypeName(Context context, int type)
	{
		String PhoneType = context.getResources().getString(R.string.phone_type_other);
		switch(type)
		{
		case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_home); 
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_mobile);		
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_work);	
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_fax_work);	
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_fax_home);	
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_other);	
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_pager);	
		}
		break;
		case ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK:
		{
			PhoneType = context.getResources().getString(R.string.phone_type_callback);	
		}
		break;
		default:
			break;
		}		
		
		return PhoneType;
	}			
	
	public static Uri getPhotoUri(Context ctx, String uid) {
	    try {
	        Cursor cur = ctx.getContentResolver().query(
	                ContactsContract.Data.CONTENT_URI,
	                null,
	                ContactsContract.Data.CONTACT_ID + "=" + uid + " AND "
	                        + ContactsContract.Data.MIMETYPE + "='"
	                        + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
	                null);
	        if (cur != null) 
	        {
	            if (!cur.moveToFirst()) 
	            {
	            	cur.close();
	                return null; // no photo
	            }
	            cur.close();
	        } else {
	            return null; // error in cursor process
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
	            .parseLong(uid));
	    return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	}
	
	private static HashMap<String,Uri> mImageList = new HashMap<String,Uri>();
	private static HashMap<String,String> mSkipList = new HashMap<String,String>();
	
	public static void listClear()
	{
		if (mImageList != null)
		{
			synchronized (mImageList)
			{
				mImageList.clear();
				if (mSkipList != null)
					mSkipList.clear();
			}
		}
		
		if (mNameList != null)
		{
			synchronized(mNameList)
			{
				mNameList.clear();
			}
		}
	}
	
	public static Uri getPhotoUriByPhoneNumber(Context ctx, String number)
	{
		// 캐싱해서 처리
		
		Uri photo = null;
		if (mImageList == null) 
			return ContactHelper.getPhotoUriByPhoneNumber2(ctx, number);
		
		synchronized (mImageList)
		{
			photo = mImageList.get(number);
			if (photo == null)
			{
				if (mSkipList.get(number) != "1")
				{
					// user Photo
					//photo = ContactHelper.getPhoto(mContext, number);
					photo = ContactHelper.getPhotoUriByPhoneNumber2(ctx, number);
					if (photo != null) 
					{
						mImageList.put(number, photo);			
					}
					else
					{
						mSkipList.put(number, "1");
					}
				}
			}
		}
		return photo;
	}
	
	public static Uri getPhotoUriByPhoneNumber2(Context ctx, String phone_number)
	{
		Uri u = null;
		Uri uri = null;
		Cursor c=  null; 
		try
		{
			uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone_number));
			c = ctx.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup._ID,ContactsContract.PhoneLookup.PHOTO_ID},null,null,null);
		}
		catch(Exception e)
		{
			return null;
		}
		
		if(c == null) return null;

		if(c != null)
		{
			if (c.getCount() > 0)
			{
				if(c.moveToFirst())
				{
					long id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
					long photo_id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_ID));
					c.close();
					if( id > 0) 
					{
						if(photo_id > 0) 
							u = ContactHelper.getPhotoUri(ctx,Long.toString(id));
					}
				}
			}
			c.close();
		}
		
		return u;
	}	
	
	/*
	 * 속도가 너무 느려서 목록에서는 사용하면 안된다.
	public static Drawable getPhoto(Context ctx, String phone)
	{
		Cursor c = ContactHelper.getContactsByPhoneNumber(ctx, phone);
		if(c != null)
		{
			if (c.getCount() > 0)
			{
				if(c.moveToFirst())
				{
					long id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup._ID));
					long photo_id = c.getLong(c.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_ID));
					c.close();
					if( id > 0) 
					{
						Drawable photo = ContactHelper.getPhoto(ctx,Long.toString(id), false);
						if(photo != null && photo_id > 0) 
							return photo;
					}
				}
			}
			c.close();
		}
		return null;
				
	}
	*/
	
	 public static Bitmap applyGaussianBlur(Bitmap src) {
		  //set gaussian blur configuration
		  double[][] GaussianBlurConfig = new double[][] {
		    { 1, 2, 1 },
		    { 2, 4, 2 },
		    { 1, 2, 1 }
		  };
		  // create instance of Convolution matrix
		  ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
		  // Apply Configuration
		  convMatrix.applyConfig(GaussianBlurConfig);
		  convMatrix.Factor = 16;
		  convMatrix.Offset = 0;
		  //return out put bitmap
		  return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
		 }
	 
	public static Drawable getPhoto(Context ctx, String uid, boolean rounded) {
		Cursor cur = null;
	    try {
	        cur = ctx.getContentResolver().query(
	                ContactsContract.Data.CONTENT_URI,
	                null,
	                ContactsContract.Data.CONTACT_ID + "=" + uid + " AND "
	                        + ContactsContract.Data.MIMETYPE + "='"
	                        + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "'", null,
	                null);
	        if (cur != null) 
	        {
	            if (cur.getCount() == 0) 
	            {
	            	cur.close();
	                return null; // no photo
	            }
	            cur.close();
	        } else {
	            return null; // error in cursor process
	        }
	    } catch (Exception e) {
	        //e.printStackTrace();
	        return null;
	    }
	    finally
	    {
	    	if(cur != null) cur.close();
	    }

	    // contactPhotoUri --> content://com.android.contacts/contacts/1
	    Uri contactPhotoUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long
	            .parseLong(uid));
	    InputStream photoDataStream = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(),contactPhotoUri);
	    
	    //비트맵 코너 라운드를 사용 안할경우 
	    if(!rounded)
	    {
	    	Drawable bmpDrawable = BitmapDrawable.createFromStream(photoDataStream, "UserPhoto");
	    	return bmpDrawable;
	    }
	    
	    
	    //비트맵 코너 라운드를 사용 하는 경
	    Drawable drawableBitmap = null;
	    try
	    {
	    	Bitmap photo = BitmapFactory.decodeStream(photoDataStream);
		    if(photo != null)
		    {
		    	Bitmap roundedPhoto = BitmapHelper.getRoundedCornerBitmap(ctx, photo, 5);
		    	//roundedPhoto = applyGaussianBlur(photo);
		    	drawableBitmap = new BitmapDrawable(roundedPhoto);
		    }
		    else 
		    	drawableBitmap = new BitmapDrawable(photo);

			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
		}
	    
	    return drawableBitmap;	    
	}
	

	public static Boolean addDirectNumber(Context ctx, String name, String number)
	{
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		Builder builder = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI);
		builder.withValue(RawContacts.ACCOUNT_TYPE, null);
		builder.withValue(RawContacts.ACCOUNT_NAME, null);
		ops.add(builder.build());
		
		// Name
		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
		builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
		builder.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name);
		builder.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name);
		ops.add(builder.build());
		
		
		// Number
		builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
		//builder.withSelection(ContactsContract.Data.CONTACT_ID + " = ?",  new String[]{String.valueOf(uid)});
		builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
		builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
		builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number);
		builder.withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MAIN);
		ops.add(builder.build());
		
		// Update
		try
		{
		    ctx.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		    return false;
		}
		
		return true;
	}
	
	public static Boolean removeDirectNumber(Context ctx, String name, String number)	
	{
		long uid = getContactsIDByPhoneNumber(ctx, number);
		if (uid == 0) return false;
		
		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		Builder builder = ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI);
		builder.withSelection(ContactsContract.Data.CONTACT_ID + " = ?",  new String[]{String.valueOf(uid)});
		ops.add(builder.build());
		
		// Update
		try
		{
		    ctx.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		    return false;
		}				
		return true;
	}	
	
}
