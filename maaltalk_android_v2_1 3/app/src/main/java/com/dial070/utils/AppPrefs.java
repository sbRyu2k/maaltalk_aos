package com.dial070.utils;

import java.util.*;

import android.content.*;
import android.content.SharedPreferences.Editor;
//import android.net.*;
import android.preference.*;

import com.dial070.maaltalk.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import android.util.*;

public class AppPrefs {
	//UI
	public static final String MY_PHONE_NUMBER = "my_phone_number";
	public static final String LOG_LEVEL = "log_level";

	public static final String USER_ID = "user_id";
	public static final String USER_PWD = "user_pwd";
	public static final String AUTO_LOGIN = "auto_login";	
	public static final String LAST_CALL_NUMBER = "last_call_number";
	public static final String LAST_LOC_LATITUDE = "last_loc_latitude";
	public static final String LAST_LOC_LONGITUDE = "last_loc_longitude";		
	public static final String LAST_CALL_MEMO = "last_call_memo";
	public static final String AUTO_ANSWER = "auto_answer";
	public static final String AUTO_DIAL = "auto_dial";
	
	public static final String NOTICE_URL = "notice_url";
	public static final String NOTICE_FB_URL = "notice_fb_url";
	public static final String NOTICE_TITLE = "notice_title";
	
	
	public static final String MESSAGE_TITLE = "message_title";
	public static final String MESSAGE_NUMBER = "message_number";

	public static final String MTO_NUMBER = "mto_number";
	
	public static final String HELP_OK = "help_ok";
	public static final String TERM_OK = "term_ok";
	public static final String AUTH_OK = "auth_ok";
	public static final String AUTH_CID = "auth_cid";
	public static final String SKIP_GPS = "skip_gps";
	
	public static final String GLOBAL_INFO_SERIAL = "global_info_serial";	//클라이언트에 저장된 시리일
	public static final String LATEST_GLOBAL_INFO_SERIAL = "latest_global_info_serial";	// 서버에서 바로 받아온 시리얼
	
	// 서버
	public static final String URL_REQ_CLIENT_INFO = "url_req_client_info";
	public static final String URL_REQ_SVC_INFO = "url_req_svc_info";
	
	// 인증
	public static final String URL_REQ_VALIDCID_LOCL = "url_req_validcid_locl";
	public static final String URL_REQ_VALIDCID_INTL = "url_req_validcid_intl";
	public static final String URL_REQ_VALIDCID_CODE = "url_req_validcid_code";
	public static final String URL_REQ_ACCOUNT_INFO_LOCL = "url_req_account_info_locl";
	public static final String URL_REQ_ACCOUNT_INFO_INTL = "url_req_account_info_intl";
	
	public static final String URL_REQ_VALIDCID_LOCL_V2 = "url_req_validcid_locl_v2";
	public static final String URL_REQ_VALIDCID_GLOBAL = "url_req_valid_global";
	public static final String URL_REQ_ACCOUNT_INFO_LOCL_V2 = "url_req_account_info_locl_v2";

	// BJH 2017.04.24 인증 변경
	public static final String URL_REQ_ACCOUNT_INFO_INTL_V2 = "url_req_account_info_intl_v2";
	public static final String URL_REQ_ACCOUNT_INFO_LOCL_V3 = "url_req_account_info_locl_v3";
	
	
	// 포인트
	public static final String URL_REQ_BALANCE = "url_req_balance";
	public static final String URL_ADD_BALANCE = "url_add_balance";
	public static final String URL_ADD_BALANCE_EXTRA = "url_add_balance_extra";

	// 추가
	public static final String URL_MY_INFORMATION = "url_my_information";
	public static final String URL_MY_INFORMATION_V2 = "url_my_information_v2";
	public static final String URL_POPUP = "url_popup";
	public static final String URL_POPUP_SERIAL = "url_popup_serial";
	public static final String URL_POPUP_OLD_SERIAL = "url_popup_old_serial";
	
	
	// 통화기록
	public static final String URL_CDR_INFO = "url_cdr_info";
	public static final String URL_REC_DIR = "url_rec_dir";
	public static final String URL_MESSAGE_INFO = "url_message_info";
	
	// 로그
	public static final String URL_REQ_HISTORY = "url_req_history";
	public static final String LAST_ERROR_DATA = "last_error_data";	
		
	// 게시판
	public static final String URL_SERVICE_INFO = "url_service_info";
	public static final String URL_MANUAL = "url_manual";
	public static final String URL_NOTICE = "url_notice";
	public static final String URL_FAQ = "url_faq";
	public static final String URL_QUESTION = "url_question";
	public static final String URL_KAKAO = "url_kakao";
	public static final String URL_RATE_INFO = "url_rate_info";
	public static final String URL_RATE = "url_rate"; // BJH 2017.01.20
	public static final String URL_TERMS = "url_terms";
	public static final String URL_TERMS_TXT = "url_terms_txt";
	
	public static final String LOCALE = "locale_select";
	
	
	
	public static final String GCM_ID = "gcm_id";
	public static final String GCM_VER = "gcm_ver";
	
	
	
	public static final String CDR_TS = "cdr_ts"; // 통화기록 마지막 timestamp
	public static final String MSG_TS = "msg_ts"; // 메시지 마지막 timestamp
	public static final String PUSH_TS = "push_ts"; // BJH 푸시 마지막 timestamp
	public static final String BRIDGE_TS = "bridge_ts"; // BJH 2016.09.23 마지막 BRIDGE PUSH
	public static final String MTO_TS = "mto_ts"; // BJH 2016.09.23 마지막 MTO PUSH
	public static final String VMS_TS = "vms_ts"; // BJH 2016.09.23 마지막 VMS PUSH
	public static final String VMS_FILE_NAME = "vms_file_name"; // BJH 2016.09.23 마지막 VMS PUSH FILE_NAME
	public static final String RINGTONE_DIR = "ringtone_dir"; // BJH 2016.09.20
	public static final String RINGTONE_URI = "ringtone_uri"; // BJH 2016.09.20
	
	//public static final String ECHO_CANCEL = "echo_cancel";
	
	
	
	// 로그인후 얻어지는 설정정보 백업/복원을 위한 값
	public static final String CONF_PROXY 		= "conf_proxy";
	public static final String CONF_TRANSPORT 	= "conf_transport";
	public static final String CONF_SRTP 		= "conf_srtp";
	public static final String CONF_NUM_070 	= "conf_num_070";
	public static final String CONF_USE_070 	= "conf_use_070";
	public static final String CONF_PAY_TYPE 	= "conf_pay_type";
	public static final String CONF_USE_MSG 	= "conf_use_msg";
	
	//BJH
	public static final String URL_AD_INFO = "url_ad_info";//광고
	public static final String URL_MSG_SEND = "url_msg_send";//메시지 전송
	public static final String URL_UPDATE_SVR = "url_update_svr";//eDNS 서버 업데이트
	public static final String URL_PUSH_MSG_INFO = "url_push_msg_info";//푸시메시지함
	public static final String URL_USER_MSG = "url_user_msg";//사용자별설명서
	public static final String URL_USER_MSG_SERIAL = "url_user_msg_serial";
	public static final String URL_USER_MSG_OLD_SERIAL = "url_user_msg_old_serial";
	public static final String URL_REQ_FORWARDING_INFO = "url_req_forwardging_info";
	public static final String URL_REQ_FORWARDING = "url_req_forwardging";
	//public static final String FOREIGN_USE_CHECK = "foreign_fisrt_check";//삭제
	public static final String FOREIGN_USE = "Foreign_dns_resolver"; // 최초 인증 후에 외국 사용자 DNS Lookup 체크
	public static final String AUTO_REC = "auto_rec"; // 자동 녹취 설정일 때 InCallControls 에서 녹음 토글이 선택되어있도록 하기 위해서
	public static final String AUTO_REC_TOGGLE = "auto_rec_toggle"; // 자동 녹취 설정일 때 InCallControls 에서 녹음 토글이 선택되어있도록 하기 위해서
	public static final String START_FOREGROUND = "start_foreground"; // START_FOREGROUND 활성/비활성
	public static final String TAB_SETTING = "tab_setting"; // 설정 탭으로 
	public static final String NEW_VERSION = "new_version"; // 말톡 최신 버전
	public static final String OLD_VERSION = "old_version"; // 말톡 최신 버전
	public static final String SENSOR_ENABLE = "sensor_enable"; //센서 on//off
	public static final String DID_MODE = "did_mode"; // 착신모드 on//off
	public static final String DIAL_COUNTRY_CODE = "dial_country_code";
	public static final String DIAL_COUNTRY_FLAG = "dial_country_flag";
	public static final String URL_SVC_OUT = "URL_SVC_OUT";
	public static final String URL_REQ_VALIDCID_LOCL_V2_ENC_V2 = "url_req_validcid_locl_v2_enc_v2";//본인인증을 WebView로 통해서 하기 위해서
	public static final String OPEN_DNS_DOMAIN = "url_open_dns_domain";// eDNS를 관련 정보를 info.json 에서 가져오도록 수정
	public static final String OPEN_DNS_SERVER = "url_open_dns_sever";
	public static final String DISTRIBUTOR_ID = "distributor_id"; // BJH 2016.06.14
	public static final String URL_TERMS_USE_TXT ="url_terms_use_txt";//BJH 2016.06.28
	public static final String URL_TERMS_INFO_TXT ="url_terms_info_txt";//BJH 2016.06.28
	public static final String URL_USIM_POCKET_WIFI ="url_usim_pocket_wifi";//BJH 2016.08.12
	public static final String URL_BRIDGE_PAYMENT ="url_bridge_payment";//BJH 2016.10.02
	public static final String IP_COUNTRY_CODE = "ip_country_code";//BJH 2016.10.17
	public static final String IP_DATE = "ip_date";//BJH 2016.10.17
	public static final String PUSH_RECORD_FLAG = "push_record_flag";//BJH 2016.12.02
	public static final String USIM_SETTING_STEP = "usim_setting_step";//BJH 2016.12.08
	public static final String USIM_VIEW = "usim_view";//BJH 2016.12.08
	public static final String USIM_OPER = "usim_oper";//BJH 2016.12.08
	public static final String URL_PUSH_RECORD = "url_push_record";//BJH 2016.12.14
	/* BJH 2016.12.29 APN UPDATE*/
	public static final String INFO_USIM_VER = "info_usim_ver";
	public static final String USIM_INFO_VER = "usim_info_ver";
	//public static final String MTU_OPERATOR_NAME = "mtu_operator_name";
	//public static final String MTU_OPERATOR_CODE = "mtu_operator_code";
	public static final String MTU_DESC = "mtu_desc";
	public static final String MTU_USIM_NAME = "mtu_usim_name";
	public static final String MTU_APN = "mtu_apn";
	public static final String MTU_USER_NAME = "mtu_user_name";
	public static final String MTU_PWD = "mtu_PWD";
	public static final String MTU_MMSC = "mtu_mmsc";
	public static final String MTU_MMS_PROXY = "mtu_mms_proxy";
	public static final String MTU_MMS_PORT = "mtu_mms_port";
	public static final String MTU_MCC = "mtu_mcc";
	public static final String MTU_MNC = "mtu_mnc";
	public static final String MTU_CERT = "mtu_cert";
	public static final String MTU_TYPE = "mtu_type";
	public static final String URL_USIM_LOOKUP = "url_usim_lookup";
    public static final String URL_USIM_ACTIVATION = "url_pocket_wifi_setting";
	/* BJH 2017.03.27 */
	public static final String MT_KEYWORD = "mt_keyboard";
	public static final String URL_REQ_MYINFO = "url_req_myinfo";
	public static final String URL_REQ_CID_TYPE = "url_req_cid_type";
	public static final String URL_REQ_SVC_OUT_V2 = "url_req_svc_out_v2";
    /* BJH 2017.09.01 */
    public static final String MT_VERIFIED = "mt_verified";
    public static final String CO_REQ_URL  = "co_req_url";
	/* BJH 2017.11.08 */
	public static final String URL_REQ_GET_POCKET = "req_get_pocket";
	public static final String INCLUDE_ADVICE_KISA = "include_advice_kisa";
	/* BJH 2018.01.29 */
	public static final String MT_RTT_DATE_CHECK = "mttu_date_check";
	public static final String MT_RTT_DATE = "mttu_date";

	// 2018.10.08
	public static final String URL_COUPON_INFO = "url_coupon_info";

	// 2018.10.08
	public static final String LATEST_CLIENT_VER = "latest_client_ver";

	public static final String URL_MT_AIRPORT ="url_MT_AIRPORT";
	public static final String URL_USER_TIME = "url_user_time";

	// 2019.01.11 스플래쉬화면에서의 info.json호출시 일정시간동안 앱이 포그라운드로 진입해서 다시 호출안되게 하기위한 설정 키값.
	public static final String GLOBAL_INFO_SPLASH_EXECUTION_TIME = "info_splash_execution_time";

	public static final String MT_STARTTIME = "maaltalk_start_time";
	public static final String MT_ENDTIME = "maaltalk_end_time";

	public static final String DIAL_VERSION = "maaltalk_version";

	public static final String REJECT_CALL_MSG = "REJECT_CALL_MSG";

	public static final String RETURN_CALL_NUMBER = "RETURN_CALL_NUMBER";
	public static final String RETURN_ENABLED = "RETURN_ENABLED";
	public static final String RETURN_CONTACT_ENABLED = "RETURN_CONTACT_ENABLED";
	public static final String RETURN_ACTION_YN = "RETURN_ACTION_YN";	//돌려주기할때의 발신일때만 콜아이디 저장하기위해 이용
    public static final String RETURN_CALL_ID_DEST = "RETURN_CALL_ID_DEST";
    public static final String RETURN_CALL_ID_SRC = "RETURN_CALL_ID_SRC";

	public static final String HOME_DID = "HOME_DID";
	public static final String HOME_DID_DATE_EXPIRED = "DID_DATE_EXPIRED";
	public static final String HOME_DID_EXPIRED = "HOME_DID_EXPIRED";
	public static final String HOME_PLAN_NAME = "HOME_PLAN_NAME";
	public static final String HOME_PLAN_DATE_EXPIRED = "HOME_PLAN_DATE_EXPIRED";
	public static final String HOME_PLAN_EXPIRED = "HOME_PLAN_EXPIRED";
	public static final String HOME_BALANCE = "HOME_BALANCE";
	public static final String HOME_BALANCE_MAX = "HOME_BALANCE_MAX";
	public static final String HOME_DISTRIBUTOR_ID = "HOME_DISTRIBUTOR_ID";
	public static final String HOME_LABEL_BALANCE = "HOME_LABEL_BALANCE";
	public static final String HOME_PROGRESS = "HOME_PROGRESS";
	public static final String HOME_PROGRESS_MAX = "HOME_PROGRESS_MAX";
	public static final String HOME_PRODUCT_ID = "HOME_PRODUCT_ID";

	public static final String PHOTO_UPLOAD_AGREE = "PHOTO_UPLOAD_AGREE";
	public static final String PHOTO_ACCESS_AGREE = "PHOTO_ACCESS_AGREE";
	public static final String CATEGORY_UPLOADED_DATETIME = "CATEGORY_UPLOADED_DATETIME";
	public static final String CATEGORY_LOADED_DATE = "CATEGORY_LOADED_DATE";
	
	private final static HashMap<String, String> STRING_PREFS = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
	{
		
		//put(USER_AGENT, CustomDistribution.getUserAgent());
		put(MY_PHONE_NUMBER, "");
		put(LOG_LEVEL, "1");
//		put(LOG_LEVEL, "6");
		put(USER_ID,"");
		put(USER_PWD,"");
		put(LAST_CALL_NUMBER,"");
		put(LAST_LOC_LATITUDE, "0.0");
		put(LAST_LOC_LONGITUDE, "0.0");		
		put(LAST_CALL_MEMO,"");
		put(AUTO_ANSWER,"");
		put(NOTICE_URL,"");
		put(NOTICE_FB_URL,"");
		put(NOTICE_TITLE,"");
		
		/*BJH*/
		put(FOREIGN_USE,"");
		put(AUTO_REC,"");
		put(AUTO_REC_TOGGLE,"");
		put(NEW_VERSION,"");
		put(OLD_VERSION,"");
		put(DIAL_COUNTRY_CODE,"+82");
		put(DIAL_COUNTRY_FLAG,"kr");
		put(URL_SVC_OUT,"");
		put(URL_REQ_VALIDCID_LOCL_V2_ENC_V2,"");
		put(OPEN_DNS_DOMAIN,"cs2.maaltalk.com");
		put(OPEN_DNS_SERVER,"8.8.8.8");
		put(DISTRIBUTOR_ID,"");
		put(RINGTONE_DIR,"");
		put(RINGTONE_URI,"");
		put(BRIDGE_TS,"0");
		put(MTO_TS,"0");
		put(VMS_TS,"0");
		put(VMS_FILE_NAME,"");
		put(IP_COUNTRY_CODE,"");
		put(IP_DATE,"");
		put(PUSH_RECORD_FLAG,"");//BJH 2016.12.02
		put(USIM_SETTING_STEP,"0");//BJH 2016.12.08
		put(USIM_OPER,"");//BJH 2016.12.08
		/*BJH*/
		/* BJH 2016.12.29 */
		put(INFO_USIM_VER, "0.0"); // info.json usim version
		put(USIM_INFO_VER, "0.0");
		//put(MTU_OPERATOR_NAME, "");
		//put(MTU_OPERATOR_CODE, "");
		put(MTU_DESC, "");
		put(MTU_USIM_NAME, "");
		put(MTU_APN, "");
		put(MTU_USER_NAME, "");
		put(MTU_PWD, "");
		put(MTU_MMSC, "");
		put(MTU_MMS_PROXY, "");
		put(MTU_MMS_PORT, "");
		put(MTU_MCC, "");
		put(MTU_MNC, "");
		put(MTU_CERT, "");
		put(MTU_TYPE, "");
		/* BJH 2016.12.29 */
		
		put(MESSAGE_TITLE, "");
		put(MESSAGE_NUMBER, "");

		put(MTO_NUMBER, ""); // BJH 2017.05.15
        put(MT_VERIFIED, ""); //BJH 2017.09.01
        put(CO_REQ_URL, ""); //BJH 2017.09.01
		
		put(AUTH_CID,"");
		
		put(AUTO_DIAL, "");
		
		put(GLOBAL_INFO_SERIAL,"201901010001");
		put(LATEST_GLOBAL_INFO_SERIAL,"201901010001");

		put(MT_STARTTIME,"0");
		put(MT_ENDTIME,"0");
		
		// 중요 정보
		put(URL_REQ_CLIENT_INFO,"https://110.45.220.108/req_client_info.php");
		put(URL_REQ_SVC_INFO,"https://110.45.220.108/req_svc_info.php");
		
		// 이증
		put(URL_REQ_VALIDCID_LOCL,"http://121.254.247.58/dial070/req_valid_cid.php");
		put(URL_REQ_VALIDCID_INTL,"http://121.254.247.58/v3/req_valid_cid.php");
		put(URL_REQ_VALIDCID_CODE,"https://auth.maaltalk.com/certcid_global/req_valid_code.php");
		put(URL_REQ_ACCOUNT_INFO_LOCL,"http://121.254.247.58/dial070/req_account_info.php");
		put(URL_REQ_ACCOUNT_INFO_INTL,"http://121.254.247.58/v3/req_account_info.php");
		
		put(URL_REQ_VALIDCID_LOCL_V2,"https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/in.php");
		put(URL_REQ_VALIDCID_GLOBAL, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/new_cert.php"); // BJH 2017.06.22
		put(URL_REQ_ACCOUNT_INFO_LOCL_V2,"https://auth.maaltalk.com/certcid_locl_v2/req_account_info.php");
		// BJH 2017.04.24 인증 변경
		put(URL_REQ_ACCOUNT_INFO_INTL_V2,"https://auth.maaltalk.com/certcid_global/req_account_info_intl.php");
		put(URL_REQ_ACCOUNT_INFO_LOCL_V3,"https://auth.maaltalk.com/certcid_global/req_account_info_locl.php");
		put(URL_REQ_GET_POCKET,"https://auth.maaltalk.com/certcid_global/req_get_pocket.php"); // BJH 2017.11.08
		
		// 결제
		put(URL_REQ_BALANCE,"https://if.maaltalk.com/maaltalk/req_balance.php");
		put(URL_ADD_BALANCE,"http://pay.maaltalk.com/type/payment.php");
		put(URL_ADD_BALANCE_EXTRA,"http://pay.maaltalk.com/type/payment_extra.php");
		
		put(URL_MY_INFORMATION,"http://info.maaltalk.com/maaltalk_user/maaltalk_login.php");
		put(URL_MY_INFORMATION_V2,"http://info.maaltalk.com/maaltalk_user_v2/maaltalk_user.php"); // BJH 2017.06.22
		
		// 기타
		put(URL_POPUP,"http://eagle.maaltalk.com/maaltalk_user/maaltalk_oauth_cert.php");
		put(URL_POPUP_SERIAL,"");
		put(URL_POPUP_OLD_SERIAL,"");
		
		// 통화기록
		put(URL_CDR_INFO,"https://if.maaltalk.com/maaltalk/req_cdr.php");
		put(URL_REC_DIR,"http://rec.maaltalk.com/maaltalk/rec");
		put(URL_MESSAGE_INFO,"https://if.maaltalk.com/maaltalk/req_message.php");
		
		// 로그
		put(URL_REQ_HISTORY,"https://211.253.25.55/payment/req_history.php");
		
		//BJH
		put(URL_AD_INFO,"https://if.maaltalk.com/maaltalk/req_ad.php");
		put(URL_MSG_SEND,"https://if.maaltalk.com/maaltalk/req_send.php");
		put(URL_UPDATE_SVR,"https://if.maaltalk.com/maaltalk/req_svr_update.php");
		put(URL_USER_MSG,"https://info.maaltalk.com/maaltalk_user/userMsg/");//BJH 2016.11.21
		put(URL_USER_MSG_SERIAL,"");
		put(URL_USER_MSG_OLD_SERIAL,"");
		put(URL_PUSH_MSG_INFO,"https://if.maaltalk.com/maaltalk/req_push_msg.php");
		put(URL_USIM_POCKET_WIFI,"http://store.maaltalk.com/");//BJH 2016.08.12
		put(URL_BRIDGE_PAYMENT,"http://pay.maaltalk.com/maaltalk_bridge.php");//BJH 2016.10.02
		put(URL_PUSH_RECORD, "https://auth.maaltalk.com/push_record/req_record.php");//BJH 2016.12.14
		
		// 게시판
		put(URL_SERVICE_INFO,"http://www.maaltalk.com/intro.php");
		put(URL_MANUAL,"http://www.maaltalk.com/use.php");
		put(URL_NOTICE,"https://store.maaltalk.com/board/list.php?bdId=notice");
		put(URL_FAQ,"http://store.maaltalk.com/service/faq.php");
		put(URL_QUESTION,"https://store.maaltalk.com/board/list.php?bdId=qa");
		put(URL_KAKAO,"");
		put(URL_RATE_INFO,"");
		put(URL_RATE,"http://www.maaltalk.com/charge_table.html"); // BJH 2017.01.20
		put(URL_TERMS,"http://www.maaltalk.com/terms.php");
		put(URL_TERMS_TXT,"http://www.maaltalk.com/terms.txt");
		//BJH 2016.06.28 새로운 이용약관
		put(URL_TERMS_INFO_TXT,"http://www.maaltalk.com/terms_info.txt");
		put(URL_TERMS_USE_TXT,"http://www.maaltalk.com/terms_use.txt");
		//BJH 2016.11.09
		put(URL_REQ_FORWARDING_INFO,"https://if.maaltalk.com/maaltalk/req_forwarding_info.php");
		put(URL_REQ_FORWARDING,"https://if.maaltalk.com/maaltalk/req_forwarding.php");
		put(URL_USIM_LOOKUP,"http://info.maaltalk.com/maaltalk_usim/index.php"); // BJH 2017.02.02
        put(URL_USIM_ACTIVATION,"https://auth.maaltalk.com/pockets_v2/usim_barcode.html"); // BJH 2017.03.13
		put(MT_KEYWORD, ""); // BJH 2017.03.27
		put(URL_REQ_MYINFO, "https://if.maaltalk.com/maaltalk/req_myinfo.php");
		put(URL_REQ_CID_TYPE, "https://if.maaltalk.com/maaltalk/req_cid_type.php");
		put(URL_REQ_SVC_OUT_V2, "https://if.maaltalk.com/maaltalk/req_svc_out.php");
		
		put(LOCALE,"한국어");
		
		put(GCM_ID, "");
		put(GCM_VER, "");
		
		put(CDR_TS, "");
		put(MSG_TS, "");
		put(PUSH_TS, "");//BJH
		
		put(CONF_PROXY, "");
		put(CONF_TRANSPORT, "");
		put(CONF_SRTP, "");
		put(CONF_NUM_070, "");
		put(CONF_PAY_TYPE, "");
		
		put(LAST_ERROR_DATA,"");

		put(MT_RTT_DATE, "");

		put(URL_COUPON_INFO, "http://www.smarttravel.co.kr/"); // 2018.10.08

		put(GLOBAL_INFO_SPLASH_EXECUTION_TIME,"0");

		put(LATEST_CLIENT_VER,"0.0");

		put(URL_MT_AIRPORT,"http://www.maaltalk.com/mt_airport.php");

		put(URL_USER_TIME,"http://info.maaltalk.com/maaltalk_user_v2/maaltalk_user_time.php");

		put(DIAL_VERSION,"1.0");

		put(REJECT_CALL_MSG,"");

		put(RETURN_CALL_NUMBER,"");
		put(RETURN_CALL_ID_DEST,"");
        put(RETURN_CALL_ID_SRC,"");

		put(HOME_DID,"");
		put(HOME_DID_DATE_EXPIRED,"");
		put(HOME_DID_EXPIRED,"");
		put(HOME_PLAN_NAME,"");
		put(HOME_PLAN_DATE_EXPIRED,"");
		put(HOME_PLAN_EXPIRED,"");
		put(HOME_BALANCE,"");
		put(HOME_BALANCE_MAX,"");
		put(HOME_DISTRIBUTOR_ID,"");
		put(HOME_LABEL_BALANCE,"잔액");
		put(HOME_PROGRESS,"0");
		put(HOME_PROGRESS_MAX,"10");
		put(HOME_PRODUCT_ID,"");

		put(PHOTO_UPLOAD_AGREE, "");
		put(PHOTO_ACCESS_AGREE, "");
		put(CATEGORY_UPLOADED_DATETIME, "");
		put(CATEGORY_LOADED_DATE, "");
	}};	
	
	private final static HashMap<String, Boolean> BOOLEAN_PREFS = new HashMap<String, Boolean>(){
		private static final long serialVersionUID = 1L;
	{
		put(AUTO_LOGIN, false);		
		put(HELP_OK, false);
		put(TERM_OK, false);
		put(AUTH_OK, false);
		put(SKIP_GPS,false);
		
		put(CONF_USE_070, false);
		put(CONF_USE_MSG, false);
		
		//put(ECHO_CANCEL,false);
		
		/*BJH*/
		//put(FOREIGN_USE_CHECK, false);
		put(START_FOREGROUND, true);
		put(TAB_SETTING,false);
		put(SENSOR_ENABLE,true);
		put(USIM_VIEW,false); //BJH 2016.12.08
		put(INCLUDE_ADVICE_KISA, false); // BJH 2017.11.14

		put(MT_RTT_DATE_CHECK, false);

		put(RETURN_ENABLED,false);
		put(RETURN_CONTACT_ENABLED,false);

		put(RETURN_ACTION_YN,false);

		put(DID_MODE,true);
	}};	
	
	private final static HashMap<String, Float> FLOAT_PREFS = new HashMap<String, Float>(){
		private static final long serialVersionUID = 1L;
	{
//		put(SEND_VOLUMN_LEVEL, (float)1.0);
//		put(RECV_VOLUMN_LEVEL, (float)1.0);
		//put(SND_MIC_LEVEL, (float)1.0);
		//put(SND_SPEAKER_LEVEL, (float)1.0);
	}};

	private static final String THIS_FILE = "AppPrefs";
	
	private SharedPreferences mPrefs;
//	private ConnectivityManager mConnectivityManager;
//	private ContentResolver mResolver;

	private Context mContext;
	
	public AppPrefs(Context aContext) {
		mContext = aContext;
		mPrefs = PreferenceManager.getDefaultSharedPreferences(aContext);
//		mConnectivityManager = (ConnectivityManager) aContext.getSystemService(Context.CONNECTIVITY_SERVICE);
//		mResolver = aContext.getContentResolver();
		
	}
	//Public setters
	/**
	 * Set a preference string value
	 * @param key the preference key to set
	 * @param value the value for this key
	 */
	public void setPreferenceStringValue(String key, String value) {
		//TODO : authorized values
		Editor editor = mPrefs.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	
	public void setPreferenceIntegerValue(String key, int value) {
		//TODO : authorized values
		Editor editor = mPrefs.edit();
		editor.putString(key, String.valueOf(value));
		editor.commit();
	}	
	
	/**
	 * Set a preference boolean value
	 * @param key the preference key to set
	 * @param value the value for this key
	 */
	public void setPreferenceBooleanValue(String key, boolean value) {
		Editor editor = mPrefs.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	/**
	 * Set a preference float value
	 * @param key the preference key to set
	 * @param value the value for this key
	 */
	public void setPreferenceFloatValue(String key, float value) {
		Editor editor = mPrefs.edit();
		editor.putFloat(key, value);
		editor.commit();
	}
	
	//Private static getters
	// For string
	private static String gPrefStringValue(SharedPreferences aPrefs, String key) {
		if(STRING_PREFS.containsKey(key)) {
			return aPrefs.getString(key, STRING_PREFS.get(key));
		}
		return null;
	}
	
	// For boolean
	private static Boolean gPrefBooleanValue(SharedPreferences aPrefs, String key) {
		if(BOOLEAN_PREFS.containsKey(key)) {
			return aPrefs.getBoolean(key, BOOLEAN_PREFS.get(key));
		}
		return null;
	}
	
	// For float
	private static Float gPrefFloatValue(SharedPreferences aPrefs, String key) {
		if(FLOAT_PREFS.containsKey(key)) {
			return aPrefs.getFloat(key, FLOAT_PREFS.get(key));
		}
		return null;
	}
	
	/**
	 * Get string preference value
	 * @param key the key preference to retrieve
	 * @return the value
	 */
	public String getPreferenceStringValue(String key) {
		return gPrefStringValue(mPrefs, key);
	}
	
	/**
	 * Get boolean preference value
	 * @param key the key preference to retrieve
	 * @return the value
	 */
	public Boolean getPreferenceBooleanValue(String key) {
		return gPrefBooleanValue(mPrefs, key);
	}
	
	/**
	 * Get float preference value
	 * @param key the key preference to retrieve
	 * @return the value
	 */
	public Float getPreferenceFloatValue(String key) {
		return gPrefFloatValue(mPrefs, key);
	}
	
	/**
	 * Get integer preference value
	 * @param key the key preference to retrieve
	 * @return the value
	 */
	public int getPreferenceIntegerValue(String key) {
		try {
			return Integer.parseInt(getPreferenceStringValue(key));
		}catch(NumberFormatException e) {
			Log.e(THIS_FILE, "Invalid "+key+" format : expect a int");
		}
		return Integer.parseInt(STRING_PREFS.get(key));
	}
	
	/**
	 * Set all values to default
	 */
	public void resetAllDefaultValues() {
		for(String key : STRING_PREFS.keySet() ) {
			setPreferenceStringValue(key, STRING_PREFS.get(key));
		}
		for(String key : BOOLEAN_PREFS.keySet() ) {
			setPreferenceBooleanValue(key, BOOLEAN_PREFS.get(key));
		}
		for(String key : FLOAT_PREFS.keySet() ) {
			setPreferenceFloatValue(key, FLOAT_PREFS.get(key));
		}

		setRejectCallMsgArr();
		//Compatibility.setFirstRunParameters(this);
	}

	public void setRejectCallMsgArr(){
		/*String[] tempArray =mContext.getResources().getStringArray(R.array.select_reject_message);
		int count=tempArray.length;

		JSONObject wrapObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		try {
			for (int i = 0; i < count; i++) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("arr"+1, tempArray[i]);
				jsonArray.put(jsonObject);
			}
			wrapObject.put("list", jsonArray);
			setPreferenceStringValue(REJECT_CALL_MSG,wrapObject.toString());
		}catch (JSONException e){
			Log.d(THIS_FILE,"JSONException:"+e.getMessage());
		}*/

		String[] msgArray =mContext.getResources().getStringArray(R.array.select_reject_message);
		ArrayList<String> arrayListMsg = new ArrayList(msgArray.length);
		for(String msg: msgArray) {
			arrayListMsg.add(msg);
		}

		JSONObject wrapObject = new JSONObject();
		JSONArray jsonArray = new JSONArray(arrayListMsg);
		try {
			wrapObject.put("list", jsonArray);
			setPreferenceStringValue(REJECT_CALL_MSG,wrapObject.toString());
		}catch (JSONException e){
			Log.d(THIS_FILE,"JSONException:"+e.getMessage());
		}
	}

	public String getRejectCallMsgOriginArr(){
		/*String[] tempArray =mContext.getResources().getStringArray(R.array.select_reject_message);
		int count=tempArray.length;

		JSONObject wrapObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		try {
			for (int i = 0; i < count; i++) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("arr"+i, tempArray[i]);
				jsonArray.put(jsonObject);
			}
			wrapObject.put("list", jsonArray);
			setPreferenceStringValue(REJECT_CALL_MSG,wrapObject.toString());
		}catch (JSONException e){
			Log.d(THIS_FILE,"JSONException:"+e.getMessage());
		}*/

		String[] msgArray =mContext.getResources().getStringArray(R.array.select_reject_message);
		ArrayList<String> arrayListMsg = new ArrayList(msgArray.length);
		for(String msg: msgArray) {
			arrayListMsg.add(msg);
		}

		JSONObject wrapObject = new JSONObject();
		JSONArray jsonArray = new JSONArray(arrayListMsg);
		try {
			wrapObject.put("list", jsonArray);
			setPreferenceStringValue(REJECT_CALL_MSG,wrapObject.toString());
		}catch (JSONException e){
			Log.d(THIS_FILE,"JSONException:"+e.getMessage());
		}


		return wrapObject.toString();
	}
}
