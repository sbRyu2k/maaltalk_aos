package com.dial070.utils

import android.app.Activity
import android.os.Build
import android.view.WindowManager
import androidx.annotation.ColorInt

object WindowUtils {

    fun setStatusBarColor(context: Activity, @ColorInt color: Int) {
        val window = context.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = color
        }
    }
}