package com.dial070;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.telephony.TelephonyManager;
import android.view.Window;
import android.view.WindowManager;

import com.dial070.maaltalk.databinding.DialSplashBinding;
import com.dial070.service.Fcm;
import com.dial070.global.ServicePrefs;
import com.dial070.maaltalk.R;
import com.dial070.sip.utils.PreferencesWrapper;
import com.dial070.ui.AuthActivityV2;
import com.dial070.ui.AuthActivity;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.DebugLog;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;
import com.dial070.utils.WindowUtils;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class DialSplash extends AppCompatActivity {
	private static final String THIS_FILE = "DIAL070_SPLASH";

	private static final int DIAL_LOGIN_START 	= 1;
	private static final int DIAL_LOGIN_OK 		= 2;
	private static final int DIAL_LOGIN_ERROR 	= 3;
	private static final int DIAL_LOGIN_RETRY 	= 4;
	private static final int DIAL_NETWORK_ERROR = 5;
	private static final int DIAL_CHANGE_DEVICE = 6; // BJH 2017.10.20 기기 변경

	private Context mContext;
	private AppPrefs mPrefs;
	private String mUserId,mUserPwd;
	private long delay = 10;
	private Boolean from_push = false;
	private long mCompareMonth = 2592000000L;

	private Boolean isScheme=false;
	private String schemePhoneNumber;
	private String callType;
	private String action;
	private String type;

	//BJH
	//private int foreground_setting = 0;
	/*private String mSSID = null;
	private String mPwd = null;


	private int mType = 0;
	private String mPinCode = null;*/ // BJH 2017.01.20

	long doProcessStartTime;
	long doProcessEndTime;

	long previousTime;

	Boolean no_delay = false;

	private DialSplashBinding binding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		DebugLog.d("onCreate()");
		binding = DataBindingUtil.setContentView(this, R.layout.dial_splash);
		binding.setLifecycleOwner(this);
//		setContentView(R.layout.dial_splash);

		// status bar set
		WindowUtils.INSTANCE.setStatusBarColor(this, Color.BLACK);
		doProcessStartTime = System.currentTimeMillis();
		previousTime = doProcessStartTime;

		// by sgkim : 2015-08-26 : TODO : 향후 메인쓰레드에서 네트워크 관련 I/O는 수정할 예정입니다.
		// 임시로 조치함.
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		mContext = this;
		mPrefs = new AppPrefs(mContext);

		long mtStartTime = System.currentTimeMillis();
		DebugLog.d("process time check start : "+mtStartTime);

		mPrefs.setPreferenceStringValue(AppPrefs.MT_STARTTIME, ""+mtStartTime);

		/*
            BJH 2017.11.14 KISA 권고사항
         */
		/*boolean kisaOK = mPrefs.getPreferenceBooleanValue(AppPrefs.INCLUDE_ADVICE_KISA);
		boolean termOk = mPrefs.getPreferenceBooleanValue(AppPrefs.TERM_OK);

		if(!kisaOK && !termOk) {
			Intent KisaIntent = new Intent(this, KisaActivity.class);
			KisaIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(KisaIntent);
			finish();
			return;
		} else if(!kisaOK && termOk) {
			mPrefs.setPreferenceBooleanValue(AppPrefs.INCLUDE_ADVICE_KISA, true);
		}*/

		//BJH 2016.06.14 와이드모바일 관련 스플래시 이미지 변경
		/*LinearLayout splash_layout = (LinearLayout)findViewById(R.id.splash_layout);
		ImageView splash_view = (ImageView)findViewById(R.id.splash_log);
		String distributor_id = mPrefs.getPreferenceStringValue(AppPrefs.DISTRIBUTOR_ID);
		if(distributor_id != null && distributor_id.length() > 0 && distributor_id.equalsIgnoreCase("a7000")){
			splash_layout.setBackgroundResource(R.drawable.splash_wide);
			splash_view.setVisibility(View.GONE);
		}*/

		/*
		String id = mPrefs.getPreferenceStringValue(AppPrefs.GCM_ID);
		String pushyToken = PushyPersistance.getToken(mContext);
		Log.i(THIS_FILE, "GCM Registration id : "+ id);
		Log.i(THIS_FILE, "PushyToken : "+ pushyToken);
		Log.i(THIS_FILE, "BOARD = " + Build.BOARD);
		Log.i(THIS_FILE, "BRAND = " + Build.BRAND);
		Log.i(THIS_FILE, "CPU_ABI = " + Build.CPU_ABI);
		Log.i(THIS_FILE, "DEVICE = " + Build.DEVICE);
		Log.i(THIS_FILE, "DISPLAY = " + Build.DISPLAY);
		Log.i(THIS_FILE, "FINGERPRINT = " + Build.FINGERPRINT);
		Log.i(THIS_FILE, "HOST = " + Build.HOST);
		Log.i(THIS_FILE, "ID = " + Build.ID);
		Log.i(THIS_FILE, "MANUFACTURER = " + Build.MANUFACTURER);
		Log.i(THIS_FILE, "MODEL = " + Build.MODEL);
		Log.i(THIS_FILE, "PRODUCT = " + Build.PRODUCT);
		Log.i(THIS_FILE, "TAGS = " + Build.TAGS);
		Log.i(THIS_FILE, "TYPE = " + Build.TYPE);
		Log.i(THIS_FILE, "USER = " + Build.USER);
		Log.i(THIS_FILE, "VERSION.RELEASE = " + Build.VERSION.RELEASE);
		*/

		Fcm.init(); // 쓰레드에서 초기화하면 안되기 때문에 여기서 일단 호출함. (Pushy MQTT 서비스)

				// 로그인이 안되어 있거나 push 셋업이 안되어 있는 경우

//		Boolean no_delay = false;

//		Bundle extras = getIntent().getExtras();
//		if (extras != null)
//		{
//			no_delay = extras.getBoolean("NO_DELAY");
//			from_push = extras.getBoolean(DialMain.EXTRA_PUSH);
//
//			DebugLog.d("no_delay --> "+no_delay);
//			DebugLog.d("from_push --> "+from_push);
//
//			//BJH
//			//foreground_setting = extras.getInt(DialMain.START_FOREGROUND);//START FOREGROUND 노티바로 들어왔는지 체크
//			//foreground_setting = extras.getInt("FOREGROUND");
//
//			Uri uri = getIntent().getData(); // BJH 2018.04.02 사업팀 요청 apn=인터넷 연결을 위한 인증 정보를 입력하는 과정
//			if(uri != null && uri.toString().contains("maaltalk://set_apn"))
//			{
//				navigateApnSettings();
//				return;
//			}
//		}

		if(getIntentData()) {
			return;
		}

		if (no_delay) delay = 0;

		if (!ServicePrefs.DIAL_RETAIL) {
			// Set debug log
			mPrefs.setPreferenceStringValue(AppPrefs.LOG_LEVEL, "6");
			Log.setLogLevel(6);

		} else {
			// Set debug log
			mPrefs.setPreferenceStringValue(AppPrefs.LOG_LEVEL, "1");
			Log.setLogLevel(1);
		}

		checkPreferences();


		/*if (!isValid())
		{
			return;
		}*/

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			int hasPermissionContact = checkSelfPermission( Manifest.permission.READ_CONTACTS );
			int hasPermissionAudio = checkSelfPermission( Manifest.permission.RECORD_AUDIO );
			int hasPermissionSTATE = checkSelfPermission( Manifest.permission.READ_PHONE_STATE );
//			int hasPermissionWrite = checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE );
			int hasPermissionRead = checkSelfPermission( Manifest.permission.READ_EXTERNAL_STORAGE );
			//int hasPermissionAlert= checkSelfPermission( Manifest.permission.SYSTEM_ALERT_WINDOW );
			int hasPermissionCallPhone= checkSelfPermission( Manifest.permission.CALL_PHONE );
			int hasBluetooth = checkSelfPermission(Manifest.permission.BLUETOOTH);
			int hasPermissionBatteryOptimizations= checkSelfPermission( Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS );

			List<String> permissions = new ArrayList<String>();
			if( hasPermissionContact != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.READ_CONTACTS );
			}
			if( hasPermissionAudio != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.RECORD_AUDIO );
			}
			if( hasPermissionSTATE != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.READ_PHONE_STATE );
			}

//			if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
//				int hasPermissionSTATE = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
//
//				if(hasPermissionSTATE!=PackageManager.PERMISSION_GRANTED) {
//					permissions.add(Manifest.permission.READ_PHONE_STATE);
//				}
//
//			} else {
//				int hasPermissionSTATE = checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS);
//
//				if(hasPermissionSTATE!=PackageManager.PERMISSION_GRANTED) {
//					permissions.add(Manifest.permission.READ_PHONE_NUMBERS);
//				}
//			}

			if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q) {
				int hasPermissionWrite = checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE );

				if( hasPermissionWrite != PackageManager.PERMISSION_GRANTED ) {
					permissions.add( Manifest.permission.WRITE_EXTERNAL_STORAGE );
				}
			}

			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.S) {
				int hasBlConnect = checkSelfPermission(Manifest.permission.BLUETOOTH_CONNECT);

				if(hasBlConnect!=PackageManager.PERMISSION_GRANTED) {
					permissions.add(Manifest.permission.BLUETOOTH_CONNECT);
				}
			}

			if( hasPermissionRead != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.READ_EXTERNAL_STORAGE );
			}

			if( hasPermissionCallPhone != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.CALL_PHONE );
			}

			if(hasBluetooth!=PackageManager.PERMISSION_GRANTED) {
				permissions.add(Manifest.permission.BLUETOOTH);
			}

			if( hasPermissionBatteryOptimizations != PackageManager.PERMISSION_GRANTED ) {
				permissions.add( Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS );
			}

			// add
//			int hasPermissionMediaLocation = checkSelfPermission(Manifest.permission.ACCESS_MEDIA_LOCATION);
//			if(hasPermissionMediaLocation!=PackageManager.PERMISSION_GRANTED) {
//				permissions.add(Manifest.permission.ACCESS_MEDIA_LOCATION);
//			}

			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q) {
				int hasPermissionMediaLocation = checkSelfPermission(Manifest.permission.ACCESS_MEDIA_LOCATION);
				if(hasPermissionMediaLocation!=PackageManager.PERMISSION_GRANTED) {
					permissions.add(Manifest.permission.ACCESS_MEDIA_LOCATION);
				}
			}

			if( !permissions.isEmpty() ) {
				requestPermissions( permissions.toArray( new String[permissions.size()] ), REQUEST_CODE_PERMISSIONS );

				if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
					if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
							|| shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)
							|| shouldShowRequestPermissionRationale(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
							|| shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_MEDIA_LOCATION)
					) {
						//showMarshmallowAlert(); // BJH 2017.11.14 굳이 할 필요가 없음
					}

				} else {
					if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_NUMBERS)
							|| shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
							|| shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)
							|| shouldShowRequestPermissionRationale(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
//							|| shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_MEDIA_LOCATION)
					) {
						//showMarshmallowAlert(); // BJH 2017.11.14 굳이 할 필요가 없음
					}
				}

			} else {
				Log.d(THIS_FILE,"onCreate 2 current time milli : "+(System.currentTimeMillis()-previousTime)+" ms");
				previousTime=System.currentTimeMillis();

				MarshmallowResponse(delay);
			}
		} else {
			MarshmallowResponse(delay);
		}

		Intent intent = getIntent();
		Log.d(THIS_FILE, "url scheme 실행 : "+intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_TYPE));
		if(intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_TYPE) != null) {
			if (intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_TYPE).equals("dial") || intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_TYPE).equals("call")){
				Log.d(THIS_FILE, "url scheme 실행");
				isScheme=true;
				callType=intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_TYPE);
				schemePhoneNumber = intent.getStringExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER);
			}else{
				isScheme=false;
			}
		}else{
			isScheme=false;
			Log.d(THIS_FILE, "url scheme 실행 아님");
		}

		if (intent.getStringExtra("action") != null){
			action=intent.getStringExtra("action");
		}

		if (intent.getStringExtra("type") != null){
			type=intent.getStringExtra("type");
		}
	}

	private boolean getIntentData() {
		Bundle extras = getIntent().getExtras();

		if(extras!=null) {
			this.no_delay = extras.getBoolean("NO_DELAY");
			this.from_push = extras.getBoolean(DialMain.EXTRA_PUSH);


			DebugLog.d("no_delay --> "+no_delay);
			DebugLog.d("from_push --> "+from_push);

			Uri uri = getIntent().getData(); // BJH 2018.04.02 사업팀 요청 apn=인터넷 연결을 위한 인증 정보를 입력하는 과정
			if(uri!=null && uri.toString().contains("maaltalk://set_apn")) {
				navigateApnSettings();
				return true;
			}
		}
		return false;
	}

	private void navigateApnSettings() {
		Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		finish();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			DebugLog.d("handleMessage()-->"+msg);
			switch (msg.what)
			{
				case DIAL_LOGIN_START:
				{
				}
					break;
				case DIAL_LOGIN_OK:
				{
					goMain();
				}
					break;
				case DIAL_LOGIN_ERROR:
				{
					if (DialSplash.this != null && !DialSplash.this.isFinishing())
					{

						final int rs = msg.arg1;
						String errMsg = getResources().getString(R.string.error_login);
						if (rs == -90)
						{
							errMsg = getResources().getString(R.string.setting_sim_not_found);
						}
						else if (rs == -91)
						{
							errMsg = getResources().getString(R.string.setting_sim_changed);
						}

						new MaterialAlertDialogBuilder(mContext)
								.setTitle(getResources().getString(R.string.app_name))
								.setMessage(errMsg)
								.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										if (rs == -90 || rs == -91)
											goAuth();
										else
											finish();
									}
								})
								.show();
					}
					//mPrefs.setPreferenceBooleanValue(AppPrefs.AUTO_LOGIN, false);
				}
				break;
				case DIAL_LOGIN_RETRY:
				{
					if (DialSplash.this != null && !DialSplash.this.isFinishing())
					{

						new MaterialAlertDialogBuilder(mContext)
								.setTitle(getResources().getString(R.string.app_name))
								.setMessage(getResources().getString(R.string.error_login_restart))
								.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										finish();
									}
								})
								.show();
					}
				}
				break;

				case DIAL_NETWORK_ERROR:
				{
					if (DialSplash.this != null && !DialSplash.this.isFinishing())
					{
						new MaterialAlertDialogBuilder(mContext)
								.setTitle(getResources().getString(R.string.app_name))
								.setMessage(getResources().getString(R.string.error_network_restart))
								.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										finish();
									}
								})
								.show();
					}
				}
				break;

				case DIAL_CHANGE_DEVICE: // BJH 2017.10.20 기기변경
				{
					if (DialSplash.this != null && !DialSplash.this.isFinishing()) {

						new MaterialAlertDialogBuilder(mContext)
								.setTitle(getResources().getString(R.string.app_name))
								.setMessage(getResources().getString(R.string.mt_change_device))
								.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener(){
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										goAuth();
									}
								})
								.show();
					}
				}
				break;
			}
		}
	};

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler(Looper.getMainLooper())//BJH 2016.07.28 java.lang.OutOfMemoryError: pthread_create
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				default:
					super.handleMessage(msg);
			}
		}
	};

	/*
	final Runnable mGoLogin = new Runnable()
	{
		@Override
		public void run()
		{
			//로그인 화면으로 이동
			Intent intent = new Intent(DialSplash.this, DialLogin.class);
			startActivity(intent);
			finish();

		}
	};
	*/

	final Runnable mGoAuth = new Runnable()
	{
		@Override
		public void run()
		{
			//로그인 화면으로 이동
			Intent intent;
			if (!ServicePrefs.DIAL_RETAIL && ServicePrefs.DIAL_AUTH_SGKIM)
				intent = new Intent(DialSplash.this, AuthActivity.class);
			else
				intent = new Intent(DialSplash.this, AuthActivityV2.class);
			startActivity(intent);
			finish();
		}
	};

	final Runnable mLoginProcess = new Runnable()
	{
		@Override
		public void run()
		{
			//handler.sendEmptyMessage(DIAL_LOGIN_START);	//?사용안하는듯

			/**
			 * 아래부분 반드시 쓰레드처리
			 */
			/*int rs = ServicePrefs.login(mContext, mUserId, mUserPwd);
			Log.d(THIS_FILE,"mLoginProcess current time milli : "+(System.currentTimeMillis()-previousTime)+" ms");
			previousTime=System.currentTimeMillis();
			if (rs >= 0)
			{
				handler.sendEmptyMessage(DIAL_LOGIN_OK);
			}
			else if (rs == -2) // BJH 2017.10.20 기기변경
			{
				handler.sendEmptyMessage(DIAL_CHANGE_DEVICE);
			}
			else if (rs == -99)
			{
				// retry
				handler.sendEmptyMessage(DIAL_LOGIN_RETRY);
			}
			else
			{
				handler.sendMessage(handler.obtainMessage(DIAL_LOGIN_ERROR,rs, 0));
			}*/
			servicePrefsLogin();
		}
	};

	private void servicePrefsLogin(){
		(new AsyncTask<Void, Void, Integer>(){
			@Override
			protected Integer doInBackground(Void... params) {
				int rs = ServicePrefs.login(mContext, mUserId, mUserPwd);
				DebugLog.d("servicePrefLogin() doInBackground() --> mLoginProcess current time mills : "+(System.currentTimeMillis()-previousTime)+" ms");
//				Log.d(THIS_FILE,"mLoginProcess current time milli : "+(System.currentTimeMillis()-previousTime)+" ms");
				return rs;
			}

			@Override
			protected void onPostExecute(Integer rs) {
				DebugLog.d("onPostExecute() --> "+rs);
				previousTime=System.currentTimeMillis();
				if (rs >= 0)
				{
					handler.sendEmptyMessage(DIAL_LOGIN_OK);
				}
				else if (rs == -2) // BJH 2017.10.20 기기변경
				{
					handler.sendEmptyMessage(DIAL_CHANGE_DEVICE);
				}
				else if (rs == -99)
				{
					// retry
					handler.sendEmptyMessage(DIAL_LOGIN_RETRY);
				}
				else
				{
					handler.sendMessage(handler.obtainMessage(DIAL_LOGIN_ERROR,rs, 0));
				}

				super.onPostExecute(rs);
			}
		}).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	//오버레이 권한 확인
	//마시멜로 이상부터만 가능
	public boolean checkDrawOverlayPermission(Context context) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (Settings.canDrawOverlays(context)) {
				return true;
			} else {
				return false;
			}
		}
		else{
			return true;
		}
	}

	/* Marshmallow Response */
	private static final int REQUEST_CODE_PERMISSIONS = 1;
    private androidx.appcompat.app.AlertDialog mMsgDialog = null;
    private void showMarshmallowAlert() {
    	DebugLog.d("showMarshmallowAlert()");

        if(mMsgDialog != null)
            return;

		MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(this);
		builder.setTitle(getResources().getString(R.string.permission_setting));
		builder.setMessage(getResources().getString(R.string.permission_desc));
		builder.setCancelable(false);
		builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				mMsgDialog = null;
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				Uri uri = Uri.fromParts("package", getPackageName(), null);
				intent.setData(uri);
				startActivity(intent);
				finish();
			}
		});
		builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				mMsgDialog = null;
				finish();
			}
		});
		mMsgDialog = builder.create();
		mMsgDialog.show();
    }
	@TargetApi(Build.VERSION_CODES.M)
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		DebugLog.d("onRequestPermissionResult() --> requestCode : "+requestCode);
		switch (requestCode) {
			case REQUEST_CODE_PERMISSIONS:
				if(Build.VERSION.SDK_INT<=Build.VERSION_CODES.P) {
					if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) == PackageManager.PERMISSION_GRANTED ) {
						MarshmallowResponse(delay);
					} else {
						showMarshmallowAlert();
					}
				} else {
					if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_PHONE_NUMBERS) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
							&& checkSelfPermission(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) == PackageManager.PERMISSION_GRANTED ) {
						MarshmallowResponse(delay);
					} else {
						showMarshmallowAlert();
					}
				}
				break;
		}
	}

	private void MarshmallowResponse(long delay) {
    	DebugLog.d("MarshmallowResponse() --> "+delay);
		if(delay == 0) {
			LoadGlobalInfo(); // load가 완료되면 doProcess 진행한다.
		} else {
			boolean rs = checkRoaming(); // BJH 로밍 체크 => 2016.08.26
			if(rs)
				alertRoaming();
			else
				LoadGlobalInfo();
		}
	}

	//2016.08.26
	private boolean checkRoaming() {
    	DebugLog.d("checkRoaming()");

		PreferencesWrapper prefsWrapper = new PreferencesWrapper(this);
		TelephonyManager tel = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
		//String operator = tel.getNetworkOperator(); //로밍 사업자 정보
		if(tel.isNetworkRoaming()) {
			ConnectivityManager connManager;
			boolean lteMode = prefsWrapper.getPreferenceBooleanValue(PreferencesWrapper.USE_LTE_MODE);
			connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
			NetworkInfo wifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			NetworkInfo mobile = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			boolean data_roaming = prefsWrapper.getPreferenceBooleanValue(PreferencesWrapper.DATA_ROAMING);
			long now = System.currentTimeMillis();
			if(data_roaming) {//BJH 데이터 로밍 상태이며 로밍 체크 후 한달 지났는지 체크
				long ex_date = mCompareMonth; // BJH 2016.07.20 수정
				try
				{
					ex_date = Long.parseLong(prefsWrapper.getPreferenceStringValue(PreferencesWrapper.DATA_ROAMING_COMPARE));
				}
				catch(NumberFormatException e)
				{

				}
				if( ex_date < now ) // 한달이 지남
					data_roaming = false;
			}
			//와이파이 상태 일때 말톡 설정에 WIFI 관련 설정 있음, 한번 보인 사람은 다시 보이지 않기 위해서
			if((wifi.isConnected() && lteMode && !data_roaming) || (mobile.isConnected() && !data_roaming )) {
				long after = now + mCompareMonth;// 30일 뒤
		    	String compare_date = String.valueOf(after);
		    	prefsWrapper.setPreferenceStringValue(PreferencesWrapper.DATA_ROAMING_COMPARE, compare_date);
		    	prefsWrapper.setPreferenceBooleanValue(PreferencesWrapper.DATA_ROAMING, true);
				return true;
			}
		}
        return false;
	}
	//2016.08.26
	private void alertRoaming() {
    	DebugLog.d("alertRoaming()");

		MaterialAlertDialogBuilder builder=new MaterialAlertDialogBuilder(this);
		builder.setTitle(getResources().getString(R.string.app_name));
		builder.setMessage(getResources().getString(R.string.roaming_data));
		builder.setCancelable(false);
		builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
				startActivity(intent);
				finish();
			}
		});
		builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				LoadGlobalInfo();
			}
		});
		builder.show();
	}

	private boolean isValid()
	{
		/*
        String device = android.os.Build.DEVICE.toUpperCase(Locale.getDefault());
        if (!Compatibility.isCompatible(11))
        {
        	if (device.startsWith("SHW-M250")
        			|| device.startsWith("SHV-E110S")
        			|| device.startsWith("SHV-E120")
        			|| device.startsWith("GT-I9100")
        			|| device.startsWith("GT-I9210")
        			|| device.startsWith("GT-I9108")
        			|| device.startsWith("GT-I9105"))
        	{
				AlertDialog msgDialog = new AlertDialog.Builder(mContext).create();
				msgDialog.setTitle("단말기 OS 업그레이드 안내");
				msgDialog.setMessage("갤럭시 S2의 경우 4.1 이상으로 업데이트 하셔야 정상적으로 동작합니다.\n\n업그레이드 후 다시 실행해 주십시오.");
				msgDialog.setButton(mContext.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
					//@Override
					public void onClick(DialogInterface dialog, int which) {
						//do nothing
						finish();
						return;
					}
				});
				msgDialog.show();
	        	return false;
        	}
        }
        */

        return true;
	}

	private void doProcess() {
    	DebugLog.d("doProcess() --> current time mills : "+(System.currentTimeMillis()-previousTime)+" ms");
		// FOR TEST
		Log.d(THIS_FILE,"doProcess current time milli : "+(System.currentTimeMillis()-previousTime)+" ms");
		previousTime=System.currentTimeMillis();
		if (ServicePrefs.DIAL_TEST)
		{
			mHandler.postDelayed(mLoginProcess, delay);
			return;
		}

		Boolean authOk = mPrefs.getPreferenceBooleanValue(AppPrefs.AUTH_OK);
		mUserId = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
		mUserPwd = mPrefs.getPreferenceStringValue(AppPrefs.USER_PWD);

		if (!authOk)
		{
			mHandler.postDelayed(mGoAuth, delay);
		}
		else if(/*mPrefs.getPreferenceBooleanValue(AppPrefs.AUTO_LOGIN) &&*/ (mUserId.length() > 0 && mUserPwd.length() > 0))
		{
			//Login process
			//processLogin();
			mHandler.postDelayed(mLoginProcess, delay);
		}
		else
		{
			mHandler.postDelayed(mGoAuth, delay);
		}
	}

	private void goAuth() {
    	DebugLog.d("goAuth()");

		mPrefs.setPreferenceBooleanValue(AppPrefs.AUTH_OK, false);
		mPrefs.setPreferenceStringValue(AppPrefs.USER_ID, "");
		mPrefs.setPreferenceStringValue(AppPrefs.USER_PWD, "");
		mPrefs.setPreferenceStringValue(AppPrefs.GCM_ID, "");

		//로그인 화면으로 이동
		Intent intent = new Intent(DialSplash.this, AuthActivityV2.class);
		startActivity(intent);
		finish();
	}

	/*
	private void goLogin()
	{
		//로그인 화면으로 이동
		Intent intent = new Intent(DialSplash.this, DialLogin.class);
		startActivity(intent);
		finish();
	}
	*/

	private void goMain() {
		//Log.d(THIS_FILE,"==goMain==");
		doProcessEndTime = System.currentTimeMillis();
//		Log.d(THIS_FILE,"Process during : "+(doProcessEndTime-doProcessStartTime)/1000.0f+" sec");
		DebugLog.d("goMain() process during : "+(doProcessEndTime-doProcessStartTime)/1000.f+" sec");

		//메인 화면으로 이동
		Intent intent = new Intent(DialSplash.this, DialMain.class);
		intent.putExtra(DialMain.EXTRA_PUSH, from_push);
		//if(foreground_setting > 0)
		//intent.putExtra(DialMain.START_FOREGROUND, foreground_setting);
		if (isScheme){
			intent.putExtra(DialMain.EXTRA_SCHEME_CALL_TYPE,callType);
			intent.putExtra(DialMain.EXTRA_SCHEME_CALL_NUMBER,schemePhoneNumber);
		}

		if (action!=null){
			intent.putExtra("action",action);
		}

		if (type!=null){
			intent.putExtra("type",type);
		}
		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		//백버튼 비활성화
		//super.onBackPressed();
	}

	private void checkPreferences() {		// 네트웤 관련 프리퍼런스 셋업
		DebugLog.d("Current device " + android.os.Build.BRAND + "-" + android.os.Build.DEVICE + "-" + android.os.Build.MODEL);
		PreferencesWrapper prefsWrapper = new PreferencesWrapper(this);

		DebugLog.d("AlreadySetupService flag value --> "+prefsWrapper.hasAlreadySetupService());
		if (!prefsWrapper.hasAlreadySetupService())
		{
			//handler.sendMessage(handler.obtainMessage(INTRO_TOAST_MESSAGE, R.string.prefs_load_default, 0));
//			Log.d(THIS_FILE, "checkPreferences : Set Default Prefs..");
			DebugLog.d("Set Default Prefs...");
			prefsWrapper.resetAllDefaultValues();
			prefsWrapper.setPreferenceBooleanValue(PreferencesWrapper.HAS_ALREADY_SETUP_SERVICE, true);
		}
	}


	private void LoadGlobalInfo() {
		DebugLog.d("LoadGlobalInfo() --> current time mills : "+(System.currentTimeMillis()-previousTime)+" ms");

//		Log.d(THIS_FILE,"LoadGlobalInfo current time milli : "+(System.currentTimeMillis()-previousTime)+" ms");
		previousTime=System.currentTimeMillis();
		Thread t = new Thread()
		{
			public void run()
			{
				String userid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
				if (userid != null && userid.length() > 0)
				{
					// 인증을 받은 경우. 최소 한번 이상 실행된 것이므로.
					// 속도 향상을 위해 처음 실행시면 info.json을 먼저 갖고 오고.
					// 그 이후는 먼저 처리후 병렬로 값을 읽어 오도록 한다.

					doProcess();

					/*int rs = requestNewGlobalInfo();
					if (rs < 0)
					{
						//
					}*/

                    //ServicePrefs.mLogin=true;
					//goMain();
				}
				else
				{
					// 인증을 받지 않은 경우 원래대로..
					int rs = requestNewGlobalInfo();
					if (rs < 0)
					{
						//
					}
					doProcess();
				}
			}
		};
		if (t != null)
			t.start();
	}

	/*private String getLocalInfoJson() {
		String data = null;
		InputStream inputStream = getResources().openRawResource(R.raw.info);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		int i;
		try {
			i = inputStream.read();
			while (i != -1) {
				byteArrayOutputStream.write(i);
				i = inputStream.read();
			}

			data = new String(byteArrayOutputStream.toByteArray(),"MS949");
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}*/

	private int requestGlobalInfo()
	{
//		StringBuffer sb = new StringBuffer();



		// SEND QUERY
		Log.d(THIS_FILE, "REQUEST GLOBAL INFO : START");
		String rs = postGlobalInfo();
		Log.d(THIS_FILE, "REQUEST GLOBAL INFO : END");
		Log.d(THIS_FILE, "RESULT : " + rs);
		if (rs == null || rs.length() == 0) return -1;

		JSONObject jObject;
		try
		{
		    jObject = new JSONObject(rs);
            JSONObject responseObject = jObject.getJSONObject("RESPONSE");
            Log.d("RES_CODE", responseObject.getString("RES_CODE"));
            Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
            Log.d("RES_DESC ", responseObject.getString("RES_DESC"));

            if (responseObject.getInt("RES_CODE") == 0)
            {
                JSONObject urlObject = jObject.getJSONObject("MAALTALK_INFO");
                if (urlObject == null) return -1;
				Log.d(THIS_FILE, "APP NEW VERSION="+ urlObject.getString("VERSION"));

                String ver = urlObject.getString("VERSION");
                //BJH 2016 2016.08.26 버전이 높을때만 새로 갱신
				//BJH 2019 2019.01.09 주석처리되었던것 다시 복구 : 구버전을 계속 이용중인 고객들에게는 문제가 있음.
                String ex_ver = mPrefs.getPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL);
                Log.d(THIS_FILE, "APP CLIENT VERSION="+ ex_ver);
                if(ver.compareTo(ex_ver) < 1) {
					Log.d(THIS_FILE, "This position STOP");
					return 0;
				}

				Log.d(THIS_FILE, "This position PASS 1");
                mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL, ver);

                String Name = null;
                String Url = null;
                String Serial = null;

                JSONArray urlItemArray = urlObject.getJSONArray("URLS");
                if (urlItemArray == null) return -1;
                for (int i = 0; i < urlItemArray.length(); i++)
                {
                	Name = urlItemArray.getJSONObject(i).getString("NAME").toString();
                	Url = urlItemArray.getJSONObject(i).getString("URL").toString();

                    Log.d(THIS_FILE, "NAME="+ urlItemArray.getJSONObject(i).getString("NAME"));
                    Log.d(THIS_FILE, "URL="+ urlItemArray.getJSONObject(i).getString("URL"));

                	if(Name == null) continue;


                	if(Name.equalsIgnoreCase("REQ_CLIENT_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("REQ_SVC_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO, Url);
                	}

                	// 인증
                	else if(Name.equalsIgnoreCase("REQ_VALIDCID_LOCL"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL, Url);
                	}
                	else if(Name.equalsIgnoreCase("REQ_VALIDCID_INTL"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL, Url);
                	}
					else if(Name.equalsIgnoreCase("REQ_VALIDCID_CODE"))
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_CODE, Url);
					}
                	else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL, Url);
                	}
                	else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL, Url);
                	}

                	else if(Name.equalsIgnoreCase("REQ_VALIDCID_LOCL_V2"))
                	{
                		Serial = "";
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2, Url);
                		if (urlItemArray.getJSONObject(i).has("ENC_V2"))
                   		{
                   			Serial = urlItemArray.getJSONObject(i).getString("ENC_V2").toString();
                   		}
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, Serial);

                	}
					else if(Name.equalsIgnoreCase("REQ_VALIDCID_GLOBAL"))
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL, Url);

					}
                	else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V2"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2, Url);
                	}

                	//BJH 2017.04.24 인증 변경
					else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL_V2"))
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2, Url);
					}
					else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V3"))
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3, Url);
					}

                   	else if(Name.equalsIgnoreCase("POPUP"))
                	{
                   		Serial = "";
                   		if (urlItemArray.getJSONObject(i).has("SERIAL"))
                   		{
                   			Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
                   		}
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP, Url);
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP_SERIAL, Serial);
                	}

                	// 결제
                	else if(Name.equalsIgnoreCase("REQ_BALANCE"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_BALANCE, Url);
                	}
                	else if(Name.equalsIgnoreCase("ADD_BALANCE"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE, Url);
                	}
                	else if(Name.equalsIgnoreCase("ADD_BALANCE_EXTRA"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA, Url);
                	}
                	else if(Name.equalsIgnoreCase("MY_INFORMATION"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION, Url);
                	}

                	// 통화기록
                	else if(Name.equalsIgnoreCase("CDR_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_CDR_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("REC_DIR"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REC_DIR, Url);
                	}
                	else if(Name.equalsIgnoreCase("MESSAGE_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO, Url);
                	}

                	// 로그
                	else if(Name.equalsIgnoreCase("REQ_HISTORY"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_HISTORY, Url);
                	}

                	// 게시판
                	else if(Name.equalsIgnoreCase("SERVICE_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_SERVICE_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("MANUAL"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_MANUAL, Url);
                	}
                	else if(Name.equalsIgnoreCase("NOTICE"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_NOTICE, Url);
                	}
                	else if(Name.equalsIgnoreCase("FAQ"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_FAQ, Url);
                	}
                	else if(Name.equalsIgnoreCase("QUESTION"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_QUESTION, Url);
                	}
                	else if(Name.equalsIgnoreCase("KAKAO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_KAKAO, Url);
                	}
                	else if(Name.equalsIgnoreCase("RATE_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("RATE_INFO_URL")) // BJH 2017.01.20
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE, Url);
                	}
                	else if(Name.equalsIgnoreCase("TERMS"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS, Url);
                	}
                  	else if(Name.equalsIgnoreCase("TERMS_TXT"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_TXT, Url);
                		if (urlItemArray.getJSONObject(i).has("INFO"))//BJH 2016.06.28 Terms 추가
                   		{
                   			mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT, urlItemArray.getJSONObject(i).getString("INFO").toString());
                   			mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT, urlItemArray.getJSONObject(i).getString("USE").toString());
                   		}
                	}
                	//BJH 광고, 메시지 보내기, 서버 업데이트
                	else if(Name.equalsIgnoreCase("AD_INFO"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_AD_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("MSG_SEND"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, Url);
                	}
                	else if(Name.equalsIgnoreCase("UPDATE_SVR"))
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_UPDATE_SVR, Url);
                	}
                	else if(Name.equalsIgnoreCase("USER_MSG"))//BJH
                	{
                   		Serial = "";
                   		if (urlItemArray.getJSONObject(i).has("SERIAL"))
                   		{
                   			Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
                   		}
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG, Url);
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL, Serial);
                	}
                	else if(Name.equalsIgnoreCase("PUSH_MSG_INFO"))//BJH
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("REQ_FORWARDING_INFO"))//BJH 2016.11.09
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO, Url);
                	}
                	else if(Name.equalsIgnoreCase("REQ_FORWARDING"))//BJH 2016.11.09
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING, Url);
                	}
                	else if(Name.equalsIgnoreCase("PUSH_RECORD"))//BJH 2016.12.14
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_RECORD, Url);
                	}
                	else if(Name.equalsIgnoreCase("ANDROID_VERSION"))//BJH
                	{
                		String version = "";
                		if (urlItemArray.getJSONObject(i).has("VERSION"))
                   		{
                			version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
                			mPrefs.setPreferenceStringValue(AppPrefs.NEW_VERSION, version);
                   		}

                	}
                	else if(Name.equalsIgnoreCase("OPEN_DNS_SERVER"))//BJH
                	{
                		String domain = "";
                		String server = "";
                		if (urlItemArray.getJSONObject(i).has("DOMAIN"))
                   		{
                			domain = urlItemArray.getJSONObject(i).getString("DOMAIN").toString();
                			mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN, domain);
                   		}
                		if (urlItemArray.getJSONObject(i).has("SERVER"))
                   		{
                			server = urlItemArray.getJSONObject(i).getString("SERVER").toString();
                			mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER, server);
                   		}
                	}
                	else if(Name.equalsIgnoreCase("USIM_INFO_VERSION"))//BJH 2017.01.05
                	{
                		String version = "";
                		if (urlItemArray.getJSONObject(i).has("VERSION"))
                   		{
                			version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
                			mPrefs.setPreferenceStringValue(AppPrefs.INFO_USIM_VER, version);
                   		}
                	}
                	else if(Name.equalsIgnoreCase("REQ_SVC_OUT"))//BJH
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_SVC_OUT, Url);
                	}
                	else if(Name.equalsIgnoreCase("USIM_POCKET_WIFI"))//BJH 2016.08.12
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI, Url);
                	}
                	else if(Name.equalsIgnoreCase("BRIDGE_PAYMENT"))//BJH 2016.11.24
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT, Url);
                	}
                	else if(Name.equalsIgnoreCase("USIM_LOOKUP"))//BJH 2017.02.02
                	{
                		mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_LOOKUP, Url);
                	}
					else if(Name.equalsIgnoreCase("USIM_ACTIVATION"))//BJH 2017.03.15
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_ACTIVATION, Url);
					}
					else if(Name.equalsIgnoreCase("REQ_MYINFO"))//BJH 2017.06.05
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_MYINFO, Url);
					}
					else if(Name.equalsIgnoreCase("REQ_CID_TYPE"))//BJH 2017.06.05
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE, Url);
					}
					else if(Name.equalsIgnoreCase("REQ_SVC_OUT_V2"))//BJH 2017.06.09
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2, Url);
					}
					else if(Name.equalsIgnoreCase("REQ_GET_POCKET"))//BJH 2017.11.08
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET, Url);
					}else if(Name.equalsIgnoreCase("COUPON_INFO"))// 2018.10.08
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, Url);
					}

                }
                // CHECK VALUE
                return 0;
            }
            else
            {
            	Log.d(THIS_FILE, responseObject.getString("RES_TYPE") + "ERROR ="+ responseObject.getString("RES_DESC"));
            }
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return -1;
		}

		return -1;
	}

	/*private int setGlobalInfo()	//20190101 info.json을 트래픽을 줄이기위하여 하드코딩해서 직접삽입
	{
		Log.d(THIS_FILE, "SETUP LOCAL GLOBAL INFO : START");

		mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL, "201901100001");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO, "https://if.maaltalk.com/maaltalk/req_client_info.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO, "https://if.maaltalk.com/maaltalk/req_svc_info.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_BALANCE, "https://if.maaltalk.com/maaltalk/req_balance.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL, "http://211.253.10.158/certcid_intl/req_valid_cid.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL, "http://211.253.10.158/certcid_intl/req_account_info.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2, "https://auth.maaltalk.com/certcid_global/req_account_info_intl.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC/maaltalk_cert.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/in.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL, "https://auth.maaltalk.com/certcid_maaltalk/SMART_ENC_V2/mt_auth.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2, "https://auth.maaltalk.com/certcid_locl_v2/req_account_info.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3, "https://auth.maaltalk.com/certcid_global/req_account_info_locl.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET, "https://auth.maaltalk.com/certcid_global/req_get_pocket.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION, "http://info.maaltalk.com/maaltalk_user/maaltalk_login.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION_V2, "http://info.maaltalk.com/maaltalk_user_v2/maaltalk_user.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE, "http://pay.maaltalk.com/payment/index.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI, "http://maaltalk.com/usim_pocket.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_ACTIVATION, "http://gi.esmplus.com/dial070/etc/after_update.jpg");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT, "http://pay.maaltalk.com/payment/bridge.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA, "http://pay.maaltalk.com/payment/payment_extra.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_SVC_OUT, "http://info.maaltalk.com/maaltalk_user/maaltalk_wait.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_HISTORY, "https://211.253.25.55/payment/req_history.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_CDR_INFO, "https://if.maaltalk.com/maaltalk/req_cdr.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO, "https://if.maaltalk.com/maaltalk/req_message.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, "https://if.maaltalk.com/maaltalk/req_send.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_AD_INFO, "https://if.maaltalk.com/maaltalk/req_ad.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_UPDATE_SVR, "https://if.maaltalk.com/maaltalk/req_svr_update.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO, "https://if.maaltalk.com/maaltalk/req_push_msg.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO, "https://if.maaltalk.com/maaltalk/req_forwarding_info.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING, "https://if.maaltalk.com/maaltalk/req_forwarding.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG, "http://info.maaltalk.com/maaltalk_user/userMsg/");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL, "201601180001");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_RECORD, "https://auth.maaltalk.com/push_record/req_record.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_LOOKUP, "http://info.maaltalk.com/maaltalk_usim/index.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_MYINFO, "https://if.maaltalk.com/maaltalk/req_myinfo.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE, "https://if.maaltalk.com/maaltalk/req_cid_type.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2, "https://if.maaltalk.com/maaltalk/req_svc_out.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_REC_DIR, "http://rec.maaltalk.com/maaltalk/rec");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_SERVICE_INFO, "http://www.maaltalk.com/intro.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_MANUAL, "http://www.maaltalk.com/use.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE, "http://www.maaltalk.com/charge_table.html");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_NOTICE, "http://tb.maaltalk.com/maaltalk_board/mtb/");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_FAQ, "http://www.maaltalk.com/faq.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_QUESTION, "http://www.maaltalk.com/qna.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS, "http://www.maaltalk.com/terms.php");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, "http://www.smarttravel.co.kr/maaltalk");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_TXT, "http://www.maaltalk.com/terms.txt");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT, "http://www.maaltalk.com/terms_info.txt");
		mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT, "http://www.maaltalk.com/terms_use.txt");
		mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER, "8.8.8.8");
		mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN, "cs2.maaltalk.com");
		mPrefs.setPreferenceStringValue(AppPrefs.INFO_USIM_VER, "1.0");

		Log.d(THIS_FILE, "SETUP LOCAL GLOBAL INFO : END");
		return 0;
	}*/

	private int requestNewGlobalInfo()	//20190101 새로운 info.json 로딩방식 적용
	{
		DebugLog.d("requestNewGlobalInfo()");

		long curTime = System.currentTimeMillis();
		Log.d(THIS_FILE, "curTime:"+curTime);
		mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SPLASH_EXECUTION_TIME, curTime+"");

		// SEND QUERY
		Log.d(THIS_FILE, "REQUEST NEW GLOBAL INFO : START");

		String localVersion = mPrefs.getPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL); //기존 말톡에 셋업된 info.json 버전 받아오기
		Log.d(THIS_FILE, "localInfoVersion : "+localVersion);

		App app= (App) getApplicationContext();
		if (localVersion.compareTo(app.INFO_JSON_VERSION) < 0){
			app.setGlobalInfo();
			localVersion = mPrefs.getPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL); //기존 말톡에 셋업된 info.json 버전 받아오기
			Log.d(THIS_FILE, "setup localInfoVersion : "+localVersion);
		}

		String serverInfoJsonVer = mPrefs.getPreferenceStringValue(AppPrefs.LATEST_GLOBAL_INFO_SERIAL);
		Log.d(THIS_FILE, "INFO JSON SERVER VERSION="+ serverInfoJsonVer);
		if (serverInfoJsonVer.equals("201901010001")){
			Log.d(THIS_FILE, "This position STOP...");
			return 0;
		}

		if(serverInfoJsonVer.compareTo(localVersion) < 1) {	//앞변수가 뒷변수보다 작거나 같으면 패스
			Log.d(THIS_FILE, "This position STOP");
			return 0;
		}

		String rs = postNewGlobalInfo();
		Log.d(THIS_FILE, "REQUEST NEW GLOBAL INFO : END");
		Log.d(THIS_FILE, "NewGlobalInfo RESULT : " + rs);
		if (rs == null || rs.length() == 0) return -1;

		JSONObject jObject;
		try
		{
			jObject = new JSONObject(rs);
			String serialAndroidInfo = jObject.getString("S");
			Log.d("info_android.json serial", serialAndroidInfo);

			JSONObject urlObject = jObject.getJSONObject("ITEMS");
			if (urlObject == null) return -1;

			Log.d(THIS_FILE, "This position PASS 1 !!!");
			mPrefs.setPreferenceStringValue(AppPrefs.GLOBAL_INFO_SERIAL, serialAndroidInfo);

			String Name = null;
			String Url = null;
			String Serial = null;

			JSONArray urlItemArray = urlObject.getJSONArray("URLS");
			if (urlItemArray == null) return -1;
			for (int i = 0; i < urlItemArray.length(); i++)
			{
				Name = urlItemArray.getJSONObject(i).getString("NAME").toString();
				Url = urlItemArray.getJSONObject(i).getString("URL").toString();

				Log.d(THIS_FILE, "NAME="+ urlItemArray.getJSONObject(i).getString("NAME"));
				Log.d(THIS_FILE, "URL="+ urlItemArray.getJSONObject(i).getString("URL"));

				if(Name == null) continue;


				if(Name.equalsIgnoreCase("REQ_CLIENT_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CLIENT_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_SVC_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO, Url);
				}

				// 인증
				else if(Name.equalsIgnoreCase("REQ_VALIDCID_LOCL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_VALIDCID_INTL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_INTL, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_VALIDCID_CODE"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_CODE, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL, Url);
				}

				else if(Name.equalsIgnoreCase("REQ_VALIDCID_LOCL_V2"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2, Url);
						/*Serial = "";
						if (urlItemArray.getJSONObject(i).has("ENC_V2"))
						{
							Serial = urlItemArray.getJSONObject(i).getString("ENC_V2").toString();
						}
						mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, Serial);*/
				}
				else if(Name.equalsIgnoreCase("ENC_V2"))	//20190110 json 일관성을 위해서 REQ_VALIDCID_LOCL_V2 안에 있던 ENC_V2을 따로 밖으로 뺌.
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_LOCL_V2_ENC_V2, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_VALIDCID_GLOBAL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_VALIDCID_GLOBAL, Url);

				}
				else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V2"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V2, Url);
				}

				//BJH 2017.04.24 인증 변경
				else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_INTL_V2"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_INTL_V2, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_ACCOUNT_INFO_LOCL_V3"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_ACCOUNT_INFO_LOCL_V3, Url);
				}

				else if(Name.equalsIgnoreCase("POPUP"))
				{
					Serial = "";
					if (urlItemArray.getJSONObject(i).has("SERIAL"))
					{
						Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
					}
					mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP, Url);
					mPrefs.setPreferenceStringValue(AppPrefs.URL_POPUP_SERIAL, Serial);
				}

				// 결제
				else if(Name.equalsIgnoreCase("REQ_BALANCE"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_BALANCE, Url);
				}
				else if(Name.equalsIgnoreCase("ADD_BALANCE"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE, Url);
				}
				else if(Name.equalsIgnoreCase("ADD_BALANCE_EXTRA"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_ADD_BALANCE_EXTRA, Url);
				}
				else if(Name.equalsIgnoreCase("MY_INFORMATION"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_MY_INFORMATION, Url);
				}

				// 통화기록
				else if(Name.equalsIgnoreCase("CDR_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_CDR_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("REC_DIR"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REC_DIR, Url);
				}
				else if(Name.equalsIgnoreCase("MESSAGE_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_MESSAGE_INFO, Url);
				}

				// 로그
				else if(Name.equalsIgnoreCase("REQ_HISTORY"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_HISTORY, Url);
				}

				// 게시판
				else if(Name.equalsIgnoreCase("SERVICE_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_SERVICE_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("MANUAL"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_MANUAL, Url);
				}
				else if(Name.equalsIgnoreCase("NOTICE"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_NOTICE, Url);
				}
				else if(Name.equalsIgnoreCase("FAQ"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_FAQ, Url);
				}
				else if(Name.equalsIgnoreCase("QUESTION"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_QUESTION, Url);
				}
				else if(Name.equalsIgnoreCase("KAKAO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_KAKAO, Url);
				}
				else if(Name.equalsIgnoreCase("RATE_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("RATE_INFO_URL")) // BJH 2017.01.20
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_RATE, Url);
				}
				else if(Name.equalsIgnoreCase("TERMS"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS, Url);
				}
				else if(Name.equalsIgnoreCase("TERMS_TXT"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_TXT, Url);
					if (urlItemArray.getJSONObject(i).has("INFO"))//BJH 2016.06.28 Terms 추가
					{
						mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_INFO_TXT, urlItemArray.getJSONObject(i).getString("INFO").toString());
						mPrefs.setPreferenceStringValue(AppPrefs.URL_TERMS_USE_TXT, urlItemArray.getJSONObject(i).getString("USE").toString());
					}
				}
				//BJH 광고, 메시지 보내기, 서버 업데이트
				else if(Name.equalsIgnoreCase("AD_INFO"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_AD_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("MSG_SEND"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_MSG_SEND, Url);
				}
				else if(Name.equalsIgnoreCase("UPDATE_SVR"))
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_UPDATE_SVR, Url);
				}
				else if(Name.equalsIgnoreCase("USER_MSG"))//BJH
				{
					Serial = "";
					if (urlItemArray.getJSONObject(i).has("SERIAL"))
					{
						Serial = urlItemArray.getJSONObject(i).getString("SERIAL").toString();
					}
					mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG, Url);
					mPrefs.setPreferenceStringValue(AppPrefs.URL_USER_MSG_SERIAL, Serial);
				}
				else if(Name.equalsIgnoreCase("PUSH_MSG_INFO"))//BJH
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_MSG_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_FORWARDING_INFO"))//BJH 2016.11.09
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING_INFO, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_FORWARDING"))//BJH 2016.11.09
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_FORWARDING, Url);
				}
				else if(Name.equalsIgnoreCase("PUSH_RECORD"))//BJH 2016.12.14
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_PUSH_RECORD, Url);
				}
				else if(Name.equalsIgnoreCase("ANDROID_VERSION"))//BJH
				{
					String version = "";
					if (urlItemArray.getJSONObject(i).has("VERSION"))
					{
						version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
						mPrefs.setPreferenceStringValue(AppPrefs.NEW_VERSION, version);
					}

				}
				else if(Name.equalsIgnoreCase("OPEN_DNS_SERVER"))//BJH
				{
					String domain = "";
					String server = "";
					if (urlItemArray.getJSONObject(i).has("DOMAIN"))
					{
						domain = urlItemArray.getJSONObject(i).getString("DOMAIN").toString();
						mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN, domain);
					}
					if (urlItemArray.getJSONObject(i).has("SERVER"))
					{
						server = urlItemArray.getJSONObject(i).getString("SERVER").toString();
						mPrefs.setPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER, server);
					}
				}
				else if(Name.equalsIgnoreCase("USIM_INFO_VERSION"))//BJH 2017.01.05
				{
					String version = "";
					if (urlItemArray.getJSONObject(i).has("VERSION"))
					{
						version = urlItemArray.getJSONObject(i).getString("VERSION").toString();
						mPrefs.setPreferenceStringValue(AppPrefs.INFO_USIM_VER, version);
					}
				}
				else if(Name.equalsIgnoreCase("REQ_SVC_OUT"))//BJH
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_SVC_OUT, Url);
				}
				else if(Name.equalsIgnoreCase("USIM_POCKET_WIFI"))//BJH 2016.08.12
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_POCKET_WIFI, Url);
				}
				else if(Name.equalsIgnoreCase("BRIDGE_PAYMENT"))//BJH 2016.11.24
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_BRIDGE_PAYMENT, Url);
				}
				else if(Name.equalsIgnoreCase("USIM_LOOKUP"))//BJH 2017.02.02
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_LOOKUP, Url);
				}
				else if(Name.equalsIgnoreCase("USIM_ACTIVATION"))//BJH 2017.03.15
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_USIM_ACTIVATION, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_MYINFO"))//BJH 2017.06.05
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_MYINFO, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_CID_TYPE"))//BJH 2017.06.05
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_CID_TYPE, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_SVC_OUT_V2"))//BJH 2017.06.09
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_SVC_OUT_V2, Url);
				}
				else if(Name.equalsIgnoreCase("REQ_GET_POCKET"))//BJH 2017.11.08
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_REQ_GET_POCKET, Url);
				}else if(Name.equalsIgnoreCase("COUPON_INFO"))// 2018.10.08
				{
					mPrefs.setPreferenceStringValue(AppPrefs.URL_COUPON_INFO, Url);
				}

			}
			// CHECK VALUE
			return 0;
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return -1;
		}
	}

	private String getServerInfoJsonVersion()
	{
		String url = ServicePrefs.getIniAndroidURL();
		Log.d(THIS_FILE, "getServerInfoJsonVersion url : " + url);

		if (url == null || url.length() == 0) return null;

		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(this);

			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 10000);
			HttpConnectionParams.setSoTimeout(params, 10000);

			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();

			String serialAndroidInfo;
			try {
				JSONObject jObject = new JSONObject(sb.toString());
				serialAndroidInfo = jObject.getString("S");
				Log.d("serialServerAndroidInfo", serialAndroidInfo);
				mPrefs.setPreferenceStringValue(AppPrefs.LATEST_GLOBAL_INFO_SERIAL, serialAndroidInfo);
				String versionAndroidClient = jObject.getString("V");
				Log.d("versionServerAndroidClient", versionAndroidClient);
				mPrefs.setPreferenceStringValue(AppPrefs.LATEST_CLIENT_VER, versionAndroidClient);


			} catch (JSONException e) {
				Log.e(THIS_FILE, "JSON", e);
				return null;
			}

			return serialAndroidInfo;
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException : "+ e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "getServerInfoJsonVersion Exception : "+e);
		}
		return null;
	}

	private String postGlobalInfo() {
		String url = ServicePrefs.getInfoURL();
		DebugLog.d("postGlobalInfo() --> "+url);
//		Log.d(THIS_FILE, "GLOBAL INFO : " + url);

		if (url == null || url.length() == 0) return null;

		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(this);

			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 10000);
			HttpConnectionParams.setSoTimeout(params, 10000);

			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();

			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}

	private String postNewGlobalInfo() {
		String url = ServicePrefs.getNewInfoURL();
		DebugLog.d("postNewGlobalInfo() --> "+url);
//		Log.d(THIS_FILE, "GLOBAL NEW INFO URL : " + url);

		if (url == null || url.length() == 0) return null;

		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(this);

			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 10000);
			HttpConnectionParams.setSoTimeout(params, 10000);

			HttpPost httppost = new HttpPost(url);

			httppost.setHeader("User-Agent", ServicePrefs.getUserAgent());
			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);

			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();

			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}

}
