package com.dial070.global;

import java.util.HashMap;
import java.util.Map;

public class GlobalConst
{
	/**
	 * 
	 */
	public static final int TONE_NONE = -1;
	/**
	 * 
	 */
	public static final int USER_KEY_CODE_BASE = 10000;
	/**
	 * 
	 */
	public static final int USER_KEY_CODE_CONTACT = USER_KEY_CODE_BASE + 1;
	/**
	 * 
	 */
	public static final int USER_KEY_CODE_GCALL = USER_KEY_CODE_BASE + 2;
	/**
	 * 
	 */
	public static final int USER_KEY_CODE_SIP = USER_KEY_CODE_BASE + 3;
	/**
	 * 
	 */
	public static final int USER_KEY_CODE_DELETE = USER_KEY_CODE_BASE + 4;

	public static final int USER_KEY_CODE_CLOSE = USER_KEY_CODE_BASE + 5;

	/**
	 * @return
	 */
	public static final Map<Integer, int[]> getDialMap()
	{
		HashMap<Integer, int[]> map = new HashMap<Integer, int[]>();
		map.put(0, new int[] { 0, 0, 106, 66 });
		map.put(1, new int[] { 0, 0, 106, 66 });
		map.put(2, new int[] { 0, 0, 106, 66 });
		map.put(3, new int[] { 0, 0, 106, 66 });
		map.put(4, new int[] { 0, 0, 106, 66 });
		map.put(5, new int[] { 0, 0, 106, 66 });
		map.put(6, new int[] { 0, 0, 106, 66 });
		map.put(7, new int[] { 0, 0, 106, 66 });
		map.put(8, new int[] { 0, 0, 106, 66 });
		map.put(9, new int[] { 0, 0, 106, 66 });
		map.put(10, new int[] { 0, 0, 106, 66 });
		map.put(11, new int[] { 0, 0, 106, 66 });
		map.put(12, new int[] { 0, 0, 106, 66 });
		map.put(13, new int[] { 0, 0, 106, 66 });
		map.put(14, new int[] { 0, 0, 106, 66 });
		map.put(15, new int[] { 0, 0, 106, 66 });
		return map;
	}
}
