package com.dial070.global;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import androidx.appcompat.app.AlertDialog;

import com.dial070.DialSplash;
import com.dial070.maaltalk.R;
//import com.dial070.service.PushyMQTT;
import com.dial070.sip.api.SipProfile;
import com.dial070.sip.service.SipService;
import com.dial070.utils.AppPrefs;
import com.dial070.utils.HttpsClient;
import com.dial070.utils.Log;


class MyDnsLookUpTaskParams {
	Context context;
	String str1;
	String str2;
	String str3;

	MyDnsLookUpTaskParams(Context ctxt, String str1, String str2, String str3) {
		this.context = ctxt;
		this.str1 = str1;
		this.str2 = str2;
		this.str3 = str3;
	}
}

public class ServicePrefs {
	private static final String THIS_FILE = "ServicePrefs";

	public static boolean mLogin = false;
	public static boolean mPush = false;
	public static String mUserID = null;
	public static String mUserPWD = null;
	public static String mUser070 = null;
	public static boolean mUse070 = false; // 발신번호로 070을 사용할지.
	public static boolean mUseMSG = false; // 메시지 수진 가능한지 여부.
	
	public static String mPayType = null;
	
	public static String mProxyIP = null;
	public static int mTransport = 0; // 0:UDP, 1:TCP, 2:TLS
	public static int mSRTP = 0; // 0:DISABLE, 1:OPTION, 2:MAN

	public static String mRTT_IP = null;
	public static int mRTT_PORT = 0;

	// ///////////////////////////////////////////////SIP //////////////////////////
	// FOR VERSION
	/**
	 * DIAL_RETAIL을 false로 하더라도 콜 푸시에 의해 실행된 말톡은 로그를 보려면 따로 수정해야 보임.
	 */
//	public static final boolean DIAL_RETAIL = true;	// 공기계로 테스트할때는 false
	public static final boolean DIAL_RETAIL = false;	// 공기계로 테스트할때는 false
	public static final boolean DIAL_TB = false;
	public static final boolean DIAL_DEBUG = false;
	public static final boolean DIAL_PAY_TEST = false; 	// 카드 결제시 TB로 결제할지 여부.
	public static final boolean DIAL_SHOW_SCORE = true; // 점수표 표시할지 여부
	public static final boolean DIAL_AUTO_HANGUP = true; // 통화중 셀룰러 통화 자동 끊기
	public static final boolean DIAL_GCM_CACHE = true; // GCM 등록 최소화.
	public static final boolean DIAL_AUTH_OTHER = false; // 다른폰으로 등록할지 여부
	public static final boolean DIAL_SKIP_DUP_CONTACTS = true; // 중복된 연락처 표시 안함
	public static final boolean DIAL_PLAY_OFFLINE = false; // OFFLINE시 안내 멘트 재생.
	public static final boolean DIAL_RECENT_MERGE = false; // 통화기록 머지할찌 여부.
	public static final boolean DIAL_AUTO_EXIT = true; // 백그라운드시 자동 종료. 
	
	public static final boolean DIAL_SIM_CHECK = true; // SIM 검사.
	public static final boolean DIAL_AUTH_TEST = false; // 본인인증 없이
	public static final boolean DIAL_AUTH_SGKIM = false; // 본인인증 : 김선관 테스트폰 인증용
	
	public static final boolean DIAL_MSG_SEND = true; // 메시지 발송기능 사용여부.

	// CODEC
	public static final boolean DIAL_CODEC_G729 = true;

	public static final boolean DIAL_CODEC_OPUS = false;
	public static final boolean DIAL_CODEC_PCMU = false;

	// PJSIP NETWORK POLLER THREAD COUNT
	public static final int DIAL_WORK_THREAD_COUNT = 1;
	
	// FOR TEST
	public static final boolean DIAL_TEST = false;
	
	public static final String DIAL_TEST_SERVER = "192.168.0.200";
	public static final String DIAL_TEST_ID = "1001";
	public static final String DIAL_TEST_PWD = "1001";
	
	/*
	public static final String DIAL_TEST_ID = "1011";
	public static final String DIAL_TEST_PWD = "07076873000";
	*/
	
	// 일부 단말에서 WIFI lock을 사용할 수 없는 경우..
	public static final boolean DIAL_TEST_SKIP_LOCK = false;
	
	// A00 -> 일반 릴리즈
	// A01 -> RTT 검사 안함
	
	// 버전 정보
	public static final String DIAL_NAME = "MaalTalk";
	public static final String DIAL_BENDER = "A00";
	public static final String DIAL_BENDER_TB = "000";
	public static String DIAL_VERSION = "1.0";
	public static final String DIAL_EMAIL = "maaltalk@dial070.co.kr";
	
	// 여기까지만 수정하면 됩니다.
	// /////////////////////////////////////////////////////////////////////////
	private static final String DIAL_DOMAIN = "www.maaltalk.com";
	private static final String DIAL_DOMAIN_TB = "14.63.167.218";

	//
	public static final String NAVER_KEY = "64fa45fd2b335028173861e14e822b59";
	//public static final String NAVER_KEY = "9c13b06667b26e5dbe6e83a44641f063";
	public static final int NAVER_RESULT_CNT = 30;
	public static final String NAVER_CLIENT_ID = "Vxy1DD8bF7TDYPzghIiO";
	public static final String NAVER_CLIENT_SECRET = "mII0sNDZSj"; // BJH 2016.12.27 Naver API 종료
	//
	public static final int URL_DIAL_070 = 1;
	public static final int CLOCK_RATE = 16000;

	public static String DID_EXPIRED = "";
	
	public static String getDomain() {
		if (DIAL_TB)
		{
			return DIAL_DOMAIN_TB;
		}
		else
		{
			return DIAL_DOMAIN;
		}
	}
	
	public static String getVersion() {
		if (DIAL_TB)
		{
			return DIAL_NAME + " " + DIAL_BENDER_TB + "-" + DIAL_VERSION;
		}
		else
		{
			return DIAL_NAME + " " + DIAL_BENDER + "-" + DIAL_VERSION;
		}
	}
	
	public static String getLoginVersion() {
		if (DIAL_TB)
		{
			return DIAL_BENDER_TB + "-" + DIAL_VERSION;
		}
		else
		{
			return DIAL_BENDER + "-" + DIAL_VERSION;
		}
	}
		
	
	public static String getUserAgent() {

		if (DIAL_TB)
		{
			return DIAL_NAME + " " + DIAL_BENDER_TB + "-" + DIAL_VERSION;
		}
		else
		{
			return DIAL_NAME + " " + DIAL_BENDER + "-" + DIAL_VERSION;
		}
	}

	public static String getIniAndroidURL() {	// 20190110 info.json을 이제는 내장시키고 시리얼만 체크해서 변경된것있으면 해당사항만 업데이트하기 위한 url
		if (DIAL_TB)
		{
			return "http://" + DIAL_DOMAIN_TB + "/maaltalk/ini_android.json";
		}
		else
		{
			return "http://" + DIAL_DOMAIN + "/ini_android.json";
		}
	}
	
	public static String getInfoURL() {
		if (DIAL_TB)
		{
			return "http://" + DIAL_DOMAIN_TB + "/maaltalk/info.json";
		}
		else
		{
			return "http://" + DIAL_DOMAIN + "/info.json";
			//return "http://" + DIAL_DOMAIN + "/info_tb.json";
		}
	}

	public static String getNewInfoURL() {	// 20190110 새로운 info.json
		if (DIAL_TB)
		{
			return "http://" + DIAL_DOMAIN_TB + "/maaltalk/info_android.json";
		}
		else
		{
			return "http://" + DIAL_DOMAIN + "/info_android.json";
			//return "http://" + DIAL_DOMAIN + "/info_tb.json";
		}
	}
	
	public static boolean isCheckRTT()
	{
		if (DIAL_BENDER.equalsIgnoreCase("A01"))
		{
			return false;
		}
		return true;
	}
	
	public static boolean checkGps(Context context)
	{
//		String gpsSettings = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//		if (gpsSettings.indexOf("gps", 0) < 0) return false;
//		return true;
		return false;
	}		
	
	public static String postLoginData(Context context, String userid, String password)
	{
		long startpostLoginDataTime=System.currentTimeMillis();

		AppPrefs mPrefs = new AppPrefs(context);
		String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_SVC_INFO);
		
		String query_url = url + "?param1="+userid+"&param2="+password + "&param3="+getLoginVersion();
		Log.d(THIS_FILE, "REQ_SVC_INFO : " + query_url);
		
		try
		{
			/*HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			//HttpProtocolParams.setContentCharset(params, "utf-8");
			HttpConnectionParams.setConnectionTimeout(params, 2000);
			HttpConnectionParams.setSoTimeout(params, 2000);

			HttpClient httpclient = new DefaultHttpClient(params);*/

            HttpClient httpclient = new HttpsClient(context);

            HttpParams params = httpclient.getParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);

			HttpPost httppost = new HttpPost(query_url);
			
			// Add your data
//			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//			nameValuePairs.add(new BasicNameValuePair("param1", userid));
//			nameValuePairs.add(new BasicNameValuePair("param2", password));
//			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			Log.d(THIS_FILE, "HTTPS POST EXEC OK");
			String line = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();

			Log.d(THIS_FILE,"postLoginData during : "+(System.currentTimeMillis()-startpostLoginDataTime)+" ms");

			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		} catch (SocketTimeoutException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "SocketTimeoutException", e);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}
	
	public static int login(Context context, String userid, String password) {
		Log.d(THIS_FILE,"login : "+userid);
		logout();

		if (ServicePrefs.DIAL_TEST) {
			mLogin = true;
			mUserID = ServicePrefs.DIAL_TEST_ID;
			mUserPWD = ServicePrefs.DIAL_TEST_PWD;
			mProxyIP = ServicePrefs.DIAL_TEST_SERVER;
			mUser070 = null;
			mUse070 = false; 
			mTransport = 0;
			mSRTP = 0;				
			return 0;					
		}
	
		return login_process(context, userid, password);		
	}
	

	public static String getSIMCountry(Context context) {
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyMgr == null) return "";
        
        int state = mTelephonyMgr.getSimState();
        if (state != TelephonyManager.SIM_STATE_READY) return "";
        
        //state = mTelephonyMgr.getPhoneType();
        //if (state == TelephonyManager.PHONE_TYPE_NONE) return "";        
        
        String code = mTelephonyMgr.getSimCountryIso();
        if (code == null) code = "";
        return code;
    }
	
	public static int login_check(Context context)
	{
		if (mUserID != null && mUserID.length() > 0)
			return login_process(context, mUserID, mUserPWD);
		return 0;
	}
	
	private static int login_process(Context context, String userid, String password)
	{
		int rs = login_process_real(context, userid, password);
		if (rs >= 0)
		{
			// 성공
			backupConfig(context);
			return rs;
		}

		if(rs == -2)
		{
			// BJH 2017.10.20 기기변경
			return rs;
		}
		
		if (rs == -90 || rs == -91)
		{
			// 재인증이 필요함
			return rs;
		}

		// 그 이외의 경우에는 전에 저장된 값을 기반으로 에러를 복구한다.
		if (restoreConfig(context)) 
		{
			// 로그인 처리.
			mLogin = true;
			mUserID = userid;
			mUserPWD = password;			
			return 0;
		}
		
		// 복구하지 못한 경우 에러 처리
		return rs;
	}
	
	private static boolean backupConfig(Context context) {
		Log.d(THIS_FILE, "backupConfig");
		
		AppPrefs mPrefs = new AppPrefs(context);
		mPrefs.setPreferenceStringValue(AppPrefs.CONF_PROXY, mProxyIP);
		mPrefs.setPreferenceIntegerValue(AppPrefs.CONF_TRANSPORT, mTransport);
		mPrefs.setPreferenceIntegerValue(AppPrefs.CONF_SRTP, mSRTP);
		mPrefs.setPreferenceStringValue(AppPrefs.CONF_NUM_070, mUser070);
		mPrefs.setPreferenceBooleanValue(AppPrefs.CONF_USE_070, mUse070);
		mPrefs.setPreferenceStringValue(AppPrefs.CONF_PAY_TYPE, mPayType);
		mPrefs.setPreferenceBooleanValue(AppPrefs.CONF_USE_MSG, mUseMSG );
		
		return true;
	}
	
	private static boolean restoreConfig(Context context) {
		AppPrefs mPrefs = new AppPrefs(context);
		String value = mPrefs.getPreferenceStringValue(AppPrefs.CONF_PROXY);
		if (value == null || value.length() == 0) return false;
		
		Log.d(THIS_FILE, "restoreConfig");
		
		// 설정 복원
		
		mProxyIP = mPrefs.getPreferenceStringValue(AppPrefs.CONF_PROXY);
		mTransport = mPrefs.getPreferenceIntegerValue(AppPrefs.CONF_TRANSPORT);
		mSRTP = mPrefs.getPreferenceIntegerValue(AppPrefs.CONF_SRTP);
		mUser070 = mPrefs.getPreferenceStringValue(AppPrefs.CONF_NUM_070);
		mUse070 = mPrefs.getPreferenceBooleanValue(AppPrefs.CONF_USE_070);
		mPayType = mPrefs.getPreferenceStringValue(AppPrefs.CONF_PAY_TYPE);
		mUseMSG = mPrefs.getPreferenceBooleanValue(AppPrefs.CONF_USE_MSG);

		return true;
	}
	
	
	
	// logout을 하지 않고 수행함.
	private static int login_process_real(Context context, String userid, String password) {
		String str = postLoginData(context, userid, password);
		long startlogin_process_realTime=System.currentTimeMillis();

		if (str == null || str.length() == 0)
		{
			Log.e(THIS_FILE, "NOT RESPONSE");
			return -99; // for retry
		}
		
		Log.d(THIS_FILE, "LOGIN : " + str);
		
		AppPrefs mPrefs = new AppPrefs(context);
		
		String server = null;
//		String domain = null;
		String tls = null;
		String srtp = null;
		String num070 = null;
		String cid_type = null;
		String sim_check = null;
		String pay_type = null;
		String msg_type = null; // FOR MESSAGE
		String rtt_ip = null;
		String rtt_port = null;

		try
		{
			JSONObject jObject = new JSONObject(str);
			JSONObject responseObject = jObject.getJSONObject("RESPONSE");
			Log.d("RES_CODE", responseObject.getString("RES_CODE"));
			Log.d("RES_TYPE ", responseObject.getString("RES_TYPE"));
			Log.d("RES_DESC ", responseObject.getString("RES_DESC"));
			
			if (responseObject.getInt("RES_CODE") == 0)
			{
				JSONObject serviceObject = jObject.getJSONObject("SVC_INFO");
				if (serviceObject.has("SRV_IP1")) 
					server = serviceObject.getString("SRV_IP1");
//				if (serviceObject.has("DOMAIN"))
//					domain = serviceObject.getString("DOMAIN");
				if (serviceObject.has("TLS"))
					tls = serviceObject.getString("TLS");
				if (serviceObject.has("SRTP"))
					srtp = serviceObject.getString("SRTP");
				if (serviceObject.has("070NUMBER"))
					num070 = serviceObject.getString("070NUMBER");
				if (serviceObject.has("CID_TYPE"))
					cid_type = serviceObject.getString("CID_TYPE");
				if (serviceObject.has("SIM_CHECK"))
					sim_check = serviceObject.getString("SIM_CHECK");
				if (serviceObject.has("PAY_TYPE"))
					pay_type = serviceObject.getString("PAY_TYPE");
				if (serviceObject.has("MSG_TYPE"))
					msg_type = serviceObject.getString("MSG_TYPE");
				//BJH 자동 녹취
				if (serviceObject.has("AUTO_REC"))
					mPrefs.setPreferenceStringValue(AppPrefs.AUTO_REC, serviceObject.getString("AUTO_REC"));
				//BJH 2016.06.14
				if (serviceObject.has("CODE"))
					mPrefs.setPreferenceStringValue(AppPrefs.DISTRIBUTOR_ID, serviceObject.getString("CODE"));
				//BJH 2016.10.17 사장님 요청 사항
				if (serviceObject.has("COUNTRY_CODE"))
					mPrefs.setPreferenceStringValue(AppPrefs.IP_COUNTRY_CODE, serviceObject.getString("COUNTRY_CODE"));
				//BJH 2017.03.27
				if(serviceObject.has("KEYWORD"))
					mPrefs.setPreferenceStringValue(AppPrefs.MT_KEYWORD, serviceObject.getString("KEYWORD"));
                //BJH 2017.09.01 사장님 요청
                if(serviceObject.has("VERIFIED"))
                    mPrefs.setPreferenceStringValue(AppPrefs.MT_VERIFIED, serviceObject.getString("VERIFIED"));
                if(serviceObject.has("URL"))
                    mPrefs.setPreferenceStringValue(AppPrefs.CO_REQ_URL, serviceObject.getString("URL"));
				// by sgkim : 2017.08.24 => 2017.12.27
				if(serviceObject.has("RTT_IP"))
					rtt_ip = serviceObject.getString("RTT_IP");
				if(serviceObject.has("RTT_PORT"))
					rtt_port = serviceObject.getString("RTT_PORT");
			}
			else
			{
				Log.d(THIS_FILE, "TYPE=" + responseObject.getString("RES_TYPE") + " ERROR ="+ responseObject.getString("RES_DESC"));
				return -2; // BJH 2017.10.20 기기변경
			}
			
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "JSON", e);
			return -1;
		}

		Log.d(THIS_FILE,"login_process_real json parsing during : "+(System.currentTimeMillis()-startlogin_process_realTime)+" ms");
		
		//BJH 처음 인증 했을 때 외국국가이면 eDNS
		String country_code = getSIMCountry(context);
		String foreign_use = mPrefs.getPreferenceStringValue(AppPrefs.FOREIGN_USE);
		if( !country_code.equalsIgnoreCase("kr") && foreign_use.length() == 0 ) {
			//mPrefs.setPreferenceBooleanValue(AppPrefs.FOREIGN_USE_CHECK, true);
			String lookup_ip = eDNS(context);
			Log.d(THIS_FILE, "Lookup IP: "+lookup_ip);
			if(lookup_ip != null) {
				String arr[] = server.split(":");
				if(!arr[0].equals(lookup_ip)){
					Log.d(THIS_FILE, "server != lookup_ip");
					String srvData = postSvrData(context,userid,lookup_ip);
					try
					{
						JSONObject jObject_svr = new JSONObject(srvData);
						JSONObject responseObject_svr = jObject_svr.getJSONObject("RESPONSE");
						Log.d(THIS_FILE, responseObject_svr.getString("RES_DESC"));
						if (responseObject_svr.getInt("RES_CODE") == 0){
							server = lookup_ip.concat(":".concat(arr[1]));
							tls = "0";
							srtp = "0";
							Log.d(THIS_FILE, "SVR_IPADDR : "+server);
						}
						mPrefs.setPreferenceStringValue(AppPrefs.FOREIGN_USE,"Y");
					}
					catch (Exception e)
					{
						Log.e(THIS_FILE, "JSON", e);
					}
				}
				else {
					mPrefs.setPreferenceStringValue(AppPrefs.FOREIGN_USE,"Y");
				}
			}
		}

		Log.d(THIS_FILE,"login_process_real country_code during : "+(System.currentTimeMillis()-startlogin_process_realTime)+" ms");
		
		mLogin = true;
		mUserID = userid;
		mUserPWD = password;
		mUser070 = num070;
		mPayType = pay_type;
		
		mUse070 = false;
		if (cid_type != null && cid_type.equalsIgnoreCase("d"))
			mUse070 = true;
		
		mUseMSG = false;
		if (msg_type != null && msg_type.equalsIgnoreCase("Y"))
			mUseMSG = true;
		
		mProxyIP = server;
		mTransport = 0;
		if (tls != null && tls.equals("1"))
			mTransport = 2;
		
		mSRTP = 0;
		if (srtp != null && srtp.length() > 0)
		{
			try
			{
				mSRTP =  Integer.parseInt(srtp);
			}
			catch (Exception e)
			{
			}
		}
		
		// FOR TESET
		//mProxyIP = "110.45.220.102:5061";
		//mTransport = 2;
		//Log.e(THIS_FILE, "CONTEXT" + context);

		// FOR RTT
		mRTT_IP = rtt_ip;

		mRTT_PORT = 0;
		if (rtt_port != null && rtt_port.length() > 0)
			mRTT_PORT = Integer.parseInt(rtt_port);

		// 발신 번호가 070 번호가 아닌경우, 82로 시작되는 경우 sim 카드 검사.
		if ((sim_check != null) && (sim_check.length() > 0)  && !mUse070)
		{
			String number = mPrefs.getPreferenceStringValue(AppPrefs.MY_PHONE_NUMBER);
			if (number != null && number.length() > 0 && number.startsWith("821"))
			{
				String code = getSIMCountry(context);
				if (code == null || code.length() == 0)
				{
					// SIM 없음.
					return -90;
					
				}
				if (!code.equalsIgnoreCase("kr"))
				{
					// 한국이 아님?
					return -91;
				}
			}
		}

		Log.d(THIS_FILE,"login_process_real end during : "+(System.currentTimeMillis()-startlogin_process_realTime)+" ms");
		
		return 0;		
	}
		
	
	public static void logout() {
		Log.d(THIS_FILE,": : logout");
		mLogin = false;
		mUserID = null;
		mUserPWD = null;
		//mUser070 = null;
		mUse070 = false; 
	}
	
	public static SipProfile buildAccount(SipProfile account) {
		String userid = mUserID;
		String password = mUserPWD;
		String username = userid;
		String proxyip = mProxyIP;
		try
		{
			account.id = 0;
			account.display_name = userid;
			account.acc_id = "<sip:" + username + "@" + proxyip + ">";

			String regUri = "sip:" + proxyip;
			account.reg_uri = regUri;
			account.proxies = new String[] { regUri };

			account.realm = "*";
			account.username = username;
			account.data = password;
			account.scheme = "Digest";
			account.datatype = SipProfile.CRED_DATA_PLAIN_PASSWD;
			account.reg_timeout = 60;
			account.use_srtp = mSRTP;
			
			if (mTransport == 2)
				account.transport = SipProfile.TRANSPORT_TLS;
			else if (mTransport == 1)
				account.transport = SipProfile.TRANSPORT_TCP;
			else
				account.transport = SipProfile.TRANSPORT_UDP;
		}
		catch (Exception e)
		{
			Log.e(THIS_FILE, "buildAccount", e);
			return null;
		}
		return account;
	}
	
	
	
	public static void ReqHistory(final Context context, final String name, final String data) {
		Thread t = new Thread()
		{
			public void run()
			{
				try
				{
					Log.e(THIS_FILE, "postLogData start");
					postLogData(context, name, data);
					Log.e(THIS_FILE, "postLogData end");
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}			
			}
		};
		if (t != null)
		{
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
	}	
	
	public static String getModel()
	{
		String deviceModel=Build.MODEL;
		if (deviceModel==null) {
			deviceModel="none";
		}
		
		String osVersion=Build.VERSION.RELEASE;
		if (osVersion==null) {
			osVersion="none";
		}		
		
		return deviceModel+"("+osVersion+")";
	}	
	
	private static String postLogData(final Context context, final String name, final String data) {
		AppPrefs mPrefs = new AppPrefs(context);
		String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_REQ_HISTORY);
		String userid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
		String device = "ANDROID";
		String model = getModel();
		
		String query_url = url;// + "?param1="+userid+"&param2="+group + "&param3="+name + "&param4="+data;
		Log.d(THIS_FILE, "REQ_HISTORY : " + query_url);

		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(context);
			
			// SET TIMEOUT
			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 15000);
			HttpConnectionParams.setSoTimeout(params, 15000);
			 
			HttpPost httppost = new HttpPost(query_url);
			
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("param1", userid));
			nameValuePairs.add(new BasicNameValuePair("param2", device));
			nameValuePairs.add(new BasicNameValuePair("param3", getVersion() + " " + model));
			nameValuePairs.add(new BasicNameValuePair("param4", name));
			nameValuePairs.add(new BasicNameValuePair("param5", data));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			Log.d(THIS_FILE, "HTTPS POST EXEC OK");
			String line = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}
	
	// BJH eDNS
	public static byte[] buildRequestData(String host) {
		//head + (host length +1) + eof sign + qtype + qclass
		int size = 12 + host.length() + 1 + 1+ 4;
		ByteBuffer buff = ByteBuffer.allocate(size);

		Random random = new Random();
		byte[] seq = new byte[2];
		random.nextBytes(seq);
		buff.put(seq);
		byte[] header = {0x01,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00};
		buff.put(header);
		//add query question domain field
		String[] parts = host.split("\\.");
		for(int i= 0; i < parts.length;i++) {
			buff.put((byte) parts[i].length());
			buff.put(parts[i].getBytes());
		}
		buff.put((byte)0x00);
		byte[] tmp = {0x00,0x01,0x00,0x01};
		buff.put(tmp);

		return buff.array();
	}

	public static String decodeDnsResponse(byte[] resp,String host) {

		ByteBuffer buffer = ByteBuffer.wrap(resp);
		//parse the query answer count.
		int pos = 6;
		buffer.position(pos);
		short qncount = buffer.getShort();
//		for(int i = 0; i)
		//skip query answer field
		pos = 12 + host.length() + 1 + 1 + 4;
		buffer.position(pos);
		for(int i= 0 ; i < qncount; i++ ) {
			buffer.position(pos);
			byte pointFlg = buffer.get();
			if((pointFlg & 0xc0) == 0xc0 ) {
				pos+=2;  //point
			} else {
				pos+= 1+ host.length() + 1;
			}

			buffer.position(pos);
			int queryType =  buffer.getShort();

			pos += 8;
			buffer.position(pos);
			int dataLen = buffer.getShort();
			pos +=2; //move to data area
			//A record
			if(queryType == 0x0001) {

				String ip = "";
				for(int j = 0; j < dataLen ; j ++) {
					buffer.position(pos);
					int v  = buffer.get();
					v = v>0?v:0x0ff & v;
					ip += v + (j== dataLen -1 ? "":".");
					pos+=1;
				}
				//Log.d(THIS_FILE,"ip: "+ip);
				//return first available ip
				return ip;

			} else {
				pos+=dataLen;
			}
		}

		return null;
	}

	private static String eDNS(Context context) {
		AppPrefs mPrefs = new AppPrefs(context);
		String domain = mPrefs.getPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN);
		String server = mPrefs.getPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER);
		String dns_lookup = null;
		byte[] recvData = new byte[512];
		byte[] data = buildRequestData(domain);

		try {
			//InetAddress inet = InetAddress.getByName("8.8.8.8");
			SocketAddress googleDns = new InetSocketAddress(server, 53);
			DatagramPacket dataPacket = new DatagramPacket(data,data.length, googleDns);
			DatagramSocket dataSocket = new DatagramSocket();
			dataSocket.send(dataPacket);
			byte[] respData = null;
			DatagramPacket receivePacket = new DatagramPacket(recvData, recvData.length) ;
			dataSocket.setSoTimeout(5000);
			dataSocket.receive(receivePacket);
			respData = receivePacket.getData();
			if(respData != null) {
				Log.d(THIS_FILE,"OPEN DNS LOOKUP");
				dns_lookup =  decodeDnsResponse(respData,domain);
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(dns_lookup == null){
			Log.d(THIS_FILE,"OPEN DNS LOOKUP NULL");
			try {
				InetAddress[] inetAddressArray = InetAddress.getAllByName(domain);
				dns_lookup = inetAddressArray[0].getHostAddress();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Log.d(THIS_FILE,"dns_lookup: "+dns_lookup);
		return dns_lookup;
	}

	public static String postSvrData(final Context context, final String userid, final String svr)
	{

		AppPrefs mPrefs = new AppPrefs(context);
		String url = mPrefs.getPreferenceStringValue(AppPrefs.URL_UPDATE_SVR);
		Log.d(THIS_FILE, "REQ_UPDATE_SVR : " + url);

		try
		{
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new HttpsClient(context);

			// SET TIMEOUT
			HttpParams params = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 15000);
			HttpConnectionParams.setSoTimeout(params, 15000);

			HttpPost httppost = new HttpPost(url);

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("param1", userid));
			nameValuePairs.add(new BasicNameValuePair("param2", svr));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

			// Execute HTTP Post Request
			Log.d(THIS_FILE, "HTTPS POST EXEC");
			HttpResponse response = httpclient.execute(httppost);
			Log.d(THIS_FILE, "HTTPS POST EXEC OK");
			String line = null;
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
			br.close();
			return sb.toString();
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "ClientProtocolException", e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			Log.e(THIS_FILE, "Exception", e);
		}
		return null;
	}
	
	//BJH
	private static void Alert(Context context, String msg) {
		AlertDialog.Builder msgDialog = new AlertDialog.Builder(context);
		msgDialog.setTitle(context.getResources().getString(R.string.app_name));
		msgDialog.setMessage(msg);
		msgDialog.setNegativeButton(context.getResources().getString(R.string.close), null);
		msgDialog.show();
	}

	/* BJH Google DNS */	
	public static void DNSLookUp(final Context context) {
		(new AsyncTask<Void, Void, String>(){
			@Override
			protected String doInBackground(Void... params) {
				AppPrefs mPrefs = new AppPrefs(context);
				String domain = mPrefs.getPreferenceStringValue(AppPrefs.OPEN_DNS_DOMAIN);
				String server = mPrefs.getPreferenceStringValue(AppPrefs.OPEN_DNS_SERVER);
				String userid = mPrefs.getPreferenceStringValue(AppPrefs.USER_ID);
				String lookup_ip = null;
				byte[] recvData = new byte[512];
				byte[] data = buildRequestData(domain);

				try {
					// InetAddress inet = InetAddress.getByName("8.8.8.8");
					SocketAddress googleDns = new InetSocketAddress(server, 53);
					DatagramPacket dataPacket = new DatagramPacket(data, data.length,
							googleDns);
					DatagramSocket dataSocket = new DatagramSocket();
					dataSocket.send(dataPacket);
					byte[] respData = null;
					DatagramPacket receivePacket = new DatagramPacket(recvData,
							recvData.length);
					dataSocket.setSoTimeout(5000);
					dataSocket.receive(receivePacket);
					respData = receivePacket.getData();
					if (respData != null) {
						Log.d(THIS_FILE, "OPEN DNS LOOKUP");
						lookup_ip = ServicePrefs.decodeDnsResponse(respData, domain);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (lookup_ip == null) {
					Log.d(THIS_FILE, "OPEN DNS LOOKUP NULL");
					try {
						InetAddress[] inetAddressArray = InetAddress
								.getAllByName(domain);
						lookup_ip = inetAddressArray[0].getHostAddress();
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				Log.d(THIS_FILE, "lookupIp : " + lookup_ip);
				String str = postSvrData(context, userid, lookup_ip);
				Log.d(THIS_FILE, "postSvrData result : " + str);
				return str;
			}

			@Override
			protected void onPostExecute(String str) {
				String msg_fail = context.getResources().getString(R.string.setting_edns_fail);
				String msg_overlap = context.getResources().getString(R.string.setting_edns_best);
				String msg_kr = context.getResources().getString(R.string.setting_edns_oversea);

				JSONObject jObject;
				try {
					jObject = new JSONObject(str);
					JSONObject responseObject = jObject.getJSONObject("RESPONSE");
					Log.d(THIS_FILE, responseObject.getString("RES_CODE"));
					Log.d(THIS_FILE, responseObject.getString("RES_TYPE"));
					Log.d(THIS_FILE, responseObject.getString("RES_DESC"));
					Log.d(THIS_FILE, responseObject.getString("UPDATE_SVR"));
					if (responseObject.getInt("RES_CODE") == 0) {
						String update_svr = responseObject.getString("UPDATE_SVR");
						//BJH 2016.06.21 서버 변경 수정

						(new AsyncTask<String, Void, Bundle>(){
							@Override
							protected Bundle doInBackground(String... params) {
								int rs = login_check(context);// BJH
								Bundle myBundle = new Bundle();
								myBundle.putInt("int_rs", rs);
								myBundle.putString("str_param", params[0]);
								return myBundle;
							}

							@Override
							protected void onPostExecute(Bundle rs) {
								if (rs.getInt("int_rs") >= 0) {
									SipService.currentService.changeSipStack();
									String desc = rs.getString("str_param").concat(context.getResources().getString(R.string.svr_change));
									Alert(context, desc);
								} else{
									restartMaaltalk(context,rs.getString("str_param"));
								}

								super.onPostExecute(rs);
							}
						}).execute(update_svr);
					} else {
						if (responseObject.getString("RES_DESC").equals("OVERLAP")) {
							Alert(context, msg_overlap);
						} else if (responseObject.getString("RES_DESC").equals("KR")) {
							Alert(context, msg_kr);
						} else {
							Alert(context, msg_fail);
						}
					}
				} catch (Exception e) {
					Log.e(THIS_FILE, "JSON", e);
					Log.d(THIS_FILE, "JSON ERROR");
					Alert(context, msg_fail);
				}

				super.onPostExecute(str);
			}
		}).execute();
	}
	
	private static void restartMaaltalk(final Context context, String update_svr)// SIP 다시 레지스터 하기 위해서

	{
		String msgs = update_svr.concat(context.getResources().getString(R.string.setting_edns_restart));
		AlertDialog.Builder msgDialog = new AlertDialog.Builder(context);
		msgDialog.setTitle(context.getResources().getString(R.string.app_name));
		// msgDialog.setMessage((String)msg.obj);
		msgDialog.setMessage(msgs);
		msgDialog.setIcon(R.drawable.ic_launcher);
		msgDialog.setNegativeButton(context.getResources().getString(R.string.close),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						context.stopService(new Intent(context, SipService.class));
						Intent IntroIntent = new Intent(context,
								DialSplash.class);
						IntroIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(IntroIntent);
						((Activity) context).finish();
					}
				});
		msgDialog.show();
	}

	public static void setDidExpired(String d_day){
		DID_EXPIRED=d_day;
	}

	public static String getDidExpired(){
		return DID_EXPIRED;
	}
}
