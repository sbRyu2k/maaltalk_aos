ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* key\
* key\dial070.keystore
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

google-play-services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets
* libs\armeabi-v7a\libpj_opensl_dev.so => app\src\main\jniLibs\armeabi-v7a\libpj_opensl_dev.so
* libs\armeabi-v7a\libpjsipjni.so => app\src\main\jniLibs\armeabi-v7a\libpjsipjni.so
* libs\armeabi-v7a\libstlport_shared.so => app\src\main\jniLibs\armeabi-v7a\libstlport_shared.so
* libs\armeabi\libpj_opensl_dev.so => app\src\main\jniLibs\armeabi\libpj_opensl_dev.so
* libs\armeabi\libpjsipjni.so => app\src\main\jniLibs\armeabi\libpjsipjni.so
* libs\armeabi\libstlport_shared.so => app\src\main\jniLibs\armeabi\libstlport_shared.so
* libs\kcp_smart_phone_util.jar => app\libs\kcp_smart_phone_util.jar
* libs\opencsv-3.8.jar => app\libs\opencsv-3.8.jar
* libs\pushy-jackson.jar => app\libs\pushy-jackson.jar
* libs\sdk-1.0.15.jar => app\libs\sdk-1.0.15.jar
* libs\universal-image-loader-1.9.4.jar => app\libs\universal-image-loader-1.9.4.jar
* lint.xml => app\lint.xml
* res\ => app\src\main\res\
* src\ => app\src\main\java\
* src\com\dial070\sip\api\ISipConfiguration.aidl => app\src\main\aidl\com\dial070\sip\api\ISipConfiguration.aidl
* src\com\dial070\sip\api\ISipService.aidl => app\src\main\aidl\com\dial070\sip\api\ISipService.aidl
* src\com\dial070\sip\api\NOTICE => app\src\main\resources\com\dial070\sip\api\NOTICE
* src\com\dial070\sip\api\SipCallSession.aidl => app\src\main\aidl\com\dial070\sip\api\SipCallSession.aidl
* src\com\dial070\sip\api\SipProfile.aidl => app\src\main\aidl\com\dial070\sip\api\SipProfile.aidl
* src\com\dial070\sip\api\SipProfileState.aidl => app\src\main\aidl\com\dial070\sip\api\SipProfileState.aidl
* src\com\dial070\sip\service\MediaManager.java-ori => app\src\main\resources\com\dial070\sip\service\MediaManager.java-ori

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
